
#ifndef _STM_CRYPTO_H_
#define _STM_CRYPTO_H_

#include "crypto.h"

#define  PublicExponent  7

uint32_t RSA_Encrypt(uint8_t* Message,    uint32_t Lenght, 
                     uint32_t Key, 
                     uint8_t* OutMessage, uint32_t* OutLenght);

uint32_t Get_AES128_Key (uint8_t* Personalization, uint8_t* AES128_Key);

int32_t AES128_CFB_Encrypt(uint8_t* InputMessage,
                              uint32_t InputMessageLength,
                              uint8_t  *AES128_Key,
                              uint8_t  *OutputMessage);


int32_t AES128_CFB_Decrypt(uint8_t* InputMessage,
                              uint32_t InputMessageLength,
                              uint8_t  *AES128_Key,
                              uint8_t  *OutputMessage);

#endif /* _STM_CRYPTO_H_ */

/*

// Example

int32_t CRYPTO_temp(void)
{
  uint8_t  message[] = {' ', '�', '�', '�', '�', '�', '�', '�',
                        '�', ' ', '�', '�', '�', '�', '�', ' '};
  uint32_t size, modulus = 0xb80bcf1f;
  uint8_t shifrtext[256];
  
  RSA_Encrypt(message, sizeof(message), modulus,
                                shifrtext, &size);
      
  return size;
}*/