// ******************************************************************************

/* Includes ------------------------------------------------------------------*/

#include "GlobalHandler.h"
/**
  * Generation of new AES128 key.
  * Data:   personalization: pointer to input personalization string.
  *         AES128_Key:      input data message length in byte.
  * Return: error status: AES_SUCCESS - no errors
  *                       other - error occured.
  */

uint32_t Get_AES128_Key (uint8_t* Personalization, uint8_t  *AES128_Key)
{  
  int32_t error = AES_SUCCESS;
  RNGinitInput_stt RNGinit_st;
  RNGstate_stt RNGstate;
  uint8_t entropy_data[32];
  
  /* Entropy data generation */
  while (error < sizeof(entropy_data))
  {
    entropy_data[error] = (uint8_t)(rand() & 0xFF);
    ++ error;
  }
  
  /* Nonce. Non repeating sequence, such as a timestamp */
  uint8_t nonce[] = { (timeSystemUnix >> 48 & 0xFF),  ((timeSystemUnix >> 56) & 0xFF),
                      (timeSystemUnix & 0xFF),        ((timeSystemUnix >> 8) & 0xFF),
                      (timeSystemUnix >> 16 & 0xFF),  ((timeSystemUnix >> 24) & 0xFF),
                      (timeSystemUnix >> 32 & 0xFF),  ((timeSystemUnix >> 40) & 0xFF),
                    };
  
  /* Set the values of EntropyData, Nonce, Personalization String and their sizes inside the RNGinit_st structure */
  RNGinit_st.pmEntropyData = (uint8_t*) entropy_data;
  RNGinit_st.mEntropyDataSize = sizeof(entropy_data);
  RNGinit_st.pmNonce =  nonce;
  RNGinit_st.mNonceSize = sizeof( nonce );
  RNGinit_st.pmPersData = Personalization;
  RNGinit_st.mPersDataSize = sizeof( Personalization );
  
  /* The Random engine initialization ( status is in RNGstate ) */
  error = RNGinit(&RNGinit_st, &RNGstate);
  if(error == AES_SUCCESS)
  {
    /* Fill the random string with random bytes */
    error = RNGgenBytes(&RNGstate, NULL, AES128_Key, 16);
    if(error == AES_SUCCESS)
    {
      /* Free the state before returning */
      error = RNGfree(&RNGstate);
    }
  }
  
  return error;
}
 
/**
  * AES CFB Encryption.
  * Data:   InputMessage:         pointer to input message to be encrypted.
  *         InputMessageLength:   input data message length in byte.
  *         AES128_Key:           pointer to the 128-bit AES key to be used in the operation
  *         OutputMessage:        pointer to output message
  * Return: OutputMessageLength:  encrypted message length;  0 - error occured.
  */
int32_t AES128_CFB_Encrypt(uint8_t* InputMessage,
                           uint32_t InputMessageLength,
                           uint8_t  *AES128_Key,
                           uint8_t  *OutputMessage)
{
  AESCFBctx_stt AESctx;
  int32_t outputLength = 0, OutputMessageLength = 0; 
  uint8_t IV[18];

  /* Set flag field to default value */
  AESctx.mFlags = E_SK_DEFAULT;

  /* Set key size to 16 (corresponding to AES-128) */
  AESctx.mKeySize = 16;

  /* Set iv size field to IvLength (64 byte)*/
  AESctx.mIvSize = 16;
  sprintf((char*)IV, "SMT_Smart_GxCIK");
  /*
  for(uint8_t i = 0; i < 16; i++)
     IV[i] = i;
  IV[16] = 0;
  IV[17] = 0;
*/
  /* Initialize the operation, by passing the key.*/
  if (! AES_CFB_Encrypt_Init(&AESctx, AES128_Key, IV ))
  {
    /* Encrypt Data */ 
    if (! AES_CFB_Encrypt_Append(&AESctx,
                                          InputMessage,
                                          InputMessageLength,
                                          OutputMessage,
                                          &outputLength))
    {
      /* Write the number of data written*/
      OutputMessageLength = outputLength;
      /* Do the Finalization */
      if (! AES_CFB_Encrypt_Finish(&AESctx, OutputMessage + OutputMessageLength, &outputLength))
      {
        /* Add data written to the information to be returned */
        OutputMessageLength += outputLength;
      }
      else  OutputMessageLength = 0;
    }
  }

  return OutputMessageLength;
}


/**
  * AES CFB Decryption.
  * Data:   InputMessage:         pointer to input message to be decrypted.
  *         InputMessageLength:   input data message length in byte.
  *         AES128_Key:           pointer to the 128-bit AES key to be used in the operation
  *         OutputMessage:        pointer to output message
  * Return: OutputMessageLength:  decrypted message length;  0 - error occured.
  */

int32_t AES128_CFB_Decrypt(uint8_t* InputMessage,
                           uint32_t InputMessageLength,
                           uint8_t  *AES128_Key,
                           uint8_t  *OutputMessage)
{
  AESCFBctx_stt AESctx;
  int32_t outputLength = 0, OutputMessageLength = 0;
  uint8_t IV[18];

  /* Set flag field to default value */
  AESctx.mFlags = E_SK_DEFAULT;

  /* Set key size to 16 (corresponding to AES-128) */
  AESctx.mKeySize = 16;

  /* Set iv size field to IvLength*/
  AESctx.mIvSize = 16;
  sprintf((char*)IV, "SMT_Smart_GxCIK");
  /*
  for(uint8_t i = 0; i < 16; i++)
     IV[i] = i;
  IV[16] = 0;
  IV[17] = 0;
*/
  /* Initialize the operation, by passing the key. */
  if (! AES_CFB_Decrypt_Init(&AESctx, AES128_Key, IV))
  {
    /* Decrypt Data */
    if (! AES_CFB_Decrypt_Append(&AESctx,
                                          InputMessage,
                                          InputMessageLength,
                                          OutputMessage,
                                          &outputLength))
    {
      /* Write the number of data written*/
      OutputMessageLength = outputLength;
      /* Do the Finalization */
      if(! AES_CFB_Decrypt_Finish(&AESctx, OutputMessage + OutputMessageLength, &outputLength))
      {
        /* Add data written to the information to be returned */
        OutputMessageLength += outputLength;
      }
      else
        OutputMessageLength = 0;
    }
  }
  return OutputMessageLength;
}

/**
  * 1-3 bytes RSA Encryption.
  * Data:   Message: pointer to input message to be encrypted.
  *         Key:     pointer to the 32-bit RSA key to be used in the operation
  * Return: shifrtext: output message.
  */
uint32_t STM32_RSA_Encrypt(uint32_t Message, uint32_t Key)
{
  uint64_t shifrtext = Message;
  uint32_t index = 1;
    
  /* formula: shifrtext = Message^(PublicExponent) MOD Key */
  while (index < PublicExponent)
  {
    shifrtext = (shifrtext * Message);
    shifrtext = shifrtext % Key;
    ++ index;
  }
  if ((shifrtext >> 32) > 0) shifrtext = 0;
  
  return (shifrtext & 0xFFFFFFFF);
}

/**
  * RSA Encryption.
  * Data:   InputMessage:        pointer to input message to be encrypted.
  *         InputMessageLength:  input data message length in byte.
  *         RSA_Key:             pointer to the 128-bit AES key to be used in the operation
  *         OutputMessage:       pointer to output message
  *         OutputMessageLength: pointer to encrypted message length.
  * Return: error status: RSA_SUCCESS - no errors
  *                       other - error occured.
  */

uint32_t RSA_Encrypt(uint8_t* InputMessage, uint32_t InputMessageLength,
                     uint32_t RSA_Key, 
                     uint8_t* OutputMessage, uint32_t* OutputMessageLength)
{
  uint32_t * pointer;
  uint32_t block, position = 0,
           end_index = (InputMessageLength/3 + 1), index = 0;
  uint32_t error_status = RSA_SUCCESS;
  
  /* Checking of the Key value */
  if (RSA_Key < 16777215) 
    error_status = RSA_ERR_MODULUS_TOO_SHORT;
 // else if (RSA_Key > 4294967295)
//    error_status = RSA_ERR_BAD_KEY;
  else
  {
    pointer = (uint32_t*)InputMessage;
    
    /* if end_index > 512, then OutputMessageLength is too long */
    if (end_index > 512)
      error_status = RSA_ERR_MESSAGE_TOO_LONG;
    else if ((InputMessage == NULL) || (InputMessageLength == 0) || (OutputMessage == NULL))
      error_status = RSA_ERR_BAD_PARAMETER;
    else
    {
      while (index < end_index)
      {
        block = (*(pointer + position) << (8 * position)) +
                (*(pointer + position - 1) >> (32 - (8 * position)));
        if (index == (end_index - 1)) block = block & (0x00FFFFFF >> ((InputMessageLength - (index * 3) + 1) * 8));
        else                          block = block & 0x00FFFFFF;
      
        block = STM32_RSA_Encrypt(block, RSA_Key);
        if (block == 0) error_status = RSA_ERR_BAD_OPERATION;
        else            sprintf((char *)(OutputMessage + (index * 8)),"%08lX",block);  
        
        ++index;
        ++ position;
        
        if ((index % 4) == 0)
        {
          pointer = pointer + 3;
          position = 0;
        }
      }
      *OutputMessageLength = 8 * index;
    }
  }
    
  return error_status;
}
