//=============================================================================
/// \file    USART_Modem.c
/// \author  MNA
/// \date    23-Apr-2018
/// \brief   Module for SGM modem
//=============================================================================
// COPYRIGHT
//=============================================================================
#include "GlobalHandler.h"


#include <string.h>
#include <stdio.h>
#include <ctype.h>

//static LL_GPIO_InitTypeDef _port;

#define Mdm_RX_BUFFER_MAX_SIZE 512
#define Mdm_TX_BUFFER_MAX_SIZE 128

//char MdmTXBuffer[Mdm_TX_BUFFER_MAX_SIZE];
char MdmRXBuffer[Mdm_RX_BUFFER_MAX_SIZE];



#define GSM_PWR_PG()   READ_BIT(GPIOE->DI, GPIO_DI_6)

#define TRANSMIT_ENABLE 0xFF
#define TRANSMIT_DISABLE 0x0




uint16_t ModemTransmitEnable = TRANSMIT_DISABLE;
uint16_t MdmReceivedCounter = 0;
uint16_t MdmFlagDataReceived = 0;


//const char MdmImei[25] @".eeprom";
//const char MdmSimID [25] @".eeprom";



const float StdTemper = 293.15f;

/*
const char* mdm_cmd[]  = { "AT\r",
                           "AT+IPR=%u\n\r",
}; 

*/
uint8_t Mdm_IsRegistred(void);

//TMdm_Flags Mdm_Flags;

uint8_t GPRS_flag = 1;


void Mdm_Transmit2(uint8_t const *ptr_str)
{
  uint16_t dataLen;
  tick_count_message_from_server_msec = SystemUpTimeSecond; // xTaskGetTickCount()
  InitMessage();    

  TransmitDebugMessageOptic(ptr_str, 0);
  GSM_Update_tick_count_message_from_GSM_msec();
  /*
  while(ModemTransmitEnable == TRANSMIT_DISABLE)
  {
    ++i;
    if(i == 0xFFF)
      ModemTransmitEnable = TRANSMIT_ENABLE; // freeze protection
  }
  */
  Waiting_transfer_GSM(0xFFF);

 // dataLen = strlen((char*)MdmTXBuffer);
  dataLen = strlen((char*)ptr_str);
  if(dataLen)
  {
    CLEAR_BIT(DMA1_Channel7->CCR,DMA_CCR_EN);
    SET_BIT(DMA1->IFCR,DMA_IFCR_CTCIF7);  // ������ ���� �������� ������
    DMA1_Channel7->CNDTR = dataLen; // ����� �������� � ������� ���
    DMA1_Channel7->CMAR = (uint32_t)ptr_str;
    SET_BIT(DMA1_Channel7->CCR ,DMA_CCR_EN);
    ModemTransmitEnable =  TRANSMIT_DISABLE;
  }  
}  
// ==================== ����� =============================
void Mdm_DMA_RxReset(void)
{
// ��������� ����� DMA 5
DMA1_Channel6->CCR &= ~DMA_CCR_EN;
// ��������� DMA ��������� ������
// ����� �������� ������ ���������
DMA1_Channel6->CPAR = (uint32_t) &USART2->DR;
// ����� ������ ���������
DMA1_Channel6->CMAR = (uint32_t)ModemRxBuf;
// ����� ���� ��� �������� �� DMA
DMA1_Channel6->CNDTR = sizeof(ModemRxBuf);
// ������ ���� �������� ������
DMA1->IFCR |= DMA_IFCR_CTCIF6;
// ������ DMA
DMA1_Channel6->CCR |= DMA_CCR_EN;
}

//-----------------------------------------------------------------------------
void Waiting_transfer_GSM(uint16_t limit)
{
  uint16_t i=0;
  while(ModemTransmitEnable == TRANSMIT_DISABLE)
  {
    ++i;
    if(i == limit)
      ModemTransmitEnable = TRANSMIT_ENABLE; // freeze protection
  }
}



//-----------------------------------------------------------------------------
void  Mdm_HardwareInit(void)
{

  LL_GPIO_InitTypeDef _port = {0};
     // Enable the peripheral clock of GPIOC
  
    if (READ_BIT(RCC->AHBENR,RCC_AHBENR_GPIODEN)==0)
      SET_BIT(RCC->AHBENR,RCC_AHBENR_GPIODEN);
 //+++  if (READ_BIT(RCC->IOPENR, RCC_IOPENR_GPIOEEN)==0)
 //+++     SET_BIT(RCC->IOPENR, RCC_IOPENR_GPIOEEN);

        // GPIO configuration for UART signals 
        // Select AF mode (10) on PD3,4,5,6
    // RX TX
//    MODIFY_REG(GPIOD->MODER,/*GPIO_MODER_MODE3|GPIO_MODER_MODE4|*/GPIO_MODER_MODER5      //+++
//        |GPIO_MODER_MODER6, /*GPIO_MODER_MODE3_1|GPIO_MODER_MODE4_1|*/GPIO_MODER_MODER5_1 //+++
//        |GPIO_MODER_MODER6_1);

    _port.Pin = LL_GPIO_PIN_5 | LL_GPIO_PIN_6;
    _port.Mode = LL_GPIO_MODE_ALTERNATE ;
    _port.Speed = LL_GPIO_SPEED_FREQ_HIGH ;
    _port.OutputType =  LL_GPIO_OUTPUT_PUSHPULL;
    _port.Pull = LL_GPIO_PULL_NO;
    _port.Alternate = LL_GPIO_AF_7;
    LL_GPIO_Init(GPIOD, &_port);
    
    
     //  Power_EN, PWRKY
    _port.Pin = LL_GPIO_PIN_0 | LL_GPIO_PIN_1 | LL_GPIO_PIN_7;
    _port.Mode = LL_GPIO_MODE_OUTPUT;
    _port.Speed = LL_GPIO_SPEED_FREQ_LOW; //+++ LL_GPIO_SPEED_FREQ_HIGH
    _port.OutputType =  LL_GPIO_OUTPUT_PUSHPULL;
    _port.Pull = LL_GPIO_PULL_NO;
    LL_GPIO_Init(GPIOD, &_port);
    
    // Power Good
    _port.Pin = LL_GPIO_PIN_2;
    _port.Mode = LL_GPIO_MODE_INPUT;
    _port.Speed = LL_GPIO_SPEED_FREQ_LOW;//+++ LL_GPIO_SPEED_FREQ_HIGH
    _port.OutputType = LL_GPIO_OUTPUT_PUSHPULL;
    _port.Pull = LL_GPIO_PULL_NO;
    LL_GPIO_Init(GPIOD, &_port);
  /*
    // Enable the peripheral clock USART2
    SET_BIT(RCC->APB1ENR, RCC_APB1ENR_USART2EN);

    Mdm_USARTInit(SPEED_UART_DEFAULT);
    LL_USART_Disable(USART2);
    LL_USART_ClearFlag_TC (USART2); // Clear Transmit comp. interrapt flag
    
    // Enable DMA1 clock
   //+++ if ((RCC->AHBENR & RCC_AHBENR_DMA1EN) != RCC_AHBENR_DMA1EN)
    // RCC->AHBENR |=RCC_AHBENR_DMA1EN;
    EnableDisableDMA1(DMA1_ENABLE);
    
    // USART2_DMA_config
    DMA1_Channel4->CPAR = (uint32_t) &USART2->TDR;      //����� �������� ������ �����������
   // DMA1_Channel4->CMAR = (uint32_t)MdmTXBuffer;  //����� ������ �����������
   //����������� ����������� ��������:
   //������ �� ������, ��������� ��������� � ������
    DMA1_Channel4->CCR = DMA_CCR_DIR | DMA_CCR_MINC;
   //��������� ���������� �� ���������� ������:
   //����� 4
    DMA1_Channel4->CCR |= DMA_CCR_TCIE;

   //DMA remaped: Chanel4 - UART2_TX, Chanel5 - UART2_RX  
    DMA1_CSELR->CSELR |= 0x44000U;
   
    NVIC_EnableIRQ(DMA1_Channel4_5_6_7_IRQn);
    NVIC_SetPriority(DMA1_Channel4_5_6_7_IRQn,1);

    //������������� USART2 ��� ������ ����� DMA:
    //��������� �������� USART2 ����� DMA
    LL_USART_EnableDMAReq_TX(USART2);
    //   LL_USART_EnableDMAReq_RX(USART2);

    LL_USART_EnableIT_RXNE(USART2);
   // �������� ���������� � NVIC
    NVIC_EnableIRQ(USART2_IRQn);
    NVIC_SetPriority(USART2_IRQn,0);
    LL_USART_Enable(USART2);
    */
    
        RCC->AHBENR |=RCC_AHBENR_DMA1EN;
        SET_BIT(RCC->APB1ENR, RCC_APB1ENR_USART2EN);
//	DMA1_CSELR->CSELR = (DMA1_CSELR->CSELR & ~DMA_CSELR_C5S) | (4 << (4 * 4));
	// ����������� ����������� ��������:
	// ������ �� ��������� � ������, ��������� ��������� � ������
	DMA1_Channel6->CCR = DMA_CCR_MINC;
	Mdm_DMA_RxReset();
	//��������� ���������� �� ���������� ������:
	//����� 5
	DMA1_Channel6->CCR |= DMA_CCR_TCIE;
	// ��������� DMA ����������� ������
//	DMA1_CSELR->CSELR = (DMA1_CSELR->CSELR & ~DMA_CSELR_C4S) | (4 << (3 * 4));
	//����� �������� ������ �����������
	DMA1_Channel7->CPAR = (uint32_t) &USART2->DR;
	//����������� ����������� ��������:
	//������ �� ������, ��������� ��������� � ������
	DMA1_Channel7->CCR |= DMA_CCR_DIR | DMA_CCR_MINC;
	//��������� ���������� �� ���������� ������:
	//����� 4
	DMA1_Channel7->CCR |= DMA_CCR_TCIE;
	USART2->CR3 |= USART_CR3_DMAT | USART_CR3_DMAR; // ��������� �������� � ����� USART2 ����� DMA
	//��������� ���������� �� ������ 4 � 5 ������ DMA1:
	//HAL_NVIC_SetPriority(DMA1_Channel4_5_6_7_IRQn, 12, 0);
//	HAL_NVIC_EnableIRQ(DMA1_Channel4_5_6_7_IRQn);
        
        NVIC_EnableIRQ(DMA1_Channel6_IRQn);
        NVIC_EnableIRQ(DMA1_Channel7_IRQn);
       
        NVIC_SetPriority(DMA1_Channel6_IRQn,3);
        NVIC_SetPriority(DMA1_Channel7_IRQn,3);
	
        USART2->CR2 = 0;
	USART2->CR3 &= ~(USART_CR3_RTSE | USART_CR3_CTSE);
	//USART2->BRR=UART_BRR_SAMPLING16(HAL_RCC_GetPCLK1Freq(), Com->BaudRate);
	//USART2->BRR = (uint32_t)(__DIV_LPUART(HAL_RCC_GetSysClockFreq(), Com->BaudRate));
	//USART2->BRR = (uint32_t)(HAL_RCC_GetSysClockFreq()/SPEED_UART_DEFAULT);
        Mdm_USARTInit(SPEED_UART_DEFAULT);
	USART2->CR1 = USART_CR1_UE | USART_CR1_TE | USART_CR1_RE;
        
    ModemTransmitEnable = TRANSMIT_ENABLE;
} 

void Mdm_USARTInit(uint32_t BaudRate)
{
    LL_USART_InitTypeDef usart;
    
    LL_USART_Disable(USART2);    

    LL_USART_StructInit(&usart);
    usart.BaudRate = BaudRate; // 57600
    usart.DataWidth = LL_USART_DATAWIDTH_8B;
    usart.HardwareFlowControl = LL_USART_HWCONTROL_NONE;// LL_USART_HWCONTROL_RTS_CTS;
    usart.TransferDirection = LL_USART_DIRECTION_TX_RX;
    usart.Parity = LL_USART_PARITY_NONE;
    usart.StopBits = LL_USART_STOPBITS_1;
    LL_USART_Init(USART2, &usart);
    // [CNT_SPEED_UART] = {38400, 57600, 115200, 9600, 19200, 230400};
    switch(BaudRate)
    {
    default:
    case 38400:
         USART2->BRR = 0x01A1;
         break;
    case 57600:
         USART2->BRR = 0x0116;
         break;
    case 115200:
         USART2->BRR = 0x008B;
         break;
    case 9600:
         USART2->BRR = 0x0683;
         break;
    case 19200:
         USART2->BRR = 0x0341;
         break;
    case 230400:
         USART2->BRR = 0x0045;
         break;
    }
    
    LL_USART_Enable(USART2);
}

//-----------------------------------------------------------------------------
void USART2_IRQHandler(void)
{
  uint32_t lo_RChar;

  if(USART2->SR & USART_SR_RXNE)
  {
   //   lo_RChar = USART2->RDR; 
  }
  if(USART2->SR & USART_SR_ORE) //   Overrun error
  {
    // Reset error bit
    lo_RChar = USART2->SR;
    lo_RChar = USART2->DR;
//     SET_BIT(USART2->CR3,USART_CR3_OVRDIS); 
//     SET_BIT(USART2->ICR,USART_ICR_ORECF); 
     __no_operation();
  } 
/*  if(USART2->SR & USART_SR_CMF)  
  {
     __no_operation();
  } 
  USART2->ICR = 0xFFFFFFFFU;  */
}        

//-----------------------------------------------------------------------------

void Mdm_HardwareDeInit(void)
{
 // MCU_GSM_OFF();
 // GSM_PWR_DIS();
  LL_USART_Disable(USART2);
  LL_USART_DeInit(USART2);
    // Disable the peripheral clock USART2
  CLEAR_BIT(RCC->APB1ENR, RCC_APB1ENR_USART2EN);
   
  EnableDisableDMA1(DMA1_DISABLE);
} 

