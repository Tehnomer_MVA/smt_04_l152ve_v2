//=============================================================================
//  
//=============================================================================
/// \file    OptoComand.c
/// \author 
/// \date   
/// \brief   Comand optical interface
//=============================================================================
// COPYRIGHT
//=============================================================================
// EDIT_VOLUME_AND_ARH
//=============================================================================
#include "GlobalHandler.h"

TTempDataParsingCommand OPTIC_TempDataParsingCommand;
uint32_t tick_count_message_from_OPTIC_sec = 0;
extern xQueueHandle xARCQueue;
extern SemaphoreHandle_t xOptoArcRecReadySemaphore;
extern SemaphoreHandle_t xOpticRxSemaphore;

extern osMessageQId QueueParsingCommandHandle;


//extern uint64_t timeUnixGSMNextConnect; 
extern uint64_t timeSystemUnix;


extern uint8_t cnt_seans_TCP_mode_2;
//uint8_t R_ARC_Buf[ChangeArcTypeDefSIZE];
extern uint8_t flag_end_task;
extern uint32_t current_communication_gsm_number;

const char* lo_CRC_OK = "CRC OK";
const char* lo_CRC_ERR = "CRC ERROR";
uint16_t ReceivedCounter = 0;

void OPTO_ReadTask(void const * argument)
{
   portBASE_TYPE xStatus;
  
   while(1)
   {
      stack_OPTO_ReadTask = uxTaskGetStackHighWaterMark(NULL);
      xSemaphoreTake(xOpticRxSemaphore, portMAX_DELAY);
      flag_end_task |= 0x04;


      DataReceived = 0;
      ReceivedCounter =0;
      Opto_Uart_Transmit();
      if(while_TransmitEnable() == _OK)
      {
         if(OPTIC_TempDataParsingCommand.flag_end == _FALSE)
         {
            TypeParsingCommand elementParsingCommand;
            elementParsingCommand.ptr_commands = (uint8_t*)ReceivedBuffer;
            elementParsingCommand.source = SOURCE_OPTIC;
            elementParsingCommand.ptr_out = (uint8_t*)TransmitBuffer;
            elementParsingCommand.sem_source = &xOpticRxSemaphore;
            elementParsingCommand.ptr_temp_data = &OPTIC_TempDataParsingCommand;
            OPTIC_TempDataParsingCommand.size_bufer = TX_BUFFER_MAX_SIZE;
            OPTIC_TempDataParsingCommand.size_bufer_left = TX_BUFFER_MAX_SIZE;
            xStatus = xQueueSendToBack(QueueParsingCommandHandle, ( void * ) &elementParsingCommand, 1000);
            if( xStatus != pdPASS )
            {
              //### ������ �������� � �������! 
            }  
         }
      }

      flag_end_task &= ~0x04;
      //flag_ban_Receive_485 = 0;
   }   
}


//------------------------------------------------------------------------------
uint8_t while_TransmitEnable ()
{
   // while(TransmitEnable == TRANSMIT_DISABLE)
   for(uint32_t i = 0; (TransmitEnable == TRANSMIT_DISABLE) && (i < 0x000FFFFF); i++)
   {
     if(OptoPort==OPTO_PORT_DISABLE || i == 0x000FFFFE)
       return _FALSE;
      
   }
   
   return _OK;
}
  

uint32_t Get_tick_count_message_from_OPTIC_sec()
{
  return tick_count_message_from_OPTIC_sec;
}
