//=============================================================================
//  
//=============================================================================
/// \file    Uart.c
/// \author 
/// \date   
/// \brief  OPTICAL UART communication
//=============================================================================
// COPYRIGHT
//=============================================================================
// 
//=============================================================================
#include "GlobalHandler.h"


char TransmitBuffer[TX_BUFFER_MAX_SIZE];
//char TransmitBuffer_temp[TX_BUFFER_MAX_SIZE];
char ReceivedBuffer[RX_BUFFER_MAX_SIZE];
uint16_t _TransmitLength;
uint8_t  TransmitEnable = TRANSMIT_DISABLE; 
uint8_t  DataReceived = 0;




extern uint8_t flag_end_task;



//-----------------------------------------------------------------------------
 void Opto_Uart_Init(void)
 {  
  LL_USART_InitTypeDef lo_usart;
  LL_GPIO_InitTypeDef lo_port;
//  taskENTER_CRITICAL();
// Enable the peripheral clock of GPIOD
   if((RCC->AHBENR & RCC_AHBENR_GPIOCEN) == 0)
     RCC->AHBENR |= RCC_AHBENR_GPIOCEN;  
/*   if((RCC->AHBENR & RCC_AHBENR_GPIOAEN) == 0)
     RCC->AHBENR |= RCC_AHBENR_GPIOAEN;    */
// Enable the peripheral clock of USART3
   RCC->APB1ENR |= RCC_APB1ENR_USART3EN;
    
 // GPIO configuration for USART signals 
 // Select AF mode (AFIO7) on PC10 and PC11
   LL_GPIO_StructInit(&lo_port);
     // optic   
     lo_port.Pin =  LL_GPIO_PIN_10 | LL_GPIO_PIN_11;
     lo_port.Mode = LL_GPIO_MODE_ALTERNATE;
     lo_port.Speed = LL_GPIO_SPEED_FREQ_MEDIUM ;
     lo_port.Pull  = LL_GPIO_PULL_NO;
     lo_port.Alternate = LL_GPIO_AF_7;
     LL_GPIO_Init(GPIOC, &lo_port);  
    

// USART configuration
    LL_USART_Disable(USART3);    

    LL_USART_StructInit(&lo_usart);
    lo_usart.BaudRate = 9600; 
    lo_usart.DataWidth = LL_USART_DATAWIDTH_8B;
    lo_usart.HardwareFlowControl = LL_USART_HWCONTROL_NONE;// LL_USART_HWCONTROL_RTS_CTS;
    lo_usart.TransferDirection = LL_USART_DIRECTION_TX_RX;
    lo_usart.Parity = LL_USART_PARITY_NONE;
    lo_usart.StopBits = LL_USART_STOPBITS_1;
    LL_USART_Init(USART3, &lo_usart);

    LL_USART_DisableIT_CTS(USART3);   
    LL_USART_EnableIT_ERROR(USART3);
    LL_USART_EnableIT_IDLE(USART3);
    LL_USART_ConfigAsyncMode(USART3);
    LL_USART_EnableDMAReq_TX(USART3);
    LL_USART_EnableIT_RXNE(USART3);
    LL_USART_Disable(USART3);
    USART3->BRR = 0x00000683;
  
 // Enable DMA1 clock --------------------------------------------------------

    EnableDisableDMA1(DMA1_ENABLE);
   
   LL_DMA_SetDataTransferDirection(DMA1, LL_DMA_CHANNEL_2, LL_DMA_DIRECTION_MEMORY_TO_PERIPH);
   LL_DMA_SetChannelPriorityLevel(DMA1, LL_DMA_CHANNEL_2, LL_DMA_PRIORITY_LOW);
   LL_DMA_SetMode(DMA1, LL_DMA_CHANNEL_2, LL_DMA_MODE_NORMAL);
   LL_DMA_SetPeriphIncMode(DMA1, LL_DMA_CHANNEL_2, LL_DMA_PERIPH_NOINCREMENT);
   LL_DMA_SetMemoryIncMode(DMA1, LL_DMA_CHANNEL_2, LL_DMA_MEMORY_INCREMENT);
   LL_DMA_SetPeriphSize(DMA1, LL_DMA_CHANNEL_2, LL_DMA_PDATAALIGN_BYTE);
   LL_DMA_SetMemorySize(DMA1, LL_DMA_CHANNEL_2, LL_DMA_MDATAALIGN_BYTE);
   LL_DMA_SetPeriphAddress(DMA1, LL_DMA_CHANNEL_2,(uint32_t)&USART3->DR);
   LL_DMA_SetMemoryAddress(DMA1, LL_DMA_CHANNEL_2,(uint32_t)TransmitBuffer);
   LL_DMA_EnableIT_TC(DMA1,LL_DMA_CHANNEL_2);
   
  /* USART2 interrupt Init */
//   NVIC_SetPriority(USART3_IRQn, NVIC_EncodePriority(NVIC_GetPriorityGrouping(),3, 0));
   NVIC_SetPriority(USART3_IRQn,4);
   NVIC_EnableIRQ(USART3_IRQn);
   
   NVIC_SetPriority(DMA1_Channel2_IRQn,4);
   NVIC_EnableIRQ(DMA1_Channel2_IRQn);
   
  // if(READ_BIT(GPIOA->IDR, GPIO_IDR_IDR_15)==0)
  // {
      if(READ_BIT(GPIOC->IDR, GPIO_IDR_IDR_12)==0) 
      {
//         OPTO_PORT_ON();
         OptoPort = OPTO_PORT_ENABLE;   
         tick_count_message_from_OPTIC_sec = SystemUpTimeSecond;
         TransmitEnable =  TRANSMIT_ENABLE;
         LL_USART_Enable(USART3);
      }  
      else
      {
        OptoPort = OPTO_PORT_DISABLE; 
        flag_crypt_message[SOURCE_OPTIC] = _FALSE;
        kfactor_emulator = 65535;
        TransmitOpticTest = 0;
      }
}

//-----------------------------------------------------------------------------
void Opto_Uart_deInit(void)
{
  //Disable usart
  LL_USART_Disable(USART3);
 LL_USART_DeInit(USART3);
  //Power off usart
  MODIFY_REG(GPIOC->MODER, GPIO_MODER_MODER10, GPIO_MODER_MODER10);
  MODIFY_REG(GPIOC->MODER, GPIO_MODER_MODER11, GPIO_MODER_MODER11);
//  SET_BIT(GPIOB->ODR,GPIO_ODR_ODR_10);
  CLEAR_BIT(RCC->APB1ENR, RCC_APB1ENR_USART3EN);
  // Disable IRQ from usart
  NVIC_DisableIRQ(USART3_IRQn);  
  // Disable DMA  
  CLEAR_BIT(DMA1_Channel2->CCR,DMA_CCR_EN);

  // Clear buffer
//  memset(ReceivedBuffer,0,RX_BUFFER_MAX_SIZE);
  ReceivedCounter = 0;
  //Set system flag
  TransmitEnable =  TRANSMIT_DISABLE;
  OptoPort = OPTO_PORT_DISABLE;
  flag_crypt_message[SOURCE_OPTIC] = _FALSE;
  kfactor_emulator = 65535;
  TransmitOpticTest = 0;
  EnableDisableDMA1(DMA1_DISABLE);
  // Output from testmode
  ActivateTestMode(TestModeOFF); 
}  

//-----------------------------------------------------------------------------
void  Opto_Uart_Transmit(void)
{
  uint16_t dataLen;
  tick_count_message_from_OPTIC_sec = SystemUpTimeSecond;
  dataLen = strlen((char*)TransmitBuffer);
  //if(READ_BIT(GPIOA->IDR, GPIO_IDR_IDR_15))
  //  SET_BIT(GPIOC->ODR,GPIO_ODR_ODR_12);
  if(dataLen)
  {
    CLEAR_BIT(DMA1_Channel2->CCR,DMA_CCR_EN);
    SET_BIT(DMA1->IFCR,DMA_IFCR_CTCIF2);  // ������ ���� �������� ������
    DMA1_Channel2->CNDTR = dataLen; // ����� �������� � ������� ���

    // ������ ��������
    SET_BIT(DMA1_Channel2->CCR ,DMA_CCR_EN);
    TransmitEnable =  TRANSMIT_DISABLE;
  }  
}    


//-----------------------------------------------------------------------------
// ������ � ��������� ������� ����� ������
// � param �������� ����� ������ ���������� ��� �������
// 
//-----------------------------------------------------------------------------


   
//------------------------------------------------------------------------------   
void EnableDisableDMA1(uint8_t flag)
{
 // static uint8_t cntInput=0;
  if(flag == DMA1_ENABLE)
  {
  //   ++cntInput;
     RCC->AHBENR |=RCC_AHBENR_DMA1EN;
  }
  else
    
    // --cntInput;
  
 // if(!cntInput)
  {
    if (OptoPort == OPTO_PORT_DISABLE && GSM_Get_Status_power() == POWER_NON && GSM_Get_flag_power() == GSM_MUST_DISABLE) 
      RCC->AHBENR &= ~RCC_AHBENR_DMA1EN;
  }
}
   
        