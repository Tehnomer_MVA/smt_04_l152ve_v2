//=============================================================================
//  
//=============================================================================
/// \file    I2c.c
/// \author  SUL
/// \date    09-Dec-2014
/// \brief   Module for I2c-communication
//=============================================================================
// COPYRIGHT
//=============================================================================
//=============================================================================
/*
#include "I2c.h"
#include "Time_g.h"
#include "stm32l152xD.h"
*/
#include "GlobalHandler.h"
static void EnableGpios(void);

static void InitSclLine0(void);
static void InitSdaLine0(void);
/*
static void InitSclLine1(void);
static void InitSdaLine1(void);

static void InitSclLine2(void);
static void InitSdaLine2(void);

static void InitSclLine3(void);
static void InitSdaLine3(void);
*/
static int ReadSdaLine0(void);
static int ReadSclLine0(void);
static void SetSdaLine0Low(void);
static void SetSdaLine0High(void);
static void SetSclLine0Low(void);
static void SetSclLine0High(void);
/*
static int ReadSdaLine1(void);
static int ReadSclLine1(void);
static void SetSdaLine1Low(void);
static void SetSdaLine1High(void);
static void SetSclLine1Low(void);
static void SetSclLine1High(void);

static int ReadSdaLine2(void);
static int ReadSclLine2(void);
static inline void SetSdaLine2Low(void);
static inline void SetSdaLine2High(void);
static inline void SetSclLine2Low(void);
static inline void SetSclLine2High(void);

static int ReadSdaLine3(void);
static int ReadSclLine3(void);
static inline void SetSdaLine3Low(void);
static inline void SetSdaLine3High(void);
static inline void SetSclLine3Low(void);
static inline void SetSclLine3High(void);
*/
static Error CheckDataLine0High(void);
/*
static Error CheckDataLine1High(void);
static Error CheckDataLine2High(void);
static Error CheckDataLine3High(void);
*/
//#define  CONFIG_USE_CATEGORY_I2C
//#include "Configuration.h"

// This constant is used to correctly shape I2c-signals
static const uint32_t _i2cShapeCorrection = 1;
/// \brief Configuration of I2c-speed. Use on of Config_I2cSpeedType.
//const Config_I2cSpeedType Config_I2cSpeed = CONFIG_I2C_SPEED_30_KHZ;
const Config_I2cTSpeedType Config_I2cSpeed = CONFIG_I2C_SPEED_30_KHZ;
/* DEFINE SIGNAL LINE INTERFACE

   SDA0 - PE2
   SCL0 - PE3
   
   SDA1 - PE4
   SCL1 - PE5

   SDA2 - PE7
   SCL2 - PE8

   SDA3 - PE9
   SCL3 - PE10

   PE0  - Power ON/OFF sensors

*/

//----------------------------------------------------------------------------
inline void I2c_EnableLines0()
{
  SetSclLine0High();
  SetSdaLine0High();
}

//----------------------------------------------------------------------------
/*
inline void I2c_EnableLines1()
{
  SetSclLine1High();
  SetSdaLine1High();
}

//----------------------------------------------------------------------------
inline void I2c_EnableLines2()
{
  SetSclLine2High();
  SetSdaLine2High();
}

//----------------------------------------------------------------------------
inline void I2c_EnableLines3()
{
  SetSclLine3High();
  SetSdaLine3High();
}
*/
//----------------------------------------------------------------------------
inline void I2c_DisableLines0()
{
  SetSclLine0Low();
  SetSdaLine0Low();
}
/*
//----------------------------------------------------------------------------
inline void I2c_DisableLines1()
{
  SetSclLine1Low();
  SetSdaLine1Low();
}

//----------------------------------------------------------------------------
inline void I2c_DisableLines2()
{
  SetSclLine2Low();
  SetSdaLine2Low();
}

//----------------------------------------------------------------------------
inline void I2c_DisableLines3()
{
  SetSclLine3Low();
  SetSdaLine3Low();
}
*/

//----------------------------------------------------------------------------
void I2c_InitHardware(void)
{
  EnableGpios();
  
  InitSclLine0();
  InitSdaLine0();
  I2c_DisableLines0();
  /*
  InitSclLine1();
  InitSdaLine1();
  I2c_DisableLines1();

  InitSclLine2();
  InitSdaLine2();
  I2c_DisableLines2();

  InitSclLine3();
  InitSdaLine3();
  I2c_DisableLines3();
 */
}

//----------------------------------------------------------------------------
Error I2c_StartConditionCh0(void)
{
  Error error;
  
  SetSdaLine0High();
  SetSclLine0High();

  Time_DelayMicroSeconds(Config_I2cSpeed + _i2cShapeCorrection);
  error = CheckDataLine0High();
  if(Error_IsNone(error)) 
  {
    SetSdaLine0Low();
    Time_DelayMicroSeconds(Config_I2cSpeed + _i2cShapeCorrection);
    SetSclLine0Low();
    Time_DelayMicroSeconds(Config_I2cSpeed + _i2cShapeCorrection);
  }
  
  return error;
}
/*
//----------------------------------------------------------------------------
Error I2c_StartConditionCh1(void)
{
  Error error;
  
  SetSdaLine1High();
  SetSclLine1High();

  Time_DelayMicroSeconds(Config_I2cSpeed + _i2cShapeCorrection);
  error = CheckDataLine1High();
  if(Error_IsNone(error)) 
  {
    SetSdaLine1Low();
    Time_DelayMicroSeconds(Config_I2cSpeed + _i2cShapeCorrection);
    SetSclLine1Low();
    Time_DelayMicroSeconds(Config_I2cSpeed + _i2cShapeCorrection);
  }
  return error;
}

//----------------------------------------------------------------------------
Error I2c_StartConditionCh2(void)
{
  Error error;
  
  SetSdaLine2High();
  SetSclLine2High();

  Time_DelayMicroSeconds(Config_I2cSpeed + _i2cShapeCorrection);
  error = CheckDataLine2High();
  if(Error_IsNone(error)) 
  {
    SetSdaLine2Low();
    Time_DelayMicroSeconds(Config_I2cSpeed + _i2cShapeCorrection);
    SetSclLine2Low();
    Time_DelayMicroSeconds(Config_I2cSpeed + _i2cShapeCorrection);
  }
  return error;
}

//----------------------------------------------------------------------------
Error I2c_StartConditionCh3(void)
{
  Error error;
  
  SetSdaLine3High();
  SetSclLine3High();

  Time_DelayMicroSeconds(Config_I2cSpeed + _i2cShapeCorrection);
  error = CheckDataLine3High();
  if(Error_IsNone(error)) 
  {
    SetSdaLine3Low();
    Time_DelayMicroSeconds(Config_I2cSpeed + _i2cShapeCorrection);
    SetSclLine3Low();
    Time_DelayMicroSeconds(Config_I2cSpeed + _i2cShapeCorrection);
  }
  return error;
}
*/
//==============================================================================
void I2c_StopConditionCh0(void)
{
  SetSdaLine0Low();
  SetSclLine0Low();

  Time_DelayMicroSeconds(Config_I2cSpeed + _i2cShapeCorrection);
  SetSclLine0High();
  Time_DelayMicroSeconds(Config_I2cSpeed + _i2cShapeCorrection);
  SetSdaLine0High();
  Time_DelayMicroSeconds(Config_I2cSpeed + _i2cShapeCorrection);
}
/*
//----------------------------------------------------------------------------
void I2c_StopConditionCh1(void)
{
  SetSdaLine1Low();
  SetSclLine1Low();

  Time_DelayMicroSeconds(Config_I2cSpeed + _i2cShapeCorrection);
  SetSclLine1High();
  Time_DelayMicroSeconds(Config_I2cSpeed + _i2cShapeCorrection);
  SetSdaLine1High();
  Time_DelayMicroSeconds(Config_I2cSpeed + _i2cShapeCorrection);
}

//----------------------------------------------------------------------------
void I2c_StopConditionCh2(void)
{
  SetSdaLine2Low();
  SetSclLine2Low();

  Time_DelayMicroSeconds(Config_I2cSpeed + _i2cShapeCorrection);
  SetSclLine2High();
  Time_DelayMicroSeconds(Config_I2cSpeed + _i2cShapeCorrection);
  SetSdaLine2High();
  Time_DelayMicroSeconds(Config_I2cSpeed + _i2cShapeCorrection);
}

//----------------------------------------------------------------------------
void I2c_StopConditionCh3(void)
{
  SetSdaLine3Low();
  SetSclLine3Low();

  Time_DelayMicroSeconds(Config_I2cSpeed + _i2cShapeCorrection);
  SetSclLine3High();
  Time_DelayMicroSeconds(Config_I2cSpeed + _i2cShapeCorrection);
  SetSdaLine3High();
  Time_DelayMicroSeconds(Config_I2cSpeed + _i2cShapeCorrection);
}
*/
//==============================================================================

//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Error I2c_WriteByteCh0(uint8_t data)
{
  Error   error = Error_None();
  int     ack    = 0;
  uint8_t mask; // Shift-mask for data-bits
//while(ack == 0)  
{
  for(mask = 0x80; mask > 0; mask >>= 1) 
  {
    if(0 == (data & mask)) 
      SetSdaLine0Low();
     else 
      SetSdaLine0High();
   
    Time_DelayMicroSeconds(Config_I2cSpeed);
    SetSclLine0High();
    Time_DelayMicroSeconds(Config_I2cSpeed + _i2cShapeCorrection);
    SetSclLine0Low();
  }
  SetSdaLine0High();
  Time_DelayMicroSeconds(Config_I2cSpeed);
  SetSclLine0High();
  Time_DelayMicroSeconds(Config_I2cSpeed);
  ack = ReadSdaLine0();
  if(ack == 1) 
    Error_Add(&error, ERROR_I2C_ACK);
  SetSclLine0Low();
  Time_DelayMicroSeconds(Config_I2cSpeed);
}
  return error;
}
/*
//----------------------------------------------------------------------------
Error I2c_WriteByteCh1(uint8_t data)
{
  Error   error = Error_None();
  int     ack    = 0;
  uint8_t mask; // Shift-mask for data-bits
//while(ack == 0)  
{
  for(mask = 0x80; mask > 0; mask >>= 1) 
  {
    if(0 == (data & mask)) 
      SetSdaLine1Low();
     else 
      SetSdaLine1High();
   
    Time_DelayMicroSeconds(Config_I2cSpeed);
    SetSclLine1High();
    Time_DelayMicroSeconds(Config_I2cSpeed + _i2cShapeCorrection);
    SetSclLine1Low();
  }
  SetSdaLine1High();
  Time_DelayMicroSeconds(Config_I2cSpeed);
  SetSclLine1High();
  Time_DelayMicroSeconds(Config_I2cSpeed);
  ack = ReadSdaLine1();
  if(ack == 1) {
    Error_Add(&error, ERROR_I2C_ACK);
  }
  SetSclLine1Low();
  Time_DelayMicroSeconds(Config_I2cSpeed);
}
  return error;
}

//----------------------------------------------------------------------------
Error I2c_WriteByteCh2(uint8_t data)
{
  Error   error = Error_None();
  int     ack    = 0;
  uint8_t mask; // Shift-mask for data-bits
//while(ack == 0)  
{
  for(mask = 0x80; mask > 0; mask >>= 1) 
  {
    if(0 == (data & mask)) 
      SetSdaLine2Low();
     else 
      SetSdaLine2High();
   
    Time_DelayMicroSeconds(Config_I2cSpeed);
    SetSclLine2High();
    Time_DelayMicroSeconds(Config_I2cSpeed + _i2cShapeCorrection);
    SetSclLine2Low();
  }
  SetSdaLine2High();
  Time_DelayMicroSeconds(Config_I2cSpeed);
  SetSclLine2High();
  Time_DelayMicroSeconds(Config_I2cSpeed);
  ack = ReadSdaLine2();
  if(ack == 1) {
    Error_Add(&error, ERROR_I2C_ACK);
  }
  SetSclLine2Low();
  Time_DelayMicroSeconds(Config_I2cSpeed);
}
  return error;
}

//----------------------------------------------------------------------------
Error I2c_WriteByteCh3(uint8_t data)
{
  Error   error = Error_None();
  int     ack    = 0;
  uint8_t mask; // Shift-mask for data-bits
//while(ack == 0)  
  {
    for(mask = 0x80; mask > 0; mask >>= 1) 
    {
      if(0 == (data & mask)) 
        SetSdaLine3Low();
       else 
        SetSdaLine3High();
     
      Time_DelayMicroSeconds(Config_I2cSpeed);
      SetSclLine3High();
      Time_DelayMicroSeconds(Config_I2cSpeed + _i2cShapeCorrection);
      SetSclLine3Low();
    }
    SetSdaLine3High();
    Time_DelayMicroSeconds(Config_I2cSpeed);
    SetSclLine3High();
    Time_DelayMicroSeconds(Config_I2cSpeed);
    ack = ReadSdaLine3();
    if(ack == 1) {
      Error_Add(&error, ERROR_I2C_ACK);
    }
    SetSclLine3Low();
    Time_DelayMicroSeconds(Config_I2cSpeed);
  }
  return error;
}
*/
//==============================================================================
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
uint8_t I2c_ReadByteCh0(I2c_Ack ack)
{
  uint8_t data = 0;
  uint8_t mask; // Shift-mask for data-bits

  SetSdaLine0High();
  for (mask = 0x80; mask > 0; mask >>= 1) 
  {
    Time_DelayMicroSeconds(Config_I2cSpeed + _i2cShapeCorrection);
    SetSclLine0High();
    
    while(ReadSclLine0() == 0);
    
    Time_DelayMicroSeconds(Config_I2cSpeed);
    if(ReadSdaLine0() == 1)
       data = (data | mask);
    
    SetSclLine0Low();
  }
  if(ack == ACK) 
  
    SetSdaLine0Low();
  else 
    SetSdaLine0High();
  
  Time_DelayMicroSeconds(Config_I2cSpeed);
  SetSclLine0High();
  Time_DelayMicroSeconds(Config_I2cSpeed + _i2cShapeCorrection);
  SetSclLine0Low();
  SetSdaLine0High();
  Time_DelayMicroSeconds(Config_I2cSpeed);
  
  return data;
}
/*
//----------------------------------------------------------------------------
uint8_t I2c_ReadByteCh1(I2c_Ack ack)
{
  uint8_t data = 0;
  uint8_t mask; // Shift-mask for data-bits

  SetSdaLine1High();
  for (mask = 0x80; mask > 0; mask >>= 1)
  {
    Time_DelayMicroSeconds(Config_I2cSpeed + _i2cShapeCorrection);
    SetSclLine1High();
    
    while(ReadSclLine1() == 0);
    
    Time_DelayMicroSeconds(Config_I2cSpeed);
    if(ReadSdaLine1() == 1) 
      data = (data | mask);
   
    SetSclLine1Low();
  }
  if(ack == ACK) 
      SetSdaLine1Low();
   else 
    SetSdaLine1High();
  
  Time_DelayMicroSeconds(Config_I2cSpeed);
  SetSclLine1High();
  Time_DelayMicroSeconds(Config_I2cSpeed + _i2cShapeCorrection);
  SetSclLine1Low();
  SetSdaLine1High();
  Time_DelayMicroSeconds(Config_I2cSpeed);
  
  return data;
}
//----------------------------------------------------------------------------
uint8_t I2c_ReadByteCh2(I2c_Ack ack)
{
  uint8_t data = 0;
  uint8_t mask; // Shift-mask for data-bits

  SetSdaLine2High();
  for (mask = 0x80; mask > 0; mask >>= 1) 
  {
    Time_DelayMicroSeconds(Config_I2cSpeed + _i2cShapeCorrection);
    SetSclLine2High();
    
    while(ReadSclLine2() == 0);
    
    Time_DelayMicroSeconds(Config_I2cSpeed);
    if(ReadSdaLine2() == 1) 
     data = (data | mask);
    
    SetSclLine2Low();
  }
  if(ack == ACK)
    SetSdaLine2Low();
  else 
    SetSdaLine2High();
  
  Time_DelayMicroSeconds(Config_I2cSpeed);
  SetSclLine2High();
  Time_DelayMicroSeconds(Config_I2cSpeed + _i2cShapeCorrection);
  SetSclLine2Low();
  SetSdaLine2High();
  Time_DelayMicroSeconds(Config_I2cSpeed);
  
  return data;
}

//----------------------------------------------------------------------------
uint8_t I2c_ReadByteCh3(I2c_Ack ack)
{
  uint8_t data = 0;
  uint8_t mask; // Shift-mask for data-bits

  SetSdaLine3High();
  for (mask = 0x80; mask > 0; mask >>= 1) 
  {
    Time_DelayMicroSeconds(Config_I2cSpeed + _i2cShapeCorrection);
    SetSclLine3High();
    
    while(ReadSclLine3() == 0);
    
    Time_DelayMicroSeconds(Config_I2cSpeed);
    if(ReadSdaLine3() == 1) 
     data = (data | mask);
    
    SetSclLine3Low();
  }
  if(ack == ACK)
    SetSdaLine3Low();
  else 
    SetSdaLine3High();
  
  Time_DelayMicroSeconds(Config_I2cSpeed);
  SetSclLine3High();
  Time_DelayMicroSeconds(Config_I2cSpeed + _i2cShapeCorrection);
  SetSclLine3Low();
  SetSdaLine3High();
  Time_DelayMicroSeconds(Config_I2cSpeed);
  
  return data;
}
*/
//==============================================================================
static void EnableGpios(void)
{
  SET_BIT(RCC->AHBENR,RCC_AHBENR_GPIOEEN); 
//  SET_BIT(RCC->IOPENR,RCC_IOPENR_IOPBEN);
}

//----------------------------------------------------------------------------
static void InitSclLine0(void)
{
  // Scl-line is GPIO PB6
  // Configure as output with open drain, high speed, pull-up,
  // set output to 0
  MODIFY_REG(GPIOB->MODER, GPIO_MODER_MODER6, GPIO_MODER_MODER6_0);
  MODIFY_REG(GPIOB->OTYPER, GPIO_OTYPER_OT_6, GPIO_OTYPER_OT_6);
  MODIFY_REG(GPIOB->OSPEEDR, GPIO_OSPEEDER_OSPEEDR6,
      GPIO_OSPEEDER_OSPEEDR6_0 | GPIO_OSPEEDER_OSPEEDR6_1);
  SET_BIT(GPIOB->ODR, GPIO_ODR_ODR_6);
}
/*
//----------------------------------------------------------------------------
static void InitSclLine1(void)
{
  // Scl-line is GPIO PE5
  // Configure as output with open drain, high speed, pull-up,
  // set output to 0
  MODIFY_REG(GPIOE->MODER, GPIO_MODER_MODER5, GPIO_MODER_MODER5_0);
  MODIFY_REG(GPIOE->OTYPER, GPIO_OTYPER_OT_5, GPIO_OTYPER_OT_5);
  MODIFY_REG(GPIOE->OSPEEDR, GPIO_OSPEEDER_OSPEEDR5,
      GPIO_OSPEEDER_OSPEEDR5_0 | GPIO_OSPEEDER_OSPEEDR5_1);
  SET_BIT(GPIOE->ODR, GPIO_ODR_ODR_5);
}

//----------------------------------------------------------------------------
static void InitSclLine2(void)
{
  // Scl-line is GPIO PE8
  // Configure as output with open drain, high speed, pull-up,
  // set output to 0
  MODIFY_REG(GPIOE->MODER, GPIO_MODER_MODER8, GPIO_MODER_MODER8_0);
  MODIFY_REG(GPIOE->OTYPER, GPIO_OTYPER_OT_8, GPIO_OTYPER_OT_8);
  MODIFY_REG(GPIOE->OSPEEDR, GPIO_OSPEEDER_OSPEEDR8,
      GPIO_OSPEEDER_OSPEEDR8_0 | GPIO_OSPEEDER_OSPEEDR8_1);
  SET_BIT(GPIOE->ODR, GPIO_ODR_ODR_8);
}

//----------------------------------------------------------------------------
static void InitSclLine3(void)
{
  // Scl-line is GPIO PE10
  // Configure as output with open drain, high speed, pull-up,
  // set output to 0
  MODIFY_REG(GPIOE->MODER, GPIO_MODER_MODER10, GPIO_MODER_MODER10_0);
  MODIFY_REG(GPIOE->OTYPER, GPIO_OTYPER_OT_10, GPIO_OTYPER_OT_10);
  MODIFY_REG(GPIOE->OSPEEDR, GPIO_OSPEEDER_OSPEEDR3,
      GPIO_OSPEEDER_OSPEEDR10_0 | GPIO_OSPEEDER_OSPEEDR10_1);
  SET_BIT(GPIOE->ODR, GPIO_ODR_ODR_10);
}
//----------------------------------------------------------------------------
*/
//----------------------------------------------------------------------------
static void InitSdaLine0(void)
{
  // Scl-line is GPIO PB7
  // Configure as output with open drain, high speed, pull-up,
  // set output to 0
  MODIFY_REG(GPIOB->MODER, GPIO_MODER_MODER7, GPIO_MODER_MODER7_0);
  MODIFY_REG(GPIOB->OTYPER, GPIO_OTYPER_OT_7, GPIO_OTYPER_OT_7);
  MODIFY_REG(GPIOB->OSPEEDR, GPIO_OSPEEDER_OSPEEDR7,
      GPIO_OSPEEDER_OSPEEDR7_0 | GPIO_OSPEEDER_OSPEEDR7_1);
 // SET_BIT(GPIOB->PUPDR, GPIO_PUPDR_PUPD7_0);
  SET_BIT(GPIOB->ODR, GPIO_ODR_ODR_7);
}
/*
//----------------------------------------------------------------------------
static void InitSdaLine1(void)
{
  // Sda-line1 is GPIO PE4
  // Configure as output with open drain, high speed, pull-up,
  // set output to 0
  MODIFY_REG(GPIOE->MODER, GPIO_MODER_MODER4, GPIO_MODER_MODER4_0);
  MODIFY_REG(GPIOE->OTYPER, GPIO_OTYPER_OT_4, GPIO_OTYPER_OT_4);
  MODIFY_REG(GPIOE->OSPEEDR, GPIO_OSPEEDER_OSPEEDR4,
      GPIO_OSPEEDER_OSPEEDR4_0 | GPIO_OSPEEDER_OSPEEDR4_1);
  SET_BIT(GPIOE->ODR, GPIO_ODR_ODR_4);
}

//----------------------------------------------------------------------------
static void InitSdaLine2(void)
{
  // Sda-line2 is GPIO PE7
  // Configure as output with open drain, high speed, pull-up,
  // set output to 0
  MODIFY_REG(GPIOE->MODER, GPIO_MODER_MODER7, GPIO_MODER_MODER7_0);
  MODIFY_REG(GPIOE->OTYPER, GPIO_OTYPER_OT_7, GPIO_OTYPER_OT_7);
  MODIFY_REG(GPIOE->OSPEEDR, GPIO_OSPEEDER_OSPEEDR7,
      GPIO_OSPEEDER_OSPEEDR7_0 | GPIO_OSPEEDER_OSPEEDR7_1);
  SET_BIT(GPIOE->ODR, GPIO_ODR_ODR_7);
}

//----------------------------------------------------------------------------
static void InitSdaLine3(void)
{
  // Sda-line2 is GPIO PE9
  // Configure as output with open drain, high speed, pull-up,
  // set output to 0
  MODIFY_REG(GPIOE->MODER, GPIO_MODER_MODER9, GPIO_MODER_MODER9_0);
  MODIFY_REG(GPIOE->OTYPER, GPIO_OTYPER_OT_9, GPIO_OTYPER_OT_9);
  MODIFY_REG(GPIOE->OSPEEDR, GPIO_OSPEEDER_OSPEEDR9,
      GPIO_OSPEEDER_OSPEEDR9_0 | GPIO_OSPEEDER_OSPEEDR9_1);
  SET_BIT(GPIOE->ODR, GPIO_ODR_ODR_9);
}
//============================================================================
*/


//----------------------------------------------------------------------------
uint8_t I2c_MakeWriteHeader(uint8_t address)
{
  return (uint8_t)((address << 1 & ~I2C_RW_MASK) | I2C_WRITE);
}

//----------------------------------------------------------------------------
uint8_t I2c_MakeReadHeader(uint8_t address)
{
  return (uint8_t)((address << 1 & ~I2C_RW_MASK) | I2C_READ);
}


//=============================================================================
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
inline static int ReadSdaLine0(void)
{
  // Return bit 7 of GPIOB
  return READ_BIT(GPIOB->IDR, GPIO_IDR_IDR_7) ? 1 : 0;
}

//----------------------------------------------------------------------------
inline static int ReadSclLine0(void)
{
  // Return bit 6 of GPIOB
  return READ_BIT(GPIOB->IDR, GPIO_IDR_IDR_6) ? 1 : 0;
}

//----------------------------------------------------------------------------
inline static void SetSdaLine0Low(void)
{
  // Reset bit 7 of GPIOB
  CLEAR_BIT(GPIOB->ODR, GPIO_ODR_ODR_7);
}

//----------------------------------------------------------------------------
inline static void SetSdaLine0High(void)
{
  // Set bit 7 of GPIOB
  SET_BIT(GPIOB->ODR, GPIO_ODR_ODR_7);
}

//----------------------------------------------------------------------------
inline static void SetSclLine0Low(void)
{
  // Reset bit 6 of GPIOB
  CLEAR_BIT(GPIOB->ODR, GPIO_ODR_ODR_6);
}

//----------------------------------------------------------------------------
inline static void SetSclLine0High(void)
{
  // Set bit 6 of GPIOB
  SET_BIT(GPIOB->ODR, GPIO_ODR_ODR_6);
}

//----------------------------------------------------------------------------
static Error CheckDataLine0High(void)
{
  Error error = Error_None();
  
  if(1 != ReadSdaLine0()) 
      Error_Add(&error, ERROR_I2C_DATA_LINE_LOW);
  
  return error;
}
