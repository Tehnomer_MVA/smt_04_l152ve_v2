//=============================================================================
/// \file    Sgm.c
/// \author  RFU
/// \date    13-Apr-2016
/// \brief   Module for Sensirion Gasmeter Sensor
//=============================================================================
// COPYRIGHT
//=============================================================================
/*
#include "FreeRTOS.h"
#include "task.h"

#include "Sgm.h"
#include "I2c.h"
#include "stm32l152xD.h"
#include "Time_g.h"
#include "Utils.h"
#include "GasMeter.h"

#define CONFIG_USE_CATEGORY_SGM
#include "Configuration.h"
*/
#define CONFIG_USE_CATEGORY_SGM
#include "GlobalHandler.h"



typedef enum {
  MACRO_READID                 = 0x3624,
  MACRO_MEAS_FLOW              = 0x3603,
  MACRO_RECOGGAS               = 0x3608,
  MACRO_MEASFLOWLUTN           = 0x361E,
  MACRO_GETSFOFFSET            = 0x3624,
  MACRO_GETPRODUCTSERIALNUMBER = 0x362F,
  MACRO_GETARTICLECODE         = 0x3639,
  MACRO_SHORT_FLOW             = 0x3615,  
  MACRO_GET_VDD                = 0x3646
} Macro_Code;

/*
typedef enum {
  REGISTER_GPBUFFER            = 0xE102,
  REGISTER_STATUS_REG          = 0xEFFD
} Register_Address;
*/
typedef enum {
  REGISTER_DATABUFFER = 0xE000,
  REGISTER_GPBUFFER0  = 0xE102,
  REGISTER_GPBUFFER1  = 0xE109,
  REGISTER_GPBUFFER2  = 0xE114,
  REGISTER_GPBUFFER3  = 0xE11F,
  REGISTER_GPBUFFER4  = 0xE125,
  REGISTER_GPBUFFER5  = 0xE12E,
  REGISTER_GPBUFFER6  = 0xE133,
  REGISTER_GPBUFFER7  = 0xE138,
  REGISTER_GPBUFFER8  = 0xE147,
  REGISTER_GPBUFFER9  = 0xE14C,
  REGISTER_GPBUFFERA  = 0xE151,
  REGISTER_GPBUFFERB  = 0xE15A,
  REGISTER_GPBUFFERC  = 0xE160,
  REGISTER_GPBUFFERD  = 0xE16B,
  REGISTER_GPBUFFERE  = 0xE176,
  REGISTER_GPBUFFERF  = 0xE17D,
  REGISTER_PRAMSTATUS = 0xEFFD,
} Register_Address;

void SGM_PowerOn(void);
void SGM_PowerOff(void);

static void InitPowerGpio(void);


static Error RunMacroCh0(Macro_Code macro, uint16_t* argument,
                      uint16_t* ret0, uint16_t* ret1, uint16_t* ret2);
/*
static Error RunMacroCh1(Macro_Code macro, uint16_t* argument,
                      uint16_t* ret0, uint16_t* ret1, uint16_t* ret2);
static Error RunMacroCh2(Macro_Code macro, uint16_t* argument,
                      uint16_t* ret0, uint16_t* ret1, uint16_t* ret2);
static Error RunMacroCh3(Macro_Code macro, uint16_t* argument,
                      uint16_t* ret0, uint16_t* ret1, uint16_t* ret2);
*/
static Error WakeUpCh0(void);
/*static Error WakeUpCh1(void);
static Error WakeUpCh2(void);
static Error WakeUpCh3(void);
*/
static Error ReadGPBufferCh0(uint16_t* buffer, uint8_t size);
/*static Error ReadGPBufferCh1(uint16_t* buffer, uint8_t size);
static Error ReadGPBufferCh2(uint16_t* buffer, uint8_t size);
static Error ReadGPBufferCh3(uint16_t* buffer, uint8_t size);
*/
static Error SetPointerToAddressCh0(uint16_t address);
/*static Error SetPointerToAddressCh1(uint16_t address);
static Error SetPointerToAddressCh2(uint16_t address);
static Error SetPointerToAddressCh3(uint16_t address);
*/
static Error ReadMacroReturnValuesCh0(uint16_t* ret0, uint16_t* ret1, uint16_t* ret2);
/*static Error ReadMacroReturnValuesCh1(uint16_t* ret0, uint16_t* ret1, uint16_t* ret2);
static Error ReadMacroReturnValuesCh2(uint16_t* ret0, uint16_t* ret1, uint16_t* ret2);
static Error ReadMacroReturnValuesCh3(uint16_t* ret0, uint16_t* ret1, uint16_t* ret2);
*/
static Error ReadWordsWithCrcCheckCh0(uint16_t values[], uint8_t size);
/*static Error ReadWordsWithCrcCheckCh1(uint16_t values[], uint8_t size);
static Error ReadWordsWithCrcCheckCh2(uint16_t values[], uint8_t size);
static Error ReadWordsWithCrcCheckCh3(uint16_t values[], uint8_t size);
*/
static Error WriteWordCh0(uint16_t value);
/*static Error WriteWordCh1(uint16_t value);
static Error WriteWordCh2(uint16_t value);
static Error WriteWordCh3(uint16_t value);
*/
static Error WriteDataCh0(uint8_t data[], uint8_t size);
/*static Error WriteDataCh1(uint8_t data[], uint8_t size);
static Error WriteDataCh2(uint8_t data[], uint8_t size);
static Error WriteDataCh3(uint8_t data[], uint8_t size);
*/
static Error ReadWordCh0(uint16_t* value, bool finalizeWithNack);
/*static Error ReadWordCh1(uint16_t* value, bool finalizeWithNack);
static Error ReadWordCh2(uint16_t* value, bool finalizeWithNack);
static Error ReadWordCh3(uint16_t* value, bool finalizeWithNack);
*/
static void ReadDataCh0(uint8_t data[], uint8_t numberOfBytes, bool finalizeWithNack);
/*static void ReadDataCh1(uint8_t data[], uint8_t numberOfBytes, bool finalizeWithNack);
static void ReadDataCh2(uint8_t data[], uint8_t numberOfBytes, bool finalizeWithNack);
static void ReadDataCh3(uint8_t data[], uint8_t numberOfBytes, bool finalizeWithNack);
*/
static Error CheckCrc(uint8_t* data, uint8_t length, uint8_t checksum);

static Error ReadRegister1Ch0(Register_Address registerAddress, uint16_t* buffer,
                          uint8_t size);
/*
static Error ReadRegister1Ch1(Register_Address registerAddress, uint16_t* buffer,
                          uint8_t size);
static Error ReadRegister1Ch2(Register_Address registerAddress, uint16_t* buffer,
                          uint8_t size);
static Error ReadRegister1Ch3(Register_Address registerAddress, uint16_t* buffer,
                          uint8_t size);
*/

Sgm_Type _SgmType = SGM_TYPE_SGM4; // TODO

uint16_t manual_KL = 0;



//-----------------------------------------------------------------------------
void Sgm_InitHardware(void)
{
  InitPowerGpio();
}

//-----------------------------------------------------------------------------
Error Sgm_InitModule(void)
{
  SGM_PowerOff();
  Time_DelayMilliSeconds(150);
  SGM_PowerOn();
  Time_DelayMilliSeconds(100);
  
  return Error_None();
}


//-----------------------------------------------------------------------------
Error Sgm_DeinitModule(void)
{
  SGM_PowerOff();
 // osDelay(100);
 // Time_DelayMilliSeconds(100);
  return Error_None();
}

//-----------------------------------------------------------------------------
/*
Error Sgm_GetId(uint16_t* p_id0,uint16_t* p_id1,uint16_t* p_id2)
{
  Error error;
  error =  RunMacroCh0(MACRO_READID, 0, p_id0, 0, 0);
  error =  RunMacroCh1(MACRO_READID, 0, p_id1, 0, 0);
  error =  RunMacroCh2(MACRO_READID, 0, p_id2, 0, 0);
  error =  RunMacroCh3(MACRO_READID, 0, p_id3, 0, 0);
  return error;
}
*/
//==============================================================================

Error Sgm_MeasFlow(TSensor* pSensor)
{
  Error errorCh0,errorCh1,errorCh2,errorCh3;
  //uint16_t  lo_ShortK, lo_ShortQf,lo_ShortFineAdj;  
  
  errorCh0.Raw = 0;
  errorCh1.Raw = 0;
  errorCh2.Raw = 0;
  errorCh3.Raw = 0;
  
  GasMeter.SensError[0].Raw =0;
  GasMeter.SensError[1].Raw =0;
  GasMeter.SensError[2].Raw =0;
  GasMeter.SensError[3].Raw =0;
  
  if (GasMeter.SensEnable[0])
  {
     // Wake-up
     errorCh0 = WakeUpCh0();
     // Start condition
      if(Error_IsNone(errorCh0))
      {
       errorCh0 = I2c_StartConditionCh0();
       errorCh0 = I2c_WriteByteCh0(I2c_MakeWriteHeader(Config_SgmAddress));
       errorCh0 = SetPointerToAddressCh0(MACRO_MEAS_FLOW);
       errorCh0 = WriteWordCh0(pSensor->KL[0]);
       I2c_StopConditionCh0();
      }    
  }
   //------------------------------------------------
/*
  if(GasMeter.SensEnable[1])
  {
    errorCh1 = WakeUpCh1();
    if(Error_IsNone(errorCh1))  
      {
        errorCh1 = I2c_StartConditionCh1();
        errorCh1 = I2c_WriteByteCh1(I2c_MakeWriteHeader(Config_SgmAddress));
        errorCh1 = SetPointerToAddressCh1(MACRO_MEAS_FLOW);
        errorCh1 = WriteWordCh1(pSensor->KL[1]);
        I2c_StopConditionCh1();
      }  
  }
   //------------------------------------------------
  if(GasMeter.SensEnable[2])
  {
    errorCh2 = WakeUpCh2();
    if(Error_IsNone(errorCh2)) 
      {
        errorCh2 = I2c_StartConditionCh2();
        errorCh2 = I2c_WriteByteCh2(I2c_MakeWriteHeader(Config_SgmAddress));
        errorCh2 = SetPointerToAddressCh2(MACRO_MEAS_FLOW);
        errorCh2 = WriteWordCh2(pSensor->KL[2]);
        I2c_StopConditionCh2();
      }
  }

    //------------------------------------------------
  if(GasMeter.SensEnable[3])
   {
    errorCh3 = WakeUpCh3();
    if(Error_IsNone(errorCh3)) 
    {
      errorCh3 = I2c_StartConditionCh3();
      errorCh3 = I2c_WriteByteCh3(I2c_MakeWriteHeader(Config_SgmAddress));
      errorCh3 = SetPointerToAddressCh3(MACRO_MEAS_FLOW);
      errorCh3 = WriteWordCh3(pSensor->KL[3]);
      I2c_StopConditionCh3();
    }
  }  
*/
 // Wait for data in buffer !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!  
  Time_DelayMilliSeconds(18);  

  //Read data CH0 
  if(GasMeter.SensEnable[0] && Error_IsNone(errorCh0))
  {
      if(Error_IsNone(errorCh0))   
      {
       // Poll the Databuffer status register
        for(int i = 4; i > 0; i--) 
        {
          uint16_t pramStatus;
          errorCh0 = ReadRegister1Ch0(REGISTER_PRAMSTATUS, &pramStatus, 1);
          if(Error_IsNone(errorCh0) && (pramStatus != 0x0000)) 
            break;
          I2c_StopConditionCh0();
          Time_DelayMilliSeconds(1);
        }
        Error_AppendIfError(&errorCh0, ERROR_SGM_WAIT_FOR_PRAM_TIMEOUT);
      }
     
      // Read macro return values
      if(Error_IsNone(errorCh0))
      {
        uint16_t buffer[3];
        errorCh0 = ReadRegister1Ch0(REGISTER_DATABUFFER, buffer, 3);
        pSensor->Qfl[0] = buffer[0];    //Qf long
        //lo_ShortK  = buffer[1];    //K for short meassure
        pSensor->Qfl_FA[0] = buffer[2]; //QF  long fine adjustement
      }
      
        // 2 Read macro return values  
      if(Error_IsNone(errorCh0)) 
      {
        uint16_t buffer[3];
        errorCh0 = ReadRegister1Ch0(REGISTER_DATABUFFER, buffer, 3);
   //     lo_ShortQf = buffer[0];    //Qf short
   //     lo_ShortFineAdj     = buffer[1];    //Short fine adj 
        pSensor->Traw[0]    = buffer[2];    //Temper
      }
      I2c_StopConditionCh0();
  }
/*
  //Read Data CH1
 if(GasMeter.SensEnable[1] && Error_IsNone(errorCh1))
  {
    if(Error_IsNone(errorCh1))   
      {
       // Poll the Databuffer status register
        for(int i = 4; i > 0; i--) 
        {
          uint16_t pramStatus;
          errorCh1 = ReadRegister1Ch1(REGISTER_PRAMSTATUS, &pramStatus, 1);
          if(Error_IsNone(errorCh1) && (pramStatus != 0x0000)) 
            break;
          I2c_StopConditionCh1();
          Time_DelayMilliSeconds(1);
        }
        Error_AppendIfError(&errorCh1, ERROR_SGM_WAIT_FOR_PRAM_TIMEOUT);
      }

      // Read macro return values
      if(Error_IsNone(errorCh1))
      {
        uint16_t buffer[3];
        errorCh1 = ReadRegister1Ch1(REGISTER_DATABUFFER, buffer, 3);
        pSensor->Qfl[1] =  buffer[0];    //Qf long
   //     lo_ShortK  =  buffer[1];    //K for short meassure
        pSensor->Qfl_FA[1] = buffer[2]; //QF  long fine adjustement
      }
      
          // 2 Read macro return values  
      if(Error_IsNone(errorCh1)) 
      {
        uint16_t buffer[3];
        errorCh1 = ReadRegister1Ch1(REGISTER_DATABUFFER, buffer, 3);
    //    lo_ShortQf  = buffer[0];    //Qf short
    //    lo_ShortFineAdj  = buffer[1];    //Short fine adj 
        pSensor->Traw[1] = buffer[2];    //Temper
      }

    I2c_StopConditionCh1();
  }
  // Read DATA CH2
  if (GasMeter.SensEnable[2] && Error_IsNone(errorCh2))
  {  
      if(Error_IsNone(errorCh2))   
      {
       // Poll the Databuffer status register
        for(int i = 4; i > 0; i--) 
        {
          uint16_t pramStatus;
          errorCh2 = ReadRegister1Ch2(REGISTER_PRAMSTATUS, &pramStatus, 1);
          if(Error_IsNone(errorCh2) && (pramStatus != 0x0000)) 
            break;
          I2c_StopConditionCh2();
          Time_DelayMilliSeconds(1);
        }
        Error_AppendIfError(&errorCh2, ERROR_SGM_WAIT_FOR_PRAM_TIMEOUT);
      }
      
      // Read macro return values
      if(Error_IsNone(errorCh2))
      {
        uint16_t buffer[3];
        errorCh2 = ReadRegister1Ch2(REGISTER_DATABUFFER, buffer, 3);
        pSensor->Qfl[2] = buffer[0];    //Qf long
   //     lo_ShortK  = buffer[1];    //K for short meassure
        pSensor->Qfl_FA[2] = buffer[2]; //QF  long fine adjustement
      }
          // 2 Read macro return values  
      if(Error_IsNone(errorCh2)) 
      {
        uint16_t buffer[3];
        errorCh2 = ReadRegister1Ch2(REGISTER_DATABUFFER, buffer, 3);
  //      lo_ShortQf     = buffer[0];    //Qf short
  //      lo_ShortFineAdj     = buffer[1];    //Short fine adj 
        pSensor->Traw[2]    = buffer[2];    //Temper
      }

      I2c_StopConditionCh2();
  }
  
   // Read DATA CH3
  if (GasMeter.SensEnable[3] && Error_IsNone(errorCh3))
  {
      if(Error_IsNone(errorCh3))   
      {
       // Poll the Databuffer status register
        for(int i = 4; i > 0; i--) 
        {
          uint16_t pramStatus;
          errorCh3 = ReadRegister1Ch3(REGISTER_PRAMSTATUS, &pramStatus, 1);
          if(Error_IsNone(errorCh3) && (pramStatus != 0x0000)) 
            break;
          I2c_StopConditionCh3();
          Time_DelayMilliSeconds(1);
        }
        Error_AppendIfError(&errorCh3, ERROR_SGM_WAIT_FOR_PRAM_TIMEOUT);
      }
      
      // Read macro return values
      if(Error_IsNone(errorCh3))
      {
        uint16_t buffer[3];
        errorCh3 = ReadRegister1Ch3(REGISTER_DATABUFFER, buffer, 3);
        pSensor->Qfl[3] = buffer[0];    //Qf long
   //     lo_ShortK  = buffer[1];    //K for short meassure
        pSensor->Qfl_FA[3] = buffer[2]; //QF  long fine adjustement
      }
          // 2 Read macro return values  
      if(Error_IsNone(errorCh3)) 
      {
        uint16_t buffer[3];
        errorCh3 = ReadRegister1Ch3(REGISTER_DATABUFFER, buffer, 3);
  //      lo_ShortQf           = buffer[0];    //Qf short
  //      lo_ShortFineAdj     = buffer[1];    //Short fine adj 
        pSensor->Traw[3]    = buffer[2];    //Temper
      }

      I2c_StopConditionCh3();
  }
  */
    GasMeter.SensError[0].Raw = errorCh0.Raw;
    GasMeter.SensError[1].Raw = errorCh1.Raw;
    GasMeter.SensError[2].Raw = errorCh2.Raw;
    GasMeter.SensError[3].Raw = errorCh3.Raw;

  Error error;
    
    error.Raw = (errorCh0.Raw | errorCh1.Raw | errorCh2.Raw | errorCh3.Raw);
  return error;
}
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

//-----------------------------------------------------------------------------
Error Sgm_GasRecognition(uint16_t* p_GasRec0,uint16_t* p_GasRec1,uint16_t* p_GasRec2, uint16_t* p_GasRec3)
{
  Error error;

  if (GasMeter.SensEnable[0])
  {
    error = RunMacroCh0(MACRO_RECOGGAS, 0, p_GasRec0, 0, 0);
    GasMeter.SensError[0].Raw = error.Raw;
  } 
  /*
  if (GasMeter.SensEnable[1])
  {
    error = RunMacroCh1(MACRO_RECOGGAS, 0, p_GasRec1, 0, 0);
    GasMeter.SensError[1].Raw = error.Raw;
  } 
  if (GasMeter.SensEnable[2])
  {
    error = RunMacroCh2(MACRO_RECOGGAS, 0, p_GasRec2, 0, 0);
    GasMeter.SensError[2].Raw = error.Raw;
  } 
  if (GasMeter.SensEnable[3])
  {
    error = RunMacroCh3(MACRO_RECOGGAS, 0, p_GasRec3, 0, 0);
    GasMeter.SensError[3].Raw = error.Raw;
  } 
  */
  // if at least one module fails, then the error applies to all
  error.Raw = 0;
  return error;
}

//-----------------------------------------------------------------------------
Error Sgm_GetSf_Offset(uint16_t* p_scaleFactor, uint16_t* p_flowUnit, uint16_t* p_offset, uint8_t p_Chanel)
{
  Error error;
    switch (p_Chanel)
    {
    case 0:
         error = RunMacroCh0(MACRO_GETSFOFFSET, 0, p_offset, p_scaleFactor, p_flowUnit);
         break;
         /*
    case 1:
         error = RunMacroCh1(MACRO_GETSFOFFSET, 0, p_offset, p_scaleFactor, p_flowUnit);
         break;
    case 2:
         error = RunMacroCh2(MACRO_GETSFOFFSET, 0, p_offset, p_scaleFactor, p_flowUnit);
         break;
    case 3:
         error = RunMacroCh3(MACRO_GETSFOFFSET, 0, p_offset, p_scaleFactor, p_flowUnit);
         __no_operation();
         break;
         */
    default:break;
    }    
  return error;
}

//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Error Sgm_GetProductSerialNumberCh0(uint64_t* serialNumber)
{
  Error error; 
  uint16_t buffer[4];
    int i;
  
  error = RunMacroCh0(MACRO_GETPRODUCTSERIALNUMBER, 0, 0, 0, 0);  
  if(Error_IsNone(error))
      error = ReadGPBufferCh0((uint16_t*)buffer, 4);
    
  if(Error_IsNone(error))
  {
    *serialNumber = 0;
    for(i = 1; i < 3; i++) 
    {
      *serialNumber |= buffer[i];
      *serialNumber <<= 16;
    }
    *serialNumber |= buffer[i];
  }
  
  return error;
}

//-----------------------------------------------------------------------------
/*
Error Sgm_GetProductSerialNumberCh1(uint64_t* serialNumber)
{
  Error error; 
  uint16_t buffer[4];
    int i;
  
  error = RunMacroCh1(MACRO_GETPRODUCTSERIALNUMBER, 0, 0, 0, 0);  
  if(Error_IsNone(error))
    error = ReadGPBufferCh1((uint16_t*)buffer, 4);
  
  if(Error_IsNone(error))
  {
    *serialNumber = 0;
    for(i = 1; i < 3; i++) 
    {
      *serialNumber |= buffer[i];
      *serialNumber <<= 16;
    }
    *serialNumber |= buffer[i];
  }
  return error;
}

//-----------------------------------------------------------------------------
Error Sgm_GetProductSerialNumberCh2(uint64_t* serialNumber)
{
  Error error; 
  uint16_t buffer[4];
    int i;
  
  error = RunMacroCh2(MACRO_GETPRODUCTSERIALNUMBER, 0, 0, 0, 0);  
  if(Error_IsNone(error)) 
    error = ReadGPBufferCh2((uint16_t*)buffer, 4);
  
  if(Error_IsNone(error))
  {
    *serialNumber = 0;
    for(i = 1; i < 3; i++) 
    {
      *serialNumber |= buffer[i];
      *serialNumber <<= 16;
    }
    *serialNumber |= buffer[i];
  }
  
  return error;
}

//-----------------------------------------------------------------------------
Error Sgm_GetProductSerialNumberCh3(uint64_t* serialNumber)
{
  Error error; 
  uint16_t buffer[4];
    int i;
  
  error = RunMacroCh3(MACRO_GETPRODUCTSERIALNUMBER, 0, 0, 0, 0);  
  if(Error_IsNone(error)) 
    error = ReadGPBufferCh3((uint16_t*)buffer, 4);
   
  if(Error_IsNone(error))
  {
    *serialNumber = 0;
    for(i = 1; i < 3; i++) 
    {
      *serialNumber |= buffer[i];
      *serialNumber <<= 16;
    }
    *serialNumber |= buffer[i];
  }
  
  return error;
}
*/
//=============================================================================


//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Error Sgm_GetArticleCodeCh0(uint8_t* buffer)
{
  uint16_t localBuffer[10];
  Error error = RunMacroCh0(MACRO_GETARTICLECODE, 0, 0, 0, 0);
  
  if(Error_IsNone(error)) 
    error = ReadGPBufferCh0(localBuffer, 10);
  
  if(Error_IsNone(error)) {
    for(int i = 0; i < 10; i++) {
      buffer[i * 2]     = localBuffer[i] >> 8;
      buffer[i * 2 + 1] = localBuffer[i];
    }
  }
  return error;
}
//-----------------------------------------------------------------------------
/*
Error Sgm_GetArticleCodeCh1(uint8_t* buffer)
{
  uint16_t localBuffer[10];
  Error error = RunMacroCh1(MACRO_GETARTICLECODE, 0, 0, 0, 0);
  
  if(Error_IsNone(error)) 
    error = ReadGPBufferCh1(localBuffer, 10);
  
  if(Error_IsNone(error)) {
    for(int i = 0; i < 10; i++) {
      buffer[i * 2]     = localBuffer[i] >> 8;
      buffer[i * 2 + 1] = localBuffer[i];
    }
  }
  return error;
}
//-----------------------------------------------------------------------------
Error Sgm_GetArticleCodeCh2(uint8_t* buffer)
{
  uint16_t localBuffer[10];
  Error error = RunMacroCh2(MACRO_GETARTICLECODE, 0, 0, 0, 0);
  
  if(Error_IsNone(error)) 
    error = ReadGPBufferCh2(localBuffer, 10);
  
  if(Error_IsNone(error)) {
    for(int i = 0; i < 10; i++) {
      buffer[i * 2]     = localBuffer[i] >> 8;
      buffer[i * 2 + 1] = localBuffer[i];
    }
  }
  return error;
}

//-----------------------------------------------------------------------------
Error Sgm_GetArticleCodeCh3(uint8_t* buffer)
{
  uint16_t localBuffer[10];
  Error error = RunMacroCh3(MACRO_GETARTICLECODE, 0, 0, 0, 0);
  
  if(Error_IsNone(error)) 
    error = ReadGPBufferCh3(localBuffer, 10);
  
  if(Error_IsNone(error)) {
    for(int i = 0; i < 10; i++) {
      buffer[i * 2]     = localBuffer[i] >> 8;
      buffer[i * 2 + 1] = localBuffer[i];
    }
  }
  return error;
}
*/
//==============================================================================


/*/-----------------------------------------------------------------------------
SensorTypeParameters* Sgm_GetParameters(void)
{
  return &sensorTypeParameters[_SgmType];
}
*/

//-----------------------------------------------------------------------------
void SGM_PowerOn(void)
{
  CalculateMotoHourceSGM(MOTO_HOURCE_START);
  I2c_EnableLines0();
//  I2c_EnableLines1();
//  I2c_EnableLines2();
//  I2c_EnableLines3();
  CLEAR_BIT(GPIOC->ODR, GPIO_ODR_ODR_9);  //New HW
  Time_DelayMilliSeconds(2);
  
  
//  for(int i=0; i<0x250; i++)
//    __no_operation();
}

//-----------------------------------------------------------------------------
void SGM_PowerOff(void)
{
  I2c_DisableLines0();
/*  I2c_DisableLines1();
  I2c_DisableLines2();
  I2c_DisableLines3();*/
  SET_BIT(GPIOC->ODR, GPIO_ODR_ODR_9);  //New HW
  I2c_EnableLines0();
 /* I2c_EnableLines1();
  I2c_EnableLines2();
  I2c_EnableLines3();   */
//  I2c_EnableLines();
  CalculateMotoHourceSGM(MOTO_HOURCE_STOP);
}

//-----------------------------------------------------------------------------
static void InitPowerGpio(void)
{
  // power switch is GPIO PC9
  // configure as output with push-pull
  // set output to 0
// New HW 
  MODIFY_REG(GPIOC->MODER, GPIO_MODER_MODER9, GPIO_MODER_MODER9_0);
  SET_BIT(GPIOC->OTYPER,GPIO_OTYPER_OT_9);
  SET_BIT(GPIOC->ODR, GPIO_ODR_ODR_9);
}

//=============================================================================
static Error RunMacroCh0(Macro_Code macro, uint16_t* argument,
                      uint16_t* ret0, uint16_t* ret1, uint16_t* ret2)
{ 
  // Wake-up
  Error error = WakeUpCh0();
  
  // Start condition
  if(Error_IsNone(error))
      error = I2c_StartConditionCh0();
  
  
  // Send write header
  if(Error_IsNone(error)) 
      error = I2c_WriteByteCh0(I2c_MakeWriteHeader(Config_SgmAddress));
    
  // Write the macro code
  if(Error_IsNone(error))
    error = SetPointerToAddressCh0(macro);
  

  // Write the macro argument
  if(Error_IsNone(error))
  {
    if(argument)
      error = WriteWordCh0(*argument);
  }
  // Read return values
  if(Error_IsNone(error)) {
    for(int i = 8; i > 0; i--) 
    {
      Time_DelayMilliSeconds(5);
      error = ReadMacroReturnValuesCh0(ret0, ret1, ret2);
      if(Error_IsNone(error)) 
        break;
    }
  }
  
  I2c_StopConditionCh0();
  
  return error;
}
/*
//-----------------------------------------------------------------------------
static Error RunMacroCh1(Macro_Code macro, uint16_t* argument,
                      uint16_t* ret0, uint16_t* ret1, uint16_t* ret2)
{ 
  // Wake-up
  Error error = WakeUpCh1();
  
  // Start condition
  if(Error_IsNone(error))
      error = I2c_StartConditionCh1();
  
  
  // Send write header
  if(Error_IsNone(error)) 
      error = I2c_WriteByteCh1(I2c_MakeWriteHeader(Config_SgmAddress));
    
  // Write the macro code
  if(Error_IsNone(error))
    error = SetPointerToAddressCh1(macro);
  

  // Write the macro argument
  if(Error_IsNone(error))
  {
    if(argument)
      error = WriteWordCh1(*argument);
  }
  // Read return values
  if(Error_IsNone(error)) {
    for(int i = 8; i > 0; i--) 
    {
      Time_DelayMilliSeconds(5);
      error = ReadMacroReturnValuesCh1(ret0, ret1, ret2);
      if(Error_IsNone(error)) 
        break;
    }
  }
  
  I2c_StopConditionCh1();
  
  return error;
}

//-----------------------------------------------------------------------------
static Error RunMacroCh2(Macro_Code macro, uint16_t* argument,
                      uint16_t* ret0, uint16_t* ret1, uint16_t* ret2)
{ 
  // Wake-up
  Error error = WakeUpCh2();
  
  // Start condition
  if(Error_IsNone(error))
      error = I2c_StartConditionCh2();
  
  
  // Send write header
  if(Error_IsNone(error)) 
      error = I2c_WriteByteCh2(I2c_MakeWriteHeader(Config_SgmAddress));
    
  // Write the macro code
  if(Error_IsNone(error))
    error = SetPointerToAddressCh2(macro);
  

  // Write the macro argument
  if(Error_IsNone(error))
  {
    if(argument)
      error = WriteWordCh2(*argument);
  }
  // Read return values
  if(Error_IsNone(error)) {
    for(int i = 8; i > 0; i--) 
    {
      Time_DelayMilliSeconds(5);
      error = ReadMacroReturnValuesCh2(ret0, ret1, ret2);
      if(Error_IsNone(error)) 
        break;
    }
  }
  
  I2c_StopConditionCh2();
  
  return error;
}

//-----------------------------------------------------------------------------
static Error RunMacroCh3(Macro_Code macro, uint16_t* argument,
                      uint16_t* ret0, uint16_t* ret1, uint16_t* ret2)
{ 
  // Wake-up
  Error error = WakeUpCh3();
  
  // Start condition
  if(Error_IsNone(error))
      error = I2c_StartConditionCh3();
  
  
  // Send write header
  if(Error_IsNone(error)) 
      error = I2c_WriteByteCh3(I2c_MakeWriteHeader(Config_SgmAddress));
    
  // Write the macro code
  if(Error_IsNone(error))
    error = SetPointerToAddressCh3(macro);
  

  // Write the macro argument
  if(Error_IsNone(error))
  {
    if(argument)
      error = WriteWordCh3(*argument);
  }
  // Read return values
  if(Error_IsNone(error)) {
    for(int i = 8; i > 0; i--) 
    {
      Time_DelayMilliSeconds(5);
      error = ReadMacroReturnValuesCh3(ret0, ret1, ret2);
      if(Error_IsNone(error)) 
        break;
    }
  }
  
  I2c_StopConditionCh3();
  
  return error;
}

//==============================================================================
*/
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
static Error ReadRegister1Ch0(Register_Address registerAddress, uint16_t* buffer,
                          uint8_t size)
{
  // Start condition
  Error error = I2c_StartConditionCh0();

  // Send write header
  if(Error_IsNone(error))
    error = I2c_WriteByteCh0(I2c_MakeWriteHeader(Config_SgmAddress));

  // Set pointer to Address
  if(Error_IsNone(error)) 
    error = SetPointerToAddressCh0(registerAddress);

  // Start condition
  error = I2c_StartConditionCh0();

  // Send read header
  if(Error_IsNone(error)) 
    error = I2c_WriteByteCh0(I2c_MakeReadHeader(Config_SgmAddress));

  if(Error_IsNone(error))
      error = ReadWordsWithCrcCheckCh0(buffer, size);
  
  I2c_StopConditionCh0();
  return error;
}
/*
//------------------------------------------------------------------------------
static Error ReadRegister1Ch1(Register_Address registerAddress, uint16_t* buffer,
                          uint8_t size)
{
  // Start condition
  Error error = I2c_StartConditionCh1();

  // Send write header
  if(Error_IsNone(error))
    error = I2c_WriteByteCh1(I2c_MakeWriteHeader(Config_SgmAddress));

  // Set pointer to Address
  if(Error_IsNone(error)) 
    error = SetPointerToAddressCh1(registerAddress);

  // Start condition
  error = I2c_StartConditionCh1();

  // Send read header
  if(Error_IsNone(error)) 
    error = I2c_WriteByteCh1(I2c_MakeReadHeader(Config_SgmAddress));

  if(Error_IsNone(error))
      error = ReadWordsWithCrcCheckCh1(buffer, size);
  
  I2c_StopConditionCh1();
  return error;
}
//------------------------------------------------------------------------------
static Error ReadRegister1Ch2(Register_Address registerAddress, uint16_t* buffer,
                          uint8_t size)
{
  // Start condition
  Error error = I2c_StartConditionCh2();

  // Send write header
  if(Error_IsNone(error))
    error = I2c_WriteByteCh2(I2c_MakeWriteHeader(Config_SgmAddress));

  // Set pointer to Address
  if(Error_IsNone(error)) 
    error = SetPointerToAddressCh2(registerAddress);

  // Start condition
  error = I2c_StartConditionCh2();

  // Send read header
  if(Error_IsNone(error)) 
    error = I2c_WriteByteCh2(I2c_MakeReadHeader(Config_SgmAddress));

  if(Error_IsNone(error))
      error = ReadWordsWithCrcCheckCh2(buffer, size);
  
  I2c_StopConditionCh2();
  return error;
}

//------------------------------------------------------------------------------
static Error ReadRegister1Ch3(Register_Address registerAddress, uint16_t* buffer,
                          uint8_t size)
{
  // Start condition
  Error error = I2c_StartConditionCh3();

  // Send write header
  if(Error_IsNone(error))
    error = I2c_WriteByteCh3(I2c_MakeWriteHeader(Config_SgmAddress));

  // Set pointer to Address
  if(Error_IsNone(error)) 
    error = SetPointerToAddressCh3(registerAddress);

  // Start condition
  error = I2c_StartConditionCh3();

  // Send read header
  if(Error_IsNone(error)) 
    error = I2c_WriteByteCh3(I2c_MakeReadHeader(Config_SgmAddress));

  if(Error_IsNone(error))
      error = ReadWordsWithCrcCheckCh3(buffer, size);
  
  I2c_StopConditionCh3();
  return error;
}
//=============================================================================
*/
//-----------------------------------------------------------------------------
static Error WakeUpCh0(void)
{
  Error error;
  
  error = I2c_StartConditionCh0();
  I2c_WriteByteCh0(I2c_MakeWriteHeader(Config_SgmAddress)); 
  I2c_StopConditionCh0();

  if(Error_IsNone(error)) 
      Time_DelayMicroSeconds(500);
  return error;
}
/*
static Error WakeUpCh1(void)
{
  Error error;
  error = I2c_StartConditionCh1();
  I2c_WriteByteCh1(I2c_MakeWriteHeader(Config_SgmAddress));
  I2c_StopConditionCh1();

  if(Error_IsNone(error)) 
      Time_DelayMicroSeconds(500);
    
  return error;
}

//-----------------------------------------------------------------------------
static Error WakeUpCh2(void)
{
  Error error;

  error = I2c_StartConditionCh2();
  I2c_WriteByteCh2(I2c_MakeWriteHeader(Config_SgmAddress));
  I2c_StopConditionCh2();

  if(Error_IsNone(error)) 
      Time_DelayMicroSeconds(500);
    
  return error;
}

//-----------------------------------------------------------------------------
static Error WakeUpCh3(void)
{
  Error error;

  error = I2c_StartConditionCh3();
  I2c_WriteByteCh3(I2c_MakeWriteHeader(Config_SgmAddress));
  I2c_StopConditionCh3();

  if(Error_IsNone(error)) 
      Time_DelayMicroSeconds(500);
  return error;
}
*/
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
static Error ReadGPBufferCh0(uint16_t* buffer, uint8_t size)
{
  // Start condition
  Error error = I2c_StartConditionCh0();
  
  // Send write header
  if(Error_IsNone(error)) 
    error = I2c_WriteByteCh0(I2c_MakeWriteHeader(Config_SgmAddress));

  // Set pointer to GPBuffer
  if(Error_IsNone(error)) 
    error = SetPointerToAddressCh0(REGISTER_GPBUFFER0);
  
  // Start condition
  error = I2c_StartConditionCh0();
  
  // Send read header
  if(Error_IsNone(error)) 
    error = I2c_WriteByteCh0(I2c_MakeReadHeader(Config_SgmAddress));
  
  if(Error_IsNone(error))
      error = ReadWordsWithCrcCheckCh0(buffer, size);
  
  I2c_StopConditionCh0();
  
  return error;
}
/*
//-----------------------------------------------------------------------------
static Error ReadGPBufferCh1(uint16_t* buffer, uint8_t size)
{
  // Start condition
  Error error = I2c_StartConditionCh1();
  
  // Send write header
  if(Error_IsNone(error)) 
    error = I2c_WriteByteCh1(I2c_MakeWriteHeader(Config_SgmAddress));
  
  // Set pointer to GPBuffer
  if(Error_IsNone(error)) 
    error = SetPointerToAddressCh1(REGISTER_GPBUFFER0);
  
  // Start condition
  error = I2c_StartConditionCh1();
  
  // Send read header
  if(Error_IsNone(error)) 
    error = I2c_WriteByteCh1(I2c_MakeReadHeader(Config_SgmAddress));
  
  if(Error_IsNone(error))
      error = ReadWordsWithCrcCheckCh1(buffer, size);
  
  I2c_StopConditionCh1();
  
  return error;
}
//-----------------------------------------------------------------------------
static Error ReadGPBufferCh2(uint16_t* buffer, uint8_t size)
{
  // Start condition
  Error error = I2c_StartConditionCh2();
  
  // Send write header
  if(Error_IsNone(error)) 
    error = I2c_WriteByteCh2(I2c_MakeWriteHeader(Config_SgmAddress));
  
  // Set pointer to GPBuffer
  if(Error_IsNone(error)) 
    error = SetPointerToAddressCh2(REGISTER_GPBUFFER0);
  
  // Start condition
  error = I2c_StartConditionCh2();
  
  // Send read header
  if(Error_IsNone(error)) 
    error = I2c_WriteByteCh2(I2c_MakeReadHeader(Config_SgmAddress));
  
  if(Error_IsNone(error))
      error = ReadWordsWithCrcCheckCh2(buffer, size);
  I2c_StopConditionCh2();
  
  return error;
}

//-----------------------------------------------------------------------------
static Error ReadGPBufferCh3(uint16_t* buffer, uint8_t size)
{
  // Start condition
  Error error = I2c_StartConditionCh3();
  
  // Send write header
  if(Error_IsNone(error)) 
    error = I2c_WriteByteCh3(I2c_MakeWriteHeader(Config_SgmAddress));
  
  // Set pointer to GPBuffer
  if(Error_IsNone(error)) 
    error = SetPointerToAddressCh3(REGISTER_GPBUFFER0);
  
  // Start condition
  error = I2c_StartConditionCh3();
  
  // Send read header
  if(Error_IsNone(error)) 
    error = I2c_WriteByteCh3(I2c_MakeReadHeader(Config_SgmAddress));
  
  if(Error_IsNone(error))
      error = ReadWordsWithCrcCheckCh3(buffer, size);
  I2c_StopConditionCh3();
  
  return error;
}
//=============================================================================

*/
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
static Error SetPointerToAddressCh0(uint16_t address)
{
  Error error;
  uint8_t data[2];
  
  data[0] = address >> 8;
  data[1] = address & 0xFF;
  error = WriteDataCh0(data, sizeof(data));
  
  return error;
}
/*
//-----------------------------------------------------------------------------
static Error SetPointerToAddressCh1(uint16_t address)
{
  Error error;
  uint8_t data[2];
  
  data[0] = address >> 8;
  data[1] = address & 0xFF;
  error = WriteDataCh1(data, sizeof(data));
  
  return error;
}
//-----------------------------------------------------------------------------
static Error SetPointerToAddressCh2(uint16_t address)
{
  Error error;
  uint8_t data[2];
  
  data[0] = address >> 8;
  data[1] = address & 0xFF;
  error = WriteDataCh2(data, sizeof(data));
  
  return error;
}

//-----------------------------------------------------------------------------
static Error SetPointerToAddressCh3(uint16_t address)
{
  Error error;
  uint8_t data[2];
  
  data[0] = address >> 8;
  data[1] = address & 0xFF;
  error = WriteDataCh3(data, sizeof(data));
  
  return error;
}
//=============================================================================
*/
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
static Error ReadMacroReturnValuesCh0(uint16_t* ret0, uint16_t* ret1, uint16_t* ret2)
{
  Error error = Error_None();
  
  uint8_t numOfReturnValues = ret2 ? 3 : (ret1 ? 2 : (ret0 ? 1 : 0));
  uint16_t values[3];

  // Start to read the macro return values, if any expected.
  if(numOfReturnValues > 0)
  {
    // Start condition
    error = I2c_StartConditionCh0();
    
    // Send read header
    if(Error_IsNone(error)) 
      error = I2c_WriteByteCh0(I2c_MakeReadHeader(Config_SgmAddress));
    

    // Read the macro return values
    if(Error_IsNone(error)) 
      error = ReadWordsWithCrcCheckCh0(values, numOfReturnValues);
   
    if(Error_IsNone(error)) 
    {
      *ret0 = values[0];
      *ret1 = values[1];
      *ret2 = values[2];
    }
  }
  
  return error;
}
/*

//-----------------------------------------------------------------------------
static Error ReadMacroReturnValuesCh1(uint16_t* ret0, uint16_t* ret1, uint16_t* ret2)
{
  Error error = Error_None();
  
  uint8_t numOfReturnValues = ret2 ? 3 : (ret1 ? 2 : (ret0 ? 1 : 0));
  uint16_t values[3];

  // Start to read the macro return values, if any expected.
  if(numOfReturnValues > 0)
  {
    // Start condition
    error = I2c_StartConditionCh1();
    
    // Send read header
    if(Error_IsNone(error)) 
      error = I2c_WriteByteCh1(I2c_MakeReadHeader(Config_SgmAddress));

    // Read the macro return values
    if(Error_IsNone(error)) 
      error = ReadWordsWithCrcCheckCh1(values, numOfReturnValues);
   
    if(Error_IsNone(error)) 
    {
      *ret0 = values[0];
      *ret1 = values[1];
      *ret2 = values[2];
    }
  }
  return error;
}

//-----------------------------------------------------------------------------
static Error ReadMacroReturnValuesCh2(uint16_t* ret0, uint16_t* ret1, uint16_t* ret2)
{
  Error error = Error_None();
  
  uint8_t numOfReturnValues = ret2 ? 3 : (ret1 ? 2 : (ret0 ? 1 : 0));
  uint16_t values[3];

  // Start to read the macro return values, if any expected.
  if(numOfReturnValues > 0)
  {
    // Start condition
    error = I2c_StartConditionCh2();
    
    // Send read header
    if(Error_IsNone(error)) 
      error = I2c_WriteByteCh2(I2c_MakeReadHeader(Config_SgmAddress));

    // Read the macro return values
    if(Error_IsNone(error)) 
      error = ReadWordsWithCrcCheckCh2(values, numOfReturnValues);
   
    if(Error_IsNone(error)) 
    {
      *ret0 = values[0];
      *ret1 = values[1];
      *ret2 = values[2];
    }
  }
  return error;
}

//-----------------------------------------------------------------------------
static Error ReadMacroReturnValuesCh3(uint16_t* ret0, uint16_t* ret1, uint16_t* ret2)
{
  Error error = Error_None();
  
  uint8_t numOfReturnValues = ret2 ? 3 : (ret1 ? 2 : (ret0 ? 1 : 0));
  uint16_t values[3];

  // Start to read the macro return values, if any expected.
  if(numOfReturnValues > 0)
  {
    // Start condition
    error = I2c_StartConditionCh3();
    
    // Send read header
    if(Error_IsNone(error)) 
      error = I2c_WriteByteCh3(I2c_MakeReadHeader(Config_SgmAddress));

    // Read the macro return values
    if(Error_IsNone(error)) 
      error = ReadWordsWithCrcCheckCh3(values, numOfReturnValues);
   
    if(Error_IsNone(error)) 
    {
      *ret0 = values[0];
      *ret1 = values[1];
      *ret2 = values[2];
    }
  }
  return error;
}
//=============================================================================
*/

//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
static Error ReadWordsWithCrcCheckCh0(uint16_t values[], uint8_t size)
{
  Error error;
  uint8_t i;
  bool finalizeRead;

  for(i = 0; i < size; i++)
  {
    finalizeRead = (i == (size - 1));
    error = ReadWordCh0(&values[i], finalizeRead);
    if(Error_IsError(error))
    {
      // If the reading has not been finalized with a NACK, do that now.
      if(finalizeRead) 
        I2c_ReadByteCh0(NO_ACK);
      break;
    }
  }
  return error;
}
/*
//-----------------------------------------------------------------------------
static Error ReadWordsWithCrcCheckCh1(uint16_t values[], uint8_t size)
{
  Error error;
  uint8_t i;
  bool finalizeRead;

  for(i = 0; i < size; i++) {
    finalizeRead = (i == (size - 1));
    error = ReadWordCh1(&values[i], finalizeRead);
    if(Error_IsError(error)) 
    {
      // If the reading has not been finalized with a NACK, do that now.
      if(finalizeRead) I2c_ReadByteCh1(NO_ACK);
      break;
    }
  }
  return error;
}
//-----------------------------------------------------------------------------
static Error ReadWordsWithCrcCheckCh2(uint16_t values[], uint8_t size)
{
  Error error;
  uint8_t i;
  bool finalizeRead;

  for(i = 0; i < size; i++) {
    finalizeRead = (i == (size - 1));
    error = ReadWordCh2(&values[i], finalizeRead);
    if(Error_IsError(error))
    {
      // If the reading has not been finalized with a NACK, do that now.
      if(finalizeRead) I2c_ReadByteCh2(NO_ACK);
      break;
    }
  }
  return error;
}
//-----------------------------------------------------------------------------
static Error ReadWordsWithCrcCheckCh3(uint16_t values[], uint8_t size)
{
  Error error;
  uint8_t i;
  bool finalizeRead;

  for(i = 0; i < size; i++) {
    finalizeRead = (i == (size - 1));
    error = ReadWordCh3(&values[i], finalizeRead);
    if(Error_IsError(error)) 
    {
      // If the reading has not been finalized with a NACK, do that now.
      if(finalizeRead) 
        I2c_ReadByteCh3(NO_ACK);
      break;
    }
  }
  return error;
}
//=============================================================================
*/

//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
static Error WriteWordCh0(uint16_t value)
{
  Error error;
  uint8_t data[3];
  
  data[0] = value >> 8;
  data[1] = value & 0xFF;
  data[2] = Utils_CalculateCrc(data, 2, Config_SgmCrcPolynomial, Config_SgmCrcStartVector);
  error = WriteDataCh0(data, sizeof(data));
  
  return error;
}
/*
//-----------------------------------------------------------------------------
static Error WriteWordCh1(uint16_t value)
{
  Error error;
  uint8_t data[3];
  
  data[0] = value >> 8;
  data[1] = value & 0xFF;
  data[2] = Utils_CalculateCrc(data, 2, Config_SgmCrcPolynomial, Config_SgmCrcStartVector);
  error = WriteDataCh1(data, sizeof(data));
  
  return error;
}
//-----------------------------------------------------------------------------
static Error WriteWordCh2(uint16_t value)
{
  Error error;
  uint8_t data[3];
  
  data[0] = value >> 8;
  data[1] = value & 0xFF;
  data[2] = Utils_CalculateCrc(data, 2, Config_SgmCrcPolynomial, Config_SgmCrcStartVector);
  error = WriteDataCh2(data, sizeof(data));
  
  return error;
}
//-----------------------------------------------------------------------------
static Error WriteWordCh3(uint16_t value)
{
  Error error;
  uint8_t data[3];
  
  data[0] = value >> 8;
  data[1] = value & 0xFF;
  data[2] = Utils_CalculateCrc(data, 2, Config_SgmCrcPolynomial, Config_SgmCrcStartVector);
  error = WriteDataCh3(data, sizeof(data));
  
  return error;
}
//=============================================================================
*/

//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
static Error WriteDataCh0(uint8_t data[], uint8_t size)
{
  Error error = Error_None();
  uint8_t i;
  
  for(i = 0; i < size; i++) 
  {
    error = I2c_WriteByteCh0(data[i]);
    if(Error_IsError(error)) 
      break;
  }
  
  return error;
}
/*
//-----------------------------------------------------------------------------
static Error WriteDataCh1(uint8_t data[], uint8_t size)
{
  Error error = Error_None();
  uint8_t i;
  
  for(i = 0; i < size; i++) 
  {
    error = I2c_WriteByteCh1(data[i]);
    if(Error_IsError(error)) 
      break;
  }
  
  return error;
}
//-----------------------------------------------------------------------------
static Error WriteDataCh2(uint8_t data[], uint8_t size)
{
  Error error = Error_None();
  uint8_t i;
  
  for(i = 0; i < size; i++) 
  {
    error = I2c_WriteByteCh2(data[i]);
    if(Error_IsError(error)) 
      break;
  }
  
  return error;
}

//-----------------------------------------------------------------------------
static Error WriteDataCh3(uint8_t data[], uint8_t size)
{
  Error error = Error_None();
  uint8_t i;
  
  for(i = 0; i < size; i++) 
  {
    error = I2c_WriteByteCh3(data[i]);
    if(Error_IsError(error)) 
      break;
  }
  
  return error;
}
//=============================================================================
*/
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
static Error ReadWordCh0(uint16_t* value, bool finalizeWithNack)
{
  Error error;
  uint8_t data[3];
  
  ReadDataCh0(data, 3, finalizeWithNack);
  error = CheckCrc(data, 2 , data[2]);
  
  if(Error_IsNone(error)) 
     *value = data[0] << 8 | data[1];
  
  return error;
}
/*
//-----------------------------------------------------------------------------
static Error ReadWordCh1(uint16_t* value, bool finalizeWithNack)
{
  Error error;
  uint8_t data[3];
  
  ReadDataCh1(data, 3, finalizeWithNack);
  error = CheckCrc(data, 2 , data[2]);
  
  if(Error_IsNone(error)) 
    *value = data[0] << 8 | data[1];
  
  return error;
}
//-----------------------------------------------------------------------------
static Error ReadWordCh2(uint16_t* value, bool finalizeWithNack)
{
  Error error;
  uint8_t data[3];
  
  ReadDataCh2(data, 3, finalizeWithNack);
  error = CheckCrc(data, 2 , data[2]);
  
  if(Error_IsNone(error)) 
    *value = data[0] << 8 | data[1];
  
  return error;
}

//-----------------------------------------------------------------------------
static Error ReadWordCh3(uint16_t* value, bool finalizeWithNack)
{
  Error error;
  uint8_t data[3];
  
  ReadDataCh3(data, 3, finalizeWithNack);
  error = CheckCrc(data, 2 , data[2]);
  
  if(Error_IsNone(error)) 
     *value = data[0] << 8 | data[1];
  
  return error;
}
*/

//=============================================================================
static void ReadDataCh0(uint8_t data[], uint8_t numberOfBytes, bool finalizeWithNack)
{
  uint8_t i;
  I2c_Ack ack = ACK;
  
  for(i = 0; i < numberOfBytes; i++) 
  {
    if((i == (numberOfBytes - 1)) && finalizeWithNack)
        ack = NO_ACK;
     data[i] = I2c_ReadByteCh0(ack);
  }
}
/*
//-----------------------------------------------------------------------------
static void ReadDataCh1(uint8_t data[], uint8_t numberOfBytes, bool finalizeWithNack)
{
  uint8_t i;
  I2c_Ack ack = ACK;
  
  for(i = 0; i < numberOfBytes; i++) 
  {
    if((i == (numberOfBytes - 1)) && finalizeWithNack)
        ack = NO_ACK;
     data[i] = I2c_ReadByteCh1(ack);
  }
}

//-----------------------------------------------------------------------------
static void ReadDataCh2(uint8_t data[], uint8_t numberOfBytes, bool finalizeWithNack)
{
  uint8_t i;
  I2c_Ack ack = ACK;
  
  for(i = 0; i < numberOfBytes; i++) 
  {
    if((i == (numberOfBytes - 1)) && finalizeWithNack)
        ack = NO_ACK;
     data[i] = I2c_ReadByteCh2(ack);
  }
}

//-----------------------------------------------------------------------------
static void ReadDataCh3(uint8_t data[], uint8_t numberOfBytes, bool finalizeWithNack)
{
  uint8_t i;
  I2c_Ack ack = ACK;
  
  for(i = 0; i < numberOfBytes; i++) 
  {
    if((i == (numberOfBytes - 1)) && finalizeWithNack)
        ack = NO_ACK;
     data[i] = I2c_ReadByteCh3(ack);
  }
}
//==============================================================================

*/
//-----------------------------------------------------------------------------
static Error CheckCrc(uint8_t* data, uint8_t length, uint8_t checksum)
{
  Error   error = Error_None();
  uint8_t crc   = 0;

  crc = Utils_CalculateCrc(data, length, Config_SgmCrcPolynomial, Config_SgmCrcStartVector);
  if (crc != checksum) {
    Error_Add(&error, ERROR_SGM_CRC);
  }
  return error;
}



