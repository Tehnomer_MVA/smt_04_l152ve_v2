//=============================================================================
/// \file    FlowMeasurement.c
/// \author  SUL
/// \date    08-Dec-2014
/// \brief   Module for Flow-Measurement
//=============================================================================
// COPYRIGHT
//=============================================================================

#include "GlobalHandler.h"



static Error ReadFlowMeasurementConstants(void);
//static float ZeroingTinyFlow(float flow);

//extern bool EEprom_Lock;


TSensor Sensor @0x20000070;
TSens_Const Sensor_Const @0x200000A0;

struct TypeFlowMeasurement Flow @0x20000170 = 
      {0,        // Sensor_Qfl
      0,         // lo_QgLm3
      0,         // lo_mesCount
      0,         // dQf_l
      0.0,       // lo_LastSensQf
      0.1,       // lo_KalmanK
      0,         // lo_KalmanFlag
      0,         // lo_AddToFlow
      0,         // lo_LastMeas
      220,       // KalmanBorder
      0,         // lo_LUT_Sum
      0         // lo_TRaw_midl
      };



float _Short2LongFactor = 1.0f;
uint8_t reverse_flow=0;
uint8_t FlagBadSensor=0;
uint32_t cnt_reverse_flow_off=0;
uint32_t cnt_reverse_flow=0;


//int Sensor_Qfl;
double GasMeter_QgL_Disp = 0.0;


const TQf_param Qf_param[] @0x08080000 ={
//  min    max     dQf      ctg 
{0,0, 262141, 0.0, 0.0f},
{1,10123, 10252, 0.0f, 0.0f},
{2,10252, 10617, 0.0f, 0.0f},
{3,10617, 11567, 0.0f, 0.0f},
{4,11567, 13127, 0.0f, 0.0f},
{5,13127, 16269, 0.0f, 0.0f},
{6,16269, 20136, 0.0f, 0.0f},
{7,20136, 22647, 0.0f, 0.0f},
{8,22647, 28858, 0.0f, 0.0f},
{9,28858, 34916, 0.0f, 0.0f},
{10,34916, 40933, 0.0f, 0.0f},
{11,40933, 40950, 0.0f, 0.0f},
{12,40950, 41000, 0.0f, 0.0f},
{13,41000, 41500, 0.0f, 0.0f},
{14,41500, 42000, 0.0f, 0.0f},
{15,42000, 42500, 0.0f, 0.0f},
{16,42500, 43000, 0.0f, 0.0f},
{17,43000, 43500, 0.0f, 0.0f},
{18,43500, 44000, 0.0f, 0.0f},
{19,44000, 44500, 0.0f, 0.0f},
{20,44500, 45000, 0.0f, 0.0f},
{21,45000, 45500, 0.0f, 0.0f},
{22,45500, 46000, 0.0f, 0.0f},
{23,46000, 46500, 0.0f, 0.0f},
{24,46500, 47000, 0.0f, 0.0f},
{25,47000, 47500, 0.0f, 0.0f},
{26,47500, 48000, 0.0f, 0.0f},
{27,48000, 48500, 0.0f, 0.0f},
{28,48500, 49000, 0.0f, 0.0f},
{29,49000, 49500, 0.0f, 0.0f},
{30,49500, 50000, 0.0f, 0.0f},
{31,50000, 50500, 0.0f, 0.0f},
{32,50500, 51000, 0.0f, 0.0f},
{33,51000, 51500, 0.0f, 0.0f},
{34,51500, 52000, 0.0f, 0.0f},
{35,52000, 52500, 0.0f, 0.0f},
{36,52500, 53000, 0.0f, 0.0f},
{37,53000, 53500, 0.0f, 0.0f},
{38,53500, 54000, 0.0f, 0.0f},
{39,54000, 65535, 0.0f, 0.0f}
};

const uint16_t Qf_count = sizeof(Qf_param)/sizeof(Qf_param[0]);
const uint16_t Qf_sizeof = sizeof(Qf_param);
//const uint16_t QposLimit @".eeprom" = 10030;
//const uint16_t QnegLimit @".eeprom" = 9800;

// -- Normal Mode (default) ---------------------------------------------------
/*
/// \brief FUllFlow measurement interval in seconds.
const uint16_t Config_GasmeterMeasurementIntervalNormalModeFM @".eeprom" = 2;
/// \brief SHORTFlow measurement interval in seconds.
//const uint16_t Config_GasmeterMeasurementIntervalNormalModeSM @".eeprom" = 2;
/// \brief Gas recognition interval in seconds.
const uint16_t Config_GasmeterGasRecognitionIntervalNormalMode @".eeprom" =  21600; // 6h
const uint16_t Config_GasmeterGasRecognitionIntervalMeasureMode @".eeprom" =  2; // 
const uint16_t Config_GasmeterGasRecognitionIntervalErrorMode @".eeprom" =  10800; // 
/// \brief Gas recognition retry interval on failer in seconds.
const uint16_t Config_GasmeterGasRecognitionRetryIntervalNormalMode @".eeprom" = 600; // 10 �����
/// \brief Time until low battery symbol appears in seconds.
//#define Config_GasmeterTimeUntilLowBatterySymbolNormalMode  141912000 // 4.5y
/// \brief Buckup interval in seconds.
//#define Config_GasmeterBackupIntervalNormalMode 1800 // 30min

// -- Test Mode ---------------------------------------------------------------

/// \brief Flow measurement fast interval in seconds.
const uint16_t Config_GasmeterMeasurementIntervalTestModeFM @".eeprom" = 1;
/// \brief Gas recognition interval in seconds.
const uint16_t Config_GasmeterGasRecognitionIntervalTestMode @".eeprom" = 300;
*/


//uint16_t KalmanBorder = 220;
//float lo_KalmanK = 0.1 ;
//uint8_t lo_KalmanFlag = 0;
// ----------------------------------------------------------------------------
Error FlowMeas_InitModule(void)
{
  Error error = Error_None();
 
  error = ReadFlowMeasurementConstants();  // ��������� �� ������ !!!
  return error;
}

//----------------------------------------------------------------------------
double CalcSensOffset(int Sensor_Qf_summ)
{
  double dQf = 0;
   int lo_Qfa = 0;
  
  lo_Qfa = Sensor_Qf_summ;
  
  if (lo_Qfa >= 0)
  for( unsigned int count_ckl = 0; count_ckl < Qf_count; count_ckl++)
  { 
    if ((lo_Qfa > Qf_param[count_ckl].min_Border_Qf) && (lo_Qfa <= Qf_param[count_ckl].max_Border_Qf))
    { 
      if(count_ckl == 0)
      {
        dQf = (double)(Qf_param[count_ckl].max_Border_Qf - lo_Qfa);
        dQf  *= Qf_param[count_ckl].ctg_dQf;
        dQf  = Qf_param[count_ckl].offset_dQf - dQf;
        break;
      }         
      else  
      { 
        dQf = (double)(lo_Qfa - Qf_param[count_ckl].min_Border_Qf);
        dQf *= Qf_param[count_ckl].ctg_dQf;
        dQf += Qf_param[count_ckl].offset_dQf;
        break;
      } 
    }  
       
  }
 return dQf;
 } 

// ----------------------------------------------------------------------------
void FlowMeas_MeasureFlow(void)
{
   uint8_t lo_Cnt;  
  
//  double dQf_l;
//  static double lo_QgLm3;
 // static uint32_t lo_mesCount;
   Flow.lo_LUT_Sum = 0, 
   Flow.lo_TRaw_midl = 0;

//  static float lo_LastSensQf = 0.0f;
  
//  static uint8_t  lo_AddToFlow = 0;
//  static uint32_t lo_LastMeas = 0;
  
  Error error = Error_None();
   
  error = Sgm_MeasFlow(&Sensor);

  Sensor.Qf_summ = 0;
  FlagBadSensor = 0;

  // ��������� � ��������� ������ � ��������
  for (lo_Cnt=0; lo_Cnt < 4; lo_Cnt++ )
  { 
    if (GasMeter.SensEnable[lo_Cnt])
    {
      if(Error_IsNone(GasMeter.SensError[lo_Cnt]))
       {
         Sensor.Qf_summ += Sensor.Qfl[lo_Cnt];
//         lo_LUT_Sum += Device_STM.Lut[lo_Cnt];
         Flow.lo_LUT_Sum += st_Lut[lo_Cnt];
         Flow.lo_TRaw_midl += Sensor.Traw[lo_Cnt];
      }
      else  // ���������� ����, ��� ������ �� �������  
      {
        FlagBadSensor++;     
        Sensor.Qfl[lo_Cnt] = 0;
      }  
    } 
  }  // End for
  //------------------------------------------------------
  if (FlagBadSensor < GasMeter.SensCnt)  // �� ��� ������ ��������!!!
  {
     AllSGMModuleErr = 0;
     Sensor.Qf_summ -=  Flow.lo_LUT_Sum;
     // ������� �������
     Sensor.Qf_summ = (Sensor.Qf_summ * GasMeter.SensCnt)/(GasMeter.SensCnt - FlagBadSensor);   // (GasMeter.SensCnt - FlagBadSensor)
     Sensor.Traw_midl =   (Flow.lo_TRaw_midl)/(GasMeter.SensCnt - FlagBadSensor);// (GasMeter.SensCnt - FlagBadSensor)

  //   Sensor_Qfl = Sensor.Qf_summ;
     if(EnableKalmanFilter)
     {
        Flow.Sensor_Qfl = Sensor.Qf_summ;
        Sensor.Qf_summ += 10000;
        if(Sensor.Qf_summ > Flow.KalmanBorder + 10000)  // KalmanBorder = 220
        {
           Flow.KalmanBorder = KalmanBorderL; // 180
           if(Flow.lo_KalmanK != KalmanK2)         // ��������� �� ������ �2=0,95
           {
              if(Flow.lo_KalmanFlag >= KalmanCnt) // ��������� � ����������� ���� n ��� 
              {
                 Flow.lo_KalmanK = KalmanK2;
                 Flow.lo_LastSensQf = 10000; 
                 Flow.lo_AddToFlow =1;
              } 
              else                       // ������ ������� 
              {
                 Flow.lo_LastMeas += Sensor.Qf_summ;  // ��������� ��������
                 Sensor.Qf_summ = 10000;         // ���������� ���������
                 Flow.lo_KalmanFlag++;           // ��������� �����
              } 
           }
        }
        else                               // ���� �������
        { 
           Flow.KalmanBorder = KalmanBorderH; // 220
           if((Flow.lo_KalmanK != KalmanK1) || Flow.lo_KalmanFlag)      //   KalmanK1=0,05   
           {
              Flow.lo_KalmanK = KalmanK1;
              Flow.lo_LastSensQf = 10000;
              if (Flow.lo_KalmanFlag)
              {
                  Flow.lo_KalmanFlag = 0;
                  Flow.lo_LastMeas = 0;
              } 
           } 
        }
        

        float lo_SensQf;
        lo_SensQf = (float)Sensor.Qf_summ;

        lo_SensQf = (Flow.lo_KalmanK * lo_SensQf + (1 - Flow.lo_KalmanK) * Flow.lo_LastSensQf);
        Flow.lo_LastSensQf = lo_SensQf;
        Sensor.Qf_summ = (int)lo_SensQf - 10000; 
 
        if(Flow.lo_AddToFlow)
        {
           double lo_QgL;
           if (Flow.lo_LastMeas > QposLimit)
           {
              Flow.dQf_l = CalcSensOffset((int)Flow.lo_LastMeas);
              lo_QgL = ((double)Flow.lo_LastMeas + (double)Flow.dQf_l)/(double)Device_STM.SF_Mid; // division checked
              lo_QgL *= Device_STM.CalkValue; /// ������� �� 20 ��������
              lo_QgL *= 0.06;                 // �������� �3 � ��� 
            //  if(!StatusDevice.availability_crach)
              if(!StatusCrash.bit.error_in_SGM_module)
                 GasMeter.VE_Lm3 += (lo_QgL * (float)SGasMeter._MeasurementIntervalFM/3600.0f); //SGasMeter._MeasurementIntervalFM
           } 
           Flow.lo_LastMeas = 0;
           Flow.lo_AddToFlow = 0;
        }  
     }
     //-----------------------------------------------------------------------------------
     if(Sensor.Qf_summ > QposLimit)  // �������� ���
     {
         Flow.dQf_l = CalcSensOffset(Sensor.Qf_summ);
         GasMeter.dQf = Flow.dQf_l;
         GasMeter.QgL = ((double)Sensor.Qf_summ + (double)Flow.dQf_l)/(double)Device_STM.SF_Mid;
         GasMeter.QgL *= Device_STM.CalkValue; /// ������� �� 20 ��������
     }
     else
       GasMeter.QgL = 0.0;
    //--------------------------------------------------------------------------------------- 
    if (Flow.Sensor_Qfl >=  QposLimit)
    {
      Flow.dQf_l = CalcSensOffset(Flow.Sensor_Qfl);
      GasMeter_QgL_Disp = ((double)Flow.Sensor_Qfl + (double)Flow.dQf_l)/(double)Device_STM.SF_Mid; // division checked
      GasMeter_QgL_Disp *= Device_STM.CalkValue; /// ������� �� 20 ��������
      GasMeter_QgL_Disp *= 0.06;
    }
    else 
     GasMeter_QgL_Disp = 0.0; 
    //----------------------------------------------------------------------------------------------
   // ������ ������� ��� �������  GasMeter_QgL_Disp - m3
     if (GasMeter_QgL_Disp > 0.0)
     {
        float lo_percent;
        if(GasMeter_QgL_Disp < Qfl_limit_QgL_Disp)
          lo_percent = percent_QgL_Disp_max;
        else
          lo_percent = percent_QgL_Disp_min;

        Flow.lo_QgLm3 = (GasMeter_QgL_Disp - GasMeter.QgL_Disp)/GasMeter.QgL_Disp;  

        if((Flow.lo_QgLm3 < lo_percent) && (Flow.lo_QgLm3 > -lo_percent))
        {
           if(Flow.lo_mesCount + 1)
              GasMeter.QgL_Disp = ((GasMeter.QgL_Disp * Flow.lo_mesCount)+ GasMeter_QgL_Disp)/((float)Flow.lo_mesCount + 1.0);
           Flow.lo_mesCount++;
        }
        else
        {
          GasMeter.QgL_Disp = GasMeter_QgL_Disp;
          Flow.lo_mesCount =0;
        } 
     }        
     else
     {
        GasMeter.QgL_Disp = GasMeter_QgL_Disp;
        Flow.lo_mesCount =0;
     }
     //------------------------------
     GasMeter.QgL_m3 = GasMeter.QgL * 0.06;
  }
  else
  {
    AllSGMModuleErr = 1;
    FlagBadSensor++;
  }
  // else  //���!!! ��� �������! �� ���� ����� �� ��������!!! ����� �1!!!!
  
  //  if (SMT_Errors.Items.AllSGMModuleErr)
  // ����� ������ ������������� ��������� ��� ������ 
  //  SMT_Errors.Items.AllSGMModuleErr = 0;
  //###
  // Error_AppendIfError(&error, ERROR_FLOWMEAS_MEASURE_FLOW);
}

// ----------------------------------------------------------------------------
static Error ReadFlowMeasurementConstants(void)
{
  Error error ;
  uint8_t sensEnable=0;
  uint16_t lo_scaleFactor;
  uint16_t lo_unit;
  uint16_t lo_offset, lo_SF_cnt=0;
  uint32_t lo_SF_summ =0;

  FlowMeas_Unit _Unit = FLOWMEAS_UNIT_UNKOWN; // TODO
  sensEnable = 0;
  
  for(uint8_t i=0; i<4;i++)
  {
    error = Sgm_GetSf_Offset(&lo_scaleFactor, &lo_unit, &lo_offset,i);
    if(Error_IsNone(error))
     {
      if (Device_STM.SF[i] != lo_scaleFactor)
        Eeprom_WriteWord((uint32_t)(&Device_STM.SF[i]),lo_scaleFactor);
      lo_SF_summ += lo_scaleFactor;
      lo_SF_cnt++;
      if (Device_STM.Lut[i] != lo_offset) 
        Eeprom_WriteWord((uint32_t)&Device_STM.Lut[i],lo_offset);
      GasMeter.SensEnable[i] = 0xFF;
      sensEnable |= BIT0 << i;
      }
  
    if(Error_IsNone(error))
    {
      float CalkValue = 1.0;
      switch(lo_unit) 
      {
        case 0x0048:
          _Unit = FLOWMEAS_UNIT_NORM_LITER_PER_MINUTE;
          CalkValue = 293.15f / 273.15f;
          break;
        case 0x0248:
          _Unit = FLOWMEAS_UNIT_STANDARD_LITER_15_PER_MINUTE;
          CalkValue = 293.15f / 288.15f;
          break;
        case 0x0148:
          _Unit = FLOWMEAS_UNIT_STANDARD_LITER_20_PER_MINUTE;
          CalkValue = 293.15f / 293.15f;
          break;
        case 0x0348:
          _Unit = FLOWMEAS_UNIT_STANDARD_LITER_25_PER_MINUTE;
          CalkValue =  293.15f / 298.15f;
          break;
        case 0x005B:
          _Unit = FLOWMEAS_UNIT_NORM_LITER_PER_HOUR;
          break;
        case 0x025B:
          _Unit = FLOWMEAS_UNIT_STANDARD_LITER_15_PER_HOUR;
          break;
        case 0x015B:
          _Unit = FLOWMEAS_UNIT_STANDARD_LITER_20_PER_HOUR;
          break;
        case 0x035B:
          _Unit = FLOWMEAS_UNIT_STANDARD_LITER_25_PER_HOUR;
          break;
        default:
          _Unit = FLOWMEAS_UNIT_UNKOWN;
          Error_Add(&error, ERROR_FLOWMEAS_UNKOWN_FLOW_UNIT);
      } // END CASE
      if (Device_STM.CalkValue != CalkValue)
       Eeprom_WriteFloat((uint32_t)&Device_STM.CalkValue,CalkValue);

        if(Error_IsNone(error))
          Sensor_Const.FlowUnit[i] = _Unit;
     }
  }// END FOR
  if(sensEnable)
     if(!sensEnable_eeprom)
     {
        Eeprom_WriteChar((uint32_t)&sensEnable_eeprom, sensEnable);
        GasMeter.SensCnt = lo_SF_cnt;
     }
     else
     {
         GasMeter.SensCnt = 0;
         sensEnable = sensEnable_eeprom ;
         for(uint8_t i=0; i<4;i++)
         {
           if(sensEnable & BIT0 << i)
              ++GasMeter.SensCnt;
         }
     }

  lo_scaleFactor = lo_SF_summ/lo_SF_cnt;
  Eeprom_WriteWord((uint32_t)&Device_STM.SF_Mid,lo_scaleFactor);
  if(lo_SF_cnt == 0)
  {
     Error_Add(&error,ERROR_NO_MODULE_GASMETER);
     AllSGMModuleErr =1;
     //###
  }  
  else
     error =  Error_None();
  return error;
}

void ClearFlow()
{
        update_time_FMMeasurements();
        GasMeter.QgL = 0;
        GasMeter.QgL_Disp = 0;
        GasMeter.QgL_m3 = 0;
        Sensor.Qf_summ = 0;
        Flow.Sensor_Qfl = 0;
        Flow.lo_KalmanFlag = 0;
        Flow.lo_LastMeas = 0;
        Flow.lo_KalmanK = KalmanK1;
        Flow.lo_LastSensQf = 10000;
                  
        Kfactor_measure_mode = KFACTOR_MODE_NORM;
        Kfactor_UI_measure_mode = KFACTOR_MODE_NORM; 
}
/* ----------------------------------------------------------------------------
static float ZeroingTinyFlow(float flow)
{
  SensorTypeParameters* params = Sgm_GetParameters();
  
 // if((params->Q_negLimit < flow) && (flow < 0.0f))
  if (flow < 0.0f)
      return 0.0f;
  
  if((0.0f < flow) && (flow < params->Q_posLimit)) 
    return 0.0f;

  return flow;
}
*/

// ----------------------------------------------------------------------------
/*
static void UpdateShort2LongFactor(float qShort, float qLong)
{
  if((qShort >= Config_Short2LongFactor_MinQ_For_Update) &&  
     (qLong >= Config_Short2LongFactor_MinQ_For_Update)) 
	{
    float short2LongFactor = qLong / qShort;
		float weight = 1.0;
		if(fabsf(short2LongFactor - _Short2LongFactor) >  Config_Short2LongFactor_MaxDeviation)
		{
			weight = Config_Short2LongFactor_MaxDeviation / fabs(short2LongFactor - _Short2LongFactor);
		}
		_Short2LongFactorUpdateTimeMs = Time_GetSystemUpTimeMs();
    _Short2LongFactor += (short2LongFactor - _Short2LongFactor)
                              * Config_Short2LongFactor_SmoothingFactor * weight;
			
  }
}
*/
