//=============================================================================
/// \file    SensorTypeParameters.c
/// \author  RFU
/// \date    29-Oct-2015
/// \brief   Module for sensor specific parameters
//=============================================================================
// COPYRIGHT
//=============================================================================

#include "SensorTypeParameters.h"

// Indexes must match with Sgm_Type definition in Sgm.h!
//SensorTypeParameters sensorTypeParameters[] = { 
//   Q_neg | Q_neg | Q_pos | Q_min |   Q_t  |  Q_max |  Q_pos |
//    Sat  | Limit | Limit |       |        |        |   Sat  |        Sensor
//  {   -9.0f, -0.05f,  0.05f, 0.267f,  4.167f,  41.67f,53.0f  }, // Default
//  {  -15.0f, -0.08f,  0.08f, 0.417f,  6.670f,  66.67f,85.0f  }, // SGM6002
//  {  -24.0f, -0.13f,  0.13f, 0.667f, 10.000f, 100.00f,130.0f}, // SGM6004  
//    {  -24.0f, -0.13f,  0.01f, 0.667f, 10.000f, 100.00f,130.0f}, // SGM6004 �����������
//  {  -39.0f, -0.20f,  0.20f, 1.000f, 16.670f, 166.67f,215.0f}, // SGM6006
// }; // indexes must match SensorType-enum in Sgm.h!!!

const TSensorTypeParameters sensorTypeParameters[] /*@0x08080388*/ = {
// Q_nom |  Q_max | Q_Err  |  Tmax   |  Tmin  |  Name 
  { 4.0f,   7.0f,    7.20f,   +55.0f,  -25.0f,  "���-G4"},
  { 6.0f,  11.0f,   12.0f,    +55.0f,  -25.0f,  "���-G6"},
  {10.0f,  18.0f,   19.2f,    +55.0f,  -25.0f,  "���-G10"},
  {16.0f,  28.0f,   30.0f,    +55.0f,  -25.0f,  "���-G16"},
  {25.0f,  45.0f,   48.0f,    +55.0f,  -25.0f,  "���-G25"}
};

// Qmin in variable TypeGasmeterQmin
