//=============================================================================
/// \file    GasMeter.c
/// \author  RFU
/// \date    09-Feb-2016
/// \brief   Implementing the Gasmeter-functionality.
//=============================================================================
// COPYRIGHT
//=============================================================================
#include "GlobalHandler.h"

uint8_t TransmitOpticTest=0;
uint16_t kfactor_emulator = 65535;
//#define CONFIG_USE_CATEGORY_GASMETER
//#define CONFIG_USE_CATEGORY_WELMEC
#define CONST_DELTA_Q 0.01
#include "Configuration.h"

static Error PerformInitalMeasurements(void);
static bool AnalyzeWhichTaskIsRequered(void);
static void PerformFMMeasurements(void);
//static Error PerformSMMeasurements(void);
static void PerformGasRecognition(void);


//static uint32_t GetTimeSinceLastMeasurementSM(void);
static uint32_t GetTimeSinceLastGasRecognition(void);
static Error GetProductSerialNumber(TSens_Const* p_Sensor_const);
static Error GetArticleCode(TSens_Const* p_Sens_Const, uint8_t* size);
static Error Perform_TEST_FMMeasurements(void);

/// \brief VDD_ticks recognition interval in seconds.
const uint32_t Config_GasmeterVDDRecognitionIntervalTestMode = 5;
const uint32_t Config_GasmeterVDDRecognitionIntervalNormalMode = 60*60*6; // 6 hour
/*
const T_GasType GasType[]  @0x08080320 ={
  {0,    0,     0.0, 32000.0},
  {1, 0xFF, 32000.0, 39000.0},
  {2,    0, 39000.0, 40700.0},
  {3, 0xFF, 40700.0, 40960.0},
   };
*/

static void Uint64ToString(uint64_t number, uint8_t* buffer, uint8_t* size);

static Error Gasmeter_TEST_Process(void);

// static Error Is_ModulePresent(void);


static uint16_t TestMeassureInterval;
uint32_t Qmax_warning_cnt_measurement_temp=0;
uint32_t Qmax_cnt_measurement_temp=0;
uint32_t cnt_measurement_temper=0;
uint32_t cnt_measurement_temper_off=0;
uint32_t cnt_measurement_temper_warning=0;
uint16_t TestInterval=100;
uint16_t Kfactor_measure_cnt=0;
uint16_t Kfactor_measure_cnt_off=0;
uint16_t Kfactor_UI_measure_cnt=0;
uint16_t Kfactor_UI_measure_cnt_off=0;

uint8_t Kfactor_measure_mode=KFACTOR_MODE_NORM;
uint8_t Kfactor_UI_measure_mode=KFACTOR_MODE_NORM;
uint8_t cnt_Qmax_flow_off=0;

uint32_t cnt_TempMidl_crash=0;
//-----------------------------------------------------------------------------


/// \brief Measurement interval.
//uint32_t _MeasurementIntervalFM;

/// \brief Gas recognition interval.
static uint32_t _GasRecognitionInterval;
/// \brief Time of last measurement.
static uint32_t _LastMeasurementTimeFM = 0;
/// \brief Time of last gas recognition.
uint32_t _LastGasRecognitionTime = 0;

///// \brief Is set to true if a new measurement is required.
static bool _IsFMMeasurementRequered = false;
static bool _IsGasRecognitionRequered = false;

T_TestMode TestModeActive = TestModeOFF;
T_TestMode TestModeActive_previous = TestModeOFF;

static bool _IsGasRecognitionValid = false;
/// \brief Counts successive errors.
static uint32_t _ErrorCounter = 0;

//TGasMeter GasMeter;
TGasMeter GasMeter @0x20000000;

struct TypeGasMeter SGasMeter @0x200001B0 = 
          {0,  //_MeasurementIntervalFM
           0,  // Kalman_KT
           0,  // Range_T
           0}; // T_Kalman

const TDevice Device_STM @0x08080350 = {0,0,0,0,                    //SF
                                       0,0,0,0,                     //LUT
                                       2,
                                       1.0,
                                       FW_VERSION_MAJOR,
                                       FW_VERSION_MINOR,
                                       HW_VERSION_MAJOR,
                                       HW_VERSION_MINOR,
                                       {0,2,25,18},
                                       "123456",
                                          };




// ����� ���������� ��� ������ 
/// \brief The current flow measurement-value.
float _FlowValue = 0.0f;
/// \brief The current flow measurement-value short.
float _ShortFlowValue = 0.0f;
/// \brief The current total volume measured by the gasmeter.
double _VPulse = 0;

uint16_t _GasTemperatureTraw;
uint16_t _GasRecognitionK;
uint16_t _GasRecognitionK_T; 

double TEST_VE_L = 0.0;
double TEST_VE_Pulse = 0.0;   //�����!!!!       
double TEST_VE_Lm3 = 0.0;
uint32_t TEST_QF_summ = 0;
uint16_t TEST_QF_Count = 0;
float Test_QF_midle = 0.0;

uint32_t Int_Temp_Count_E4 = 0;
float Int_Flow_Max =0.0;
uint16_t Int_Temp_Count = 0;
float Int_Temp_Max = 0;
float Int_Temp_Min = 0;
double Int_Temp_Midl =0;
double Int_cnt_Temp_Midl =0;
double Temp_midl_crash=0;
//extern TypeStatus StatusDevice;


//-----------------------------------------------------------------------------
Error Gasmeter_InitModule(void)
{
  uint8_t size; 
  Error error = Error_None();
  
  memset((char*)Sensor_Const.SN_SGM, 0, sizeof(Sensor_Const) - sizeof(Sensor_Const.FlowUnit));
  error = GetProductSerialNumber(&Sensor_Const);
  if(Error_IsNone(error))   
   error =  GetArticleCode(&Sensor_Const,&size);

 // if(Error_IsNone(error))   
 // {
    _SgmType = (Sgm_Type)type_device;
    switch(type_device)
    {
    case SGM_TYPE_SGM4:
         type_device_int = 4;
         break;
    case SGM_TYPE_SGM6:
         type_device_int = 6;
         break;    
    case SGM_TYPE_SGM10:
         type_device_int = 10;
         break;  
    case SGM_TYPE_SGM16:
         type_device_int = 16;
         break;    
    case SGM_TYPE_SGM25:
         type_device_int = 25;
         break;  
    default:
         type_device_int = 0;
         _SgmType = SGM_TYPE_SGM4;
         Error_Add(&error, ERROR_SGM_CRC);
         break;
    }
 // }
  
  if(Error_IsNone(error) && !error_in_SGM_module) 
    error = PerformInitalMeasurements();
  
  ActivateTestMode(TestModeOFF); 
  
  return error;
}

//-----------------------------------------------------------------------------
Error Gasmeter_Process(void)
{
#define Kalman_T_Low  1.0
#define Kalman_T_Mid  0.05
#define Kalman_T_Hi   0.95
  
  Error error = Error_None();  
 
  if(TestModeActive)
  {
    error = Gasmeter_TEST_Process();
    return error;
  } 
  
   if (AnalyzeWhichTaskIsRequered())             // ����� ������� ������
       SGM_PowerOn();
   else   // ��� �������� ������
     return error;
  
   if(_IsGasRecognitionRequered && !reverse_flow)               //���������� �������� � ������
     PerformGasRecognition();
   else
   {  // ���������� ��������� ������� 
     if(_IsFMMeasurementRequered)                // Long ���������
        PerformFMMeasurements();
   }
  SGM_PowerOff();
   
  if(Error_IsError(error)) 
      _ErrorCounter++;
  else 
  {
// ���������� �������� ���� � ���    
    float lo_Temper;

    lo_Temper = GasMeter.var_T;
    _ErrorCounter = 0;
    
    if (Int_Temp_Count==0)
    {
      Int_Temp_Max = lo_Temper;
      Int_Temp_Min = Int_Temp_Max;
      Int_Temp_Midl = lo_Temper;
      Int_Flow_Max = GasMeter.QgL_m3;
      Int_QgL_m3_midl = GasMeter.QgL_m3;
    }
    else
    {
      if(lo_Temper >= Int_Temp_Max)
        Int_Temp_Max = lo_Temper;
      if(lo_Temper <= Int_Temp_Min)
        Int_Temp_Min = lo_Temper;
      if(GasMeter.QgL_m3 > Int_Flow_Max)
        Int_Flow_Max = GasMeter.QgL_m3;
      
      Int_Temp_Midl =(Int_Temp_Midl* Int_Temp_Count + lo_Temper)/(Int_Temp_Count+1);
      Int_QgL_m3_midl = (Int_QgL_m3_midl * Int_Temp_Count + GasMeter.QgL_m3) / (Int_Temp_Count + 1);
    }
   Int_Temp_Count++; 
      //--------------------------------------------------------------------------------------
    Temp_midl_crash += GasMeter.var_T;
    ++cnt_TempMidl_crash;
    
   if ((GasMeter.var_T > 60.0)|| (GasMeter.var_T < -30.0))  
      SGasMeter.Kalman_KT = Kalman_T_Hi;
   else
    if ((GasMeter.var_T > -25.0) && (GasMeter.var_T < 55.0))
      SGasMeter.Kalman_KT = Kalman_T_Low;
    else
      SGasMeter.Kalman_KT = Kalman_T_Mid; 
   
    
   SGasMeter.T_Kalman = SGasMeter.Kalman_KT * GasMeter.var_T + (1.0 - SGasMeter.Kalman_KT) * SGasMeter.T_Kalman;

   if ((SGasMeter.T_Kalman > 60.0) || (SGasMeter.T_Kalman < -30.0))
     SGasMeter.Range_T = 2; 
   else
    if ( (SGasMeter.T_Kalman > -25.0) && (SGasMeter.T_Kalman < 55.0) )
       SGasMeter.Range_T = 0;
    else
      SGasMeter.Range_T = 1;
  
  if(!going_beyond_T_gas)
  {
    if (SGasMeter.Range_T == 2)
      Int_Temp_Count_E4 += cnt_measurement_temper_on_chort_eeprom;
    if (SGasMeter.Range_T == 1)
      Int_Temp_Count_E4 += 1;
    if (SGasMeter.Range_T == 0)
      Int_Temp_Count_E4 = 0;
   if (Int_Temp_Count_E4 >= cnt_measurement_temper_on_long_eeprom)
   {
     going_beyond_T_gas = 1;
     Int_Temp_Count_E4 = 0;
   }
  }
  else  
   {
     if (SGasMeter.Range_T == 0)
      Int_Temp_Count_E4 += cnt_measurement_temper_off_eeprom;
     if (Int_Temp_Count_E4 >= cnt_measurement_temper_on_long_eeprom)
       {
         going_beyond_T_gas = 0;
         Int_Temp_Count_E4 = 0;
       } 
     }

  }
  
    if(TransmitEnable == TRANSMIT_ENABLE && TransmitOpticTest)
      {
        char *ptr;
        GetDate_Time(&SystDate,&SystTime); 
        sprintf((char*)TransmitBuffer,"%0.2u:%0.2u:%0.2u\tQfSK=%d\tQfS=%d\tQgL3=%0.6f\tQgL3D=%0.6f\tKf=%f", // dQf = %0.4f
        SystTime.Hours, SystTime.Minutes, SystTime.Seconds, Sensor.Qf_summ, Flow.Sensor_Qfl, GasMeter.QgL_m3, GasMeter_QgL_Disp, 
        GasMeter.mid_GasRecond );
        ptr = (char*)TransmitBuffer + strlen(TransmitBuffer);
        sprintf(ptr,"\tTg=%0.1f\tTo=%0.1f\tV=%0.6f\tKm=%d\tKK=%0.3f\tKCF=%d\tKK1=%0.3f\tKK2=%0.3f\tKB=%d\tKC=%d\r\n", // 
                                       GasMeter.var_T, GasMeter.var_T/*GetExtTemp()*/, GasMeter.VE_Lm3, EnableKalmanFilter, Flow.lo_KalmanK, Flow.lo_KalmanFlag,
                                       KalmanK1,KalmanK2, Flow.KalmanBorder, KalmanCnt);
        Opto_Uart_Transmit(); 
      }  
  
  if(_ErrorCounter >= 3)
    Error_SetFatal(&error);
 return error;   
}

double GetTempMidl_crash()
{
  if(cnt_TempMidl_crash)
     return Temp_midl_crash / cnt_TempMidl_crash;
  else
     return 0.0;
}

void Clear_cnt_TempMidl_crash()
{
  cnt_TempMidl_crash = 0;
  Temp_midl_crash = 0;
}

uint32_t Get_cnt_TempMidl_crash()
{
  return cnt_TempMidl_crash;
}
//-----------------------------------------------------------------------------
static Error Gasmeter_TEST_Process(void)
{
  uint8_t lo_Flag = 0;
  uint16_t lo_pulsePeriod;
  uint16_t lo_sysdelay = 0;
    
  Error error = Error_None();  
  
//  startPeriodicTimer();
   
  if(AnalyzeWhichTaskIsRequered())       // ����� ������� ������
     SGM_PowerOn();
    
  if(_IsGasRecognitionRequered)               //���������� �������� � ������
    PerformGasRecognition();
 
 
  if(_IsFMMeasurementRequered)                // Long ���������
    {
      error = Perform_TEST_FMMeasurements();
      lo_Flag = 0xff;
    }

  SGM_PowerOff(); 

//  lo_sysdelay = stopPeriodicTimer();
  
  if(Error_IsError(error)) 
    _ErrorCounter++;
  else 
    _ErrorCounter = 0;
  
  if(_ErrorCounter >= 3)
      Error_SetFatal(&error);
    
  if(lo_Flag)
  {
    switch(TestModeActive)
          { 
            case TestMode_4:  
                   GetDate_Time(&SystDate,&SystTime); 
                   sprintf((char*)TransmitBuffer,"%0.2u:%0.2u:%0.2u\t%0.5f\t%0.5f\r\n",\
                          SystTime.Hours,SystTime.Minutes,SystTime.Seconds,GasMeter.QgL_m3,\
                           TEST_VE_Lm3);

                   Opto_Uart_Transmit(); 
                  break;
            case TestMode_3:
                   if(TestMeassureInterval)
                     {
                       TestMeassureInterval -= SGasMeter._MeasurementIntervalFM;
                       TEST_QF_summ += Sensor.Qf_summ;
                       TEST_QF_Count++;
                     }
                    if(TestMeassureInterval==0)
                      {
                        Test_QF_midle = (float)TEST_QF_summ/(float)TEST_QF_Count;
                         if(TransmitEnable == TRANSMIT_ENABLE)
                           {
                             sprintf((char*)TransmitBuffer,"STOP: %0.2u sek.\t%0.6f\t%0.6f\t%0.4f\t%u\r\n",\
                               TestInterval, TEST_VE_L, TEST_VE_Lm3,Test_QF_midle,TEST_QF_Count );   
                             
                              Opto_Uart_Transmit(); 
                           }
                        GasMeter.VE_Lm3 += TEST_VE_Lm3; //### check 1
                        ActivateTestMode(TestModeOFF);  
                      }              
                   break;
          case TestMode_2:
                _VPulse += TEST_VE_Pulse * 10.0f; 
                Pulse = (int)_VPulse;          // ����� ����� ������ � ������
                _VPulse -= (float)Pulse;
      
                if(Pulse) 
                  {
                    lo_pulsePeriod = (950-lo_sysdelay)/(Pulse*2);
                    lo_pulsePeriod *= SGasMeter._MeasurementIntervalFM;
//                    startOutputTimer(lo_pulsePeriod);
                  }  
                break;  
          case TestMode_1: 
                if(TransmitEnable == TRANSMIT_ENABLE)
                  {
//                    float lo_SummQ;
                    GetDate_Time(&SystDate,&SystTime); 
//                    lo_SummQ = GasMeter.QgL_T[0] + GasMeter.QgL_T[1] + GasMeter.QgL_T[2];
                    sprintf((char*)TransmitBuffer,"%0.2u:%0.2u:%0.2u \t%5d \t%5d \t%5d \t%5d \t%5d \t%0.4f\t%0.4f\t%0.6f\r\n",\
                    SystTime.Hours,SystTime.Minutes,SystTime.Seconds,\
                    Sensor.Qfl[0],Sensor.Qfl[1],Sensor.Qfl[2],Sensor.Qfl[3],Sensor.Qf_summ,\
                    /*GasMeter.QgL_T[0],GasMeter.QgL_T[1],GasMeter.QgL_T[2]*/GasMeter.QgL_m3,
                    GasMeter.QgL_Disp, TEST_VE_Lm3 );

                    Opto_Uart_Transmit(); 
                  }
                break;  
         case TestMode_5: 
                if(TransmitEnable == TRANSMIT_ENABLE)
                  {
                    GetDate_Time(&SystDate,&SystTime); 
                    uint32_t sizefree = xPortGetFreeHeapSize();
                    sprintf((char*)TransmitBuffer,"%0.2u:%0.2u:%0.2u\t%u\t%u\t%u\t%u\t%u\t%u\t%u\t%u\t%u\r\n", 
                    SystTime.Hours,SystTime.Minutes,SystTime.Seconds,
                    stack_DispatcherTask, stack_GasmeterTask, stack_OPTO_ReadTask, stack_ReadWriteARC, 
                    stack_task_GSM, stack_GSM_RX_UARTTask, stack_ParsingComandsTask, stack_IDLE, sizefree);

                    Opto_Uart_Transmit(); 
                  }
                break;                  
          }  // end switch  
    lo_Flag =0;
   }
  return error;
}

//-----------------------------------------------------------------------------
static Error PerformInitalMeasurements(void)
{
  Error error = Error_None();
  
  for(uint8_t i = 4; i > 0; i--) 
  {
    PerformGasRecognition(); 
    if(_IsGasRecognitionValid)
      break;
  }
  
  if(!_IsGasRecognitionValid) 
  {
    Error_Add(&error, ERROR_GASMETER_INIT_GASREC);
    //SMT_Warnings.Items.GasRecNotDefined = 1;
  }
    
  if(Error_IsNone(error)) 
    PerformFMMeasurements();
  
  return error;
}

//-----------------------------------------------------------------------------
static bool AnalyzeWhichTaskIsRequered(void)
{
  uint32_t gasRegInterval; 
  
  switch(Kfactor_measure_mode)
  {
  default:
  case KFACTOR_MODE_NORM:
       gasRegInterval = Config_GasmeterGasRecognitionIntervalNormalMode; // 21600
       break;
  case KFACTOR_MODE_MEASURE:
       gasRegInterval = Config_GasmeterGasRecognitionIntervalMeasureMode;       // 10
       break;
  case KFACTOR_MODE_ERROR:
       gasRegInterval = Config_GasmeterGasRecognitionIntervalErrorMode;       // 3600
       break;
  }  
  switch(Kfactor_UI_measure_mode)
  {
  default:
  case KFACTOR_MODE_NORM:
       if(Kfactor_measure_mode == KFACTOR_MODE_NORM)
          gasRegInterval = Config_GasmeterGasRecognitionIntervalNormalMode; // 21600
       break;
  case KFACTOR_MODE_MEASURE:
       gasRegInterval = Config_GasmeterGasRecognitionIntervalMeasureMode;       // 10
       break;
  case KFACTOR_MODE_ERROR:
       if(Kfactor_measure_mode != KFACTOR_MODE_MEASURE)
          gasRegInterval = Config_GasmeterGasRecognitionIntervalErrorMode;       // 3600
       break;
  }  
  
  if(Global_Menu == MAIN_MENU_GASTYPE)
     gasRegInterval = Config_GasmeterGasRecognitionIntervalMeasureMode;
  
  if(!_IsGasRecognitionValid) // error measure K factor
    gasRegInterval = Config_GasmeterGasRecognitionRetryIntervalNormalMode;       // 600 sec
  if(TestModeActive) 
  {   
     gasRegInterval = _IsGasRecognitionValid ? _GasRecognitionInterval
                                            : Config_GasmeterGasRecognitionIntervalTestMode;

     _IsGasRecognitionRequered = GetTimeSinceLastGasRecognition() >= 
                                   gasRegInterval;
     
     _IsFMMeasurementRequered = GetTimeSinceLastMeasurementFM() >= SGasMeter._MeasurementIntervalFM;

    /* if (TestModeActive == TestMode_4)
       _IsSMMeasurementRequered = GetTimeSinceLastMeasurementSM() >= _MeasurementIntervalSM;
      else
        _IsSMMeasurementRequered = false; */
  }
  else
  {
    /*
    if(GasMeter.Gas_Type_Good) 
        gasRegInterval = _IsGasRecognitionValid ? Config_GasmeterGasRecognitionRetryIntervalNormalMode
                                          : Config_GasmeterGasRecognitionIntervalTestMode;
      else                                    
       gasRegInterval = Config_GasmeterGasRecognitionIntervalTestMode;
*/
      _IsGasRecognitionRequered = GetTimeSinceLastGasRecognition() >= 
                                   gasRegInterval;
      _IsFMMeasurementRequered = GetTimeSinceLastMeasurementFM() >= SGasMeter._MeasurementIntervalFM;
   }
  if(_IsGasRecognitionRequered || _IsFMMeasurementRequered )
    return true;
  else
    return false;
}  

//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
static void PerformFMMeasurements(void)
{
  
  double WE_IntervalValue;
  uint32_t Meas_interval;
  
  Error error = Error_None();
  
//   Time_DelayMilliSeconds(Config_GasmeterTimeBeforeFlowMeasurementMilliseconds);
   FlowMeas_MeasureFlow();
   if(AllSGMModuleErr == 0)
   { 
       Meas_interval = GetTimeSinceLastMeasurementFM();

     // ��������� ����� � ������ "��������" �������" 
     WE_IntervalValue = (GasMeter.QgL * (float)Meas_interval/60.0f ); //�����/������
     //GasMeter.VE_L += WE_IntervalValue;
     GasMeter.VE_Pulse = WE_IntervalValue;   //�����!!!!   
  //   if(!StatusDevice.availability_crach)
     if(!StatusCrash.bit.error_in_SGM_module)
        GasMeter.VE_Lm3 += (GasMeter.QgL_m3 * (float)Meas_interval/3600.0f);
 
      GasMeter.var_T = (float)Sensor.Traw_midl/595.0f -35.0f;
      // �������� �����
   
  }
  _LastMeasurementTimeFM = Time_GetSystemUpTimeSecond();
}
//------------------------------------------------------------------------------
static Error Perform_TEST_FMMeasurements(void)
{
  
  double WE_IntervalValue;
  uint16_t Meas_interval;
  
  Error error = Error_None();
  
   FlowMeas_MeasureFlow();
   if(AllSGMModuleErr == 0) 
   { 
     Meas_interval = GetTimeSinceLastMeasurementFM();
     // ��������� ����� � ������ "��������" �������" 
     WE_IntervalValue = (GasMeter.QgL * (float)Meas_interval/60.0f ); //�����/������
     TEST_VE_L += WE_IntervalValue;
     TEST_VE_Pulse = WE_IntervalValue;   //�����!!!!       
     TEST_VE_Lm3 += (GasMeter.QgL_m3 * (float)Meas_interval/3600.0f);

   // GasMeter.var_T = (float)Sensor.Traw[0]/595.0f -35.0f;
     GasMeter.var_T = (float)Sensor.Traw_midl/595.0f -35.0f;
      // �������� �����
   
  }
  _LastMeasurementTimeFM = Time_GetSystemUpTimeSecond();
  return error;
}
//------------------------------------------------------------------------------
void update_time_FMMeasurements()
{
  _LastMeasurementTimeFM = Time_GetSystemUpTimeSecond() - 1;
}
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
// � ������ ������ � ����������� �-������� ��������� ���������� ��������
static void PerformGasRecognition(void)
{
  uint16_t lo_recGas[4];
  uint32_t lo_SumRec[4] = {0,0,0,0};
  uint16_t lo_Count_GasRecond[4] = {0,0,0,0};
  float lo_mid_GasRecond = 0.0;
  uint16_t lo_CntMidGasRecond =0;

  for (uint8_t i=0; i<5; i++)
  {
    Sgm_GasRecognition(&lo_recGas[0],&lo_recGas[1],&lo_recGas[2], &lo_recGas[3]);
    for (uint8_t j=0; j<4; j++)
    {
      if (GasMeter.SensEnable[j])
      {
        if (GasMeter.SensError[j].Raw == 0)
        {
          lo_SumRec[j] += lo_recGas[j];
          ++lo_Count_GasRecond[j];
        } 
      } 
    }
   } 
  
  for (uint8_t j=0; j<4; j++)
  {  
    if ((GasMeter.SensEnable[j]) && (Error_IsNone(GasMeter.SensError[j])))
    {
      if(lo_Count_GasRecond[j])
      {
        Sensor.KL[j] = lo_SumRec[j]/lo_Count_GasRecond[j];
        lo_mid_GasRecond += (float)Sensor.KL[j];
        lo_CntMidGasRecond++;
      }  
    }  
  } 

  if (lo_CntMidGasRecond) 
    GasMeter.mid_GasRecond = lo_mid_GasRecond /(float)lo_CntMidGasRecond;

    if(kfactor_emulator != 65535)
      GasMeter.mid_GasRecond = (float)kfactor_emulator;
    //--------------------------------------------------------
  if((GasMeter.mid_GasRecond > (float)Kfactor_average && GasMeter.mid_GasRecond < (float)Kfactor_max) || (GasMeter.mid_GasRecond < (float)Kfactor_min))
  {
     if(Kfactor_measure_mode == KFACTOR_MODE_NORM)
     {
       Kfactor_measure_mode = KFACTOR_MODE_MEASURE;
     }
     if(Kfactor_measure_mode != KFACTOR_MODE_ERROR && Kfactor_measure_cnt > Kfactor_measure_cnt_eeprom)
     {
        Kfactor_measure_mode = KFACTOR_MODE_ERROR;
     }
     ++Kfactor_measure_cnt;
     Kfactor_measure_cnt_off = 0;
  }
  else
  {
     ++Kfactor_measure_cnt_off;
     Kfactor_measure_cnt = 0;
     if(Kfactor_measure_mode == KFACTOR_MODE_ERROR)
     {
       Kfactor_measure_mode = KFACTOR_MODE_MEASURE;
     }
     if(Kfactor_measure_mode != KFACTOR_MODE_NORM && Kfactor_measure_cnt_off > Kfactor_measure_cnt_eeprom)
     {
        Kfactor_measure_mode = KFACTOR_MODE_NORM;
     }
  }
  //----------------------------------------------------------------------
  if(StatusWarning.bit.counter_not_put_into_operation)
  {
     if(GasMeter.mid_GasRecond < (float)Kfactor_UI_max && GasMeter.mid_GasRecond > (float)Kfactor_UI_min)
     {
        if(Kfactor_UI_measure_mode == KFACTOR_MODE_NORM)
        {
          Kfactor_UI_measure_mode = KFACTOR_MODE_MEASURE;
        }
        if(Kfactor_UI_measure_mode != KFACTOR_MODE_ERROR && Kfactor_UI_measure_cnt > Kfactor_UI_measure_cnt_eeprom)
        {
           Kfactor_UI_measure_mode = KFACTOR_MODE_ERROR;
        }
        ++Kfactor_UI_measure_cnt;
        Kfactor_UI_measure_cnt_off = 0;
     }
     else
     {
        ++Kfactor_UI_measure_cnt_off;
        Kfactor_UI_measure_cnt = 0;
        if(Kfactor_UI_measure_mode == KFACTOR_MODE_ERROR)
        {
          Kfactor_UI_measure_mode = KFACTOR_MODE_MEASURE;
        }
        if(Kfactor_UI_measure_mode != KFACTOR_MODE_NORM && Kfactor_UI_measure_cnt_off > Kfactor_UI_measure_cnt_eeprom)
        {
           Kfactor_UI_measure_mode = KFACTOR_MODE_NORM;
        }
     }
  }
  else
     Kfactor_UI_measure_mode = KFACTOR_MODE_NORM;  
  //--------------------------------------------------------
  _IsGasRecognitionValid = (Error_IsNone(GasMeter.SensError[0]) || Error_IsNone(GasMeter.SensError[1])\
    || Error_IsNone(GasMeter.SensError[2]) || Error_IsNone(GasMeter.SensError[3]));
  _LastGasRecognitionTime = Time_GetSystemUpTimeSecond();
}

//-----------------------------------------------------------------------------
uint32_t GetTimeSinceLastMeasurementFM(void)
{
  return Time_GetSystemUpTimeSecond() - _LastMeasurementTimeFM;
}

//-----------------------------------------------------------------------------
static uint32_t GetTimeSinceLastGasRecognition(void)
{
  return Time_GetSystemUpTimeSecond() - _LastGasRecognitionTime;
}

//-----------------------------------------------------------------------------
static void Uint64ToString(uint64_t number, uint8_t* buffer, uint8_t* size)
{
  uint64_t divisor = 10000000000000000000ul;
  uint8_t  digit;
  *size = 0;
  while((number < divisor) && (divisor > 0)) {
    divisor /= 10;
  }
  while(divisor) {
    digit = number / divisor;
    number -= digit * divisor;
    buffer[(*size)++] = '0' + digit;
    divisor /= 10;
  }
  if(*size == 0) {
    buffer[0] = '0';
    *size = 1;
  }
}

//-----------------------------------------------------------------------------
static Error GetProductSerialNumber(TSens_Const* p_Sensor_const)
{
  Error error;
  uint8_t size;
  uint64_t lo_serialNumber;
  
if (GasMeter.SensEnable[0])
 {
  error = Sgm_GetProductSerialNumberCh0(&lo_serialNumber);
//  ��������� � ������ 
  if(Error_IsNone(error))
      Uint64ToString(lo_serialNumber,p_Sensor_const->SN_SGM[0], &size);
  else 
    GasMeter.SensError[0].Raw = error.Raw;
 } 
/*
if (GasMeter.SensEnable[1])
 {  
   error = Sgm_GetProductSerialNumberCh1(&lo_serialNumber);
//  ��������� � ������ 
  if(Error_IsNone(error))
      Uint64ToString(lo_serialNumber,p_Sensor_const->SN_SGM[1], &size);
  else
     GasMeter.SensError[1].Raw = error.Raw;
  }

if (GasMeter.SensEnable[2])
 {
   error = Sgm_GetProductSerialNumberCh2(&lo_serialNumber);
//  ��������� � ������ 
   if(Error_IsNone(error))
     Uint64ToString(lo_serialNumber,p_Sensor_const->SN_SGM[2], &size);
   else
     GasMeter.SensError[2].Raw = error.Raw;
 }

if (GasMeter.SensEnable[3])
 {
  error = Sgm_GetProductSerialNumberCh3(&lo_serialNumber);
  //  ��������� � ������ 
  if(Error_IsNone(error))
     Uint64ToString(lo_serialNumber,p_Sensor_const->SN_SGM[3], &size);
  else
     GasMeter.SensError[3].Raw = error.Raw;
 }
*/
 return error;
}

//-----------------------------------------------------------------------------
static Error GetArticleCode(TSens_Const* p_Sens_Const, uint8_t* size)
{
  Error error;
  *size = 20;
  if (GasMeter.SensEnable[0])
     error = Sgm_GetArticleCodeCh0(p_Sens_Const->Article[0]);
  /*
  if (GasMeter.SensEnable[1])
     error = Sgm_GetArticleCodeCh1(p_Sens_Const->Article[1]);
  if (GasMeter.SensEnable[2])
    error = Sgm_GetArticleCodeCh2(p_Sens_Const->Article[2]);
  if (GasMeter.SensEnable[3])
    error = Sgm_GetArticleCodeCh3(p_Sens_Const->Article[3]); 
  */
  return error;
}

//-----------------------------------------------------------------------------
void ActivateTestMode(T_TestMode pTestMode)
{
     if(TestModeActive != pTestMode)
     {
        TestModeActive = pTestMode;
     }
     if (TestModeActive)
     {
       TEST_VE_L = 0.0;
       TEST_VE_Pulse = 0.0;
       TEST_VE_Lm3 = 0.0;
        Kfactor_measure_mode = KFACTOR_MODE_NORM;   
       Kfactor_UI_measure_mode = KFACTOR_MODE_NORM;   
       SGasMeter._MeasurementIntervalFM = Config_GasmeterMeasurementIntervalTestModeFM;
       _GasRecognitionInterval = Config_GasmeterGasRecognitionIntervalTestMode;
       
       _LastMeasurementTimeFM = Time_GetSystemUpTimeSecond();
/*     if(pTestMode == TestMode_2)
          InitOtputTimer();
      else
          DeInitOtputTimer();      */
       if(pTestMode == TestMode_3)
        {
          TestMeassureInterval = TestInterval;
          TEST_QF_summ = 0;
          TEST_QF_Count = 0;
        } 
      } 
   else
     {
       //DeInitOtputTimer();
      SGasMeter._MeasurementIntervalFM = Config_GasmeterMeasurementIntervalNormalModeFM;
      _GasRecognitionInterval = Config_GasmeterGasRecognitionIntervalNormalMode;
     }  
}

//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
