/*
PO_RELEASE
*/


#include "GlobalHandler.h"


//#include "main_function.h"
//#define BUF_MAS_SIZE 40
//uint8_t buf_mas[BUF_MAS_SIZE];
//#define CRYPTSTR_SIZE 40
//uint8_t CryptStr[BUF_MAS_SIZE];
//uint8_t partParsingMessageTMR;
//uint8_t flag_end_transmit_parts;
uint8_t success_session_TCP = _FALSE;
//uint16_t cnt_str_transmit=0;
//TArchiv_type flag_detection_archive;
//uint8_t flag_archive; // ��� ��������� ������ �� TCP
//uint8_t  successful_transfer_data_to_server = _OK;
uint8_t cnt_GSM_TCP_Connect_FALSE=0;
uint8_t cnt_seans_TCP_mode_2=0;
uint8_t flag_change_server_url=_FALSE;
int8_t flag_event=0; // for archive telemetry for exstremal_power_down
int8_t rez_GSM_ExtremalPowerDown=0; // for what reason exstremal_power_down
int8_t flag_event_menu=0;
uint32_t cnt_error_SGM=0;
uint8_t flagPowerONGSMOneTime = 0;
uint8_t CLB_lock_IT = 0;
//uint8_t  cntQueueGSMModemHandle=0;
//uint8_t CRC_old_buf[4];
uint8_t error_connect_server = 0;
//int8_t HiPower_ON = 0;
uint8_t flag_check_crc_dQf=0;
//uint8_t Qmax_warning_temp=0;
uint8_t going_beyond_T_gas = 0;
uint8_t going_beyond_Q = 0;
uint8_t going_beyond_T_warning = 0;
uint8_t source_change_CLB_LOCK = CHANGE_PROGRAM;
uint8_t number_server_transmit=0;
uint8_t AllSGMModuleErr = 0;
//uint8_t flag_ban_Receive_485 = 0;
//uint8_t flag_transmit_485_end = 0;
int8_t cnt_pressed_button_calib = 0; // number of analysis cycles of pressed state


uint64_t battery_telemetry_installation_time=0;
uint64_t battery_counter_installation_time=0;
uint64_t   time_record_Volume=0; // autosave to eeprom

uint64_t  valve_going_beyond_Q_nexTime = 0;
uint8_t   valve_going_beyond_Q_flag_begin = 0; // flag
double    valve_going_beyond_Q_volue = 0;

uint32_t system_tick_ms = 0; // increment each milliseconds

uint64_t story_crash_old = 0;
//uint64_t going_beyond_Q_nexTime = 0;
//uint64_t going_beyond_Q_time = 0;



//uint16_t code_event_buf=EVENT_CHANGE_STATUS;
uint32_t pinPB2_old = 0;
uint32_t PressKeyTime;
uint32_t stack_task_GSM=1000;
uint32_t stack_ParsingComandsTask=1000;
uint32_t stack_DispatcherTask=1000;
uint32_t stack_GasmeterTask=10000;
uint32_t stack_OPTO_ReadTask=1000;
uint32_t stack_ReadWriteARC=1000;
uint32_t stack_GSM_RX_UARTTask=1000;
uint32_t stack_IDLE=1000;
uint64_t event_int_story=0;
//uint64_t event_int_story_old=0;
uint32_t tick_time_lcd_display_on_sec=0;
//uint32_t temp_time_stop_mode;
uint32_t going_beyond_Q_timeU = 0;
uint32_t going_beyond_Q_off_timeU = 0;
uint32_t revers_flow_on_utime;
uint32_t revers_flow_off_utime;
uint32_t valve_revers_flow_on_utime;
int8_t HiPower_ON = 0;
uint16_t difference_lptim;
IntArcTypeDef frameArchiveInt; // For SMS
//EventArcTypeDef frameArchiveEvent;
//ChangeArcTypeDef frameArchiveChange;
uint8_t *ptr_frameArchive;
TypeParamServer ParamServer;
structSMSOutput SMSOutput;

UTypeStatusWarning StatusWarning;
UTypeStatusAlarm   StatusAlarm;
UTypeStatusCrash   StatusCrash;
UTypeStatusSystem  StatusSystem;
UTypeStatusSystem  StatusSystem_buf;
TypeStatusDevice   StatusDevice;

typeStatusEventGSMModem QueueGSMModemHandleStatus;
TTempDataParsingCommand MDM_TempDataParsingCommand;

uint32_t tick_count_message_from_server_msec;
uint32_t numberArcRecFind;
uint32_t currentArcRecNo;
//uint32_t reserved_int;
uint32_t number_communication_gsm_fail;
uint32_t number_communication_gsm_fail_2;
uint32_t number_communication_gsm_fail_3;
TypeValveStatus ValveStatusOld;
//#pragma loc#pragma locationation=0x08080000
//#pragma section = ".eeprom"
//-----------------------------------------------------------------------------------------------------------------------------
// const TQf_param Qf_param[] @0x08080000 - 0x0808031F
const  double VolumeBeforeReset[4]                                                              @0x08080320 ={0.0,0.0,0.0,0.0};
const uint32_t going_beyond_Q_period_sec                                                        @0x08080340 = 180;
const uint16_t Kfactor_UI_min                                                                   @0x08080344 = 1;
const uint16_t Kfactor_UI_max                                                                   @0x08080346 = 32000;
const float Tmin_gas_crash                                                                      @0x08080348 = -25.0;
const float Tmax_gas_crash                                                                      @0x0808034C = 55.0;
// const TDevice Device_STM @0x08080350 - 0x08080387
// const TSensorTypeParameters sensorTypeParameters[] @0x08080388 - 0x080803FF
const  double pressure_abs                                                                      @0x08080400 = 101.325;
const uint32_t cnt_TempMidl_crash_eeprom                                                        @0x08080408 = 432000;
const  uint32_t number_communication_gsm_fail_2_eeprom                                          @0x0808040C = 0;
const float Q_max_multiplier_up                                                                 @0x08080410 = 1.0;
//�������� 0x08080414 - 0x08080417 const float Qmax_min_warning_eeprom                                                  @0x08080414 = 0.8;
const float KalmanK1                                                                            @0x08080418 = 0.05f;
const float KalmanK2                                                                            @0x0808041C = 0.95f;

const  uint32_t number_communication_gsm_fail_eeprom                                            @0x08080420 = 0; 
const  uint32_t current_communication_gsm_number_eeprom                                         @0x08080424 = 0; 
const  uint32_t cnt_fail_sim_card_eeprom                                                        @0x08080428 = 0; 
const  uint32_t cnt_fail_speed_gsm_modem_eeprom                                                 @0x0808042C = 0; 
const  uint32_t reserved_int                                                                    @0x08080430 = 3600;//43200 300
const  int32_t max_cnt_communication_gsm                                                        @0x08080434 = 3000;
const  uint32_t crc_dQf_eeprom                                                                  @0x08080438;
const  uint32_t WaitingTime_LCD_ON_Sec                                                          @0x0808043C  = 180;

const uint32_t IntArcRecNo                                                                      @0x08080440 = 1;//768
const uint32_t DayArcRecNo                                                                      @0x08080444 = 1;//192
const uint32_t ChangeArcRecNo                                                                   @0x08080448 = 1;
const uint32_t SystemArcRecNo                                                                   @0x0808044C = 1;
const uint32_t TelemetryArcRecNo                                                                @0x08080450 = 1;

const int QposLimit                                                                             @0x08080454 = 80;
const int QnegLimit                                                                             @0x08080458 = -6000;
const  uint16_t hw_version                                                                      @0x0808045C = 300; 

//const uint16_t Qmax_warning_cnt_measurement                                                   @0x0808045E = 20;
const uint16_t cnt_going_beyond_Q_off_eeprom                                                    @0x08080460 = 180;
const uint16_t cnt_measurement_temper_on_long_eeprom                                            @0x08080462 = 900;
const uint8_t cnt_measurement_temper_on_chort_eeprom                                            @0x08080464 = 30;  // 
const uint8_t cnt_measurement_temper_off_eeprom                                                 @0x08080465 = 10;  // 
const  uint8_t LCD_fcr_ps                                                                       @0x08080466 = 2; 
const  uint8_t LCD_fcr_div                                                                      @0x08080467 = 12; 
//const int16_t Qfl_reverse_flow                                                                  @0x08080468 = -12000;
const uint32_t cnt_error_SGM_eeprom                                                             @0x08080468 = 864000;
const uint16_t Kfactor_min                                                                      @0x0808046C = 32000;
const uint16_t Kfactor_average                                                                  @0x0808046E = 38000;
const uint16_t Kfactor_max                                                                      @0x08080470 = 40000;
const uint16_t  GasDayBorder                                                                    @0x08080472 = 10;
// -- Normal Mode (default) ---------------------------------------------------
// \name Normal Mode (default) 
/// \brief FUllFlow measurement interval in seconds.
const uint16_t Config_GasmeterMeasurementIntervalNormalModeFM                                   @0x08080474 = 2;
/// \brief Gas recognition interval in seconds.
const uint16_t Config_GasmeterGasRecognitionIntervalNormalMode                                  @0x08080476 =  21600; // 6h
const uint16_t Config_GasmeterGasRecognitionIntervalMeasureMode                                 @0x08080478 =  10; // 
const uint16_t Config_GasmeterGasRecognitionIntervalErrorMode                                   @0x0808047A =  3600; // 
/// \brief Gas recognition retry interval on failer in seconds.
const uint16_t Config_GasmeterGasRecognitionRetryIntervalNormalMode                             @0x0808047C = 600; // 10 �����
// -- Test Mode ---------------------------------------------------------------
// \name Test Mode 
/// \brief Flow measurement fast interval in seconds.
const uint16_t Config_GasmeterMeasurementIntervalTestModeFM                                     @0x0808047E = 1;
/// \brief Gas recognition interval in seconds.
const uint16_t Config_GasmeterGasRecognitionIntervalTestMode                                    @0x08080480 = 300;
const uint16_t KalmanBorderH                                                                    @0x08080482 = 220;
const uint16_t KalmanBorderL                                                                    @0x08080484 = 180;
const uint16_t EnableKalmanFilter                                                               @0x08080486 = 1;
const uint16_t KalmanCnt                                                                        @0x08080488 = 4;


const char EEPROM_SN_SGM [4][SIZE_EEPROM_SN_SGM]                                                @0x0808048A;
#ifdef PO_RELEASE 
const char MdmURL_Server[3][SIZE_MdmURL_Server]                                                 @0x080804DA = {{"089.109.032.058"},0,0} ; // "089.109.032.058" - "89.109.23.203"
const char MdmPort_Server[3][SIZE_MdmPort_Server]                                               @0x0808050A = {{"40200"}, 0, 0} ;//"40200" - "28008"
#endif

#ifndef PO_RELEASE 
const char MdmURL_Server[3][SIZE_MdmURL_Server]                                                 @0x080804DA = {{"089.109.032.058"},0,0} ; // "089.109.032.058" - "89.109.23.203"
const char MdmPort_Server[3][SIZE_MdmPort_Server]                                               @0x0808050A = {{"28005"}, 0, 0} ;//"40200" - "28008"
#endif


const char MdmAPN_Adress[SIZE_MdmAPN_Adress]                                                    @0x0808051C = {0};
const char MdmAPN_Login [SIZE_MdmAPN_Login]                                                     @0x08080534 = {0};
const char MdmAPN_Password[SIZE_MdmAPN_Password]                                                @0x08080540 = {0};
const char MdmNumberBalans[SIZE_MdmNumberBalans]                                                @0x0808054C = {0};
const uint8_t serial_number_board[15]                                                           @0x08080552 = {0}; //
const struct _Mode_Transfer Mode_Transfer                                                       @0x08080562 = {2,10,0,1}; 
// �������� 568 - 5AF
const LL_RTC_DateTypeDef DateVerification                                                       @0x080805B0;
const LL_RTC_DateTypeDef DateNextVerification                                                   @0x080805B4;

const uint8_t auto_switching_mode                                                               @0x080805B8 = 0;
const  uint8_t hard_fault                                                                       @0x080805B9 = 0; 
const uint8_t telephone_number_SMS1[15]                                                         @0x080805BA = {0}; // 14 + null, user server +79101200197
const uint8_t telephone_number_SMS2[15]                                                         @0x080805CA = {0}; // 14 + null, user server
const int8_t type_device                                                                        @0x080805DA = SGM_TYPE_SGM4;
const int8_t CLB_lock_eeprom                                                                    @0x080805DB = 0x66; // factory value
const int8_t optic_kontrast                                                                     @0x080805DC = 7;
const uint8_t Kfactor_UI_measure_cnt_eeprom                                                     @0x080805DD = 18;
const int8_t SensCntErrorLong                                                                   @0x080805DE = 1;
const uint8_t Kfactor_measure_cnt_eeprom                                                        @0x080805DF = 18;
const uint8_t sensEnable_eeprom                                                                 @0x080805E0 = 0;
const uint16_t st_Lut[4]                                                                        @0x080805E2  = {10000,10000,10000,10000};  // ����� ���� 
const uint8_t password_provider[8]                                                              @0x080805EA  = {"000000"};
const _TypeDateTimeCommissioning dateTimeCommissioning                                          @0x080805F4  = {0};
const uint32_t waiting_time_connecting_optic_sec                                                @0x08080600  = 600;
const uint16_t delayMeasureExtTemp                                                              @0x08080604  = 10;
const float ExtTempMax                                                                          @0x08080608  = 55.0;
const float ExtTempMin                                                                          @0x0808060C  = -40.0;
const uint16_t going_beyond_T_environ_cnt_measurement                                           @0x08080610  = 180;
const float percent_QgL_Disp_min                                                                @0x08080614  = 0.05;
const float percent_QgL_Disp_max                                                                @0x08080618  = 0.15;
const uint16_t Qfl_limit_QgL_Disp                                                               @0x0808061C  = 20000;
const UTypeStatusWarning StatusWarningEnable                                                    @0x08080620  = {0xFFFF};
const UTypeStatusAlarm StatusAlarmEnable                                                        @0x08080622  = {0xFFFF};
const UTypeStatusCrash StatusCrashEnable                                                        @0x08080624  = {0xFFFF};
const uint8_t show_ver_po_eeprom                                                                @0x08080626 = 0; // for version 1.010247
#ifdef PO_RELEASE 
const uint8_t modem_log_eeprom                                                                  @0x08080627 = 0; // for version 1.010250
#endif

#ifndef PO_RELEASE 
const uint8_t modem_log_eeprom                                                                  @0x08080627 = 1; // for version 1.010250
#endif
const float Tmin_environ_crash                                                                  @0x08080628  = -40.0;
const float Tmax_environ_crash                                                                  @0x0808062C  = 55.0;
const uint32_t cnt_TempEnvironMidl_crash_eeprom                                                 @0x08080630  = 86400;
const uint32_t cnt_error_SGM_long_eeprom                                                        @0x08080634  = 864000;
const uint32_t cnt_error_SGM_short_eeprom                                                       @0x08080638  = 864000;
const uint32_t number_communication_gsm_fail_3_eeprom                                           @0x0808063C  = 0;
const uint32_t delay_time_BAT_REPLACE                                                           @0x08080640  = 86400;
// �������� 0x08080644 - 0x08080647
const uint64_t datetime_eeprom[8]                                                               @0x08080648  = {0, 0, 0, 0, 0, 0, 0, 0};

// 20200127 1.010236 - 1.010257
// to 1.030201 datetime_eeprom[8]
// const uint16_t cnt_measurement_temper_on_long_eeprom @".eeprom" = 900;  // 
// const uint8_t cnt_measurement_temper_on_chort_eeprom @".eeprom" = 30;  // 
// const uint8_t cnt_measurement_temper_off_eeprom @".eeprom" = 10;  // 
//-----------------------------------------------------------------
// to 1.030301

const float GasmeterQmin[5]                                                                     @0x08080688 = {0.04, 0.06, 0.1, 0.16, 0.25};
const float valve_check_flow_multiplier_up_eeprom                                               @0x0808069C = 1.0;

const uint32_t valve_going_beyond_Q_period_sec                                                  @0x080806A0 = 30;
//�������� const uint64_t reverse_flow_up_delay_close_valve_eeprom                                         @0x080806A0 = 30;
const float valve_check_flow_multiplier_down_eeprom                                             @0x080806A8 = 0.0001;
const uint32_t valve_going_beyond_Q_period_sec_eeprom                                           @0x080806AC = 3600;
const uint16_t valve_time_open_slightly_eeprom                                                  @0x080806B0 = 500;
const uint16_t valve_time_open_additionally_eeprom                                              @0x080806B2 = 2500;
const uint16_t valve_time_close_eeprom                                                          @0x080806B4 = 6000;
const uint16_t valve_delay_before_check_flow_eeprom                                             @0x080806B6 = 10;
const uint16_t valve_time_check_flow_eeprom                                                     @0x080806B8 = 10;
const uint16_t valve_time_delay_open_flow_non_null_eeprom                                       @0x080806BA = 30;
const uint8_t enable_MDM_change_valve                                                           @0x080806BC = 0;
const uint8_t valve_cnt_min_eeprom                                                              @0x080806BD = 120;
const uint8_t valve_presence_eeprom                                                             @0x080806BE = 0; //
const uint8_t valve_enable_auto_control_eeprom                                                  @0x080806BF = 0;
const  double VolumeBeforeReset2[4]                                                             @0x080806C0 ={0.0,0.0,0.0,0.0};  // 0x08080688
const uint32_t revers_flow_on_period_eeprom                                                     @0x080806E0 = 180; 
const uint32_t revers_flow_off_period_eeprom                                                    @0x080806E4 = 180;
const uint32_t valve_revers_flow_period_eeprom                                                  @0x080806E8 = 200;
const TypeValveStatus ValveStatus                                                               @0x080806EC= VALVE_UNKNOWN;
const uint8_t valve_resolution_from_TCP_eeprom                                                  @0x080806ED= 1;
const uint8_t mode_conservation                                                                 @0x080806EE= 0;
const _TypeMotoHoursToMils MotoHoursToMilsEEPROM                                                @0x080806F0 = {0,0,0,0,0,0,0, 15, 120, 4, 4, 4, 100, 11000, 42000};
const uint64_t event_int_story_old                                                              @0x08080748 = 0;






//-------------------------------------------------------------------------------------------------------------------------
uint8_t lcd_flag_feature=0;
uint64_t timeUnixMeasureExtTemp = 0;
uint64_t reverse_flow_up_timeUnix=0;
uint64_t   body_cover_open_it_time = 0;
uint8_t  battery_cover_open = 0;
//uint8_t  battery_cover_open_it_flag = 0;
uint64_t battery_cover_open_it_time = 0;
uint8_t  body_cover_open = 0;
uint8_t  body_cover_open_it_flag = 1;
uint32_t pinPB14_old = 0;
uint8_t type_device_int;
uint8_t enable_switch_menu = _OK; 
//uint8_t going_beyond_K_factor=0;
uint32_t error_in_SGM_module;
uint8_t moto_time_start = 0;

double pressure_abs_gas = 96.325;
float residual_capacity_battery_modem_percent = 100.0;
float residual_capacity_battery_main_percent = 100.0;

uint32_t numFindArcBeg;
uint32_t numFindArcEnd;


const char str_end[]={"\x7D\r\n\x1A"};

uint64_t TimeFindUnixBegin;
uint64_t TimeFindUnixEnd;

uint64_t   timeSystemUnix;
uint64_t   timeGSMUnixStart;
uint64_t   timeUnixGSMNextConnect;
_TypeMotoHoursToMils MotoHoursToMils;
_TypeMotoHoursTempToMils MotoHoursTempToMils;
TypeSourceStartModem source_start_modem = SOURCE_START_MDM_NON;

extern osMessageQId QueueGSMModemHandle;
extern osMessageQId QueueParsingCommandHandle;
extern SemaphoreHandle_t xMdmRxSemaphore;
extern SemaphoreHandle_t xCalculateCRC32Semaphore;





//SMT-G
const uint8_t SMT_type[] = "0053004d0054002d0047";
//���.��.��.-
const uint8_t tek_znach_[] = "002004420435043A002E0437043D002E04410447002E002D";
//��� ������
const uint8_t net_interneta_[] = "0020043D0435044200200438043D044204350440043D043504420430";
// ������ �� ���������
const uint8_t server_non_reply[] = "00200441043504400432043504400020043D04350020043E0442043204350447043004350442";
// ����� ���������
const uint8_t break_gprs[] = "0020043E04310440044B043200200438043D044204350440043D043504420430";
//���. ��� � ������
const uint8_t mode_time_in_week[] = "0020044004350436002E0020044004300437002004320020043D043504340435043B044E";
//���. ��� � �����
const uint8_t mode_time_in_month[] = "0020044004350436002E0020044004300437002004320020043C04350441044F0446";
// ������� �������
const uint8_t text_triggered_sensor[] = "00200421044004300431043E04420430043B002004340430044204470438043A0020";
// ������ ������� �������
const uint8_t text_end_triggered_sensor[] = "0020041E0442043C0435043D043000200442044004350432043E04330438002004340430044204470438043A04300020";
// ����
const uint8_t text_gas[] = "0433043004370430";
// ����������
const uint8_t text_smoke[] = "043704300434044B043C043B0435043D0438044F";
// �������
const uint8_t text_flames[] = "043F043B0430043C0435043D0438";
// �������� ����
const uint8_t text_carbon_monoxide[] = "0443043304300440043D043E0433043E00200433043004370430";
// �������� ����
const uint8_t text_water_leakage[] = "043F0440043E044204350447043A043800200432043E0434044B";
// 220V
const uint8_t text_220V[] = "0032003200300056";
// ����������� �������������
const uint8_t text_coolant_temperature[] = "04420435043C043F043504400430044204430440044B002004420435043F043B043E043D043E0441043804420435043B044F";
//�������� �������
const uint8_t text_gas_valve[] = "043304300437043E0432043E0433043E0020043A043B0430043F0430043D0430";

//_Setting Setting;
struct _FlagMessageTMR FlagMessageTMR;
//struct _Mode_Transfer Mode_Transfer;

// ADC
int adc_data_T=0;
int adc_data_Vref=0;
float temperature_environs, volue_Vref;
uint8_t cnt_conversiy_ADC=0;
uint16_t *ptr_TS_CAL1 = (uint16_t*)0x1FF800FA;
uint16_t *ptr_TS_CAL2 = (uint16_t*)0x1FF800FE;
uint16_t *ptr_VREFINT_CAL = (uint16_t*)0x1FF800F8;
uint8_t flag_measure_temperature=0;

extern TSens_Const Sensor_Const;
extern const TDevice Device_STM;
extern xQueueHandle xARCQueue;

/**********************************************************************************************************
** Description :                                                                                         **
***********************************************************************************************************
** In  :                                                                                          **
** Out :                                                                                     **
**********************************************************************************************************/


uint8_t ReadMessageTMR(char *ptrMessageTMR)
{
  /*
        if(ReadMessageTMR((char*)&GSMstring_GSMModule[temp_number_indicator_message_for_gsm-1][0], 
                         GSMcnt_string_GSMModule[temp_number_indicator_message_for_gsm-1],
                         GSM_TCP)) return;
   return 1;  - ���� ���������� ������
   return 0;  - ���  ���������� ������  
  */
 
  portBASE_TYPE xStatus;
  TypeParsingCommand elementParsingCommand;
  memset((char*)FlagMessageTMR.buf_message_tcp, 0, sizeof(FlagMessageTMR.buf_message_tcp));
  elementParsingCommand.ptr_commands = (uint8_t*)ptrMessageTMR;
  elementParsingCommand.source = SOURCE_TCP;
  elementParsingCommand.ptr_out = (uint8_t*)FlagMessageTMR.buf_message_tcp;
  elementParsingCommand.sem_source = &xMdmRxSemaphore;
  elementParsingCommand.ptr_temp_data = &MDM_TempDataParsingCommand;
  MDM_TempDataParsingCommand.number_frame = 0;
  MDM_TempDataParsingCommand.flag_end = _OK;
  MDM_TempDataParsingCommand.index_commands = -1;
  MDM_TempDataParsingCommand.flag_message_of_patrs = _FALSE;
  MDM_TempDataParsingCommand.size_bufer = BUF_MESSAGE_GSM_MODULE_SIZE;
  MDM_TempDataParsingCommand.size_bufer_left = GSM_Get_data_size_left();
  FlagMessageTMR.command = MESSAGE_BT_TCP_NON;
  tick_count_message_from_server_msec = SystemUpTimeSecond;
  //TransmitDebugMessageOptic("QTMR\r\n");  
  xStatus = xQueueSendToBack(QueueParsingCommandHandle, ( void * ) &elementParsingCommand, 1000);
  if( xStatus != pdPASS )
  {
     //### ������ �������� � �������! 
  }  
  
  return 1;
}

/**********************************************************************************************************
** Description :                                                                                         **
***********************************************************************************************************
** In  :                                                                                          **
** Out :                                                                                     **
**********************************************************************************************************/
void ParsingMessageTMR()
{
   if(xSemaphoreTake(xMdmRxSemaphore, 0) == pdPASS)
   {
      if(FlagMessageTMR.command == MESSAGE_TCP_CLOSED)
      {
         if(GSMCloseTCP() == _OK)
         {
            FlagMessageTMR.command = MESSAGE_BT_TCP_NON;
            QueueGSMModemHandleStatus = EVENT_NON;
         }
         else
         {
           xSemaphoreGive(xMdmRxSemaphore);
           //TransmitDebugMessageOptic("SemCL\r\n");
         }
      } 
      else
      {
         if(TransmitMessageTMR(FlagMessageTMR.buf_message_tcp, MDM_TempDataParsingCommand.flag_message_of_patrs_end, MDM_TempDataParsingCommand.flag_message_of_patrs) == _OK
            || GSM_Get_Status_AT_CIPSEND() == SENT_MESSAGE_PART)
         {   
        //    TransmitDebugMessageOptic("TMRO\r\n", 0);
            if(MDM_TempDataParsingCommand.flag_end == _FALSE)
            {
               portBASE_TYPE xStatus;
            
               TypeParsingCommand elementParsingCommand;
               memset((char*)FlagMessageTMR.buf_message_tcp, 0, sizeof(FlagMessageTMR.buf_message_tcp));
               elementParsingCommand.ptr_commands = (uint8_t*)ReceivedBuffer; // it doesn't matter which pointer
               elementParsingCommand.source = SOURCE_TCP;
               elementParsingCommand.ptr_out = (uint8_t*)FlagMessageTMR.buf_message_tcp;
               elementParsingCommand.sem_source = &xMdmRxSemaphore;
               elementParsingCommand.ptr_temp_data = &MDM_TempDataParsingCommand;
               MDM_TempDataParsingCommand.size_bufer_left = GSM_Get_data_size_left();
               tick_count_message_from_server_msec = SystemUpTimeSecond;
               //TransmitDebugMessageOptic("TMRFQ\r\n", 0);
               xStatus = xQueueSendToBack(QueueParsingCommandHandle, ( void * ) &elementParsingCommand, 1000);
               if( xStatus != pdPASS )
               {
                 //### ������ �������� � �������! 
               }  
            }        
         }
         else
         {
            xSemaphoreGive(xMdmRxSemaphore);
            //TransmitDebugMessageOptic("SemPar\r\n", 0);
         }
      }
   }
}
/*!
Transmit message for server across GSM Modem
\param[in] uint8_t *ptr_buf - string 
\param[in] uint8_t last_line - sign of the last part (_OK or _FALSE)
\param[in] uint8_t parts - message consists of parts (MESSAGE_NON_PARTS or MESSAGE_PARTS)
\return _OK - end or _FALSE - in processing
*/
uint8_t TransmitMessageTMR(uint8_t *ptr_buf, uint8_t last_line, uint8_t parts)
{
   uint8_t rez = 1;
//   switch(FlagMessageTMR.source_message)
//   {
//   case GSM_TCP:
        if(parts == _FALSE)
           rez = GSMTransmitMessageTCP(ptr_buf);
        else
           rez = GSMTransmitMessageTCPParts(ptr_buf, last_line);
  //      break;
  // default:break;
  // }
   return rez;
}
/**********************************************************************************************************
** Description :                                                                                         **
***********************************************************************************************************
** In  :                                                                                          **
** Out :                                                                                     **
**********************************************************************************************************/
void CalculationNextCommunicationSession()
{
   if(Mode_Transfer.mode == MODE_ONCE_AN_HOUR)
   {
     timeUnixGSMNextConnect = timeGSMUnixStart + reserved_int;
     // in this mode there are no repeated and standby sessions
     ParamServer.success_session[0] = _OK;
     ParamServer.success_session[1] = _OK;
     ParamServer.success_session[2] = _OK;
   }
   //----------------------------------
   if(Mode_Transfer.mode == MODE_ONCE_A_DAY)
     ++cnt_seans_TCP_mode_2;   
   //----------------------------------
   if(ParamServer.success_session[0] == _OK /*&& ParamServer.success_session[1] == _OK && ParamServer.success_session[2] == _OK*/)
   {
      flagPowerONGSMOneTime = 0;
      cnt_GSM_TCP_Connect_FALSE = 0;
      error_connect_server = 0;
      switch(Mode_Transfer.mode)
      {
      case MODE_ONCE_A_DAY: 
           CalculationTimeOnceADay();
           break;
      case MODE_ONCE_A_DECADE:
           CalculationTimeOnceADecade();
           break;           
      case MODE_ONCE_A_MONTH: 
           CalculationTimeOnceAMonth();
           break;
      default:break;
      }
   }
   else
   {
     if(flagPowerONGSMOneTime == 1)
     {
        timeUnixGSMNextConnect = timeSystemUnix + 40;
     }
     else
     {
      // flagPowerONGSMOneTime = 0;
       /*
        if(number_server_transmit == 0)
        {
          if(telephone_number_SMS1[0] != 0)
             QueueSendGSMModem(TRANSMIT_SMS, 0, SMS_TO_SERVER);
          if(telephone_number_SMS2[0] != 0)
             QueueSendGSMModem(TRANSMIT_SMS, 1, SMS_BREAK_GPRS);
          number_server_transmit = 1;
        }
       */
       source_start_modem = SOURCE_START_MDM_NON;
       ++cnt_GSM_TCP_Connect_FALSE;
       switch(Mode_Transfer.mode)
       {
       case MODE_ONCE_A_DAY: 
           if(cnt_GSM_TCP_Connect_FALSE > 6)
           {
              CalculationTimeOnceADay();
              if(StatusWarningEnable.bit.error_connect_server)
                 error_connect_server = 1;
              flagPowerONGSMOneTime = 0;
              cnt_GSM_TCP_Connect_FALSE = 0;
           }
           else
           {
             timeUnixGSMNextConnect = timeGSMUnixStart + reserved_int;
           }
           /*
           if(cnt_GSM_TCP_Connect_FALSE > 5 && auto_switching_mode)
           {           
              change_transfermode_to_5(CHANGE_PROGRAM);

              cnt_GSM_TCP_Connect_FALSE = 0;
              CalculationTimeOnceADecade();
           }
           */
           break;
       case MODE_ONCE_A_DECADE:
           if(cnt_GSM_TCP_Connect_FALSE > 2)
           {
              flagPowerONGSMOneTime = 0;
              CalculationTimeOnceADecade();
              cnt_GSM_TCP_Connect_FALSE = 0;
              if(StatusWarningEnable.bit.error_connect_server)
                 error_connect_server = 1;
           }
           else
           {
             timeUnixGSMNextConnect = timeGSMUnixStart + reserved_int;
           }           
           break;           
       case MODE_ONCE_A_MONTH: 
           if(cnt_GSM_TCP_Connect_FALSE > 4)
           {
              flagPowerONGSMOneTime = 0;
              cnt_GSM_TCP_Connect_FALSE = 0;
              CalculationTimeOnceAMonth();
              if(StatusWarningEnable.bit.error_connect_server)
                 error_connect_server = 1;
           }
           else
           {
             timeUnixGSMNextConnect = timeGSMUnixStart + reserved_int;
           }               
           break;
       default:break;
       }     
     }
   }
   
   if(flag_change_server_url == _OK)
   {
     flag_change_server_url = _FALSE;
     timeUnixGSMNextConnect = timeGSMUnixStart;
   }
   
}
/**********************************************************************************************************
** Description :                                                                                         **
***********************************************************************************************************
** In  :                                                                                          **
** Out :                                                                                     **
**********************************************************************************************************/       
void  CalculationTimeOnceADay()
{
   struct tm lo_tm;
   memcpy(&lo_tm, _gmtime_((time_t*)&timeGSMUnixStart), sizeof(lo_tm));
   lo_tm.tm_hour = Mode_Transfer.hour;
   lo_tm.tm_min = Mode_Transfer.minute;
   lo_tm.tm_sec = 0;
   timeUnixGSMNextConnect = mktime(&lo_tm) + 86400;
}
/**********************************************************************************************************
** Description :                                                                                         **
***********************************************************************************************************
** In  :                                                                                          **
** Out :                                                                                     **
**********************************************************************************************************/       
void  CalculationTimeOnceADecade()
{
     LL_RTC_DateTypeDef lo_ArcDate;   
     LL_RTC_TimeTypeDef lo_ArcTime;
     uint64_t Ts;
     uint64_t T;
     
     GetDate_Time(&lo_ArcDate, &lo_ArcTime);
     lo_ArcTime.Minutes = Mode_Transfer.minute;
     lo_ArcTime.Hours = Mode_Transfer.hour;     
     lo_ArcDate.Day = Mode_Transfer.day;
     Ts = Get_IndexTime(lo_ArcDate, lo_ArcTime);
     
     for(uint8_t i = 0; i < 3; i++)
     {
        T = Ts + 864000 * i;
        if(T < timeSystemUnix)
        {
          if(i == 2)
          {
            ++lo_ArcDate.Month;
            if(lo_ArcDate.Month == 13)
            {
              lo_ArcDate.Month = 1;
              ++lo_ArcDate.Year;
            }
            timeUnixGSMNextConnect = Get_IndexTime(lo_ArcDate, lo_ArcTime);
          }
        }
        else
        {
           timeUnixGSMNextConnect = T;
           break;
        }
     }
}
              /**********************************************************************************************************
** Description :                                                                                         **
***********************************************************************************************************
** In  :                                                                                          **
** Out :                                                                                     **
**********************************************************************************************************/       
void  CalculationTimeOnceAMonth()
{
     LL_RTC_DateTypeDef lo_ArcDate;   
     LL_RTC_TimeTypeDef lo_ArcTime;
     
     GetDate_Time(&lo_ArcDate, &lo_ArcTime);
     lo_ArcTime.Minutes = Mode_Transfer.minute;
     lo_ArcTime.Hours = Mode_Transfer.hour;     
     lo_ArcDate.Day = Mode_Transfer.day;
     
     ++lo_ArcDate.Month;
     if(lo_ArcDate.Month == 13)
     {
       lo_ArcDate.Month = 1;
       ++lo_ArcDate.Year;
     }
     timeUnixGSMNextConnect = Get_IndexTime(lo_ArcDate, lo_ArcTime);
}

/**********************************************************************************************************
** Description :                                                                                         **
***********************************************************************************************************
** In  :                                                                                          **
** Out :                                                                                     **
**********************************************************************************************************/  
void TransmitDebugMessageOptic(const uint8_t *ptr_message, int32_t rez)
{
   if(OptoPort == OPTO_PORT_ENABLE &&  modem_log_eeprom )
   {
      for(uint32_t i = 0; i < 0xFFFFF; i++)
      {
        if(TransmitEnable == TRANSMIT_ENABLE)
        {
           if(rez)
             sprintf((char*)TransmitBuffer,"GSMD %s%d\r\n", ptr_message, rez);
           else
             sprintf((char*)TransmitBuffer,"GSMD %s\r\n", ptr_message);
           Opto_Uart_Transmit(); 
           return;
        }
      }
   }
   else
     osDelay(5);
}
/**********************************************************************************************************
** Description :                                                                                         **
***********************************************************************************************************
** In  :                                                                                          **
** Out :                                                                                     **
**********************************************************************************************************/  
void WriteTelemetry(uint16_t p_Event_Code, int8_t p_Event_Result, uint16_t p_Sesion_Number, uint8_t type_record)
{
 //  LL_RTC_DateTypeDef lo_ArcDate;   
 //  LL_RTC_TimeTypeDef lo_ArcTime; 
   
 //  GetDate_Time(&lo_ArcDate,&lo_ArcTime);  
 //  code_event_buf = p_Event_Code;
   if(type_record == EVENT_BEGIN)
   {
 //     code_event_buf = p_Event_Code;
      EventTimeBegin = timeSystemUnix;
      
   }
   if(type_record == EVENT_END)
   {
      EventTimeEnd = timeSystemUnix;
   //   if(p_Event_Code == 0)
   //     p_Event_Code = code_event_buf;
      WriteTelemetryArcRecord(p_Event_Code, p_Event_Result, p_Sesion_Number);
   }
}




/**********************************************************************************************************
** Description :                                                                                         **
***********************************************************************************************************
** In  :   STR_ALL_END         STR_PART_END                                                                               **
** Out :                                                                                     **
**********************************************************************************************************/

void change_transfermode_to_5(uint8_t flag_change)
{
   sprintf((char*)OldValue, "%d,%.2d:%.2d,%d", Mode_Transfer.mode, Mode_Transfer.hour, Mode_Transfer.minute, Mode_Transfer.day);  

   Eeprom_WriteChar((uint32_t)&Mode_Transfer.mode, MODE_ONCE_A_DECADE); // every 10 days
   Eeprom_WriteChar((uint32_t)&Mode_Transfer.day,  1);
       
   sprintf((char*)NewValue, "%d,%.2d:%.2d,%d", Mode_Transfer.mode, Mode_Transfer.hour, Mode_Transfer.minute, Mode_Transfer.day);  
   WriteChangeArcRecord(CHANGE_TRANSFER_MODE, flag_change);
   //-----------------------------------------------------
   sprintf((char*)OldValue,"%d", reserved_int); 
   Eeprom_WriteDword((uint32_t)&reserved_int, 43200);
   sprintf((char*)NewValue,"%d", reserved_int); 
   WriteChangeArcRecord(CHANGE_RESERVED_INT, CHANGE_PROGRAM); 
}
/*!
Setting CRC32 prerphi
\param[in] archiv_type ��� ������
\param[in] n_record ����� �������� ������
\param[out] *ptr_out ��������� �� ����� ���� ����������
\return the result of the operation
*/       
void CRC_Init(void)
{
  /*
    hcrc.Instance = CRC;
  hcrc.Init.DefaultPolynomialUse = DEFAULT_POLYNOMIAL_ENABLE;
  hcrc.Init.DefaultInitValueUse = DEFAULT_INIT_VALUE_ENABLE;
  hcrc.Init.InputDataInversionMode = CRC_INPUTDATA_INVERSION_NONE;
  hcrc.Init.OutputDataInversionMode = CRC_OUTPUTDATA_INVERSION_DISABLE;
  hcrc.InputDataFormat = CRC_INPUTDATA_FORMAT_WORDS;
  if (HAL_CRC_Init(&hcrc) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }
  */
  //------------------
   SET_BIT(RCC->AHBENR, RCC_AHBENR_CRCEN);
   // DEFAULT_POLYNOMIAL_ENABLE;
 //  WRITE_REG(CRC->POL, 0x04C11DB7U);  //  DEFAULT_CRC32_POLY
 //  MODIFY_REG(CRC->CR, CRC_CR_POLYSIZE, 0); // CRC_POLYLENGTH_32B
   // DEFAULT_INIT_VALUE_ENABLE
//   WRITE_REG(CRC->INIT, 0xFFFFFFFFU); // DEFAULT_CRC_INITVALUE
   // CRC_INPUTDATA_INVERSION_NONE
 //  MODIFY_REG(CRC->CR, CRC_CR_REV_IN, 0x00000000U); 
   // CRC_OUTPUTDATA_INVERSION_DISABLE
  // MODIFY_REG(CRC->CR, CRC_CR_REV_OUT, 0x00000000U); 
}
/*!
Parsing Queue QueueGSMModemHandle
*/
void ParsingQueueGSMModemHandle()
{
  TypeQueueGSMModem readQueueGSMModem;
   if(QueueGSMModemHandleStatus == EVENT_NON || QueueGSMModemHandleStatus == EVENT_NON_POWER)
   {
      if(GSM_Get_TCPFlag() == TCP_MUST_ENABLE)
         GSM_Set_TCPFlag(TCP_MUST_DISABLE);
 //     if(SMSOutput.must == SMS_MUST_OUTPUT_PDU)
 //     {
 //        SMSOutput.must = SMS_NON_MUST_OUTPUT;
 //        GSM_Set_flag_SMSOutput(SMS_NON_MUST_OUTPUT);
 //     }
           
   //   cntQueueGSMModemHandle = uxQueueMessagesWaiting(QueueGSMModemHandle);
      if(xQueueReceive(QueueGSMModemHandle, ( void * ) &readQueueGSMModem, 0 ) == pdPASS )
      {
    //      cntQueueGSMModemHandle = uxQueueMessagesWaiting(QueueGSMModemHandle);
          //---------------------------------
          if(QueueGSMModemHandleStatus != EVENT_NON_POWER)
          {
             QueueGSMModemHandleStatus = EVENT_DURING;             
             switch(readQueueGSMModem.typeEvent)
             {
        //    case TRANSMIT_TCP_SERVER_ADD:
             case TRANSMIT_TCP_SERVER:
       //        if(readQueueGSMModem.typeEvent == TRANSMIT_TCP_SERVER_ADD && !flagPowerONGSMOneTime)
       //        {
        //         QueueGSMModemHandleStatus = EVENT_NON;
        //         break;
        //       }
                  number_server_transmit = readQueueGSMModem.number_phone_server;
                  //ParamServer.number_phone_server = number_server_transmit + 1;
                  SetURLPortServer((uint8_t*)&MdmURL_Server[readQueueGSMModem.number_phone_server][0], (uint8_t*)&MdmPort_Server[readQueueGSMModem.number_phone_server][0]);
                  GSM_Set_GPRSFlag(GPRS_MUST_ENABLE);  
                  GSM_Set_TCPFlag(TCP_MUST_ENABLE);   
                  GSM_Set_flag_power(GSM_MUST_ENABLE);
                  TransmitDebugMessageOptic("GSM_MUST_ENABLE\r\n", 0);
                  break;
                  /*
             case TRANSMIT_SMS:
           
                  SMSOutput.must = SMS_MUST_OUTPUT_PDU;
                  if(readQueueGSMModem.number_phone_server == 0)
                  {
                     CopyTelNumberOutSMS((uint8_t*)&telephone_number_SMS1[0]);
                  }
                  if(readQueueGSMModem.number_phone_server == 1)
                  {
                     CopyTelNumberOutSMS((uint8_t*)&telephone_number_SMS2[0]);
                  }
                  GSM_Set_flag_SMSOutput(SMS_MUST_OUTPUT_PDU);
                  GSM_Set_flag_power(GSM_MUST_ENABLE);
                  GSM_Set_GPRSFlag(GPRS_MUST_DISABLE);
                  TransmitDebugMessageOptic("GSM_MUST_ENABLE2\r\n", 0);
                  break;
                  */
             case GSM_CALL_CSD:
                  // GSM_Set_flag_CALLOutput(CALL_MUST_OUTPUT);
                  QueueGSMModemHandleStatus = EVENT_NON; //###
                  break;
             default:break;
             }
          }
      }
      else
      {   
          WriteTelemetry(TELEM_EVENT_OFF_GSM_MODEM, 0, 0, EVENT_BEGIN);// EVENT_END EVENT_BEGIN
          GSM_Set_GPRSFlag(GPRS_MUST_DISABLE);
          GSM_Set_flag_power(GSM_MUST_DISABLE);
          QueueGSMModemHandleStatus = EVENT_NULL; 
          TransmitDebugMessageOptic("PWR_DOWN\r\n", 0);
          ParamServer.availability_server1 = 0;
          ParamServer.availability_server2 = 0;
          ParamServer.availability_server3 = 0;
          ClosePassword();
      }
   }
}
/*!
Edite parametr queue QueueGSMModemHandle
\param[in] _typeEventGSMModem type event
\param[in] numb_phone number phone or server
\param[out] *ptr_out ��������� �� ����� ���� ����������
\return the result of the operation
*/   
void QueueSendGSMModem(typeEventGSMModem _typeEventGSMModem, uint8_t numb_phone, TypeSMSOutput type_sms_)
{
  portBASE_TYPE xStatus;
  TypeQueueGSMModem writeQueueGSMModem;
  writeQueueGSMModem.typeEvent = _typeEventGSMModem;
  writeQueueGSMModem.number_phone_server = numb_phone;
  writeQueueGSMModem.type_sms = type_sms_;
  TransmitDebugMessageOptic("QueueSendGSM\r\n", 0);
  xStatus = xQueueSend(QueueGSMModemHandle, ( void * ) &writeQueueGSMModem, 0);  
  if( xStatus != pdPASS )
  {
     TransmitDebugMessageOptic("QueueSendGSM_NON\r\n", 0);
  } 
}

void CalculateMotoHourceGSM(uint8_t flag)
{
  if(flag == MOTO_HOURCE_START)
  {
    moto_time_start |= MOTO_TIME_START_GSM;
    MotoHoursTempToMils.time_workingGSM = xTaskGetTickCount();
    MotoHoursTempToMils.flag_start_time_workingGSM = _OK;
  }
    
  if(flag == MOTO_HOURCE_STOP && MotoHoursTempToMils.flag_start_time_workingGSM == _OK)
  {
     MotoHoursTempToMils.flag_start_time_workingGSM = _FALSE;
     MotoHoursTempToMils.time_workingGSM = xTaskGetTickCount() - MotoHoursTempToMils.time_workingGSM;
     MotoHoursToMils.time_workingGSM += MotoHoursTempToMils.time_workingGSM;
     moto_time_start &= ~MOTO_TIME_START_GSM;
  }
}

void CalculateMotoHourceOPTIC(uint8_t flag)
{
  if(flag == MOTO_HOURCE_START && !StatusDevice.rs485_port_communication_session)
  {
    moto_time_start |= MOTO_TIME_START_OPTIC;
    MotoHoursTempToMils.time_workingOPTIC = xTaskGetTickCount();
    MotoHoursTempToMils.flag_start_time_workingOPTIC = _OK;
  }
    
  if(flag == MOTO_HOURCE_STOP && MotoHoursTempToMils.flag_start_time_workingOPTIC == _OK)
  {
     MotoHoursTempToMils.flag_start_time_workingOPTIC = _FALSE;
     MotoHoursTempToMils.time_workingOPTIC = xTaskGetTickCount() - MotoHoursTempToMils.time_workingOPTIC;
     MotoHoursToMils.time_workingOPTIC += MotoHoursTempToMils.time_workingOPTIC;
     moto_time_start &= ~MOTO_TIME_START_OPTIC;
  }
}

void CalculateMotoHourceSGM(uint8_t flag)
{
  if(flag == MOTO_HOURCE_START && !StatusDevice.rs485_port_communication_session)
  {
    moto_time_start |= MOTO_TIME_START_SGM;
    MotoHoursTempToMils.time_workingSGM = xTaskGetTickCount();//xTaskGetTickCount()
    MotoHoursTempToMils.flag_start_time_workingSGM = _OK;
  }
    
  if(flag == MOTO_HOURCE_STOP && MotoHoursTempToMils.flag_start_time_workingSGM == _OK)
  {
     MotoHoursTempToMils.flag_start_time_workingSGM = _FALSE;
     MotoHoursTempToMils.time_workingSGM = xTaskGetTickCount() - MotoHoursTempToMils.time_workingSGM;//xTaskGetTickCount()
     MotoHoursToMils.time_workingSGM += MotoHoursTempToMils.time_workingSGM;
     moto_time_start &= ~MOTO_TIME_START_SGM;
  }
}


void CalculateMotoHourceWorkCPU(uint8_t flag)
{
   if(flag == MOTO_HOURCE_START && !StatusDevice.rs485_port_communication_session)
   {
      MotoHoursTempToMils.flag_start_time_workingCPU = _OK;
      MotoHoursTempToMils.time_workingCPU = xTaskGetTickCount();
   }
   
  if(flag == MOTO_HOURCE_STOP && MotoHoursTempToMils.flag_start_time_workingCPU == _OK)
  {
     MotoHoursTempToMils.flag_start_time_workingCPU = _FALSE;
     MotoHoursTempToMils.time_workingCPU = xTaskGetTickCount() - MotoHoursTempToMils.time_workingCPU;
     MotoHoursTempToMils.time_workingCPU_sum += MotoHoursTempToMils.time_workingCPU;
  }
}

void CalculateMotoHourceStopCPU()
{
   if(!StatusDevice.rs485_port_communication_session)
   {
      if(!(SystemUpTimeSecond % 600))
      {
        if(MotoHoursTempToMils.time_workingCPU_sum < 600000)
           MotoHoursToMils.time_StopCPU += 600000 - MotoHoursTempToMils.time_workingCPU_sum;
        MotoHoursTempToMils.time_workingCPU_sum = 0;
      }
   }
}

/*!
CalculateMotoHourceWorkCPU()
\param[in]   MOTO_HOURCE_START, MOTO_HOURCE_STOP
\return 
*/ 
void CalculateMotoHourceWorkCPU_IDLE()
{
   if(!(moto_time_start & MASK_MOTO_TIME_START))
      ++MotoHoursToMils.time_WorkCPU;
}

void CalculateMotoHourceWorkLCD(uint8_t flag)
{
   if(flag == MOTO_HOURCE_START && !StatusDevice.rs485_port_communication_session && MotoHoursTempToMils.flag_start_time_workingLCD == _FALSE)
   {
      moto_time_start |= MOTO_TIME_START_LCD;
      MotoHoursTempToMils.flag_start_time_workingLCD = _OK;
      MotoHoursTempToMils.time_workingLCD = xTaskGetTickCount();
   }
   
  if(flag == MOTO_HOURCE_STOP && MotoHoursTempToMils.flag_start_time_workingLCD == _OK)
  {
     MotoHoursTempToMils.flag_start_time_workingLCD = _FALSE;
     MotoHoursTempToMils.time_workingLCD = xTaskGetTickCount() - MotoHoursTempToMils.time_workingLCD;
     MotoHoursToMils.time_workingLCD += MotoHoursTempToMils.time_workingLCD;
     moto_time_start &= ~MOTO_TIME_START_LCD;
  }
}

void Calculation_residual_capacity_battery_percent()
{
//  float residual_capacity_battery_percent_temp;
 // uint64_t energy_temp_main, energy_temp_modem;
  uint64_t energy_temp;
  float temperature_coefficient = 0.4;
  
  float temp;

  __NOP();
 // temp = GetExtTemp();  
  temp = GasMeter.var_T; 
  if(temp > -5.0)
     temperature_coefficient = 1.0;
  if(temp > -15.0 && temp <= -5.0)
     temperature_coefficient = 0.95;
  if(temp > -25.0 && temp <= -15.0)
     temperature_coefficient = 0.9;
  if(temp > -35.0 && temp <= -25.0)
     temperature_coefficient = 0.7;
  if(temp > -45.0 && temp <= -35.0)
     temperature_coefficient = 0.5;
  if(temp <= -45.0)
     temperature_coefficient = 0.4;
  
  
  energy_temp = MotoHoursToMils.time_workingGSM / 1000; // sec
  energy_temp *= MotoHoursToMils.workingGSM_ma; // sec * mA
  residual_capacity_battery_modem_percent = 100 - energy_temp * 100 / ((uint64_t)MotoHoursToMils.capacity_battery_modem_ma * temperature_coefficient * 3600);
  if(residual_capacity_battery_modem_percent < 0)
    residual_capacity_battery_modem_percent = 0;

  energy_temp = (uint64_t)((MotoHoursToMils.time_StopCPU / 1000) * (MotoHoursToMils.StopCPU_mkA / 1000)); 
  energy_temp += (MotoHoursToMils.time_WorkCPU / 1000) * MotoHoursToMils.workingSGM_ma;  
  energy_temp += (MotoHoursToMils.time_workingOPTIC / 1000) * MotoHoursToMils.workingOPTIC_ma;
  energy_temp += (MotoHoursToMils.time_workingSGM / 1000) * MotoHoursToMils.workingSGM_ma;  
  energy_temp += (MotoHoursToMils.time_workingLCD / 1000) * MotoHoursToMils.workingLCD_ma;
  residual_capacity_battery_main_percent = 100 - energy_temp * 100 / ((uint64_t)MotoHoursToMils.capacity_battery_main_ma * temperature_coefficient * 3600);
  if(residual_capacity_battery_main_percent < 0)
    residual_capacity_battery_main_percent = 0;

}


/**********************************************************************************************************
** Description :                                                                                         **
***********************************************************************************************************
** In  :                                                                                          **
** Out :                                                                                     **
**********************************************************************************************************/
/*
void SMS_Output()
{
   if(SMSOutput.must == SMS_MUST_OUTPUT_PDU)
   {
      
     if(GSM_GetStatusOutSMS() == SMS_NON_TRANSMITTED)
     {
        struct tm lo_tm;
        memcpy(&lo_tm, _gmtime_((time_t*)&frameArchiveInt.Index_Time), sizeof(lo_tm));
        memset((char*)SMSOutput.text, 0, 10);
        
        sprintf((char*)buf_str,"SMT-G%dC,%s,%d.%.2d%.2d%.2d;", 
                                                type_device_int,
                                                (uint8_t*)Device_STM.SN_Device, 
                                                FW_VERSION_MAJOR, 
                                                FW_VERSION_MINOR,
                                                FW_GSM_VERSION,
                                                FW_TEST_VERSION); //### ������� ������ �����������

        AnsiToUnicod((uint8_t*)SMSOutput.text, buf_str);
        sprintf((char*)buf_str, "%d;%02d.%02d.%d,%02d:00:00;%d;" ,
                                frameArchiveInt.ArcRecNo = DayArcRecNo,
                                lo_tm.tm_mday, lo_tm.tm_mon + 1, lo_tm.tm_year + 1900, 
                                lo_tm.tm_hour, 
                                (int)frameArchiveInt.ArcVolume * 1000);
        AnsiToUnicod((uint8_t*)SMSOutput.text, buf_str);        
        sprintf((char*)buf_str, "%0.2f;%d;%d;",        
                                frameArchiveInt.ArcTempGas,    
                                frameArchiveInt.ArcK,   
                                frameArchiveInt.num_int);
        AnsiToUnicod((uint8_t*)SMSOutput.text, buf_str);        
        sprintf((char*)buf_str, "%016llX",
                                frameArchiveInt.event_story);                
        AnsiToUnicod((uint8_t*)SMSOutput.text, buf_str);         

        strcat((char*)SMSOutput.text,"\x1A"); 
        GSM_Set_StatusOutSMS(SMS_TRANSMITTED);
     }
   
     GSM_OutSMS((uint8_t*)SMSOutput.text);
   
     if(GSM_GetStatusOutSMS() == SMS_TRANSMIT_OK)
     {
        QueueGSMModemHandleStatus = EVENT_NON;
        SMSOutput.type = SMS_NON;
        SMSOutput.must = SMS_NON_MUST_OUTPUT;
        GSM_Set_flag_SMSOutput(SMS_NON_MUST_OUTPUT);
     }
   }
}
*/
void ParsingWarning()
{
     //--------------------------------------------------------------------------------------------
     // P1
     if(dateTimeCommissioning.date.Month == 0)
     {
        if(!StatusWarning.bit.counter_not_put_into_operation && StatusWarningEnable.bit.counter_not_put_into_operation)
        {
          StatusWarning.bit.counter_not_put_into_operation = 1;
          WriteIntEventArcRecord(EVENT_INT_COMMISSIONING, EVENT_INT_BEGIN);
        }
     }
     else
     {
        if(StatusWarning.bit.counter_not_put_into_operation)
        {
           StatusWarning.bit.counter_not_put_into_operation = 0;
           WriteIntEventArcRecord(EVENT_INT_COMMISSIONING, EVENT_INT_END);
        }
     }
     //--------------------------------------------------------------------------------------------   
     // P2
     if(READ_BIT(GPIOB->IDR, GPIO_IDR_IDR_9))
       StatusDevice.SIM_card_non = 1;
     else
       StatusDevice.SIM_card_non = 0;
      
     if(valve_presence_eeprom == 1)
     {
       if(StatusDevice.SIM_card_non == 1)
       {
         if(!StatusWarning.bit.SIM_card_non && StatusWarningEnable.bit.SIM_card_non)
         {
           StatusWarning.bit.SIM_card_non = 1;
           WriteIntEventArcRecord(EVENT_INT_SIM_CARD_NON, EVENT_INT_BEGIN);
           ValveSetAction(VALVE_ACTION_NON_SIM_CARD);
         } 
       }
       else
       {
          if(StatusWarning.bit.SIM_card_non == 1)
          {
             StatusWarning.bit.SIM_card_non = 0;
             WriteIntEventArcRecord(EVENT_INT_SIM_CARD_NON, EVENT_INT_END);         
          }
       }
     }
     else
     {
        if(StatusDevice.GSM_modem_SIM_card_non == 1)
        {
           if(!StatusWarning.bit.SIM_card_non && StatusWarningEnable.bit.SIM_card_non)
           {
              StatusWarning.bit.SIM_card_non = 1;
              WriteIntEventArcRecord(EVENT_INT_SIM_CARD_NON, EVENT_INT_BEGIN);
           }
        }
        else
        {
          if(StatusWarning.bit.SIM_card_non == 1)
          {
             StatusWarning.bit.SIM_card_non = 0;
             WriteIntEventArcRecord(EVENT_INT_SIM_CARD_NON, EVENT_INT_END);         
          }
        }
     }
     //--------------------------------------------------------------------------------------------   
     // P3
     if(error_connect_server)
     {// error_connect_server
        if(!StatusWarning.bit.error_connect_server && StatusWarningEnable.bit.error_connect_server)
        {
          StatusWarning.bit.error_connect_server = 1;
          WriteIntEventArcRecord(EVENT_INT_ERROR_SESSION_TCP, EVENT_INT_BEGIN);
        }
     }
     else
     {
        if(StatusWarning.bit.error_connect_server)
        {
          StatusWarning.bit.error_connect_server = 0;
          WriteIntEventArcRecord(EVENT_INT_ERROR_SESSION_TCP, EVENT_INT_END);
        }
     }
     //-------------------------------------------------------------------------------------------- 
     // P4
     if(READ_BIT(GPIOE->IDR, GPIO_IDR_IDR_6))
     {// battery not connected 
        if(!StatusWarning.bit.battery_not_connected && StatusWarningEnable.bit.battery_not_connected)
        {
           StatusWarning.bit.battery_not_connected = 1;
           WriteIntEventArcRecord(EVENT_INT_BATARY_TELEMETRY_NON, EVENT_INT_BEGIN);
           ValveSetAction(VALVE_ACTION_CLOSE_BATARY);
        }
     }
     else
     {
        if(StatusWarning.bit.battery_not_connected)
        {
          StatusWarning.bit.battery_not_connected = 0;
          WriteIntEventArcRecord(EVENT_INT_BATARY_TELEMETRY_NON, EVENT_INT_END);
          battery_telemetry_installation_time = timeSystemUnix;
        }
     }
     //-------------------------
     // P5
     if(residual_capacity_battery_modem_percent < 15.0)
     {
        if(!StatusWarning.bit.residual_capacity_battery_modem_min15 && StatusWarningEnable.bit.residual_capacity_battery_modem_min15)
        {
           StatusWarning.bit.residual_capacity_battery_modem_min15 = 1;
           WriteIntEventArcRecord(EVENT_INT_BATTERY_TELEMETRY_MIN_15, EVENT_INT_BEGIN);
        }
     }
     else
     {
        if(StatusWarning.bit.residual_capacity_battery_modem_min15)
        {
           StatusWarning.bit.residual_capacity_battery_modem_min15 = 0;
           WriteIntEventArcRecord(EVENT_INT_BATTERY_TELEMETRY_MIN_15, EVENT_INT_END);
        }
     }
     //-------------------------
     // P6
     if(residual_capacity_battery_modem_percent < 10.0)
     {
        if(!StatusWarning.bit.residual_capacity_battery_modem_min10 && StatusWarningEnable.bit.residual_capacity_battery_modem_min10)
        {
           StatusWarning.bit.residual_capacity_battery_modem_min10 = 1;
           WriteIntEventArcRecord(EVENT_INT_BATTERY_TELEMETRY_MIN_10, EVENT_INT_BEGIN);
        }
     }
     else
     {
        if(StatusWarning.bit.residual_capacity_battery_modem_min10)
        {
           StatusWarning.bit.residual_capacity_battery_modem_min10 = 0;
           WriteIntEventArcRecord(EVENT_INT_BATTERY_TELEMETRY_MIN_10, EVENT_INT_END);
        }
     }
     /*
     //-------------------------
     // P7
     if(residual_capacity_battery_main_percent < 15.0)
     {
        if(!StatusWarning.bit.residual_capacity_battery_main_min15 && StatusWarningEnable.bit.residual_capacity_battery_main_min15)
        {
           StatusWarning.bit.residual_capacity_battery_main_min15 = 1;
           WriteIntEventArcRecord(EVENT_INT_BATTERY_COUNTER_MIN_15, EVENT_INT_BEGIN);
        }
     }
     else
     {
        if(StatusWarning.bit.residual_capacity_battery_main_min15)
        {
           StatusWarning.bit.residual_capacity_battery_main_min15 = 0;
           WriteIntEventArcRecord(EVENT_INT_BATTERY_COUNTER_MIN_15, EVENT_INT_END);
        }
     }  
     //-------------------------
     // P8
     if(residual_capacity_battery_main_percent < 10.0)
     {
        if(!StatusWarning.bit.residual_capacity_battery_main_min10 && StatusWarningEnable.bit.residual_capacity_battery_main_min10)
        {
           StatusWarning.bit.residual_capacity_battery_main_min10 = 1;
           WriteIntEventArcRecord(EVENT_INT_BATTERY_COUNTER_MIN_10, EVENT_INT_BEGIN);
        }
     }
     else
     {
        if(StatusWarning.bit.residual_capacity_battery_main_min10)
        {
           StatusWarning.bit.residual_capacity_battery_main_min10 = 0;
           WriteIntEventArcRecord(EVENT_INT_BATTERY_COUNTER_MIN_10, EVENT_INT_END);
        }
     } 
     */
     //--------------------------------------------------------------------------------------------      
     // P9
  //   if(READ_BIT(GPIOC->IDR, GPIO_IDR_IDR_9) == 0)
     if(battery_cover_open == 1)
     {// battery_cover_open
        if(!StatusWarning.bit.battery_cover_open && StatusWarningEnable.bit.battery_cover_open)
        {
          StatusWarning.bit.battery_cover_open = 1;
          WriteIntEventArcRecord(EVENT_INT_BATTERY_COVER_OPEN, EVENT_INT_BEGIN);
          sprintf((char*)OldValue,"%d", 0);  
          sprintf((char*)NewValue, "%d", 1); 
          WriteChangeArcRecord(CHANGE_BATTERY_COVER_OPEN, CHANGE_PROGRAM); 
          ValveSetAction(VALVE_ACTION_CLOSE_BAT_BODY);
        }
     }
     else
     {
        if(StatusWarning.bit.battery_cover_open)
        {
          StatusWarning.bit.battery_cover_open = 0;
          WriteIntEventArcRecord(EVENT_INT_BATTERY_COVER_OPEN, EVENT_INT_END);
          sprintf((char*)OldValue,"%d", 1);  
          sprintf((char*)NewValue, "%d", 0); 
          WriteChangeArcRecord(CHANGE_BATTERY_COVER_OPEN, CHANGE_PROGRAM); 
        }
     }   
     //--------------------------------------------------------------------------------------------      
     // P11
     if(ValveStatusOld != ValveGetStatus())
     {
        switch(ValveGetStatus())
        {
        case VALVE_CLOSE_TCP: 
        case VALVE_CLOSE_REVERSE_FLOW:          
        case VALVE_CLOSE_Q_MAX:
        case VALVE_CLOSE_NON_NULL:  
        case VALVE_CLOSE_OPTIC:            
        case VALVE_CLOSE_RF_SENSOR:
        case VALVE_CLOSE_BATARY:          
        case VALVE_CLOSE_NON_SIM_CARD: 
        case VALVE_CLOSE_OPEN_BODY:  
        case VALVE_CLOSE_DISCHARGE_BATARY:
        case VALVE_CLOSE_OPEN_BAT_BODY:       
             if(!StatusWarning.bit.close_valve && StatusWarningEnable.bit.close_valve)
             {
                uint64_t stat_valve = ValveGetStatus();
                stat_valve <<= 48;
                StatusWarning.bit.close_valve = 1;                
                WriteIntEventArcRecord(EVENT_INT_CLOSE_VALVE | stat_valve, EVENT_INT_BEGIN);
             }
             break;
        case VALVE_OPEN: 
        case VALVE_NON_POWER:         
        case VALVE_UNKNOWN:  
        case VALVE_CLOSE_OPEN:  
        case VALVE_OPEN_CLOSE: 
        case VALVE_OPEN_FORCED:           
        default:
             if(StatusWarning.bit.close_valve)
             {
                uint64_t stat_valve = ValveStatusOld;
                stat_valve <<= 48;
                StatusWarning.bit.close_valve = 0;
                WriteIntEventArcRecord(EVENT_INT_CLOSE_VALVE | stat_valve, EVENT_INT_END);
             }
             break;
        }
        ValveStatusOld = ValveGetStatus();
     }
}

void ParsingAlarm()
{
     //-----------------------------------------------------------------------
     if(Flow.Sensor_Qfl < QnegLimit)
     {
        if(SystemUpTimeSecond - revers_flow_on_utime > revers_flow_on_period_eeprom)
          reverse_flow = 1;
        revers_flow_off_utime = SystemUpTimeSecond;        
     //   if(SystemUpTimeSecond - valve_revers_flow_on_utime > valve_revers_flow_period_eeprom)
     //     valve_reverse_flow_close_flag = 1;
        if(SystemUpTimeSecond - valve_revers_flow_on_utime > valve_revers_flow_period_eeprom)
           ValveSetAction(VALVE_ACTION_CLOSE_REVERSE_FLOW);
     }
     else
     {
        if(SystemUpTimeSecond - revers_flow_off_utime > revers_flow_off_period_eeprom)
          reverse_flow = 0;
        revers_flow_on_utime = SystemUpTimeSecond;
        valve_revers_flow_on_utime = SystemUpTimeSecond;
     }
     // T1
     if(reverse_flow)
     {
        if(!StatusAlarm.bit.reverse_flow && StatusAlarmEnable.bit.reverse_flow)
        {
          StatusAlarm.bit.reverse_flow = 1;
        //  WriteEventArcRecord(EVENT_REVERSE_FLOW_BEGIN, GSM_Get_current_communication_gsm_number());
          WriteIntEventArcRecord(EVENT_INT_REVERSE_FLOW, EVENT_INT_BEGIN);
        }
     }
     else
     {
        if(StatusAlarm.bit.reverse_flow)
        {
          StatusAlarm.bit.reverse_flow = 0;
       //   WriteEventArcRecord(EVENT_REVERSE_FLOW_END, GSM_Get_current_communication_gsm_number());
          WriteIntEventArcRecord(EVENT_INT_REVERSE_FLOW, EVENT_INT_END);
        }
     }    
     //-----------------------------------------------------------------------
     // T2 going_beyond_Q
     if(GasMeter.QgL_m3 > sensorTypeParameters[_SgmType].Q_max * Q_max_multiplier_up)
     {
        if(!valve_going_beyond_Q_flag_begin)
        {
           valve_going_beyond_Q_flag_begin = 1;
           valve_going_beyond_Q_volue = sensorTypeParameters[_SgmType].Q_max * ((float)valve_going_beyond_Q_period_sec / 3600.0) * Q_max_multiplier_up + GasMeter.VE_Lm3;
           valve_going_beyond_Q_nexTime = timeSystemUnix + valve_going_beyond_Q_period_sec + KalmanCnt * 2;
        }
        //-------------
        if(SystemUpTimeSecond - going_beyond_Q_timeU > going_beyond_Q_period_sec)
           going_beyond_Q = 1;
        going_beyond_Q_off_timeU = SystemUpTimeSecond;
     }
     else
     {
       if(going_beyond_Q == 1)
       {
          if(SystemUpTimeSecond - going_beyond_Q_off_timeU > cnt_going_beyond_Q_off_eeprom)
             going_beyond_Q = 0;
       }
       going_beyond_Q_timeU = SystemUpTimeSecond;
     }
     // T2
     if(going_beyond_Q)
     {
        if(!StatusAlarm.bit.going_beyond_Q && StatusAlarmEnable.bit.going_beyond_Q)
        {
          StatusAlarm.bit.going_beyond_Q = 1;
          WriteIntEventArcRecord(EVENT_INT_MAX_Q, EVENT_INT_BEGIN);
       //   WriteEventArcRecord(EVENT_GOING_BEYOND_Q_BEGIN, GSM_Get_current_communication_gsm_number());
        }
     }
     else
     {
        if(StatusAlarm.bit.going_beyond_Q)
        {
          StatusAlarm.bit.going_beyond_Q = 0;
          WriteIntEventArcRecord(EVENT_INT_MAX_Q, EVENT_INT_END);
        //  WriteEventArcRecord(EVENT_GOING_BEYOND_Q_END, GSM_Get_current_communication_gsm_number());
        }
     }
     //----------------------------------
     // valve QMAX
     if(valve_going_beyond_Q_flag_begin == 1)
     {
        if((timeSystemUnix > valve_going_beyond_Q_nexTime) || valve_going_beyond_Q_volue < GasMeter.VE_Lm3)
        {
           if(valve_going_beyond_Q_volue < GasMeter.VE_Lm3)
           {
              ValveSetAction(VALVE_ACTION_CLOSE_Q_MAX);
           }
           valve_going_beyond_Q_flag_begin = 0;
        }
     }
     //-----------------------------------------------------------------------
     // T3
     if(going_beyond_T_gas)
     {
        if(!StatusAlarm.bit.going_beyond_T_gas && StatusAlarmEnable.bit.going_beyond_T_gas)
        {
          StatusAlarm.bit.going_beyond_T_gas = 1;
          WriteIntEventArcRecord(EVENT_INT_GOING_BEYOND_T_GAS, EVENT_INT_BEGIN);
        }
     }
     else
     {
        if(StatusAlarm.bit.going_beyond_T_gas)
        {
          StatusAlarm.bit.going_beyond_T_gas = 0;
          WriteIntEventArcRecord(EVENT_INT_GOING_BEYOND_T_GAS, EVENT_INT_END);
        }
     }
     //-----------------------------------------------------------------------
     // T4
     /*
     if(Get_cnt_ExtTempMidl_2() >= going_beyond_T_environ_cnt_measurement)
     {
        if(GetExtTempMidl_2() > ExtTempMax || GetExtTempMidl_2() < ExtTempMin)
        {
           if(!StatusAlarm.bit.going_beyond_T_environ && StatusAlarmEnable.bit.going_beyond_T_environ)
           {
              StatusAlarm.bit.going_beyond_T_environ = 1;
              WriteIntEventArcRecord(EVENT_INT_GOING_BEYOND_T_ENVIRON, EVENT_INT_BEGIN);
           }
        }
        else
        {
           if(StatusAlarm.bit.going_beyond_T_environ)
           {
              StatusAlarm.bit.going_beyond_T_environ = 0;
              WriteIntEventArcRecord(EVENT_INT_GOING_BEYOND_T_ENVIRON, EVENT_INT_END);
           }
        }
        Clear_cnt_ExtTempMidl_2();
     }
     */
     
     
     //-----------------------------------------------------------------------
     // T5
     if(Kfactor_measure_mode == KFACTOR_MODE_ERROR)
     {
        if(!StatusAlarm.bit.going_beyond_K_factor && StatusAlarmEnable.bit.going_beyond_K_factor)
        {
           StatusAlarm.bit.going_beyond_K_factor = 1;
           WriteIntEventArcRecord(EVENT_INT_GOING_BEYOND_K_FACTOR, EVENT_INT_BEGIN);  
        }
     }
     else
     {
        if(StatusAlarm.bit.going_beyond_K_factor)
        {
            StatusAlarm.bit.going_beyond_K_factor = 0;
            WriteIntEventArcRecord(EVENT_INT_GOING_BEYOND_K_FACTOR, EVENT_INT_END);  
        }
     }
     //------------------------------------------------------------------------------
     // T6
     if(Kfactor_UI_measure_mode == KFACTOR_MODE_ERROR)
     {
        if(!StatusAlarm.bit.unauthorized_intervention && StatusAlarmEnable.bit.unauthorized_intervention)
        {
           StatusAlarm.bit.unauthorized_intervention = 1;
           WriteIntEventArcRecord(EVENT_INT_UNAUTHORIZED_INTERVENTION, EVENT_INT_BEGIN);  
        }
     }
     else
     {
        if(StatusAlarm.bit.unauthorized_intervention)
        {
            StatusAlarm.bit.unauthorized_intervention = 0;
            WriteIntEventArcRecord(EVENT_INT_UNAUTHORIZED_INTERVENTION, EVENT_INT_END);  
        }
     }
}

void ParsingCrash()
{
     //--------------------------------------------------------------------------------------------
     if(event_int_story_old != event_int_story)
     {
        Eeprom_WriteQword((uint32_t)&event_int_story_old, event_int_story);
        
     }     
  
  
     if(CLB_lock != 0x66)
     {
        if((event_int_story & (uint64_t)0x0000FFFF00000000) & ((uint64_t)StatusCrashEnable.word << 32))
        {
            StatusDevice.availability_crach = 1;
            if(story_crash_old != (event_int_story & (uint64_t)0x0000FFFF00000000))
            {
               timeUnixGSMNextConnect = timeSystemUnix;
               story_crash_old = event_int_story & (uint64_t)0x0000FFFF00000000;
            }
        }
        else
          StatusDevice.availability_crach = 0;
     }
     else
        StatusDevice.availability_crach = 0;
     //--------------------------------------------------------------------------------------------
     // A1
     if(error_in_SGM_module)
     {
     //  ClearFlow();
       if(!StatusCrash.bit.error_in_SGM_module && StatusCrashEnable.bit.error_in_SGM_module)
       {
          StatusCrash.bit.error_in_SGM_module = 1;
          WriteIntEventArcRecord(EVENT_INT_ERROR_SGM_MODULE, EVENT_INT_BEGIN);
       }
     }
     else
     {
       if(CLB_lock == 0x66)
       {
          if(StatusCrash.bit.error_in_SGM_module)
          {
             StatusCrash.bit.error_in_SGM_module = 0;
             WriteIntEventArcRecord(EVENT_INT_ERROR_SGM_MODULE, EVENT_INT_END);
          }
       }
     }
     //-------------------------------------------------------------------------------------------- 
     // A2
     if(GasMeter.var_T < Tmin_gas_crash || GasMeter.var_T > Tmax_gas_crash)
     {
       if(!StatusDevice.going_beyond_T_crash_gas)
       {
          StatusDevice.going_beyond_T_crash_gas = 1;
          Clear_cnt_TempMidl_crash();
       }
     }
     if(StatusDevice.going_beyond_T_crash_gas)
     {
       if(Get_cnt_TempMidl_crash() >= cnt_TempMidl_crash_eeprom)
       {
          if(GetTempMidl_crash() < Tmin_gas_crash || GetTempMidl_crash() > Tmax_gas_crash)
          {
             if(!StatusCrash.bit.going_beyond_T_gas_long && StatusCrashEnable.bit.going_beyond_T_gas_long)
             {
                StatusCrash.bit.going_beyond_T_gas_long = 1;
                WriteIntEventArcRecord(EVENT_INT_GOING_BEYOND_T_GAS_LONG, EVENT_INT_BEGIN);
                sprintf((char*)OldValue,"%lf", GetTempMidl_crash());  
                sprintf((char*)NewValue, "%lf", GetTempMidl_crash());
                WriteChangeArcRecord(CHANGE_TEMP_GAS_CRASH, CHANGE_PROGRAM); 
             }
          }
          else
          {
            StatusDevice.going_beyond_T_crash_gas = 0;
            Clear_cnt_TempMidl_crash();
            if(CLB_lock == 0x66)
            {
               if(StatusCrash.bit.going_beyond_T_gas_long)
               {
                  StatusCrash.bit.going_beyond_T_gas_long = 0;
                  WriteIntEventArcRecord(EVENT_INT_GOING_BEYOND_T_GAS_LONG, EVENT_INT_END);
               }
            }
          }
       }
     }
     //-------------------------------------------------------------------------------------------- 
     // �3
     /*
     if(GetExtTemp() < Tmin_environ_crash || GetExtTemp() > Tmax_environ_crash)
     {
       if(!StatusDevice.going_beyond_T_crash_environ)
       {
          StatusDevice.going_beyond_T_crash_environ = 1;
          Clear_cnt_ExtTempMidl_3();
       }
     }
     if(StatusDevice.going_beyond_T_crash_environ)
     {
       if(Get_cnt_ExtTempMidl_3() >= cnt_TempEnvironMidl_crash_eeprom)
       {
          if(GetExtTempMidl_3() < Tmin_environ_crash || GetExtTempMidl_3() > Tmax_environ_crash)
          {
             if(!StatusCrash.bit.going_beyond_T_environ_long && StatusCrashEnable.bit.going_beyond_T_environ_long)
             {
                StatusCrash.bit.going_beyond_T_environ_long = 1;
                WriteIntEventArcRecord(EVENT_INT_GOING_BEYOND_T_ENVIRON_LONG, EVENT_INT_BEGIN);
                sprintf((char*)OldValue,"%lf", GetExtTempMidl_3());  
                sprintf((char*)NewValue, "%lf", GetExtTempMidl_3());
                WriteChangeArcRecord(CHANGE_TEMP_ENVIRON_CRASH, CHANGE_PROGRAM); 
             }
          }
          else
          {
            StatusDevice.going_beyond_T_crash_environ = 0;
            Clear_cnt_ExtTempMidl_3();
            if(CLB_lock == 0x66)
            {
               if(StatusCrash.bit.going_beyond_T_environ_long)
               {
                  StatusCrash.bit.going_beyond_T_environ_long = 0;
                  WriteIntEventArcRecord(EVENT_INT_GOING_BEYOND_T_ENVIRON_LONG, EVENT_INT_END);
               }
            }
          }
       }
     }
     */
     //-------------------------------------------------------------------------------------------- 
     // A4
     if(body_cover_open)
     {// body cover open
        if(!StatusCrash.bit.body_cover_open && StatusCrashEnable.bit.body_cover_open)
        {
          StatusCrash.bit.body_cover_open = 1;
          WriteIntEventArcRecord(EVENT_INT_OPEN_CASE, EVENT_INT_BEGIN);
          sprintf((char*)OldValue,"%d", 0);  
          sprintf((char*)NewValue, "%d", 1); 
          WriteChangeArcRecord(CHANGE_BODY_COVER_OPEN, CHANGE_PROGRAM); 
          ValveSetAction(VALVE_ACTION_CLOSE_BODY);
        }

     }
     else
     {
        if(CLB_lock == 0x66)
        {
           if(StatusCrash.bit.body_cover_open)
           {
              StatusCrash.bit.body_cover_open = 0;
              WriteIntEventArcRecord(EVENT_INT_OPEN_CASE, EVENT_INT_END);
           }
        }
     }     
     //--------------------------------------------------------------------------------------------
     // A5
     if(CLB_lock == 0x66 || CLB_lock == 0x55)
     {
         if(!StatusCrash.bit.calibration_lock_open && StatusCrashEnable.bit.calibration_lock_open)
         {
            StatusCrash.bit.calibration_lock_open = 1;

            WriteIntEventArcRecord(EVENT_INT_OPEN_CALIB_SWITCH, EVENT_INT_BEGIN);
            
     //       sprintf((char*)OldValue,"%d", CLB_lock);  
    //        sprintf((char*)NewValue, "%d", CLB_lock); 
    //        WriteChangeArcRecord(CHANGE_CLB_LOCK, CHANGE_PROGRAM); 
         }
     }
     else
     {
        if(StatusCrash.bit.calibration_lock_open)
        {
            StatusCrash.bit.calibration_lock_open = 0;
            Eeprom_WriteChar((uint32_t)&CLB_lock_eeprom, 0);

            WriteIntEventArcRecord(EVENT_INT_OPEN_CALIB_SWITCH, EVENT_INT_END);
            
     //       sprintf((char*)OldValue,"%d", CLB_lock);  
     //       sprintf((char*)NewValue, "%d", CLB_lock); 
     //       WriteChangeArcRecord(CHANGE_CLB_LOCK, CHANGE_PROGRAM); 
        }
     }
     
     //--------------------------------------------------------------------------------------------  
     // A6
     if(flag_check_crc_dQf)
     {
        flag_check_crc_dQf = 0;
        uint32_t crc;
        portBASE_TYPE xStatus;
        xStatus = xSemaphoreTake(xCalculateCRC32Semaphore, 5000);
        if( xStatus != pdPASS )
        {
         // ������ 
           xSemaphoreGive(xCalculateCRC32Semaphore);
           return;
        }
        crc = CalculateCRC32((uint8_t*)Qf_param, Qf_sizeof, CRC_START_STOP, 0);
        xSemaphoreGive(xCalculateCRC32Semaphore);
        if(crc != crc_dQf_eeprom)
        {
           if(!StatusCrash.bit.check_crc_dQf_false && StatusCrashEnable.bit.check_crc_dQf_false)
           {
              StatusCrash.bit.check_crc_dQf_false = 1;
              WriteIntEventArcRecord(EVENT_INT_ERROR_CS_DQF, EVENT_INT_BEGIN);
           }
        }
        else
        {
           if(CLB_lock == 0x66)
           {
             if(StatusCrash.bit.check_crc_dQf_false)
             {
               StatusCrash.bit.check_crc_dQf_false = 0;
               WriteIntEventArcRecord(EVENT_INT_ERROR_CS_DQF, EVENT_INT_END);
             }
           }
        }
     }
     //-------------------------------------------------------------------------------------------- 
     // A7
     if(StatusSystem.bit.check_sn_sgm_false == 1
      || StatusSystem.bit.check_sn_sgm2_false == 1
      || StatusSystem.bit.check_sn_sgm3_false == 1
      || StatusSystem.bit.check_sn_sgm4_false == 1 )
     {
       if(!StatusCrash.bit.check_sn_sgm_false && StatusCrashEnable.bit.check_sn_sgm_false)
       {
          StatusCrash.bit.check_sn_sgm_false = 1;
          WriteIntEventArcRecord(EVENT_INT_ERROR_SER_NUMB_SGM, EVENT_INT_BEGIN);
       }
     }
     else
     {
           if(CLB_lock == 0x66)
           {
             if(StatusCrash.bit.check_sn_sgm_false)
             {
               StatusCrash.bit.check_sn_sgm_false = 0;
               WriteIntEventArcRecord(EVENT_INT_ERROR_SER_NUMB_SGM, EVENT_INT_END);
             }
           }
     }
}

void IncrCntSessionServer(uint8_t numb_serv)
{
   switch(numb_serv)
   {
   case 0:
        ++number_communication_gsm_fail;
        Eeprom_WriteDword((uint32_t)&number_communication_gsm_fail_eeprom,  number_communication_gsm_fail);
        break;
   case 1:
        ++number_communication_gsm_fail_2;
        Eeprom_WriteDword((uint32_t)&number_communication_gsm_fail_2_eeprom,  number_communication_gsm_fail_2);
        break;
   case 2:
        ++number_communication_gsm_fail_3;
        Eeprom_WriteDword((uint32_t)&number_communication_gsm_fail_3_eeprom,  number_communication_gsm_fail_3);
        break;
   default:break;
   }
}

void ClosePassword()
{
   if(StatusDevice.password_manufacture_enter)
   {
       sprintf((char*)OldValue,"%d", StatusDevice.password_manufacture_enter);
       StatusDevice.password_manufacture_enter = 0;
       sprintf((char*)NewValue,"%d", StatusDevice.password_manufacture_enter);
       WriteChangeArcRecord(CHANGE_OPEN_PASSWORD_FABRIC, SOURCE_PROGRAM);
   }
   if(StatusDevice.password_provider_enter)
   {
       sprintf((char*)OldValue,"%d", StatusDevice.password_provider_enter);
       StatusDevice.password_provider_enter = 0;
       sprintf((char*)NewValue,"%d", StatusDevice.password_provider_enter);
       WriteChangeArcRecord(CHANGE_OPEN_PASSWORD_PROVIDER, SOURCE_PROGRAM);
   }
   srt_license_key[0] = 0;
}
/**
* @brief Command close for server
* @param parString: pointer for string after char '='
* @param ptr_out_buf: pointer for output bufer
* @param ptr_str_reply: pointer for reply to char '='
* @param source: what communication channel does the team go through
* @retval None
*/
void StackFreeRTOSAanalysis()
{
  uint16_t min_stack = 50;
  /*
                      stack_DispatcherTask, stack_GasmeterTask, stack_OPTO_ReadTask, stack_ReadWriteARC, 
                    stack_task_GSM, stack_GSM_RX_UARTTask, stack_ParsingComandsTask, stack_IDLE, sizefree);
  */
   if(stack_DispatcherTask < min_stack && !StatusDevice.stack_DispatcherTask)
   {
      StatusDevice.stack_DispatcherTask = 1;
      sprintf((char*)OldValue,"%u,%u,%u,%u,%u,%u,%u,%u,%u", stack_DispatcherTask, stack_GasmeterTask, stack_OPTO_ReadTask, stack_ReadWriteARC, 
                    stack_task_GSM, stack_GSM_RX_UARTTask, stack_ParsingComandsTask, stack_IDLE, xPortGetFreeHeapSize());  
      sprintf((char*)NewValue, "%u,%u,%u,%u,%u,%u,%u,%u,%u", 0,0,0,0,0,0,0,0,0);  
      WriteChangeArcRecord(CHANGE_MIN_STACK_DISPATCHER_TASK, SOURCE_PROGRAM);
   }
   if(stack_GasmeterTask < min_stack && !StatusDevice.stack_GasmeterTask)
   {
      StatusDevice.stack_GasmeterTask = 1;
      sprintf((char*)OldValue,"%u,%u,%u,%u,%u,%u,%u,%u,%u", stack_DispatcherTask, stack_GasmeterTask, stack_OPTO_ReadTask, stack_ReadWriteARC, 
                    stack_task_GSM, stack_GSM_RX_UARTTask, stack_ParsingComandsTask, stack_IDLE, xPortGetFreeHeapSize());  
      sprintf((char*)NewValue, "%u,%u,%u,%u,%u,%u,%u,%u,%u", 0,0,0,0,0,0,0,0,0);  
      WriteChangeArcRecord(CHANGE_MIN_STACK_GASMETER_TASK, SOURCE_PROGRAM);
   }
   if(stack_OPTO_ReadTask < min_stack && !StatusDevice.stack_OPTO_ReadTask)
   {
      StatusDevice.stack_OPTO_ReadTask = 1;
      sprintf((char*)OldValue,"%u,%u,%u,%u,%u,%u,%u,%u,%u", stack_DispatcherTask, stack_GasmeterTask, stack_OPTO_ReadTask, stack_ReadWriteARC, 
                    stack_task_GSM, stack_GSM_RX_UARTTask, stack_ParsingComandsTask, stack_IDLE, xPortGetFreeHeapSize());  
      sprintf((char*)NewValue, "%u,%u,%u,%u,%u,%u,%u,%u,%u", 0,0,0,0,0,0,0,0,0);  
      WriteChangeArcRecord(CHANGE_MIN_STACK_OPTO_READ_TASK, SOURCE_PROGRAM);
   }
   if(stack_ReadWriteARC < min_stack && !StatusDevice.stack_ReadWriteARC)
   {
      StatusDevice.stack_ReadWriteARC = 1;
      sprintf((char*)OldValue,"%u,%u,%u,%u,%u,%u,%u,%u,%u", stack_DispatcherTask, stack_GasmeterTask, stack_OPTO_ReadTask, stack_ReadWriteARC, 
                    stack_task_GSM, stack_GSM_RX_UARTTask, stack_ParsingComandsTask, stack_IDLE, xPortGetFreeHeapSize());  
      sprintf((char*)NewValue, "%u,%u,%u,%u,%u,%u,%u,%u,%u", 0,0,0,0,0,0,0,0,0);  
      WriteChangeArcRecord(CHANGE_MIN_STACK_READ_WRITE_ARC_TASK, SOURCE_PROGRAM);
   }
   if(stack_task_GSM < min_stack && !StatusDevice.stack_task_GSM_small)
   {
      StatusDevice.stack_task_GSM_small = 1;
      sprintf((char*)OldValue,"%u,%u,%u,%u,%u,%u,%u,%u,%u", stack_DispatcherTask, stack_GasmeterTask, stack_OPTO_ReadTask, stack_ReadWriteARC, 
                    stack_task_GSM, stack_GSM_RX_UARTTask, stack_ParsingComandsTask, stack_IDLE, xPortGetFreeHeapSize());  
      sprintf((char*)NewValue, "%u,%u,%u,%u,%u,%u,%u,%u,%u", 0,0,0,0,0,0,0,0,0);  
      WriteChangeArcRecord(CHANGE_MIN_STACK_GSM_TASK, SOURCE_PROGRAM);
   }
   if(stack_GSM_RX_UARTTask < min_stack && !StatusDevice.stack_GSM_RX_UARTTask)
   {
      StatusDevice.stack_GSM_RX_UARTTask = 1;
      sprintf((char*)OldValue,"%u,%u,%u,%u,%u,%u,%u,%u,%u", stack_DispatcherTask, stack_GasmeterTask, stack_OPTO_ReadTask, stack_ReadWriteARC, 
                    stack_task_GSM, stack_GSM_RX_UARTTask, stack_ParsingComandsTask, stack_IDLE, xPortGetFreeHeapSize());  
      sprintf((char*)NewValue, "%u,%u,%u,%u,%u,%u,%u,%u,%u", 0,0,0,0,0,0,0,0,0);  
      WriteChangeArcRecord(CHANGE_MIN_STACK_GSM_RX_UART_TASK, SOURCE_PROGRAM);
   }
   if(stack_ParsingComandsTask < min_stack && !StatusDevice.stack_ParsingComandsTask)
   {
      StatusDevice.stack_ParsingComandsTask = 1;
      sprintf((char*)OldValue,"%u,%u,%u,%u,%u,%u,%u,%u,%u", stack_DispatcherTask, stack_GasmeterTask, stack_OPTO_ReadTask, stack_ReadWriteARC, 
                    stack_task_GSM, stack_GSM_RX_UARTTask, stack_ParsingComandsTask, stack_IDLE, xPortGetFreeHeapSize());  
      sprintf((char*)NewValue, "%u,%u,%u,%u,%u,%u,%u,%u,%u", 0,0,0,0,0,0,0,0,0);  
      WriteChangeArcRecord(CHANGE_MIN_STACK_PARSING_COMANDS_TASK, SOURCE_PROGRAM);
   }
   if(stack_IDLE < min_stack && !StatusDevice.stack_IDLE)
   {
      StatusDevice.stack_IDLE = 1;
      sprintf((char*)OldValue,"%u,%u,%u,%u,%u,%u,%u,%u,%u", stack_DispatcherTask, stack_GasmeterTask, stack_OPTO_ReadTask, stack_ReadWriteARC, 
                    stack_task_GSM, stack_GSM_RX_UARTTask, stack_ParsingComandsTask, stack_IDLE, xPortGetFreeHeapSize());  
      sprintf((char*)NewValue, "%u,%u,%u,%u,%u,%u,%u,%u,%u", 0,0,0,0,0,0,0,0,0);  
      WriteChangeArcRecord(CHANGE_MIN_IDLE_TASK, SOURCE_PROGRAM);
   }
   if(xPortGetFreeHeapSize() < min_stack && !StatusDevice.sizefree)
   {
      StatusDevice.sizefree = 1;
      sprintf((char*)OldValue,"%u,%u,%u,%u,%u,%u,%u,%u,%u", stack_DispatcherTask, stack_GasmeterTask, stack_OPTO_ReadTask, stack_ReadWriteARC, 
                    stack_task_GSM, stack_GSM_RX_UARTTask, stack_ParsingComandsTask, stack_IDLE, xPortGetFreeHeapSize());  
      sprintf((char*)NewValue, "%u,%u,%u,%u,%u,%u,%u,%u,%u", 0,0,0,0,0,0,0,0,0);  
      WriteChangeArcRecord(CHANGE_MIN_SIZEFREE_TASK, SOURCE_PROGRAM);
   }
}



