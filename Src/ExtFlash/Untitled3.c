////////////// DEFINE ////////////////
#define SPI_COMMS           SPI1
#define PORT_SPI_COMMS      GPIOE
//#define PIN_SPI_COMMS_NCS   GPIO_Pin_12
#define PIN_SPI_COMMS_SCK   GPIO_Pin_13
#define PIN_SPI_COMMS_MISO  GPIO_Pin_14
#define PIN_SPI_COMMS_MOSI  GPIO_Pin_15

#define SPI_WRITE_ENABLE  0x06
#define SPI_WRITE_DISABLE 0x04
#define SPI_READ_STATUS   0x05
#define SPI_ERASE_CHIP    0x60
#define SPI_WRITE_STATUS  0x01
#define SPI_GLOBAL_UNPROT 0x00
#define SPI_ERASE_4K      0x20
#define SPI_UNPROT_64K    0x39
#define SPI_WRITE         0x02
#define SPI_READ          0x03
#define SPI_LPM_ON        0xB9
#define SPI_LPM_OFF       0xAB

#define SPITXREADY  (SPI_COMMS->SR & SPI_SR_TXE)
#define SPIRXREADY  (SPI_COMMS->SR & SPI_SR_RXNE)

#define SPI_SEND(X)  SPI_COMMS->DR = X
#define SPIRXBUF     SPI_COMMS->DR
