//=============================================================================
/// \file    Archiv.c
/// \author  MVA
/// \date    13-Mar-2018
/// \brief   Functions for archive
//=============================================================================
// COPYRIGHT
//=============================================================================

#include "GlobalHandler.h"



//IntArcTypeDef*    IntArc;
//EventArcTypeDef*  EvArc;
//ChangeArcTypeDef ChArc;

//IntArcTypeDef frameArchiveInt;
//EventArcTypeDef frameArchiveEvent;
//ChangeArcTypeDef frameArchiveChange;

//ChangeArcTypeDef* ChangeArc;
//uint8_t W_Arc_buffer[65];
//uint8_t W_Event_buffer[65];

char OldValue[64];
char NewValue[64];
uint64_t EventTimeBegin;
uint64_t EventTimeEnd;
uint8_t srt_license_key[30] = {0};
//TARC_Queue ArcRec_Queue;   // ������� �������

/* ������ ������� �������� ������� */
//#pragma section = ".eeprom"


uint8_t day_arc_buf = 0;
const uint8_t IntArcLen = sizeof(IntArcTypeDef);
//const uint16_t ChangeArcLen = sizeof(ChangeArcTypeDef);
const uint8_t ChangeArcLen = sizeof(ChangeArcTypeDef); ;
const uint8_t EventArcLen = sizeof(EventSystemArcTypeDef); 
const uint8_t SystemArcLen = sizeof(EventSystemArcTypeDef); 
const uint8_t TelemetryArcLen = sizeof(EventSystemArcTypeDef); 

// const uint32_t IntArcMem_Size = IntArcMemBorder_End - IntArcMemBorder_Start; 
const uint16_t IntArcRecCount = (IntArcMemBorder_End - IntArcMemBorder_Start)/IntArcTypeDefSIZE_FLASH;

// const uint32_t DayArcMem_Size = DayArcMemBorder_End - DayArcMemBorder_Start; 
const uint16_t DayArcRecCount = (DayArcMemBorder_End - DayArcMemBorder_Start)/IntArcTypeDefSIZE_FLASH;

// const uint32_t EvArcMem_Size = EventArcMemBorder_End - EventArcMemBorder_Start; 
const uint16_t EvArcRecCount =(EventArcMemBorder_End - EventArcMemBorder_Start)/EventSystemArcTypeDefSIZE_FLASH;

// const uint32_t ChangeArcMem_Size = ChangeArcMemBorder_End - ChangeArcMemBorder_Start; 
//const uint16_t ChangeArcRecCount =(ChangeArcMemBorder_End - ChangeArcMemBorder_Start)/sizeof(ChangeArcTypeDef);
const uint16_t ChangeArcRecCount =(ChangeArcMemBorder_End - ChangeArcMemBorder_Start)/ChangeArcTypeDefSIZE_FLASH;

const uint16_t SystemArcRecCount = (SystemArcMemBorder_End - SystemArcMemBorder_Start)/EventSystemArcTypeDefSIZE_FLASH;

const uint16_t TelemetryArcRecCount = (TelemetryArcMemBorder_End - TelemetryArcMemBorder_Start)/EventSystemArcTypeDefSIZE_FLASH;

#define SECTOR_LEN 0x1000

//const uint16_t IntArcRecordPerSector = SECTOR_LEN / sizeof(IntArcTypeDef);
//const uint16_t EventArcRecordPerSector = SECTOR_LEN / sizeof(EventSystemArcTypeDef);
//const uint16_t ChangeRecordPerSector = SECTOR_LEN / sizeof(ChangeArcTypeDef);
//const uint16_t ChangeRecordPerSector = SECTOR_LEN / 128;

uint8_t IntArcRecBlok = 0;
uint16_t Day_Temp_Count = 0;

double Day_Temp_Midl =0.0; 
double Day_TempExt_Midl =0.0; 
float Day_Flow_Max =0.0;
float Day_Temp_Max =0.0;
float Day_Temp_Min =0.0;  
double Int_QgL_m3_midl = 0.0;

extern xQueueHandle xARCQueue;
extern SemaphoreHandle_t xArcRecReadyWriteSemaphore;
extern SemaphoreHandle_t xCalculateCRC32Semaphore;
//extern TypeStatus StatusDevice;
extern uint64_t   timeSystemUnix;


 

//-----------------------------------------------------------------------------
uint64_t Get_IndexTime(LL_RTC_DateTypeDef p_ArcDate, LL_RTC_TimeTypeDef p_ArcTime)
{
  struct tm lo_tm;  
  uint64_t ret;
  
  memset(&lo_tm, 0, sizeof(lo_tm));
  lo_tm.tm_hour = p_ArcTime.Hours;
  lo_tm.tm_min = p_ArcTime.Minutes;
  lo_tm.tm_sec = p_ArcTime.Seconds;  

  lo_tm.tm_year = p_ArcDate.Year + 100; // since the time has gone since 1900
  if(p_ArcDate.Month == 0)
    p_ArcDate.Month = 1;
  lo_tm.tm_mon =  p_ArcDate.Month - 1; // from 0
  if(p_ArcDate.Day == 0)
    p_ArcDate.Day = 1;
  lo_tm.tm_mday = p_ArcDate.Day;
 
  ret =  mktime(&lo_tm);
 return ret;
} 

//-----------------------------------------------------------------------------
uint8_t WriteIntEventArcRecord(uint64_t  ArcFlag, uint64_t flag_begin_end)
{
  uint16_t ret =0;
 // int32_t temp_temper_gas, temp_temper_env;
  LL_RTC_DateTypeDef lo_ArcDate;   
  LL_RTC_TimeTypeDef lo_ArcTime;  
 // uint32_t lo_CurAdress;
  portBASE_TYPE xStatus;
  TARC_Queue Queue_enumIntArc;
 // IntArc = (IntArcTypeDef*)W_Arc_buffer;
 // IntArc = &frameArchiveInt;
  event_int_story |= ArcFlag;
  GetDate_Time(&lo_ArcDate,&lo_ArcTime);
  
//  if(!(IntArcRecNo % 20)) //###
//    Eeprom_WriteDword((uint32_t)&IntArcRecNo, IntArcRecNo + 1);
  
 // temp_temper_gas = (int16_t)(Int_Temp_Midl * 100.0);
//  temp_temper_env = (int16_t)(GetExtTempMidl() * 100.0);
  
  timeSystemUnix = Get_IndexTime(lo_ArcDate,lo_ArcTime);
  Queue_enumIntArc.Arc_Buffer.IntArc.Index_Time =  timeSystemUnix;
//  Queue_enumIntArc.Arc_Buffer.IntArc.QgL_m3_midl = Int_QgL_m3_midl;
  Queue_enumIntArc.Arc_Buffer.IntArc.ArcVolume = GasMeter.VE_Lm3;
 // Queue_enumIntArc.Arc_Buffer.IntArc.ArcRecNo = IntArcRecNo;
 // Queue_enumIntArc.Arc_Buffer.IntArc.ArcRecNo = 1;
  Queue_enumIntArc.Arc_Buffer.IntArc.event = ArcFlag | flag_begin_end;
  Queue_enumIntArc.Arc_Buffer.IntArc.event_story = event_int_story;  
  if(ArcFlag == EVENT_INT_CURRENT_FLOW)
  {
     Queue_enumIntArc.Arc_Buffer.IntArc.ArcTempGas = (float)Int_Temp_Midl; //  temp_temper_gas
     Queue_enumIntArc.Arc_Buffer.IntArc.ArcTempEnv = 0; //  temp_temper_env
  }
  else
  {
     Queue_enumIntArc.Arc_Buffer.IntArc.ArcTempGas = GasMeter.var_T; //  temp_temper_gas current
     Queue_enumIntArc.Arc_Buffer.IntArc.ArcTempEnv = 0; //  temp_temper_env current
  }
  Queue_enumIntArc.Arc_Buffer.IntArc.ArcK = (uint16_t)GasMeter.mid_GasRecond;  
  Queue_enumIntArc.Arc_Buffer.IntArc.num_int = IntArcRecNo - 1;
  Queue_enumIntArc.Arc_Buffer.IntArc.status_system = StatusSystem.word;
  Queue_enumIntArc.Arc_Buffer.IntArc.status_warning = StatusWarning.word;
  Queue_enumIntArc.Arc_Buffer.IntArc.status_alarm = StatusAlarm.word;
  Queue_enumIntArc.Arc_Buffer.IntArc.status_crash = StatusCrash.word;
//  Queue_enumIntArc.Arc_Buffer.IntArc.num_change = ChangeArcRecNo - 1;
//  Queue_enumIntArc.Arc_Buffer.IntArc.num_system = SystemArcRecNo - 1;
//  Queue_enumIntArc.Arc_Buffer.IntArc.num_tel = TelemetryArcRecNo - 1;
 // Queue_enumIntArc.Arc_Buffer.IntArc.ArcStatus = *((uint32_t*)&StatusDevice);  
  
  
  //Queue_enumIntArc.Arc_Buffer.IntArc.ArcMaxQ = Int_Flow_Max;
  //Queue_enumIntArc.Arc_Buffer.IntArc.ArcMaxT = Int_Temp_Max;
  //Queue_enumIntArc.Arc_Buffer.IntArc.ArcMinT = Int_Temp_Min;  

  //Queue_enumIntArc.Arc_Buffer.IntArc.ArcError = Gl_Error.Raw;

  //Queue_enumIntArc.Arc_Buffer.IntArc.Reserv = 0;
  //Queue_enumIntArc.Arc_Buffer.IntArc.Reserv2 = 0;  

/*
  xStatus = xSemaphoreTake(xCalculateCRC32Semaphore, 5000);
  if( xStatus != pdPASS )
  {
    // ������ 
     xSemaphoreGive(xCalculateCRC32Semaphore);
     return ret;
  }
  Queue_enumIntArc.Arc_Buffer.IntArc.ArcCRC  = CalculateCRC32((uint8_t*)&Queue_enumIntArc.Arc_Buffer, IntArcLen - 4, CRC_START_STOP, 0); 
  xSemaphoreGive(xCalculateCRC32Semaphore);
  */
  if(ArcFlag == EVENT_INT_CURRENT_FLOW)
  {
     Int_Temp_Count = 0;
  //   Day_TempExt_Midl += GetExtTempMidl(); //   temp_temper_env
  //   Clear_cnt_ExtTempMidl();
     // ������� ����������� �� �����
     Day_Temp_Midl += Int_Temp_Midl; //  temp_temper_gas
     Day_Temp_Count++;
   }
  // ���-��� �� �����
  if(Day_Flow_Max < Int_Flow_Max)
     Day_Flow_Max = Int_Flow_Max;
  if(Day_Temp_Max < Int_Temp_Max)
     Day_Temp_Max = Int_Temp_Max;
  if(Day_Temp_Min  > Int_Temp_Min)
      Day_Temp_Min = Int_Temp_Min;  
    
  //  ��������� � �������� � ������� �������
   // ArcRec_Queue.Arc_Buffer = (uint8_t*)IntArc;
    Queue_enumIntArc.Arc_RW = ARC_WRITE;
    Queue_enumIntArc.Arc_Type = TYPE_ARCHIVE_HOURLY_TIME;
    Queue_enumIntArc.Arc_RecNo = IntArcRecNo;

    xStatus = xQueueSendToBack(xARCQueue, &Queue_enumIntArc, 1000);  
    if( xStatus != pdPASS )
    {
      //### ������ �������� � �������! 
    }  
    /*
    lo_CurAdress = IntArcRecNo+1;
    Eeprom_WriteDword((uint32_t)&IntArcRecNo,lo_CurAdress);
    */
    /*
    // ����-� �������, ��� ����������� ����� �������.
    xStatus = xSemaphoreTake(xArcRecReadyWriteSemaphore, 5000);
    if( xStatus != pdPASS )
    {
      //### ������  
    } 
    */
   if(!lo_ArcTime.Hours)
     flag_check_crc_dQf = 1;
 if(lo_ArcTime.Hours == GasDayBorder && ArcFlag == EVENT_INT_CURRENT_FLOW)  // ����� �������� ���
 {
   day_arc_buf = lo_ArcDate.Day;
   // ������� ����������� �� �����
   Day_Temp_Midl /=  Day_Temp_Count; 
 //  Day_TempExt_Midl /= Day_Temp_Count;
   
   Day_Temp_Count = 0;
   
 //  if(!(DayArcRecNo % 20)) //###
 //    Eeprom_WriteDword((uint32_t)&DayArcRecNo, DayArcRecNo + 1);
   
 //  Queue_enumIntArc.Arc_Buffer.IntArc.ArcRecNo = DayArcRecNo;
   Queue_enumIntArc.Arc_Buffer.IntArc.ArcRecNo = 1;
   Queue_enumIntArc.Arc_Buffer.IntArc.ArcTempGas = (float)Day_Temp_Midl;
   Queue_enumIntArc.Arc_Buffer.IntArc.ArcTempEnv = 0;//(float)Day_TempExt_Midl
 //  Day_TempExt_Midl = 0;
   Day_Temp_Midl = 0;
   Queue_enumIntArc.Arc_Buffer.IntArc.num_int = IntArcRecNo - 1;
 //  Queue_enumIntArc.Arc_Buffer.IntArc.num_change = ChangeArcRecNo - 1;
 //  Queue_enumIntArc.Arc_Buffer.IntArc.num_system = SystemArcRecNo - 1;
 //  Queue_enumIntArc.Arc_Buffer.IntArc.num_tel = TelemetryArcRecNo - 1;
   // For SMS
   frameArchiveInt.ArcRecNo = DayArcRecNo;
   frameArchiveInt.Index_Time =  timeSystemUnix;
   frameArchiveInt.ArcVolume = GasMeter.VE_Lm3;
   frameArchiveInt.ArcTempGas = (float)Day_Temp_Midl;    
   frameArchiveInt.ArcK = (uint16_t)GasMeter.mid_GasRecond;   
   frameArchiveInt.num_int = IntArcRecNo - 1;
//   frameArchiveInt.num_change = ChangeArcRecNo - 1;
 //  frameArchiveInt.num_system = SystemArcRecNo - 1;
 //  frameArchiveInt.num_tel = TelemetryArcRecNo - 1;
   frameArchiveInt.event_story = event_int_story;
   
   
//   IntArc->ArcFlag = p_ArcFlag;
 //  Queue_enumIntArc.Arc_Buffer.IntArc.ArcMaxQ = Day_Flow_Max;
 //  Queue_enumIntArc.Arc_Buffer.IntArc.ArcMaxT = Day_Temp_Max;
 //  Queue_enumIntArc.Arc_Buffer.IntArc.ArcMinT = Day_Temp_Min;  
   /*
   xStatus = xSemaphoreTake(xCalculateCRC32Semaphore, 5000);
   if( xStatus != pdPASS )
   {
    // ������ 
     xSemaphoreGive(xCalculateCRC32Semaphore);
     return ret;
   }
   Queue_enumIntArc.Arc_Buffer.IntArc.ArcCRC  = CalculateCRC32((uint8_t*)&Queue_enumIntArc.Arc_Buffer, IntArcLen - 4, CRC_START_STOP, 0); 
   xSemaphoreGive(xCalculateCRC32Semaphore);
*/

   //  ��������� � �������� � ������� �������
 //   ArcRec_Queue.Arc_Buffer = (uint8_t*)IntArc;
    Queue_enumIntArc.Arc_RW = ARC_WRITE;
    Queue_enumIntArc.Arc_Type = TYPE_ARCHIVE_DAILY_TIME;
    Queue_enumIntArc.Arc_RecNo = DayArcRecNo;

    xStatus = xQueueSendToBack(xARCQueue, &Queue_enumIntArc, 1000);  
    if( xStatus != pdPASS )
    {
      //### ������ �������� � �������! 
    }  
    /*
  lo_CurAdress = DayArcRecNo+1;
  Eeprom_WriteDword((uint32_t)&DayArcRecNo,lo_CurAdress);
    */
  /*
  // ����-� �������, ��� ����������� ����� �������.
    xStatus = xSemaphoreTake(xArcRecReadyWriteSemaphore, 5000);
    if( xStatus != pdPASS )
    {
      //### ������ �������� � �������! 
    } 
    */
  Day_Temp_Count = 0;
   
 }
// SPI_DeInitHardware();
 System_SaweBeforeReset();
 return ret;
}  

//-----------------------------------------------------------------------------
uint8_t WriteTelemetryArcRecord(uint16_t p_Event_Code, int8_t p_Event_Result, uint16_t p_Sesion_Number)
{
  uint16_t ret =0;
 // LL_RTC_DateTypeDef lo_ArcDate;   
 // LL_RTC_TimeTypeDef lo_ArcTime;  
//  uint32_t lo_CurAdress;
  portBASE_TYPE xStatus;
  TARC_Queue Queue_enumSystemArc;
  //EvArc = (EventArcTypeDef*)W_Event_buffer;
 // EvArc = &frameArchiveEvent;  

 // GetDate_Time(&lo_ArcDate,&lo_ArcTime);

 // if(!(TelemetryArcRecNo % 20)) //###
//    Eeprom_WriteDword((uint32_t)&TelemetryArcRecNo, TelemetryArcRecNo + 1);
  
 // Queue_enumSystemArc.Arc_Buffer.EventSystemArc.ArcRecNo  = TelemetryArcRecNo; 
  Queue_enumSystemArc.Arc_Buffer.EventSystemArc.ArcRecNo  = 1; 
  Queue_enumSystemArc.Arc_Buffer.EventSystemArc.TimeBegine = EventTimeBegin;
  Queue_enumSystemArc.Arc_Buffer.EventSystemArc.TimeEnd    = EventTimeEnd;  
  
  Queue_enumSystemArc.Arc_Buffer.EventSystemArc.Event_Code   = p_Event_Code;
  Queue_enumSystemArc.Arc_Buffer.EventSystemArc.Event_Result = p_Event_Result;
  Queue_enumSystemArc.Arc_Buffer.EventSystemArc.Sesion_Number = p_Sesion_Number;
  Queue_enumSystemArc.Arc_Buffer.EventSystemArc.Status = StatusSystem.word;  
  Queue_enumSystemArc.Arc_Buffer.EventSystemArc.avail_server1 = ParamServer.availability_server1;
  Queue_enumSystemArc.Arc_Buffer.EventSystemArc.avail_server2 = ParamServer.availability_server2;   
  Queue_enumSystemArc.Arc_Buffer.EventSystemArc.avail_server3 = ParamServer.availability_server3;
  /*
  xStatus = xSemaphoreTake(xCalculateCRC32Semaphore, 5000);
  if( xStatus != pdPASS )
  {
    // ������ 
     xSemaphoreGive(xCalculateCRC32Semaphore);
     return ret;
  }
  Queue_enumSystemArc.Arc_Buffer.EventSystemArc.ArcCRC =  CalculateCRC32((uint8_t*)&Queue_enumSystemArc.Arc_Buffer, TelemetryArcLen - 4, CRC_START_STOP, 0);
  xSemaphoreGive(xCalculateCRC32Semaphore);
  */
   //  ��������� � �������� � ������� �������
//  ArcRec_Queue.Arc_Buffer = (uint8_t*)EvArc;
  Queue_enumSystemArc.Arc_RW = ARC_WRITE;
  Queue_enumSystemArc.Arc_Type = TYPE_ARCHIVE_TELEMETRY_NUMBER;
  Queue_enumSystemArc.Arc_RecNo = TelemetryArcRecNo;

    xStatus = xQueueSendToBack(xARCQueue, &Queue_enumSystemArc, 5000);  
    if( xStatus != pdPASS )
    {
     // ������ �������� � �������! 
    }  
    /*
  lo_CurAdress = TelemetryArcRecNo + 1;
  Eeprom_WriteDword((uint32_t)&TelemetryArcRecNo, lo_CurAdress);
    */
  // ����-� �������, ��� ����������� ����� �������.
//    xSemaphoreTake(xArcRecReadyWriteSemaphore, 5000);
  return ret;
}
//-----------------------------------------------------------------------------
uint8_t WriteSystemArcRecord(uint16_t p_Event_Code, uint16_t p_Sesion_Number)
{
  uint16_t ret =0;
//  LL_RTC_DateTypeDef lo_ArcDate;   
//  LL_RTC_TimeTypeDef lo_ArcTime;  
//  uint32_t lo_CurAdress;
  portBASE_TYPE xStatus;
  TARC_Queue Queue_enumEventArc; 

 // GetDate_Time(&lo_ArcDate,&lo_ArcTime);
  
 // if(!(SystemArcRecNo % 20)) //###
 //    Eeprom_WriteDword((uint32_t)&SystemArcRecNo, SystemArcRecNo + 1);
  
 // Queue_enumEventArc.Arc_Buffer.EventSystemArc.ArcRecNo  = SystemArcRecNo; 
  Queue_enumEventArc.Arc_Buffer.EventSystemArc.ArcRecNo  = 1; 
  Queue_enumEventArc.Arc_Buffer.EventSystemArc.TimeBegine = timeSystemUnix;
  Queue_enumEventArc.Arc_Buffer.EventSystemArc.TimeEnd    = timeSystemUnix;  
  
  Queue_enumEventArc.Arc_Buffer.EventSystemArc.Event_Code   = p_Event_Code;
  Queue_enumEventArc.Arc_Buffer.EventSystemArc.Event_Result = 0;
  Queue_enumEventArc.Arc_Buffer.EventSystemArc.Sesion_Number = p_Sesion_Number;
  Queue_enumEventArc.Arc_Buffer.EventSystemArc.Status = StatusSystem.word;  
  /*
  xStatus = xSemaphoreTake(xCalculateCRC32Semaphore, 5000);
  if( xStatus != pdPASS )
  {
    // ������ 
     xSemaphoreGive(xCalculateCRC32Semaphore);
     return ret;
  }
  Queue_enumEventArc.Arc_Buffer.EventSystemArc.ArcCRC =  CalculateCRC32((uint8_t*)&Queue_enumEventArc.Arc_Buffer, EventArcLen - 4, CRC_START_STOP, 0);
  xSemaphoreGive(xCalculateCRC32Semaphore);
  */
   //  ��������� � �������� � ������� �������
//  ArcRec_Queue.Arc_Buffer = (uint8_t*)EvArc;
  Queue_enumEventArc.Arc_RW = ARC_WRITE;
  Queue_enumEventArc.Arc_Type = TYPE_ARCHIVE_SYSTEM_NUMBER;
  Queue_enumEventArc.Arc_RecNo = SystemArcRecNo;

    xStatus = xQueueSendToBack(xARCQueue, &Queue_enumEventArc, 5000);  
    if( xStatus != pdPASS )
    {
     // ������ �������� � �������! 
    }  
    /*
  lo_CurAdress = SystemArcRecNo + 1;
  Eeprom_WriteDword((uint32_t)&SystemArcRecNo, lo_CurAdress);
    */
  // ����-� �������, ��� ����������� ����� �������.
//    xSemaphoreTake(xArcRecReadyWriteSemaphore, 5000);
  return ret;
}

//-----------------------------------------------------------------------------
uint8_t WriteChangeArcRecord(TCHANG_Type p_Parametr, uint8_t _ID_Change)   
{
  uint16_t ret =0;
  TARC_Queue lo_ArcRec_Queue;
 // uint32_t lo_CurAdress;
  portBASE_TYPE xStatus;
  
  lo_ArcRec_Queue.Arc_Buffer.ChangeArc.Index_Time = timeSystemUnix;
  
  if(CLB_lock == 0x55 || CLB_lock == 0x66)
     lo_ArcRec_Queue.Arc_Buffer.ChangeArc.ArcCLB_Lock = 1;  
  else
     lo_ArcRec_Queue.Arc_Buffer.ChangeArc.ArcCLB_Lock = 0;
  lo_ArcRec_Queue.Arc_Buffer.ChangeArc.lock_manufacture = StatusDevice.password_manufacture_enter;
  lo_ArcRec_Queue.Arc_Buffer.ChangeArc.lock_provider = StatusDevice.password_provider_enter;
  
  strcpy((char*)lo_ArcRec_Queue.Arc_Buffer.ChangeArc.OldValue, OldValue);
  strcpy((char*)lo_ArcRec_Queue.Arc_Buffer.ChangeArc.NewValue, NewValue);
  
//  if(!(ChangeArcRecNo % 20)) //###
 //   Eeprom_WriteDword((uint32_t)&ChangeArcRecNo, ChangeArcRecNo + 1);
  
  lo_ArcRec_Queue.Arc_Buffer.ChangeArc.status_cover = StatusCrash.bit.body_cover_open;
//  lo_ArcRec_Queue.Arc_Buffer.ChangeArc.ArcRecNo = ChangeArcRecNo;
//  lo_ArcRec_Queue.Arc_Buffer.ChangeArc.ArcRecNo = ChangeArcRecNo;  
  lo_ArcRec_Queue.Arc_Buffer.ChangeArc.ID_Param = (uint8_t) p_Parametr;
  lo_ArcRec_Queue.Arc_Buffer.ChangeArc.ID_Change = _ID_Change;
  if(strlen((char*)srt_license_key) == 0 && _ID_Change == CHANGE_PROGRAM)
    strcpy((char*)srt_license_key, "1234567890");
  strcpy((char*)lo_ArcRec_Queue.Arc_Buffer.ChangeArc.license_key, (char*)srt_license_key);
  /*
  xStatus = xSemaphoreTake(xCalculateCRC32Semaphore, 5000);
  if( xStatus != pdPASS )
  {
    // ������ 
     xSemaphoreGive(xCalculateCRC32Semaphore);
     return ret;
  }
  lo_ArcRec_Queue.Arc_Buffer.ChangeArc.ArcCRC =  CalculateCRC32((uint8_t*)&lo_ArcRec_Queue.Arc_Buffer, ChangeArcLen - 4, CRC_START_STOP, 0);
  xSemaphoreGive(xCalculateCRC32Semaphore);
  */
  // ��������� � �������� � ������� �������

  lo_ArcRec_Queue.Arc_RW = ARC_WRITE;
  lo_ArcRec_Queue.Arc_Type = TYPE_ARCHIVE_CHANG_NUMBER;
  lo_ArcRec_Queue.Arc_RecNo = ChangeArcRecNo;

  xStatus = xQueueSendToBack(xARCQueue, &lo_ArcRec_Queue, 1000);  
  if( xStatus != pdPASS )
  {
   // ������ �������� � �������! 
    __NOP();
  }  
  /*
  lo_CurAdress = ChangeArcRecNo+1;
  Eeprom_WriteDword((uint32_t)&ChangeArcRecNo, lo_CurAdress);
*/
  return ret;
}

/*!
Read record without compare through queue
\param[in] archiv_type ��� ������
\param[in] n_record ����� �������� ������
\param[out] *ptr_out ��������� �� ����� ���� ����������
\return the result of the operation
*/
uint8_t ReadArchiveQueue(TArchiv_type archiv_type, 
                    uint32_t     n_record,
                    uint64_t     label_begin,
                    uint64_t     label_end,
                    uint8_t      *ptr_out)
{
    TARC_Queue lo_ArcRec_Queue;
    portBASE_TYPE xStatus;
    uint8_t      type_label = LABEL_DATE;
    
    if(label_begin == 0 && label_end == 0)
       type_label = LABEL_NUMBER;
    
    lo_ArcRec_Queue.Arc_RW = ARC_READ;
    lo_ArcRec_Queue.Arc_Type = archiv_type;
    lo_ArcRec_Queue.Arc_RecNo = n_record;
    lo_ArcRec_Queue.Arc_Buffer.ptr_read = (uint8_t*)&lo_ArcRec_Queue.Arc_Buffer;
    xStatus = xQueueSendToBack(xARCQueue, &lo_ArcRec_Queue, 5000);  
    if( xStatus != pdPASS )
    {
      // ������ �������� � �������! 
      return ARC_READ_STATUS_TIME_FAIL_ERROR;
    }
    
    // ����-� �������, ��� ����������� ����� ��������.
    xStatus = xSemaphoreTake(xArcRecReadyWriteSemaphore, 5000);
    if( xStatus != pdPASS )
    {
      // ������ 
       return ARC_READ_STATUS_TIME_FAIL_ERROR;
    }
    //-------------------------------------------------------
    if(type_label == LABEL_DATE)
    {
       if(lo_ArcRec_Queue.Arc_Buffer.NoAndTimeArcType.Index_Time < label_begin
          || lo_ArcRec_Queue.Arc_Buffer.NoAndTimeArcType.Index_Time > label_end)
          return ARC_READ_STATUS_NOT_RANGE_ERROR;
    }

    //-------------------------------------------------------
    uint16_t size_arh;
    uint32_t crc32_rez;
    uint32_t *ptr_crc32;
    switch(archiv_type)
    {
    case TYPE_ARCHIVE_HOURLY_TIME:
    case TYPE_ARCHIVE_HOURLY_NUMBER:
    case TYPE_ARCHIVE_DAILY_TIME:
    case TYPE_ARCHIVE_DAILY_NUMBER:      
         size_arh = IntArcLen;
         break;
    case TYPE_ARCHIVE_SYSTEM_NUMBER:     
    case TYPE_ARCHIVE_SYSTEM_TIME:
         size_arh = SystemArcLen;
         break; 
    case TYPE_ARCHIVE_TELEMETRY_TIME:
    case TYPE_ARCHIVE_TELEMETRY_NUMBER:      
         size_arh = TelemetryArcLen;
         break;    
    case TYPE_ARCHIVE_CHANG_NUMBER:
    case TYPE_ARCHIVE_CHANG_TIME:     
         size_arh = ChangeArcLen;
         break;
    default:break;
    }
    ptr_crc32 = (uint32_t*)((uint8_t*)&lo_ArcRec_Queue.Arc_Buffer + size_arh - 4);
    xStatus = xSemaphoreTake(xCalculateCRC32Semaphore, 5000);
    if( xStatus != pdPASS )
    {
    // ������ 
       xSemaphoreGive(xCalculateCRC32Semaphore);
       return ARC_READ_STATUS_CRC_ERROR;
    }
    crc32_rez = CalculateCRC32((uint8_t*)&lo_ArcRec_Queue.Arc_Buffer, size_arh - 4, CRC_START_STOP, 0);
    xSemaphoreGive(xCalculateCRC32Semaphore);
    if(*ptr_crc32 != crc32_rez)
 //   {
       return ARC_READ_STATUS_CRC_ERROR;
 //      ReadArchiveParsing(&lo_ArcRec_Queue.Arc_Buffer, archiv_type, ptr_out, lo_ArcRec_Queue.Arc_RecNo);
 //   }
 //   else
       ReadArchiveParsing(&lo_ArcRec_Queue.Arc_Buffer, archiv_type, ptr_out, 0);
    
    return ARC_READ_STATUS_IN_RANGE_OK;
}
//QT style, auto-documenting with Doxygen, https://habr.com/ru/post/252101/
/*! 
Read record without compare through queue
\param[in] archiv_type ��� ������
\param[in] n_record ����� �������� ������
\param[out] *ptr_out ��������� �� ����� ���� ����������
\param[in] numberArcCRC_ERROR 0 ���� CRC ���������, ����� �������� ������ ���� �� ���������
\return void
*/
void ReadArchiveParsing(enumArcTypeDef *ptr_Arc_Buffer, TArchiv_type archiv_type, uint8_t *ptr_out, uint32_t numberArcCRC_ERROR)
{
   struct tm lo_tm, lo_tm_end;
   memcpy(&lo_tm, _gmtime_((time_t*)&ptr_Arc_Buffer->NoAndTimeArcType.Index_Time), sizeof(lo_tm));
   if(archiv_type == TYPE_ARCHIVE_TELEMETRY_TIME || archiv_type == TYPE_ARCHIVE_TELEMETRY_NUMBER)
      memcpy(&lo_tm_end, _gmtime_((time_t*)&ptr_Arc_Buffer->NoAndTimeArcType.Index_Time_end), sizeof(lo_tm_end));
  // float temp_temper_gas, temp_temper_env;
 //  temp_temper_gas = (float)ptr_Arc_Buffer->IntArc.ArcTempGas / 100;
 //  temp_temper_env = (float)ptr_Arc_Buffer->IntArc.ArcTempEnv / 100;
   switch(archiv_type)
   {
   case TYPE_ARCHIVE_HOURLY_TIME:
   case TYPE_ARCHIVE_HOURLY_NUMBER: 
        if(!ptr_Arc_Buffer->IntArc.event)
          lo_tm.tm_sec = 0;
        sprintf((char *)ptr_out,
                "%d;%02d.%02d.%d,%02d:%02d:%02d;%llu;%0.2f;%0.2f;%d;", 
                ptr_Arc_Buffer->IntArc.ArcRecNo,    
                lo_tm.tm_mday, lo_tm.tm_mon + 1, lo_tm.tm_year + 1900, 
                lo_tm.tm_hour, lo_tm.tm_min, lo_tm.tm_sec,
                (uint64_t)(ptr_Arc_Buffer->IntArc.ArcVolume * 10000),
                ptr_Arc_Buffer->IntArc.ArcTempGas, 
                ptr_Arc_Buffer->IntArc.ArcTempEnv, 
                ptr_Arc_Buffer->IntArc.ArcK
                );
        sprintf((char *)ptr_out + strlen((char*)ptr_out),
                "%X;%X;%X;%X;%016llX\r\n", 
                ptr_Arc_Buffer->IntArc.status_system,   
                ptr_Arc_Buffer->IntArc.status_warning, 
                ptr_Arc_Buffer->IntArc.status_alarm, 
                ptr_Arc_Buffer->IntArc.status_crash, 
                ptr_Arc_Buffer->IntArc.event 
                );
        break;
   case TYPE_ARCHIVE_DAILY_TIME:
   case TYPE_ARCHIVE_DAILY_NUMBER: 
        sprintf((char *)ptr_out,
                "%d;%02d.%02d.%d,%02d:00:00;%llu;%0.2f;%0.2f;%d;", 
                ptr_Arc_Buffer->IntArc.ArcRecNo,    
                lo_tm.tm_mday, lo_tm.tm_mon + 1, lo_tm.tm_year + 1900, 
                lo_tm.tm_hour, 
                (uint64_t)(ptr_Arc_Buffer->IntArc.ArcVolume * 10000),
                ptr_Arc_Buffer->IntArc.ArcTempGas, 
                ptr_Arc_Buffer->IntArc.ArcTempEnv,
                ptr_Arc_Buffer->IntArc.ArcK            
                );
        sprintf((char *)ptr_out + strlen((char*)ptr_out),
                "%X;%X;%X;%X;%d\r\n", 
                ptr_Arc_Buffer->IntArc.status_system,   
                ptr_Arc_Buffer->IntArc.status_warning, 
                ptr_Arc_Buffer->IntArc.status_alarm, 
                ptr_Arc_Buffer->IntArc.status_crash,                
                ptr_Arc_Buffer->IntArc.num_int  
                );
        break;
  // case TYPE_ARCHIVE_EVENT_NUMBER:
   case TYPE_ARCHIVE_SYSTEM_NUMBER:     
   case TYPE_ARCHIVE_SYSTEM_TIME:
 //  case TYPE_ARCHIVE_EVENT_TIME:       
        sprintf((char *)ptr_out,
                "%d;%02d.%02d.%04d,%02d:%02d:%02d;%08X\r\n", 
                ptr_Arc_Buffer->EventSystemArc.ArcRecNo,                 
                lo_tm.tm_mday, lo_tm.tm_mon + 1, lo_tm.tm_year + 1900, 
                lo_tm.tm_hour, lo_tm.tm_min, lo_tm.tm_sec,           
                ptr_Arc_Buffer->EventSystemArc.Status
                );
        break;
   case TYPE_ARCHIVE_TELEMETRY_TIME:
   case TYPE_ARCHIVE_TELEMETRY_NUMBER:      
        sprintf((char *)ptr_out,
                "%d;%02d.%02d.%04d,%02d:%02d:%02d;%02d.%02d.%04d,%02d:%02d:%02d;", 
                ptr_Arc_Buffer->EventSystemArc.ArcRecNo,                 
                lo_tm.tm_mday, lo_tm.tm_mon + 1, lo_tm.tm_year + 1900, 
                lo_tm.tm_hour, lo_tm.tm_min, lo_tm.tm_sec,   
                lo_tm_end.tm_mday, lo_tm_end.tm_mon + 1, lo_tm_end.tm_year + 1900, 
                lo_tm_end.tm_hour, lo_tm_end.tm_min, lo_tm_end.tm_sec
                );
        sprintf((char *)ptr_out + strlen((char*)ptr_out),
                "%d;%d;%d;%d;%d;%d;%08X\r\n",
                ptr_Arc_Buffer->EventSystemArc.Event_Code,
                ptr_Arc_Buffer->EventSystemArc.avail_server1,  
                ptr_Arc_Buffer->EventSystemArc.avail_server2,              
                ptr_Arc_Buffer->EventSystemArc.avail_server3,            
                ptr_Arc_Buffer->EventSystemArc.Event_Result,
                ptr_Arc_Buffer->EventSystemArc.Sesion_Number, //
                ptr_Arc_Buffer->EventSystemArc.Status 
                );               
        
        break;    
        
   case TYPE_ARCHIVE_CHANG_NUMBER:
   case TYPE_ARCHIVE_CHANG_TIME:    
   {
        uint32_t len1, len2;
        len1 = strlen(ptr_Arc_Buffer->ChangeArc.OldValue);
        len2 = strlen(ptr_Arc_Buffer->ChangeArc.NewValue);
        if((len1 + len2) < (BUF_MESSAGE_GSM_MODULE_SIZE - 40))
           sprintf((char *)ptr_out,
                "%d;%02d.%02d.%04d,%02d:%02d:%02d;%d;%d;%d;%d;%d;%s;%s;%s;%d\r\n", 
                ptr_Arc_Buffer->ChangeArc.ArcRecNo,                 
                lo_tm.tm_mday, lo_tm.tm_mon + 1, lo_tm.tm_year + 1900, 
                lo_tm.tm_hour, lo_tm.tm_min, lo_tm.tm_sec,
                ptr_Arc_Buffer->ChangeArc.ArcCLB_Lock,
                ptr_Arc_Buffer->ChangeArc.lock_manufacture,
                ptr_Arc_Buffer->ChangeArc.lock_provider,
                ptr_Arc_Buffer->ChangeArc.ID_Change,
                ptr_Arc_Buffer->ChangeArc.ID_Param, //
                ptr_Arc_Buffer->ChangeArc.OldValue,
                ptr_Arc_Buffer->ChangeArc.NewValue,
                ptr_Arc_Buffer->ChangeArc.license_key,
                ptr_Arc_Buffer->ChangeArc.status_cover
                );       
   }
        break;
   default:break;
   }
   if(numberArcCRC_ERROR)
     strcat((char*)ptr_out + strlen((char*)ptr_out) - 2, ";CRC_ERROR\r\n");
}

/*!
Read record without compare through queue
\param[in] archiv_type ��� ������
\param[in] n_record ����� �������� ������
\param[out] *ptr_out ��������� �� ����� ���� ����������
\return the result of the operation
*/         
uint8_t TransmitARCHIVE(uint8_t *ptr_buf_in, uint8_t *ptr_buf_out, TTempDataParsingCommand *ptr_TempDataParsingCommand)
{
   char *end_ptr, *beg_ptr;
   uint8_t rez = _OK;
   ptr_TempDataParsingCommand->flag_message_of_patrs = MESSAGE_PARTS;
   ++ptr_TempDataParsingCommand->number_frame;
   if(ptr_TempDataParsingCommand->number_frame == 1)
   {
     if(CheckChrString((uint8_t*)ptr_buf_in, "%d();.:,", strlen((char*)ptr_buf_in)) == _FALSE) 
     {
       //strcpy((char*)ptr_out_buf, reply_command[COMMAND_INCORRECT_VALUE]); ptr_TempDataParsingCommand->flag_end = _OK; ptr_TempDataParsingCommand->flag_message_of_patrs = _OK; return;  }  
        rez = _FALSE;
     }
     else
     {
        //flag_detection_archive = (TArchiv_type)strtoul((char*)(ptr_buf_in + 1), &end_ptr, 10);
        ptr_TempDataParsingCommand->flag_detection_archive = (TArchiv_type)strtoul((char*)(ptr_buf_in + 1), &end_ptr, 10);
        //numFindArcBeg = 1;
        ptr_TempDataParsingCommand->numBeg = 1;
        switch(ptr_TempDataParsingCommand->flag_detection_archive)
        {
        case  TYPE_ARCHIVE_HOURLY_TIME:
        case  TYPE_ARCHIVE_HOURLY_NUMBER:       
              if(IntArcRecNo > 9984 )
              ptr_TempDataParsingCommand->numBeg = IntArcRecNo - 9984;
              ptr_TempDataParsingCommand->numEnd = IntArcRecNo - 1;
              break;
        case  TYPE_ARCHIVE_DAILY_TIME:
        case  TYPE_ARCHIVE_DAILY_NUMBER:       
              if(DayArcRecNo > 2496 )
                 ptr_TempDataParsingCommand->numBeg = DayArcRecNo - 2496;
              ptr_TempDataParsingCommand->numEnd = DayArcRecNo - 1;
              break;
        case  TYPE_ARCHIVE_CHANG_TIME:
        case  TYPE_ARCHIVE_CHANG_NUMBER:       
              if(ChangeArcRecNo > 1088 )
                 ptr_TempDataParsingCommand->numBeg = ChangeArcRecNo - 1088;
              ptr_TempDataParsingCommand->numEnd = ChangeArcRecNo - 1;
              break;
        case  TYPE_ARCHIVE_TELEMETRY_TIME:
        case  TYPE_ARCHIVE_TELEMETRY_NUMBER:       
              if(TelemetryArcRecNo > 11456 )
                 ptr_TempDataParsingCommand->numBeg = TelemetryArcRecNo - 11456;
              ptr_TempDataParsingCommand->numEnd = TelemetryArcRecNo - 1;
              break;       
        case  TYPE_ARCHIVE_SYSTEM_TIME:
        case  TYPE_ARCHIVE_SYSTEM_NUMBER:       
              if(SystemArcRecNo > 1088 )
                 ptr_TempDataParsingCommand->numBeg = SystemArcRecNo - 1088;
              ptr_TempDataParsingCommand->numEnd = SystemArcRecNo - 1;
              break;           
           
        default:
              rez = _FALSE;
              break;
        }     
     
        if(ptr_TempDataParsingCommand->flag_detection_archive == TYPE_ARCHIVE_HOURLY_TIME 
           || ptr_TempDataParsingCommand->flag_detection_archive == TYPE_ARCHIVE_DAILY_TIME
           || ptr_TempDataParsingCommand->flag_detection_archive == TYPE_ARCHIVE_CHANG_TIME
           || ptr_TempDataParsingCommand->flag_detection_archive == TYPE_ARCHIVE_TELEMETRY_TIME
           || ptr_TempDataParsingCommand->flag_detection_archive == TYPE_ARCHIVE_SYSTEM_TIME)
       {
           LL_RTC_DateTypeDef lo_ArcDate;   
           LL_RTC_TimeTypeDef lo_ArcTime;
           if(str_fing_chr((char*)ptr_buf_in, ':', 1000) == 4 && str_fing_chr((char*)ptr_buf_in, '.', 1000) == 4)
           {
              ptr_buf_in = (uint8_t*)strchr((char*)ptr_buf_in, ';') + 1;
              lo_ArcDate.Day = strtoul((char*)ptr_buf_in, &end_ptr, 10); ptr_buf_in = (uint8_t*) end_ptr + 1;
              lo_ArcDate.Month = strtoul((char*)ptr_buf_in, &end_ptr, 10); ptr_buf_in = (uint8_t*)end_ptr + 1;
              lo_ArcDate.Year = strtoul((char*)ptr_buf_in, &end_ptr, 10); ptr_buf_in = (uint8_t*)end_ptr + 1;
              lo_ArcTime.Hours = strtoul((char*)ptr_buf_in, &end_ptr, 10); ptr_buf_in = (uint8_t*)end_ptr + 1;
              lo_ArcTime.Minutes = strtoul((char*)ptr_buf_in, &end_ptr, 10); ptr_buf_in = (uint8_t*)end_ptr + 1;
              lo_ArcTime.Seconds = strtoul((char*)ptr_buf_in, &end_ptr, 10); ptr_buf_in = (uint8_t*)end_ptr + 1;     
              //TimeFindUnixBegin = Get_IndexTime(lo_ArcDate, lo_ArcTime);
              ptr_TempDataParsingCommand->TimeFindUnixBegin = Get_IndexTime(lo_ArcDate, lo_ArcTime);
 
              lo_ArcDate.Day = strtoul((char*)ptr_buf_in, &end_ptr, 10); ptr_buf_in = (uint8_t*)end_ptr + 1;
              lo_ArcDate.Month = strtoul((char*)ptr_buf_in, &end_ptr, 10); ptr_buf_in = (uint8_t*)end_ptr + 1;
              lo_ArcDate.Year = strtoul((char*)ptr_buf_in, &end_ptr, 10); ptr_buf_in = (uint8_t*)end_ptr + 1;
              lo_ArcTime.Hours = strtoul((char*)ptr_buf_in, &end_ptr, 10); ptr_buf_in = (uint8_t*)end_ptr + 1;
              lo_ArcTime.Minutes = strtoul((char*)ptr_buf_in, &end_ptr, 10); ptr_buf_in = (uint8_t*)end_ptr + 1;
              lo_ArcTime.Seconds = strtoul((char*)ptr_buf_in, &end_ptr, 10); ptr_buf_in = (uint8_t*)end_ptr + 1;    
              //TimeFindUnixEnd = Get_IndexTime(lo_ArcDate, lo_ArcTime);
              ptr_TempDataParsingCommand->TimeFindUnixEnd = Get_IndexTime(lo_ArcDate, lo_ArcTime);
           }
           else
             rez = _FALSE;
           

        }
        else
        {// old type archive
         uint32_t numFindArcEnd_buf, numFindArcBeg_buf;
         
       // ptr_buf_in = (uint8_t*)(strchr((char*)ptr_buf_in, ';') + 1);
         ptr_buf_in = (uint8_t*)strchr((char*)ptr_buf_in, ';') + 1;
         //numFindArcBeg = strtoul((char*)ptr_buf_in, &end_ptr, 10);
         numFindArcBeg_buf = strtoul((char*)ptr_buf_in, &end_ptr, 10);
         if(numFindArcBeg_buf == 0)
           numFindArcBeg_buf = 1;
         beg_ptr = end_ptr + 1;
         numFindArcEnd_buf = strtoul(beg_ptr, &end_ptr, 10);
         if(numFindArcEnd_buf == 0)
          numFindArcEnd_buf = 1;
         
         if(numFindArcBeg_buf > ptr_TempDataParsingCommand->numBeg)
           ptr_TempDataParsingCommand->numBeg = numFindArcBeg_buf;
         if(numFindArcEnd_buf < ptr_TempDataParsingCommand->numEnd)
           ptr_TempDataParsingCommand->numEnd = numFindArcEnd_buf;
         
         //TimeFindUnixBegin = 0;
         ptr_TempDataParsingCommand->TimeFindUnixBegin = 0;
         //TimeFindUnixEnd = 0;
         ptr_TempDataParsingCommand->TimeFindUnixEnd = 0;
        }
        memset((char*)ptr_buf_out, 0, ptr_TempDataParsingCommand->size_bufer);
        *ptr_buf_out = '{';
      //  resultReply.end_part_message = _FALSE;
      //  resultReply.end_message = _FALSE;
        ptr_TempDataParsingCommand->flag_message_of_patrs_end = _FALSE;
        ptr_TempDataParsingCommand->flag_end = _FALSE;
     
      //  flag_ban_Receive_485 = 1;
     //  return resultReply;
     }
   }
   //----------------------------------------------------------------------------------------------
   if(ptr_TempDataParsingCommand->number_frame > 1)
   {
      uint8_t result;
      uint16_t strlen_buf;
      
      while(1)
      {
         memset((char*)ptr_buf_out, 0, ptr_TempDataParsingCommand->size_bufer);
         //if(numFindArcBeg <= numFindArcEnd)
         if(ptr_TempDataParsingCommand->numBeg <= ptr_TempDataParsingCommand->numEnd)
            //result = ReadArchiveQueue(flag_detection_archive, numFindArcBeg, TimeFindUnixBegin, TimeFindUnixEnd, ptr_buf_out);
            result = ReadArchiveQueue((TArchiv_type)ptr_TempDataParsingCommand->flag_detection_archive, ptr_TempDataParsingCommand->numBeg, ptr_TempDataParsingCommand->TimeFindUnixBegin, ptr_TempDataParsingCommand->TimeFindUnixEnd, ptr_buf_out);
         else
         {
            result = ARC_READ_STATUS_END_FIND;
            break;
         }
        // ++numFindArcBeg;
         ++ptr_TempDataParsingCommand->numBeg;
         //--------------------------------------------------------------------------      
         if(result == ARC_READ_STATUS_TIME_FAIL_ERROR)
         {
           result = ARC_READ_STATUS_END_FIND;
           break;
         }
         //--------------------------------------------------------------------------
         if(result == ARC_READ_STATUS_IN_RANGE_OK
          //  || (TimeFindUnixBegin == 0 && TimeFindUnixEnd == 0 && result == ARC_READ_STATUS_CRC_ERROR))
              || (ptr_TempDataParsingCommand->TimeFindUnixBegin == 0 && ptr_TempDataParsingCommand->TimeFindUnixEnd == 0 && result == ARC_READ_STATUS_CRC_ERROR))
         {
            strlen_buf = strlen((char*)ptr_buf_out);
          //  if(strlen_buf < GSM_Get_data_size_left())
            
            if(strlen_buf < ptr_TempDataParsingCommand->size_bufer_left)
            {
               //resultReply.end_part_message = _FALSE;
               //resultReply.end_message = _FALSE;
               ptr_TempDataParsingCommand->flag_message_of_patrs_end = _FALSE;
               ptr_TempDataParsingCommand->flag_end = _FALSE;
            }
            else
            {
             //  resultReply.end_part_message = _OK;
             //  resultReply.end_message = _FALSE;
                ptr_TempDataParsingCommand->flag_message_of_patrs_end = _OK;
                ptr_TempDataParsingCommand->flag_end = _FALSE;
               memset((char*)ptr_buf_out, 0, ptr_TempDataParsingCommand->size_bufer);
              // --numFindArcBeg;
               --ptr_TempDataParsingCommand->numBeg;
            }
            break;
         }
      }        
      //--------------------------------------------------------------------------
      if(result == ARC_READ_STATUS_END_FIND)
      {
         strcat((char*)ptr_buf_out, "}");
        // resultReply.end_part_message = _OK;
        // resultReply.end_message = _OK;
         ptr_TempDataParsingCommand->flag_message_of_patrs_end = _OK;
         ptr_TempDataParsingCommand->flag_end = _OK;
      }    
   }
   return rez; 
}