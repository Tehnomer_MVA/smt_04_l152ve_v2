//=============================================================================
// 
//=============================================================================
/// \file    Io.c
/// \author  SUL
/// \date    08-Dec-2014
/// \brief   Module for Io (jumpers and switch on Gasmeter Reference Design)
//=============================================================================

//=============================================================================

#include "GlobalHandler.h"


void startOutputTimer(uint16_t pPeriod);
void stopOutputTimer(void);


uint16_t Pulse;
uint8_t OptoPort = OPTO_PORT_DISABLE;
uint32_t PressKeyDelay = 0, LockPressKeyDelay =0;
TKeyPressed KeyPressed = KEY_PRESSED_NOT, LockKeyState = KEY_PRESSED_NOT;

// ������������� ����� - ������ ������, �������� �������� �� 55 - ������  
uint8_t CLB_lock; 


// ----------------------------------------------------------------------------
// Init OptoSwitch and Key 
void Io_InitHardware(void)
{
  //Power off for Extern Flash and sensor
  // PORT C

  SET_BIT(RCC->AHBENR,RCC_AHBENR_GPIOAEN);
  SET_BIT(RCC->AHBENR,RCC_AHBENR_GPIOBEN);    
  SET_BIT(RCC->AHBENR,RCC_AHBENR_GPIOCEN);  
  SET_BIT(RCC->AHBENR,RCC_AHBENR_GPIODEN);  
  SET_BIT(RCC->AHBENR,RCC_AHBENR_GPIOEEN);  
  SET_BIT(RCC->AHBENR,RCC_AHBENR_GPIOHEN); 
  
  GPIOA->MODER = 0xEBFFFFFF;
  GPIOB->MODER = 0xFFFFFFFF;
  GPIOC->MODER = 0xFFFFFFFF;
  GPIOD->MODER = 0xFFFFFFFF;
  GPIOE->MODER = 0xFFFFFFFF;
  GPIOH->MODER = 0xF; 
  
  MODIFY_REG(GPIOC->MODER, GPIO_MODER_MODER6, GPIO_MODER_MODER6_0); // FLASH ON   
  MODIFY_REG(GPIOC->MODER, GPIO_MODER_MODER9, GPIO_MODER_MODER9_0); // SENS_PWR_ON
 // MODIFY_REG(GPIOC->MODER, GPIO_MODER_MODE7, GPIO_MODER_MODE7_0); // PWR_BAT2 
 
  SET_BIT(GPIOC->ODR,GPIO_ODR_ODR_9);  // Sensor Power OFF
  SET_BIT(GPIOC->ODR,GPIO_ODR_ODR_6);  // Power Flash OFF

//  CLEAR_BIT(GPIOA->MODER,GPIO_MODER_MODE5);
     
  // Key1 is GPIO PC13, configure as input with pullup
  CLEAR_BIT(GPIOC->MODER, GPIO_MODER_MODER13);
  CLEAR_BIT(GPIOC->PUPDR, GPIO_PUPDR_PUPDR13_0);
  
  // Redcontact (OPTO _ON) is GPIO PC12, configure as input with pullup
  CLEAR_BIT(GPIOC->MODER, GPIO_MODER_MODER12);
  CLEAR_BIT(GPIOC->PUPDR, GPIO_PUPDR_PUPDR12_0);  
  
  CLEAR_BIT(GPIOB->MODER, GPIO_MODER_MODER9); // sim card
  CLEAR_BIT(GPIOD->MODER, GPIO_MODER_MODER3); // status modem
  // CalibrLock - PB2
  CLEAR_BIT(GPIOB->MODER, GPIO_MODER_MODER2);
  SET_BIT(GPIOB->PUPDR, GPIO_PUPDR_PUPDR2_0);

    // batary telemetry
  CLEAR_BIT(GPIOE->MODER, GPIO_MODER_MODER6);
  CLEAR_BIT(GPIOE->PUPDR, GPIO_PUPDR_PUPDR6);

#ifdef E7
      // body cover
  CLEAR_BIT(GPIOC->MODER, GPIO_MODER_MODE8);
  CLEAR_BIT(GPIOC->PUPDR, GPIO_PUPDR_PUPD8);
  //### CLEAR_BIT(GPIOC->MODER, GPIO_MODER_MODE7);
  //### CLEAR_BIT(GPIOC->PUPDR, GPIO_PUPDR_PUPD7);
#endif  
  
   // PD2 - input SGM_PWR_PG
  SET_BIT(RCC->AHBENR,RCC_AHBENR_GPIODEN); 
  CLEAR_BIT(GPIOD->MODER, GPIO_MODER_MODER2);  
  CLEAR_BIT(GPIOD->PUPDR, GPIO_PUPDR_PUPDR2);    
   // PD1 SGM_PWR_EN
  MODIFY_REG(GPIOD->MODER, GPIO_MODER_MODER1,GPIO_MODER_MODER1_0);  
  MODIFY_REG(GPIOD->PUPDR, GPIO_PUPDR_PUPDR1,GPIO_PUPDR_PUPDR1_1);
  CLEAR_BIT(GPIOD->ODR, GPIO_ODR_ODR_1);
   //PD0 MCU_GSM_PWR
  MODIFY_REG(GPIOD->MODER, GPIO_MODER_MODER0,GPIO_MODER_MODER0_0); 
  CLEAR_BIT(GPIOD->ODR, GPIO_ODR_ODR_0);  
  
    //PD8,PD9  Valve ON/OFF   
  MODIFY_REG(GPIOA->MODER, GPIO_MODER_MODER4,GPIO_MODER_MODER4_0);
  MODIFY_REG(GPIOA->MODER, GPIO_MODER_MODER5,GPIO_MODER_MODER5_0);
  CLEAR_BIT(GPIOA->ODR, GPIO_ODR_ODR_4);  
  CLEAR_BIT(GPIOA->ODR, GPIO_ODR_ODR_5);  
  CLEAR_BIT(RCC->AHBENR,RCC_AHBENR_GPIODEN);  

        
  
  //External Watchdog  

  CLEAR_BIT(GPIOH->MODER,GPIO_MODER_MODER1);  //  ���� WAKE
  MODIFY_REG(GPIOH->MODER, GPIO_MODER_MODER0, GPIO_MODER_MODER0_0); // ����� Done
  SET_BIT(GPIOH->PUPDR, GPIO_PUPDR_PUPDR0_1); // pull down
  SET_BIT(GPIOH->PUPDR, GPIO_PUPDR_PUPDR1_1); // pull down
  CLEAR_BIT(GPIOH->ODR, GPIO_ODR_ODR_0);

  //End External Watchdog;  
  
  
  RCC->APB2ENR |= RCC_APB2ENR_SYSCFGEN;
  
  // � �������� ��������� ���������� 12,13 ���� C
  SYSCFG->EXTICR[3] |= (uint16_t)SYSCFG_EXTICR4_EXTI13_PC;  // Key1
  SYSCFG->EXTICR[3] |= (uint16_t)SYSCFG_EXTICR4_EXTI12_PC;  // Enable OPTO_PORT
//  SYSCFG->EXTICR[2] |= (uint16_t)SYSCFG_EXTICR3_EXTI8_PC;  // Enable OPTO_PORT  
  SYSCFG->EXTICR[0] |= (uint16_t)SYSCFG_EXTICR1_EXTI2_PB;   // Kalibr LOCK
  SYSCFG->EXTICR[0] |= (uint16_t)SYSCFG_EXTICR1_EXTI1_PH;   // External Watchdog  
  
 // �������� ������������ ���������� �� �����
  SET_BIT(EXTI->FTSR, EXTI_FTSR_TR12|EXTI_FTSR_TR13|EXTI_FTSR_TR8|EXTI_FTSR_TR2);
  // �������� ������������ ���������� �� ������������ ������
  SET_BIT(EXTI->RTSR, EXTI_RTSR_TR12|EXTI_RTSR_TR13|EXTI_RTSR_TR2|EXTI_RTSR_TR1);

  // ������� CMSIS ����������� ���������� � NVIC
  NVIC_SetPriority(EXTI15_10_IRQn,3);
  NVIC_EnableIRQ(EXTI15_10_IRQn );

  NVIC_SetPriority(EXTI2_IRQn,3);
  NVIC_EnableIRQ(EXTI2_IRQn );
  
  NVIC_SetPriority(EXTI1_IRQn,3);
  NVIC_EnableIRQ(EXTI1_IRQn );  
  
  //��������� ����������
  EXTI->IMR |= EXTI_IMR_IM13|EXTI_IMR_IM12|EXTI_IMR_IM2;
#ifdef E7
  EXTI->IMR |= EXTI_IMR_IM8; 
#endif
  if(READ_BIT(GPIOC->IDR, GPIO_IDR_IDR_12)==0) 
  {
      OptoPort = OPTO_PORT_ENABLE;     
      tick_count_message_from_OPTIC_sec = SystemUpTimeSecond;
  }
  
 /* 
  RCC->IOPSMENR =0;
  SET_BIT(RCC->IOPSMENR, RCC_IOPSMENR_IOPASMEN | RCC_IOPSMENR_IOPBSMEN\
     | RCC_IOPSMENR_IOPCSMEN); 
  RCC->AHBSMENR = 0;
  SET_BIT(RCC->AHBSMENR,RCC_AHBSMENR_DMASMEN | RCC_AHBSMENR_SRAMSMEN);
  RCC->APB2SMENR =0;
  SET_BIT(RCC->APB2SMENR, RCC_APB2SMENR_SYSCFGSMEN);
  RCC->APB1SMENR =0;
  SET_BIT(RCC->APB1SMENR,RCC_APB1SMENR_LCDSMEN |RCC_APB1SMENR_WWDGSMEN\
    |RCC_APB1SMENR_LPUART1SMEN);
*/
  
}

// ----------------------------------------------------------------------------
/* ----------------------------------------------------------------------------
void InitOtputTimer(void)
{
  SET_BIT(RCC->APB1ENR,RCC_APB1ENR_TIM6EN);
  TIM6->PSC = 16000-1;  
  TIM6->ARR = 0xFFFF;
  SET_BIT(TIM6->DIER,TIM_DIER_UIE);
  NVIC_SetPriority(TIM6_IRQn,4);
  NVIC_EnableIRQ(TIM6_IRQn);
  MODIFY_REG(GPIOC->MODER,GPIO_MODER_MODER10,GPIO_MODER_MODER10_0); 
}
// ----------------------------------------------------------------------------
void DeInitOtputTimer(void)
{
  CLEAR_BIT(RCC->APB1ENR,RCC_APB1ENR_TIM6EN);
  MODIFY_REG(GPIOC->MODER,GPIO_MODER_MODER10,GPIO_MODER_MODER10_1); 
}  
// ----------------------------------------------------------------------------
void stopOutputTimer(void)
{
  CLEAR_BIT(TIM6->CR1,TIM_CR1_CEN);       // ��������� �������
  CLEAR_BIT(GPIOC->ODR,GPIO_ODR_ODR_10);  // ����� � 0
  MODIFY_REG(GPIOC->MODER,GPIO_MODER_MODER10,GPIO_MODER_MODER10_1);
}

// ----------------------------------------------------------------------------
void TIM6_IRQHandler(void)
{
 if(TIM6->SR & TIM_SR_UIF)
 {
  CLEAR_BIT(TIM6->SR,TIM_SR_UIF);
    if (Pulse == 0)
    {
      stopOutputTimer();
      return;
    } 
    if(READ_BIT(GPIOC->IDR,GPIO_IDR_IDR_10))
       CLEAR_BIT(GPIOC->ODR,GPIO_ODR_ODR_10); // ����� � 0
    else
    {
      SET_BIT(GPIOC->ODR,GPIO_ODR_ODR_10);
      Pulse--;
    }

 } 
} 
   

// ----------------------------------------------------------------------------
void startOutputTimer(uint16_t pPeriod)
{
  TIM6->ARR = pPeriod;
  TIM6->CNT = 0;
  MODIFY_REG(GPIOC->MODER,GPIO_MODER_MODER10,GPIO_MODER_MODER10_0);
  SET_BIT(GPIOC->ODR,GPIO_ODR_ODR_10);
  SET_BIT(TIM6->CR1,TIM_CR1_CEN);
  
}

*/

  


