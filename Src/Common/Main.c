//=============================================================================
/// \file    Main.c
/// \author  
/// \date    
/// \brief   Main-module implementing the Gasmeter-functionality.
//=============================================================================
// COPYRIGHT
//=============================================================================

#include "Typedef.h"
#include "Error.h"
#include "Time_g.h"
#include "Io.h"
#include "I2c.h"
#include "lcd_8s.h"
#include "Utils.h"
#include "System.h"
#include "Sgm.h"
#include "Gasmeter.h"
#include "FlowMeasurement.h"
#include "Menu.h"
#include "UART_152.h"
#include "ExtFlash.h"
#include "Archiv.h"
#include <string.h>
#include "OptoComand.h"
#include "USART_Modem.h"

#define CONFIG_USE_CATEGORY_MAIN
#define CONFIG_USE_CATEGORY_WELMEC
#include "Configuration.h"


//-----------------------------------------------------------------------------
// Definition of module errors. Add here new module errors. 
// Module errors from 0x00 to 0x1F are allowed.
#define ERROR_MAIN_PROGRAM_CRC       (ERROR_MODULE_MAIN | 0x01)
#define ERROR_MAIN_PARAMETER_CRC     (ERROR_MODULE_MAIN | 0x02)

static Error Boot(void);
static Error InitModules(void); 
static void LogError(Error error);

Error Gl_Error;
uint64_t Status;  // ������� ������  


//-----------------------------------------------------------------------------
int main(void) 
{
   Gl_Error = Error_None();
//  Error error = Error_None();
  Gl_Error = Boot();
  IntArcFlag = 1;
  
  while(!Error_IsFatal(Gl_Error)) 
  {
   System_WatchdogRefresh();
 
   Gl_Error = Gasmeter_Process();

    if(Error_IsError(Gl_Error))
      LCD8_Menu(TEH_MENU_ERROR,0);
    else
      LCD8_Menu(0,0);     

    if (IntArcFlag)
    {
      WriteIntArcRecord(0);
      IntArcFlag =0;
    } 
    
//    if (GPRS_flag)
    {

    }

    
    Time_WaitUntilNextSecond();
  }
  
//  ErrorLog_IncrementErrorCounter(ERRORLOG_REPEATED_GASMETER_FAILURE);
  LogError(Gl_Error);
  System_Reset();
}

//-----------------------------------------------------------------------------
static Error Boot(void)
{
  uint8_t StepHarwareInit;
  
  Error error = Error_None();
  System_Init();
  Time_InitHardware();
  LCD8_Init();
  Io_InitHardware();  //// Init OptoSwitch and Key 
  I2c_InitHardware();
  Sgm_InitHardware();
  Opto_Uart_Init();

  if(Error_IsNone(error)) 
    LCD8_Menu(TEH_MENU_SW,0);

  
  while(StepHarwareInit = Mdm_HardwareInit())
     LCD8_Menu(TEH_MENU_MDMTST,StepHarwareInit);

 while(StepHarwareInit = Mdm_Test(0))
     {
       LCD8_Menu(TEH_MENU_MDMTST,StepHarwareInit);
       if(StepHarwareInit == 0xFF)   // 
       {
         Mdm_Flags.Items.Mdm_TestOk = 0;
         break;
       }  
     }
  
   Mdm_HardwareDeInit();

   if(Error_IsNone(error)) 
    error = InitModules();
  
   WriteIntArcRecord(1);       // ��������� ������ 

  if(Error_IsError(error))
  {
    Error_SetFatal(&error);
//  ErrorLog_IncrementErrorCounter(ERRORLOG_BOOT_FAILURE);
  }
  
  return error;
}

//-----------------------------------------------------------------------------
static Error InitModules(void)
{
  Error error;
 
  error = Sgm_InitModule();
  
  if(Error_IsNone(error))
    error = FlowMeas_InitModule();
  
  if(Error_IsNone(error)) 
    error = Gasmeter_InitModule();
  
  error = Sgm_DeinitModule();
  return error;
}

//-----------------------------------------------------------------------------
static void LogError(Error error)
{
  if(Gl_Error.Raw != error.Raw)
  {
    Gl_Error.Raw = error.Raw;
//    WriteIntArcRecord(2);   /// Log if Error
  }  
  // Add Error in EventArc
}

//-----------------------------------------------------------------------------


//-----------------------------------------------------------------------------

