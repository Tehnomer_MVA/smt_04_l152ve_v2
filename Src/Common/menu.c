//=============================================================================
// 
//=============================================================================
/// \file    Menu.c
/// \author  SUL
/// \date    08-Dec-2014
/// \brief   Module for MenuSwith 
//=============================================================================

//=============================================================================

#include "GlobalHandler.h"



 uint8_t Global_Menu = 0;


//-----------------------------------------------------------------------------
void Switch_Next_Menu(void)
{
  uint8_t temp_MAX_MAIN_MENU = MAX_MAIN_MENU;
  if(!valve_presence_eeprom)
    --temp_MAX_MAIN_MENU;
  if(Global_Menu == MAIN_MENU_TEMPER_GAZ)   
    ++Global_Menu;
 if(Global_Menu > MAX_MAIN_MENU)       // ������� ������� � ��������������� ����
 {
   if(++Global_Menu > MAX_TEH_MENU)
     Global_Menu = TEH_MENU_WARNING;
 }
 else
 {
   if(++Global_Menu > temp_MAX_MAIN_MENU)
     Global_Menu = MAIN_MENU_VOLUME;
 } 
 LCD8_Menu(0,0);
 KeyPressed = KEY_PRESSED_NOT;
}    

//-----------------------------------------------------------------------------
void Switch_Sub_Menu(void)
{
 if(Global_Menu > MAX_MAIN_MENU)       // ������� ������� � ��������������� ����
    Global_Menu = MAIN_MENU_VOLUME;
 else
    Global_Menu = TEH_MENU_WARNING;
 
 LCD8_Menu(0,0);
 KeyPressed = KEY_PRESSED_NOT;
} 

//-----------------------------------------------------------------------------
void LCD8_Menu(uint8_t pMenu, uint8_t pParam)
{
  uint8_t pLCD_menu_point, loMenu;
  uint8_t lo_blink_point = 0;
//  uint8_t errorKfaktor = _FALSE;
  if (pMenu)
    loMenu = pMenu;
  else
    loMenu = Global_Menu;
  /*
  if(Sensor.KL > 38999 && Sensor.KL < 40701)
  {
     loMenu = TEH_MENU_ERROR;
     errorKfaktor = _OK;
  }
  */
  switch(loMenu)
  {
  case MAIN_MENU_VOLUME:  
  default:  
         if(mode_conservation == 1)
            sprintf(LCD_string,"%s", " 5LEEP  ");// "   -    "
         else
         {
            sprintf(LCD_string,"%9.3f", GasMeter.VE_Lm3);
            pLCD_menu_point = 0x01;             
         }
/*
         if(StatusAlarm.bit.reverse_flow)
           sprintf(LCD_string,"E%d", 6);         
         if(type_device_int == 0)
           sprintf(LCD_string,"E %d", 1);
#ifdef E7
         if(!READ_BIT(GPIOC->IDR, GPIO_IDR_ID8))
         {
            if(StatusCrash.bit.body_cover_open)
              sprintf(LCD_string,"E%d", 7); 
         }
#endif
         if(StatusCrash.bit.error_in_SGM_module)
           sprintf(LCD_string,"E%d", 1);      
         
         */

         Global_Menu = MAIN_MENU_VOLUME;
      break;
  case MAIN_MENU_FLOW:   
         sprintf(LCD_string,"%9.3f", GasMeter.QgL_Disp);
         pLCD_menu_point = 0x02; 
      break;
  case MAIN_MENU_DATE: 
         GetDate_Time(&SystDate,&SystTime);  
         sprintf(LCD_string,"%0.2u.%0.2u.20%0.2u", SystDate.Day, SystDate.Month, SystDate.Year);
         pLCD_menu_point = 0x04; 
      break;      
  case MAIN_MENU_VALVE:
       {
         char ch_auto;
         if(valve_enable_auto_control_eeprom)
           ch_auto = 'A';
         else
           ch_auto = ' ';
         switch(ValveGetStatus())
         {
         default:
         case VALVE_CLOSE_TCP:
           
         case VALVE_CLOSE_REVERSE_FLOW:  
         case VALVE_CLOSE_Q_MAX:  
         case VALVE_CLOSE_NON_NULL: 
            sprintf(LCD_string,"CL05E %d%c", ValveGetStatus(), ch_auto);
            break;
         case VALVE_OPEN_CLOSE: 
            sprintf(LCD_string, "%s", "0P.-CL05E");
            break;  
         case VALVE_CLOSE_OPEN: 
            sprintf(LCD_string,"CL.-0PE�%d", ValveGetCntOpenFlowNonNull());
            break;
         case VALVE_OPEN: 
            sprintf(LCD_string,"  0PE� %c", ch_auto);
            break;
         case VALVE_UNKNOWN:   
         case VALVE_NON_POWER: 
            sprintf(LCD_string,"--------"); 
            break;   
         }
         pLCD_menu_point = 0x08; 
       }
       break;       
      
  case TEH_MENU_WARNING:   
  {
       uint8_t mas[16], i=0, len=0, cnt_digit=0, flag_overflow=0;
       memset(mas, 0, 16);
       if(StatusWarning.word & StatusWarningEnable.word)
       {
          if(StatusWarning.bit.counter_not_put_into_operation && StatusWarningEnable.bit.counter_not_put_into_operation)
          {  mas[i++] = 1; ++cnt_digit; }
          if(StatusWarning.bit.SIM_card_non && StatusWarningEnable.bit.SIM_card_non)
          {  mas[i++] = 2; ++cnt_digit;}
          if(StatusWarning.bit.error_connect_server && StatusWarningEnable.bit.error_connect_server)
          {  mas[i++] = 3; ++cnt_digit;}
          if(StatusWarning.bit.battery_not_connected && StatusWarningEnable.bit.battery_not_connected)
          {  mas[i++] = 4; ++cnt_digit;}
          if(StatusWarning.bit.residual_capacity_battery_modem_min15 && StatusWarningEnable.bit.residual_capacity_battery_modem_min15)
          {  mas[i++] = 5; ++cnt_digit;}
          if(StatusWarning.bit.residual_capacity_battery_modem_min10 && StatusWarningEnable.bit.residual_capacity_battery_modem_min10)
          {  mas[i++] = 6; ++cnt_digit;}
          if(StatusWarning.bit.residual_capacity_battery_main_min15 && StatusWarningEnable.bit.residual_capacity_battery_main_min15)
          {  mas[i++] = 7; ++cnt_digit;}
          if(StatusWarning.bit.residual_capacity_battery_main_min10 && StatusWarningEnable.bit.residual_capacity_battery_main_min10)
          {  
             if(cnt_digit > 6)
             {
                if(timeSystemUnix % 2)
                {
                   memset(mas, 0, 16);
                   i = 0;
                   flag_overflow = 1;
                }
             }
             mas[i++] = 8; ++cnt_digit;
          }
          if(StatusWarning.bit.battery_cover_open && StatusWarningEnable.bit.battery_cover_open)
          {
            if(cnt_digit > 6)
            {
               if(timeSystemUnix % 2 && !flag_overflow)
               {
                  memset(mas, 0, 16);
                  i = 0;
                  flag_overflow = 1;
               }
            }
            mas[i++] = 9; ++cnt_digit;
          }
          if(StatusWarning.bit.close_valve && StatusWarningEnable.bit.close_valve)
          {
            if(cnt_digit > 6)
            {
               if(timeSystemUnix % 2 && !flag_overflow)
               {
                  memset(mas, 0, 16);
                  i = 0;
                  flag_overflow = 1;
               }
            }
            mas[i++] = 11; cnt_digit += 2;
          }

          len = sprintf(LCD_string,"%c", '�'); 
          for(uint8_t j=0; j < i; j++)
             len += sprintf(LCD_string + len,"%d.", mas[j]); 
       }
       else
         sprintf(LCD_string,"%s", "�-------");
      pLCD_menu_point = 0x41;
      break;  
  }      
  case MENU_REGISTR_STORY_WAR:
   {
       uint8_t mas[16], i=0, len=0, cnt_digit=0, flag_overflow=0;
       memset(mas, 0, 16);
       if(event_int_story & EVENT_INT_WARNING_MASK & (uint64_t)StatusWarningEnable.word)
       {
         if((event_int_story & EVENT_INT_COMMISSIONING) && StatusWarningEnable.bit.counter_not_put_into_operation)
            mas[i++] = 1;
         if((event_int_story & EVENT_INT_SIM_CARD_NON) && StatusWarningEnable.bit.SIM_card_non)
            mas[i++] = 2;
         if((event_int_story & EVENT_INT_ERROR_SESSION_TCP) && StatusWarningEnable.bit.error_connect_server)
            mas[i++] = 3;    
         if((event_int_story & EVENT_INT_BATARY_TELEMETRY_NON) && StatusWarningEnable.bit.battery_not_connected)
            mas[i++] = 4;      
         if((event_int_story & EVENT_INT_BATTERY_TELEMETRY_MIN_15) && StatusWarningEnable.bit.residual_capacity_battery_modem_min15)
            mas[i++] = 5;    
         if((event_int_story & EVENT_INT_BATTERY_TELEMETRY_MIN_10) && StatusWarningEnable.bit.residual_capacity_battery_modem_min10)
            mas[i++] = 6;       
         if((event_int_story & EVENT_INT_BATTERY_COUNTER_MIN_15) && StatusWarningEnable.bit.residual_capacity_battery_main_min15)
          {  
             if(cnt_digit > 5)
             {
                if(timeSystemUnix % 2)
                {
                   memset(mas, 0, 16);
                   i = 0;
                   flag_overflow = 1;
                }
             }
             mas[i++] = 7; ++cnt_digit;
          }
         if((event_int_story & EVENT_INT_BATTERY_COUNTER_MIN_10) && StatusWarningEnable.bit.residual_capacity_battery_main_min10)
          {
            if(cnt_digit > 6)
            {
               if(timeSystemUnix % 2 && !flag_overflow)
               {
                  memset(mas, 0, 16);
                  i = 0;
                  flag_overflow = 1;
               }
            }
            mas[i++] = 7; ++cnt_digit;
          }           
         if((event_int_story & EVENT_INT_BATTERY_COVER_OPEN) && StatusWarningEnable.bit.battery_cover_open)
          {
            if(cnt_digit > 6)
            {
               if(timeSystemUnix % 2 && !flag_overflow)
               {
                  memset(mas, 0, 16);
                  i = 0;
                  flag_overflow = 1;
               }
            }
            mas[i++] = 9; ++cnt_digit;
          }
         if((event_int_story & EVENT_INT_CLOSE_VALVE) && StatusWarningEnable.bit.close_valve)
          {
            if(cnt_digit > 6)
            {
               if(timeSystemUnix % 2 && !flag_overflow)
               {
                  memset(mas, 0, 16);
                  i = 0;
                  flag_overflow = 1;
               }
            }
            mas[i++] = 11; ++cnt_digit;
          }       

          len = sprintf(LCD_string,"%s", "P.�."); 
          for(uint8_t j=0; j < i; j++)
             len += sprintf(LCD_string + len,"%d.", mas[j]); 
       }
       else
         sprintf(LCD_string,"%s", "P.�.------");
       pLCD_menu_point = 0x48;
   }
       break;
  case TEH_MENU_ALARM:   
  {
       uint8_t mas[16], i=0, len=0;
       memset(mas, 0, 16);
       if(StatusAlarm.word & StatusAlarmEnable.word)
       {
          if(StatusAlarm.bit.reverse_flow && StatusAlarmEnable.bit.reverse_flow)
            mas[i++] = 1;
          if(StatusAlarm.bit.going_beyond_Q && StatusAlarmEnable.bit.going_beyond_Q)
            mas[i++] = 2;           
          if(StatusAlarm.bit.going_beyond_T_gas && StatusAlarmEnable.bit.going_beyond_T_gas)
            mas[i++] = 3;
          if(StatusAlarm.bit.going_beyond_T_environ && StatusAlarmEnable.bit.going_beyond_T_environ)
            mas[i++] = 4;    
          if(StatusAlarm.bit.going_beyond_K_factor && StatusAlarmEnable.bit.going_beyond_K_factor)
            mas[i++] = 5;      
          if(StatusAlarm.bit.unauthorized_intervention && StatusAlarmEnable.bit.unauthorized_intervention)
            mas[i++] = 6;    

          len = sprintf(LCD_string,"%c", 'E'); 
          for(uint8_t j=0; j < i; j++)
             len += sprintf(LCD_string + len,"%d.", mas[j]); 
       }
       else
         sprintf(LCD_string,"%s", "E-------");
      pLCD_menu_point = 0x42;
      break;  
  }      
  case MENU_REGISTR_STORY_ALARM:
   {
       uint8_t mas[16], i=0, len=0;
       memset(mas, 0, 16);
       if(event_int_story & EVENT_INT_ALARM_MASK & ((uint64_t)StatusAlarmEnable.word << 16))
       {
         if((event_int_story & EVENT_INT_REVERSE_FLOW) && StatusAlarmEnable.bit.reverse_flow)
            mas[i++] = 1;
         if((event_int_story & EVENT_INT_MAX_Q) && StatusAlarmEnable.bit.going_beyond_Q)
            mas[i++] = 2;
         if((event_int_story & EVENT_INT_GOING_BEYOND_T_GAS) && StatusAlarmEnable.bit.going_beyond_T_gas)
            mas[i++] = 3;    
         if((event_int_story & EVENT_INT_GOING_BEYOND_T_ENVIRON) && StatusAlarmEnable.bit.going_beyond_T_environ)
            mas[i++] = 4;      
         if((event_int_story & EVENT_INT_GOING_BEYOND_K_FACTOR) && StatusAlarmEnable.bit.going_beyond_K_factor)
            mas[i++] = 5;    
         if((event_int_story & EVENT_INT_UNAUTHORIZED_INTERVENTION) && StatusAlarmEnable.bit.unauthorized_intervention)
            mas[i++] = 6;       

          len = sprintf(LCD_string,"%s", "P.E."); 
          for(uint8_t j=0; j < i; j++)
             len += sprintf(LCD_string + len,"%d.", mas[j]); 
       }
       else
         sprintf(LCD_string,"%s", "P.E.------");
       pLCD_menu_point = 0x50;
   }
       break;
  case TEH_MENU_CRASH:   
    {
       uint8_t mas[16], i=0, len=0;
       memset(mas, 0, 16);
     
       if(CLB_lock == 0x66)
       {  
          if(StatusCrash.word)
          {
             if(StatusCrash.bit.error_in_SGM_module)
               mas[i++] = 1;
             if(StatusCrash.bit.going_beyond_T_gas_long)
               mas[i++] = 2;
             if(StatusCrash.bit.going_beyond_T_environ_long)
               mas[i++] = 3;    
             if(StatusCrash.bit.body_cover_open)
               mas[i++] = 4;      
             if(StatusCrash.bit.calibration_lock_open)
               mas[i++] = 5;    
             if(StatusCrash.bit.check_crc_dQf_false)
               mas[i++] = 6;    
             if(StatusCrash.bit.check_sn_sgm_false)
               mas[i++] = 7;    
          
             len = sprintf(LCD_string,"%c", 'A'); 
             for(uint8_t j=0; j < i; j++)
                len += sprintf(LCD_string + len,"%d.", mas[j]); 
          }
          else
            sprintf(LCD_string,"%s", "A-------");
       }
       else
          if(StatusDevice.availability_crach)
          {
             if((event_int_story & EVENT_INT_ERROR_SGM_MODULE) && StatusCrashEnable.bit.error_in_SGM_module)
                mas[i++] = 1;
             if((event_int_story & EVENT_INT_GOING_BEYOND_T_GAS_LONG) && StatusCrashEnable.bit.going_beyond_T_gas_long)  
                mas[i++] = 2;      
             if((event_int_story & EVENT_INT_GOING_BEYOND_T_ENVIRON_LONG) && StatusCrashEnable.bit.going_beyond_T_environ_long)  
                mas[i++] = 3;    
             if((event_int_story & EVENT_INT_OPEN_CASE) && StatusCrashEnable.bit.body_cover_open)
                mas[i++] = 4;    
             if((event_int_story & EVENT_INT_OPEN_CALIB_SWITCH) && StatusCrashEnable.bit.calibration_lock_open)
                mas[i++] = 5;    
             if((event_int_story & EVENT_INT_ERROR_CS_DQF) && StatusCrashEnable.bit.check_crc_dQf_false)
                mas[i++] = 6;  
             if((event_int_story & EVENT_INT_ERROR_SER_NUMB_SGM) && StatusCrashEnable.bit.check_sn_sgm_false)
                mas[i++] = 7;  
             
             len = sprintf(LCD_string,"%c", 'A'); 
             for(uint8_t j=0; j < i; j++)
                len += sprintf(LCD_string + len,"%d.", mas[j]); 
          }
          else
            sprintf(LCD_string,"%s", "A-------");
       pLCD_menu_point = 0x44;
    }     
    break;
  case TEH_MENU_SW:  
         if(!show_ver_po_eeprom)
            sprintf(LCD_string," %d.%02d%02d%02d",FW_VERSION_MAJOR, FW_VERSION_MINOR, FW_GSM_VERSION, FW_TEST_VERSION);
         else
            sprintf(LCD_string,"�0  %s", "1.08");
        // pLCD_menu_point = 0x46; 
         pLCD_menu_point = 0x79;
       break;   
  case TEH_MENU_VOLUME_FLOAT:
       {
       uint32_t temp;
       temp = (uint32_t)(GasMeter.VE_Lm3 * 10000);
       temp %= 10000;
       sprintf(LCD_string,"    .%04d", temp);
    //   LCD_string[3] = ' ';
       pLCD_menu_point = 0x60;    
       }
       break;  

  case MAIN_MENU_GASTYPE: 
         sprintf(LCD_string,"C.�. %1.0f", GasMeter.mid_GasRecond);    
         pLCD_menu_point = 0x61;
      break;    
  case MAIN_MENU_TEMPER_GAZ:  
         sprintf(LCD_string,"t��.%6.2f", GasMeter.var_T);  
         pLCD_menu_point = 0x62;
      break;    
  case MAIN_MENU_TIME:    
         GetDate_Time(&SystDate,&SystTime);  
         sprintf(LCD_string,"  %0.2u.%0.2u.%0.2u", SystTime.Hours, SystTime.Minutes, SystTime.Seconds);
         pLCD_menu_point = 0x68;
      break;   
  case MENU_MODE_TRANSMI:    
         if(timeSystemUnix % 2)
            sprintf(LCD_string,"P.�.�.%3.1d.%02d", Mode_Transfer.mode, Mode_Transfer.day);
         else
            sprintf(LCD_string,"P.�.�.%3.2d.%02d", Mode_Transfer.hour, Mode_Transfer.minute);
         pLCD_menu_point = 0x70;
      break; 
  case TEH_MENU_MDMTST:  
       if(flag_event_menu > 0)
         sprintf(LCD_string,"�.�. %02d %02d", flag_event_menu, GSM_Get_level_gsm_signal());
       else
         if(flag_event != TELEM_EVENT_OFF_GSM_MODEM)
            sprintf(LCD_string,"�.�.%d.%d %d", flag_event_menu, flag_event, GSM_Get_level_gsm_signal());
         else
            if(success_session_TCP == _OK)
               sprintf(LCD_string,"�.�. %02d %02d", flag_event, GSM_Get_level_gsm_signal());
            else
               sprintf(LCD_string,"�.�.0.%d %d", flag_event, GSM_Get_level_gsm_signal());
  //  sprintf(LCD_string,"%s", "�.�.�.�.�.�.�.�.");
     //    pLCD_menu_point =0x44;
         pLCD_menu_point = 0x71;
      break;  
  case MENU_RESIDUAL_CAPACITY_BATTERY_MEAS:  
         sprintf(LCD_string,"bat1 %3.1f", residual_capacity_battery_main_percent);    
         pLCD_menu_point = 0x72;
      break;    
  case MENU_RESIDUAL_CAPACITY_BATTERY_MODEM:  
         sprintf(LCD_string,"bat2 %3.1f", residual_capacity_battery_modem_percent);    
         pLCD_menu_point = 0x74;
      break;  
  case TEH_SERIAL_DEVICE:  
    { char *ptr;
      char len;
      ptr  = (char*)Device_STM.SN_Device;
      len = strlen((char*)Device_STM.SN_Device);
      if(len > 8)
        ptr += len - 8;
       sprintf(LCD_string,"%s", ptr);
         pLCD_menu_point =0x78;
    }
      break;  

  case MENU_CHKSUMM:  
         sprintf(LCD_string,"C1  6314");
          pLCD_menu_point = 0x7A;
      break;  
  case MENU_DATE_VERIFIC:  
         sprintf(LCD_string,"%02d.%02d.20%02d", DateVerification.Day, DateVerification.Month, DateVerification.Year);
          pLCD_menu_point = 0x7C;
      break; 
  case MENU_DATE_VERIFIC_NEXT:  
         sprintf(LCD_string,"%02d.%02d.20%02d", DateNextVerification.Day, DateNextVerification.Month, DateNextVerification.Year);
          pLCD_menu_point = 0x7D;
      break; 
  case TEH_TEST_LCD:
      sprintf(LCD_string,"%s", "8.8.8.8.8.8.8.8.");
      pLCD_menu_point =0xFF;
      break;
      
  }  
  
  if (loMenu < MAIN_MENU_VALVE)  
    if (ValveGetStatus() != VALVE_OPEN && valve_presence_eeprom == 1)
       lo_blink_point = 0x08;   

  if(mode_conservation != 1)
  {
     if(OptoPort == OPTO_PORT_ENABLE || GSM_Get_Status_power() == GSM_POWER_ON)
        pLCD_menu_point |=0x80;

     if(   (event_int_story & EVENT_INT_WARNING_MASK & (uint64_t)StatusWarningEnable.word)
        || (event_int_story & EVENT_INT_ALARM_MASK & ((uint64_t)StatusAlarmEnable.word << 16))
        || (event_int_story & EVENT_INT_CRASH_MASK & ((uint64_t)StatusCrashEnable.word << 32)))
       lo_blink_point |= 0x40; 
  }
  display_buffer(pLCD_menu_point, lo_blink_point );
  
  
}
