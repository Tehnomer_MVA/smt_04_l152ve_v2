//=============================================================================
//  _DLIB_TIME_USES_64
//=============================================================================
/// \file    Time.c
/// \author  LWI
/// \date    18-Dec-2014
/// \brief   Module for Timing and MCU Low Power Wait functions
//=============================================================================
//
//
//=============================================================================
#include "GlobalHandler.h"


static void InitSecondAlarm(void);
static void InitHourAlarm(void);
static void InitSystemClock(void);
static void SetWakeUpTimer(uint32_t WakeUpCounter);
static void DeactivateWakeUpTimer(void);
void EnterSTOPMode(uint8_t);
void RTC_DisableWriteProtection(void);
void RTC_EnableWriteProtection(void);
void SetDate_Time(LL_RTC_DateTypeDef* NewDate, LL_RTC_TimeTypeDef* NewTime);

//void RTC_GetDate(uint32_t RTC_Format,LL_RTC_DateTypeDef* p_Date);
//void RTC_GetTime(uint32_t RTC_Format,LL_RTC_TimeTypeDef* p_Time);
void RTC_GetDate(/*uint32_t RTC_Format,*/LL_RTC_DateTypeDef* p_Date);
void RTC_GetTime(/*uint32_t RTC_Format,*/LL_RTC_TimeTypeDef* p_Time);
void RTC_SetDate(uint32_t RTC_Format,LL_RTC_DateTypeDef* p_Date);
void RTC_SetTime(uint32_t RTC_Format,LL_RTC_DateTypeDef* p_Time);



uint32_t SystemUpTimeSecond;
uint8_t WakeUpTimerFlag;
uint8_t AlarmFlag;
uint8_t IntArcFlag;
//static void (*BeforeSleep)(bool* cancle) = 0;

#define _RTC_CONVERT_BIN2BCD(__VALUE__) (uint8_t)((((__VALUE__) / 10U) << 4U) | ((__VALUE__) % 10U))
#define _RTC_CONVERT_BCD2BIN(__VALUE__) (uint8_t)(((uint8_t)((__VALUE__) & (uint8_t)0xF0U) >> (uint8_t)0x4U) * 10U + ((__VALUE__) & (uint8_t)0x0FU))

LL_RTC_TimeTypeDef SystTime;
LL_RTC_DateTypeDef SystDate;

RTC_TypeDef loRTC;

extern SemaphoreHandle_t xRTCSemaphore;


//-----------------------------------------------------------------------------
void Time_DelayMicroSeconds(uint32_t delay)
{
  uint32_t i;
  for(i = 0; i < delay; ++i)
  {
    __NOP(); __NOP(); __NOP(); __NOP();
    __NOP(); __NOP(); __NOP(); __NOP();
    __NOP(); __NOP(); __NOP(); __NOP();
  }
}

//-----------------------------------------------------------------------------
void Time_DelayMilliSeconds(uint32_t delay)
{
  SetWakeUpTimer(delay);
  while(WakeUpTimerFlag == 0)
    EnterSTOPMode(_FALSE);   // check for wakeup from Timer
  DeactivateWakeUpTimer();   
}

//-----------------------------------------------------------------------------
void Time_WaitUntilNextSecond(void)
{
  // clear Flag
  AlarmFlag = 0;
  while(AlarmFlag == 0) 
  { // check for Alarm Flag
//    while (DataReceived)  
//        OPTO_Uart_Read();
    EnterSTOPMode(_FALSE);
   // Time_DelayMilliSeconds(2000);
  }
}

//-----------------------------------------------------------------------------
void Time_SetSysTime(void)
{
  SystTime.Hours = 14;
  SystTime.Minutes = 55;
  SystTime.Seconds =0;
  SystTime.TimeFormat = LL_RTC_TIME_FORMAT_AM_OR_24;
  
  SystDate.Day = 18;
  SystDate.Month = 6;
  SystDate.Year = 18;
  SystDate.WeekDay = 1;
  
  SetDate_Time(&SystDate,&SystTime);
}  

//-----------------------------------------------------------------------------
uint8_t GetDayOfWeek (LL_RTC_DateTypeDef* NewDate)
{
  /* ��� ������
   1 � ������, �������;
   2 � ���;
   3 � ������;
   4 � �������, ����, ������;
   5 � ����;
   6 � �������, ��������;
   0 � ������, ����.
 */ 

uint8_t KodeYear;
uint8_t ret;
const uint8_t KodeMonth[] = {1,4,4,0,2,5,0,3,6,1,4,6};

  KodeYear = NewDate->Year & 0xFF;
  KodeYear = (6 + KodeYear + KodeYear/4)%7;
  
  ret = (NewDate->Day + KodeMonth[NewDate->Month-1]+ KodeYear)%7;
  
  if ((NewDate->Year%4)==0)
    if (NewDate->Month < 3)
      ret++;
  ret = (ret+6)%7;
  if (ret==0) ret = 7;
  return ret;
} 

//-----------------------------------------------------------------------------
void Time_InitHardware(void)
{
  // enable PWR clk
  SET_BIT(RCC->APB1ENR, RCC_APB1ENR_PWREN);
  
  // Init System Clock
  InitSystemClock();
  
  // Enable write access to Backup domain
  SET_BIT(PWR->CR, PWR_CR_DBP); 
  // Wait for Backup domain Write protection disable
  while((PWR->CR & PWR_CR_DBP) == 0);
  
  // Enable the Ultra Low Power mode and fast wake up
  SET_BIT(PWR->CR, PWR_CR_ULP | PWR_CR_FWU);
  
  // Select HSI as system clock source after Wake Up from Stop mode
  //MODIFY_REG(RCC->CFGR,RCC_CFGR_STOPWUCK, RCC_CFGR_STOPWUCK); //152-����!!
  
  //===================== Enable LSE ============================
  
  // Reset LSEON and LSEBYP bits before configuring the LSE
  MODIFY_REG(RCC->CSR, RCC_CSR_LSEON|RCC_CSR_LSEBYP, 0);  
  
  // Set to highest LSE driving Level NOT in152 series!!
//  MODIFY_REG(RCC->CSR, RCC_CSR_LSEDRV, RCC_CSR_LSEDRV_1|RCC_CSR_LSEDRV_0);
  
  // Set the new LSE configuration
  MODIFY_REG(RCC->CSR, RCC_CSR_LSEON|RCC_CSR_LSEBYP, RCC_CSR_LSEON); 
  
  // Wait till LSE is ready
  while((RCC->CSR & RCC_CSR_LSERDY) == 1);
  
  //============  Select LSE as RTC clock source ================
  
  //Enable the LSE as RTC peripheral Clock 
  MODIFY_REG(RCC->CSR, RCC_CSR_RTCSEL, RCC_CSR_RTCSEL_LSE); 
  
  // Enable RTC Clock
  SET_BIT(RCC->CSR, RCC_CSR_RTCEN);
  
  //======== Configure the NVIC for RTC Wake up Tamper ==========
  SET_BIT(EXTI->RTSR, EXTI_IMR_IM20);
  SET_BIT(EXTI->IMR, EXTI_IMR_IM20);
  
  NVIC_SetPriority(RTC_WKUP_IRQn,1);
  NVIC_EnableIRQ(RTC_WKUP_IRQn);
  
 RTC_DisableWriteProtection();
 if(!(RTC->ISR & RTC_ISR_INITF))
 {
   RTC->ISR = RTC_INIT_MASK;
   while(!(RTC->ISR & RTC_ISR_INITF));
 }
 
#ifndef RTC_SPEED 
  // Configure the RTC PRER with prescaler = 32768
  // Synchronous prescaler factor: ck_spre frequency = ck_apre frequency/256
  RTC->PRER = (uint32_t)(0x00FF);    // 256
  // Asynchronous prescaler factor: ck_apre frequency = RTCCLK frequency/128
  RTC->PRER |= (uint32_t)(0x7F << 16);   // 128
#endif
 // ### 59.041 ��
 /*
  RTC->PRER = (uint32_t)(111);    // 111
  // Asynchronous prescaler factor: ck_apre frequency = RTCCLK frequency/128
  RTC->PRER |= (uint32_t)(0x05 << 16);   // 5
 */
  
#ifdef RTC_SPEED 
     RTC->PRER = (uint32_t)(111);    // 111
     // Asynchronous prescaler factor: ck_apre frequency = RTCCLK frequency/128
     RTC->PRER |= (uint32_t)(0x05 << 16);   // 5
#endif
  
  
  SET_BIT(RTC->ISR,RTC_ISR_INIT);
  while((RTC->ISR & RTC_ISR_INITF)==0);
  CLEAR_BIT(RTC->ISR,RTC_ISR_INIT);
 
  SET_BIT(RTC->CR,RTC_CR_BYPSHAD);
   
  RTC_EnableWriteProtection();
  
  // no Wake up timer active
  DeactivateWakeUpTimer();
  WakeUpTimerFlag = 0; 
  
  // Init System up Time 
  SystemUpTimeSecond = 0;
  
  if(System_IsWatchdogResetOccurred() || System_IsSoftwareResetOccurred())
  {
    __no_operation();
    
  } 
   else
     //### Time_SetSysTime();
  __NOP();
  
  // Set Second Alarm
  InitSecondAlarm();
  InitHourAlarm();
  SystemCoreClockUpdate();

//  initPeriodicTimer();
  
}

//-----------------------------------------------------------------------------
uint32_t Time_GetSystemUpTimeSecond(void)
{
   return SystemUpTimeSecond;
}

//-----------------------------------------------------------------------------

/**
  * @brief  Disable the write protection for RTC registers.
  * @rmtoll WPR          KEY           LL_RTC_DisableWriteProtection
  * @param  RTCx RTC Instance
  * @retval None
  */
__STATIC_INLINE void RTC_DisableWriteProtection(void)
{
   // Disable the write protection for RTC registers */
  RTC->WPR = 0xCA; // Password 1
  RTC->WPR = 0x53; // Password 2
}

//-----------------------------------------------------------------------------
__STATIC_INLINE void RTC_EnableWriteProtection(void)
  // Enable the write protection for RTC registers
{
  RTC->WPR = 0xFF;
} 

//-----------------------------------------------------------------------------
static void SetWakeUpTimer(uint32_t WakeUpTime)
{
  // Clear Flag
  WakeUpTimerFlag = 0;
  
  // minimum Sleep time is 1ms
  if(WakeUpTime == 0)
    WakeUpTime = 1;
  
  // Disable the write protection for RTC registers */
  RTC_DisableWriteProtection();
    
  // Disable Wacke up timer
  CLEAR_BIT(RTC->CR, RTC_CR_WUTE);  
  CLEAR_BIT(RTC->ISR, RTC_ISR_WUTF);  
  SET_BIT(PWR->CR, PWR_CR_CWUF); 

  // Wait till RTC WUTWF flag is set
  while(READ_BIT(RTC->ISR, RTC_ISR_WUTWF) == 0);

  // Configure the Wakeup Timer counter for given Time in ms, the Time is adjusted
  // with -3 for compensate the wakeup and configuration time
  RTC->WUTR = (uint32_t)WakeUpTime * 16384 / 1000 - 3;

  // Clear the Wakeup Timer clock source bits in CR register
  CLEAR_BIT(RTC->CR, RTC_CR_WUCKSEL);

  // Configure the clock source: Div2 
  RTC->CR |= RTC_CR_WUCKSEL_0 | RTC_CR_WUCKSEL_1;

  // RTC WakeUpTimer Interrupt Configuration: EXTI configuration 
//  SET_BIT(EXTI->IMR, EXTI_IMR_IM20);
//  SET_BIT(EXTI->RTSR, EXTI_IMR_IM20);

  // Configure the Interrupt in the RTC_CR register
  SET_BIT(RTC->CR, RTC_CR_WUTIE);
  
  // Enable the Wakeup Timer
  SET_BIT(RTC->CR, RTC_CR_WUTE);

  // Enable the write protection for RTC registers
  RTC->WPR = 0xFF;
}

//-----------------------------------------------------------------------------
static void DeactivateWakeUpTimer(void)
{
  // Disable the write protection for RTC registers */
  RTC_DisableWriteProtection();
  // Disable Wacke up timer
  CLEAR_BIT(RTC->CR, RTC_CR_WUTE);

  // In case of interrupt mode is used, the interrupt source must disabled
  CLEAR_BIT(RTC->CR, RTC_CR_WUTIE);

  // Wait till RTC WUTWF flag is set
  while(READ_BIT(RTC->ISR, RTC_ISR_WUTWF) == 0);

  // Enable the write protection for RTC registers
  RTC->WPR = 0xFF;
}

//-----------------------------------------------------------------------------
void EnterSTOPMode(uint8_t flag_main)
{    
/*  Optoport active!!  */
  if (OptoPort == OPTO_PORT_DISABLE 
      && HiPower_ON == 0
     // &&  GSM_Get_Status_power() == POWER_NON 
     // && GSM_Get_flag_power() == GSM_MUST_DISABLE
        ) 
  {  
    CalculateMotoHourceWorkCPU(MOTO_HOURCE_STOP);  
     if(flag_main == _OK) 
        CalculateMotoHourceStopCPU();
    // Set Standby Mode with low Power Regulator
    MODIFY_REG(PWR->CR, PWR_CR_PDDS | PWR_CR_LPSDSR, PWR_CR_LPSDSR);
    
    // Set SLEEPDEEP bit of Cortex System Control Register
    SCB->SCR |= SCB_SCR_SLEEPDEEP_Msk;
    
    // Request Wait For Interrupt
  #ifndef LOW_POWER_DISABLED 
    __WFI();
  #endif
    
    // Reset SLEEPDEEP bit of Cortex System Control Register
    SCB->SCR &= (uint32_t)~((uint32_t)SCB_SCR_SLEEPDEEP_Msk);
     
    CLEAR_BIT(PWR->CR, PWR_CR_LPSDSR);
    // Re-init System Clock
    CalculateMotoHourceWorkCPU(MOTO_HOURCE_START);     
    InitSystemClock();
  }

//  if (KeyPressed == KEY_PRESSED_SHORT)
//      Switch_Next_Menu();

///  xSemaphoreGive(xRTCSemaphore);//, &xHigherPriorityTaskWoken );

}

//-----------------------------------------------------------------------------
static void InitSecondAlarm(void)
{
  // Clear Alarm Flag
  AlarmFlag = 0;
  
  // Disable the write protection for RTC registers
  RTC->WPR = 0xCA;  // Password 1
  RTC->WPR = 0x53;  // Password 2 
  
  // Disable Alarm
  CLEAR_BIT(RTC->CR, RTC_CR_ALRAE);  

  // Configure the Alarm Timer counter: ignore date, hour, minute and second
  // for wakeup the core every second
//   RTC->ALRMAR = RTC_ALRMAR_MSK4|RTC_ALRMAR_MSK3|RTC_ALRMAR_MSK2|RTC_ALRMAR_MSK1;
 // RTC->ALRMAR = RTC_ALRMAR_MSK4|RTC_ALRMAR_MSK3|RTC_ALRMAR_MSK2/*|RTC_ALRMAR_MSK1*/; //### ���� ���� Test TEST

#ifndef RTC_SPEED 
     RTC->ALRMAR = RTC_ALRMAR_MSK4|RTC_ALRMAR_MSK3|RTC_ALRMAR_MSK2|RTC_ALRMAR_MSK1;
#endif   
   
#ifdef RTC_SPEED 
     RTC->ALRMAR = RTC_ALRMAR_MSK4|RTC_ALRMAR_MSK3|RTC_ALRMAR_MSK2/*|RTC_ALRMAR_MSK1*/;
#endif      
   

  // RTC WakeUpTimer Interrupt Configuration: EXTI configuration 
  SET_BIT(EXTI->IMR, EXTI_IMR_IM17);
  SET_BIT(EXTI->RTSR, EXTI_IMR_IM17);

  NVIC_SetPriority(RTC_Alarm_IRQn, 4);

//  NVIC_EnableIRQ(RTC_Alarm_IRQn);

  // Configure the Interrupt in the RTC_CR register
  SET_BIT(RTC->CR, RTC_CR_ALRAIE);
  
  // Enable the Alarm A
  if(mode_conservation == 0)
  SET_BIT(RTC->CR, RTC_CR_ALRAE);

  // Enable the write protection for RTC registers
  RTC->WPR = 0xFF;
}

//-----------------------------------------------------------------------------
static void InitHourAlarm(void)
{
   
  // Disable the write protection for RTC registers
  RTC->WPR = 0xCA;  // Password 1
  RTC->WPR = 0x53;  // Password 2 
  
  // Disable Alarm
  CLEAR_BIT(RTC->CR, RTC_CR_ALRBE);  

// TEST!!!!!!!!!!!!!!!!!!!!!!!! �������� ������ ��� � ������  
  // Configure the Alarm Timer counter: ignore date, hour
  // for wakeup the core every minutes
// RTC->ALRMBR = RTC_ALRMBR_MSK4|RTC_ALRMBR_MSK3|RTC_ALRMBR_MSK2;
// CLEAR_BIT(RTC->ALRMBR,RTC_ALRMBR_SU|RTC_ALRMBR_MNU);
// TEST!!!!!!!!!!!!!!!!!!!!!!!! �������� ������ ��� � ������  

  // Configure the Alarm Timer counter: ignore date, hour
  // for wakeup the core every hour
  RTC->ALRMBR = RTC_ALRMBR_MSK4|RTC_ALRMBR_MSK3;
  CLEAR_BIT(RTC->ALRMBR,RTC_ALRMBR_ST|RTC_ALRMBR_MNU);

  
  // RTC WakeUpTimer Interrupt Configuration: EXTI configuration 
//  SET_BIT(EXTI->IMR, EXTI_IMR_IM17);
//  SET_BIT(EXTI->RTSR, EXTI_IMR_IM17);

//  NVIC_SetPriority(RTC_Alarm_IRQn, 3);
//  NVIC_EnableIRQ(RTC_Alarm_IRQn);

  // Configure the Interrupt in the RTC_CR register
  SET_BIT(RTC->CR, RTC_CR_ALRBIE);
  
  // Enable the Alarm A
  SET_BIT(RTC->CR, RTC_CR_ALRBE);

  // Enable the write protection for RTC registers
  RTC->WPR = 0xFF;
}




//-----------------------------------------------------------------------------
static void InitSystemClock(void)
{
  // Enable HSI (16MHz)
  SET_BIT(RCC->CR, RCC_CR_HSION);
  // Wait until HSI Ready
  while(READ_BIT(RCC->CR, RCC_CR_HSIRDY) == 0);
  
  // Set HSI as System Clock
  MODIFY_REG(RCC->CFGR, RCC_CFGR_SW, RCC_CFGR_SW_HSI);
 
  // Set 1.5V (Range 2)
  MODIFY_REG(PWR->CR, PWR_CR_VOS, PWR_CR_VOS_1);
  
  // Disable MSI
  CLEAR_BIT(RCC->CR, RCC_CR_MSION);

  // ���������� ��������� �������  
  SystemCoreClockUpdate();
}

//-----------------------------------------------------------------------------
/*
void RTC_IRQHandler(void)
{
  // Check Wakeup timer Flag
  if(READ_BIT(RTC->ISR, RTC_ISR_WUTF)) 
  {
    // Get the status of the Interrupt 
    if(RTC->CR & RTC_CR_WUTIE)
    { 
      // Set Flag
      WakeUpTimerFlag = 1;
      
      // Clear Wake Up Flag
      PWR->CR |= PWR_CR_CWUF;
      
      // Clear the WAKEUPTIMER interrupt pending bit
      CLEAR_BIT(RTC->ISR, RTC_ISR_WUTF);
    }
    
    // Clear the EXTI's line Flag for RTC WakeUpTimer
    SET_BIT(EXTI->PR,EXTI_IMR_IM20);
  }
    // Check Alarm Flag A
  if(READ_BIT(RTC->ISR, RTC_ISR_ALRAF)) 
  {
    // Get the status of the Interrupt 
    if(RTC->CR & RTC_CR_ALRAIE) 
    { 
      // Increase Time
      SystemUpTimeSecond++;
      AlarmFlag = 1;
      
      // Clear Wake Up Flag
      PWR->CR |= PWR_CR_CWUF;
      
      // Clear the ALARM A interrupt pending bit
      CLEAR_BIT(RTC->ISR, RTC_ISR_ALRAF);
      
      if (KeyPressed==KEY_PRESSED_DOWN)
      {
        PressKeyDelay++;
        if(PressKeyDelay > 2)
           Switch_Sub_Menu();
      }  
    }
  }  
        // Check Alarm Flag B
  if(READ_BIT(RTC->ISR, RTC_ISR_ALRBF)) 
  {
    // Get the status of the Interrupt 
    if(RTC->CR & RTC_CR_ALRBIE) 
    { 
      IntArcFlag = 1;
      // Clear Wake Up Flag
      PWR->CR |= PWR_CR_CWUF;
      // Clear the ALARM A interrupt pending bit
      CLEAR_BIT(RTC->ISR, RTC_ISR_ALRBF);
    }

  } 
    // Clear the EXTI's line Flag for RTC Alarm
    SET_BIT(EXTI->PR, EXTI_IMR_IM17);
  
}
*/
//-----------------------------------------------------------------------------
void SetDate_Time(LL_RTC_DateTypeDef* NewDate,LL_RTC_TimeTypeDef* NewTime)
{
  uint32_t H1,M1,S1,T1;
  uint32_t tickstart = 0U;
  NewDate->WeekDay = GetDayOfWeek(NewDate);
  taskENTER_CRITICAL();
  RTC_DisableWriteProtection();
  RTC->ISR = RTC_ISR_INIT;
  while((RTC->ISR & RTC_ISR_INITF)!=RTC_ISR_INITF)
      __no_operation();

  H1 = __LL_RTC_CONVERT_BIN2BCD(NewDate->Year);
  M1 = __LL_RTC_CONVERT_BIN2BCD(NewDate->Month);
  S1 = __LL_RTC_CONVERT_BIN2BCD(NewDate->Day);
  T1 = __LL_RTC_CONVERT_BIN2BCD(NewDate->WeekDay);
  LL_RTC_DATE_Config(RTC,T1,S1,M1,H1);

  H1 = __LL_RTC_CONVERT_BIN2BCD(NewTime->Hours);
  M1 = __LL_RTC_CONVERT_BIN2BCD(NewTime->Minutes);
  S1 = __LL_RTC_CONVERT_BIN2BCD(NewTime->Seconds);
  T1 = LL_RTC_TIME_FORMAT_AM_OR_24;;  
  LL_RTC_TIME_Config(RTC,T1, H1, M1, S1);

  
  RTC->ISR =~ RTC_ISR_INIT;
  tickstart = xTaskGetTickCount();
//  while((RTC->ISR & RTC_ISR_INITF)==RTC_ISR_INITF)
  //    __no_operation();
  if((RTC->CR & RTC_CR_BYPSHAD) == RESET)
  {
    while((RTC->ISR & RTC_ISR_RSF) == RESET)
    {
       if((xTaskGetTickCount() - tickstart ) > 1000)
       {
          break;
       }
    }
  }

  RTC_EnableWriteProtection();
  taskEXIT_CRITICAL();
} 
//-----------------------------------------------------------------------------
void RTC_SetDateTimeForUnix(time_t time)
{
  LL_RTC_DateTypeDef loDate;
  LL_RTC_TimeTypeDef loTime;
  struct tm lo_tm;
  
  memcpy(&lo_tm, _gmtime_((time_t*)&time), sizeof(lo_tm));
   
  loDate.Day = lo_tm.tm_mday;
  loDate.Month = lo_tm.tm_mon + 1;
  loDate.Year = lo_tm.tm_year + 1900 - 2000;
   
  loTime.Hours = lo_tm.tm_hour;
  loTime.Minutes = lo_tm.tm_min;
  loTime.Seconds = lo_tm.tm_sec;
  loTime.TimeFormat = LL_RTC_TIME_FORMAT_AM_OR_24;
   
  lo_tm.tm_hour = Mode_Transfer.hour;
  lo_tm.tm_min = Mode_Transfer.minute;
  lo_tm.tm_sec = 0;
  
  SetDate_Time(&loDate, &loTime);
}


//-----------------------------------------------------------------------------
void GetDate_Time(LL_RTC_DateTypeDef* Date,LL_RTC_TimeTypeDef* Time)
{
  uint32_t temp;
  
  temp = RTC->DR;
  
  Date->WeekDay = __LL_RTC_CONVERT_BCD2BIN((temp& RTC_DR_WDU) >> RTC_DR_WDU_Pos);
  Date->Day    = __LL_RTC_CONVERT_BCD2BIN((((temp & RTC_DR_DT) >> RTC_DR_DT_Pos) << 4U) | ((temp & RTC_DR_DU) >> RTC_DR_DU_Pos));
  Date->Month  = __LL_RTC_CONVERT_BCD2BIN((((temp & RTC_DR_MT) >> RTC_DR_MT_Pos) << 4U) | ((temp & RTC_DR_MU) >> RTC_DR_MU_Pos));
  Date->Year   = __LL_RTC_CONVERT_BCD2BIN((((temp & RTC_DR_YT) >> RTC_DR_YT_Pos) << 4U) | ((temp & RTC_DR_YU) >> RTC_DR_YU_Pos));
    
  temp = RTC->TR;
  
  Time->Hours   = __LL_RTC_CONVERT_BCD2BIN((((temp & RTC_TR_HT) >> RTC_TR_HT_Pos) << 4U) | ((temp & RTC_TR_HU) >> RTC_TR_HU_Pos));
  Time->Minutes = __LL_RTC_CONVERT_BCD2BIN((((temp & RTC_TR_MNT) >> RTC_TR_MNT_Pos) << 4U) | ((temp & RTC_TR_MNU) >> RTC_TR_MNU_Pos));
  Time->Seconds = __LL_RTC_CONVERT_BCD2BIN((((temp & RTC_TR_ST) >> RTC_TR_ST_Pos) << 4U) | ((temp & RTC_TR_SU) >> RTC_TR_SU_Pos));
}

/*-----------------------------------------------------------------------------
void initPeriodicTimer(void)
{
  SET_BIT(RCC->APB1ENR,RCC_APB1ENR_TIM7EN);
  TIM7->PSC = 16000-1;  
  TIM7->ARR = 0xFFFF;
//  SET_BIT(TIM7->DIER,TIM_DIER_UIE);
//  NVIC_SetPriority(TIM7_IRQn,1);
//  NVIC_EnableIRQ(TIM7_IRQn);
}*/

/*-----------------------------------------------------------------------------
void DeInitPeriodicTimer(void)
{
  CLEAR_BIT(TIM7->CR1,TIM_CR1_CEN);
  CLEAR_BIT(RCC->APB1ENR,RCC_APB1ENR_TIM7EN);
}
//-----------------------------------------------------------------------------
void startPeriodicTimer(void)
{
   TIM7->CNT = 0;
   SET_BIT(TIM7->CR1,TIM_CR1_CEN);    
}

//-----------------------------------------------------------------------------
uint16_t stopPeriodicTimer(void)
{  
   CLEAR_BIT(TIM7->CR1,TIM_CR1_CEN); 
   return(TIM7->CNT);
}*/  

//-----------------------------------------------------------------------------
//void RTC_GetDate(uint32_t RTC_Format,LL_RTC_DateTypeDef* p_Date)
void RTC_GetDate(/*uint32_t RTC_Format,*/LL_RTC_DateTypeDef* p_Date)
{
   register uint32_t temp = 0U;
   temp = RTC->DR;
  p_Date->Day= __LL_RTC_CONVERT_BCD2BIN(__LL_RTC_GET_DAY(temp));
  p_Date->Month =__LL_RTC_CONVERT_BCD2BIN(__LL_RTC_GET_MONTH(temp));                                            
  p_Date->Year  =__LL_RTC_CONVERT_BCD2BIN(__LL_RTC_GET_YEAR(temp));
} 

//-----------------------------------------------------------------------------
void RTC_GetTime(/*uint32_t RTC_Format,*/LL_RTC_TimeTypeDef* p_Time)
//void RTC_GetTime(uint32_t RTC_Format,LL_RTC_TimeTypeDef* p_Time)
{
   register uint32_t temp = 0U;
   temp = RTC->TR;
  p_Time->Hours   = __LL_RTC_CONVERT_BCD2BIN(__LL_RTC_GET_HOUR(temp));
  p_Time->Minutes = __LL_RTC_CONVERT_BCD2BIN(__LL_RTC_GET_MINUTE(temp));                                            
  p_Time->Seconds = __LL_RTC_CONVERT_BCD2BIN(__LL_RTC_GET_SECOND(temp));
} 

