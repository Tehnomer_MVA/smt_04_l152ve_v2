//=============================================================================
//    S E N S I R I O N   AG,  Laubisruetistr. 50, CH-8712 Staefa, Switzerland
//=============================================================================
/// \file    Nvm.c
/// \author  RFU
/// \date    07-Jan-2016
/// \brief   Module for the non-volatile memory
//=============================================================================
// COPYRIGHT
//=============================================================================
// Copyright (c) 2014, Sensirion AG
// All rights reserved.
// The content of this code is confidential. Redistribution and use in source 
// and binary forms, with or without modification, are not permitted.
// 
// THIS CODE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
// FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
// DAMAGES(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
// SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
// CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT 
// LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY 
// OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH 
// DAMAGE.
//=============================================================================

#include "Nvm.h"
#include "Eeprom.h"

//-----------------------------------------------------------------------------
Error Nvm_ReadBootCounter(uint32_t* value)
{
  Error error;
  
  error = Eeprom_ReadDword(NVM_ADDRESS_BOOT_COUNTER, value);
  Error_AppendIfError(&error, ERROR_NVM_READ);
  
  return error;
}

//-----------------------------------------------------------------------------
Error Nvm_WriteBootCounter(uint32_t value)
{
  Error error;
  
  error = Eeprom_WriteDword(NVM_ADDRESS_BOOT_COUNTER, value);
  Error_AppendIfError(&error, ERROR_NVM_WRITE);
  
  return error;
}

//-----------------------------------------------------------------------------
Error Nvm_ReadCrc32Program(uint32_t* value)
{
  return Eeprom_ReadDword(NVM_ADDRESS_CRC32_PROGRAM, value);
}

//-----------------------------------------------------------------------------
Error Nvm_WriteCrc32Program(uint32_t value)
{
  return Eeprom_WriteDword(NVM_ADDRESS_CRC32_PROGRAM, value);
}

//-----------------------------------------------------------------------------
Error Nvm_ReadCrc32Parameter(uint32_t* value)
{
  return Eeprom_ReadDword(NVM_ADDRESS_CRC32_PARAMETERS, value);
}

//-----------------------------------------------------------------------------
Error Nvm_WriteCrc32Parameter(uint32_t value)
{
  return Eeprom_WriteDword(NVM_ADDRESS_CRC32_PARAMETERS, value);
}

//-----------------------------------------------------------------------------
Error Nvm_ReadLastSuccessfulBoot(uint32_t* value)
{
  return Eeprom_ReadDword(NVM_ADDRESS_LAST_SUCCESSFUL_BOOT, value);
}

//-----------------------------------------------------------------------------
Error Nvm_WriteLastSuccessfulBoot(uint32_t value)
{
  return Eeprom_WriteDword(NVM_ADDRESS_LAST_SUCCESSFUL_BOOT, value);
}

//-----------------------------------------------------------------------------
Error Nvm_ReadTestModePassword(uint32_t* value)
{
  return Eeprom_ReadDword(NVM_ADDRESS_TEST_MODE_PASSWORD, value);
}

//-----------------------------------------------------------------------------
Error Nvm_WriteTestModePassword(uint32_t value)
{
  return Eeprom_WriteDword(NVM_ADDRESS_TEST_MODE_PASSWORD, value);
}

//-----------------------------------------------------------------------------
Error Nvm_ReadErrorCounter(uint32_t* value, uint8_t index)
{
  Error error = Error_None();
  uint32_t address;
  
  if(index > 7) {
    Error_Add(&error, ERROR_NVM_INDEX_OUT_OF_RANGE);
  } else {
    address = NVM_ADDRESS_ERROR_COUNTERS + 4 * index;
    error = Eeprom_ReadDword(address, value);
    Error_AppendIfError(&error, ERROR_NVM_READ);
  }
  
  return error;
}

//-----------------------------------------------------------------------------
Error Nvm_WriteErrorCounter(uint32_t value, uint8_t index)
{
  Error error = Error_None();
  uint32_t address;
  
  if(index > 7) {
    Error_Add(&error, ERROR_NVM_INDEX_OUT_OF_RANGE);
  } else {
    address = NVM_ADDRESS_ERROR_COUNTERS + 4 * index;
    error = Eeprom_WriteDword(address, value);
    Error_AppendIfError(&error, ERROR_NVM_WRITE);
  }
  
  return error;
}

//-----------------------------------------------------------------------------
Error Nvm_ReadBackupBlock(BackupBlock* backup, uint8_t index)
{
  Error error = Error_None();
  uint32_t address;
  
  if(index > 7) {
    Error_Add(&error, ERROR_NVM_INDEX_OUT_OF_RANGE);
  } 
  
  if(Error_IsNone(error)) {
    address = NVM_ADDRESS_BACKUP_BLOCKS + NVM_BLOCK_SIZE_BACKUP_BLOCK * index;
    error = Eeprom_ReadQword(address, (uint64_t*)&(backup->Itmes.Volume));
    Error_AppendIfError(&error, ERROR_NVM_READ);
  }
  
  if(Error_IsNone(error)) {
    error = Eeprom_ReadDword(address + 8, &(backup->Itmes.Timestamp));
    Error_AppendIfError(&error, ERROR_NVM_READ);
  }
  
  if(Error_IsNone(error)) {
    error = Eeprom_ReadDword(address + 12, &(backup->Itmes.Crc32));
    Error_AppendIfError(&error, ERROR_NVM_READ);
  }
  
  return error;
}

//-----------------------------------------------------------------------------
Error Nvm_WriteBackupBlock(BackupBlock backup, uint8_t index)
{
  Error error = Error_None();
  uint32_t address;
  
  if(index > 7) {
    Error_Add(&error, ERROR_NVM_INDEX_OUT_OF_RANGE);
  }
  
  if(Error_IsNone(error)) {
    address = NVM_ADDRESS_BACKUP_BLOCKS + NVM_BLOCK_SIZE_BACKUP_BLOCK * index;
    error = Eeprom_WriteQword(address, *(uint64_t*)&backup.Itmes.Volume);
    Error_AppendIfError(&error, ERROR_NVM_WRITE);
  }
  
  if(Error_IsNone(error)) {
    error = Eeprom_WriteDword(address + 8, backup.Itmes.Timestamp);
    Error_AppendIfError(&error, ERROR_NVM_WRITE);
  }
  
  if(Error_IsNone(error)) {
    error = Eeprom_WriteDword(address + 12, backup.Itmes.Crc32);
    Error_AppendIfError(&error, ERROR_NVM_WRITE);
  }
  
  return error;
}

//-----------------------------------------------------------------------------
Error Nvm_ReadLastBatteryReplacement(uint32_t* timestamp)
{
  Error error;
  
  error = Eeprom_ReadDword(NVM_ADDRESS_LAST_BATTERY_REPLACEMENT, timestamp);
  
  return error;
}

//-----------------------------------------------------------------------------
Error Nvm_WriteLastBatteryReplacement(uint32_t timestamp)
{
  Error error;
  
  error = Eeprom_WriteDword(NVM_ADDRESS_LAST_BATTERY_REPLACEMENT, timestamp);
  
  return error;
}

//-----------------------------------------------------------------------------
Error Nvm_ReadCorrectionLut(FlowCorrectionLut* value, uint8_t index)
{
  Error error = Error_None();
  uint32_t address;
  uint16_t length;
  uint8_t i;
  
  if(index > 3) {
    Error_Add(&error, ERROR_NVM_INDEX_OUT_OF_RANGE);
  }

  if(Error_IsNone(error)) {
    address = NVM_ADDRESS_CORRECTION_LUTS + NVM_BLOCK_SIZE_CORRECTION_LUT * index;
    error = Eeprom_ReadWord(address, &length);
  }
    
  if(Error_IsNone(error)) {
    if(length > FLOW_CORR_MAX_NUMBER_OF_SAMPLING_POINTS) {
      Error_Add(&error, ERROR_NVM_LUT_IS_TOO_LARGE);
    } else {
      value->length = length;
    }
  }
  
  for(i = 0; i < length; i++) {
    if(Error_IsError(error)) break;
    address += 2;
    error = Eeprom_ReadWord(address, &(value->samplingPoints[i].x));
    Error_AppendIfError(&error, ERROR_NVM_READ);
    
    if(Error_IsError(error)) break;
    address += 2;
    error = Eeprom_ReadWord(address, (uint16_t*)&(value->samplingPoints[i].y));
    Error_AppendIfError(&error, ERROR_NVM_READ);
  }

  return error;
}

//-----------------------------------------------------------------------------
Error Nvm_WriteCorrectionLut(FlowCorrectionLut value, uint8_t index)
{
  Error error = Error_None();
  uint32_t address;
  uint8_t i;
  
  if(index > 3) {
    Error_Add(&error, ERROR_NVM_INDEX_OUT_OF_RANGE);
  }
  
  if(Error_IsNone(error)) {
    if(value.length > FLOW_CORR_MAX_NUMBER_OF_SAMPLING_POINTS) {
      Error_Add(&error, ERROR_NVM_LUT_IS_TOO_LARGE);
    }
  }

  if(Error_IsNone(error)) {
    address = NVM_ADDRESS_CORRECTION_LUTS + NVM_BLOCK_SIZE_CORRECTION_LUT * index;
    error = Eeprom_WriteWord(address, value.length);
  }
    
  for(i = 0; i < value.length; i++) {
    if(Error_IsError(error)) break;
    address += 2;
    error = Eeprom_WriteWord(address, value.samplingPoints[i].x);
    Error_AppendIfError(&error, ERROR_NVM_WRITE);
    
    if(Error_IsError(error)) break;
    address += 2;
    error = Eeprom_WriteWord(address, value.samplingPoints[i].y);
    Error_AppendIfError(&error, ERROR_NVM_WRITE);
  }

  return error;
}

//-----------------------------------------------------------------------------
Error Nvm_ReadErrorLogEntry(Error* value, uint8_t logBlockIndex,
      uint8_t entryIndex)
{
  Error error = Error_None();
  return Error_Add(&error, ERROR_NVM_NOT_IMPLEMENTED);
}

//-----------------------------------------------------------------------------
Error Nvm_WriteErrorLogEntry(Error value, uint8_t logBlockIndex,
      uint8_t entryIndex)
{
  Error error = Error_None();
  return Error_Add(&error, ERROR_NVM_NOT_IMPLEMENTED);
}
