//=============================================================================
//    S E N S I R I O N   AG,  Laubisruetistr. 50, CH-8712 Staefa, Switzerland
//=============================================================================
/// \file    ErrorLog.h
/// \author  RFU
/// \date    12-Feb-2016
/// \brief   Module for error logging
//=============================================================================
// COPYRIGHT
//=============================================================================
// Copyright (c) 2014, Sensirion AG
// All rights reserved.
// The content of this code is confidential. Redistribution and use in source 
// and binary forms, with or without modification, are not permitted.
// 
// THIS CODE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
// FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
// DAMAGES(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
// SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
// CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT 
// LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY 
// OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH 
// DAMAGE.
//=============================================================================

#ifndef ERRORLOG_H
#define ERRORLOG_H

#include "Typedef.h"
#include "Error.h"

typedef enum{
  ERRORLOG_WATCHDOG_RESET             = 0, // Counts watch dog resets.
  ERRORLOG_GASMETER_FAILURE           = 1, // Counts each error in Gasmeter_Process() .
  ERRORLOG_REPEATED_GASMETER_FAILURE  = 2, // Counts repeated error in Gasmeter_Process(), failed 3 times in a row.
  ERRORLOG_BOOT_FAILURE               = 3, // Counts failed boot procedures.
  ERRORLOG_REPEATED_BOOT_FAILURE      = 4, // Counts repeated boot failures, failed 2 times in a row.
  ERRORLOG_GASREC_NOT_SUCCESSFUL      = 5  // Counts faild gas recognitions.
} ErrorCounter;

//-----------------------------------------------------------------------------
/// \brief  Sets all error counter to 0.                                        
/// \return Error information, => Error_None() if no error occurred.
Error ErrorLog_ResetErrorCounters(void);

//-----------------------------------------------------------------------------
/// \brief  Increments the specified error counter by one.                                                                   
/// \param  errorCounter  Specifies the error counter.
/// \return Error information, => Error_None() if no error occurred.
Error ErrorLog_IncrementErrorCounter(ErrorCounter errorCounter);

//-----------------------------------------------------------------------------
/// \brief  Reads the value of the specified error counter.                    
/// \param  errorCounter  Specifies the error counter.
/// \param  value         Pointer to return the value of the error counter.
/// \return Error information, => Error_None() if no error occurred.
Error ErrorLog_ReadErrorCounter(ErrorCounter errorCounter, uint32_t* value);

#endif /* ERRORLOG_H */
