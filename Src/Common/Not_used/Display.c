//=============================================================================
//    S E N S I R I O N   AG,  Laubisruetistr. 50, CH-8712 Staefa, Switzerland
//=============================================================================
/// \file    Display.c
/// \author  LWI
/// \date    18-Dec-2014
/// \brief   Module for Display modifications
//=============================================================================
// COPYRIGHT
//=============================================================================
// Copyright (c) 2014, Sensirion AG
// All rights reserved.
// The content of this code is confidential. Redistribution and use in source 
// and binary forms, with or without modification, are not permitted.
// 
// THIS CODE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
// FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
// DAMAGES(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
// SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
// CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT 
// LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY 
// OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH 
// DAMAGE.
//=============================================================================

#include "Display.h"

                                             //      a
                                             //    -----
                                             //   |     |
                                             //  f|     |b
                                             //   |  g  |
                                             //    -----
                                             //   |     |
                                             //  e|     |c
                                             //   |  d  |
                                             //    -----
                                             //
                                             //   xgfedcba
static const uint8_t _digitLookup[] = {0x3F, // 0b00111111, // 0 
                                       0x06, // 0b00000110, // 1
                                       0x5B, // 0b01011011, // 2
                                       0x4F, // 0b01001111, // 3
                                       0x66, // 0b01100110, // 4
                                       0x6D, // 0b01101101, // 5
                                       0x7D, // 0b01111101, // 6
                                       0x07, // 0b00000111, // 7
                                       0x7F, // 0b01111111, // 8
                                       0x6F, // 0b01101111, // 9
                                       0x77, // 0b01110111, // A
                                       0x7C, // 0b01111100, // b
                                       0x39, // 0b00111001, // c
                                       0x5E, // 0b01011110, // d
                                       0x79, // 0b01111001, // e
                                       0x71, // 0b01110001, // f
                                       0x00, // 0b00000000, //   (clear)
                                       0x40, // 0b01000000, // - (neg)
                                       0x3E, // 0b00111110, // U
                                       0x50, // 0b01010000, // r
                                       0x76, // 0b01110110, // H
                                       0x38, // 0b00111000, // L
                                       0x73, // 0b01110011, // P
                                       0x30, // 0b00110000, // I
                                      };

//-----------------------------------------------------------------------------
static void Display_WriteFloat2InRam(uint8_t maxDecs, float number);
static bool IsNegativeFloat(float value);
static void WriteLine1Err(void);
static void WriteLine1ErrorCode(uint8_t part1, uint8_t part2);
static void WriteLine2Prefix(DisplayDigit prefix);
static void WriteLine2ErrorCode(uint8_t part1, uint8_t part2);
static void WriteLine2Hex4Digits(uint16_t digits);

//-----------------------------------------------------------------------------
void Display_InitHardware()
{  
  Lcd_Init();
  Display_AllOnInRam();
  Display_Update(); 
  Display_Enable();
}

//-----------------------------------------------------------------------------
void Display_ClearRam(void)
{
  uint8_t i;
  
  while(Lcd_IsBusy()) {
    // Wait for finished update
  }
  // Set all segments off
  for(i = 0; i < 8; i++) {
    Lcd_WriteCOMDataInRam(i, 0x00000000);
  }
}

//-----------------------------------------------------------------------------
void Display_AllOnInRam(void)
{
  uint8_t i;
  
  while(Lcd_IsBusy()) {
    // Wait for finished update
  }
  // Set all segments on
  for(i = 0; i < 8; i++) {
    Lcd_WriteCOMDataInRam(i, 0xFFFFFFFF);
  }
}

//-----------------------------------------------------------------------------
void Display_Update(void)
{
  Lcd_Update();
}

//-----------------------------------------------------------------------------
void Display_Enable(void)
{
  Lcd_Enable();
}

//-----------------------------------------------------------------------------
void Display_Disable(void)
{
  Lcd_Disable();
}

void Display_EnableLogo(void)
{
  Display_WriteSymbolInRam(SYMBOL_SENSIRION, true);
}

//-----------------------------------------------------------------------------
void Display_WriteFloat1InRam(uint8_t maxDecs, float number)
{
  bool    isNegative;
  uint8_t decimalPlaces ;

  // Limit range of number
  if(number > 9999.0f) {
    number = 9999.0f;
  }
  if(number < -999.0f) {
    number = -999.0f;
  }
  
  isNegative = IsNegativeFloat(number);
  
  // use only positive numbers for further calculations
  if(isNegative) {
    number = -number;
  }
  
  // determine the maximum number of decimal places in case of a positive number
  if(number < 9.9995f) {
    decimalPlaces = 3;
  } else if (number < 99.995f) {
    decimalPlaces = 2;
  } else if (number < 999.95f) {
    decimalPlaces = 1;
  } else {
    decimalPlaces = 0;
  }
  
  // one decimal less for negative numbers
  if(isNegative) {
    decimalPlaces--;
  }
  
  // limit the number of decimal places by the specified maximum
  if(decimalPlaces > maxDecs) {
    decimalPlaces = maxDecs;
  }
  
  // scaling for integer displaying
  switch(decimalPlaces) {
    case 0:
      break;
    case 1:
      number *= 10.0f;
      break;
    case 2:
      number *= 100.0f;
      break;
    case 3:
      number *= 1000.0f;
      break;
  }
  
  // +0.5 for rounding
  number += 0.5f;
  
  Display_WriteLine1InRam(isNegative, decimalPlaces, (uint16_t)(number));
}

//-----------------------------------------------------------------------------
void Display_WriteLine1InRam(bool negative, uint8_t decs, uint16_t number)
{
  uint8_t i;
  uint8_t temp;
  bool    enableZeros = false;
  
  // Clear all decimal points on Line 1
  for(i = 7; i <= 9; i++) {
    Display_WriteDpInRam(i, false);
  }
  
  // set decimal point
  if(decs != 0) {
    Display_WriteDpInRam(10 - decs, true);
  }
  
  // limit number
  if(negative) {
    if(number > 999) {
      number = 999;
    }
  } else {
    if(number > 9999) {
      number = 9999;
    }
  }
  
  // Digit 7
  temp = number / 1000;
  if((temp != 0) || (decs >= 3)) {
    if(!negative) { // No first digit for negative number
      Display_WriteDigitInRam(7, (DisplayDigit)temp);
      enableZeros = true;
    }
  }
  else Display_WriteDigitInRam(7, DIGIT_CLEAR); // clear digit
  
  // Digit 8
  number = number % 1000;
  temp = number / 100;
  if((temp != 0) || enableZeros || (decs >= 2)) {
    if(negative) { // write Neg once
      Display_WriteDigitInRam(7, DIGIT_NEGATIV);
      negative = false;
    }
    Display_WriteDigitInRam(8, (DisplayDigit)temp);
    enableZeros = true;
  } else {
    Display_WriteDigitInRam(8, DIGIT_CLEAR); // clear digit
  }
  
  // Digit 9
  number = number % 100;
  temp = number / 10;
  if((temp != 0) || enableZeros || (decs >= 1)) {
    if(negative) { // write Neg once
      Display_WriteDigitInRam(8, DIGIT_NEGATIV);
      negative = false;
    }
    Display_WriteDigitInRam(9, (DisplayDigit)temp);
    enableZeros = true;
  } else {
    Display_WriteDigitInRam(9, DIGIT_CLEAR); // clear digit
  }
  
  // Digit 10
  temp = number % 10;
  if(negative) { // write Neg once
    Display_WriteDigitInRam(9, DIGIT_NEGATIV);
    negative = false;
  }
  Display_WriteDigitInRam(10, (DisplayDigit)temp);
  enableZeros = true;
}

//-----------------------------------------------------------------------------
void Display_WriteLine2InRam(bool negative, uint8_t decs, uint32_t number,DisplayDigit prefix)
{
  uint8_t  i;
  uint16_t firstPart;
  uint16_t secondPart;
  uint8_t  temp;
  bool     enableZeros = false;
  
  // Clear all decimal points on Line 1
  for(i = 0; i <= 6; i++)
    Display_WriteDpInRam(i, false);
  // set DP
  if(decs != 0) Display_WriteDpInRam(6 - decs, true);
  
  // limit Number
  if(negative) {
    if(number > 999999) {
      number = 999999;
    }
  } else {
    if(number > 9999999) {
      number = 9999999;
    }
  }
  
  firstPart = number / 10000;
  secondPart = number % 10000;
  
  // Digit 0
  temp = firstPart / 100;
  if((temp != 0) || (decs >= 6)) {
    if(!negative) { // no first Digit for negative Number
      Display_WriteDigitInRam(0, (DisplayDigit)temp);
      enableZeros = true;
    }
  }
  else Display_WriteDigitInRam(0, DIGIT_CLEAR); // clear Digit

  // Digit 1
  firstPart = firstPart % 100;
  temp = firstPart / 10;
  if((temp != 0) || enableZeros || (decs >= 5)) {
    if(negative) { // write Neg once  
      Display_WriteDigitInRam(0, DIGIT_NEGATIV);
      negative = false;
    }
    Display_WriteDigitInRam(1, (DisplayDigit)temp);
    enableZeros = true;
  }
  else Display_WriteDigitInRam(1, DIGIT_CLEAR); // clear Digit

  // Digit 2
  temp = firstPart % 10;
  if((temp != 0) || enableZeros || (decs >= 4)) {
    if(negative) { // write Neg once 
      Display_WriteDigitInRam(1, DIGIT_NEGATIV);
      negative = false;
    }
    Display_WriteDigitInRam(2, (DisplayDigit)temp);
    enableZeros = true;
  }
  else Display_WriteDigitInRam(2, DIGIT_CLEAR); // clear Digit
  
  // Digit 3
  temp = secondPart / 1000;
  if((temp != 0) || enableZeros || (decs >= 3)) {
    if(negative) { // write Neg once
      Display_WriteDigitInRam(2, DIGIT_NEGATIV);
      negative = false;
    }
    Display_WriteDigitInRam(3, (DisplayDigit)temp);
    enableZeros = true;
  }
  else Display_WriteDigitInRam(3, DIGIT_CLEAR); // clear Digit
  
  // Digit 4
  secondPart = secondPart % 1000;
  temp = secondPart / 100;
  if((temp != 0) || enableZeros || (decs>=2)) {
    if(negative) { // write Neg once 
      Display_WriteDigitInRam(3, DIGIT_NEGATIV);
      negative = false;
    }
    Display_WriteDigitInRam(4, (DisplayDigit)temp);
    enableZeros = true;
  }
  else Display_WriteDigitInRam(4, DIGIT_CLEAR); // clear Digit
  
  // Digit 5
  secondPart = secondPart % 100;
  temp = secondPart / 10;
  if((temp != 0) || enableZeros || (decs >= 1)) {
    if(negative) {// write Neg once
      Display_WriteDigitInRam(4, DIGIT_NEGATIV);
      negative = false;
    }
    Display_WriteDigitInRam(5, (DisplayDigit)temp);
    enableZeros = true;
  }
  else Display_WriteDigitInRam(5, DIGIT_CLEAR); // clear Digit
  
  // Digit 6
  temp = secondPart % 10;
  if(negative) { // write Neg once 
    Display_WriteDigitInRam(5, DIGIT_NEGATIV);
    negative = false;
  }
  Display_WriteDigitInRam(6, (DisplayDigit)temp);
  enableZeros = true;
  if (prefix != 0)
    WriteLine2Prefix(prefix);
}

//-----------------------------------------------------------------------------
void Display_WriteDigitInRam(uint8_t position, DisplayDigit digit)
{
  if(position <= 3) { // Digit 0-3 
    position = position * 2;
    Lcd_WriteSegmentInRam(0, 15 - position, _digitLookup[digit] & 0x01); // Segment A
    Lcd_WriteSegmentInRam(0, 14 - position, _digitLookup[digit] & 0x02); // Segment B
    Lcd_WriteSegmentInRam(2, 14 - position, _digitLookup[digit] & 0x04); // Segment C
    Lcd_WriteSegmentInRam(3, 15 - position, _digitLookup[digit] & 0x08); // Segment D
    Lcd_WriteSegmentInRam(2, 15 - position, _digitLookup[digit] & 0x10); // Segment E
    Lcd_WriteSegmentInRam(1, 15 - position, _digitLookup[digit] & 0x20); // Segment F
    Lcd_WriteSegmentInRam(1, 14 - position, _digitLookup[digit] & 0x40); // Segment G
  }else if (position <= 6) { // Digit 4-6
    position = position * 2;
    Lcd_WriteSegmentInRam(0, position - 8, _digitLookup[digit] & 0x01); // Segment A
    Lcd_WriteSegmentInRam(0, position - 7, _digitLookup[digit] & 0x02); // Segment B
    Lcd_WriteSegmentInRam(2, position - 7, _digitLookup[digit] & 0x04); // Segment C
    Lcd_WriteSegmentInRam(3, position - 8, _digitLookup[digit] & 0x08); // Segment D
    Lcd_WriteSegmentInRam(2, position - 8, _digitLookup[digit] & 0x10); // Segment E
    Lcd_WriteSegmentInRam(1, position - 8, _digitLookup[digit] & 0x20); // Segment F
    Lcd_WriteSegmentInRam(1, position - 7, _digitLookup[digit] & 0x40); // Segment G
  } else {// Digit 7-10
    position = position * 2;
    Lcd_WriteSegmentInRam(7, 29 - position, _digitLookup[digit] & 0x01); // Segment A
    Lcd_WriteSegmentInRam(7, 28 - position, _digitLookup[digit] & 0x02); // Segment B
    Lcd_WriteSegmentInRam(5, 28 - position, _digitLookup[digit] & 0x04); // Segment C
    Lcd_WriteSegmentInRam(4, 29 - position, _digitLookup[digit] & 0x08); // Segment D
    Lcd_WriteSegmentInRam(5, 29 - position, _digitLookup[digit] & 0x10); // Segment E
    Lcd_WriteSegmentInRam(6, 29 - position, _digitLookup[digit] & 0x20); // Segment F
    Lcd_WriteSegmentInRam(6, 28 - position, _digitLookup[digit] & 0x40); // Segment G
  }
}

//-----------------------------------------------------------------------------
void Display_WriteDpInRam(uint8_t position, bool enabled)
{  
  if(position <= 3) { // Digit 0-3
    position = position * 2;
    Lcd_WriteSegmentInRam(3, 14 - position, enabled); // Segment DP
  } else if (position <= 6) { // Digit 4-6
    position = position * 2;
    Lcd_WriteSegmentInRam(3, position - 7, enabled); // Segment DP
  } else {// Digit 7-10
    position = position * 2;
    Lcd_WriteSegmentInRam(4, 28 - position, enabled); // Segment DP
  }
}

//-----------------------------------------------------------------------------
void Display_WriteSymbolInRam(DisplaySymbol Symbol, bool enabled)
{ 
  switch(Symbol)
  {
    case SYMBOL_SENSIRION : Lcd_WriteSegmentInRam(7, 7, enabled); break; // S1
    case SYMBOL_RANGE     : Lcd_WriteSegmentInRam(6, 7, enabled); break; // S2
    case SYMBOL_CYCLE     : Lcd_WriteSegmentInRam(5, 7, enabled); break; // S3
    case SYMBOL_DOT       : Lcd_WriteSegmentInRam(4, 7, enabled); break; // S4
    case SYMBOL_PA        : Lcd_WriteSegmentInRam(6, 0, enabled); break; // S5
    case SYMBOL_RH1       : Lcd_WriteSegmentInRam(7, 1, enabled); break; // S6
    case SYMBOL_M3_1      : Lcd_WriteSegmentInRam(5, 0, enabled); break; // S7
    case SYMBOL_PH        : Lcd_WriteSegmentInRam(6, 1, enabled); break; // S8
    case SYMBOL_C1        : Lcd_WriteSegmentInRam(6, 2, enabled); break; // S9
    case SYMBOL_U         : Lcd_WriteSegmentInRam(4, 0, enabled); break; // S10
    case SYMBOL_L11       : Lcd_WriteSegmentInRam(5, 1, enabled); break; // S11
    case SYMBOL_PMIN11    : Lcd_WriteSegmentInRam(5, 2, enabled); break; // S12
    case SYMBOL_M1        : Lcd_WriteSegmentInRam(4, 8, enabled); break; // S13
    case SYMBOL_L12       : Lcd_WriteSegmentInRam(4, 1, enabled); break; // S14
    case SYMBOL_N1        : Lcd_WriteSegmentInRam(4, 2, enabled); break; // S15
    case SYMBOL_PMIN12    : Lcd_WriteSegmentInRam(4, 3, enabled); break; // S16
    case SYMBOL_RH2       : Lcd_WriteSegmentInRam(4, 4, enabled); break; // S17
    case SYMBOL_M3_2      : Lcd_WriteSegmentInRam(5, 5, enabled); break; // S18
    case SYMBOL_C2        : Lcd_WriteSegmentInRam(5, 4, enabled); break; // S19
    case SYMBOL_M2        : Lcd_WriteSegmentInRam(6, 5, enabled); break; // S20
    case SYMBOL_L2        : Lcd_WriteSegmentInRam(7, 5, enabled); break; // S21
    case SYMBOL_N2        : Lcd_WriteSegmentInRam(6, 4, enabled); break; // S22
    case SYMBOL_GAS       : Lcd_WriteSegmentInRam(7, 6, enabled); break; // S23
    case SYMBOL_NAT       : Lcd_WriteSegmentInRam(6, 6, enabled); break; // S24
    case SYMBOL_AIR       : Lcd_WriteSegmentInRam(5, 6, enabled); break; // S25
    case SYMBOL_FLOW      : Lcd_WriteSegmentInRam(0, 6, enabled); break; // S26
    case SYMBOL_MAX       : Lcd_WriteSegmentInRam(1, 6, enabled); break; // S27
    case SYMBOL_MIN       : Lcd_WriteSegmentInRam(2, 6, enabled); break; // S28
    case SYMBOL_LOWBATT   : Lcd_WriteSegmentInRam(3, 6, enabled); break; // S29
    default: break;
  }
}

//-----------------------------------------------------------------------------
void Display_WriteFwVersion(uint8_t major, uint8_t minor, uint8_t debug)
{
  Display_WriteSymbolInRam(SYMBOL_SENSIRION, true);
  Display_WriteDigitInRam(7, (DisplayDigit)(major/10));
  Display_WriteDigitInRam(8, (DisplayDigit)(major%10));
  Display_WriteDpInRam(8, true);
  Display_WriteDigitInRam(9, (DisplayDigit)(minor/10));
  Display_WriteDigitInRam(10, (DisplayDigit)(minor%10));
  if(0 != debug) 
  {
    Display_WriteDigitInRam(1, DIGIT_D);
    Display_WriteDigitInRam(2, DIGIT_E);
    Display_WriteDigitInRam(3, DIGIT_B);
    Display_WriteDigitInRam(4, DIGIT_CHAR_U);
    Display_WriteDigitInRam(5, DIGIT_9);
  }
}

//-----------------------------------------------------------------------------
void Display_WriteCurrentFlow(float flow)
{
  Display_WriteFloat1InRam(2, flow);
  Display_WriteSymbolInRam(SYMBOL_L12, 1);
  Display_WriteSymbolInRam(SYMBOL_N1, 1);
  Display_WriteSymbolInRam(SYMBOL_PMIN12, 1);
}

//-----------------------------------------------------------------------------
void Display_WriteTotalVolume(double totalVolume, uint8_t decimalPlaces)
{
  Display_WriteFloat2InRam(decimalPlaces, (float)totalVolume);
  Display_WriteSymbolInRam(SYMBOL_M3_2, 1);
}

//-----------------------------------------------------------------------------
void Display_WriteError(Error error, uint8_t page)
{
  switch(page) {
    case 0:
      WriteLine1Err();
      WriteLine2Prefix(DIGIT_CHAR_P);
      WriteLine2ErrorCode(error.Items.PrimaryError / 32, error.Items.PrimaryError % 32);
      break;
    case 1:
      WriteLine1ErrorCode(error.Items.LastError / 32, error.Items.LastError % 32);
      WriteLine2Prefix(DIGIT_CHAR_I);
      WriteLine2ErrorCode(error.Items.InnerError / 32, error.Items.InnerError % 32);
      break;
    default:
      break;
  }
}

//-----------------------------------------------------------------------------
void Display_WriteCrcError(Error error, uint32_t crc, uint8_t page)
{
  switch(page) {
    case 0:
      WriteLine1Err();
      WriteLine2Prefix(DIGIT_CHAR_H);
      WriteLine2Hex4Digits(crc >> 16);
      break;
    case 1:
      WriteLine1ErrorCode(error.Items.LastError / 32, error.Items.LastError % 32);
      WriteLine2Prefix(DIGIT_CHAR_L);
      WriteLine2Hex4Digits(crc);
      break;
    default:
      break;
  }
}

//-----------------------------------------------------------------------------
static void Display_WriteFloat2InRam(uint8_t decimalPlaces, float number)
{
  bool isNegative;

  // Limit range of number
  if(number > 9999999.0f) {
    number = 9999999.0f;
  }
  if(number < -999999.0f) {
    number = -999999.0f;
  }
  
  isNegative = IsNegativeFloat(number);
  
  // use only positive numbers for further calculations
  if(isNegative) {
    number = -number;
  }
  
  // scaling for integer displaying
  switch(decimalPlaces) {
    case 0:
      break;
    case 1:
      number *= 10.0f;
      break;
    case 2:
      number *= 100.0f;
      break;
    case 3:
      number *= 1000.0f;
      break;
    case 4:
      number *= 10000.0f;
      break;
    case 5:
      number *= 100000.0f;
      break;
    case 6:
      number *= 1000000.0f;
      break;
  }
  
  // +0.5 for rounding
  number += 0.5f;
  
  Display_WriteLine2InRam(isNegative, decimalPlaces, (uint32_t)(number),0);
}

//-----------------------------------------------------------------------------
static void WriteLine1Err(void)
{
    Display_WriteDigitInRam(7, DIGIT_E);
    Display_WriteDigitInRam(8, DIGIT_CHAR_r);
    Display_WriteDigitInRam(9, DIGIT_CHAR_r);
}

//-----------------------------------------------------------------------------
static void WriteLine1ErrorCode(uint8_t part1, uint8_t part2)
{
  Display_WriteDigitInRam(7, (DisplayDigit)(part1 / 16));
  Display_WriteDigitInRam(8, (DisplayDigit)(part1 % 16));
  Display_WriteDpInRam(8, true);
  Display_WriteDigitInRam(9, (DisplayDigit)(part2 / 16));
  Display_WriteDigitInRam(10, (DisplayDigit)(part2 % 16));
}

//-----------------------------------------------------------------------------
static void WriteLine2Prefix(DisplayDigit prefix)
{
  Display_WriteDigitInRam(0, prefix);
  Display_WriteDpInRam(0, true);
}

//-----------------------------------------------------------------------------
static void WriteLine2ErrorCode(uint8_t part1, uint8_t part2)
{
  WriteLine2Hex4Digits(part1 << 8 | part2);
  Display_WriteDpInRam(4, true);
}

//-----------------------------------------------------------------------------
static void WriteLine2Hex4Digits(uint16_t digits)
{
  int      i;
  uint8_t  digit;
  
  for(i = 0; i < 4; i++) {
    digit = digits & 0x0F;
    Display_WriteDigitInRam((6 - i), (DisplayDigit)digit);
    digits >>= 4;
  }
}

//-----------------------------------------------------------------------------
static bool IsNegativeFloat(float value)
{
  // returns true for values < 0.0f including -0.0f, false otherwise
  return *(int32_t*)(&value) < 0;
}
