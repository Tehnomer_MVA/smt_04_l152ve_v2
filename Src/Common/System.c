//=============================================================================
//   
//=============================================================================
/// \file    System.h
/// \author  
/// \date    
/// \brief   Watchdog timer, which restarts the device on unexpected hang up.
//=============================================================================
// COPYRIGHT
//=============================================================================
// 
//=============================================================================

#include "GlobalHandler.h"

#define CONFIG_USE_CATEGORY_WELMEC

#define IWDG_START         (uint32_t)0x0000CCCC
#define IWDG_WRITE_ACCESS  (uint32_t)0x00005555
#define IWDG_REFRESH       (uint32_t)0x0000AAAA

static bool isWatchdogResetOccurred;
static bool isSoftwareResetOccurred;
static void CheckResetFlags(void);
static void StartWatchdog(void);

//static const  

//-----------------------------------------------------------------------------
void System_Init(void)
{
  CheckResetFlags();
  
  //### StartWatchdog();
}

void System_Init_volue(void)
{
  uint8_t cell_volume, cell_volume_flow;  
  if(DayArcRecNo < 15330)
  {
    cell_volume = 6;
    cell_volume_flow = 12;
  }
  else
  {
    cell_volume = 7;
    cell_volume_flow = 13;
  }
  if(DayArcRecNo < 13140)
  {
    cell_volume = 5;
    cell_volume_flow = 11;
  }
  if(DayArcRecNo < 10950)
  {
    cell_volume = 4;
    cell_volume_flow = 10;
  }
  if(DayArcRecNo < 8760)
  {
    cell_volume = 3;
    cell_volume_flow = 3;
  }
  if(DayArcRecNo < 6570)
  {
    cell_volume = 2;
    cell_volume_flow = 2;
  }
  if(DayArcRecNo < 4380)
  {
    cell_volume = 1;
    cell_volume_flow = 1;
  }
  if(DayArcRecNo < 2190)
  {
    cell_volume = 0;
    cell_volume_flow = 0;
  }
  
  if(cell_volume_flow > 4)
  {
    cell_volume_flow -= 10;
    GasMeter.VE_Lm3 = VolumeBeforeReset2[cell_volume_flow];
  }
  else
     GasMeter.VE_Lm3 = VolumeBeforeReset[cell_volume_flow];
  //-----------------------------------
  GetDate_Time(&SystDate,&SystTime);
  if(SystDate.Year == 0)
     RTC_SetDateTimeForUnix(datetime_eeprom[cell_volume]);   
}
//-----------------------------------------------------------------------------
__ramfunc void System_WatchdogRefresh(void)
{
//  SET_BIT(RCC->IOPENR,RCC_IOPENR_IOPHEN); 
  SET_BIT(RCC->AHBENR,RCC_AHBENR_GPIOHEN);  
  CLEAR_BIT(GPIOH->MODER,GPIO_MODER_MODER1);  //  ���� WAKE
  MODIFY_REG(GPIOH->MODER, GPIO_MODER_MODER0, GPIO_MODER_MODER0_0); // ����� Done
  SET_BIT(GPIOH->ODR, GPIO_ODR_ODR_0);
  for (int i=0; i<50; i++)
   __no_operation();
  CLEAR_BIT(GPIOH->ODR, GPIO_ODR_ODR_0);
//  CLEAR_BIT(RCC->AHBENR,RCC_AHBENR_GPIOHEN);  

  //### IWDG->KR = IWDG_REFRESH;
}

//-----------------------------------------------------------------------------
void System_Reset(void)
{
  //TODO: add save to eeprom current data-time, volume 
  System_SaweBeforeReset();
  NVIC_SystemReset();
}

//-----------------------------------------------------------------------------
bool System_IsWatchdogResetOccurred(void)
{
  return isWatchdogResetOccurred;
}

//-----------------------------------------------------------------------------
bool System_IsSoftwareResetOccurred(void)
{
  return isSoftwareResetOccurred;
}

//-----------------------------------------------------------------------------
static void CheckResetFlags(void)
{
  // check if last reset was a watchdog reset
  isWatchdogResetOccurred = (RCC->CSR & RCC_CSR_IWDGRSTF) == RCC_CSR_IWDGRSTF;
  
  // check if last reset was a software reset
  isSoftwareResetOccurred = (RCC->CSR & RCC_CSR_IWDGRSTF) == RCC_CSR_SFTRSTF;

  // clear reset flags
//  RCC->CSR |= RCC_CSR_RMVF;
}

//-----------------------------------------------------------------------------
static void StartWatchdog(void)
{
  // Activate IWDG (not needed if done in option bytes)
  IWDG->KR  = IWDG_START;
  // Enable write access to IWDG registers
  IWDG->KR  = IWDG_WRITE_ACCESS;
  // Set prescaler by 256
  IWDG->PR  = 6;
  // Set reload value to have a rollover each 5s (37kHz / 256 * 5s)
  IWDG->RLR = 723;
  // Check if flags are reset
  while(IWDG->SR);
  // Refresh counter
  System_WatchdogRefresh();
}


//------------------------------------------------------------------------------
void System_SaweBeforeReset(void)
{
  uint8_t cell_volume, cell_volume_flow;
  if(DayArcRecNo < 15330)
  {
    cell_volume = 6;
    cell_volume_flow = 12;
  }
  else
  {
    cell_volume = 7;
    cell_volume_flow = 13;
  }
  if(DayArcRecNo < 13140)
  {
    cell_volume = 5;
    cell_volume_flow = 11;
  }
  if(DayArcRecNo < 10950)
  {
    cell_volume = 4;
    cell_volume_flow = 10;
  }
  if(DayArcRecNo < 8760)
  {
    cell_volume = 3;
    cell_volume_flow = 3;
  }
  if(DayArcRecNo < 6570)
  {
    cell_volume = 2;
    cell_volume_flow = 2;
  }
  if(DayArcRecNo < 4380)
  {
    cell_volume = 1;
    cell_volume_flow = 1;
  }
  if(DayArcRecNo < 2190)
  {
    cell_volume = 0;
    cell_volume_flow = 0;
  }
  
  if(cell_volume_flow > 4)
  {
    cell_volume_flow -= 10;
    Eeprom_WriteDouble((uint32_t)&VolumeBeforeReset2[cell_volume_flow], GasMeter.VE_Lm3);
  }
  else
     Eeprom_WriteDouble((uint32_t)&VolumeBeforeReset[cell_volume_flow], GasMeter.VE_Lm3);
  
  Eeprom_WriteQword((uint32_t)&datetime_eeprom[cell_volume], timeSystemUnix) ;
}

