#include "GlobalHandler.h"
// EDIT_VOLUME_AND_ARH

const char* reply_command[] = { "OK",               //0
                                "ERROR",            //1 
                                "INCORRECT_VALUE",  //2
                                "ACCESS_DENIED",    //3 
                                "READ_ONLY",         //4 
                                "ERROR_CRC",         //5
                                "ERROR_COMMAND",
                                "WRITE_ONLY",
                                "INCORRECT_PASSWORD"                           
                              }; 

const char* reply_OK = "\r\nOK";

const TParser PFuncCommand[] = {
                       {(TParsFunc)PF_Server_MT,           "Server_MT"},        // 
                       {(TParsFunc)PF_EXIT,                "EXIT"},            //
                       {(TParsFunc)PF_CLOSED,              "CLOSED"},            //
                       
                       {(TParsFunc)PF_DateTime,            "DATETIME"},      
                       {(TParsFunc)PF_GroupInfo,           "GET_GROUP_INFO"}, 
                       {(TParsFunc)PF_GET_GSM_INFO,        "GET_GSM_INFO"}, 
                       {(TParsFunc)PF_ReadArc,             "ARCHIVE"},           //6 !!! SHOULD NOT MOVE WITH THIS POSITION                  
                       {(TParsFunc)PF_PASWORD_FABRIC,      "PASSWORD_FABRIC"},   //7 !!! SHOULD NOT MOVE WITH THIS POSITION     
                       {(TParsFunc)PF_PASWORD_PROVIDER,    "PASSWORD_PROVIDER"}, //8 !!! SHOULD NOT MOVE WITH THIS POSITION            
                       {(TParsFunc)PF_READY_TO_DIALOG,     "READY_TO_DIALOG"},   //9 !!! SHOULD NOT MOVE WITH THIS POSITION                         
                       {(TParsFunc)PF_PASWORD_PROVID_VALUE,"PASWORD_PROVID_VALUE"},
                       {(TParsFunc)PF_Qdf,                 "QdF"},               //11 !!! SHOULD NOT MOVE WITH THIS POSITION   
                       {(TParsFunc)PF_READYTD,                 "READYTD"},               //### 12 !!! SHOULD NOT MOVE WITH THIS POSITION                      
                       
                       // GENERAL INFORMATION
                       {(TParsFunc)PF_TestMode,            "TestMode"},     
                       {(TParsFunc)PF_DevInfo,             "DevInfo"},      
                       {(TParsFunc)PF_SerialNum,           "DEVICE_SN"},       
                       {(TParsFunc)PF_GetLockState,        "LOCK_STATE"},                    
                       {(TParsFunc)PF_hw_version,          "HW_VERSION"},
                       {(TParsFunc)PF_LCD_MENU,            "LCD_MENU"},
                       {(TParsFunc)PF_SN_SGM_E,            "SN_SGM_E"},
                       {(TParsFunc)PF_MOTO_INFO,           "MOTO_INFO"},
                       {(TParsFunc)PF_DATE_VERIFIC_NEXT,   "DATE_VERIFIC_NEXT"},
                       {(TParsFunc)PF_DATE_VERIFIC,        "DATE_VERIFIC"},
                       {(TParsFunc)PF_serial_number_board, "NUMBER_BOARD"},
                       {(TParsFunc)PF_status_system,       "STATUS_SYSTEM"},
                       {(TParsFunc)PF_status_warning,      "STATUS_WARNING"},
                       {(TParsFunc)PF_status_alarm,        "STATUS_ALARM"},
                       {(TParsFunc)PF_status_crash,        "STATUS_CRASH"},
                       {(TParsFunc)PF_ENABLE_SWITCH_MENU,  "ENABLE_SWITCH_MENU"},
                       {(TParsFunc)PF_COMMISSIONING,       "COMMISSIONING"},   
                       {(TParsFunc)PF_OPTIC_TIME,          "OPTIC_TIME"},
                       {(TParsFunc)PF_EVENT_WARNING_EN,    "EVENT_WARNING_EN"},
                       {(TParsFunc)PF_EVENT_ALARM_EN,      "EVENT_ALARM_EN"},
                       {(TParsFunc)PF_EVENT_CRASH_EN,      "EVENT_CRASH_EN"},
                       {(TParsFunc)PF_TEL_BAT_REPLACE,     "TEL_BAT_REPLACE"},
                       {(TParsFunc)PF_COUNT_BAT_REPLACE,   "COUNT_BAT_REPLACE"},
                       {(TParsFunc)PF_TIME_BAT_REPLACE,    "TIME_BAT_REPLACE"},
                       {(TParsFunc)PF_WARNING_CLEAR,       "WARNING_CLEAR"},
                       {(TParsFunc)PF_ALARM_CLEAR,         "ALARM_CLEAR"},
                       {(TParsFunc)PF_CRASH_CLEAR,         "CRASH_CLEAR"},
                       {(TParsFunc)PF_VER_PO,              "VER_PO"},
                       {(TParsFunc)PF_RSTSMT,              "RSTSMT"},
                       {(TParsFunc)PF_MdmToOpto,           "Modem_LOG"},
                       {(TParsFunc)PF_CLEAR_SN_SGM,        "CLEAR_SN_SGM"},
                       {(TParsFunc)PF_MAGIC,               "MAGIC"},
                       {(TParsFunc)PF_MODE_CONSERVATION,   "MODE_CONSERVATION"},


                       //TELEMETRY
                       {(TParsFunc)PF_ServerURL1,          "SERVER_URL"},
                   //    {(TParsFunc)PF_ServerURL2,          "SERVER_URL2"},
                   //    {(TParsFunc)PF_ServerURL3,          "SERVER_URL3"},
                       {(TParsFunc)PF_APNAdress,           "APN_ADDRESS"},
                       {(TParsFunc)PF_APNLogin,            "APN_LOGIN"},
                       {(TParsFunc)PF_APNPassword,         "APN_PASSWORD"},
                       {(TParsFunc)PF_MdmStart,            "Modem_START"},
                       {(TParsFunc)PF_GasDayBorter,        "GAS_DAY"},
                       {(TParsFunc)PF_ModemModeTransfer,   "MODE_TRANSFER"},
                       {(TParsFunc)PF_ModemBalansePhohe,   "BALANCE_PHONE"},
                       {(TParsFunc)PF_ArcNumRecords,       "ArcNumRecords"},
                       {(TParsFunc)PF_ModemReservedInterval,"RESERVED_INT"},
                       {(TParsFunc)PF_ClearArc,            "CLEAR_ARHIVE"},
                       {(TParsFunc)PF_ClearCount,          "CLEAR_COUNT"},
                       {(TParsFunc)PF_ClearMdmCnt,         "CLEAR_MDM_CNT"},
                       {(TParsFunc)PF_COUNT_SESSION,       "COUNT_SESSION"},
                       {(TParsFunc)PF_ERROR_SESSION,       "ERROR_SESSION"},
                       {(TParsFunc)PF_ERROR_2_SESSION,     "ERROR_2_SESSION"},
                       {(TParsFunc)PF_ERROR_3_SESSION,     "ERROR_3_SESSION"},
                       {(TParsFunc)PF_COUNT_FAIL_SIM,      "COUNT_FAIL_SIM"},
                       {(TParsFunc)PF_COUNT_FAIL_SPEED,    "COUNT_FAIL_SPEED"},
                       {(TParsFunc)PF_MAX_SESSION,         "MAX_SESSION"},
                       {(TParsFunc)PF_VER_PROTOCOL,        "VER_PROTOCOL"},
                       {(TParsFunc)PF_KONTRAST,            "KONTRAST"},
                       {(TParsFunc)PF_LCD_PARAM,           "LCD_PARAM"},                
                       
                       //METROLOGY
                       {(TParsFunc)PF_MeasTest,            "START"},
                       {(TParsFunc)PF_CurVol,              "Volume"},
                       {(TParsFunc)PF_GasLTime,            "N_GasLMesTime"},
                       {(TParsFunc)PF_TestLTime,           "Test_GasLMesTime"},
                       {(TParsFunc)PF_type_device,         "TYPE_DEVICE"},
                       {(TParsFunc)PF_QposLimit,           "Q_PosLimit"},
                       {(TParsFunc)PF_pressure_abs,        "PABS"},
                       {(TParsFunc)PF_QGL_MIN,             "QGL_MIN"},
                       {(TParsFunc)PF_QGL_MAX,             "QGL_MAX"},
                       {(TParsFunc)PF_QFL_LIMIT,           "QFL_LIMIT"},
                       {(TParsFunc)PF_VOLUME_INST,         "VOLUME_INST"},
                       {(TParsFunc)PF_DQF_END,             "DQF_CRC"},
                       {(TParsFunc)PF_Self_GasBorder,      "SELF_BORDER"},
                       {(TParsFunc)PF_Set_KalmanFilter,    "EN_KALMAN_FILTER"},
                       {(TParsFunc)PF_KalmanK,             "KALMAN_K"},
                       {(TParsFunc)PF_KalmanBorderH,       "KALMAN_BORDER_H"},
                       {(TParsFunc)PF_KalmanBorderL,       "KALMAN_BORDER_L"},
                       {(TParsFunc)PF_KalmanCounter,       "KALMAN_COUNTER"},
                       {(TParsFunc)PF_MANUAL_LUT,          "MANUAL_LUT"},
                       {(TParsFunc)PF_ERR_SGM_PER_LONG,    "ERR_SGM_PER_LONG"}, 
                       {(TParsFunc)PF_ERR_SGM_PER_SHORT,   "ERR_SGM_PER_SHORT"},
                       {(TParsFunc)PF_ERR_SGM_PER_FAST,    "ERR_SGM_PER_FAST"},
                       {(TParsFunc)PF_ERR_SGM_CNT_LONG,    "ERR_SGM_CNT_LONG"},
                       
                       // K FACTOR
                       {(TParsFunc)PF_GasRTime,            "GAS_REC_TIME"},
                       {(TParsFunc)PF_GasRange,            "GAS_RANGE"},
                       {(TParsFunc)PF_GAS_RANGE_UI,        "GAS_RANGE_UI"},
                       {(TParsFunc)PF_N_GasRTime,          "N_GAS_REC"},
                       {(TParsFunc)PF_N_GAS_REC_UI,        "N_GAS_REC_UI"},
                       {(TParsFunc)PF_TestRTime,           "Test_GasRecTime"},
                       {(TParsFunc)PF_KFACTOR,             "KFACTOR"},

                       // QMAX
                       {(TParsFunc)PF_MaxFlowRate,         "Max_FlowRate"},
                       {(TParsFunc)PF_Q_max_multiplier_up, "QMAX_MULTIPLIER_UP"},
                       {(TParsFunc)PF_QMAX_ON_PER_S,       "QMAX_ON_PER_S"},
                       {(TParsFunc)PF_QMAX_OFF_PER_S,      "QMAX_OFF_PER_S"},

                       // TEMPERATURE
                       {(TParsFunc)PF_MaxTemp,             "Max_Temp"},
                       {(TParsFunc)PF_MinTemp,             "Min_Temp"},
                       {(TParsFunc)PF_EXT_MAX_TEMP,        "EXT_MAX_TEMP"},
                       {(TParsFunc)PF_EXT_MIN_TEMP,        "EXT_MIN_TEMP"},
                       {(TParsFunc)PF_EXT_TEMP_CNT,        "EXT_TEMP_CNT"},
                       {(TParsFunc)PF_cnt_measurement_temper_eeprom, "CNT_MEAS_TEMP"},
                    //   {(TParsFunc)PF_TIME_MEAS_EX_TEMP,   "TIME_MEAS_EX_TEMP"},
                       {(TParsFunc)PF_TEMP_GAS_MIN_CRASH,  "TEMP_GAS_MIN_CRASH"},
                       {(TParsFunc)PF_TEMP_GAS_MAX_CRASH,  "TEMP_GAS_MAX_CRASH"},
                       {(TParsFunc)PF_TEMP_GAS_PER_CRASH,  "TEMP_GAS_PER_CRASH"},
                       {(TParsFunc)PF_TEMP_ENV_MIN_CRASH,  "TEMP_ENV_MIN_CRASH"},
                       {(TParsFunc)PF_TEMP_ENV_MAX_CRASH,  "TEMP_ENV_MAX_CRASH"},
                       {(TParsFunc)PF_TEMP_ENV_PER_CRASH,  "TEMP_ENV_PER_CRASH"},

                       // REVERS FLOW
                       {(TParsFunc)PF_REV_FL_ON_PER_S,     "REV_FL_ON_PER_S"},
                       {(TParsFunc)PF_REV_FL_OFF_PER_S,    "REV_FL_OFF_PER_S"},
                       {(TParsFunc)PF_QnegLimit,           "Q_NegLimit"},

                       // VALVE
                       {(TParsFunc)PF_VALVE_SET,           "VALVE_SET"},  
                       {(TParsFunc)PF_VALVE_TIME,          "VALVE_TIME"}, 
                       {(TParsFunc)PF_VALVE_DELAY_BEFORE_OPEN,          "VALVE_DELAY_BEFORE_OPEN"},  
                     //  {(TParsFunc)PF_CHECK_OPEN_TIME,     "CHECK_OPEN_TIME"},   
                     //  {(TParsFunc)PF_VALVE_QMIN,          "VALVE_QMIN"},  
                       {(TParsFunc)PF_VALVE_MIN,           "VALVE_MIN"}, 
                       {(TParsFunc)PF_CNT_VALVE_MIN,       "CNT_VALVE_MIN"},
                       {(TParsFunc)PF_VALVE_AUTO_CONTROL,  "VALVE_AUTO_CONTROL"},
                       {(TParsFunc)PF_VALVE_CLOSE_REV_P,   "VALVE_CLOSE_REV_P"},  
                       {(TParsFunc)PF_VALVE_CLOSE_Q_PH,    "VALVE_CLOSE_Q_PH"},
                   //    {(TParsFunc)PF_FORCE_VALVE,         "FORCE_VALVE"},                 
                       {(TParsFunc)PF_VALVE,               "VALVE"},  
                       {(TParsFunc)PF_EN_MDM_CHNG_VLV,     "EN_MDM_CHNG_VLV"},    
                       {(TParsFunc)PF_VALVE_DELAY_TRY_OPEN,             "VALVE_DELAY_TRY_OPEN"},




    //                   {(TParsFunc)PFW_GasSTime,          "N_GasSMesTime"}, 
  //                   {(TParsFunc)PFW_ShortMeassure,      "N_ShortMeasUsed"}, 
  //                   {(TParsFunc)PFW_ManualK,            "MANUAL_K"},      
  //                     {(TParsFunc)PF_ModemAutoSwitchMode, "AUTO_SWITCH_MODE"}, //
                 //      {(TParsFunc)PF_sms_phone2,          "SMS_PHONE2"},  //
                 //      {(TParsFunc)PF_sms_phone1,          "SMS_PHONE"},//
//                       {(TParsFunc)PFW_Gl_Error,            "GL_ERROR"},
    //                   {(TParsFunc)PFW_All_Mod_ErrorCnt,   "AllModuleErrCount"},
                  //     {(TParsFunc)PF_WaitingTimeLCD,      "WaitingTimeLCD"},

};

const uint16_t MAX_FUNC_COMMAND = sizeof(PFuncCommand)/sizeof(PFuncCommand[0]);
uint8_t check_crc32;
uint32_t index_command = 0;

uint32_t crypto_open_key[5] = {0,0,0,0,0}; // !!! depends on TypeSourceCommands
uint8_t AES128_Key[5][16]; // !!! depends on TypeSourceCommands
uint8_t flag_crypt_message[5] = {_FALSE, _FALSE, _FALSE, _FALSE, _FALSE};
uint8_t decrypt_message[400];

extern osMessageQId QueueParsingCommandHandle;
extern SemaphoreHandle_t xCalculateCRC32Semaphore;

void ParsingCommands()
{
   TypeParsingCommand elementParsingCommand;   
   uint8_t i=0, len_str = 0;
   uint8_t flag_start_crc32;
   uint8_t chr_TAB_strlen=0;
   char *end_ptr, *begin_ptr, *chr_TAB;
   
   portBASE_TYPE xStatus;
   if(xQueueReceive(QueueParsingCommandHandle, ( void * ) &elementParsingCommand, 0 ) == pdPASS )
   {
      uint8_t sLen;
      uint8_t rez = _FALSE;
      char *command_for_write;
      if(elementParsingCommand.ptr_temp_data->index_commands > -1)
        i = elementParsingCommand.ptr_temp_data->index_commands;
      if(strlen((char*)elementParsingCommand.ptr_commands) > 2 || elementParsingCommand.ptr_temp_data->index_commands != -1)
      {
         if(elementParsingCommand.ptr_temp_data->index_commands == -1)
         {
            check_crc32 = check_crc32_str(&elementParsingCommand.ptr_commands[0], _OK);
         //TransmitDebugMessageOptic("Strl=1\r\n", 0);

            chr_TAB = strchr((char*)elementParsingCommand.ptr_commands, 0x09);  // '/t'
            if(chr_TAB)
            {
              // *chr_TAB = 0;
               *elementParsingCommand.ptr_commands = 0; // delete symbol 'i'
               ++elementParsingCommand.ptr_commands;
               index_command = atoi((char*)elementParsingCommand.ptr_commands);   
               if(index_command == 10)
                 __NOP();
               ++chr_TAB;
               if(index_command)
               {
                  chr_TAB_strlen = (uint32_t)chr_TAB - (uint32_t)elementParsingCommand.ptr_commands;
                  strncpy((char*)elementParsingCommand.ptr_out, (char*)elementParsingCommand.ptr_commands, chr_TAB_strlen);
                  elementParsingCommand.ptr_commands = (uint8_t*)chr_TAB;
                  //-------------------
                  // crypt
                  // non crypt "READY_TO_DIALOG" and "READYTD"
                  sLen = strlen((char*)PFuncCommand[9].commandStr);
                  if(strncmp(PFuncCommand[9].commandStr, (char*)elementParsingCommand.ptr_commands, sLen) == 0)
                      flag_crypt_message[elementParsingCommand.source] = _FALSE;
                  //###
                  sLen = strlen((char*)PFuncCommand[12].commandStr);
                  if(strncmp(PFuncCommand[12].commandStr, (char*)elementParsingCommand.ptr_commands, sLen) == 0)
                      flag_crypt_message[elementParsingCommand.source] = _FALSE; 
                  
                  if(flag_crypt_message[elementParsingCommand.source] == _OK)
                  {
                     uint16_t len;
                     memset(decrypt_message, 0, sizeof(decrypt_message));
                     len = StrToHex(elementParsingCommand.ptr_commands, elementParsingCommand.ptr_commands, 2);
                     elementParsingCommand.ptr_commands[len] = 0;
                     AES128_CFB_Decrypt(elementParsingCommand.ptr_commands, 
                                        strlen((char*)elementParsingCommand.ptr_commands), 
                                        &AES128_Key[elementParsingCommand.source][0],
                                        decrypt_message);
                     elementParsingCommand.ptr_commands = decrypt_message;
                  }
               }
            //   elementParsingCommand.ptr_commands = (uint8_t*)chr_TAB;
            }
         }
         //--------------------------------------------------
         for(; i < MAX_FUNC_COMMAND; i++)
         {
           
         if(elementParsingCommand.ptr_temp_data->index_commands == -1)
         {
            if(check_crc32 == _FALSE && i > 2)  // past commands 0-2
              break;

            if(PFuncCommand[i].commandStr[0] == elementParsingCommand.ptr_commands[0]) 
            {
               end_ptr = strchr((char*)elementParsingCommand.ptr_commands, '=');
               len_str = strlen((char*)elementParsingCommand.ptr_commands);
               if(end_ptr)
               {
                  begin_ptr = (char*)elementParsingCommand.ptr_commands;
                  len_str = end_ptr - begin_ptr;
               }
               else
               {
                 end_ptr = strchr((char*)elementParsingCommand.ptr_commands, 0x06);
                 if(end_ptr)
                 {
                   *end_ptr = 0;
                   len_str = strlen((char*)elementParsingCommand.ptr_commands);
                 }
               }
               sLen = strlen((char*)PFuncCommand[i].commandStr);
               if(strncmp(PFuncCommand[i].commandStr, (char*)elementParsingCommand.ptr_commands, sLen) != 0)
               {
                  if( i == MAX_FUNC_COMMAND - 1)
                     rez = _FALSE;
                  continue;  
               }
               if(sLen != len_str && i != 11 && i != 0 && i != 6) continue;  // ��������� ���� ��������, non check ServerMT, QdF, ARCHIVE
               elementParsingCommand.ptr_temp_data->index_commands = i;
               command_for_write = strchr((char*)elementParsingCommand.ptr_commands, '=');
               if(command_for_write == NULL)
               {// read
                  command_for_write = strchr((char*)elementParsingCommand.ptr_commands, '(');
                  if(command_for_write == NULL)
                  {
                     command_for_write = strchr((char*)elementParsingCommand.ptr_commands, '"');
                     if(command_for_write == NULL)
                        PFuncCommand[i].ParsFunc(0, elementParsingCommand.ptr_out + chr_TAB_strlen, (uint8_t*)PFuncCommand[i].commandStr, elementParsingCommand.source, elementParsingCommand.ptr_temp_data);
                     else
                        // '"'
                        PFuncCommand[i].ParsFunc((char*)elementParsingCommand.ptr_commands, elementParsingCommand.ptr_out + chr_TAB_strlen, (uint8_t*)PFuncCommand[i].commandStr, elementParsingCommand.source, elementParsingCommand.ptr_temp_data);
                  }   
                  else
                     // '('
                     PFuncCommand[i].ParsFunc(command_for_write, elementParsingCommand.ptr_out + chr_TAB_strlen, 0, elementParsingCommand.source, elementParsingCommand.ptr_temp_data);
               }
               else
               {// write '='
                  char *ptr_license_key;
                  uint8_t size_license_key;
                  ptr_license_key = strchr((char*)elementParsingCommand.ptr_commands, '\x06');
                  if(ptr_license_key)
                  {
                     ++ptr_license_key;
                     size_license_key = strlen(ptr_license_key);
                     if(size_license_key > 9 && size_license_key < 30 
                        && (CLB_lock == 0x55 || CLB_lock == 0x66 || StatusDevice.password_manufacture_enter == 1 || StatusDevice.password_provider_enter == 1
                            || i == 7 || i == 8))
                     {
                        strcpy((char*)srt_license_key, ptr_license_key);
                        --ptr_license_key;
                        *ptr_license_key = 0;
                        PFuncCommand[i].ParsFunc(command_for_write + 1, elementParsingCommand.ptr_out + chr_TAB_strlen, 0, elementParsingCommand.source, elementParsingCommand.ptr_temp_data);  //TODO: ������� ������
                     }
                     else
                     {
                        memset((char*)srt_license_key, 0, sizeof(srt_license_key));
                        strcpy((char*)elementParsingCommand.ptr_out + chr_TAB_strlen, reply_command[COMMAND_ACCESS_DENIED]);
                        elementParsingCommand.ptr_temp_data->flag_end = _OK;
                        rez = _OK;
                        break;
                     }
                  }
               }
               rez = _OK;
               break;
            }
            
         }
         else
         {
           PFuncCommand[i].ParsFunc(0, elementParsingCommand.ptr_out + chr_TAB_strlen, 0, elementParsingCommand.source, elementParsingCommand.ptr_temp_data);
           rez = _OK;
           break;
         }
      }// for
      if(rez == _FALSE && i != 0)
      {
         strcpy((char*)elementParsingCommand.ptr_out + chr_TAB_strlen, reply_command[COMMAND_ERROR_COMMAND]);
         elementParsingCommand.ptr_temp_data->flag_end = _OK;
        // TransmitDebugMessageOptic("ComEr\r\n");
      }
      if(check_crc32 == _FALSE && i > 2)
      {
         strcpy((char*)elementParsingCommand.ptr_out + chr_TAB_strlen, reply_command[COMMAND_ERROR_CRC]);
         elementParsingCommand.ptr_temp_data->flag_end = _OK;
      }
      //------------------------------------------------------------------------------      
      if(flag_crypt_message[elementParsingCommand.source] == _OK && elementParsingCommand.ptr_temp_data->index_commands != 9 
         && elementParsingCommand.ptr_temp_data->index_commands != 12) //###
      {
         uint16_t len;
         char *ptr_tab;
         ptr_tab = strchr((char*)elementParsingCommand.ptr_out, '\t');
         memset(decrypt_message, 0, sizeof(decrypt_message));
         if(ptr_tab)
           //elementParsingCommand.ptr_out = (uint8_t*)(ptr_tab + 1);
           ++ptr_tab;
         else
           ptr_tab = (char*)elementParsingCommand.ptr_out;
         len = AES128_CFB_Encrypt((uint8_t*)ptr_tab, strlen(ptr_tab), AES128_Key[elementParsingCommand.source], decrypt_message);
//         AES128_CFB_Decrypt(decrypt_message, len, AES128_Key[elementParsingCommand.source], elementParsingCommand.ptr_out);
         BinToHexStr((uint8_t*)ptr_tab, decrypt_message, len);
         strcat(ptr_tab, "\x06");

      }
      //------------------------------------------------------------------------------
      // CRC32
      if(i)
      { 
         if(elementParsingCommand.ptr_temp_data->flag_end == _FALSE)
         {
            if(elementParsingCommand.ptr_temp_data->number_frame == 1)
               flag_start_crc32 = CRC_START;
            else
               flag_start_crc32 = CRC_IN_PROCESSING;
         
            xStatus = xSemaphoreTake(xCalculateCRC32Semaphore, 5000);
            if( xStatus == pdPASS )
               elementParsingCommand.ptr_temp_data->crc32_old = CalculateCRC32(elementParsingCommand.ptr_out, strlen((char*)elementParsingCommand.ptr_out), flag_start_crc32, elementParsingCommand.ptr_temp_data->crc32_old);
            xSemaphoreGive(xCalculateCRC32Semaphore);
         }
         else
         {
            if(elementParsingCommand.ptr_temp_data->number_frame == 0)
               flag_start_crc32 = CRC_START_STOP;
            else
               flag_start_crc32 = CRC_STOP;
            xStatus = xSemaphoreTake(xCalculateCRC32Semaphore, 5000);
            if( xStatus == pdPASS )
               elementParsingCommand.ptr_temp_data->crc32_old = CalculateCRC32(elementParsingCommand.ptr_out, strlen((char*)elementParsingCommand.ptr_out), flag_start_crc32, elementParsingCommand.ptr_temp_data->crc32_old);
            xSemaphoreGive(xCalculateCRC32Semaphore);
            sprintf((char*)elementParsingCommand.ptr_out + strlen((char*)elementParsingCommand.ptr_out), "\t%08X\r\n", elementParsingCommand.ptr_temp_data->crc32_old);
         }
      }
      //------------------------------------------------------------------------------
      xSemaphoreGive(*(elementParsingCommand.sem_source));
      //------------------------------------------------------------------------------
      }
      else
        TransmitDebugMessageOptic("Strl=0\r\n", 0);
   }
}

//=============================================================================
// 
//=============================================================================
//------------------------------------------------------------------------------
uint8_t Str2DataTime(LL_RTC_DateTypeDef* p_Date,LL_RTC_TimeTypeDef* p_Time, char*parString, uint8_t check_time)
{
  char* index;
  char delimiter = '.';
  uint32_t temp_value;
  uint8_t ret=0;
   
    // ������ ���
    index = parString;
    uint8_t i=0;
    while((ret==0) && (i<6))
    {
       switch (i)
       {
        case 0:
           temp_value = atoi(index);
           if ((temp_value)> 0 && (temp_value)< 32)
             p_Date->Day = temp_value;
           else 
             ret = 2;
           break;
        case 1:
           temp_value = atoi(index);
           if ((temp_value)> 0 && (temp_value)< 13)
             p_Date->Month = temp_value;
           else 
             ret = 2;
           break;
        case 2:
           temp_value = atoi(index);
           if ((temp_value)> 0 && (temp_value)< 99)
             p_Date->Year = temp_value;
           else 
             ret = 2;
           delimiter = ',';
           break;
        case 3:
           temp_value = atoi(index);
           if (temp_value < 25 && is_digit_cnt(index, 2) == 2)
           {
             p_Time->Hours = temp_value;
              delimiter = ':';
           }  
           else 
             if(check_time == _OK)
                ret = 0x2;
           break;
        case 4:
           temp_value = atoi(index);
           if (temp_value < 60 && is_digit_cnt(index, 2) == 2)
             p_Time->Minutes = temp_value;
           else 
             if(check_time == _OK)
                ret = 0x2;
           break;
        case 5:
           temp_value = atoi(index);
           if (temp_value < 60 && is_digit_cnt(index, 2) == 2)
             p_Time->Seconds = temp_value;
           else 
             if(check_time == _OK)
                ret = 0x2;
           break;
       }           
     index = strchr(index, delimiter);
     if(index)
        index++;
     i++;
    }
    if(!ret)
    {
  //      return 0x2;
    // check correctness
      if(check_time == _FALSE)
      {
        p_Time->Hours = 0;
        p_Time->Minutes = 0;
        p_Time->Seconds = 0;
      }
    struct tm lo_tm;
    time_t temp_unix;
    lo_tm.tm_mday = p_Date->Day;
    lo_tm.tm_mon = p_Date->Month-1;
    lo_tm.tm_year = p_Date->Year + 100;
    lo_tm.tm_hour = p_Time->Hours;
    lo_tm.tm_min = p_Time->Minutes;
    lo_tm.tm_sec = p_Time->Seconds;
    lo_tm.tm_isdst = 0;
    temp_unix = mktime(&lo_tm);
    memcpy(&lo_tm, gmtime(&temp_unix), sizeof(lo_tm));
    if( lo_tm.tm_mday == p_Date->Day &&
        lo_tm.tm_mon  == p_Date->Month-1 &&
        lo_tm.tm_year == p_Date->Year + 100 &&
        lo_tm.tm_hour == p_Time->Hours &&
        lo_tm.tm_min  == p_Time->Minutes &&
        lo_tm.tm_sec  == p_Time->Seconds)
      ret = 0;
    else
      ret = 2;
    }
  return ret;
}
/**
* @brief Setting date and time 
* @param parString: pointer for string after char '='
* @param ptr_out_buf: pointer for output bufer
* @param ptr_str_reply: pointer for reply to char '='
* @param source: what communication channel does the team go through
* @param ptr_TempDataParsingCommand: temporary data for serial output
* @retval None
*/
void PF_DateTime(char* parString, uint8_t *ptr_out_buf, uint8_t *ptr_str_reply, TypeSourceCommands source, TTempDataParsingCommand *ptr_TempDataParsingCommand)
{
  LL_RTC_DateTypeDef loDate, lo_ArcDate;
  LL_RTC_TimeTypeDef loTime, lo_ArcTime;
  uint8_t ret =0;
  
  if(parString ==0)
  {
     GetDate_Time(&loDate,&loTime);
     strcpy((char*)ptr_out_buf, (const char*)ptr_str_reply);
     sprintf((char*)(ptr_out_buf + strlen((char*)ptr_out_buf)),"=%0.2u.%0.2u.%0.2u,%0.2u:%0.2u:%0.2u%s",
             loDate.Day, loDate.Month, loDate.Year,
             loTime.Hours, loTime.Minutes, loTime.Seconds, reply_OK);
  }
 else
 {
    if(CheckChrString((uint8_t*)parString, "%d.,:", strlen(parString)) == _FALSE) {strcpy((char*)ptr_out_buf, reply_command[COMMAND_INCORRECT_VALUE]); ptr_TempDataParsingCommand->flag_end = _OK; return;  }
    ret = Str2DataTime(&loDate, &loTime, parString, _OK);
    if(ret == 0)
    {
       GetDate_Time(&lo_ArcDate,&lo_ArcTime);
       sprintf((char*)OldValue,"%02d.%02d.%02d,%02d:%02d:%02d",
                                      lo_ArcDate.Day, lo_ArcDate.Month, lo_ArcDate.Year, 
                                      lo_ArcTime.Hours, lo_ArcTime.Minutes, lo_ArcTime.Seconds);  
       SetDate_Time(&loDate, &loTime);
       timeSystemUnix = Get_IndexTime(loDate,loTime);       
       sprintf((char*)NewValue,"%02d.%02d.%02d,%02d:%02d:%02d",
                                      loDate.Day, loDate.Month, loDate.Year, 
                                      loTime.Hours, loTime.Minutes, loTime.Seconds);  
       
       WriteChangeArcRecord(CHANGE_DATE_TIME, source);


       timeGSMUnixStart = timeSystemUnix;
       timeUnixGSMNextConnect = timeSystemUnix + 3600; // do not interfere with shutdown
       valve_going_beyond_Q_volue = sensorTypeParameters[_SgmType].Q_max * ((float)valve_going_beyond_Q_period_sec / 3600.0) * Q_max_multiplier_up + GasMeter.VE_Lm3;
       valve_going_beyond_Q_nexTime = timeSystemUnix + valve_going_beyond_Q_period_sec + KalmanCnt * 2;
       if(source != SOURCE_TCP)
       {
          ParamServer.success_session[0] = _OK;
          ParamServer.success_session[1] = _OK;
          ParamServer.success_session[2] = _OK;         
      //    ParamServer.success_session_old[0] = ParamServer.success_session[0];
      //    ParamServer.success_session_old[1] = ParamServer.success_session[1];
      ///    ParamServer.success_session_old[2] = ParamServer.success_session[2];
       }
       strcpy((char*)ptr_out_buf, reply_command[COMMAND_OK]);
    }  
   else
     strcpy((char*)ptr_out_buf, reply_command[COMMAND_INCORRECT_VALUE]);

 } 
 ptr_TempDataParsingCommand->flag_end = _OK; 
}
//QT style, auto-documenting with Doxygen, https://habr.com/ru/post/252101/ 
//QT style, auto-documenting with Doxygen, https://habr.com/ru/post/252101/ 
/*!
read brief information
\param[in] pointer string
\param[in] pointer output bufer
\param[in] pointer request
\param[in] source command
\param[in] temp data source command
\return void
*/
void PF_GroupInfo(char* parString, uint8_t *ptr_out_buf, uint8_t *ptr_str_reply, TypeSourceCommands source, TTempDataParsingCommand *ptr_TempDataParsingCommand)
{
   if(parString == 0)
   {
      ++ptr_TempDataParsingCommand->number_frame;
      ptr_TempDataParsingCommand->flag_message_of_patrs = _OK;
      if(ptr_TempDataParsingCommand->number_frame < 10)
      {
         FormationGROUP_INFO((uint8_t*)ptr_out_buf, ptr_TempDataParsingCommand);
         ptr_TempDataParsingCommand->flag_end = _FALSE;
         ptr_TempDataParsingCommand->flag_message_of_patrs_end = _FALSE;
      }
      else
      {
         ptr_TempDataParsingCommand->flag_end = _OK;
         ptr_TempDataParsingCommand->flag_message_of_patrs_end = _OK;
         strcpy((char*)ptr_out_buf + strlen((char*)ptr_out_buf), reply_command[COMMAND_OK]);
      }
   }
   else
   {
     strcpy((char*)ptr_out_buf, reply_command[COMMAND_READ_ONLY]);
     ptr_TempDataParsingCommand->flag_end = _OK; 
   }
}
//QT style, auto-documenting with Doxygen, https://habr.com/ru/post/252101/ 
//QT style, auto-documenting with Doxygen, https://habr.com/ru/post/252101/ 
/*!
read brief information
\param[out] pointer part string
\param[in] cnt part
\return void
*/
void FormationGROUP_INFO(uint8_t *ptr_buf, TTempDataParsingCommand *ptr_TempDataParsingCommand)
{
   switch(ptr_TempDataParsingCommand->number_frame)
   {
   case 1:
        {
           uint64_t cntGas;
           
           cntGas = (uint32_t)(GasMeter.VE_Lm3 * 10000);
           GetDate_Time(&SystDate,&SystTime);
           
           sprintf((char*)ptr_buf,"{%04X;10000;%llu;%02d.%02d.%02d,%02d:%02d:%02d;", 
                                                        StatusSystem.word,
                                                        cntGas,
                                                        SystDate.Day, SystDate.Month, SystDate.Year, 
                                                        SystTime.Hours, SystTime.Minutes, SystTime.Seconds);
        }
        break;
   case 2:
     {
       uint32_t len;
       len = strlen(MdmAPN_Adress) + strlen(MdmAPN_Login) + strlen(MdmAPN_Password) 
             + strlen((char*)MdmURL_Server) + strlen((char*)MdmPort_Server);
       if(len < 120)
        sprintf((char*)ptr_buf,"%s;%s;%s;%s:%s;", (uint8_t*)MdmAPN_Adress, 
                                            (uint8_t*)MdmAPN_Login, 
                                            (uint8_t*)MdmAPN_Password,
                                            (uint8_t*)MdmURL_Server,
                                            (uint8_t*)MdmPort_Server);
     }
        break;
   case 3:
        if(strlen(MdmNumberBalans) < 100)
           sprintf((char*)ptr_buf,"%s;%s;%d,",  telephone_number_SMS1,
                                           (uint8_t*)MdmNumberBalans,
                                           Mode_Transfer.mode);
        break;
   case 4:
        sprintf((char*)ptr_buf,"%02d:%02d,%d;4000;%.0f;%d;%d;%d;%d;", 
                                                            
                                                           Mode_Transfer.hour, Mode_Transfer.minute, Mode_Transfer.day,
                                                           //    (uint16_t)battery_voltage_mv_main,
                                                           //   (uint16_t)battery_voltage_mv_rezerv,
                                                           GasMeter.var_T,
                                                           reserved_int,
                                                           max_cnt_communication_gsm,
                                                         //  GSM_Get_current_communication_gsm_number(),
                                                           current_communication_gsm_number_eeprom,
                                                           number_communication_gsm_fail_eeprom
                                                           );
        break;
   case 5:
        sprintf((char*)ptr_buf,"%d;%d;%d;%d;%d;%d;%d;" , 

                                                                GasDayBorder,
                                                                hw_version,
                                                                auto_switching_mode,
                                                                ChangeArcRecNo - 1,
                                                                SystemArcRecNo - 1,
                                                                IntArcRecNo - 1,
                                                                DayArcRecNo - 1
                                                                );
        break;
   case 6:
     sprintf((char*)ptr_buf,"%f;%d;%d;%d;%d;%d;%1.0f;%d" , 
                                                          pressure_abs,
                                                          valve_resolution_from_TCP_eeprom, 
                                                          (uint8_t)ValveGetStatus(),
                                                          cnt_fail_sim_card_eeprom, 
                                                          cnt_fail_speed_gsm_modem_eeprom,
                                                          valve_enable_auto_control_eeprom,
                                                          GasMeter.mid_GasRecond,
                                                          TelemetryArcRecNo - 1 
                                                                );
        break;        
   case 7:
     sprintf((char*)ptr_buf,";%.2f;%s:%s;%s:%s;%s;" , 
                                                          residual_capacity_battery_modem_percent,
                                                          (uint8_t*)MdmURL_Server[1],
                                                          (uint8_t*)MdmPort_Server[1],
                                                          (uint8_t*)MdmURL_Server[2],
                                                          (uint8_t*)MdmPort_Server[2],
                                                          serial_number_board
                                                                );
        break;            
    case 8:
      { 
           sprintf((char*)ptr_buf,"%s;%04X;%04X;%04X;%016llX;" , 
                                                          telephone_number_SMS2,
                                                          StatusWarning.word & StatusWarningEnable.word,
                                                          StatusAlarm.word & StatusAlarmEnable.word,
                                                          StatusCrash.word & StatusCrashEnable.word,
                                                          event_int_story                                                          
                                                          );
      }
      break;
    case 9:
           sprintf((char*)ptr_buf,"%d;%d;%.2f}\r\n" , 
                                                          number_communication_gsm_fail_2,
                                                          number_communication_gsm_fail_3,
                                                          residual_capacity_battery_main_percent
                                                          );
        break;   
   default:break;
   }
}
//QT style, auto-documenting with Doxygen, https://habr.com/ru/post/252101/ 
//QT style, auto-documenting with Doxygen, https://habr.com/ru/post/252101/ 
/*!
read brief information
\param[in] pointer string
\param[in] pointer output bufer
\param[in] pointer request
\param[in] source command
\param[in] temp data source command
\return void
*/
void PF_GET_GSM_INFO(char* parString, uint8_t *ptr_out_buf, uint8_t *ptr_str_reply, TypeSourceCommands source, TTempDataParsingCommand *ptr_TempDataParsingCommand)
{
   if(parString == 0)
   {
      ++ptr_TempDataParsingCommand->number_frame;
      ptr_TempDataParsingCommand->flag_message_of_patrs = _OK;
      if(ptr_TempDataParsingCommand->number_frame < 3)
      {
         FormationGSM_INFO((uint8_t*)ptr_out_buf, ptr_TempDataParsingCommand);
         ptr_TempDataParsingCommand->flag_end = _FALSE;
         ptr_TempDataParsingCommand->flag_message_of_patrs_end = _FALSE;
      }
      else
      {
         ptr_TempDataParsingCommand->flag_end = _OK;
         ptr_TempDataParsingCommand->flag_message_of_patrs_end = _OK;
         strcpy((char*)ptr_out_buf + strlen((char*)ptr_out_buf), reply_command[COMMAND_OK]);
      }
   }
   else
   {
     strcpy((char*)ptr_out_buf, reply_command[COMMAND_READ_ONLY]);
     ptr_TempDataParsingCommand->flag_end = _OK; 
   }
}

void  FormationGSM_INFO(uint8_t *ptr_buf, TTempDataParsingCommand *ptr_TempDataParsingCommand)
{
   switch(ptr_TempDataParsingCommand->number_frame)
   {
   case 1:
     {
        uint32_t len[2];
        len[0] = strlen(GSM_Get_ptr_network_name());
        len[1] = strlen(GSM_Get_ptr_CCID_SIM_card());
        if((len[0] + len[1]) < BUF_MESSAGE_GSM_MODULE_SIZE - 5)
        sprintf((char*)ptr_buf,"{%s;%s;%d;",
                                                    //  GSM_network_name,
                                                    //  CCID_SIM_card,
                                                      
                                                       GSM_Get_ptr_network_name(),
                                                       GSM_Get_ptr_CCID_SIM_card(),
                                                       GSM_Get_level_gsm_signal()
                                                       );
     }
        break;
   case 2:
     {
        uint32_t len[2];
        len[0] = strlen(GSM_Get_ptr_message_SIM_card_balance());
        len[1] = strlen(GSM_Get_ptr_IMEI());
        if((len[0] + len[1]) < BUF_MESSAGE_GSM_MODULE_SIZE - 5)
        sprintf((char*)ptr_buf,"%s;%s}\r\n", GSM_Get_ptr_message_SIM_card_balance(), GSM_Get_ptr_IMEI());
     }
        break;
   default:
        strcat((char*)ptr_buf,"\r\n\x1A");
     break;
   }
}
/**
* @brief Command close for server
* @param parString: pointer for string after char '='
* @param ptr_out_buf: pointer for output bufer
* @param ptr_str_reply: pointer for reply to char '='
* @param source: what communication channel does the team go through
* @retval None
*/
void PF_EXIT(char* parString, uint8_t *ptr_out_buf, uint8_t *ptr_str_reply, TypeSourceCommands source, TTempDataParsingCommand *ptr_TempDataParsingCommand)
{
   strcpy((char*)ptr_out_buf, reply_command[COMMAND_OK]);   
   ptr_TempDataParsingCommand->flag_end = _OK;
   flag_crypt_message[source] = _FALSE;
   if(source == SOURCE_TCP)
   {
      source_start_modem = SOURCE_START_MDM_NON;
      ParamServer.success_session[number_server_transmit] = _OK;      
   }
}
/**
* @brief Setting mode output information debug
* @param parString: pointer for string after char '='
* @param ptr_out_buf: pointer for output bufer
* @param ptr_str_reply: pointer for reply to char '='
* @param source: what communication channel does the team go through
* @param ptr_TempDataParsingCommand: temporary data for serial output
* @retval None
*/
void PF_TestMode(char* parString, uint8_t *ptr_out_buf, uint8_t *ptr_str_reply, TypeSourceCommands source, TTempDataParsingCommand *ptr_TempDataParsingCommand)
{
  if(parString ==0)
  {
     strcpy((char*)ptr_out_buf, (const char*)ptr_str_reply);
     sprintf((char*)(ptr_out_buf + strlen((char*)ptr_out_buf)),"=%d%s", TestModeActive, reply_OK);
  }
  else
  {
    uint32_t temp;
    if(CheckChrString((uint8_t*)parString, "%d", strlen(parString)) == _FALSE) {strcpy((char*)ptr_out_buf, reply_command[COMMAND_INCORRECT_VALUE]); ptr_TempDataParsingCommand->flag_end = _OK; return;  }  
    temp = atoi(parString);    
    if (CLB_lock != 0x55 && CLB_lock != 0x66 && StatusDevice.password_manufacture_enter == 0)
       strcpy((char*)ptr_out_buf, reply_command[COMMAND_ACCESS_DENIED]);   
    else
    {
       if(temp < 6)
       {
          switch(temp)
          {
          case 0: ActivateTestMode(TestModeOFF);
               break;
          case 1: ActivateTestMode(TestMode_1);
               break;
          case 2: ActivateTestMode(TestMode_2);
               break;
//        case 4: ActivateTestMode(TestMode_4);
//             break;
          case 5: ActivateTestMode(TestMode_5);
               break;                    
          default: ActivateTestMode(TestModeOFF);         
               break;         
          }
          strcpy((char*)ptr_out_buf, reply_command[COMMAND_OK]);
       }  
       else
          strcpy((char*)ptr_out_buf, reply_command[COMMAND_INCORRECT_VALUE]);
    }
  }
  ptr_TempDataParsingCommand->flag_end = _OK; 
} 
/**
* @brief Write and read DQF constant
* @param parString: pointer for string after char '='
* @param ptr_out_buf: pointer for output bufer
* @param ptr_str_reply: pointer for reply to char '='
* @param source: what communication channel does the team go through
* @retval None
*/
void PF_Qdf(char* parString, uint8_t *ptr_out_buf, uint8_t *ptr_str_reply, TypeSourceCommands source, TTempDataParsingCommand *ptr_TempDataParsingCommand)
{
  uint16_t ret = 0, str_len = 0, paramIndex = 0;
  char* index = parString;
  char* newData; 

     if(parString == 0)
     {
         if (CLB_lock != 0x66 && CLB_lock != 0x55 && StatusDevice.password_manufacture_enter == 0)
         {
            strcpy((char*)ptr_out_buf, reply_command[COMMAND_ACCESS_DENIED]);   
            ptr_TempDataParsingCommand->flag_end = _OK;
         }
         else
         {
            newData = strchr((char*)ReceivedBuffer,'[');  
        
            if(newData != NULL)
            {
               ++newData;
               paramIndex = atoi(newData);
               if(paramIndex >= Qf_count)
                  strcpy((char*)ptr_out_buf, reply_command[COMMAND_INCORRECT_VALUE]); 
               else
                  sprintf((char*)ptr_out_buf,"%0.2u\t%0.5u\t%0.5u\t%0.2f\t%0.6f\r\n",
                            Qf_param[paramIndex].num_param, Qf_param[paramIndex].min_Border_Qf, Qf_param[paramIndex].max_Border_Qf,
                            Qf_param[paramIndex].offset_dQf, Qf_param[paramIndex].ctg_dQf);        
               ptr_TempDataParsingCommand->flag_end = _OK;
               strcpy((char*)(ptr_out_buf + strlen((char*)ptr_out_buf)), reply_command[COMMAND_OK]); 
            }
            else
            {
               paramIndex = ptr_TempDataParsingCommand->number_frame++;
               sprintf((char*)ptr_out_buf,"%0.2u\t%0.5u\t%0.5u\t%0.2f\t%0.6f\r\n",
                            Qf_param[paramIndex].num_param, Qf_param[paramIndex].min_Border_Qf, Qf_param[paramIndex].max_Border_Qf,
                            Qf_param[paramIndex].offset_dQf, Qf_param[paramIndex].ctg_dQf);     
               if(paramIndex >= Qf_count-1)
               {
                  ptr_TempDataParsingCommand->flag_end = _OK;
                  strcpy((char*)(ptr_out_buf + strlen((char*)ptr_out_buf)), reply_command[COMMAND_OK]);
               }
               else
               {
                  ptr_TempDataParsingCommand->flag_end = _FALSE;
                  ptr_TempDataParsingCommand->flag_message_of_patrs = _FALSE;
               }
            }
         }
     }
     else
     {// write
        uint16_t lo_num;
        uint32_t lo_min,lo_max;
        float lo_diff;
        float lo_tan;
        uint8_t i=0;    
        char delimiter = ';';
        
        if (CLB_lock != 0x66 && CLB_lock != 0x55)
        {
           strcpy((char*)ptr_out_buf, reply_command[COMMAND_ACCESS_DENIED]);   
           ptr_TempDataParsingCommand->flag_message_of_patrs = _OK;
           ptr_TempDataParsingCommand->flag_end = _OK;
        }
        else
        {
            if(CheckChrString((uint8_t*)parString, "%d.-;", strlen(parString)) == _FALSE) {strcpy((char*)ptr_out_buf, reply_command[COMMAND_INCORRECT_VALUE]); ptr_TempDataParsingCommand->flag_end = _OK; return;  }  
        str_len = strlen(parString);
        if(str_len < 20) 
           strcpy((char*)ptr_out_buf, reply_command[COMMAND_INCORRECT_VALUE]); 
        else
        {
           while((ret == 0) && (i < 5))
           {
              switch (i)
              {
              case 0:
                   lo_num = atoi(index);
                   if(lo_num > Qf_count)
                      ret = 2;
                   break;           
              case 1:
                   lo_min = atol(index);
                   if(lo_num > 0 && lo_min < Qf_param[lo_num-1].max_Border_Qf)
                      ret = 2;
                   break;
              case 2:
                   lo_max = atol(index);
                   if(lo_max < lo_min)
                      ret = 2;
                   break;
              case 3:
                   lo_diff = atof(index);
                   if(lo_diff < -100000.0 || lo_diff > 100000.0)
                     ret = 2;
                   break;
              case 4:
                   lo_tan = atof(index);
                   if(lo_tan < -20.0 || lo_tan > 20.0)
                     ret = 2;
                   break;
              default:
                   ret= 2;
                   break;
              }           
              index = strchr(index, delimiter);
              ++index;
              ++i;
           }
           if(ret == 0 && i == 5)
           {
     //         sprintf((char*)OldValue,"%d,%d,%d,%f,%f", lo_num, lo_min, lo_max, lo_diff, lo_tan);  
              Eeprom_WriteQf(lo_num,lo_min,lo_max,lo_diff,lo_tan, source);
     //         sprintf((char*)NewValue,"%d,%d,%d,%f,%f", lo_num, lo_min, lo_max, lo_diff, lo_tan); 
     //         WriteChangeArcRecord(CHANGE__dQF, source);   
        
              if(lo_num == 0)
              {// save serial SGM module
                 for(uint8_t i = 0; i < 1; i++)
                 {
                    sprintf((char*)OldValue,"%s", EEPROM_SN_SGM[i]);  
                    Eeprom_WriteString((uint8_t*)EEPROM_SN_SGM[i], Sensor_Const.SN_SGM[i], strlen((char*)Sensor_Const.SN_SGM[i]), _OK);
                    sprintf((char*)NewValue,"%s", EEPROM_SN_SGM[i]); 
                    WriteChangeArcRecord((TCHANG_Type)((uint8_t)CHANGE_SERIAL_NUMBER_SGM1 + i), source);  
                 }
              }
              strcpy((char*)ptr_out_buf, reply_command[COMMAND_OK]);
           }
           else
             strcpy((char*)ptr_out_buf, reply_command[COMMAND_INCORRECT_VALUE]);
        }
        ptr_TempDataParsingCommand->flag_end = _OK;
        }
     }
} 
/**
* @brief Read info device
* @param parString: pointer for string after char '='
* @param ptr_out_buf: pointer for output bufer
* @param ptr_str_reply: pointer for reply to char '='
* @param source: what communication channel does the team go through
* @retval None
*/
void PF_DevInfo(char* parString, uint8_t *ptr_out_buf, uint8_t *ptr_str_reply, TypeSourceCommands source, TTempDataParsingCommand *ptr_TempDataParsingCommand)
{
  uint8_t temp_major, temp_minor;
  temp_major = hw_version / 100;
  temp_minor = hw_version % 100;
  if(parString ==0)
  {
     if(ptr_TempDataParsingCommand->number_frame == 0)
     {
        char type[2] = {0, 0,};
        if(valve_presence_eeprom == 1)
          type[0] = 'K';
        sprintf((char*)ptr_out_buf,"DevName=SMT_Smart-G%d%s\r\nDEVICE_SN=%s\r\nSW_VER=%0.2u.%0.2u%0.2u%0.2u\r\nHW_VER=%0.2u.%0.2u\r\nCHK_SUMM=6314\r\n",
                       type_device_int, type, Device_STM.SN_Device,  
                       FW_VERSION_MAJOR, FW_VERSION_MINOR, FW_GSM_VERSION, FW_TEST_VERSION,
                       temp_major, temp_minor);
     }
     else
     {
       // uint8_t lo_cnt = ptr_TempDataParsingCommand->number_frame - 1;
   //     if (GasMeter.SensEnable[lo_cnt])
   //     {
           sprintf((char*)ptr_out_buf, "\r\nArticleCode[0]=%s\r\nSGM_SN[0]=%s\r\nSGM_SF[0]=%d\r\nSGM_LUT[0]=%d\r\nSt_LUT[0]=%d\r\n",
                    Sensor_Const.Article[0], Sensor_Const.SN_SGM[0], Device_STM.SF[0], Device_STM.Lut[0],  st_Lut[0]);
   //     }
     }
     if(ptr_TempDataParsingCommand->number_frame >= 1)
     {
       ptr_TempDataParsingCommand->flag_end = _OK;
       ptr_TempDataParsingCommand->flag_message_of_patrs = _OK;
       strcpy((char*)ptr_out_buf + strlen((char*)ptr_out_buf), reply_command[COMMAND_OK]);
     }
     else 
     {
       ptr_TempDataParsingCommand->flag_end = _FALSE;
       ptr_TempDataParsingCommand->flag_message_of_patrs = _OK;
       ptr_TempDataParsingCommand->flag_message_of_patrs_end = _FALSE;
     }
     ptr_TempDataParsingCommand->number_frame++;     
  }
   else
   {
     strcpy((char*)ptr_out_buf, reply_command[COMMAND_READ_ONLY]);
     ptr_TempDataParsingCommand->flag_end = _OK; 
   }
}
/**
* @brief Rite and Read serial number device
* @param parString: pointer for string after char '='
* @param ptr_out_buf: pointer for output bufer
* @param ptr_str_reply: pointer for reply to char '='
* @param source: what communication channel does the team go through
* @retval None
*/
void PF_SerialNum(char* parString, uint8_t *ptr_out_buf, uint8_t *ptr_str_reply, TypeSourceCommands source, TTempDataParsingCommand *ptr_TempDataParsingCommand)
{
  uint32_t len = strlen((char*)Device_STM.SN_Device);
  if(parString ==0)
  {
      strcpy((char*)ptr_out_buf, (const char*)ptr_str_reply);
      if(len < 100)
         sprintf((char*)(ptr_out_buf + strlen((char*)ptr_out_buf)),"=%s%s", Device_STM.SN_Device, reply_OK);
  }
  else
  {
    uint8_t len_buf =0, flag_bad=_FALSE;
    if (CLB_lock != 0x66 && CLB_lock != 0x55)
       strcpy((char*)ptr_out_buf, reply_command[COMMAND_ACCESS_DENIED]);  
    else
    {
       if(CheckChrString((uint8_t*)parString, "%d", strlen(parString)) == _FALSE) {strcpy((char*)ptr_out_buf, reply_command[COMMAND_INCORRECT_VALUE]); ptr_TempDataParsingCommand->flag_end = _OK; return;  }  

       len_buf = strlen(parString);
       if((len_buf > 20) || (len_buf == 0))  //GasMeter.h  TDevice
          strcpy((char*)ptr_out_buf, reply_command[COMMAND_INCORRECT_VALUE]);  
       else
       {
          for(uint8_t i=0; i< len_buf;i++)
             if(!isdigit(parString[i]))
             {
                strcpy((char*)ptr_out_buf, reply_command[COMMAND_INCORRECT_VALUE]); 
                flag_bad = _OK;
                break;
             }
          if(flag_bad == _FALSE)
          {
            if(len < 50)
               sprintf((char*)OldValue, "%s", Device_STM.SN_Device);  
            Eeprom_WriteString((uint8_t*)Device_STM.SN_Device,(uint8_t*)parString,len_buf, _OK); 
            sprintf((char*)NewValue, "%s", Device_STM.SN_Device); 
            WriteChangeArcRecord(CHANGE_SERIAL_NUMBER, source); 
            strcpy((char*)ptr_out_buf, reply_command[COMMAND_OK]); 
          }
       }
    }
  }   
  ptr_TempDataParsingCommand->flag_end = _OK;  
}
/**
* @brief START calibration
* @param parString: pointer for string after char '='
* @param ptr_out_buf: pointer for output bufer
* @param ptr_str_reply: pointer for reply to char '='
* @param source: what communication channel does the team go through
* @retval None
*/
void PF_MeasTest(char* parString, uint8_t *ptr_out_buf, uint8_t *ptr_str_reply, TypeSourceCommands source, TTempDataParsingCommand *ptr_TempDataParsingCommand)
{
   if(parString ==0)   
   {
      sprintf((char*)ptr_out_buf,"STOP: %0.2u sek.\t%0.6f\t%0.6f\t%0.4f\t%u%s",
             TestInterval, TEST_VE_L, TEST_VE_Lm3, Test_QF_midle, TEST_QF_Count, reply_OK);  
   }
   else
   {
       uint32_t lo_TestInterval;
       if(CheckChrString((uint8_t*)parString, "%d", strlen(parString)) == _FALSE) {strcpy((char*)ptr_out_buf, reply_command[COMMAND_INCORRECT_VALUE]); ptr_TempDataParsingCommand->flag_end = _OK; return;  }  

       lo_TestInterval = atoi(parString);
       if (lo_TestInterval % Config_GasmeterMeasurementIntervalTestModeFM)
         strcpy((char*)ptr_out_buf, reply_command[COMMAND_INCORRECT_VALUE]);
       else
       {
          if ((lo_TestInterval > 0) && (lo_TestInterval < 3600))
          {
             TestInterval=lo_TestInterval;  
             ActivateTestMode(TestMode_3);
             strcpy((char*)ptr_out_buf, reply_command[COMMAND_OK]);
          }
          else
             strcpy((char*)ptr_out_buf, reply_command[COMMAND_INCORRECT_VALUE]);
       }
   }
   ptr_TempDataParsingCommand->flag_end = _OK;
}
/**
* @brief Read volume counter device
* @param parString: pointer for string after char '='
* @param ptr_out_buf: pointer for output bufer
* @param ptr_str_reply: pointer for reply to char '='
* @param source: what communication channel does the team go through
* @retval None
*/
void PF_CurVol(char* parString, uint8_t *ptr_out_buf, uint8_t *ptr_str_reply, TypeSourceCommands source, TTempDataParsingCommand *ptr_TempDataParsingCommand)
{
   if(parString ==0)   
   {
     strcpy((char*)ptr_out_buf, (const char*)ptr_str_reply);
     sprintf((char*)(ptr_out_buf + strlen((char*)ptr_out_buf)),"=%0.4f%s", GasMeter.VE_Lm3, reply_OK);
   }
   else
   {
#ifdef EDIT_VOLUME_AND_ARH 
     double buf;
     if (CLB_lock != 0x66 && CLB_lock != 0x55 && StatusDevice.password_manufacture_enter == 0)
       strcpy((char*)ptr_out_buf, reply_command[COMMAND_ACCESS_DENIED]); 
     {
       if(CheckChrString((uint8_t*)parString, "%d.", strlen(parString)) == _FALSE) {strcpy((char*)ptr_out_buf, reply_command[COMMAND_INCORRECT_VALUE]); ptr_TempDataParsingCommand->flag_end = _OK; return;  }  

        buf = atof(parString);
        GasMeter.VE_Lm3 = buf;
        strcpy((char*)ptr_out_buf, reply_command[COMMAND_OK]);
     }
#endif
     
#ifndef EDIT_VOLUME_AND_ARH 
     strcpy((char*)ptr_out_buf, reply_command[COMMAND_READ_ONLY]);
#endif  
   }
   ptr_TempDataParsingCommand->flag_end = _OK;
}
/**
* @brief registration to server
* @param parString: pointer for string after char '='
* @param ptr_out_buf: pointer for output bufer
* @param ptr_str_reply: pointer for reply to char '='
* @param source: what communication channel does the team go through
* @retval None
*/
// Server_MT,"37513830595831444A5054445954504F5146474136425049434A5947375157325A55494E44"
void PF_Server_MT(char* parString, uint8_t *ptr_out_buf, uint8_t *ptr_str_reply, TypeSourceCommands source, TTempDataParsingCommand *ptr_TempDataParsingCommand)
{
   uint8_t cnt_ch, len=0;
   char *ptr;
   uint8_t CryptStr[40];
   char type[2]={0,0};
   if(source == SOURCE_TCP)
   {
      WriteTelemetry(TELEM_EVENT_START_DATA_SERVER, EVENT_OK, GSM_Get_current_communication_gsm_number(), EVENT_END);// EVENT_END EVENT_BEGIN 
      if(valve_presence_eeprom == 1)
         type[0] = 'K';
      flag_event = flag_event_menu = TELEM_EVENT_DATA_SERVER;
      WriteTelemetry(TELEM_EVENT_DATA_SERVER, 0, 0, EVENT_BEGIN);// EVENT_END EVENT_BEGIN
   }
   
   parString = strchr(parString, '"') + 1;
   ptr = strchr(parString, '"');
   if(ptr - parString > 70 && (uint32_t)parString > 1 && ptr)
   {   
      *ptr = 0;
      if(CheckChrString((uint8_t*)parString, "%x", strlen(parString)) == _FALSE) {strcpy((char*)ptr_out_buf, reply_command[COMMAND_INCORRECT_VALUE]); ptr_TempDataParsingCommand->flag_end = _OK; return;  }  
      ptr -= 8;
      crypto_open_key[source] = strtol(ptr, 0, 16);
      cnt_ch = StrToHex(buf_str, (uint8_t*)parString , 2);
      memset((char*)CryptStr, 0, sizeof(CryptStr));
      sprintf((char*)CryptStr,"SMT-G%d%s,%s,%d.%.2d%.2d%.2d,%d", 
                                                type_device_int,
                                                type,                                                
                                                (uint8_t*)Device_STM.SN_Device, 
                                                FW_VERSION_MAJOR, 
                                                FW_VERSION_MINOR,
                                                FW_GSM_VERSION,
                                                FW_TEST_VERSION,
                                                VER_PROTOCOL); //### ������� ������ �����������
      for(uint8_t i=0; i < cnt_ch; i++) // �������� ������ ����������� �������� ����� 
         len += sprintf((char *)ptr_out_buf + len,"%02X", (CryptStr[i] ^ buf_str[i]) );
      strcat((char*)ptr_out_buf,"\r\n"); // �������� ������� ����� ������
   }
   else
     strcpy((char*)ptr_out_buf, reply_command[COMMAND_INCORRECT_VALUE]);
   ptr_TempDataParsingCommand->flag_end = _OK; 
}
/**
* @brief Command close for server
* @param parString: pointer for string after char '='
* @param ptr_out_buf: pointer for output bufer
* @param ptr_str_reply: pointer for reply to char '='
* @param source: what communication channel does the team go through
* @retval None
*/
void PF_CLOSED(char* parString, uint8_t *ptr_out_buf, uint8_t *ptr_str_reply, TypeSourceCommands source, TTempDataParsingCommand *ptr_TempDataParsingCommand)
{
  if(source == SOURCE_TCP)
  {
    FlagMessageTMR.command = MESSAGE_TCP_CLOSED;
    if(ParamServer.success_session[number_server_transmit] == _FALSE)
    {
      WriteTelemetry(TELEM_EVENT_DATA_SERVER, EVENT_ERROR, GSM_Get_current_communication_gsm_number(), EVENT_END);// EVENT_END EVENT_BEGIN 
      success_session_TCP = _FALSE;
      IncrCntSessionServer(number_server_transmit);
    }
    else
    {
      WriteTelemetry(TELEM_EVENT_DATA_SERVER, EVENT_OK, GSM_Get_current_communication_gsm_number(), EVENT_END);// EVENT_END EVENT_BEGIN 
      success_session_TCP = _OK;
    }
  }
  else
    strcpy((char*)ptr_out_buf, reply_command[COMMAND_ERROR_COMMAND]);
   ptr_TempDataParsingCommand->flag_end = _OK; 
}
/**
* @brief Read and write time settings measure type gas
* @param parString: pointer for string after char '='
* @param ptr_out_buf: pointer for output bufer
* @param ptr_str_reply: pointer for reply to char '='
* @param source: what communication channel does the team go through
* @retval None
*/
void PF_GasRTime(char* parString, uint8_t *ptr_out_buf, uint8_t *ptr_str_reply, TypeSourceCommands source, TTempDataParsingCommand *ptr_TempDataParsingCommand)
{
   if(parString ==0)   
   {
     strcpy((char*)ptr_out_buf, (const char*)ptr_str_reply);
     sprintf((char*)(ptr_out_buf + strlen((char*)ptr_out_buf)),"=%d;%d;%d%s", 
                                            Config_GasmeterGasRecognitionIntervalNormalMode,
                                            Config_GasmeterGasRecognitionIntervalMeasureMode,
                                            Config_GasmeterGasRecognitionIntervalErrorMode,
                                            reply_OK);  
   }
   else
   {
      if (CLB_lock != 0x66 && CLB_lock != 0x55 && StatusDevice.password_manufacture_enter == 0)
         strcpy((char*)ptr_out_buf, reply_command[COMMAND_ACCESS_DENIED]); 
      else
      {
         if(CheckChrString((uint8_t*)parString, "%d;", strlen(parString)) == _FALSE) {strcpy((char*)ptr_out_buf, reply_command[COMMAND_INCORRECT_VALUE]); ptr_TempDataParsingCommand->flag_end = _OK; return;  }  

         if(change_GasRecognitionInterval(parString, source) == _OK)
            strcpy((char*)ptr_out_buf, reply_command[COMMAND_OK]);
         else
            strcpy((char*)ptr_out_buf, reply_command[COMMAND_INCORRECT_VALUE]);
      }
   } 
   ptr_TempDataParsingCommand->flag_end = _OK;
}

uint8_t change_GasRecognitionInterval(char* parString, TypeSourceCommands flag_source)
{
  uint32_t temp1=65535, temp2=65535, temp3=65535;

     sscanf(parString, "%d;%d;%d", &temp1, &temp2, &temp3);
     

     if ((temp1 > 0) && (temp1 < 43201)
         && (temp2 > 0) && (temp2 < 3501)
         && (temp3 > 0) && (temp3 < 43201))   // ���� 1 ��� � �����
     {
        sprintf((char*)OldValue,"%d,%d,%d", Config_GasmeterGasRecognitionIntervalNormalMode,
                                            Config_GasmeterGasRecognitionIntervalMeasureMode,
                                            Config_GasmeterGasRecognitionIntervalErrorMode);  

        Eeprom_WriteWord((uint32_t)&Config_GasmeterGasRecognitionIntervalNormalMode, temp1);
        Eeprom_WriteWord((uint32_t)&Config_GasmeterGasRecognitionIntervalMeasureMode, temp2);
        Eeprom_WriteWord((uint32_t)&Config_GasmeterGasRecognitionIntervalErrorMode, temp3);
        
        sprintf((char*)NewValue, "%d,%d,%d", Config_GasmeterGasRecognitionIntervalNormalMode,
                                            Config_GasmeterGasRecognitionIntervalMeasureMode,
                                            Config_GasmeterGasRecognitionIntervalErrorMode);  
        WriteChangeArcRecord(CHANGE_REC_TIME, flag_source);   
       
       return _OK;
     }
    return _FALSE;
}
/**
* @brief Read and write settings measure type gas counter measure
* @param parString: pointer for string after char '='
* @param ptr_out_buf: pointer for output bufer
* @param ptr_str_reply: pointer for reply to char '='
* @param source: what communication channel does the team go through
* @retval None
*/
void PF_N_GAS_REC_UI(char* parString, uint8_t *ptr_out_buf, uint8_t *ptr_str_reply, TypeSourceCommands source, TTempDataParsingCommand *ptr_TempDataParsingCommand)
{
   if(parString ==0)   
   {
     strcpy((char*)ptr_out_buf, (const char*)ptr_str_reply);
     sprintf((char*)(ptr_out_buf + strlen((char*)ptr_out_buf)),"=%d%s", Kfactor_UI_measure_cnt_eeprom, reply_OK);  
   }
   else
   {
       if (CLB_lock != 0x66 && CLB_lock != 0x55 && StatusDevice.password_manufacture_enter == 0)
         strcpy((char*)ptr_out_buf, reply_command[COMMAND_ACCESS_DENIED]); 
      else
      {
         uint32_t temp;
         if(CheckChrString((uint8_t*)parString, "%d", strlen(parString)) == _FALSE) {strcpy((char*)ptr_out_buf, reply_command[COMMAND_INCORRECT_VALUE]); ptr_TempDataParsingCommand->flag_end = _OK; return;  }  

         temp = atoi(parString);

         if(temp > 0 && temp < 256)
         {
            sprintf((char*)OldValue,"%d", Kfactor_UI_measure_cnt_eeprom);   
            Eeprom_WriteChar((uint32_t)&Kfactor_UI_measure_cnt_eeprom, (uint8_t)temp);
            sprintf((char*)NewValue, "%d", Kfactor_UI_measure_cnt_eeprom);   
            WriteChangeArcRecord(CHANGE_GAS_CNT_ERROR_UI, source);
         }
         else
           strcpy((char*)ptr_out_buf, reply_command[COMMAND_INCORRECT_VALUE]);
      }
   }       
   ptr_TempDataParsingCommand->flag_end = _OK;
}
/**
* @brief Read and write settings measure type gas counter measure
* @param parString: pointer for string after char '='
* @param ptr_out_buf: pointer for output bufer
* @param ptr_str_reply: pointer for reply to char '='
* @param source: what communication channel does the team go through
* @retval None
*/
void PF_N_GasRTime(char* parString, uint8_t *ptr_out_buf, uint8_t *ptr_str_reply, TypeSourceCommands source, TTempDataParsingCommand *ptr_TempDataParsingCommand)
{
   if(parString ==0)   
   {
     strcpy((char*)ptr_out_buf, (const char*)ptr_str_reply);
     sprintf((char*)(ptr_out_buf + strlen((char*)ptr_out_buf)),"=%d%s", Kfactor_measure_cnt_eeprom, reply_OK);  
   }
   else
   {
       if (CLB_lock != 0x66 && CLB_lock != 0x55 && StatusDevice.password_manufacture_enter == 0)
         strcpy((char*)ptr_out_buf, reply_command[COMMAND_ACCESS_DENIED]); 
      else
      {
         if(CheckChrString((uint8_t*)parString, "%d", strlen(parString)) == _FALSE) {strcpy((char*)ptr_out_buf, reply_command[COMMAND_INCORRECT_VALUE]); ptr_TempDataParsingCommand->flag_end = _OK; return;  }  

         if(change_Kfactor_measure_cnt_eeprom(parString, source) == _OK)
            strcpy((char*)ptr_out_buf, reply_command[COMMAND_OK]);
         else
            strcpy((char*)ptr_out_buf, reply_command[COMMAND_INCORRECT_VALUE]);
      }
   }       
   ptr_TempDataParsingCommand->flag_end = _OK;
}
              
uint8_t change_Kfactor_measure_cnt_eeprom(char* parString, TypeSourceCommands flag_source)
{
     uint32_t temp;

     temp = atoi(parString);

     if(temp > 0 && temp < 256)
     {
        sprintf((char*)OldValue,"%d", Kfactor_measure_cnt_eeprom);   
        Eeprom_WriteChar((uint32_t)&Kfactor_measure_cnt_eeprom, (uint8_t)temp);
        sprintf((char*)NewValue, "%d", Kfactor_measure_cnt_eeprom);   
        
        WriteChangeArcRecord(CHANGE_GAS_CNT_ERROR, flag_source);   
       
       return _OK;
     }
     return _FALSE;  
}
/**
* @brief Read and write settings measure type gas working range
* @param parString: pointer for string after char '='
* @param ptr_out_buf: pointer for output bufer
* @param ptr_str_reply: pointer for reply to char '='
* @param source: what communication channel does the team go through
* @retval None
*/
void PF_GAS_RANGE_UI(char* parString, uint8_t *ptr_out_buf, uint8_t *ptr_str_reply, TypeSourceCommands source, TTempDataParsingCommand *ptr_TempDataParsingCommand)
{
   if(parString ==0)   
   {
     strcpy((char*)ptr_out_buf, (const char*)ptr_str_reply);
     sprintf((char*)(ptr_out_buf + strlen((char*)ptr_out_buf)),"=%d;%d%s", 
                                            Kfactor_UI_min,
                                            Kfactor_UI_max,
                                            reply_OK);  
   }
   else
   {
      if (CLB_lock != 0x66 && CLB_lock != 0x55 && StatusDevice.password_manufacture_enter == 0)
         strcpy((char*)ptr_out_buf, reply_command[COMMAND_ACCESS_DENIED]); 
      else
      {
         uint32_t temp1=65535, temp2=65535;
         if(CheckChrString((uint8_t*)parString, "%d;", strlen(parString)) == _FALSE) {strcpy((char*)ptr_out_buf, reply_command[COMMAND_INCORRECT_VALUE]); ptr_TempDataParsingCommand->flag_end = _OK; return;  }  

         sscanf(parString, "%d;%d", &temp1, &temp2);
     
         if (temp1 < temp2 
            && temp2 < 40961)   
         {
           sprintf((char*)OldValue,"%d,%d", 
                                            Kfactor_UI_min,
                                            Kfactor_UI_max); 

           Eeprom_WriteWord((uint32_t)&Kfactor_UI_min, temp1);
           Eeprom_WriteWord((uint32_t)&Kfactor_UI_max, temp2);
        
           sprintf((char*)NewValue,"%d,%d", 
                                            Kfactor_UI_min,
                                            Kfactor_UI_max); 
           WriteChangeArcRecord(CHANGE_GAS_RANGE_UI, source);   
       
           strcpy((char*)ptr_out_buf, reply_command[COMMAND_OK]);
         }
         else
           strcpy((char*)ptr_out_buf, reply_command[COMMAND_INCORRECT_VALUE]);
      }
   }      
   ptr_TempDataParsingCommand->flag_end = _OK;
}
/**
* @brief Read and write settings measure type gas working range
* @param parString: pointer for string after char '='
* @param ptr_out_buf: pointer for output bufer
* @param ptr_str_reply: pointer for reply to char '='
* @param source: what communication channel does the team go through
* @retval None
*/
void PF_GasRange(char* parString, uint8_t *ptr_out_buf, uint8_t *ptr_str_reply, TypeSourceCommands source, TTempDataParsingCommand *ptr_TempDataParsingCommand)
{
   if(parString ==0)   
   {
     strcpy((char*)ptr_out_buf, (const char*)ptr_str_reply);
     sprintf((char*)(ptr_out_buf + strlen((char*)ptr_out_buf)),"=%d;%d;%d%s", 
                                            Kfactor_min,
                                            Kfactor_average,
                                            Kfactor_max,
                                            reply_OK);  
   }
   else
   {
      if (CLB_lock != 0x66 && CLB_lock != 0x55 && StatusDevice.password_manufacture_enter == 0)
         strcpy((char*)ptr_out_buf, reply_command[COMMAND_ACCESS_DENIED]); 
      else
      {
         if(CheckChrString((uint8_t*)parString, "%d;", strlen(parString)) == _FALSE) {strcpy((char*)ptr_out_buf, reply_command[COMMAND_INCORRECT_VALUE]); ptr_TempDataParsingCommand->flag_end = _OK; return;  }  

         if(change_Kfactor_range(parString, source) == _OK)
           strcpy((char*)ptr_out_buf, reply_command[COMMAND_OK]);
         else
           strcpy((char*)ptr_out_buf, reply_command[COMMAND_INCORRECT_VALUE]);
      }
   }      
   ptr_TempDataParsingCommand->flag_end = _OK;
}

uint8_t change_Kfactor_range(char* parString, TypeSourceCommands flag_source)
{
     uint32_t temp1=65535, temp2=65535, temp3=65535;

     sscanf(parString, "%d;%d;%d", &temp1, &temp2, &temp3);
     
     if (temp1 < temp2 
         && temp2 < temp3
         && temp3 < 40961)   // ���� 1 ��� � �����
     {
        sprintf((char*)OldValue,"%d,%d,%d", 
                                            Kfactor_min,
                                            Kfactor_average,
                                            Kfactor_max); 

        Eeprom_WriteWord((uint32_t)&Kfactor_min, temp1);
        Eeprom_WriteWord((uint32_t)&Kfactor_average, temp2);
        Eeprom_WriteWord((uint32_t)&Kfactor_max, temp3);
        
        sprintf((char*)NewValue, "%d,%d,%d", 
                                            Kfactor_min,
                                            Kfactor_average,
                                            Kfactor_max); 
        WriteChangeArcRecord(CHANGE_GAS_RANGE, flag_source);   
       
       return _OK;
     }
     return _FALSE;   
}
/**
* @brief Read and write settings time measure gas
* @param parString: pointer for string after char '='
* @param ptr_out_buf: pointer for output bufer
* @param ptr_str_reply: pointer for reply to char '='
* @param source: what communication channel does the team go through
* @retval None
*/
void PF_GasLTime(char* parString, uint8_t *ptr_out_buf, uint8_t *ptr_str_reply, TypeSourceCommands source, TTempDataParsingCommand *ptr_TempDataParsingCommand)
{
  if(parString ==0)   
  {
     strcpy((char*)ptr_out_buf, (const char*)ptr_str_reply);
     sprintf((char*)(ptr_out_buf + strlen((char*)ptr_out_buf)),"=%u%s", Config_GasmeterMeasurementIntervalNormalModeFM, reply_OK);  
  }
  else
  {
     uint32_t lo_TestInterval;
 
      if (CLB_lock != 0x66 && CLB_lock != 0x55 && StatusDevice.password_manufacture_enter == 0)
         strcpy((char*)ptr_out_buf, reply_command[COMMAND_ACCESS_DENIED]); 
      else
      {
        lo_TestInterval = atoi(parString);
         if(CheckChrString((uint8_t*)parString, "%d", strlen(parString)) == _FALSE) {strcpy((char*)ptr_out_buf, reply_command[COMMAND_INCORRECT_VALUE]); ptr_TempDataParsingCommand->flag_end = _OK; return;  }  

        if ((lo_TestInterval > 0) && (lo_TestInterval < 43200)) 
        {
           sprintf((char*)OldValue,"%d", Config_GasmeterMeasurementIntervalNormalModeFM);  
           Eeprom_WriteWord((uint32_t)&Config_GasmeterMeasurementIntervalNormalModeFM,lo_TestInterval);
           sprintf((char*)NewValue, "%d", Config_GasmeterMeasurementIntervalNormalModeFM); 
           WriteChangeArcRecord(CHANGE_L_MES_TIME, source);  
           strcpy((char*)ptr_out_buf, reply_command[COMMAND_OK]);
        }
        else
          strcpy((char*)ptr_out_buf, reply_command[COMMAND_INCORRECT_VALUE]);
     }
  }
  ptr_TempDataParsingCommand->flag_end = _OK; 
}
/**
* @brief 
* @param parString: pointer for string after char '='
* @param ptr_out_buf: pointer for output bufer
* @param ptr_str_reply: pointer for reply to char '='
* @param source: what communication channel does the team go through
* @retval None
*/
void PF_TestRTime(char* parString, uint8_t *ptr_out_buf, uint8_t *ptr_str_reply, TypeSourceCommands source, TTempDataParsingCommand *ptr_TempDataParsingCommand)
{
   if(parString == 0)   
   {
      strcpy((char*)ptr_out_buf, (const char*)ptr_str_reply);
      sprintf((char*)(ptr_out_buf + strlen((char*)ptr_out_buf)),"=%u%s", Config_GasmeterGasRecognitionIntervalTestMode, reply_OK);  
   }
   else
   {
     uint32_t lo_TestInterval;
     if (CLB_lock != 0x66 && CLB_lock != 0x55 && StatusDevice.password_manufacture_enter == 0)
         strcpy((char*)ptr_out_buf, reply_command[COMMAND_ACCESS_DENIED]); 
     else
     {
         if(CheckChrString((uint8_t*)parString, "%d", strlen(parString)) == _FALSE) {strcpy((char*)ptr_out_buf, reply_command[COMMAND_INCORRECT_VALUE]); ptr_TempDataParsingCommand->flag_end = _OK; return;  }  

        lo_TestInterval = atoi(parString);
        if((lo_TestInterval > 0) && (lo_TestInterval < 43201))   // ���� 1 ��� � �����
        {
           sprintf((char*)OldValue, "%d", Config_GasmeterGasRecognitionIntervalTestMode);
           Eeprom_WriteWord((uint32_t)&Config_GasmeterGasRecognitionIntervalTestMode, lo_TestInterval);
           sprintf((char*)NewValue, "%d", Config_GasmeterGasRecognitionIntervalTestMode);
           WriteChangeArcRecord(CHANGE_TEST_REC_TIME, source);
           strcpy((char*)ptr_out_buf, reply_command[COMMAND_OK]);
        }
        else
          strcpy((char*)ptr_out_buf, reply_command[COMMAND_INCORRECT_VALUE]);
     }
   }         
   ptr_TempDataParsingCommand->flag_end = _OK; 
}
/**
* @brief 
* @param parString: pointer for string after char '='
* @param ptr_out_buf: pointer for output bufer
* @param ptr_str_reply: pointer for reply to char '='
* @param source: what communication channel does the team go through
* @retval None
*/
void PF_TestLTime(char* parString, uint8_t *ptr_out_buf, uint8_t *ptr_str_reply, TypeSourceCommands source, TTempDataParsingCommand *ptr_TempDataParsingCommand)
{
   if(parString ==0)   
   {
      strcpy((char*)ptr_out_buf, (const char*)ptr_str_reply);
      sprintf((char*)(ptr_out_buf + strlen((char*)ptr_out_buf)),"=%u%s", Config_GasmeterMeasurementIntervalTestModeFM, reply_OK);  
   }
   else
   {
      uint32_t lo_TestInterval;
      if (CLB_lock != 0x66 && CLB_lock != 0x55 && StatusDevice.password_manufacture_enter == 0)
         strcpy((char*)ptr_out_buf, reply_command[COMMAND_ACCESS_DENIED]); 
      else
      {
         if(CheckChrString((uint8_t*)parString, "%d", strlen(parString)) == _FALSE) {strcpy((char*)ptr_out_buf, reply_command[COMMAND_INCORRECT_VALUE]); ptr_TempDataParsingCommand->flag_end = _OK; return;  }  

         lo_TestInterval = atoi(parString);
         if ((lo_TestInterval > 0) && (lo_TestInterval < 43200))   // �� ����� 2 ��� � �����
         {
            sprintf((char*)OldValue, "%d", Config_GasmeterMeasurementIntervalTestModeFM);
            Eeprom_WriteWord((uint32_t)&Config_GasmeterMeasurementIntervalTestModeFM, lo_TestInterval);
            sprintf((char*)NewValue, "%d", Config_GasmeterMeasurementIntervalTestModeFM);
            WriteChangeArcRecord(CHANGE_TEST_L_MES_TIME, source);
            strcpy((char*)ptr_out_buf, reply_command[COMMAND_OK]);
         }
         else
            strcpy((char*)ptr_out_buf, reply_command[COMMAND_INCORRECT_VALUE]);
      }
   }         
   ptr_TempDataParsingCommand->flag_end = _OK;    
}
/**
* @brief Read arhive  frame
* @param parString: pointer for string after char '='
* @param ptr_out_buf: pointer for output bufer
* @param ptr_str_reply: pointer for reply to char '='
* @param source: what communication channel does the team go through
* @retval None
*/
void PF_ReadArc(char* parString, uint8_t *ptr_out_buf, uint8_t *ptr_str_reply, TypeSourceCommands source, TTempDataParsingCommand *ptr_TempDataParsingCommand)
{
   if(TransmitARCHIVE((uint8_t*)parString, (uint8_t*)ptr_out_buf, ptr_TempDataParsingCommand) == _FALSE)
   {
     ptr_TempDataParsingCommand->flag_message_of_patrs = _OK;
     ptr_TempDataParsingCommand->flag_end = _OK;
     strcpy((char*)ptr_out_buf, reply_command[COMMAND_INCORRECT_VALUE]);
   }
   else
   {
      if(ptr_TempDataParsingCommand->flag_end == _OK/* && ptr_TempDataParsingCommand->flag_message_of_patrs == _OK*/)
      {
         strcpy((char*)ptr_out_buf + strlen((char*)ptr_out_buf), reply_command[COMMAND_OK]);
      }
   }
}
/**
* @brief Read arhive  frame
* @param parString: pointer for string after char '='
* @param ptr_out_buf: pointer for output bufer
* @param ptr_str_reply: pointer for reply to char '='
* @param source: what communication channel does the team go through
* @retval None
*/
void PF_ArcNumRecords(char* parString, uint8_t *ptr_out_buf, uint8_t *ptr_str_reply, TypeSourceCommands source, TTempDataParsingCommand *ptr_TempDataParsingCommand)
{
   if(parString ==0)   
   {
      sprintf((char*)ptr_out_buf,"Hour_Arc=%u\r\nDayli_Arc=%u\r\nSystem_Arc=%u\r\nChange_Arc=%u\r\nTelemetry_Arc=%u%s",
            IntArcRecNo-1, DayArcRecNo-1, SystemArcRecNo-1, ChangeArcRecNo - 1, TelemetryArcRecNo - 1, reply_OK);
   }
   else
   {
#ifdef EDIT_VOLUME_AND_ARH 
     uint32_t buf_int, buf_day, buf_sys, buf_ch, buf_tel;
     if (CLB_lock != 0x66 && CLB_lock != 0x55)
       strcpy((char*)ptr_out_buf, reply_command[COMMAND_ACCESS_DENIED]);   
     else
     {
         if(CheckChrString((uint8_t*)parString, "%d;", strlen(parString)) == _FALSE) {strcpy((char*)ptr_out_buf, reply_command[COMMAND_INCORRECT_VALUE]); ptr_TempDataParsingCommand->flag_end = _OK; return;  }  

        sprintf((char*)OldValue, "%d,%d,%d,%d,%d",IntArcRecNo - 1, DayArcRecNo - 1, SystemArcRecNo - 1, ChangeArcRecNo - 1, TelemetryArcRecNo - 1);  // + 
        sscanf((char*)parString,"%d;%d;%d;%d;%d", &buf_int, &buf_day, &buf_sys, &buf_ch, &buf_tel);
        Eeprom_WriteDword((uint32_t)&IntArcRecNo, buf_int + 1);
        Eeprom_WriteDword((uint32_t)&DayArcRecNo, buf_day + 1);
        Eeprom_WriteDword((uint32_t)&SystemArcRecNo, buf_sys + 1);
        Eeprom_WriteDword((uint32_t)&ChangeArcRecNo, buf_ch + 1);
        Eeprom_WriteDword((uint32_t)&TelemetryArcRecNo, buf_tel + 1);
        sprintf((char*)NewValue, "%d,%d,%d,%d,%d",IntArcRecNo - 1, DayArcRecNo - 1, SystemArcRecNo - 1, ChangeArcRecNo - 1, TelemetryArcRecNo - 1);  // + 
        WriteChangeArcRecord(CHANGE_ARCHIVE_NUMBER, source);
        strcpy((char*)ptr_out_buf, reply_command[COMMAND_OK]);
     }
#endif
     
#ifndef EDIT_VOLUME_AND_ARH 
     strcpy((char*)ptr_out_buf, reply_command[COMMAND_READ_ONLY]);
#endif     
   }
   ptr_TempDataParsingCommand->flag_end = _OK; 
}

void PF_ServerURL(char* parString, uint8_t *ptr_out_buf, uint8_t *ptr_str_reply, TypeSourceCommands source, TTempDataParsingCommand *ptr_TempDataParsingCommand, uint8_t number)
{
  uint32_t len_s, len_p;
  len_s = strlen((char*)MdmURL_Server);
  len_p = strlen((char*)MdmPort_Server);
  if(parString ==0)   
  {
     strcpy((char*)ptr_out_buf, (const char*)ptr_str_reply);
     if((len_s + len_p) < 100)
       sprintf((char*)(ptr_out_buf + strlen((char*)ptr_out_buf)),"=%s:%s%s",
             &MdmURL_Server[number][0], &MdmPort_Server[number][0], reply_OK);  
  }
  else
  {
     if (number == 2 && CLB_lock != 0x66 && CLB_lock != 0x55 && StatusDevice.password_manufacture_enter == 0)
         strcpy((char*)ptr_out_buf, reply_command[COMMAND_ACCESS_DENIED]);   
     else 
     { 
        if(CheckChrString((uint8_t*)parString, "%d:.", strlen(parString)) == _FALSE) {strcpy((char*)ptr_out_buf, reply_command[COMMAND_INCORRECT_VALUE]); ptr_TempDataParsingCommand->flag_end = _OK; return;  }  

        if(ChangeURLPortServer((char*)parString, number, source) == _OK)
           strcpy((char*)ptr_out_buf, reply_command[COMMAND_OK]);
        else
           strcpy((char*)ptr_out_buf, reply_command[COMMAND_INCORRECT_VALUE]);
     }
   }       
   ptr_TempDataParsingCommand->flag_end = _OK;
} 

void PF_ServerURL1(char* parString, uint8_t *ptr_out_buf, uint8_t *ptr_str_reply, TypeSourceCommands source, TTempDataParsingCommand *ptr_TempDataParsingCommand)
{
   PF_ServerURL(parString, ptr_out_buf, ptr_str_reply, source, ptr_TempDataParsingCommand, 0);
}
void PF_ServerURL2(char* parString, uint8_t *ptr_out_buf, uint8_t *ptr_str_reply, TypeSourceCommands source, TTempDataParsingCommand *ptr_TempDataParsingCommand)
{
   PF_ServerURL(parString, ptr_out_buf, ptr_str_reply, source, ptr_TempDataParsingCommand, 1);
}
void PF_ServerURL3(char* parString, uint8_t *ptr_out_buf, uint8_t *ptr_str_reply, TypeSourceCommands source, TTempDataParsingCommand *ptr_TempDataParsingCommand)
{
   PF_ServerURL(parString, ptr_out_buf, ptr_str_reply, source, ptr_TempDataParsingCommand, 2);
}

//QT style, auto-documenting with Doxygen, https://habr.com/ru/post/252101/ 
//QT style, auto-documenting with Doxygen, https://habr.com/ru/post/252101/ 
/*!
Edite parametr setting server
\param[in] pointer string
\param[in] pointer output bufer
\param[in] pointer request
\param[in] source command
\param[in] temp data source command
\return void
*/
uint8_t ChangeURLPortServer(char *ptr_str, uint8_t index_server, uint8_t source)
{
   char *end_ptr;
   uint8_t rez = _OK, str_len_port;
   uint32_t adser[4], adser_cmp[4];
   uint32_t port_str, port_str_cmp;
   end_ptr = strchr(ptr_str, ':');
   str_len_port = strlen(end_ptr + 1);
 //  if(end_ptr != 0)
 //  {
      if(CheckUrlServer(ptr_str) == _OK)
      {
         sscanf(ptr_str, "%d.%d.%d.%d:%d", &adser_cmp[0], &adser_cmp[1], &adser_cmp[2], &adser_cmp[3], &port_str_cmp);
         for(uint8_t i = 0; i < 3; i++)
         {
            sscanf(&MdmURL_Server[i][0], "%d.%d.%d.%d", &adser[0], &adser[1], &adser[2], &adser[3]);
            port_str = atoi(&MdmPort_Server[i][0]);
            if(port_str == port_str_cmp)
            {
              rez = _FALSE;
              for(uint8_t j = 0; j < 4; j++)
              { 
                 if(adser_cmp[j] != adser[j])
                 {
                    rez = _OK;
                    j = 4;
                 }
                 else
                    rez = _FALSE;
              }
            }
            if(rez == _FALSE)
            {
               i = 3; 
            }
         }
         if(rez == _OK)
         {
           uint32_t len1;
           len1 = strlen(&MdmURL_Server[index_server][0]) + strlen(&MdmPort_Server[index_server][0]);
           if(len1 < 62)
              sprintf((char*)OldValue,"%s:%s", &MdmURL_Server[index_server][0], &MdmPort_Server[index_server][0]);  

            Eeprom_WriteString((uint8_t*)&MdmURL_Server[index_server][0], (uint8_t*)ptr_str, end_ptr - ptr_str, _OK);
            Eeprom_WriteString((uint8_t*)&MdmPort_Server[index_server][0], (uint8_t*)end_ptr + 1, str_len_port, _OK);
            len1 = strlen(&MdmURL_Server[index_server][0]) + strlen(&MdmPort_Server[index_server][0]);
            if(len1 < 62)
               sprintf((char*)NewValue,"%s:%s", &MdmURL_Server[index_server][0], &MdmPort_Server[index_server][0]); 
            WriteChangeArcRecord((TCHANG_Type)(CHANGE_TCP_SERVER1 + index_server), source);   
     
            if(source == CHANGE_TCP)
               flag_change_server_url = _OK;
         }
      }
      else
        rez = _FALSE;
      /*
   }
   else
   {
      sprintf((char*)OldValue,"%s:%s", &MdmURL_Server[index_server][0], &MdmPort_Server[index_server][0]);  

      Eeprom_WriteChar((uint32_t)&MdmURL_Server[index_server][0], 0);
      Eeprom_WriteChar((uint32_t)&MdmPort_Server[index_server][0], 0);
       
      sprintf((char*)NewValue,"%s:%s", &MdmURL_Server[index_server][0], &MdmPort_Server[index_server][0]); 
      WriteChangeArcRecord((TCHANG_Type)(CHANGE_TCP_SERVER1 + index_server), source);   
     
      rez = _OK;
   }
      */
  return rez;
}
//QT style, auto-documenting with Doxygen, https://habr.com/ru/post/252101/ 
//QT style, auto-documenting with Doxygen, https://habr.com/ru/post/252101/ 
/*!
Read and  write APNAdress
\param[in] pointer string
\param[in] pointer output bufer
\param[in] pointer request
\param[in] source command
\param[in] temp data source command
\return void
*/
void PF_APNAdress(char* parString, uint8_t *ptr_out_buf, uint8_t *ptr_str_reply, TypeSourceCommands source, TTempDataParsingCommand *ptr_TempDataParsingCommand)
{
  uint16_t lo_strlen;
  
  if(parString ==0)   
  {
     strcpy((char*)ptr_out_buf, (const char*)ptr_str_reply);
     sprintf((char*)(ptr_out_buf + strlen((char*)ptr_out_buf)),"=%s%s", MdmAPN_Adress, reply_OK);  
  }
  else
  {
         if(CheckChrString((uint8_t*)parString, "%d.:-_%L%l", strlen(parString)) == _FALSE) {strcpy((char*)ptr_out_buf, reply_command[COMMAND_INCORRECT_VALUE]); ptr_TempDataParsingCommand->flag_end = _OK; return;  }  

    lo_strlen = strlen(parString); 
    if (lo_strlen < sizeof(MdmAPN_Adress)) 
    {
       if(strlen((char*)MdmAPN_Adress) < 60)
          sprintf((char*)OldValue,"%s",MdmAPN_Adress);  
       Eeprom_WriteString((uint8_t*)&MdmAPN_Adress,(uint8_t*)parString,lo_strlen, _OK);
       if(strlen((char*)MdmAPN_Adress) < 60)
         sprintf((char*)NewValue,"%s",MdmAPN_Adress); 
       WriteChangeArcRecord(CHANGE_APN_ADDRESS, source);      
       strcpy((char*)ptr_out_buf, reply_command[COMMAND_OK]);
    }
    else
      strcpy((char*)ptr_out_buf, reply_command[COMMAND_INCORRECT_VALUE]);  
  }
  ptr_TempDataParsingCommand->flag_end = _OK;
}
//QT style, auto-documenting with Doxygen, https://habr.com/ru/post/252101/ 
//QT style, auto-documenting with Doxygen, https://habr.com/ru/post/252101/ 
/*!
Read and  write APNLogin
\param[in] pointer string
\param[in] pointer output bufer
\param[in] pointer request
\param[in] source command
\param[in] temp data source command
\return void
*/
void PF_APNLogin(char* parString, uint8_t *ptr_out_buf, uint8_t *ptr_str_reply, TypeSourceCommands source, TTempDataParsingCommand *ptr_TempDataParsingCommand)
{
  uint16_t lo_strlen;
  
  if(parString ==0)   
  {
     strcpy((char*)ptr_out_buf, (const char*)ptr_str_reply);
     sprintf((char*)(ptr_out_buf + strlen((char*)ptr_out_buf)),"=%s%s", MdmAPN_Login, reply_OK);  
  }
  else
  {
         if(CheckChrString((uint8_t*)parString, "%d.:-_%L%l", strlen(parString)) == _FALSE) {strcpy((char*)ptr_out_buf, reply_command[COMMAND_INCORRECT_VALUE]); ptr_TempDataParsingCommand->flag_end = _OK; return;  }  

    lo_strlen = strlen(parString); 
    if (lo_strlen < sizeof(MdmAPN_Login))
    {
       if(strlen((char*)MdmAPN_Login) < 60)
          sprintf((char*)OldValue,"%s", MdmAPN_Login);  
       Eeprom_WriteString((uint8_t*)&MdmAPN_Login,(uint8_t*)parString, lo_strlen, _OK);
       if(strlen((char*)MdmAPN_Login) < 60)
          sprintf((char*)NewValue,"%s",MdmAPN_Login); 
       WriteChangeArcRecord(CHANGE_APN_LOGIN, source); 
       strcpy((char*)ptr_out_buf, reply_command[COMMAND_OK]);
    }
    else
      strcpy((char*)ptr_out_buf, reply_command[COMMAND_INCORRECT_VALUE]);  
  }
  ptr_TempDataParsingCommand->flag_end = _OK;
}
//QT style, auto-documenting with Doxygen, https://habr.com/ru/post/252101/ 
//QT style, auto-documenting with Doxygen, https://habr.com/ru/post/252101/ 
/*!
Read and  write APNPassword
\param[in] pointer string
\param[in] pointer output bufer
\param[in] pointer request
\param[in] source command
\param[in] temp data source command
\return void
*/
void PF_APNPassword(char* parString, uint8_t *ptr_out_buf, uint8_t *ptr_str_reply, TypeSourceCommands source, TTempDataParsingCommand *ptr_TempDataParsingCommand)
{
  uint16_t lo_strlen;
  
  if(parString ==0)   
  {
     strcpy((char*)ptr_out_buf, (const char*)ptr_str_reply);
     sprintf((char*)(ptr_out_buf + strlen((char*)ptr_out_buf)),"=%s%s", MdmAPN_Password, reply_OK);  
  }
  else
  {
         if(CheckChrString((uint8_t*)parString, "%d.:-_%L%l", strlen(parString)) == _FALSE) {strcpy((char*)ptr_out_buf, reply_command[COMMAND_INCORRECT_VALUE]); ptr_TempDataParsingCommand->flag_end = _OK; return;  }  

    lo_strlen = strlen(parString); 
    if (lo_strlen < sizeof(MdmAPN_Password) )
    {
       if(strlen((char*)MdmAPN_Password) < 60)
          sprintf((char*)OldValue,"%s",MdmAPN_Password);  
       Eeprom_WriteString((uint8_t*)&MdmAPN_Password,(uint8_t*)parString,lo_strlen, _OK);
       if(strlen((char*)MdmAPN_Password) < 60)
          sprintf((char*)NewValue,"%s",MdmAPN_Password); 
       WriteChangeArcRecord(CHANGE_APN_PASWORD, source); 
       strcpy((char*)ptr_out_buf, reply_command[COMMAND_OK]); 
    }
    else
      strcpy((char*)ptr_out_buf, reply_command[COMMAND_INCORRECT_VALUE]);  
  }
  ptr_TempDataParsingCommand->flag_end = _OK;
} 
//QT style, auto-documenting with Doxygen, https://habr.com/ru/post/252101/ 
//QT style, auto-documenting with Doxygen, https://habr.com/ru/post/252101/ 
/*!
Read Qmax
\param[in] pointer string
\param[in] pointer output bufer
\param[in] pointer request
\param[in] source command
\param[in] temp data source command
\return void
*/
void PF_MaxFlowRate(char* parString, uint8_t *ptr_out_buf, uint8_t *ptr_str_reply, TypeSourceCommands source, TTempDataParsingCommand *ptr_TempDataParsingCommand)
{
  if(parString ==0)   
  {
    strcpy((char*)ptr_out_buf, (const char*)ptr_str_reply);
    sprintf((char*)(ptr_out_buf + strlen((char*)ptr_out_buf)),"=%0.4f%s", sensorTypeParameters[_SgmType].Q_max, reply_OK);  
  }
  else
    strcpy((char*)ptr_out_buf, reply_command[COMMAND_READ_ONLY]);
  ptr_TempDataParsingCommand->flag_end = _OK;
}
//QT style, auto-documenting with Doxygen, https://habr.com/ru/post/252101/ 
//QT style, auto-documenting with Doxygen, https://habr.com/ru/post/252101/ 
/*!
Read and write Max Temp External
\param[in] pointer string
\param[in] pointer output bufer
\param[in] pointer request
\param[in] source command
\param[in] temp data source command
\return void
*/
void PF_EXT_MAX_TEMP(char* parString, uint8_t *ptr_out_buf, uint8_t *ptr_str_reply, TypeSourceCommands source, TTempDataParsingCommand *ptr_TempDataParsingCommand)
{
  if(parString ==0)   
  {
     strcpy((char*)ptr_out_buf, (const char*)ptr_str_reply);
     sprintf((char*)(ptr_out_buf + strlen((char*)ptr_out_buf)),"=%0.2f%s", ExtTempMax, reply_OK);  
  }
  else
  {
     if (CLB_lock != 0x66 && CLB_lock != 0x55 && StatusDevice.password_manufacture_enter == 0)
       strcpy((char*)ptr_out_buf, reply_command[COMMAND_ACCESS_DENIED]);   
     else
     {
        float temp;
         if(CheckChrString((uint8_t*)parString, "%d.", strlen(parString)) == _FALSE) {strcpy((char*)ptr_out_buf, reply_command[COMMAND_INCORRECT_VALUE]); ptr_TempDataParsingCommand->flag_end = _OK; return;  }  

        temp = atof(parString);
        if(temp > 30 && temp < 100)
        {
           sprintf((char*)OldValue,"%f", ExtTempMax);  
           Eeprom_WriteFloat((uint32_t)&ExtTempMax, temp);
           sprintf((char*)NewValue, "%f", ExtTempMax);  
           WriteChangeArcRecord(CHANGE_EXT_MAX_TEMP, source);
           strcpy((char*)ptr_out_buf, reply_command[COMMAND_OK]);
        }
        else
           strcpy((char*)ptr_out_buf, reply_command[COMMAND_INCORRECT_VALUE]);  
     }
  }
  ptr_TempDataParsingCommand->flag_end = _OK;
}
//QT style, auto-documenting with Doxygen, https://habr.com/ru/post/252101/ 
//QT style, auto-documenting with Doxygen, https://habr.com/ru/post/252101/ 
/*!
Read and write Min Temp External
\param[in] pointer string
\param[in] pointer output bufer
\param[in] pointer request
\param[in] source command
\param[in] temp data source command
\return void
*/
void PF_EXT_MIN_TEMP(char* parString, uint8_t *ptr_out_buf, uint8_t *ptr_str_reply, TypeSourceCommands source, TTempDataParsingCommand *ptr_TempDataParsingCommand)
{
  if(parString ==0)   
  {
     strcpy((char*)ptr_out_buf, (const char*)ptr_str_reply);
     sprintf((char*)(ptr_out_buf + strlen((char*)ptr_out_buf)),"=%0.2f%s", ExtTempMin, reply_OK);  
  }
  else
  {
     if (CLB_lock != 0x66 && CLB_lock != 0x55 && StatusDevice.password_manufacture_enter == 0)
       strcpy((char*)ptr_out_buf, reply_command[COMMAND_ACCESS_DENIED]);   
     else
     {
        float temp;
         if(CheckChrString((uint8_t*)parString, "%d.-", strlen(parString)) == _FALSE) {strcpy((char*)ptr_out_buf, reply_command[COMMAND_INCORRECT_VALUE]); ptr_TempDataParsingCommand->flag_end = _OK; return;  }  

        temp = atof(parString);
        if(temp > -50 && temp < 10)
        {
           sprintf((char*)OldValue,"%f", ExtTempMin);  
           Eeprom_WriteFloat((uint32_t)&ExtTempMin, temp);
           sprintf((char*)NewValue, "%f", ExtTempMin);  
           WriteChangeArcRecord(CHANGE_EXT_MIN_TEMP, source);
           strcpy((char*)ptr_out_buf, reply_command[COMMAND_OK]);
        }
        else
           strcpy((char*)ptr_out_buf, reply_command[COMMAND_INCORRECT_VALUE]);  
     }
  }
  ptr_TempDataParsingCommand->flag_end = _OK;
}
//QT style, auto-documenting with Doxygen, https://habr.com/ru/post/252101/ 
/*!
Read and write counter measure Temper External
\param[in] pointer string
\param[in] pointer output bufer
\param[in] pointer request
\param[in] source command
\param[in] temp data source command
\return void
*/
void PF_EXT_TEMP_CNT(char* parString, uint8_t *ptr_out_buf, uint8_t *ptr_str_reply, TypeSourceCommands source, TTempDataParsingCommand *ptr_TempDataParsingCommand)
{
  if(parString ==0)   
  {
     strcpy((char*)ptr_out_buf, (const char*)ptr_str_reply);
     sprintf((char*)(ptr_out_buf + strlen((char*)ptr_out_buf)),"=%d%s", going_beyond_T_environ_cnt_measurement, reply_OK);  
  }
  else
  {
     if (CLB_lock != 0x66 && CLB_lock != 0x55 && StatusDevice.password_manufacture_enter == 0)
       strcpy((char*)ptr_out_buf, reply_command[COMMAND_ACCESS_DENIED]);   
     else
     {
        uint32_t temp;
         if(CheckChrString((uint8_t*)parString, "%d", strlen(parString)) == _FALSE) {strcpy((char*)ptr_out_buf, reply_command[COMMAND_INCORRECT_VALUE]); ptr_TempDataParsingCommand->flag_end = _OK; return;  }  

        temp = atoi(parString);
        if(temp > 0 && temp < 1001)
        {
           sprintf((char*)OldValue,"%d", going_beyond_T_environ_cnt_measurement);  
           Eeprom_WriteChar((uint32_t)&going_beyond_T_environ_cnt_measurement, (uint16_t)temp);
           sprintf((char*)NewValue, "%d", going_beyond_T_environ_cnt_measurement);  
           WriteChangeArcRecord(CHANGE_EXT_TEMP_CNT, source);
           strcpy((char*)ptr_out_buf, reply_command[COMMAND_OK]);
           //Clear_cnt_ExtTempMidl();
         //  Clear_cnt_ExtTempMidl_2();
          // Clear_cnt_ExtTempMidl_3();
        }
        else
           strcpy((char*)ptr_out_buf, reply_command[COMMAND_INCORRECT_VALUE]);  
     }
  }
  ptr_TempDataParsingCommand->flag_end = _OK;
}
//QT style, auto-documenting with Doxygen, https://habr.com/ru/post/252101/ 
/*!
Read and write MaxTemp
\param[in] pointer string
\param[in] pointer output bufer
\param[in] pointer request
\param[in] source command
\param[in] temp data source command
\return void
*/
void PF_MaxTemp(char* parString, uint8_t *ptr_out_buf, uint8_t *ptr_str_reply, TypeSourceCommands source, TTempDataParsingCommand *ptr_TempDataParsingCommand)
{
  if(parString ==0)   
  {
     strcpy((char*)ptr_out_buf, (const char*)ptr_str_reply);
     sprintf((char*)(ptr_out_buf + strlen((char*)ptr_out_buf)),"=%0.2f%s", sensorTypeParameters[_SgmType].Tmax, reply_OK);  
  }
  else
  {
    strcpy((char*)ptr_out_buf, reply_command[COMMAND_READ_ONLY]);
    /*
     if (CLB_lock != 0x66 && CLB_lock != 0x55 && StatusDevice.password_manufacture_enter == 0)
       strcpy((char*)ptr_out_buf, reply_command[COMMAND_ACCESS_DENIED]);   
     else
     {
        float temp;
         if(CheckChrString((uint8_t*)parString, "%d.", strlen(parString)) == _FALSE) {strcpy((char*)ptr_out_buf, reply_command[COMMAND_INCORRECT_VALUE]); ptr_TempDataParsingCommand->flag_end = _OK; return;  }  

        temp = atof(parString);
        if(temp > 30 && temp < 100)
        {
           sprintf((char*)OldValue,"%f", sensorTypeParameters[_SgmType].Tmax);  
           Eeprom_WriteFloat((uint32_t)&sensorTypeParameters[_SgmType].Tmax, temp);
           sprintf((char*)NewValue, "%f", sensorTypeParameters[_SgmType].Tmax);  
           WriteChangeArcRecord(CHANGE_TEMP_PAR_MAX, source);
           strcpy((char*)ptr_out_buf, reply_command[COMMAND_OK]);
        }
        else
           strcpy((char*)ptr_out_buf, reply_command[COMMAND_INCORRECT_VALUE]);  
     }
     */
  }
  ptr_TempDataParsingCommand->flag_end = _OK;
}
//QT style, auto-documenting with Doxygen, https://habr.com/ru/post/252101/ 
/*!
Read and write MinTemp
\param[in] pointer string
\param[in] pointer output bufer
\param[in] pointer request
\param[in] source command
\param[in] temp data source command
\return void
*/
void PF_MinTemp(char* parString, uint8_t *ptr_out_buf, uint8_t *ptr_str_reply, TypeSourceCommands source, TTempDataParsingCommand *ptr_TempDataParsingCommand)
{
  if(parString ==0)   
  {
     strcpy((char*)ptr_out_buf, (const char*)ptr_str_reply);
     sprintf((char*)(ptr_out_buf + strlen((char*)ptr_out_buf)),"=%0.2f%s", sensorTypeParameters[_SgmType].Tmin, reply_OK);  
  }
  else
  {
    strcpy((char*)ptr_out_buf, reply_command[COMMAND_READ_ONLY]);
    /*
     if (CLB_lock != 0x66 && CLB_lock != 0x55 && StatusDevice.password_manufacture_enter == 0)
       strcpy((char*)ptr_out_buf, reply_command[COMMAND_ACCESS_DENIED]);   
     else
     {
        float temp;
         if(CheckChrString((uint8_t*)parString, "%d-.", strlen(parString)) == _FALSE) {strcpy((char*)ptr_out_buf, reply_command[COMMAND_INCORRECT_VALUE]); ptr_TempDataParsingCommand->flag_end = _OK; return;  }  

        temp = atof(parString);
        if(temp > -50 && temp < 10)
        {
           sprintf((char*)OldValue,"%f", sensorTypeParameters[_SgmType].Tmin);  
           Eeprom_WriteFloat((uint32_t)&sensorTypeParameters[_SgmType].Tmin, temp);
           sprintf((char*)NewValue, "%f", sensorTypeParameters[_SgmType].Tmin);  
           WriteChangeArcRecord(CHANGE_TEMP_PAR_MIN, source);
           strcpy((char*)ptr_out_buf, reply_command[COMMAND_OK]);
        }
        else
           strcpy((char*)ptr_out_buf, reply_command[COMMAND_INCORRECT_VALUE]);  
     }
    */
  }
  ptr_TempDataParsingCommand->flag_end = _OK;
} 
//QT style, auto-documenting with Doxygen, https://habr.com/ru/post/252101/ 
/*!
Read and close calibration key
\param[in] pointer string
\param[in] pointer output bufer
\param[in] pointer request
\param[in] source command
\param[in] temp data source command
\return void
*/
void PF_GetLockState(char* parString, uint8_t *ptr_out_buf, uint8_t *ptr_str_reply, TypeSourceCommands source, TTempDataParsingCommand *ptr_TempDataParsingCommand)
{
  const char* lo_Lock_Open = "Lock_OPEN";
  const char* lo_Lock_Close = "Lock_CLOSE";
  char* lo_Lock_State = (char*)lo_Lock_Close;
  
  if(parString == 0)   
  {
     if(CLB_lock == 0x66 || CLB_lock == 0x55)
        lo_Lock_State = (char*)lo_Lock_Open;

     strcpy((char*)ptr_out_buf, (const char*)ptr_str_reply);
     sprintf((char*)(ptr_out_buf + strlen((char*)ptr_out_buf)),"=%s%s", lo_Lock_State, reply_OK);  
  }
  else
  {
     sprintf((char*)OldValue,"%d", CLB_lock);   
     CLB_lock = 0;
     Eeprom_WriteChar((uint32_t)&CLB_lock_eeprom, CLB_lock);
     sprintf((char*)NewValue, "%d", CLB_lock); 
     WriteChangeArcRecord(CHANGE_CLB_LOCK, source); 
     strcpy((char*)ptr_out_buf, reply_command[COMMAND_OK]);
  }
  ptr_TempDataParsingCommand->flag_end = _OK;
}  
//QT style, auto-documenting with Doxygen, https://habr.com/ru/post/252101/ 
/*!
Start session to server
\param[in] pointer string
\param[in] pointer output bufer
\param[in] pointer request
\param[in] source command
\param[in] temp data source command
\return void
*/
void PF_MdmStart(char* parString, uint8_t *ptr_out_buf, uint8_t *ptr_str_reply, TypeSourceCommands source, TTempDataParsingCommand *ptr_TempDataParsingCommand)
{
   uint32_t lo_mdmInterval;
  
   if(parString ==0)   
     strcpy((char*)ptr_out_buf, reply_command[COMMAND_WRITE_ONLY]);
   else
   {
         if(CheckChrString((uint8_t*)parString, "%d", strlen(parString)) == _FALSE) {strcpy((char*)ptr_out_buf, reply_command[COMMAND_INCORRECT_VALUE]); ptr_TempDataParsingCommand->flag_end = _OK; return;  }  

     lo_mdmInterval = atol(parString);
     if(lo_mdmInterval < 3601)
     {
        timeUnixGSMNextConnect = timeSystemUnix + lo_mdmInterval;
        if(source == SOURCE_OPTIC)
           StatusDevice.GSM_Modem_Power_optic = _OK;
        strcpy((char*)ptr_out_buf, reply_command[COMMAND_OK]);        
     } 
     else
       strcpy((char*)ptr_out_buf, reply_command[COMMAND_INCORRECT_VALUE]);
   }     
   ptr_TempDataParsingCommand->flag_end = _OK;
}  
//QT style, auto-documenting with Doxygen, https://habr.com/ru/post/252101/ 
/*!
Start session to server
\param[in] pointer string
\param[in] pointer output bufer
\param[in] pointer request
\param[in] source command
\param[in] temp data source command
\return void
*/
void PF_GasDayBorter(char* parString, uint8_t *ptr_out_buf, uint8_t *ptr_str_reply, TypeSourceCommands source, TTempDataParsingCommand *ptr_TempDataParsingCommand)
{
  uint32_t lo_GasDayBorder;
   
  if(parString ==0)   
  {
     strcpy((char*)ptr_out_buf, (const char*)ptr_str_reply);
     sprintf((char*)(ptr_out_buf + strlen((char*)ptr_out_buf)),"=%d%s", GasDayBorder, reply_OK);  
  }
 else
 {
         if(CheckChrString((uint8_t*)parString, "%d", strlen(parString)) == _FALSE) {strcpy((char*)ptr_out_buf, reply_command[COMMAND_INCORRECT_VALUE]); ptr_TempDataParsingCommand->flag_end = _OK; return;  }  

   lo_GasDayBorder = atoi(parString);
   if (lo_GasDayBorder < 24)
   {
          sprintf((char*)OldValue, "%d", GasDayBorder);  
          Eeprom_WriteWord((uint32_t)&GasDayBorder,lo_GasDayBorder);
          sprintf((char*)NewValue, "%d", GasDayBorder);  
          WriteChangeArcRecord(CHANGE_GAS_DAY, source);    
           strcpy((char*)ptr_out_buf, reply_command[COMMAND_OK]);
    }
    else
      strcpy((char*)ptr_out_buf, reply_command[COMMAND_INCORRECT_VALUE]);  
  }
  ptr_TempDataParsingCommand->flag_end = _OK;
}  
//QT style, auto-documenting with Doxygen, https://habr.com/ru/post/252101/ 
/*!
Read and write mode session TCP
\param[in] pointer string
\param[in] pointer output bufer
\param[in] pointer request
\param[in] source command
\param[in] temp data source command
\return void
*/
void PF_ModemModeTransfer(char* parString, uint8_t *ptr_out_buf, uint8_t *ptr_str_reply, TypeSourceCommands source, TTempDataParsingCommand *ptr_TempDataParsingCommand)
{
  uint32_t lo_TransferMode, lo_TransferHour,lo_TransferMinutes, lo_TransferDay;
  uint8_t rez = _OK;
  if(parString ==0)   
  {
     strcpy((char*)ptr_out_buf, (const char*)ptr_str_reply);
     sprintf((char*)(ptr_out_buf + strlen((char*)ptr_out_buf)),"=%d,%.2d:%.2d,%d%s", Mode_Transfer.mode, Mode_Transfer.hour, Mode_Transfer.minute, Mode_Transfer.day, reply_OK);
  }
  else
  {
    if(CheckChrString((uint8_t*)parString, "%d,:", strlen(parString)) == _FALSE) {strcpy((char*)ptr_out_buf, reply_command[COMMAND_INCORRECT_VALUE]); ptr_TempDataParsingCommand->flag_end = _OK; return;  }  

    lo_TransferMode = atoi(parString);
    if ((lo_TransferMode < 1) || (lo_TransferMode > 5) || lo_TransferMode == 3)
       rez = _FALSE; 
    parString = strchr(parString,',');
    if(parString && rez == _OK)
    {
      lo_TransferHour = atoi(parString+1);
      if(lo_TransferHour > 23)
        rez = _FALSE;
    }
    else
      rez = _FALSE;
    parString = strchr(parString,':');
    if(parString && rez == _OK)
    {
      lo_TransferMinutes = atoi(parString+1);
      if(lo_TransferMinutes > 59)
        rez = _FALSE;
    } 
    else
      rez = _FALSE;
    parString = strchr(parString,',');
    if(parString && rez == _OK)
    {
      lo_TransferDay = atoi(parString+1);
      if(lo_TransferDay > 28 || (lo_TransferMode == 5 && lo_TransferDay > 8) || lo_TransferDay == 0)
        rez = _FALSE;
    } 
    else
      rez = _FALSE;
    if(rez == _OK)
    {
       cnt_seans_TCP_mode_2 = 0;
       uint32_t reserved_int_temp = 3601;
       sprintf((char*)OldValue, "%d,%.2d:%.2d,%d", Mode_Transfer.mode, Mode_Transfer.hour, Mode_Transfer.minute, Mode_Transfer.day);  
       Eeprom_WriteChar((uint32_t)&Mode_Transfer.mode,lo_TransferMode);
       Eeprom_WriteChar((uint32_t)&Mode_Transfer.hour,lo_TransferHour);
       Eeprom_WriteChar((uint32_t)&Mode_Transfer.minute,lo_TransferMinutes);
       Eeprom_WriteChar((uint32_t)&Mode_Transfer.day,lo_TransferDay);
       timeGSMUnixStart = timeSystemUnix;
       timeUnixGSMNextConnect = timeSystemUnix + 3600; // do not interfere with shutdown
       if(source != SOURCE_TCP)
       {
          ParamServer.success_session[0] = _OK;
          ParamServer.success_session[1] = _OK;
          ParamServer.success_session[2] = _OK;         
      //    ParamServer.success_session_old[0] = ParamServer.success_session[0];
      //    ParamServer.success_session_old[1] = ParamServer.success_session[1];
      //    ParamServer.success_session_old[2] = ParamServer.success_session[2];
       }
       sprintf((char*)NewValue, "%d,%.2d:%.2d,%d", Mode_Transfer.mode, Mode_Transfer.hour, Mode_Transfer.minute, Mode_Transfer.day);  
       WriteChangeArcRecord(CHANGE_TRANSFER_MODE, source); 
       //----------------------------------------------------
       sprintf((char*)OldValue,"%d", reserved_int); 
       if(Mode_Transfer.mode == 2)
          reserved_int_temp = 3600;
       if(Mode_Transfer.mode == 5 || Mode_Transfer.mode == 4)
          reserved_int_temp = 43200;
       if(reserved_int_temp != 3601) 
       {
          Eeprom_WriteDword((uint32_t)&reserved_int, reserved_int_temp);
          sprintf((char*)NewValue,"%d", reserved_int); 
          WriteChangeArcRecord(CHANGE_RESERVED_INT, CHANGE_PROGRAM); 
       }
       strcpy((char*)ptr_out_buf, reply_command[COMMAND_OK]);
     }
     else
       strcpy((char*)ptr_out_buf, reply_command[COMMAND_INCORRECT_VALUE]);  
  }
  ptr_TempDataParsingCommand->flag_end = _OK;
}  
//QT style, auto-documenting with Doxygen, https://habr.com/ru/post/252101/ 
/*!
Read and write numbber balans
\param[in] pointer string
\param[in] pointer output bufer
\param[in] pointer request
\param[in] source command
\param[in] temp data source command
\return void
*/
void PF_ModemBalansePhohe(char* parString, uint8_t *ptr_out_buf, uint8_t *ptr_str_reply, TypeSourceCommands source, TTempDataParsingCommand *ptr_TempDataParsingCommand)
{
 uint16_t lo_charCnt;
 
  if(parString ==0)   
  {
     strcpy((char*)ptr_out_buf, (const char*)ptr_str_reply);
     sprintf((char*)(ptr_out_buf + strlen((char*)ptr_out_buf)),"=%s%s", MdmNumberBalans, reply_OK);  
  }
  else
  {
    if(CheckChrString((uint8_t*)parString, "%d*#+", strlen(parString)) == _FALSE) {strcpy((char*)ptr_out_buf, reply_command[COMMAND_INCORRECT_VALUE]); ptr_TempDataParsingCommand->flag_end = _OK; return;  }  

     lo_charCnt = (strlen(parString));
     if(lo_charCnt > sizeof(MdmNumberBalans) - 1)
       strcpy((char*)ptr_out_buf, reply_command[COMMAND_INCORRECT_VALUE]);  
     else
     {
         if(strlen(MdmNumberBalans) < 60)
           sprintf((char*)OldValue, "%s", MdmNumberBalans);  
         Eeprom_WriteString((uint8_t*)MdmNumberBalans,(uint8_t*)parString, lo_charCnt, _OK); 
         if(strlen(MdmNumberBalans) < 60)
            sprintf((char*)NewValue, "%s", MdmNumberBalans); 
         WriteChangeArcRecord(CHANGE_BALANCE_NUMBER, source);     
        strcpy((char*)ptr_out_buf, reply_command[COMMAND_OK]);
     }
  }
  ptr_TempDataParsingCommand->flag_end = _OK;
}
//QT style, auto-documenting with Doxygen, https://habr.com/ru/post/252101/ 
/*!
Read and write RESERVED INTERVAL
\param[in] pointer string
\param[in] pointer output bufer
\param[in] pointer request
\param[in] source command
\param[in] temp data source command
\return void
*/
void PF_ModemReservedInterval(char* parString, uint8_t *ptr_out_buf, uint8_t *ptr_str_reply, TypeSourceCommands source, TTempDataParsingCommand *ptr_TempDataParsingCommand)
{
 uint32_t lo_mdmInterval;
  
   if(parString ==0)
   {
     strcpy((char*)ptr_out_buf, (const char*)ptr_str_reply);
     sprintf((char*)(ptr_out_buf + strlen((char*)ptr_out_buf)),"=%d%s", reserved_int, reply_OK);  
   }
   else
   {
         if(CheckChrString((uint8_t*)parString, "%d", strlen(parString)) == _FALSE) {strcpy((char*)ptr_out_buf, reply_command[COMMAND_INCORRECT_VALUE]); ptr_TempDataParsingCommand->flag_end = _OK; return;  }  

     lo_mdmInterval = atol(parString);
     if(lo_mdmInterval && ((lo_mdmInterval < 172801 && Mode_Transfer.mode != 2) || (Mode_Transfer.mode == 2 && lo_mdmInterval < 10801)))
     {
       sprintf((char*)OldValue,"%d", reserved_int);  
       Eeprom_WriteDword((uint32_t)&reserved_int, lo_mdmInterval);
       sprintf((char*)NewValue,"%d", reserved_int); 
       WriteChangeArcRecord(CHANGE_RESERVED_INT, source); 
       strcpy((char*)ptr_out_buf, reply_command[COMMAND_OK]);
     }
     else
       strcpy((char*)ptr_out_buf, reply_command[COMMAND_INCORRECT_VALUE]);  
  }
  ptr_TempDataParsingCommand->flag_end = _OK;
}  
//QT style, auto-documenting with Doxygen, https://habr.com/ru/post/252101/ 
/*!
transmit log for modem
\param[in] pointer string
\param[in] pointer output bufer
\param[in] pointer request
\param[in] source command
\param[in] temp data source command
\return void
*/
void PF_MdmToOpto(char* parString, uint8_t *ptr_out_buf, uint8_t *ptr_str_reply, TypeSourceCommands source, TTempDataParsingCommand *ptr_TempDataParsingCommand)
{
  if(parString ==0)   
  {
     strcpy((char*)ptr_out_buf, (const char*)ptr_str_reply);
     sprintf((char*)(ptr_out_buf + strlen((char*)ptr_out_buf)),"=%d%s", modem_log_eeprom, reply_OK);  //NoLogToOptik
  }
  else
  {
    uint32_t temp;
         if(CheckChrString((uint8_t*)parString, "%d", strlen(parString)) == _FALSE) {strcpy((char*)ptr_out_buf, reply_command[COMMAND_INCORRECT_VALUE]); ptr_TempDataParsingCommand->flag_end = _OK; return;  }  

    temp = atoi(parString);
    if(temp == 0 || temp == 1)
    {
         sprintf((char*)OldValue, "%d", modem_log_eeprom);  
         Eeprom_WriteChar((uint32_t)&modem_log_eeprom, temp);
         sprintf((char*)NewValue, "%d", modem_log_eeprom);  
         WriteChangeArcRecord(CHANGE_MODEM_LOG, source);
         strcpy((char*)ptr_out_buf, reply_command[COMMAND_OK]);
    }
    else
      strcpy((char*)ptr_out_buf, reply_command[COMMAND_INCORRECT_VALUE]);  
  }
  ptr_TempDataParsingCommand->flag_end = _OK;
}

//QT style, auto-documenting with Doxygen, https://habr.com/ru/post/252101/ 
/*!
clear arhive
\param[in] pointer string
\param[in] pointer output bufer
\param[in] pointer request
\param[in] source command
\param[in] temp data source command
\return void
*/
void PF_ClearArc(char* parString, uint8_t *ptr_out_buf, uint8_t *ptr_str_reply, TypeSourceCommands source, TTempDataParsingCommand *ptr_TempDataParsingCommand)
{
   if(parString == 0)
   {
     strcpy((char*)ptr_out_buf, reply_command[COMMAND_WRITE_ONLY]);   
   }
   else
   {
   if (CLB_lock != 0x66 && CLB_lock != 0x55 && StatusDevice.password_manufacture_enter == 0)
       strcpy((char*)ptr_out_buf, reply_command[COMMAND_ACCESS_DENIED]);   
   else
   {
     if(CheckChrString((uint8_t*)parString, "%d,", strlen(parString)) == _FALSE) {strcpy((char*)ptr_out_buf, reply_command[COMMAND_INCORRECT_VALUE]); ptr_TempDataParsingCommand->flag_end = _OK; return;  }  
     
     sprintf((char*)OldValue,"%d,%d,%d,%d",
             IntArcRecNo - 1, DayArcRecNo - 1, SystemArcRecNo - 1, TelemetryArcRecNo - 1);
     Eeprom_WriteDword((uint32_t)&IntArcRecNo, 1);
     Eeprom_WriteDword((uint32_t)&DayArcRecNo, 1);
     Eeprom_WriteDword((uint32_t)&SystemArcRecNo, 1);
  //   Eeprom_WriteDword((uint32_t)&ChangeArcRecNo, 1);
     Eeprom_WriteDword((uint32_t)&TelemetryArcRecNo, 1);

     sprintf((char*)NewValue,"%d,%d,%d,%d",
             IntArcRecNo - 1, DayArcRecNo - 1, SystemArcRecNo - 1, TelemetryArcRecNo - 1);
     WriteChangeArcRecord(CHANGE_CLEAR_ARHIVE, source);
     strcpy((char*)ptr_out_buf, reply_command[COMMAND_OK]);
   }
   }
   ptr_TempDataParsingCommand->flag_end = _OK;
}
//QT style, auto-documenting with Doxygen, https://habr.com/ru/post/252101/ 
/*!
Clear device counter
\param[in] pointer string
\param[in] pointer output bufer
\param[in] pointer request
\param[in] source command
\param[in] temp data source command
\return void
*/
void PF_ClearCount(char* parString, uint8_t *ptr_out_buf, uint8_t *ptr_str_reply, TypeSourceCommands source, TTempDataParsingCommand *ptr_TempDataParsingCommand)
{
   if(parString == 0)
   {
     strcpy((char*)ptr_out_buf, reply_command[COMMAND_WRITE_ONLY]);   
   }
   else
   {
  if(CLB_lock == 0x66 || CLB_lock == 0x55)
  {
     sprintf((char*)OldValue,"%0.6f", GasMeter.VE_Lm3 );
 //    GasMeter.VE_S =0.0;
 //    GasMeter.VE_Sm3 = 0.0;
     GasMeter.VE_Lm3 = 0.0;
     System_SaweBeforeReset();
     sprintf((char*)NewValue, "%0.6f", GasMeter.VE_Lm3 );
     WriteChangeArcRecord(CHANGE_CLEAR_COUNTER, source);
     strcpy((char*)ptr_out_buf, reply_command[COMMAND_OK]);
  }
  else
     strcpy((char*)ptr_out_buf, reply_command[COMMAND_ACCESS_DENIED]);
   }
  ptr_TempDataParsingCommand->flag_end = _OK;
}
//QT style, auto-documenting with Doxygen, https://habr.com/ru/post/252101/ 
/*!
Clear counter modem
\param[in] pointer string
\param[in] pointer output bufer
\param[in] pointer request
\param[in] source command
\param[in] temp data source command
\return void
*/
void PF_ClearMdmCnt(char* parString, uint8_t *ptr_out_buf, uint8_t *ptr_str_reply, TypeSourceCommands source, TTempDataParsingCommand *ptr_TempDataParsingCommand)
{
   if(parString != 0)
   {
      if(CLB_lock != 0x66 && CLB_lock != 0x55  && StatusDevice.password_manufacture_enter == 0)
       strcpy((char*)ptr_out_buf, reply_command[COMMAND_ACCESS_DENIED]);
     else
     {
       ClearCntSession(source, 0, 0, 0, 0, 0, 0);
       strcpy((char*)ptr_out_buf, reply_command[COMMAND_OK]);
     }
   }
   else
     strcpy((char*)ptr_out_buf, reply_command[COMMAND_WRITE_ONLY]);
   ptr_TempDataParsingCommand->flag_end = _OK;
}
//QT style, auto-documenting with Doxygen, https://habr.com/ru/post/252101/ 
/*!
Read and write sms phone 2
\param[in] pointer string
\param[in] pointer output bufer
\param[in] pointer request
\param[in] source command
\param[in] temp data source command
\return void
*/
void PF_sms_phone2(char* parString, uint8_t *ptr_out_buf, uint8_t *ptr_str_reply, TypeSourceCommands source, TTempDataParsingCommand *ptr_TempDataParsingCommand)
{
   uint8_t cnt_char;
   if(parString ==0)
   {
      strcpy((char*)ptr_out_buf, (const char*)ptr_str_reply);
      sprintf((char*)(ptr_out_buf + strlen((char*)ptr_out_buf)),"=%s%s", telephone_number_SMS2, reply_OK);  
   }
   else
   {
         if(CheckChrString((uint8_t*)parString, "%d+", strlen(parString)) == _FALSE) {strcpy((char*)ptr_out_buf, reply_command[COMMAND_INCORRECT_VALUE]); ptr_TempDataParsingCommand->flag_end = _OK; return;  }  

     cnt_char = strlen(parString); 
     if(cnt_char < 15)
     {
        sprintf((char*)OldValue,"%s", telephone_number_SMS2);          
        if(*parString == '0')
           Eeprom_WriteChar((uint32_t)&telephone_number_SMS2[0], 0);
        else
           Eeprom_WriteString((uint8_t*)telephone_number_SMS2,(uint8_t*)parString, cnt_char, _OK);
        sprintf((char*)NewValue,"%s",telephone_number_SMS2); 
        WriteChangeArcRecord(CHANGE_SMS_NUMBBER2, source); 
        strcpy((char*)ptr_out_buf, reply_command[COMMAND_OK]);
      }
      else
        strcpy((char*)ptr_out_buf, reply_command[COMMAND_INCORRECT_VALUE]);  
   }
   ptr_TempDataParsingCommand->flag_end = _OK;
}
//QT style, auto-documenting with Doxygen, https://habr.com/ru/post/252101/ 
/*!
Read and write sms phone 1
\param[in] pointer string
\param[in] pointer output bufer
\param[in] pointer request
\param[in] source command
\param[in] temp data source command
\return void
*/
void PF_sms_phone1(char* parString, uint8_t *ptr_out_buf, uint8_t *ptr_str_reply, TypeSourceCommands source, TTempDataParsingCommand *ptr_TempDataParsingCommand)
{
   uint8_t cnt_char;
   if(parString ==0)
   {
      strcpy((char*)ptr_out_buf, (const char*)ptr_str_reply);
      sprintf((char*)(ptr_out_buf + strlen((char*)ptr_out_buf)),"=%s%s", telephone_number_SMS1, reply_OK);  
   }
   else
   {
         if(CheckChrString((uint8_t*)parString, "%d+", strlen(parString)) == _FALSE) {strcpy((char*)ptr_out_buf, reply_command[COMMAND_INCORRECT_VALUE]); ptr_TempDataParsingCommand->flag_end = _OK; return;  }  

     cnt_char = strlen(parString); 
     if(cnt_char < 15)
     {
        sprintf((char*)OldValue,"%s", telephone_number_SMS1);          
        if(*parString == '0')
           Eeprom_WriteChar((uint32_t)&telephone_number_SMS1[0], 0);
        else
           Eeprom_WriteString((uint8_t*)telephone_number_SMS1,(uint8_t*)parString, cnt_char, _OK);
        sprintf((char*)NewValue,"%s",telephone_number_SMS1); 
        WriteChangeArcRecord(CHANGE_SMS_NUMBBER, source); 
        strcpy((char*)ptr_out_buf, reply_command[COMMAND_OK]);
      }
      else
        strcpy((char*)ptr_out_buf, reply_command[COMMAND_INCORRECT_VALUE]);  
   }
   ptr_TempDataParsingCommand->flag_end = _OK;
}
//QT style, auto-documenting with Doxygen, https://habr.com/ru/post/252101/ 
/*!
Read and write type device
\param[in] pointer string
\param[in] pointer output bufer
\param[in] pointer request
\param[in] source command
\param[in] temp data source command
\return void
*/
void PF_type_device(char* parString, uint8_t *ptr_out_buf, uint8_t *ptr_str_reply, TypeSourceCommands source, TTempDataParsingCommand *ptr_TempDataParsingCommand)
{
   if(parString ==0)   
   {
      strcpy((char*)ptr_out_buf, (const char*)ptr_str_reply);
      sprintf((char*)(ptr_out_buf + strlen((char*)ptr_out_buf)),"=%d%s", type_device_int, reply_OK);  
   }
   else
   { 
     if(CLB_lock != 0x66 && CLB_lock != 0x55  && StatusDevice.password_manufacture_enter == 0)
       strcpy((char*)ptr_out_buf, reply_command[COMMAND_ACCESS_DENIED]);
     else
     {
       uint32_t temp;
         if(CheckChrString((uint8_t*)parString, "%d", strlen(parString)) == _FALSE) {strcpy((char*)ptr_out_buf, reply_command[COMMAND_INCORRECT_VALUE]); ptr_TempDataParsingCommand->flag_end = _OK; return;  }  

       temp = atol(parString);
       if(temp == 4 
          || temp == 6  
          || temp == 10
          || temp == 16
          || temp == 25 )
       {
          sprintf((char*)OldValue,"%d", type_device);  
          switch(temp)
          {
          case 4:
               Eeprom_WriteChar((uint32_t)&type_device, SGM_TYPE_SGM4);
               break;
          case 6:
               Eeprom_WriteChar((uint32_t)&type_device, SGM_TYPE_SGM6);
               break;      
          case 10:
               Eeprom_WriteChar((uint32_t)&type_device, SGM_TYPE_SGM10);
               break;          
          case 16:
               Eeprom_WriteChar((uint32_t)&type_device, SGM_TYPE_SGM16);
               break;
          case 25:
               Eeprom_WriteChar((uint32_t)&type_device, SGM_TYPE_SGM25);
               break;
               
          default:break;
          }
          Eeprom_WriteChar((uint32_t)&sensEnable_eeprom, 0);
          memset((char*)&Sensor_Const, 0, sizeof(Sensor_Const));
          memset((char*)&GasMeter.VDD_power, 0, sizeof(GasMeter.VDD_power) + sizeof(GasMeter.SensEnable) + sizeof(GasMeter.SensError) + sizeof(GasMeter.SensCnt));
          cnt_error_SGM = 5;
          sprintf((char*)NewValue,"%d", type_device);  
          WriteChangeArcRecord(CHANGE_TYPE_DEVICE, source); 
          strcpy((char*)ptr_out_buf, reply_command[COMMAND_OK]);
      }
      else
        strcpy((char*)ptr_out_buf, reply_command[COMMAND_INCORRECT_VALUE]);  
     }
   }
   ptr_TempDataParsingCommand->flag_end = _OK;
}
//QT style, auto-documenting with Doxygen, https://habr.com/ru/post/252101/ 
/*!
Read and write upper limit of the start of measurement QposLimit
\param[in] pointer string
\param[in] pointer output bufer
\param[in] pointer request
\param[in] source command
\param[in] temp data source command
\return void
*/
void PF_QposLimit(char* parString, uint8_t *ptr_out_buf, uint8_t *ptr_str_reply, TypeSourceCommands source, TTempDataParsingCommand *ptr_TempDataParsingCommand)
{
  int lo_QposLimit;

  if(parString ==0)   
  {
      strcpy((char*)ptr_out_buf, (const char*)ptr_str_reply);
      sprintf((char*)(ptr_out_buf + strlen((char*)ptr_out_buf)),"=%d%s", QposLimit, reply_OK);  
  }
  else
  {
     if (CLB_lock != 0x66 && CLB_lock != 0x55 && StatusDevice.password_manufacture_enter == 0)
       strcpy((char*)ptr_out_buf, reply_command[COMMAND_ACCESS_DENIED]);   
     else
     {
         if(CheckChrString((uint8_t*)parString, "%d-", strlen(parString)) == _FALSE) {strcpy((char*)ptr_out_buf, reply_command[COMMAND_INCORRECT_VALUE]); ptr_TempDataParsingCommand->flag_end = _OK; return;  }  

        lo_QposLimit = atoi(parString);
        if (lo_QposLimit > -2000 && lo_QposLimit < 2000)
        {
           sprintf((char*)OldValue,"%d", QposLimit);
           Eeprom_WriteSWord ((uint32_t)&QposLimit,lo_QposLimit);
           sprintf((char*)NewValue,"%d", QposLimit); 
           WriteChangeArcRecord(CHANGE_QPOS_LIMIT, source);
           strcpy((char*)ptr_out_buf, reply_command[COMMAND_OK]);
        }
        else
           strcpy((char*)ptr_out_buf, reply_command[COMMAND_INCORRECT_VALUE]);  
     }
   }
   ptr_TempDataParsingCommand->flag_end = _OK;
}
//QT style, auto-documenting with Doxygen, https://habr.com/ru/post/252101/ 
/*!
Read and write lower limit of the start of measurement QposLimit
\param[in] pointer string
\param[in] pointer output bufer
\param[in] pointer request
\param[in] source command
\param[in] temp data source command
\return void
*/
void PF_QnegLimit(char* parString, uint8_t *ptr_out_buf, uint8_t *ptr_str_reply, TypeSourceCommands source, TTempDataParsingCommand *ptr_TempDataParsingCommand)
{
  int lo_QnegLimit;

  if(parString ==0)   
  {
      strcpy((char*)ptr_out_buf, (const char*)ptr_str_reply);
      sprintf((char*)(ptr_out_buf + strlen((char*)ptr_out_buf)),"=%d%s", QnegLimit, reply_OK);  
  }
  else
  {
     if (CLB_lock != 0x66 && CLB_lock != 0x55 && StatusDevice.password_manufacture_enter == 0)
       strcpy((char*)ptr_out_buf, reply_command[COMMAND_ACCESS_DENIED]);   
     else
     {
         if(CheckChrString((uint8_t*)parString, "%d-", strlen(parString)) == _FALSE) {strcpy((char*)ptr_out_buf, reply_command[COMMAND_INCORRECT_VALUE]); ptr_TempDataParsingCommand->flag_end = _OK; return;  }  

        lo_QnegLimit = atoi(parString);
        if(lo_QnegLimit > -20000 && lo_QnegLimit < 2000)
        {
           sprintf((char*)OldValue,"%d", QnegLimit);
           Eeprom_WriteSWord ((uint32_t)&QnegLimit,lo_QnegLimit);
           sprintf((char*)NewValue,"%d", QnegLimit); 
           WriteChangeArcRecord(CHANGE_QNEG_LIMIT, source);
           strcpy((char*)ptr_out_buf, reply_command[COMMAND_OK]);
        }
        else
           strcpy((char*)ptr_out_buf, reply_command[COMMAND_INCORRECT_VALUE]);  
     }
   }
   ptr_TempDataParsingCommand->flag_end = _OK;
} 
//QT style, auto-documenting with Doxygen, https://habr.com/ru/post/252101/ 
/*!
Read and write hardware version
\param[in] pointer string
\param[in] pointer output bufer
\param[in] pointer request
\param[in] source command
\param[in] temp data source command
\return void
*/
void PF_hw_version(char* parString, uint8_t *ptr_out_buf, uint8_t *ptr_str_reply, TypeSourceCommands source, TTempDataParsingCommand *ptr_TempDataParsingCommand)
{
  if(parString ==0)   
  {
      strcpy((char*)ptr_out_buf, (const char*)ptr_str_reply);
      sprintf((char*)(ptr_out_buf + strlen((char*)ptr_out_buf)),"=%d%s", hw_version, reply_OK);  
  }
  else
  { 
    if (CLB_lock != 0x66 && CLB_lock != 0x55 && StatusDevice.password_manufacture_enter == 0)
       strcpy((char*)ptr_out_buf, reply_command[COMMAND_ACCESS_DENIED]);   
    else
    {
      uint32_t temp;
         if(CheckChrString((uint8_t*)parString, "%d", strlen(parString)) == _FALSE) {strcpy((char*)ptr_out_buf, reply_command[COMMAND_INCORRECT_VALUE]); ptr_TempDataParsingCommand->flag_end = _OK; return;  }  

      temp = atoi(parString);
      if(temp > 0 && temp < 10000)
      {
         sprintf((char*)OldValue,"%d", hw_version);
         Eeprom_WriteWord((uint32_t)&hw_version, temp);
         sprintf((char*)NewValue,"%d", hw_version); 
         WriteChangeArcRecord(CHANGE_HW_VERSION, source);
         strcpy((char*)ptr_out_buf, reply_command[COMMAND_OK]);
      }
      else
        strcpy((char*)ptr_out_buf, reply_command[COMMAND_INCORRECT_VALUE]); 
    }
   }
   ptr_TempDataParsingCommand->flag_end = _OK;
}
//QT style, auto-documenting with Doxygen, https://habr.com/ru/post/252101/ 
/*!
Read and write pressure absolute
\param[in] pointer string
\param[in] pointer output bufer
\param[in] pointer request
\param[in] source command
\param[in] temp data source command
\return void
*/
void PF_pressure_abs(char* parString, uint8_t *ptr_out_buf, uint8_t *ptr_str_reply, TypeSourceCommands source, TTempDataParsingCommand *ptr_TempDataParsingCommand)
{
  if(parString == 0)   
  {
      strcpy((char*)ptr_out_buf, (const char*)ptr_str_reply);
      sprintf((char*)(ptr_out_buf + strlen((char*)ptr_out_buf)),"=%.3f%s",  pressure_abs, reply_OK);  
  }
  else
  { 
     double pressure_abs_buf;
         if(CheckChrString((uint8_t*)parString, "%d.", strlen(parString)) == _FALSE) {strcpy((char*)ptr_out_buf, reply_command[COMMAND_INCORRECT_VALUE]); ptr_TempDataParsingCommand->flag_end = _OK; return;  }  

     sscanf((char*)parString,"%lf", &pressure_abs_buf);
     if(pressure_abs_buf >= 96.325 && pressure_abs_buf <= 106.325)
     {
        sprintf((char*)OldValue,"%f", pressure_abs);
        Eeprom_WriteDouble((uint32_t)&pressure_abs, pressure_abs_buf);
        sprintf((char*)NewValue,"%f", pressure_abs);
        WriteChangeArcRecord(CHANGE_PRESSURE_ABS, source); 
        strcpy((char*)ptr_out_buf, reply_command[COMMAND_OK]);
     }
     else
        strcpy((char*)ptr_out_buf, reply_command[COMMAND_INCORRECT_VALUE]);  
   }
   ptr_TempDataParsingCommand->flag_end = _OK;
}
//QT style, auto-documenting with Doxygen, https://habr.com/ru/post/252101/ 
/*!
On all segment LCD
\param[in] pointer string
\param[in] pointer output bufer
\param[in] pointer request
\param[in] source command
\param[in] temp data source command
\return void
*/
void PF_LCD_MENU(char* parString, uint8_t *ptr_out_buf, uint8_t *ptr_str_reply, TypeSourceCommands source, TTempDataParsingCommand *ptr_TempDataParsingCommand)
{
  if(parString == 0)   
  {
      strcpy((char*)ptr_out_buf, (const char*)ptr_str_reply);
      sprintf((char*)(ptr_out_buf + strlen((char*)ptr_out_buf)),"=%d%s",  Global_Menu, reply_OK);  
  }
  else
  { 
     uint32_t temp;
     if(CheckChrString((uint8_t*)parString, "%d", strlen(parString)) == _FALSE) {strcpy((char*)ptr_out_buf, reply_command[COMMAND_INCORRECT_VALUE]); ptr_TempDataParsingCommand->flag_end = _OK; return;  }  

     temp = atoi(parString);
     if(temp > TEH_TEST_LCD)
       strcpy((char*)ptr_out_buf, reply_command[COMMAND_INCORRECT_VALUE]);   
     else
     {
        if(temp == MAIN_MENU_TEMPER_ENVIRONS)   
          temp = MAIN_MENU_TEMPER_GAZ;
        Global_Menu = temp;
        strcpy((char*)ptr_out_buf, reply_command[COMMAND_OK]);
        PressKeyTime = Time_GetSystemUpTimeSecond();

        tick_time_lcd_display_on_sec = SystemUpTimeSecond;
     }
  }
   ptr_TempDataParsingCommand->flag_end = _OK;
}
//QT style, auto-documenting with Doxygen, https://habr.com/ru/post/252101/ 
/*!
Read serial number SGM module
\param[in] pointer string
\param[in] pointer output bufer
\param[in] pointer request
\param[in] source command
\param[in] temp data source command
\return void
*/
void PF_SN_SGM_E(char* parString, uint8_t *ptr_out_buf, uint8_t *ptr_str_reply, TypeSourceCommands source, TTempDataParsingCommand *ptr_TempDataParsingCommand)
{
   if(parString == 0)
   {
      sprintf((char*)TransmitBuffer,"SGM_SN[0]=%s\r\nSGM_SN[1]=\r\nSGM_SN[2]=\r\nSGM_SN[3]=%s", EEPROM_SN_SGM[0], reply_OK);  
   }
   else
     strcpy((char*)ptr_out_buf, reply_command[COMMAND_READ_ONLY]); 
  ptr_TempDataParsingCommand->flag_end = _OK;   
}
//QT style, auto-documenting with Doxygen, https://habr.com/ru/post/252101/ 
/*!
Read and write QGL_MAX for LCD
\param[in] pointer string
\param[in] pointer output bufer
\param[in] pointer request
\param[in] source command
\param[in] temp data source command
\return void
*/
void PF_QGL_MAX(char* parString, uint8_t *ptr_out_buf, uint8_t *ptr_str_reply, TypeSourceCommands source, TTempDataParsingCommand *ptr_TempDataParsingCommand)
{
  if(parString ==0)   
  {
      strcpy((char*)ptr_out_buf, (const char*)ptr_str_reply);
      sprintf((char*)(ptr_out_buf + strlen((char*)ptr_out_buf)),"=%f%s", percent_QgL_Disp_max, reply_OK);  
  }
  else
  {
    float temp;
     if(CheckChrString((uint8_t*)parString, "%d.", strlen(parString)) == _FALSE) {strcpy((char*)ptr_out_buf, reply_command[COMMAND_INCORRECT_VALUE]); ptr_TempDataParsingCommand->flag_end = _OK; return;  }  

    temp = atof(parString);
    if (CLB_lock != 0x66 && CLB_lock != 0x55 && StatusDevice.password_manufacture_enter == 0)
       strcpy((char*)ptr_out_buf, reply_command[COMMAND_ACCESS_DENIED]);   
    else
    {
       if(temp >= 0.01 && temp <= 0.3)
       {
          sprintf((char*)OldValue,"%f", percent_QgL_Disp_max);
          Eeprom_WriteFloat((uint32_t)&percent_QgL_Disp_max, temp);
          sprintf((char*)NewValue,"%f", percent_QgL_Disp_max);
          WriteChangeArcRecord(CHANGE_QGL_MAX, source); 
          strcpy((char*)ptr_out_buf, reply_command[COMMAND_OK]);
       }
       else
          strcpy((char*)ptr_out_buf, reply_command[COMMAND_INCORRECT_VALUE]);  
    }
   }
   ptr_TempDataParsingCommand->flag_end = _OK;
}
//QT style, auto-documenting with Doxygen, https://habr.com/ru/post/252101/ 
/*!
Read and write QGL_MIN for LCD
\param[in] pointer string
\param[in] pointer output bufer
\param[in] pointer request
\param[in] source command
\param[in] temp data source command
\return void
*/
void PF_QGL_MIN(char* parString, uint8_t *ptr_out_buf, uint8_t *ptr_str_reply, TypeSourceCommands source, TTempDataParsingCommand *ptr_TempDataParsingCommand)
{
  if(parString ==0)   
  {
      strcpy((char*)ptr_out_buf, (const char*)ptr_str_reply);
      sprintf((char*)(ptr_out_buf + strlen((char*)ptr_out_buf)),"=%f%s", percent_QgL_Disp_min, reply_OK);  
  }
  else
  {
    float temp;
     if(CheckChrString((uint8_t*)parString, "%d.", strlen(parString)) == _FALSE) {strcpy((char*)ptr_out_buf, reply_command[COMMAND_INCORRECT_VALUE]); ptr_TempDataParsingCommand->flag_end = _OK; return;  }  

    temp = atof(parString);
    if (CLB_lock != 0x66 && CLB_lock != 0x55 && StatusDevice.password_manufacture_enter == 0)
       strcpy((char*)ptr_out_buf, reply_command[COMMAND_ACCESS_DENIED]);   
    else
    {
       if(temp >= 0.01 && temp <= 0.3)
       {
          sprintf((char*)OldValue,"%f", percent_QgL_Disp_min);
          Eeprom_WriteFloat((uint32_t)&percent_QgL_Disp_min, temp);
          sprintf((char*)NewValue,"%f", percent_QgL_Disp_min);
          WriteChangeArcRecord(CHANGE_QGL_MIN, source); 
          strcpy((char*)ptr_out_buf, reply_command[COMMAND_OK]);
       }
       else
          strcpy((char*)ptr_out_buf, reply_command[COMMAND_INCORRECT_VALUE]);  
    }
   }
   ptr_TempDataParsingCommand->flag_end = _OK;
}
//QT style, auto-documenting with Doxygen, https://habr.com/ru/post/252101/ 
/*!
Read and write QFL_LIMIT for LCD
\param[in] pointer string
\param[in] pointer output bufer
\param[in] pointer request
\param[in] source command
\param[in] temp data source command
\return void
*/
void PF_QFL_LIMIT(char* parString, uint8_t *ptr_out_buf, uint8_t *ptr_str_reply, TypeSourceCommands source, TTempDataParsingCommand *ptr_TempDataParsingCommand)
{
  if(parString ==0)   
  {
      strcpy((char*)ptr_out_buf, (const char*)ptr_str_reply);
      sprintf((char*)(ptr_out_buf + strlen((char*)ptr_out_buf)),"=%d%s", Qfl_limit_QgL_Disp, reply_OK);  
  }
  else
  {
    
    uint32_t temp;
     if(CheckChrString((uint8_t*)parString, "%d", strlen(parString)) == _FALSE) {strcpy((char*)ptr_out_buf, reply_command[COMMAND_INCORRECT_VALUE]); ptr_TempDataParsingCommand->flag_end = _OK; return;  }  

    temp = atoi(parString);
    if (CLB_lock != 0x66 && CLB_lock != 0x55 && StatusDevice.password_manufacture_enter == 0)
       strcpy((char*)ptr_out_buf, reply_command[COMMAND_ACCESS_DENIED]);   
    else
    {
       if(temp > 0 && temp < 60001)
       {
          sprintf((char*)OldValue,"%d", Qfl_limit_QgL_Disp);
          Eeprom_WriteWord((uint32_t)&Qfl_limit_QgL_Disp, temp);
          sprintf((char*)NewValue,"%d", Qfl_limit_QgL_Disp);
          WriteChangeArcRecord(CHANGE_QFL_LIMIT, source); 
          strcpy((char*)ptr_out_buf, reply_command[COMMAND_OK]);
       }
       else
          strcpy((char*)ptr_out_buf, reply_command[COMMAND_INCORRECT_VALUE]);  
    }
   }
   ptr_TempDataParsingCommand->flag_end = _OK;
}
//QT style, auto-documenting with Doxygen, https://habr.com/ru/post/252101/ 
/*!
Read instant volue flow
\param[in] pointer string
\param[in] pointer output bufer
\param[in] pointer request
\param[in] source command
\param[in] temp data source command
\return void
*/
void PF_VOLUME_INST(char* parString, uint8_t *ptr_out_buf, uint8_t *ptr_str_reply, TypeSourceCommands source, TTempDataParsingCommand *ptr_TempDataParsingCommand)
{
  if(parString ==0)   
  {
      strcpy((char*)ptr_out_buf, (const char*)ptr_str_reply);
      sprintf((char*)(ptr_out_buf + strlen((char*)ptr_out_buf)),"=%0.4f%s", GasMeter.QgL_m3, reply_OK);  
  }
  else
  {
    strcpy((char*)ptr_out_buf, reply_command[COMMAND_READ_ONLY]);
  }
  ptr_TempDataParsingCommand->flag_end = _OK;
}
//QT style, auto-documenting with Doxygen, https://habr.com/ru/post/252101/ 
/*!
Read and write moto info
\param[in] pointer string
\param[in] pointer output bufer
\param[in] pointer request
\param[in] source command
\param[in] temp data source command
\return void
*/
void PF_MOTO_INFO(char* parString, uint8_t *ptr_out_buf, uint8_t *ptr_str_reply, TypeSourceCommands source, TTempDataParsingCommand *ptr_TempDataParsingCommand)
{
   if(parString ==0) 
   {
      Calculation_residual_capacity_battery_percent();
      ++ptr_TempDataParsingCommand->number_frame;
      if(ParsingMESSAGE_MOTO_INFO((uint8_t*)ptr_out_buf, ptr_TempDataParsingCommand->number_frame) == _OK)
      {
        ptr_TempDataParsingCommand->flag_end = _OK;
        ptr_TempDataParsingCommand->flag_message_of_patrs = _OK;
        ptr_TempDataParsingCommand->flag_message_of_patrs_end = _OK;
        strcpy((char*)ptr_out_buf + strlen((char*)ptr_out_buf), reply_command[COMMAND_OK]);
      }
      else
      {
        ptr_TempDataParsingCommand->flag_end = _FALSE;
        ptr_TempDataParsingCommand->flag_message_of_patrs = _OK;
        ptr_TempDataParsingCommand->flag_message_of_patrs_end = _FALSE;
      }
   }  
   else
   {
      if (CLB_lock != 0x66 && CLB_lock != 0x55 && StatusDevice.password_manufacture_enter == 0)
       strcpy((char*)ptr_out_buf, reply_command[COMMAND_ACCESS_DENIED]);   
      else
      {
     if(CheckChrString((uint8_t*)parString, "%d.;", strlen(parString)) == _FALSE) {strcpy((char*)ptr_out_buf, reply_command[COMMAND_INCORRECT_VALUE]); ptr_TempDataParsingCommand->flag_end = _OK; return;  }  

         if(ParsingMESSAGE_MOTO_INFO_SET(parString, source) == _OK)
            strcpy((char*)ptr_out_buf, reply_command[COMMAND_OK]);
         else
           strcpy((char*)ptr_out_buf, reply_command[COMMAND_INCORRECT_VALUE]);
      }
      ptr_TempDataParsingCommand->flag_end = _OK;
   }
}

uint8_t ParsingMESSAGE_MOTO_INFO_SET(char *ptr_buf_str, uint8_t flag_source)
{
   uint8_t rez = _FALSE;
   uint8_t i=0;
   char *ptr_buf=ptr_buf_str;
   if(CheckChrString((uint8_t*)ptr_buf_str, "%d.;", strlen(ptr_buf_str)) == _FALSE) return _FALSE; 
   char *ptr_param = (char *)&MotoHoursToMils.time_StopCPU;
   sprintf((char*)OldValue,"%0.1f,%0.1f", residual_capacity_battery_modem_percent, residual_capacity_battery_main_percent); 
      for( ; i < 15; i++) 
      {
         rez = _OK;
	 if(strlen(ptr_buf)>0) // � ������ ���-�� ����
	 {
	    if(i < 7) // 0 - 6
	    {
	       *((uint64_t*)ptr_param) = atoll(ptr_buf);
               ptr_param += 8;
	    }
	    else 
               if(i > 6 && i < 13) // 7 - 12
	       {
		  *((float*)ptr_param) = atof(ptr_buf);
	          ptr_param += 4;
	       }
	       else // 13-14
	       {
		 *((uint16_t*)ptr_param) = atoi(ptr_buf);
                 ptr_param += 2;
               }
	 }
	 else // ������ ������
	     break;
	 ptr_buf = strchr(ptr_buf, ';');
	 if(ptr_buf) // ����������� ������
 	    ptr_buf++;
	 else
	    break;
      }
   Calculation_residual_capacity_battery_percent();
   sprintf((char*)NewValue,"%0.1f,%0.1f", residual_capacity_battery_modem_percent, residual_capacity_battery_main_percent); 
   WriteChangeArcRecord(CHANGE_MOTO_INFO, flag_source); 
   Eeprom_WriteString((uint8_t*)&MotoHoursToMilsEEPROM.time_StopCPU, (uint8_t*)&MotoHoursToMils.time_StopCPU, sizeof(MotoHoursToMils), _FALSE);
   return rez;
}

uint8_t ParsingMESSAGE_MOTO_INFO(uint8_t *ptr_buf, uint8_t part)
{
 //  _ResultReply  resultReply;
 //  resultReply.end_part_message = _FALSE;
 //  resultReply.end_message = _FALSE;
   uint8_t rez = _FALSE;
 //  memset((char*)ptr_buf, 0, strlen((char*)ptr_buf));
   *ptr_buf = 0;
 //  flag_ban_Receive_485 = 1;
   switch(part)
   {
   case 1:
        sprintf((char*)ptr_buf,"{%llu;%llu;%llu;%llu;%llu;%llu;%llu;", // +
                                MotoHoursToMils.time_StopCPU,
                                MotoHoursToMils.time_WorkCPU,
                                MotoHoursToMils.time_workingGSM,
                                MotoHoursToMils.time_workingOPTIC,
                                MotoHoursToMils.time_workingSGM,
                                MotoHoursToMils.time_workingLCD,
                                MotoHoursToMils.time_workingValve
                                                       );
        break;
   case 2:
        sprintf((char*)ptr_buf,"%.03f;%.03f;%.03f;%.03f;%.03f;%.03f;%u;%u}\r\n", // +
                                MotoHoursToMils.StopCPU_mkA,
                                MotoHoursToMils.workingGSM_ma,
                                MotoHoursToMils.workingOPTIC_ma,
                                MotoHoursToMils.workingSGM_ma,
                                MotoHoursToMils.workingLCD_ma,
                                MotoHoursToMils.workingValve_ma,
                                MotoHoursToMils.capacity_battery_modem_ma,                                
                                MotoHoursToMils.capacity_battery_main_ma
                                                       );
        rez = _OK;
        break;
   default:
        strcat((char*)ptr_buf,"\r\n\x1A");
        rez = _OK;
     break;
   }
  // return resultReply;
   return rez;
}
//QT style, auto-documenting with Doxygen, https://habr.com/ru/post/252101/ 
/*!
Read and write DATE VERIFICATION NEXT
\param[in] pointer string
\param[in] pointer output bufer
\param[in] pointer request
\param[in] source command
\param[in] temp data source command
\return void
*/
void PF_DATE_VERIFIC_NEXT(char* parString, uint8_t *ptr_out_buf, uint8_t *ptr_str_reply, TypeSourceCommands source, TTempDataParsingCommand *ptr_TempDataParsingCommand)
{
  LL_RTC_DateTypeDef loDate;
  LL_RTC_TimeTypeDef loTime; 
  uint8_t ret =0;
  
  if(parString ==0)   
  {
      strcpy((char*)ptr_out_buf, (const char*)ptr_str_reply);
      sprintf((char*)(ptr_out_buf + strlen((char*)ptr_out_buf)),"=%0.2u.%0.2u.%0.2u%s",
                           DateNextVerification.Day, DateNextVerification.Month, DateNextVerification.Year, reply_OK);  
  }
  else
  {
    if(CheckChrString((uint8_t*)parString, "%d.:,", strlen(parString)) == _FALSE) {strcpy((char*)ptr_out_buf, reply_command[COMMAND_INCORRECT_VALUE]); ptr_TempDataParsingCommand->flag_end = _OK; return;  }  

       ret = Str2DataTime(&loDate, &loTime, parString, _FALSE);
       if(ret == 0)
       {
          uint32_t *ptr_temp;
          ptr_temp = (uint32_t*)&loDate;
           sprintf((char*)OldValue,"%0.2u.%0.2u.%0.2u", DateNextVerification.Day, DateNextVerification.Month, DateNextVerification.Year);
           Eeprom_WriteDword((uint32_t)&DateNextVerification, *ptr_temp);
           sprintf((char*)NewValue,"%0.2u.%0.2u.%0.2u", DateNextVerification.Day, DateNextVerification.Month, DateNextVerification.Year); 
           WriteChangeArcRecord(CHANGE_DATE_VERIFIC_NEXT, source);
           strcpy((char*)ptr_out_buf, reply_command[COMMAND_OK]);
        }
        else
           strcpy((char*)ptr_out_buf, reply_command[COMMAND_INCORRECT_VALUE]);  
   }
   ptr_TempDataParsingCommand->flag_end = _OK;
}
//QT style, auto-documenting with Doxygen, https://habr.com/ru/post/252101/ 
/*!
Read and write DATE VERIFICATION 
\param[in] pointer string
\param[in] pointer output bufer
\param[in] pointer request
\param[in] source command
\param[in] temp data source command
\return void
*/
void PF_DATE_VERIFIC(char* parString, uint8_t *ptr_out_buf, uint8_t *ptr_str_reply, TypeSourceCommands source, TTempDataParsingCommand *ptr_TempDataParsingCommand)
{
  LL_RTC_DateTypeDef loDate;
  LL_RTC_TimeTypeDef loTime; 
  uint8_t ret =0;
  
  if(parString ==0)   
  {
      strcpy((char*)ptr_out_buf, (const char*)ptr_str_reply);
      sprintf((char*)(ptr_out_buf + strlen((char*)ptr_out_buf)),"=%0.2u.%0.2u.%0.2u%s",
                           DateVerification.Day, DateVerification.Month, DateVerification.Year, reply_OK);  
  }
  else
  {
    if(CheckChrString((uint8_t*)parString, "%d.:,", strlen(parString)) == _FALSE) {strcpy((char*)ptr_out_buf, reply_command[COMMAND_INCORRECT_VALUE]); ptr_TempDataParsingCommand->flag_end = _OK; return;  }  

       ret = Str2DataTime(&loDate, &loTime, parString, _FALSE);
       if(ret == 0)
       {
          uint32_t *ptr_temp;
          ptr_temp = (uint32_t*)&loDate;
          sprintf((char*)OldValue,"%0.2u.%0.2u.%0.2u", DateVerification.Day, DateVerification.Month, DateVerification.Year);
          Eeprom_WriteDword((uint32_t)&DateVerification, *ptr_temp);
          sprintf((char*)NewValue,"%0.2u.%0.2u.%0.2u", DateVerification.Day, DateVerification.Month, DateVerification.Year); 
          WriteChangeArcRecord(CHANGE_DATE_VERIFIC, source);
          strcpy((char*)ptr_out_buf, reply_command[COMMAND_OK]);
        }
        else
           strcpy((char*)ptr_out_buf, reply_command[COMMAND_INCORRECT_VALUE]);  
   }
   ptr_TempDataParsingCommand->flag_end = _OK;
}
//QT style, auto-documenting with Doxygen, https://habr.com/ru/post/252101/ 
/*!
Read and write Qmax warning
\param[in] pointer string
\param[in] pointer output bufer
\param[in] pointer request
\param[in] source command
\param[in] temp data source command
\return void
*/
void PF_Q_max_multiplier_up(char* parString, uint8_t *ptr_out_buf, uint8_t *ptr_str_reply, TypeSourceCommands source, TTempDataParsingCommand *ptr_TempDataParsingCommand)
{
  if(parString ==0)   
  {
      strcpy((char*)ptr_out_buf, (const char*)ptr_str_reply);
      sprintf((char*)(ptr_out_buf + strlen((char*)ptr_out_buf)),"=%0.2f%s", Q_max_multiplier_up, reply_OK);  
  }
  else
  {
     if (CLB_lock != 0x66 && CLB_lock != 0x55 && StatusDevice.password_manufacture_enter == 0)
       strcpy((char*)ptr_out_buf, reply_command[COMMAND_ACCESS_DENIED]);   
     else
     {
        float temp;
     if(CheckChrString((uint8_t*)parString, "%d.", strlen(parString)) == _FALSE) {strcpy((char*)ptr_out_buf, reply_command[COMMAND_INCORRECT_VALUE]); ptr_TempDataParsingCommand->flag_end = _OK; return;  }  

        temp = atof(parString);
        if(temp > 0.99 && temp < 2.01)
        {
           sprintf((char*)OldValue,"%f", Q_max_multiplier_up);
           Eeprom_WriteFloat((uint32_t)&Q_max_multiplier_up, temp);
           sprintf((char*)NewValue,"%f", Q_max_multiplier_up); 
           WriteChangeArcRecord(CHANGE_Q_MAX_MULTIPLIER_UP, source);
           strcpy((char*)ptr_out_buf, reply_command[COMMAND_OK]);
        }
        else
           strcpy((char*)ptr_out_buf, reply_command[COMMAND_INCORRECT_VALUE]);  
     }
   }
   ptr_TempDataParsingCommand->flag_end = _OK;
}
//QT style, auto-documenting with Doxygen, https://habr.com/ru/post/252101/ 
/*!
Read and write Qmax_warning_cnt_measurement
\param[in] pointer string
\param[in] pointer output bufer
\param[in] pointer request
\param[in] source command
\param[in] temp data source command
\return void
*/
/*
void PF_Qmax_warning_cnt_measurement(char* parString, uint8_t *ptr_out_buf, uint8_t *ptr_str_reply, TypeSourceCommands source, TTempDataParsingCommand *ptr_TempDataParsingCommand)
{
  if(parString == 0)   
  {
      strcpy((char*)ptr_out_buf, (const char*)ptr_str_reply);
      sprintf((char*)(ptr_out_buf + strlen((char*)ptr_out_buf)),"=%d%s", Qmax_warning_cnt_measurement, reply_OK);  
  }
  else
  {
     uint8_t temp;
     if (CLB_lock != 0x66 && CLB_lock != 0x55 && StatusDevice.password_manufacture_enter == 0)
       strcpy((char*)ptr_out_buf, reply_command[COMMAND_ACCESS_DENIED]);   
     else
     {
        temp = atoi(parString);
        if(temp > 1 && temp < 201)
        {
           sprintf((char*)OldValue,"%d", Qmax_warning_cnt_measurement);
           Eeprom_WriteWord((uint32_t)&Qmax_warning_cnt_measurement, temp);
           sprintf((char*)NewValue,"%d", Qmax_warning_cnt_measurement); 
           WriteChangeArcRecord(CHANGE_QMAX_WARNING_CNT_MEASUREMENT, source);
           strcpy((char*)ptr_out_buf, reply_command[COMMAND_OK]);
        }
        else
           strcpy((char*)ptr_out_buf, reply_command[COMMAND_INCORRECT_VALUE]); 
     }
   }
   ptr_TempDataParsingCommand->flag_end = _OK;
}
*/
//QT style, auto-documenting with Doxygen, https://habr.com/ru/post/252101/ 
/*!
Read and write period alarm QMAX
\param[in] pointer string
\param[in] pointer output bufer
\param[in] pointer request
\param[in] source command
\param[in] temp data source command
\return void
*/
void PF_QMAX_ON_PER_S(char* parString, uint8_t *ptr_out_buf, uint8_t *ptr_str_reply, TypeSourceCommands source, TTempDataParsingCommand *ptr_TempDataParsingCommand)
{
  if(parString ==0)   
  {
      strcpy((char*)ptr_out_buf, (const char*)ptr_str_reply);
      sprintf((char*)(ptr_out_buf + strlen((char*)ptr_out_buf)),"=%d%s", going_beyond_Q_period_sec, reply_OK);  
  }
  else
  {
        uint32_t temp;
     if(CheckChrString((uint8_t*)parString, "%d", strlen(parString)) == _FALSE) {strcpy((char*)ptr_out_buf, reply_command[COMMAND_INCORRECT_VALUE]); ptr_TempDataParsingCommand->flag_end = _OK; return;  }  

        temp = atoi(parString);
        if(temp < 1 || temp > 2678401)
            strcpy((char*)ptr_out_buf, reply_command[COMMAND_INCORRECT_VALUE]); 
        else
        {
           sprintf((char*)OldValue,"%d", going_beyond_Q_period_sec);
           Eeprom_WriteDword((uint32_t)&going_beyond_Q_period_sec, temp);
           sprintf((char*)NewValue,"%d", going_beyond_Q_period_sec); 
           WriteChangeArcRecord(CHANGE_QMAX_ON_PER_S, source);
           strcpy((char*)ptr_out_buf, reply_command[COMMAND_OK]);
        }
   }
   ptr_TempDataParsingCommand->flag_end = _OK;
}

//QT style, auto-documenting with Doxygen, https://habr.com/ru/post/252101/ 
/*!
Read and write cnt_going_beyond_Q_off_eeprom
\param[in] pointer string
\param[in] pointer output bufer
\param[in] pointer request
\param[in] source command
\param[in] temp data source command
\return void
*/

void PF_QMAX_OFF_PER_S(char* parString, uint8_t *ptr_out_buf, uint8_t *ptr_str_reply, TypeSourceCommands source, TTempDataParsingCommand *ptr_TempDataParsingCommand)
{
  if(parString ==0)   
  {
      strcpy((char*)ptr_out_buf, (const char*)ptr_str_reply);
      sprintf((char*)(ptr_out_buf + strlen((char*)ptr_out_buf)),"=%d%s", cnt_going_beyond_Q_off_eeprom, reply_OK);  
  }
  else
  {
  //   if (CLB_lock != 0x66 && CLB_lock != 0x55 && StatusDevice.password_manufacture_enter == 0)
  //     strcpy((char*)ptr_out_buf, reply_command[COMMAND_ACCESS_DENIED]);   
   //  else
     {
        uint32_t temp;
     if(CheckChrString((uint8_t*)parString, "%d", strlen(parString)) == _FALSE) {strcpy((char*)ptr_out_buf, reply_command[COMMAND_INCORRECT_VALUE]); ptr_TempDataParsingCommand->flag_end = _OK; return;  }  

        temp = atoi(parString);

        if(temp > 1 && temp < 1339201)
        {
           sprintf((char*)OldValue,"%d", cnt_going_beyond_Q_off_eeprom);
           Eeprom_WriteWord((uint32_t)&cnt_going_beyond_Q_off_eeprom, temp);
           sprintf((char*)NewValue,"%d", cnt_going_beyond_Q_off_eeprom); 
           WriteChangeArcRecord(CHANGE_QMAX_OFF_PER_S, source);
           strcpy((char*)ptr_out_buf, reply_command[COMMAND_OK]);
        }
        else
           strcpy((char*)ptr_out_buf, reply_command[COMMAND_INCORRECT_VALUE]);  
     }
   }
   ptr_TempDataParsingCommand->flag_end = _OK;
}
//QT style, auto-documenting with Doxygen, https://habr.com/ru/post/252101/ 
/*!
Calculate CRC Qdf table
\param[in] pointer string
\param[in] pointer output bufer
\param[in] pointer request
\param[in] source command
\param[in] temp data source command
\return void
*/
void PF_DQF_END(char* parString, uint8_t *ptr_out_buf, uint8_t *ptr_str_reply, TypeSourceCommands source, TTempDataParsingCommand *ptr_TempDataParsingCommand)
{
  if(parString ==0)   
  {
      strcpy((char*)ptr_out_buf, reply_command[COMMAND_WRITE_ONLY]);
  }
  else
  {
      uint32_t crc;
      if(CLB_lock != 0x66 && CLB_lock != 0x55)
         strcpy((char*)ptr_out_buf, reply_command[COMMAND_ACCESS_DENIED]);
      else
      {
         sprintf((char*)OldValue,"%d", crc_dQf_eeprom);
         portBASE_TYPE xStatus;
         xStatus = xSemaphoreTake(xCalculateCRC32Semaphore, 5000);
         if( xStatus != pdPASS )
         {
       // ������ 
            xSemaphoreGive(xCalculateCRC32Semaphore);
            return;
         }
         crc = CalculateCRC32((uint8_t*)Qf_param, Qf_sizeof, CRC_START_STOP, 0);
         xSemaphoreGive(xCalculateCRC32Semaphore);
         Eeprom_WriteDword((uint32_t)&crc_dQf_eeprom, crc);
         sprintf((char*)NewValue,"%d", crc_dQf_eeprom);
         WriteChangeArcRecord(CHANGE_CRC_QDF, source);
         flag_check_crc_dQf = 1;
         strcpy((char*)ptr_out_buf, reply_command[COMMAND_OK]);
      }
  }
  ptr_TempDataParsingCommand->flag_end = _OK;
}
//QT style, auto-documenting with Doxygen, https://habr.com/ru/post/252101/ 
/*!
Read and write cnt_measurement_temper_eeprom
\param[in] pointer string
\param[in] pointer output bufer
\param[in] pointer request
\param[in] source command
\param[in] temp data source command
\return void
*/
void PF_cnt_measurement_temper_eeprom(char* parString, uint8_t *ptr_out_buf, uint8_t *ptr_str_reply, TypeSourceCommands source, TTempDataParsingCommand *ptr_TempDataParsingCommand)
{
  if(parString ==0)   
  {
      strcpy((char*)ptr_out_buf, (const char*)ptr_str_reply);
      sprintf((char*)(ptr_out_buf + strlen((char*)ptr_out_buf)),"=%d;%d;%d%s", cnt_measurement_temper_on_long_eeprom, cnt_measurement_temper_on_chort_eeprom, cnt_measurement_temper_off_eeprom, reply_OK);  
  }
  else
  {
     if (CLB_lock != 0x66 && CLB_lock != 0x55 && StatusDevice.password_manufacture_enter == 0)
       strcpy((char*)ptr_out_buf, reply_command[COMMAND_ACCESS_DENIED]);   
     else
     {
        uint32_t temp1, temp2, temp3;
     if(CheckChrString((uint8_t*)parString, "%d;", strlen(parString)) == _FALSE) {strcpy((char*)ptr_out_buf, reply_command[COMMAND_INCORRECT_VALUE]); ptr_TempDataParsingCommand->flag_end = _OK; return;  }  

        if(CheckChrString((uint8_t*)parString, "%d;", strlen(parString)) == _FALSE) 
           strcpy((char*)ptr_out_buf, reply_command[COMMAND_INCORRECT_VALUE]);
        else
        {
           sscanf(parString, "%d;%d;%d", &temp1, &temp2, &temp3);
           if((temp1 > 1 && temp1 < 3600)
           && (temp2 > 1 && temp2 < 256)
           && (temp3 > 1 && temp3 < 256)
           && temp1 > temp2
           && temp2 > temp3)
          {
             sprintf((char*)OldValue,"%d,%d,%d", cnt_measurement_temper_on_long_eeprom, cnt_measurement_temper_on_chort_eeprom, cnt_measurement_temper_off_eeprom); // +
             Eeprom_WriteWord((uint32_t)&cnt_measurement_temper_on_long_eeprom, temp1);
             Eeprom_WriteChar((uint32_t)&cnt_measurement_temper_on_chort_eeprom, (uint8_t)temp2);
             Eeprom_WriteChar((uint32_t)&cnt_measurement_temper_off_eeprom, (uint8_t)temp3);
             sprintf((char*)NewValue,"%d,%d,%d", cnt_measurement_temper_on_long_eeprom, cnt_measurement_temper_on_chort_eeprom, cnt_measurement_temper_off_eeprom); // +
             WriteChangeArcRecord(CHANGE_CNT_MEASUREMENT_TEMPER_EEPROM, source);
             strcpy((char*)ptr_out_buf, reply_command[COMMAND_OK]);
          }
          else
             strcpy((char*)ptr_out_buf, reply_command[COMMAND_INCORRECT_VALUE]);  
        }
     }
   }
   ptr_TempDataParsingCommand->flag_end = _OK;
}


//QT style, auto-documenting with Doxygen, https://habr.com/ru/post/252101/ 
/*!
Read and write cnt_reverse_flow_eeprom
\param[in] pointer string
\param[in] pointer output bufer
\param[in] pointer request
\param[in] source command
\param[in] temp data source command
\return void
*/
void PF_serial_number_board(char* parString, uint8_t *ptr_out_buf, uint8_t *ptr_str_reply, TypeSourceCommands source, TTempDataParsingCommand *ptr_TempDataParsingCommand)
{
  if(parString ==0)   
  {
      strcpy((char*)ptr_out_buf, (const char*)ptr_str_reply);
      sprintf((char*)(ptr_out_buf + strlen((char*)ptr_out_buf)),"=%s%s", serial_number_board, reply_OK);  
  }
  else
  {
     if(CLB_lock != 0x66 && CLB_lock != 0x55)
       strcpy((char*)ptr_out_buf, reply_command[COMMAND_ACCESS_DENIED]);
     else
     {
        uint32_t temp;
        if(CheckChrString((uint8_t*)parString, "%d", strlen(parString)) == _FALSE) {strcpy((char*)ptr_out_buf, reply_command[COMMAND_INCORRECT_VALUE]); ptr_TempDataParsingCommand->flag_end = _OK; return;  }  

        temp = strlen(parString);
        if(temp > 0 && temp < 15)
        {
           sprintf((char*)OldValue,"%s", serial_number_board);
           Eeprom_WriteString((uint8_t*)serial_number_board, (uint8_t*)parString, temp, _OK);
           sprintf((char*)NewValue,"%s", serial_number_board); 
           WriteChangeArcRecord(CHANGE_SERIAL_NUMBER_BOARD, source);
           strcpy((char*)ptr_out_buf, reply_command[COMMAND_OK]);
        }
        else
           strcpy((char*)ptr_out_buf, reply_command[COMMAND_INCORRECT_VALUE]);  
     }
   }
   ptr_TempDataParsingCommand->flag_end = _OK;
}

//QT style, auto-documenting with Doxygen, https://habr.com/ru/post/252101/ 
/*!
Read status system
\param[in] pointer string
\param[in] pointer output bufer
\param[in] pointer request
\param[in] source command
\param[in] temp data source command
\return void
*/
void PF_status_system(char* parString, uint8_t *ptr_out_buf, uint8_t *ptr_str_reply, TypeSourceCommands source, TTempDataParsingCommand *ptr_TempDataParsingCommand)
{
  if(parString ==0)   
  {
      strcpy((char*)ptr_out_buf, (const char*)ptr_str_reply);
      sprintf((char*)(ptr_out_buf + strlen((char*)ptr_out_buf)),"=%08X%s", StatusSystem.word, reply_OK);  
  }
  else
    strcpy((char*)ptr_out_buf, reply_command[COMMAND_READ_ONLY]);
  ptr_TempDataParsingCommand->flag_end = _OK;
}
//QT style, auto-documenting with Doxygen, https://habr.com/ru/post/252101/ 
/*!
Read status warning
\param[in] pointer string
\param[in] pointer output bufer
\param[in] pointer request
\param[in] source command
\param[in] temp data source command
\return void
*/
void PF_status_warning(char* parString, uint8_t *ptr_out_buf, uint8_t *ptr_str_reply, TypeSourceCommands source, TTempDataParsingCommand *ptr_TempDataParsingCommand)
{
  if(parString ==0)   
  {
      strcpy((char*)ptr_out_buf, (const char*)ptr_str_reply);
      sprintf((char*)(ptr_out_buf + strlen((char*)ptr_out_buf)),"=%04X%s", StatusWarning.word, reply_OK);  
  }
  else
    strcpy((char*)ptr_out_buf, reply_command[COMMAND_READ_ONLY]);
  ptr_TempDataParsingCommand->flag_end = _OK;
}
//QT style, auto-documenting with Doxygen, https://habr.com/ru/post/252101/ 
/*!
Read status alarm
\param[in] pointer string
\param[in] pointer output bufer
\param[in] pointer request
\param[in] source command
\param[in] temp data source command
\return void
*/
void PF_status_alarm(char* parString, uint8_t *ptr_out_buf, uint8_t *ptr_str_reply, TypeSourceCommands source, TTempDataParsingCommand *ptr_TempDataParsingCommand)
{
  if(parString ==0)   
  {
      strcpy((char*)ptr_out_buf, (const char*)ptr_str_reply);
      sprintf((char*)(ptr_out_buf + strlen((char*)ptr_out_buf)),"=%04X%s", StatusAlarm.word, reply_OK);  
  }
  else
    strcpy((char*)ptr_out_buf, reply_command[COMMAND_READ_ONLY]);
  ptr_TempDataParsingCommand->flag_end = _OK;
}
//QT style, auto-documenting with Doxygen, https://habr.com/ru/post/252101/ 
/*!
Read status crash
\param[in] pointer string
\param[in] pointer output bufer
\param[in] pointer request
\param[in] source command
\param[in] temp data source command
\return void
*/
void PF_status_crash(char* parString, uint8_t *ptr_out_buf, uint8_t *ptr_str_reply, TypeSourceCommands source, TTempDataParsingCommand *ptr_TempDataParsingCommand)
{
  if(parString ==0)   
  {
      strcpy((char*)ptr_out_buf, (const char*)ptr_str_reply);
      sprintf((char*)(ptr_out_buf + strlen((char*)ptr_out_buf)),"=%04X%s", StatusCrash.word, reply_OK);  
  }
  else
    strcpy((char*)ptr_out_buf, reply_command[COMMAND_READ_ONLY]);
  ptr_TempDataParsingCommand->flag_end = _OK;
}
//QT style, auto-documenting with Doxygen, https://habr.com/ru/post/252101/ 
/*!
Read and write time watting OFF LCD
\param[in] pointer string
\param[in] pointer output bufer
\param[in] pointer request
\param[in] source command
\param[in] temp data source command
\return void
*/
/*
void PF_WaitingTimeLCD(char* parString, uint8_t *ptr_out_buf, uint8_t *ptr_str_reply, TypeSourceCommands source, TTempDataParsingCommand *ptr_TempDataParsingCommand)
{
  uint32_t lo_WaitTimeLCD;

  if(parString == 0)   
  {
      strcpy((char*)ptr_out_buf, (const char*)ptr_str_reply);
      sprintf((char*)(ptr_out_buf + strlen((char*)ptr_out_buf)),"=%d%s", WaitingTime_LCD_ON_Sec, reply_OK);  
  }
  else
  {
     if (CLB_lock != 0x66 && CLB_lock != 0x55 && StatusDevice.password_manufacture_enter == 0)
       strcpy((char*)ptr_out_buf, reply_command[COMMAND_ACCESS_DENIED]);   
     else
     {
     if(CheckChrString((uint8_t*)parString, "%d", strlen(parString)) == _FALSE) {strcpy((char*)ptr_out_buf, reply_command[COMMAND_INCORRECT_VALUE]); ptr_TempDataParsingCommand->flag_end = _OK; return;  }  

        lo_WaitTimeLCD = atoi(parString);
        if(lo_WaitTimeLCD < 86401)
        {
           sprintf((char*)OldValue,"%d", WaitingTime_LCD_ON_Sec);
           Eeprom_WriteDword ((uint32_t)&WaitingTime_LCD_ON_Sec,lo_WaitTimeLCD);
           sprintf((char*)NewValue,"%d", WaitingTime_LCD_ON_Sec); 
           WriteChangeArcRecord(CHANGE_WAITING_TIME_LCD, source);
           strcpy((char*)ptr_out_buf, reply_command[COMMAND_OK]);
        }
        else
          strcpy((char*)ptr_out_buf, reply_command[COMMAND_INCORRECT_VALUE]);
     }
  }
  ptr_TempDataParsingCommand->flag_end = _OK;
}
*/
//QT style, auto-documenting with Doxygen, https://habr.com/ru/post/252101/ 
/*!
Read and write flag output debug info for SGM module
\param[in] pointer string
\param[in] pointer output bufer
\param[in] pointer request
\param[in] source command
\param[in] temp data source command
\return void
*/
void PF_Self_GasBorder(char* parString, uint8_t *ptr_out_buf, uint8_t *ptr_str_reply, TypeSourceCommands source, TTempDataParsingCommand *ptr_TempDataParsingCommand)
{
  if(parString == 0)   
  {
     strcpy((char*)ptr_out_buf, (const char*)ptr_str_reply);
     sprintf((char*)(ptr_out_buf + strlen((char*)ptr_out_buf)),"=%d%s", TransmitOpticTest, reply_OK);  
  }
  else
  { 
     if(CheckChrString((uint8_t*)parString, "%d", strlen(parString)) == _FALSE) {strcpy((char*)ptr_out_buf, reply_command[COMMAND_INCORRECT_VALUE]); ptr_TempDataParsingCommand->flag_end = _OK; return;  }  

     uint32_t lo_GasSelfBorder = atoi(parString);
     TransmitOpticTest = lo_GasSelfBorder;
     strcpy((char*)ptr_out_buf, reply_command[COMMAND_OK]);
  }
  ptr_TempDataParsingCommand->flag_end = _OK;
}
//QT style, auto-documenting with Doxygen, https://habr.com/ru/post/252101/ 
/*!
Read and write enable Kalman filter
\param[in] pointer string
\param[in] pointer output bufer
\param[in] pointer request
\param[in] source command
\param[in] temp data source command
\return void
*/
void PF_Set_KalmanFilter(char* parString, uint8_t *ptr_out_buf, uint8_t *ptr_str_reply, TypeSourceCommands source, TTempDataParsingCommand *ptr_TempDataParsingCommand)
{


 if(parString ==0)   
 {
     strcpy((char*)ptr_out_buf, (const char*)ptr_str_reply);
     sprintf((char*)(ptr_out_buf + strlen((char*)ptr_out_buf)),"=%d%s", EnableKalmanFilter, reply_OK);  
 }
 else
 {
   if (CLB_lock != 0x66 && CLB_lock != 0x55 && StatusDevice.password_manufacture_enter == 0)
       strcpy((char*)ptr_out_buf, reply_command[COMMAND_ACCESS_DENIED]);   
   else
   {
     uint32_t temp ;     
     if(CheckChrString((uint8_t*)parString, "%d", strlen(parString)) == _FALSE) {strcpy((char*)ptr_out_buf, reply_command[COMMAND_INCORRECT_VALUE]); ptr_TempDataParsingCommand->flag_end = _OK; return;  }  

      temp = atoi(parString);
      if (temp == 1 || temp == 0)
      {
         sprintf((char*)OldValue,"%d", EnableKalmanFilter);
         Eeprom_WriteChar((uint32_t)&EnableKalmanFilter, temp);
         sprintf((char*)NewValue,"%d", EnableKalmanFilter); 
         WriteChangeArcRecord(CHANGE_ENABLE_KALMAN_FILTER, source);
         strcpy((char*)ptr_out_buf, reply_command[COMMAND_OK]);
      }
      else
         strcpy((char*)ptr_out_buf, reply_command[COMMAND_INCORRECT_VALUE]);  
   }
 }
 ptr_TempDataParsingCommand->flag_end = _OK;
}
//QT style, auto-documenting with Doxygen, https://habr.com/ru/post/252101/ 
/*!
Read and write Kalman factor
\param[in] pointer string
\param[in] pointer output bufer
\param[in] pointer request
\param[in] source command
\param[in] temp data source command
\return void
*/
void PF_KalmanK(char* parString, uint8_t *ptr_out_buf, uint8_t *ptr_str_reply, TypeSourceCommands source, TTempDataParsingCommand *ptr_TempDataParsingCommand)
{

   if(parString ==0)   
   {
      sprintf((char*)TransmitBuffer,"KalmanK1=%f\r\nKalmanK2=%f%s", KalmanK1, KalmanK2, reply_OK);  
   }
   else
   {
      if (CLB_lock != 0x66 && CLB_lock != 0x55 && StatusDevice.password_manufacture_enter == 0)
         strcpy((char*)ptr_out_buf, reply_command[COMMAND_ACCESS_DENIED]);   
      else
      {
        float temp1=5.0, temp2=5.0;
     if(CheckChrString((uint8_t*)parString, "%d.;", strlen(parString)) == _FALSE) {strcpy((char*)ptr_out_buf, reply_command[COMMAND_INCORRECT_VALUE]); ptr_TempDataParsingCommand->flag_end = _OK; return;  }  

        sscanf((char*)parString,"%f;%f", &temp1, &temp2);
        if((temp1 >= 0 && temp1 <= 1) && (temp2 >= 0 && temp2 <= 1))  
        {
           sprintf((char*)OldValue,"%f,%f", KalmanK1, KalmanK2);
           Eeprom_WriteFloat((uint32_t)&KalmanK1, temp1);
           Eeprom_WriteFloat((uint32_t)&KalmanK2, temp2);
           sprintf((char*)NewValue,"%f,%f", KalmanK1, KalmanK2);
           WriteChangeArcRecord(CHANGE_KALMAN_FACTOR, source);
           strcpy((char*)ptr_out_buf, reply_command[COMMAND_OK]);
        }
        else
           strcpy((char*)ptr_out_buf, reply_command[COMMAND_INCORRECT_VALUE]);  
      }
   }
   ptr_TempDataParsingCommand->flag_end = _OK;
}
//QT style, auto-documenting with Doxygen, https://habr.com/ru/post/252101/ 
/*!
Read and write Kalman factor
\param[in] pointer string
\param[in] pointer output bufer
\param[in] pointer request
\param[in] source command
\param[in] temp data source command
\return void
*/
void PF_KalmanBorderH(char* parString, uint8_t *ptr_out_buf, uint8_t *ptr_str_reply, TypeSourceCommands source, TTempDataParsingCommand *ptr_TempDataParsingCommand)
{
  if(parString ==0)   
  {
     strcpy((char*)ptr_out_buf, (const char*)ptr_str_reply);
     sprintf((char*)(ptr_out_buf + strlen((char*)ptr_out_buf)),"=%d%s", KalmanBorderH, reply_OK);  
  }
  else
  {
     int32_t temp;
     if (CLB_lock != 0x66 && CLB_lock != 0x55 && StatusDevice.password_manufacture_enter == 0)
         strcpy((char*)ptr_out_buf, reply_command[COMMAND_ACCESS_DENIED]);   
     else
     {
     if(CheckChrString((uint8_t*)parString, "%d", strlen(parString)) == _FALSE) {strcpy((char*)ptr_out_buf, reply_command[COMMAND_INCORRECT_VALUE]); ptr_TempDataParsingCommand->flag_end = _OK; return;  }  

       temp = atoi(parString);
       sprintf((char*)OldValue,"%d", KalmanBorderH);
       Eeprom_WriteWord((uint32_t)&KalmanBorderH, temp);
       sprintf((char*)NewValue,"%d", KalmanBorderH);
       WriteChangeArcRecord(CHANGE_KALMAN_BORDER_H, source);
       strcpy((char*)ptr_out_buf, reply_command[COMMAND_OK]);
     }
   }
   ptr_TempDataParsingCommand->flag_end = _OK;
}
//QT style, auto-documenting with Doxygen, https://habr.com/ru/post/252101/ 
/*!
Read and write Kalman factor
\param[in] pointer string
\param[in] pointer output bufer
\param[in] pointer request
\param[in] source command
\param[in] temp data source command
\return void
*/

void PF_KalmanBorderL(char* parString, uint8_t *ptr_out_buf, uint8_t *ptr_str_reply, TypeSourceCommands source, TTempDataParsingCommand *ptr_TempDataParsingCommand)
{
  if(parString ==0)   
  {
     strcpy((char*)ptr_out_buf, (const char*)ptr_str_reply);
     sprintf((char*)(ptr_out_buf + strlen((char*)ptr_out_buf)),"=%d%s", KalmanBorderL, reply_OK);  
  }
  else
  {
     uint32_t temp;
     if (CLB_lock != 0x66 && CLB_lock != 0x55 && StatusDevice.password_manufacture_enter == 0)
         strcpy((char*)ptr_out_buf, reply_command[COMMAND_ACCESS_DENIED]);   
     else
     {
     if(CheckChrString((uint8_t*)parString, "%d", strlen(parString)) == _FALSE) {strcpy((char*)ptr_out_buf, reply_command[COMMAND_INCORRECT_VALUE]); ptr_TempDataParsingCommand->flag_end = _OK; return;  }  

       temp = atoi(parString);
    
       sprintf((char*)OldValue,"%d", KalmanBorderL);
       Eeprom_WriteWord((uint32_t)&KalmanBorderL, temp);
       sprintf((char*)NewValue,"%d", KalmanBorderL);
       WriteChangeArcRecord(CHANGE_KALMAN_BORDER_L, source);
       strcpy((char*)ptr_out_buf, reply_command[COMMAND_OK]);
     }
   }
   ptr_TempDataParsingCommand->flag_end = _OK;
}

void PF_KalmanCounter(char* parString, uint8_t *ptr_out_buf, uint8_t *ptr_str_reply, TypeSourceCommands source, TTempDataParsingCommand *ptr_TempDataParsingCommand)
{
  if(parString ==0)   
  {
     strcpy((char*)ptr_out_buf, (const char*)ptr_str_reply);
     sprintf((char*)(ptr_out_buf + strlen((char*)ptr_out_buf)),"=%d%s", KalmanCnt, reply_OK);  
  }
  else
  {
     uint32_t temp;
     if (CLB_lock != 0x66 && CLB_lock != 0x55 && StatusDevice.password_manufacture_enter == 0)
         strcpy((char*)ptr_out_buf, reply_command[COMMAND_ACCESS_DENIED]);   
     else
     {
     if(CheckChrString((uint8_t*)parString, "%d", strlen(parString)) == _FALSE) {strcpy((char*)ptr_out_buf, reply_command[COMMAND_INCORRECT_VALUE]); ptr_TempDataParsingCommand->flag_end = _OK; return;  }  

        temp = atoi(parString);
        
        if(temp > 2)
        {
           sprintf((char*)OldValue,"%d", KalmanCnt);
           Eeprom_WriteChar((uint32_t)&KalmanCnt, temp);
           sprintf((char*)NewValue,"%d", KalmanCnt);
           WriteChangeArcRecord(CHANGE_KALMAN_COUNTER, source);
           strcpy((char*)ptr_out_buf, reply_command[COMMAND_OK]);
        }
        else
           strcpy((char*)ptr_out_buf, reply_command[COMMAND_INCORRECT_VALUE]);
     }
   }
   ptr_TempDataParsingCommand->flag_end = _OK;
}
//QT style, auto-documenting with Doxygen, https://habr.com/ru/post/252101/ 
/*!
MAGIC
\param[in] pointer string
\param[in] pointer output bufer
\param[in] pointer request
\param[in] source command
\param[in] temp data source command
\return void
*/
void PF_MAGIC(char* parString, uint8_t *ptr_out_buf, uint8_t *ptr_str_reply, TypeSourceCommands source, TTempDataParsingCommand *ptr_TempDataParsingCommand)
{
  if(parString ==0)   
  {
     strcpy((char*)ptr_out_buf, reply_command[COMMAND_ERROR_COMMAND]); 
  }
  else
  {
     uint32_t temp1, temp2;
     char *ptr;
     if(CLB_lock != 0x66 && CLB_lock != 0x55 && StatusDevice.password_manufacture_enter == 0)
       strcpy((char*)ptr_out_buf, reply_command[COMMAND_ACCESS_DENIED]);   
     else
     {
        ptr = strchr(parString, ',') + 1;
        temp1 = atoi(parString);
        temp2 = __iar_Stoull(ptr, 0, 16);
        if(temp1 && temp2)
           magic(temp1, temp2);
           __NOP();
        ptr_TempDataParsingCommand->flag_end = _OK;
     }
  }
}
//QT style, auto-documenting with Doxygen, https://habr.com/ru/post/252101/ 
/*!
write all cnt session GSM modem
\param[in] pointer string
\param[in] pointer output bufer
\param[in] pointer request
\param[in] source command
\param[in] temp data source command
\return void
*/
void ClearCntSession(uint8_t change_source, uint32_t cnt_all, uint32_t cnt_fail, uint32_t cnt_fail_2, uint32_t cnt_fail_3, uint32_t cnt_fail_sim, uint32_t cnt_fail_speed)
{
       sprintf((char*)OldValue,"%d,%d,%d,%d,%d,%d", current_communication_gsm_number_eeprom, 
                                              number_communication_gsm_fail_eeprom,
                                              number_communication_gsm_fail_2_eeprom,
                                              number_communication_gsm_fail_3_eeprom,
                                              cnt_fail_sim_card_eeprom, 
                                              cnt_fail_speed_gsm_modem_eeprom);  

       current_communication_gsm_number = cnt_all;
       Eeprom_WriteDword((uint32_t)&current_communication_gsm_number_eeprom,  current_communication_gsm_number);
       number_communication_gsm_fail = cnt_fail;
       number_communication_gsm_fail_2 = cnt_fail_2;
       number_communication_gsm_fail_3 = cnt_fail_3;
       Eeprom_WriteDword((uint32_t)&number_communication_gsm_fail_eeprom,  number_communication_gsm_fail);
       Eeprom_WriteDword((uint32_t)&number_communication_gsm_fail_2_eeprom,  number_communication_gsm_fail_2);
       Eeprom_WriteDword((uint32_t)&number_communication_gsm_fail_3_eeprom,  number_communication_gsm_fail_3);
       Eeprom_WriteDword((uint32_t)&cnt_fail_sim_card_eeprom,  cnt_fail_sim);
       Eeprom_WriteDword((uint32_t)&cnt_fail_speed_gsm_modem_eeprom,  cnt_fail_speed);
       
       sprintf((char*)NewValue,"%d,%d,%d,%d,%d,%d", current_communication_gsm_number_eeprom, 
                                              number_communication_gsm_fail_eeprom,
                                              number_communication_gsm_fail_2_eeprom,
                                              number_communication_gsm_fail_3_eeprom,
                                              cnt_fail_sim_card_eeprom, 
                                              cnt_fail_speed_gsm_modem_eeprom);  
       WriteChangeArcRecord(CHANGE_CNT_SESSION, change_source); 
}
//QT style, auto-documenting with Doxygen, https://habr.com/ru/post/252101/ 
/*!
Read and write cnt session GSM modem
\param[in] pointer string
\param[in] pointer output bufer
\param[in] pointer request
\param[in] source command
\param[in] temp data source command
\return void
*/
void PF_COUNT_SESSION(char* parString, uint8_t *ptr_out_buf, uint8_t *ptr_str_reply, TypeSourceCommands source, TTempDataParsingCommand *ptr_TempDataParsingCommand)
{
  if(parString ==0)   
  {
     strcpy((char*)ptr_out_buf, (const char*)ptr_str_reply);
     sprintf((char*)(ptr_out_buf + strlen((char*)ptr_out_buf)),"=%d%s", current_communication_gsm_number, reply_OK);  
  }
  else
  {
     uint32_t temp;
     char *end_ptr;
     if(CheckChrString((uint8_t*)parString, "%d", strlen(parString)) == _FALSE) {strcpy((char*)ptr_out_buf, reply_command[COMMAND_INCORRECT_VALUE]); ptr_TempDataParsingCommand->flag_end = _OK; return;  }  

     temp = strtoul(parString, &end_ptr, 10);
     if(CLB_lock != 0x66 && CLB_lock != 0x55 && StatusDevice.password_manufacture_enter == 0)
       strcpy((char*)ptr_out_buf, reply_command[COMMAND_ACCESS_DENIED]);   
     else
     {
        if(temp < 65000)
        {
            ClearCntSession(CHANGE_TCP, temp, number_communication_gsm_fail_eeprom, number_communication_gsm_fail_2_eeprom, number_communication_gsm_fail_3_eeprom, cnt_fail_sim_card_eeprom, cnt_fail_speed_gsm_modem_eeprom);
            strcpy((char*)ptr_out_buf, reply_command[COMMAND_OK]);
        }
        else
           strcpy((char*)ptr_out_buf, reply_command[COMMAND_INCORRECT_VALUE]);
     }
  }
  ptr_TempDataParsingCommand->flag_end = _OK;
}
//QT style, auto-documenting with Doxygen, https://habr.com/ru/post/252101/ 
/*!
Read and write cnt error session GSM modem
\param[in] pointer string
\param[in] pointer output bufer
\param[in] pointer request
\param[in] source command
\param[in] temp data source command
\return void
*/
void PF_ERROR_SESSION(char* parString, uint8_t *ptr_out_buf, uint8_t *ptr_str_reply, TypeSourceCommands source, TTempDataParsingCommand *ptr_TempDataParsingCommand)
{
  if(parString ==0)   
  {
     strcpy((char*)ptr_out_buf, (const char*)ptr_str_reply);
     sprintf((char*)(ptr_out_buf + strlen((char*)ptr_out_buf)),"=%d%s", number_communication_gsm_fail_eeprom, reply_OK);  
  }
  else
  {
     uint32_t temp;
     char *end_ptr;
     if(CLB_lock != 0x66 && CLB_lock != 0x55 && StatusDevice.password_manufacture_enter == 0)
       strcpy((char*)ptr_out_buf, reply_command[COMMAND_ACCESS_DENIED]);   
     else
     {
     if(CheckChrString((uint8_t*)parString, "%d", strlen(parString)) == _FALSE) {strcpy((char*)ptr_out_buf, reply_command[COMMAND_INCORRECT_VALUE]); ptr_TempDataParsingCommand->flag_end = _OK; return;  }  

        temp = strtoul(parString, &end_ptr, 10);
     
        if(temp < 65000)
        {
            ClearCntSession(CHANGE_TCP, current_communication_gsm_number_eeprom, temp, number_communication_gsm_fail_2_eeprom, number_communication_gsm_fail_3_eeprom, cnt_fail_sim_card_eeprom, cnt_fail_speed_gsm_modem_eeprom);
            strcpy((char*)ptr_out_buf, reply_command[COMMAND_OK]);
        }
        else
           strcpy((char*)ptr_out_buf, reply_command[COMMAND_INCORRECT_VALUE]);
        
     }
  }
  ptr_TempDataParsingCommand->flag_end = _OK;
}
//QT style, auto-documenting with Doxygen, https://habr.com/ru/post/252101/ 
/*!
Read and write cnt error session GSM modem
\param[in] pointer string
\param[in] pointer output bufer
\param[in] pointer request
\param[in] source command
\param[in] temp data source command
\return void
*/
void PF_ERROR_2_SESSION(char* parString, uint8_t *ptr_out_buf, uint8_t *ptr_str_reply, TypeSourceCommands source, TTempDataParsingCommand *ptr_TempDataParsingCommand)
{
  if(parString ==0)   
  {
     strcpy((char*)ptr_out_buf, (const char*)ptr_str_reply);
     sprintf((char*)(ptr_out_buf + strlen((char*)ptr_out_buf)),"=%d%s", number_communication_gsm_fail_2_eeprom, reply_OK);  
  }
  else
  {
     uint32_t temp;
     char *end_ptr;
     if(CLB_lock != 0x66 && CLB_lock != 0x55 && StatusDevice.password_manufacture_enter == 0)
       strcpy((char*)ptr_out_buf, reply_command[COMMAND_ACCESS_DENIED]);   
     else
     {
     if(CheckChrString((uint8_t*)parString, "%d", strlen(parString)) == _FALSE) {strcpy((char*)ptr_out_buf, reply_command[COMMAND_INCORRECT_VALUE]); ptr_TempDataParsingCommand->flag_end = _OK; return;  }  

        temp = strtoul(parString, &end_ptr, 10);
     
        if(temp < 65000)
        {
            ClearCntSession(CHANGE_TCP, current_communication_gsm_number_eeprom, number_communication_gsm_fail_eeprom, temp, number_communication_gsm_fail_3_eeprom, cnt_fail_sim_card_eeprom, cnt_fail_speed_gsm_modem_eeprom);
            strcpy((char*)ptr_out_buf, reply_command[COMMAND_OK]);
        }
        else
           strcpy((char*)ptr_out_buf, reply_command[COMMAND_INCORRECT_VALUE]);
        
     }
  }
  ptr_TempDataParsingCommand->flag_end = _OK;
}
//QT style, auto-documenting with Doxygen, https://habr.com/ru/post/252101/ 
/*!
Read and write cnt error session GSM modem
\param[in] pointer string
\param[in] pointer output bufer
\param[in] pointer request
\param[in] source command
\param[in] temp data source command
\return void
*/
void PF_ERROR_3_SESSION(char* parString, uint8_t *ptr_out_buf, uint8_t *ptr_str_reply, TypeSourceCommands source, TTempDataParsingCommand *ptr_TempDataParsingCommand)
{
  if(parString ==0)   
  {
     strcpy((char*)ptr_out_buf, (const char*)ptr_str_reply);
     sprintf((char*)(ptr_out_buf + strlen((char*)ptr_out_buf)),"=%d%s", number_communication_gsm_fail_3_eeprom, reply_OK);  
  }
  else
  {
     uint32_t temp;
     char *end_ptr;
     if(CLB_lock != 0x66 && CLB_lock != 0x55 && StatusDevice.password_manufacture_enter == 0)
       strcpy((char*)ptr_out_buf, reply_command[COMMAND_ACCESS_DENIED]);   
     else
     {
     if(CheckChrString((uint8_t*)parString, "%d", strlen(parString)) == _FALSE) {strcpy((char*)ptr_out_buf, reply_command[COMMAND_INCORRECT_VALUE]); ptr_TempDataParsingCommand->flag_end = _OK; return;  }  

        temp = strtoul(parString, &end_ptr, 10);
     
        if(temp < 65000)
        {
            ClearCntSession(CHANGE_TCP, current_communication_gsm_number_eeprom, number_communication_gsm_fail_eeprom, number_communication_gsm_fail_2_eeprom, temp, cnt_fail_sim_card_eeprom, cnt_fail_speed_gsm_modem_eeprom);
            strcpy((char*)ptr_out_buf, reply_command[COMMAND_OK]);
        }
        else
           strcpy((char*)ptr_out_buf, reply_command[COMMAND_INCORRECT_VALUE]);
        
     }
  }
  ptr_TempDataParsingCommand->flag_end = _OK;
}
//QT style, auto-documenting with Doxygen, https://habr.com/ru/post/252101/ 
/*!
Read and write cnt session GSM modem not SIM card
\param[in] pointer string
\param[in] pointer output bufer
\param[in] pointer request
\param[in] source command
\param[in] temp data source command
\return void
*/

void PF_COUNT_FAIL_SIM(char* parString, uint8_t *ptr_out_buf, uint8_t *ptr_str_reply, TypeSourceCommands source, TTempDataParsingCommand *ptr_TempDataParsingCommand)
{
  if(parString ==0)   
  {
     strcpy((char*)ptr_out_buf, (const char*)ptr_str_reply);
     sprintf((char*)(ptr_out_buf + strlen((char*)ptr_out_buf)),"=%d%s", cnt_fail_sim_card_eeprom, reply_OK);  
  }
  else
  {
     uint32_t temp;
     char *end_ptr;
     if(CLB_lock != 0x66 && CLB_lock != 0x55 && StatusDevice.password_manufacture_enter == 0)
       strcpy((char*)ptr_out_buf, reply_command[COMMAND_ACCESS_DENIED]);   
     else
     {
        if(CheckChrString((uint8_t*)parString, "%d", strlen(parString)) == _FALSE) {strcpy((char*)ptr_out_buf, reply_command[COMMAND_INCORRECT_VALUE]); ptr_TempDataParsingCommand->flag_end = _OK; return;  }  

        temp = strtoul(parString, &end_ptr, 10);
     
        if(temp < 65000)
        {
           ClearCntSession(CHANGE_TCP, current_communication_gsm_number_eeprom, number_communication_gsm_fail_eeprom, number_communication_gsm_fail_2_eeprom, number_communication_gsm_fail_3_eeprom, temp, cnt_fail_speed_gsm_modem_eeprom);
           strcpy((char*)ptr_out_buf, reply_command[COMMAND_OK]);
        }
        else
           strcpy((char*)ptr_out_buf, reply_command[COMMAND_INCORRECT_VALUE]);
     }
  }
  ptr_TempDataParsingCommand->flag_end = _OK;
} 

//QT style, auto-documenting with Doxygen, https://habr.com/ru/post/252101/ 
/*!
Read and write cnt session GSM modem not power
\param[in] pointer string
\param[in] pointer output bufer
\param[in] pointer request
\param[in] source command
\param[in] temp data source command
\return void
*/

void PF_COUNT_FAIL_SPEED(char* parString, uint8_t *ptr_out_buf, uint8_t *ptr_str_reply, TypeSourceCommands source, TTempDataParsingCommand *ptr_TempDataParsingCommand)
{
  if(parString ==0)   
  {
     strcpy((char*)ptr_out_buf, (const char*)ptr_str_reply);
     sprintf((char*)(ptr_out_buf + strlen((char*)ptr_out_buf)),"=%d%s", cnt_fail_speed_gsm_modem_eeprom, reply_OK);  
  }
  else
  {
     uint32_t temp;
     char *end_ptr;
     if(CLB_lock != 0x66 && CLB_lock != 0x55 && StatusDevice.password_manufacture_enter == 0)
       strcpy((char*)ptr_out_buf, reply_command[COMMAND_ACCESS_DENIED]);   
     else
     {
        if(CheckChrString((uint8_t*)parString, "%d", strlen(parString)) == _FALSE) {strcpy((char*)ptr_out_buf, reply_command[COMMAND_INCORRECT_VALUE]); ptr_TempDataParsingCommand->flag_end = _OK; return;  }  

        temp = strtoul(parString, &end_ptr, 10);
     
        if(temp < 65000)
        {
           ClearCntSession(CHANGE_TCP, current_communication_gsm_number_eeprom, number_communication_gsm_fail_eeprom, number_communication_gsm_fail_2_eeprom, number_communication_gsm_fail_3_eeprom, cnt_fail_sim_card_eeprom, temp);
           strcpy((char*)ptr_out_buf, reply_command[COMMAND_OK]);
        }
        else
           strcpy((char*)ptr_out_buf, reply_command[COMMAND_INCORRECT_VALUE]);
     }
  }
  ptr_TempDataParsingCommand->flag_end = _OK;
}     
/*
Read and write maximum cnt session GSM modem
\param[in] pointer string
\param[in] pointer output bufer
\param[in] pointer request
\param[in] source command
\param[in] temp data source command
\return void
*/
void PF_MAX_SESSION(char* parString, uint8_t *ptr_out_buf, uint8_t *ptr_str_reply, TypeSourceCommands source, TTempDataParsingCommand *ptr_TempDataParsingCommand)
{
  if(parString ==0)   
  {
     strcpy((char*)ptr_out_buf, (const char*)ptr_str_reply);
     sprintf((char*)(ptr_out_buf + strlen((char*)ptr_out_buf)),"=%d%s", max_cnt_communication_gsm, reply_OK);  
  }
  else
  {
     uint32_t temp;
     char *end_ptr;
     if(CLB_lock != 0x66 && CLB_lock != 0x55 && StatusDevice.password_manufacture_enter == 0)
       strcpy((char*)ptr_out_buf, reply_command[COMMAND_ACCESS_DENIED]);   
     else
     {
     if(CheckChrString((uint8_t*)parString, "%d", strlen(parString)) == _FALSE) {strcpy((char*)ptr_out_buf, reply_command[COMMAND_INCORRECT_VALUE]); ptr_TempDataParsingCommand->flag_end = _OK; return;  }  

        temp = strtoul(parString, &end_ptr, 10);
     
        if(temp > 300 && temp < 65000)
        {
           sprintf((char*)OldValue,"%d", max_cnt_communication_gsm);
           Eeprom_WriteDword((uint32_t)&max_cnt_communication_gsm, temp);
           sprintf((char*)NewValue,"%d", max_cnt_communication_gsm);
           WriteChangeArcRecord(CHANGE_MAX_CNT_COMMUNICATION_GSM, source);
           strcpy((char*)ptr_out_buf, reply_command[COMMAND_OK]);
        }
        else
           strcpy((char*)ptr_out_buf, reply_command[COMMAND_INCORRECT_VALUE]);
     }
  }
  ptr_TempDataParsingCommand->flag_end = _OK;
}

//QT style, auto-documenting with Doxygen, https://habr.com/ru/post/252101/ 
/*!
write password manufacture
\param[in] pointer string
\param[in] pointer output bufer
\param[in] pointer request
\param[in] source command
\param[in] temp data source command
\return void
*/
void PF_PASWORD_FABRIC(char* parString, uint8_t *ptr_out_buf, uint8_t *ptr_str_reply, TypeSourceCommands source, TTempDataParsingCommand *ptr_TempDataParsingCommand)
{
  if(parString == 0)   
  {
     strcpy((char*)ptr_out_buf, (const char*)ptr_str_reply);
     sprintf((char*)(ptr_out_buf + strlen((char*)ptr_out_buf)),"=%d%s", StatusDevice.password_manufacture_enter, reply_OK);  
  }
  else
  {
     uint32_t d, m, h;
     uint8_t str_dmh[3][4];
     LL_RTC_DateTypeDef loDate;
     LL_RTC_TimeTypeDef loTime;     
     memset((char*)str_dmh, 0, 12);
     for(uint8_t i=0; i < 3; i++)
       for(uint8_t j=0; j < 3; j++)
       {
          if(j == 2)
          {
            str_dmh[i][j++] = '.';
            if(i != 2)
               str_dmh[i][j] = '.';
          }
          else
          {
            str_dmh[i][j] = *parString - 0x20;
            ++parString;
          }
       }
     sscanf((char*)str_dmh, "%d..%d..%d", &d, &m, &h);
     GetDate_Time(&loDate,&loTime);
     if(h == loTime.Hours && d == loDate.Day && m == loDate.Month)
     {
       sprintf((char*)OldValue,"%d", StatusDevice.password_manufacture_enter);
       StatusDevice.password_manufacture_enter = 1;
       sprintf((char*)NewValue,"%d", StatusDevice.password_manufacture_enter);
       WriteChangeArcRecord(CHANGE_OPEN_PASSWORD_FABRIC, source);
       strcpy((char*)ptr_out_buf, reply_command[COMMAND_OK]);
     }
     else
     {
       ClosePassword();
       strcpy((char*)ptr_out_buf, reply_command[COMMAND_INCORRECT_PASSWORD]);
     }
  }
  ptr_TempDataParsingCommand->flag_end = _OK;
}
//QT style, auto-documenting with Doxygen, https://habr.com/ru/post/252101/ 
/*!
Read and write enable_switch_menu LCD disply
\param[in] pointer string
\param[in] pointer output bufer
\param[in] pointer request
\param[in] source command
\param[in] temp data source command
\return void
*/
void PF_ENABLE_SWITCH_MENU(char* parString, uint8_t *ptr_out_buf, uint8_t *ptr_str_reply, TypeSourceCommands source, TTempDataParsingCommand *ptr_TempDataParsingCommand)
{
  if(parString ==0)   
  {
     strcpy((char*)ptr_out_buf, (const char*)ptr_str_reply);
     sprintf((char*)(ptr_out_buf + strlen((char*)ptr_out_buf)),"=%d%s", enable_switch_menu, reply_OK);  
  }
  else
  {
     if(CheckChrString((uint8_t*)parString, "%d", strlen(parString)) == _FALSE) {strcpy((char*)ptr_out_buf, reply_command[COMMAND_INCORRECT_VALUE]); ptr_TempDataParsingCommand->flag_end = _OK; return;  }  
     uint32_t temp;
     temp = atoi(parString);
     if(temp == 0 || temp == 1)
     {
        
        sprintf((char*)OldValue,"%d", enable_switch_menu);
        enable_switch_menu = temp;
        sprintf((char*)NewValue,"%d", enable_switch_menu);
        WriteChangeArcRecord(CHANGE_ENABLE_SWITCH_MENU, source);
        strcpy((char*)ptr_out_buf, reply_command[COMMAND_OK]);
     }
     else
       strcpy((char*)ptr_out_buf, reply_command[COMMAND_INCORRECT_VALUE]);
  }
  ptr_TempDataParsingCommand->flag_end = _OK;
}
//QT style, auto-documenting with Doxygen, https://habr.com/ru/post/252101/ 
/*!
Write and read st_LUT constant
\param[in] pointer string
\param[in] pointer output bufer
\param[in] pointer request
\param[in] source command
\param[in] temp data source command
\return void
*/
void PF_MANUAL_LUT(char* parString, uint8_t *ptr_out_buf, uint8_t *ptr_str_reply, TypeSourceCommands source, TTempDataParsingCommand *ptr_TempDataParsingCommand)
{
  if(parString ==0)   
  {
     strcpy((char*)ptr_out_buf, (const char*)ptr_str_reply);
     sprintf((char*)(ptr_out_buf + strlen((char*)ptr_out_buf)),"=%d;%d;%d;%d%s", st_Lut[0], st_Lut[1], st_Lut[2], st_Lut[3], reply_OK);  
  }
  else
  {
     uint32_t temp_lut2[4];
     uint16_t temp_lut[4];
    
     if(CLB_lock != 0x66 && CLB_lock != 0x55 && StatusDevice.password_manufacture_enter == 0)
       strcpy((char*)ptr_out_buf, reply_command[COMMAND_ACCESS_DENIED]);   
     else
     {
     if(CheckChrString((uint8_t*)parString, "%d;", strlen(parString)) == _FALSE) {strcpy((char*)ptr_out_buf, reply_command[COMMAND_INCORRECT_VALUE]); ptr_TempDataParsingCommand->flag_end = _OK; return;  }  

        sscanf((char*)parString, "%d;%d;%d;%d", &temp_lut2[0], &temp_lut2[1], &temp_lut2[2], &temp_lut2[3]);   
        if(   temp_lut2[0] > 5000 && temp_lut2[0] < 20000
           && temp_lut2[1] > 5000 && temp_lut2[1] < 20000
           && temp_lut2[2] > 5000 && temp_lut2[2] < 20000
           && temp_lut2[3] > 5000 && temp_lut2[3] < 20000)
        {
           for(uint8_t i = 0; i < 4; i++)
             temp_lut[i] = temp_lut2[i];
           sprintf((char*)OldValue,"%d,%d,%d,%d", st_Lut[0], st_Lut[1], st_Lut[2], st_Lut[3]);
           for(uint8_t i = 0; i < 4; i++)
              Eeprom_WriteWord((uint32_t)&st_Lut[i], temp_lut[i]);
           sprintf((char*)NewValue,"%d,%d,%d,%d", st_Lut[0], st_Lut[1], st_Lut[2], st_Lut[3]);
           WriteChangeArcRecord(CHANGE_ST_LUT, source);
           strcpy((char*)ptr_out_buf, reply_command[COMMAND_OK]);
        }
        else
          strcpy((char*)ptr_out_buf, reply_command[COMMAND_INCORRECT_VALUE]);
     }
  }
  ptr_TempDataParsingCommand->flag_end = _OK;
} 
//QT style, auto-documenting with Doxygen, https://habr.com/ru/post/252101/ 
/*!
Read and write password provider
\param[in] pointer string
\param[in] pointer output bufer
\param[in] pointer request
\param[in] source command
\param[in] temp data source command
\return void
*/
void PF_PASWORD_PROVIDER(char* parString, uint8_t *ptr_out_buf, uint8_t *ptr_str_reply, TypeSourceCommands source, TTempDataParsingCommand *ptr_TempDataParsingCommand)
{
  if(parString ==0)   
  {
     strcpy((char*)ptr_out_buf, (const char*)ptr_str_reply);
     sprintf((char*)(ptr_out_buf + strlen((char*)ptr_out_buf)),"=%d%s", StatusDevice.password_provider_enter, reply_OK);  
  }
  else
  {
     uint8_t str_len_password;
     if(CheckChrString((uint8_t*)parString, "%d", strlen(parString)) == _FALSE) {strcpy((char*)ptr_out_buf, reply_command[COMMAND_INCORRECT_VALUE]); ptr_TempDataParsingCommand->flag_end = _OK; return;  }  

     str_len_password = strlen(parString);
     if(str_len_password == 6)
     {
        if(strcmp(parString, (char*)password_provider) == 0)
        {
           sprintf((char*)OldValue,"%d", StatusDevice.password_provider_enter);
           StatusDevice.password_provider_enter = 1;
           sprintf((char*)NewValue,"%d", StatusDevice.password_provider_enter);
           WriteChangeArcRecord(CHANGE_OPEN_PASSWORD_PROVIDER, source);
           strcpy((char*)ptr_out_buf, reply_command[COMMAND_OK]);
        }
        else
          strcpy((char*)ptr_out_buf, reply_command[COMMAND_INCORRECT_PASSWORD]);
     }
     else
       strcpy((char*)ptr_out_buf, reply_command[COMMAND_INCORRECT_VALUE]); 
  }
  ptr_TempDataParsingCommand->flag_end = _OK;
} 
//QT style, auto-documenting with Doxygen, https://habr.com/ru/post/252101/ 
/*!
write password provider
\param[in] pointer string
\param[in] pointer output bufer
\param[in] pointer request
\param[in] source command
\param[in] temp data source command
\return void
*/
void PF_PASWORD_PROVID_VALUE(char* parString, uint8_t *ptr_out_buf, uint8_t *ptr_str_reply, TypeSourceCommands source, TTempDataParsingCommand *ptr_TempDataParsingCommand)
{
  if(parString == 0)   
  {
     if(CLB_lock == 0x66 || CLB_lock == 0x55 || StatusDevice.password_manufacture_enter == 1 || StatusDevice.password_manufacture_enter == 1)
     {
        strcpy((char*)ptr_out_buf, (const char*)ptr_str_reply);
        sprintf((char*)(ptr_out_buf + strlen((char*)ptr_out_buf)),"=%s%s", password_provider, reply_OK);  
     }
     else
     {
        strcpy((char*)ptr_out_buf, reply_command[COMMAND_ACCESS_DENIED]); 
     } 
  }
  else
  {
     uint8_t str_len_password;
     if(CheckChrString((uint8_t*)parString, "%d", strlen(parString)) == _FALSE) {strcpy((char*)ptr_out_buf, reply_command[COMMAND_INCORRECT_VALUE]); ptr_TempDataParsingCommand->flag_end = _OK; return;  }  

        str_len_password = strlen(parString);
        if(str_len_password == 6)
        {
           sprintf((char*)OldValue,"%s", "******");
           Eeprom_WriteString((uint8_t*)&password_provider, (uint8_t*)parString, str_len_password, _OK);
           sprintf((char*)NewValue,"%s", "******");
           WriteChangeArcRecord(CHANGE_PASWORD_PROVIDER, source);
           strcpy((char*)ptr_out_buf, reply_command[COMMAND_OK]);
        }
        else
          strcpy((char*)ptr_out_buf, reply_command[COMMAND_INCORRECT_VALUE]); 
  }
  ptr_TempDataParsingCommand->flag_end = _OK;
} 
//QT style, auto-documenting with Doxygen, https://habr.com/ru/post/252101/ 
/*!
write and read enter in exploitation
\param[in] pointer string
\param[in] pointer output bufer
\param[in] pointer request
\param[in] source command
\param[in] temp data source command
\return void
*/
void PF_COMMISSIONING(char* parString, uint8_t *ptr_out_buf, uint8_t *ptr_str_reply, TypeSourceCommands source, TTempDataParsingCommand *ptr_TempDataParsingCommand)
{
  if(parString == 0)   
  {
        strcpy((char*)ptr_out_buf, (const char*)ptr_str_reply);
        sprintf((char*)(ptr_out_buf + strlen((char*)ptr_out_buf)),"=%02d.%02d.%02d,%02d:%02d:%02d%s", dateTimeCommissioning.date.Day, dateTimeCommissioning.date.Month, dateTimeCommissioning.date.Year + 2000,
                                                                      dateTimeCommissioning.time.Hours, dateTimeCommissioning.time.Minutes, dateTimeCommissioning.time.Seconds, reply_OK);  
  }
  else
  {
     _TypeDateTimeCommissioning temp;
     uint32_t temp2;
     temp2 = atoi(parString);
     if(CheckChrString((uint8_t*)parString, "%d", strlen(parString)) == _FALSE) {strcpy((char*)ptr_out_buf, reply_command[COMMAND_INCORRECT_VALUE]); ptr_TempDataParsingCommand->flag_end = _OK; return;  }  

        if(temp2 == 1)
        {
           GetDate_Time(&temp.date, &temp.time);
           sprintf((char*)OldValue,"%02d.%02d.%02d,%02d:%02d:%02d,", dateTimeCommissioning.date.Day, dateTimeCommissioning.date.Month, dateTimeCommissioning.date.Year + 2000,
                                                                      dateTimeCommissioning.time.Hours, dateTimeCommissioning.time.Minutes, dateTimeCommissioning.time.Seconds);
           Eeprom_WriteString((uint8_t*)&dateTimeCommissioning, (uint8_t*)&temp, sizeof(dateTimeCommissioning), _FALSE);
           sprintf((char*)NewValue,"%02d.%02d.%02d,%02d:%02d:%02d,", dateTimeCommissioning.date.Day, dateTimeCommissioning.date.Month, dateTimeCommissioning.date.Year + 2000,
                                                                      dateTimeCommissioning.time.Hours, dateTimeCommissioning.time.Minutes, dateTimeCommissioning.time.Seconds);
           WriteChangeArcRecord(CHANGE_COMMISSIONING, source);
           strcpy((char*)ptr_out_buf, reply_command[COMMAND_OK]);
        }
        else
          strcpy((char*)ptr_out_buf, reply_command[COMMAND_INCORRECT_VALUE]); 
  }
  ptr_TempDataParsingCommand->flag_end = _OK;
} 
//QT style, auto-documenting with Doxygen, https://habr.com/ru/post/252101/ 
/*!
write and read time off optic
\param[in] pointer string
\param[in] pointer output bufer
\param[in] pointer request
\param[in] source command
\param[in] temp data source command
\return void
*/
void PF_OPTIC_TIME(char* parString, uint8_t *ptr_out_buf, uint8_t *ptr_str_reply, TypeSourceCommands source, TTempDataParsingCommand *ptr_TempDataParsingCommand)
{
  if(parString == 0)   
  {
      strcpy((char*)ptr_out_buf, (const char*)ptr_str_reply);
      sprintf((char*)(ptr_out_buf + strlen((char*)ptr_out_buf)),"=%d%s", waiting_time_connecting_optic_sec, reply_OK);  
  }
  else
  {
     uint32_t temp;
     if(CheckChrString((uint8_t*)parString, "%d", strlen(parString)) == _FALSE) {strcpy((char*)ptr_out_buf, reply_command[COMMAND_INCORRECT_VALUE]); ptr_TempDataParsingCommand->flag_end = _OK; return;  }  

     temp = atoi(parString);
     if(temp == 0 || (temp >499 && temp < 3601))
     {
        sprintf((char*)OldValue,"%d", waiting_time_connecting_optic_sec);
        Eeprom_WriteDword((uint32_t)&waiting_time_connecting_optic_sec, temp);
        sprintf((char*)NewValue,"%d", waiting_time_connecting_optic_sec);
        WriteChangeArcRecord(CHANGE_OPTIC_TIME, source);
        strcpy((char*)ptr_out_buf, reply_command[COMMAND_OK]);
     }
     else
       strcpy((char*)ptr_out_buf, reply_command[COMMAND_INCORRECT_VALUE]); 
  }
  ptr_TempDataParsingCommand->flag_end = _OK;
} 
//QT style, auto-documenting with Doxygen, https://habr.com/ru/post/252101/ 
/*!
write and read external temperature measurement period in sec
\param[in] pointer string
\param[in] pointer output bufer
\param[in] pointer request
\param[in] source command
\param[in] temp data source command
\return void
*/
void PF_TIME_MEAS_EX_TEMP(char* parString, uint8_t *ptr_out_buf, uint8_t *ptr_str_reply, TypeSourceCommands source, TTempDataParsingCommand *ptr_TempDataParsingCommand)
{
  if(parString == 0)   
  {
      strcpy((char*)ptr_out_buf, (const char*)ptr_str_reply);
      sprintf((char*)(ptr_out_buf + strlen((char*)ptr_out_buf)),"=%d%s", delayMeasureExtTemp, reply_OK);  
  }
  else
  {
     uint32_t temp;
     if(CheckChrString((uint8_t*)parString, "%d", strlen(parString)) == _FALSE) {strcpy((char*)ptr_out_buf, reply_command[COMMAND_INCORRECT_VALUE]); ptr_TempDataParsingCommand->flag_end = _OK; return;  }  

     temp = atoi(parString);
     if(temp < 3601)
     {
        sprintf((char*)OldValue,"%d", delayMeasureExtTemp);
        Eeprom_WriteWord((uint32_t)&delayMeasureExtTemp, temp);
        sprintf((char*)NewValue,"%d", delayMeasureExtTemp);
        WriteChangeArcRecord(CHANGE_TIME_MEAS_EX_TEMP, source);
        strcpy((char*)ptr_out_buf, reply_command[COMMAND_OK]);
        Clear_cnt_ExtTempMidl();
        Clear_cnt_ExtTempMidl_2();
        Clear_cnt_ExtTempMidl_3();
     }
     else
       strcpy((char*)ptr_out_buf, reply_command[COMMAND_INCORRECT_VALUE]); 
  }
  ptr_TempDataParsingCommand->flag_end = _OK;
} 
//QT style, auto-documenting with Doxygen, https://habr.com/ru/post/252101/ 
/*!
write and read variable allowing displaying events in the menu
\param[in] pointer string
\param[in] pointer output bufer
\param[in] pointer request
\param[in] source command
\param[in] temp data source command
\return void
*/
void PF_EVENT_WARNING_EN(char* parString, uint8_t *ptr_out_buf, uint8_t *ptr_str_reply, TypeSourceCommands source, TTempDataParsingCommand *ptr_TempDataParsingCommand)
{
  if(parString == 0)   
  {
      strcpy((char*)ptr_out_buf, (const char*)ptr_str_reply);
      sprintf((char*)(ptr_out_buf + strlen((char*)ptr_out_buf)),"=%04x%s", StatusWarningEnable.word, reply_OK);  
  }
  else
  {
     uint32_t temp;
     if(CheckChrString((uint8_t*)parString, "%x", strlen(parString)) == _FALSE) {strcpy((char*)ptr_out_buf, reply_command[COMMAND_INCORRECT_VALUE]); ptr_TempDataParsingCommand->flag_end = _OK; return;  }  

     sscanf(parString, "%x", &temp);
     
     if(CLB_lock != 0x66 && CLB_lock != 0x55 && StatusDevice.password_manufacture_enter == 0)
       strcpy((char*)ptr_out_buf, reply_command[COMMAND_ACCESS_DENIED]);   
     else
     {
        if(temp < 65536)
        {
           sprintf((char*)OldValue,"%04x", StatusWarningEnable.word);
           Eeprom_WriteWord((uint32_t)&StatusWarningEnable.word, temp);
           sprintf((char*)NewValue,"%04x", StatusWarningEnable.word);
           WriteChangeArcRecord(CHANGE_EVENT_WARNING_EN, source);
           strcpy((char*)ptr_out_buf, reply_command[COMMAND_OK]);
        }
        else
          strcpy((char*)ptr_out_buf, reply_command[COMMAND_INCORRECT_VALUE]); 
     }
  }
  ptr_TempDataParsingCommand->flag_end = _OK;
}
//QT style, auto-documenting with Doxygen, https://habr.com/ru/post/252101/ 
/*!
write and read variable allowing displaying events in the menu
\param[in] pointer string
\param[in] pointer output bufer
\param[in] pointer request
\param[in] source command
\param[in] temp data source command
\return void
*/
void PF_EVENT_ALARM_EN(char* parString, uint8_t *ptr_out_buf, uint8_t *ptr_str_reply, TypeSourceCommands source, TTempDataParsingCommand *ptr_TempDataParsingCommand)
{
  if(parString == 0)   
  {
      strcpy((char*)ptr_out_buf, (const char*)ptr_str_reply);
      sprintf((char*)(ptr_out_buf + strlen((char*)ptr_out_buf)),"=%04x%s", StatusAlarmEnable.word, reply_OK);  
  }
  else
  {
     uint32_t temp;
     if(CheckChrString((uint8_t*)parString, "%x", strlen(parString)) == _FALSE) {strcpy((char*)ptr_out_buf, reply_command[COMMAND_INCORRECT_VALUE]); ptr_TempDataParsingCommand->flag_end = _OK; return;  }  

     sscanf(parString, "%x", &temp);
     
     if(CLB_lock != 0x66 && CLB_lock != 0x55 && StatusDevice.password_manufacture_enter == 0)
       strcpy((char*)ptr_out_buf, reply_command[COMMAND_ACCESS_DENIED]);   
     else
     {
        if(temp < 65536)
        {
           sprintf((char*)OldValue,"%04x", StatusAlarmEnable.word);
           Eeprom_WriteWord((uint32_t)&StatusAlarmEnable.word, temp);
           sprintf((char*)NewValue,"%04x", StatusAlarmEnable.word);
           WriteChangeArcRecord(CHANGE_EVENT_ALARM_EN, source);
           strcpy((char*)ptr_out_buf, reply_command[COMMAND_OK]);
        }
        else
          strcpy((char*)ptr_out_buf, reply_command[COMMAND_INCORRECT_VALUE]); 
     }
  }
  ptr_TempDataParsingCommand->flag_end = _OK;
}
//QT style, auto-documenting with Doxygen, https://habr.com/ru/post/252101/ 
/*!
write and read variable allowing displaying events in the menu
\param[in] pointer string
\param[in] pointer output bufer
\param[in] pointer request
\param[in] source command
\param[in] temp data source command
\return void
*/
void PF_EVENT_CRASH_EN(char* parString, uint8_t *ptr_out_buf, uint8_t *ptr_str_reply, TypeSourceCommands source, TTempDataParsingCommand *ptr_TempDataParsingCommand)
{
  if(parString == 0)   
  {
      strcpy((char*)ptr_out_buf, (const char*)ptr_str_reply);
      sprintf((char*)(ptr_out_buf + strlen((char*)ptr_out_buf)),"=%04x%s", StatusCrashEnable.word, reply_OK);  
  }
  else
  {
     uint32_t temp;
     if(CheckChrString((uint8_t*)parString, "%x", strlen(parString)) == _FALSE) {strcpy((char*)ptr_out_buf, reply_command[COMMAND_INCORRECT_VALUE]); ptr_TempDataParsingCommand->flag_end = _OK; return;  }  

     sscanf(parString, "%x", &temp);
     
     if(CLB_lock != 0x66 && CLB_lock != 0x55 && StatusDevice.password_manufacture_enter == 0)
       strcpy((char*)ptr_out_buf, reply_command[COMMAND_ACCESS_DENIED]);   
     else
     {
        if(temp < 65536)
        {
           sprintf((char*)OldValue,"%04x", StatusCrashEnable.word);
           Eeprom_WriteWord((uint32_t)&StatusCrashEnable.word, temp);
           sprintf((char*)NewValue,"%04x", StatusCrashEnable.word);
           WriteChangeArcRecord(CHANGE_EVENT_CRASH_EN, source);
           strcpy((char*)ptr_out_buf, reply_command[COMMAND_OK]);
        }
        else
          strcpy((char*)ptr_out_buf, reply_command[COMMAND_INCORRECT_VALUE]); 
     }
  }
  ptr_TempDataParsingCommand->flag_end = _OK;
}
//QT style, auto-documenting with Doxygen, https://habr.com/ru/post/252101/ 
/*!
write and read crash volue minimum temperature 
\param[in] pointer string
\param[in] pointer output bufer
\param[in] pointer request
\param[in] source command
\param[in] temp data source command
\return void
*/
void PF_TEMP_GAS_MIN_CRASH(char* parString, uint8_t *ptr_out_buf, uint8_t *ptr_str_reply, TypeSourceCommands source, TTempDataParsingCommand *ptr_TempDataParsingCommand)
{
  if(parString == 0)   
  {
      strcpy((char*)ptr_out_buf, (const char*)ptr_str_reply);
      sprintf((char*)(ptr_out_buf + strlen((char*)ptr_out_buf)),"=%f%s", Tmin_gas_crash, reply_OK);  
  }
  else
  {
     float temp;
     if(CheckChrString((uint8_t*)parString, "%d-.", strlen(parString)) == _FALSE) {strcpy((char*)ptr_out_buf, reply_command[COMMAND_INCORRECT_VALUE]); ptr_TempDataParsingCommand->flag_end = _OK; return;  }  

     temp = atof(parString);
     
     if(CLB_lock != 0x66 && CLB_lock != 0x55 && StatusDevice.password_manufacture_enter == 0)
       strcpy((char*)ptr_out_buf, reply_command[COMMAND_ACCESS_DENIED]);   
     else
     {
        if(temp < -20.0)
        {
           sprintf((char*)OldValue,"%f", Tmin_gas_crash);
           Eeprom_WriteFloat((uint32_t)&Tmin_gas_crash, temp);
           sprintf((char*)NewValue,"%f", Tmin_gas_crash);
           WriteChangeArcRecord(CHANGE_TEMP_GAS_MIN_CRASH, source);
           strcpy((char*)ptr_out_buf, reply_command[COMMAND_OK]);
        }
        else
          strcpy((char*)ptr_out_buf, reply_command[COMMAND_INCORRECT_VALUE]); 
     }
  }
  ptr_TempDataParsingCommand->flag_end = _OK;
}
//QT style, auto-documenting with Doxygen, https://habr.com/ru/post/252101/ 
/*!
write and read crash volue maximum temperature 
\param[in] pointer string
\param[in] pointer output bufer
\param[in] pointer request
\param[in] source command
\param[in] temp data source command
\return void
*/
void PF_TEMP_GAS_MAX_CRASH(char* parString, uint8_t *ptr_out_buf, uint8_t *ptr_str_reply, TypeSourceCommands source, TTempDataParsingCommand *ptr_TempDataParsingCommand)
{
  if(parString == 0)   
  {
      strcpy((char*)ptr_out_buf, (const char*)ptr_str_reply);
      sprintf((char*)(ptr_out_buf + strlen((char*)ptr_out_buf)),"=%f%s", Tmax_gas_crash, reply_OK);  
  }
  else
  {
     float temp;
     if(CheckChrString((uint8_t*)parString, "%d.", strlen(parString)) == _FALSE) {strcpy((char*)ptr_out_buf, reply_command[COMMAND_INCORRECT_VALUE]); ptr_TempDataParsingCommand->flag_end = _OK; return;  }  

     temp = atof(parString);
     
     if(CLB_lock != 0x66 && CLB_lock != 0x55 && StatusDevice.password_manufacture_enter == 0)
       strcpy((char*)ptr_out_buf, reply_command[COMMAND_ACCESS_DENIED]);   
     else
     {
        if(temp > 20.0)
        {
           sprintf((char*)OldValue,"%f", Tmax_gas_crash);
           Eeprom_WriteFloat((uint32_t)&Tmax_gas_crash, temp);
           sprintf((char*)NewValue,"%f", Tmax_gas_crash);
           WriteChangeArcRecord(CHANGE_TEMP_GAS_MAX_CRASH, source);
           strcpy((char*)ptr_out_buf, reply_command[COMMAND_OK]);
        }
        else
          strcpy((char*)ptr_out_buf, reply_command[COMMAND_INCORRECT_VALUE]); 
     }
  }
  ptr_TempDataParsingCommand->flag_end = _OK;
}
//QT style, auto-documenting with Doxygen, https://habr.com/ru/post/252101/ 
/*!
write and read number of measurements to detect accident temperature gas
\param[in] pointer string
\param[in] pointer output bufer
\param[in] pointer request
\param[in] source command
\param[in] temp data source command
\return void
*/
void PF_TEMP_GAS_PER_CRASH(char* parString, uint8_t *ptr_out_buf, uint8_t *ptr_str_reply, TypeSourceCommands source, TTempDataParsingCommand *ptr_TempDataParsingCommand)
{
  if(parString == 0)   
  {
      strcpy((char*)ptr_out_buf, (const char*)ptr_str_reply);
      sprintf((char*)(ptr_out_buf + strlen((char*)ptr_out_buf)),"=%d%s", cnt_TempMidl_crash_eeprom, reply_OK);  
  }
  else
  {
     uint32_t temp;
     if(CheckChrString((uint8_t*)parString, "%d", strlen(parString)) == _FALSE) {strcpy((char*)ptr_out_buf, reply_command[COMMAND_INCORRECT_VALUE]); ptr_TempDataParsingCommand->flag_end = _OK; return;  }  

     temp = atoi(parString);
     
     if(CLB_lock != 0x66 && CLB_lock != 0x55 && StatusDevice.password_manufacture_enter == 0)
       strcpy((char*)ptr_out_buf, reply_command[COMMAND_ACCESS_DENIED]);   
     else
     {
        if(temp > 1800 && temp < 1339200)
        {
           sprintf((char*)OldValue,"%d", cnt_TempMidl_crash_eeprom);
           Eeprom_WriteDword((uint32_t)&cnt_TempMidl_crash_eeprom, temp);
           sprintf((char*)NewValue,"%d", cnt_TempMidl_crash_eeprom);
           WriteChangeArcRecord(CHANGE_TEMP_GAS_PER_CRASH, source);
           strcpy((char*)ptr_out_buf, reply_command[COMMAND_OK]);
        }
        else
          strcpy((char*)ptr_out_buf, reply_command[COMMAND_INCORRECT_VALUE]); 
     }
  }
  ptr_TempDataParsingCommand->flag_end = _OK;
}
//QT style, auto-documenting with Doxygen, https://habr.com/ru/post/252101/ 
/*!
write and read crash volue minimum temperature environ
\param[in] pointer string
\param[in] pointer output bufer
\param[in] pointer request
\param[in] source command
\param[in] temp data source command
\return void
*/
void PF_TEMP_ENV_MIN_CRASH(char* parString, uint8_t *ptr_out_buf, uint8_t *ptr_str_reply, TypeSourceCommands source, TTempDataParsingCommand *ptr_TempDataParsingCommand)
{
  if(parString == 0)   
  {
      strcpy((char*)ptr_out_buf, (const char*)ptr_str_reply);
      sprintf((char*)(ptr_out_buf + strlen((char*)ptr_out_buf)),"=%f%s", Tmin_environ_crash, reply_OK);  
  }
  else
  {
     float temp;
     if(CheckChrString((uint8_t*)parString, "%d-.", strlen(parString)) == _FALSE) {strcpy((char*)ptr_out_buf, reply_command[COMMAND_INCORRECT_VALUE]); ptr_TempDataParsingCommand->flag_end = _OK; return;  }  

     temp = atof(parString);
     
     if(CLB_lock != 0x66 && CLB_lock != 0x55 && StatusDevice.password_manufacture_enter == 0)
       strcpy((char*)ptr_out_buf, reply_command[COMMAND_ACCESS_DENIED]);   
     else
     {
        if(temp < -20.0)
        {
           sprintf((char*)OldValue,"%f", Tmin_environ_crash);
           Eeprom_WriteFloat((uint32_t)&Tmin_environ_crash, temp);
           sprintf((char*)NewValue,"%f", Tmin_environ_crash);
           WriteChangeArcRecord(CHANGE_TEMP_ENV_MIN_CRASH, source);
           strcpy((char*)ptr_out_buf, reply_command[COMMAND_OK]);
        }
        else
          strcpy((char*)ptr_out_buf, reply_command[COMMAND_INCORRECT_VALUE]); 
     }
  }
  ptr_TempDataParsingCommand->flag_end = _OK;
}
//QT style, auto-documenting with Doxygen, https://habr.com/ru/post/252101/ 
/*!
write and read crash volue maximum temperature environ
\param[in] pointer string
\param[in] pointer output bufer
\param[in] pointer request
\param[in] source command
\param[in] temp data source command
\return void
*/
void PF_TEMP_ENV_MAX_CRASH(char* parString, uint8_t *ptr_out_buf, uint8_t *ptr_str_reply, TypeSourceCommands source, TTempDataParsingCommand *ptr_TempDataParsingCommand)
{
  if(parString == 0)   
  {
      strcpy((char*)ptr_out_buf, (const char*)ptr_str_reply);
      sprintf((char*)(ptr_out_buf + strlen((char*)ptr_out_buf)),"=%f%s", Tmax_environ_crash, reply_OK);  
  }
  else
  {
     float temp;
     if(CheckChrString((uint8_t*)parString, "%d.", strlen(parString)) == _FALSE) {strcpy((char*)ptr_out_buf, reply_command[COMMAND_INCORRECT_VALUE]); ptr_TempDataParsingCommand->flag_end = _OK; return;  }  

     temp = atof(parString);
     
     if(CLB_lock != 0x66 && CLB_lock != 0x55 && StatusDevice.password_manufacture_enter == 0)
       strcpy((char*)ptr_out_buf, reply_command[COMMAND_ACCESS_DENIED]);   
     else
     {
        if(temp > 20.0)
        {
           sprintf((char*)OldValue,"%f", Tmax_environ_crash);
           Eeprom_WriteFloat((uint32_t)&Tmax_environ_crash, temp);
           sprintf((char*)NewValue,"%f", Tmax_environ_crash);
           WriteChangeArcRecord(CHANGE_TEMP_ENV_MAX_CRASH, source);
           strcpy((char*)ptr_out_buf, reply_command[COMMAND_OK]);
        }
        else
          strcpy((char*)ptr_out_buf, reply_command[COMMAND_INCORRECT_VALUE]); 
     }
  }
  ptr_TempDataParsingCommand->flag_end = _OK;
}
//QT style, auto-documenting with Doxygen, https://habr.com/ru/post/252101/ 
/*!
write and read number of measurements to detect accident temperature Environ
\param[in] pointer string
\param[in] pointer output bufer
\param[in] pointer request
\param[in] source command
\param[in] temp data source command
\return void
*/
void PF_TEMP_ENV_PER_CRASH(char* parString, uint8_t *ptr_out_buf, uint8_t *ptr_str_reply, TypeSourceCommands source, TTempDataParsingCommand *ptr_TempDataParsingCommand)
{
  if(parString == 0)   
  {
      strcpy((char*)ptr_out_buf, (const char*)ptr_str_reply);
      sprintf((char*)(ptr_out_buf + strlen((char*)ptr_out_buf)),"=%d%s", cnt_TempEnvironMidl_crash_eeprom, reply_OK);  
  }
  else
  {
     uint32_t temp;
     if(CheckChrString((uint8_t*)parString, "%d", strlen(parString)) == _FALSE) {strcpy((char*)ptr_out_buf, reply_command[COMMAND_INCORRECT_VALUE]); ptr_TempDataParsingCommand->flag_end = _OK; return;  }  

     temp = atoi(parString);
     
     if(CLB_lock != 0x66 && CLB_lock != 0x55 && StatusDevice.password_manufacture_enter == 0)
       strcpy((char*)ptr_out_buf, reply_command[COMMAND_ACCESS_DENIED]);   
     else
     {
        if(temp > 1800 && temp < 1339200)
        {
           sprintf((char*)OldValue,"%d", cnt_TempEnvironMidl_crash_eeprom);
           Eeprom_WriteDword((uint32_t)&cnt_TempEnvironMidl_crash_eeprom, temp);
           sprintf((char*)NewValue,"%d", cnt_TempEnvironMidl_crash_eeprom);
           WriteChangeArcRecord(CHANGE_TEMP_ENV_PER_CRASH, source);
           strcpy((char*)ptr_out_buf, reply_command[COMMAND_OK]);
        }
        else
          strcpy((char*)ptr_out_buf, reply_command[COMMAND_INCORRECT_VALUE]); 
     }
  }
  ptr_TempDataParsingCommand->flag_end = _OK;
}
//QT style, auto-documenting with Doxygen, https://habr.com/ru/post/252101/ 
/*!
write and read long period error SGM module
\param[in] pointer string
\param[in] pointer output bufer
\param[in] pointer request
\param[in] source command
\param[in] temp data source command
\return void
*/
void PF_ERR_SGM_PER_LONG(char* parString, uint8_t *ptr_out_buf, uint8_t *ptr_str_reply, TypeSourceCommands source, TTempDataParsingCommand *ptr_TempDataParsingCommand)
{
  if(parString == 0)   
  {
      strcpy((char*)ptr_out_buf, (const char*)ptr_str_reply);
      sprintf((char*)(ptr_out_buf + strlen((char*)ptr_out_buf)),"=%d%s", cnt_error_SGM_long_eeprom, reply_OK);  
  }
  else
  {
     uint32_t temp;
     if(CheckChrString((uint8_t*)parString, "%d", strlen(parString)) == _FALSE) {strcpy((char*)ptr_out_buf, reply_command[COMMAND_INCORRECT_VALUE]); ptr_TempDataParsingCommand->flag_end = _OK; return;  }  

     temp = atoi(parString);
     
     if(CLB_lock != 0x66 && CLB_lock != 0x55 && StatusDevice.password_manufacture_enter == 0)
       strcpy((char*)ptr_out_buf, reply_command[COMMAND_ACCESS_DENIED]);   
     else
     {
        if(temp > 11 && temp < 2678401)
        {
           sprintf((char*)OldValue,"%d", cnt_error_SGM_long_eeprom);
           Eeprom_WriteDword((uint32_t)&cnt_error_SGM_long_eeprom, temp);
           sprintf((char*)NewValue,"%d", cnt_error_SGM_long_eeprom);
           WriteChangeArcRecord(CHANGE_ERR_SGM_PER_LONG, source);
           strcpy((char*)ptr_out_buf, reply_command[COMMAND_OK]);
        }
        else
          strcpy((char*)ptr_out_buf, reply_command[COMMAND_INCORRECT_VALUE]); 
     }
  }
  ptr_TempDataParsingCommand->flag_end = _OK;
}
//QT style, auto-documenting with Doxygen, https://habr.com/ru/post/252101/ 
/*!
write and read short period error SGM module
\param[in] pointer string
\param[in] pointer output bufer
\param[in] pointer request
\param[in] source command
\param[in] temp data source command
\return void
*/
void PF_ERR_SGM_PER_SHORT(char* parString, uint8_t *ptr_out_buf, uint8_t *ptr_str_reply, TypeSourceCommands source, TTempDataParsingCommand *ptr_TempDataParsingCommand)
{
  if(parString == 0)   
  {
      strcpy((char*)ptr_out_buf, (const char*)ptr_str_reply);
      sprintf((char*)(ptr_out_buf + strlen((char*)ptr_out_buf)),"=%d%s", cnt_error_SGM_short_eeprom, reply_OK);  
  }
  else
  {
     uint32_t temp;
     if(CheckChrString((uint8_t*)parString, "%d", strlen(parString)) == _FALSE) {strcpy((char*)ptr_out_buf, reply_command[COMMAND_INCORRECT_VALUE]); ptr_TempDataParsingCommand->flag_end = _OK; return;  }  

     temp = atoi(parString);
     
     if(CLB_lock != 0x66 && CLB_lock != 0x55 && StatusDevice.password_manufacture_enter == 0)
       strcpy((char*)ptr_out_buf, reply_command[COMMAND_ACCESS_DENIED]);   
     else
     {
        if(temp > 10 && temp < 2678401)
        {
           sprintf((char*)OldValue,"%d", cnt_error_SGM_short_eeprom);
           Eeprom_WriteDword((uint32_t)&cnt_error_SGM_short_eeprom, temp);
           sprintf((char*)NewValue,"%d", cnt_error_SGM_short_eeprom);
           WriteChangeArcRecord(CHANGE_ERR_SGM_PER_SHORT, source);
           strcpy((char*)ptr_out_buf, reply_command[COMMAND_OK]);
        }
        else
          strcpy((char*)ptr_out_buf, reply_command[COMMAND_INCORRECT_VALUE]); 
     }
  }
  ptr_TempDataParsingCommand->flag_end = _OK;
}
//QT style, auto-documenting with Doxygen, https://habr.com/ru/post/252101/ 
/*!
write and read fast period error SGM module
\param[in] pointer string
\param[in] pointer output bufer
\param[in] pointer request
\param[in] source command
\param[in] temp data source command
\return void
*/
void PF_ERR_SGM_PER_FAST(char* parString, uint8_t *ptr_out_buf, uint8_t *ptr_str_reply, TypeSourceCommands source, TTempDataParsingCommand *ptr_TempDataParsingCommand)
{
  if(parString == 0)   
  {
      strcpy((char*)ptr_out_buf, (const char*)ptr_str_reply);
      sprintf((char*)(ptr_out_buf + strlen((char*)ptr_out_buf)),"=%d%s", cnt_error_SGM_eeprom, reply_OK);  
  }
  else
  {
     uint32_t temp;
     if(CheckChrString((uint8_t*)parString, "%d", strlen(parString)) == _FALSE) {strcpy((char*)ptr_out_buf, reply_command[COMMAND_INCORRECT_VALUE]); ptr_TempDataParsingCommand->flag_end = _OK; return;  }  

     temp = atoi(parString);
     
     if(CLB_lock != 0x66 && CLB_lock != 0x55 && StatusDevice.password_manufacture_enter == 0)
       strcpy((char*)ptr_out_buf, reply_command[COMMAND_ACCESS_DENIED]);   
     else
     {
        if(temp > 10 && temp < 2678401)
        {
           sprintf((char*)OldValue,"%d", cnt_error_SGM_eeprom);
           Eeprom_WriteDword((uint32_t)&cnt_error_SGM_eeprom, temp);
           sprintf((char*)NewValue,"%d", cnt_error_SGM_eeprom);
           WriteChangeArcRecord(CHANGE_ERR_SGM_PER_FAST, source);
           strcpy((char*)ptr_out_buf, reply_command[COMMAND_OK]);
        }
        else
          strcpy((char*)ptr_out_buf, reply_command[COMMAND_INCORRECT_VALUE]); 
     }
  }
  ptr_TempDataParsingCommand->flag_end = _OK;
}
//QT style, auto-documenting with Doxygen, https://habr.com/ru/post/252101/ 
/*!
write and read counter error SGM module for long period
\param[in] pointer string
\param[in] pointer output bufer
\param[in] pointer request
\param[in] source command
\param[in] temp data source command
\return void
*/
void PF_ERR_SGM_CNT_LONG(char* parString, uint8_t *ptr_out_buf, uint8_t *ptr_str_reply, TypeSourceCommands source, TTempDataParsingCommand *ptr_TempDataParsingCommand)
{
  if(parString == 0)   
  {
      strcpy((char*)ptr_out_buf, (const char*)ptr_str_reply);
      sprintf((char*)(ptr_out_buf + strlen((char*)ptr_out_buf)),"=%d%s", SensCntErrorLong, reply_OK);  
  }
  else
  {
     uint32_t temp;
     if(CheckChrString((uint8_t*)parString, "%d", strlen(parString)) == _FALSE) {strcpy((char*)ptr_out_buf, reply_command[COMMAND_INCORRECT_VALUE]); ptr_TempDataParsingCommand->flag_end = _OK; return;  }  

     temp = atoi(parString);
     
     if(CLB_lock != 0x66 && CLB_lock != 0x55 && StatusDevice.password_manufacture_enter == 0)
       strcpy((char*)ptr_out_buf, reply_command[COMMAND_ACCESS_DENIED]);   
     else
     {
        if(temp <= GasMeter.SensCnt)
        {
           sprintf((char*)OldValue,"%d", SensCntErrorLong);
           Eeprom_WriteChar((uint32_t)&SensCntErrorLong, (uint8_t)temp);
           sprintf((char*)NewValue,"%d", SensCntErrorLong);
           WriteChangeArcRecord(CHANGE_ERR_SGM_CNT_LONG, source);
           strcpy((char*)ptr_out_buf, reply_command[COMMAND_OK]);
        }
        else
          strcpy((char*)ptr_out_buf, reply_command[COMMAND_INCORRECT_VALUE]); 
     }
  }
  ptr_TempDataParsingCommand->flag_end = _OK;
}
//QT style, auto-documenting with Doxygen, https://habr.com/ru/post/252101/ 
/*!
command when replacing counter batteries
\param[in] pointer string
\param[in] pointer output bufer
\param[in] pointer request
\param[in] source command
\param[in] temp data source command
\return void
*/
void PF_TEL_BAT_REPLACE(char* parString, uint8_t *ptr_out_buf, uint8_t *ptr_str_reply, TypeSourceCommands source, TTempDataParsingCommand *ptr_TempDataParsingCommand)
{
  if(parString == 0)   
  {
     strcpy((char*)ptr_out_buf, reply_command[COMMAND_WRITE_ONLY]);
  }
  else
  {
     uint32_t temp;
     if(CheckChrString((uint8_t*)parString, "%d", strlen(parString)) == _FALSE) {strcpy((char*)ptr_out_buf, reply_command[COMMAND_INCORRECT_VALUE]); ptr_TempDataParsingCommand->flag_end = _OK; return;  }  

     temp = atoi(parString);
     if(timeSystemUnix - battery_telemetry_installation_time > delay_time_BAT_REPLACE)
        strcpy((char*)ptr_out_buf, reply_command[COMMAND_ACCESS_DENIED]);
     else
     {
        if(temp == 1)
        {
           sprintf((char*)OldValue,"%f", residual_capacity_battery_modem_percent);
           MotoHoursToMils.time_workingGSM = 0;
           residual_capacity_battery_modem_percent = 100.0;
           sprintf((char*)NewValue,"%f", residual_capacity_battery_modem_percent);
           WriteChangeArcRecord(CHANGE_TEL_BAT_REPLACE, source);
           strcpy((char*)ptr_out_buf, reply_command[COMMAND_OK]);
        }
        else
          strcpy((char*)ptr_out_buf, reply_command[COMMAND_INCORRECT_VALUE]); 
     }
  }
  ptr_TempDataParsingCommand->flag_end = _OK;
}
//QT style, auto-documenting with Doxygen, https://habr.com/ru/post/252101/ 
/*!
command when replacing telemetry batteries
\param[in] pointer string
\param[in] pointer output bufer
\param[in] pointer request
\param[in] source command
\param[in] temp data source command
\return void
*/
void PF_COUNT_BAT_REPLACE(char* parString, uint8_t *ptr_out_buf, uint8_t *ptr_str_reply, TypeSourceCommands source, TTempDataParsingCommand *ptr_TempDataParsingCommand)
{
  if(parString == 0)   
  {
     strcpy((char*)ptr_out_buf, reply_command[COMMAND_WRITE_ONLY]);
  }
  else
  {
     uint32_t temp;
     if(CheckChrString((uint8_t*)parString, "%d", strlen(parString)) == _FALSE) {strcpy((char*)ptr_out_buf, reply_command[COMMAND_INCORRECT_VALUE]); ptr_TempDataParsingCommand->flag_end = _OK; return;  }  

     temp = atoi(parString);
     if(timeSystemUnix - battery_counter_installation_time > delay_time_BAT_REPLACE 
        || (CLB_lock != 0x66 && CLB_lock != 0x55 && StatusDevice.password_manufacture_enter == 0))
        strcpy((char*)ptr_out_buf, reply_command[COMMAND_ACCESS_DENIED]);
     else
     {
        if(temp == 1)
        {
           sprintf((char*)OldValue,"%f", residual_capacity_battery_main_percent);
           MotoHoursToMils.time_StopCPU = 0;  
           MotoHoursToMils.time_workingOPTIC = 0;
           MotoHoursToMils.time_workingSGM = 0;  
           MotoHoursToMils.time_workingLCD = 0;
           residual_capacity_battery_main_percent = 100.0;
           sprintf((char*)NewValue,"%f", residual_capacity_battery_main_percent);
           WriteChangeArcRecord(CHANGE_COUNT_BAT_REPLACE, source);
           strcpy((char*)ptr_out_buf, reply_command[COMMAND_OK]);
        }
        else
          strcpy((char*)ptr_out_buf, reply_command[COMMAND_INCORRECT_VALUE]); 
     }
  }
  ptr_TempDataParsingCommand->flag_end = _OK;
}
//QT style, auto-documenting with Doxygen, https://habr.com/ru/post/252101/ 
/*!
standby time after connecting batteries
\param[in] pointer string
\param[in] pointer output bufer
\param[in] pointer request
\param[in] source command
\param[in] temp data source command
\return void
*/
void PF_TIME_BAT_REPLACE(char* parString, uint8_t *ptr_out_buf, uint8_t *ptr_str_reply, TypeSourceCommands source, TTempDataParsingCommand *ptr_TempDataParsingCommand)
{
  if(parString == 0)   
  {
      strcpy((char*)ptr_out_buf, (const char*)ptr_str_reply);
      sprintf((char*)(ptr_out_buf + strlen((char*)ptr_out_buf)),"=%d%s", delay_time_BAT_REPLACE, reply_OK);  
  }
  else
  {
     uint32_t temp;
     if(CheckChrString((uint8_t*)parString, "%d", strlen(parString)) == _FALSE) {strcpy((char*)ptr_out_buf, reply_command[COMMAND_INCORRECT_VALUE]); ptr_TempDataParsingCommand->flag_end = _OK; return;  }  

     temp = atoi(parString);
     if(CLB_lock != 0x66 && CLB_lock != 0x55 && StatusDevice.password_manufacture_enter == 0)
        strcpy((char*)ptr_out_buf, reply_command[COMMAND_ACCESS_DENIED]);
     else
     {
        if(temp > 3599 && temp < 2678399)
        {
           sprintf((char*)OldValue,"%d", delay_time_BAT_REPLACE);
           Eeprom_WriteDword((uint32_t)&delay_time_BAT_REPLACE, temp);
           sprintf((char*)NewValue,"%d", delay_time_BAT_REPLACE);
           WriteChangeArcRecord(CHANGE_TIME_BAT_REPLACE, source);
           strcpy((char*)ptr_out_buf, reply_command[COMMAND_OK]);
        }
        else
          strcpy((char*)ptr_out_buf, reply_command[COMMAND_INCORRECT_VALUE]); 
     }
  }
  ptr_TempDataParsingCommand->flag_end = _OK;
}
//QT style, auto-documenting with Doxygen, https://habr.com/ru/post/252101/ 
/*!
reading current K factor
\param[in] pointer string
\param[in] pointer output bufer
\param[in] pointer request
\param[in] source command
\param[in] temp data source command
\return void
*/
void PF_KFACTOR(char* parString, uint8_t *ptr_out_buf, uint8_t *ptr_str_reply, TypeSourceCommands source, TTempDataParsingCommand *ptr_TempDataParsingCommand)
{
  if(parString == 0)   
  {
      strcpy((char*)ptr_out_buf, (const char*)ptr_str_reply);
      sprintf((char*)(ptr_out_buf + strlen((char*)ptr_out_buf)),"=%d\t%d\t%d\t%d\t%f\t%d%s", Sensor.KL[0], Sensor.KL[1], Sensor.KL[2], Sensor.KL[3], GasMeter.mid_GasRecond, _LastGasRecognitionTime,  reply_OK);  
  }
  else
  {
    if(CLB_lock != 0x66 && CLB_lock != 0x55 && StatusDevice.password_manufacture_enter == 0)
        strcpy((char*)ptr_out_buf, reply_command[COMMAND_READ_ONLY]);
    else
    {
        uint32_t temp;
     if(CheckChrString((uint8_t*)parString, "%d", strlen(parString)) == _FALSE) {strcpy((char*)ptr_out_buf, reply_command[COMMAND_INCORRECT_VALUE]); ptr_TempDataParsingCommand->flag_end = _OK; return;  }  

        temp = atoi(parString);
        if(temp < 65536)
        {
          kfactor_emulator = (uint16_t)temp;
          strcpy((char*)ptr_out_buf, reply_command[COMMAND_OK]);
        }
        else
          strcpy((char*)ptr_out_buf, reply_command[COMMAND_INCORRECT_VALUE]); 
    }
  }
  ptr_TempDataParsingCommand->flag_end = _OK;
}
//QT style, auto-documenting with Doxygen, https://habr.com/ru/post/252101/ 
/*!
WARNING_CLEAR
\param[in] pointer string
\param[in] pointer output bufer
\param[in] pointer request
\param[in] source command
\param[in] temp data source command
\return void
*/
void PF_WARNING_CLEAR(char* parString, uint8_t *ptr_out_buf, uint8_t *ptr_str_reply, TypeSourceCommands source, TTempDataParsingCommand *ptr_TempDataParsingCommand)
{
  if(parString == 0)   
  {
      strcpy((char*)ptr_out_buf, reply_command[COMMAND_WRITE_ONLY]);
  }
  else
  {
     uint32_t temp;
     if(CheckChrString((uint8_t*)parString, "%d", strlen(parString)) == _FALSE) {strcpy((char*)ptr_out_buf, reply_command[COMMAND_INCORRECT_VALUE]); ptr_TempDataParsingCommand->flag_end = _OK; return;  }  

     temp = atoi(parString);

        if(temp == 1)
        {
           WriteIntEventArcRecord(EVENT_INT_WARNING_CLEAR, EVENT_INT_BEGIN);          
           sprintf((char*)OldValue,"%04X,%016llX", StatusWarning.word, event_int_story);
           event_int_story &= ~EVENT_INT_WARNING_MASK;
           StatusWarning.word = 0;
           sprintf((char*)NewValue,"%04X,%016llX", StatusWarning.word, event_int_story);
           WriteChangeArcRecord(CHANGE_WARNING_CLEAR, source);
           strcpy((char*)ptr_out_buf, reply_command[COMMAND_OK]);
        }
        else
          strcpy((char*)ptr_out_buf, reply_command[COMMAND_INCORRECT_VALUE]); 
  }
  ptr_TempDataParsingCommand->flag_end = _OK;
}
//QT style, auto-documenting with Doxygen, https://habr.com/ru/post/252101/ 
/*!
ALARM_CLEAR
\param[in] pointer string
\param[in] pointer output bufer
\param[in] pointer request
\param[in] source command
\param[in] temp data source command
\return void
*/
void PF_ALARM_CLEAR(char* parString, uint8_t *ptr_out_buf, uint8_t *ptr_str_reply, TypeSourceCommands source, TTempDataParsingCommand *ptr_TempDataParsingCommand)
{
  if(parString == 0)   
  {
      strcpy((char*)ptr_out_buf, reply_command[COMMAND_WRITE_ONLY]);
  }
  else
  {
     uint32_t temp;
     if(CheckChrString((uint8_t*)parString, "%d", strlen(parString)) == _FALSE) {strcpy((char*)ptr_out_buf, reply_command[COMMAND_INCORRECT_VALUE]); ptr_TempDataParsingCommand->flag_end = _OK; return;  }  

     temp = atoi(parString);
        if(temp == 1)
        {
           WriteIntEventArcRecord(EVENT_INT_ALARM_CLEAR, EVENT_INT_BEGIN);          
           sprintf((char*)OldValue,"%04X,%016llX", StatusAlarm.word, event_int_story);
           event_int_story &= ~EVENT_INT_ALARM_MASK;
           StatusAlarm.word = 0;
           sprintf((char*)NewValue,"%04X,%016llX", StatusAlarm.word, event_int_story);
           WriteChangeArcRecord(CHANGE_ALARM_CLEAR, source);
           strcpy((char*)ptr_out_buf, reply_command[COMMAND_OK]);
        }
        else
          strcpy((char*)ptr_out_buf, reply_command[COMMAND_INCORRECT_VALUE]); 
  }
  ptr_TempDataParsingCommand->flag_end = _OK;
}
//QT style, auto-documenting with Doxygen, https://habr.com/ru/post/252101/ 
/*!
CRASH_CLEAR
\param[in] pointer string
\param[in] pointer output bufer
\param[in] pointer request
\param[in] source command
\param[in] temp data source command
\return void
*/
void PF_CRASH_CLEAR(char* parString, uint8_t *ptr_out_buf, uint8_t *ptr_str_reply, TypeSourceCommands source, TTempDataParsingCommand *ptr_TempDataParsingCommand)
{
  if(parString == 0)   
  {
      strcpy((char*)ptr_out_buf, reply_command[COMMAND_WRITE_ONLY]);
  }
  else
  {
     uint32_t temp;
     if(CheckChrString((uint8_t*)parString, "%d", strlen(parString)) == _FALSE) {strcpy((char*)ptr_out_buf, reply_command[COMMAND_INCORRECT_VALUE]); ptr_TempDataParsingCommand->flag_end = _OK; return;  }  

     temp = atoi(parString);
     if(CLB_lock != 0x66 && CLB_lock != 0x55 && StatusDevice.password_manufacture_enter == 0)
        strcpy((char*)ptr_out_buf, reply_command[COMMAND_ACCESS_DENIED]);
     else
     {
        if(temp == 1)
        {
           WriteIntEventArcRecord(EVENT_INT_CRASH_CLEAR, EVENT_INT_BEGIN);          
           sprintf((char*)OldValue,"%04X,%016llX", StatusCrash.word, event_int_story);
           StatusCrash.word = 0;
           event_int_story = 0;
           Eeprom_WriteQword((uint32_t)&event_int_story_old, event_int_story);
         //  CLB_lock = 0;
         //  Eeprom_WriteChar((uint32_t)&CLB_lock_eeprom, CLB_lock);
           sprintf((char*)NewValue,"%04X,%016llX", StatusCrash.word, event_int_story);
           WriteChangeArcRecord(CHANGE_CRASH_CLEAR, source);
           strcpy((char*)ptr_out_buf, reply_command[COMMAND_OK]);
        }
        else
          strcpy((char*)ptr_out_buf, reply_command[COMMAND_INCORRECT_VALUE]); 
     }
  }
  ptr_TempDataParsingCommand->flag_end = _OK;
}
//QT style, auto-documenting with Doxygen, https://habr.com/ru/post/252101/ 
/*!
VER_PO
\param[in] pointer string
\param[in] pointer output bufer
\param[in] pointer request
\param[in] source command
\param[in] temp data source command
\return void
*/
void PF_VER_PO(char* parString, uint8_t *ptr_out_buf, uint8_t *ptr_str_reply, TypeSourceCommands source, TTempDataParsingCommand *ptr_TempDataParsingCommand)
{
  if(parString == 0)   
  {
      strcpy((char*)ptr_out_buf, (const char*)ptr_str_reply);
      sprintf((char*)(ptr_out_buf + strlen((char*)ptr_out_buf)),"=%d%s", show_ver_po_eeprom,  reply_OK);  
  }
  else
  {
     uint32_t temp;
     if(CheckChrString((uint8_t*)parString, "%d", strlen(parString)) == _FALSE) {strcpy((char*)ptr_out_buf, reply_command[COMMAND_INCORRECT_VALUE]); ptr_TempDataParsingCommand->flag_end = _OK; return;  }  
     temp = atoi(parString);
     if(temp == 1 || temp == 0)
     {
           sprintf((char*)OldValue,"%d", show_ver_po_eeprom);
           Eeprom_WriteChar((uint32_t)&show_ver_po_eeprom, temp);
           sprintf((char*)NewValue,"%d", show_ver_po_eeprom);
           WriteChangeArcRecord(CHANGE_SHOW_VER_PO, source);
           strcpy((char*)ptr_out_buf, reply_command[COMMAND_OK]);
     }
     else
       strcpy((char*)ptr_out_buf, reply_command[COMMAND_INCORRECT_VALUE]); 
  }
  ptr_TempDataParsingCommand->flag_end = _OK;
}
//QT style, auto-documenting with Doxygen, https://habr.com/ru/post/252101/ 
/*!
VALVE_SET
\param[in] pointer string
\param[in] pointer output bufer
\param[in] pointer request
\param[in] source command
\param[in] temp data source command
\return void
*/
void PF_VALVE_SET(char* parString, uint8_t *ptr_out_buf, uint8_t *ptr_str_reply, TypeSourceCommands source, TTempDataParsingCommand *ptr_TempDataParsingCommand)
{
  if(parString == 0)   
  {
      strcpy((char*)ptr_out_buf, (const char*)ptr_str_reply);
      sprintf((char*)(ptr_out_buf + strlen((char*)ptr_out_buf)),"=%d%s", valve_presence_eeprom,  reply_OK);  
  }
  else
  {
     uint32_t temp;
     if(CheckChrString((uint8_t*)parString, "%d", strlen(parString)) == _FALSE) {strcpy((char*)ptr_out_buf, reply_command[COMMAND_INCORRECT_VALUE]); ptr_TempDataParsingCommand->flag_end = _OK; return;  }  
     temp = atoi(parString);
     if(CLB_lock != 0x66 && CLB_lock != 0x55 && StatusDevice.password_manufacture_enter == 0)
        strcpy((char*)ptr_out_buf, reply_command[COMMAND_ACCESS_DENIED]);
     else
     {
        if(temp == 1 || temp == 0)
        {
           sprintf((char*)OldValue,"%d", valve_presence_eeprom);
           EEPROM_Write_Byte_FrameSemafor((uint32_t)&valve_presence_eeprom, (uint8_t*)&temp, sizeof(valve_presence_eeprom), 10000);
           sprintf((char*)NewValue,"%d", valve_presence_eeprom);
           WriteChangeArcRecord(CHANGE_VALVE_PRESENCE, source);            
           strcpy((char*)ptr_out_buf, reply_command[COMMAND_OK]);
        }
        else
          strcpy((char*)ptr_out_buf, reply_command[COMMAND_INCORRECT_VALUE]); 
     }
  }
  ptr_TempDataParsingCommand->flag_end = _OK;
}
//QT style, auto-documenting with Doxygen, https://habr.com/ru/post/252101/ 
/*!
VALVE
\param[in] pointer string
\param[in] pointer output bufer
\param[in] pointer request
\param[in] source command
\param[in] temp data source command
\return void
*/
void PF_VALVE(char* parString, uint8_t *ptr_out_buf, uint8_t *ptr_str_reply, TypeSourceCommands source, TTempDataParsingCommand *ptr_TempDataParsingCommand)
{
  if(parString == 0)   
  {
      strcpy((char*)ptr_out_buf, (const char*)ptr_str_reply);
      sprintf((char*)(ptr_out_buf + strlen((char*)ptr_out_buf)),"=%d%s", ValveGetStatus(),  reply_OK);  
  }
  else
  {
     ChangeValve(parString, ptr_out_buf, ptr_str_reply, source, ptr_TempDataParsingCommand, VALVE_ACTION_OPEN_TCP);
  }
  ptr_TempDataParsingCommand->flag_end = _OK;
}
//QT style, auto-documenting with Doxygen, https://habr.com/ru/post/252101/ 
/*!
FORCE_VALVE
\param[in] pointer string
\param[in] pointer output bufer
\param[in] pointer request
\param[in] source command
\param[in] temp data source command
\return void
*/
void PF_FORCE_VALVE(char* parString, uint8_t *ptr_out_buf, uint8_t *ptr_str_reply, TypeSourceCommands source, TTempDataParsingCommand *ptr_TempDataParsingCommand)
{
  if(parString == 0)   
  {
      strcpy((char*)ptr_out_buf, (const char*)ptr_str_reply);
      sprintf((char*)(ptr_out_buf + strlen((char*)ptr_out_buf)),"=%d%s", ValveGetStatus(),  reply_OK);  
  }
  else
  {
     ChangeValve(parString, ptr_out_buf, ptr_str_reply, source, ptr_TempDataParsingCommand, VALVE_ACTION_OPEN_FORCED_TCP);
  }
  ptr_TempDataParsingCommand->flag_end = _OK;
}

void ChangeValve(char* parString, uint8_t *ptr_out_buf, uint8_t *ptr_str_reply, TypeSourceCommands source, TTempDataParsingCommand *ptr_TempDataParsingCommand, TypeValveAction action)
{
    // action == VALVE_ACTION_OPEN_TCP
    // action == VALVE_ACTION_OPEN_FORCED_TCP
     uint32_t temp;
     if(CheckChrString((uint8_t*)parString, "%d", strlen(parString)) == _FALSE) {strcpy((char*)ptr_out_buf, reply_command[COMMAND_INCORRECT_VALUE]); ptr_TempDataParsingCommand->flag_end = _OK; return;  }  
     temp = atoi(parString);
     if(temp < 2)
     {
          uint8_t rez = _FALSE;
          if(source != SOURCE_TCP)
          {          
             switch(ValveGetStatus())
             {
             case VALVE_CLOSE_TCP: 
             case VALVE_CLOSE_REVERSE_FLOW:          
             case VALVE_CLOSE_Q_MAX:
             case VALVE_CLOSE_RF_SENSOR:
             case VALVE_CLOSE_BATARY:          
             case VALVE_CLOSE_NON_SIM_CARD: 
             case VALVE_CLOSE_OPEN_BODY:      
             case VALVE_CLOSE_OPEN_BAT_BODY:   
             case VALVE_CLOSE_DISCHARGE_BATARY:             
                  strcpy((char*)ptr_out_buf, reply_command[COMMAND_ACCESS_DENIED]);
                  break;
             default:
                  if(valve_resolution_from_TCP_eeprom == _FALSE)
                    strcpy((char*)ptr_out_buf, reply_command[COMMAND_ACCESS_DENIED]);
                  else
                  {
                     if(!temp)
                        ValveSetAction(VALVE_ACTION_CLOSE_OPTIC);
                     else
                        ValveSetAction((TypeValveAction)((uint8_t)action + 1)); // VALVE_ACTION_OPEN_FORCED_OPTIC or VALVE_ACTION_OPEN_OPTIC
                     strcpy((char*)ptr_out_buf, reply_command[COMMAND_OK]);
                     rez = _OK;
                  }
                  break;
             }
          }
          else
          {
                if(temp == 1)
                {
                   if(source_start_modem == SOURCE_START_MDM_BUTTON
                      && !((StatusWarning.bit.residual_capacity_battery_modem_min10 
                            || StatusWarning.bit.residual_capacity_battery_main_min10
                            || StatusWarning.bit.battery_cover_open
                            || ((event_int_story & EVENT_INT_OPEN_CASE) && StatusCrashEnable.bit.body_cover_open)) && valve_enable_auto_control_eeprom == 1))
                   {
                      uint8_t t = _OK;
                      EEPROM_Write_Byte_FrameSemafor((uint32_t)&valve_resolution_from_TCP_eeprom, (uint8_t*)&t, sizeof(valve_resolution_from_TCP_eeprom), 10000);
                      ValveSetAction(action); // VALVE_ACTION_OPEN_FORCED_TCP  or VALVE_ACTION_OPEN_TCP
                      strcpy((char*)ptr_out_buf, reply_command[COMMAND_OK]);
                      rez = _OK;
                   }
                   else
                      strcpy((char*)ptr_out_buf, reply_command[COMMAND_ACCESS_DENIED]);
                }
                else
                {
                   ValveSetAction(VALVE_ACTION_CLOSE_TCP); 
                   strcpy((char*)ptr_out_buf, reply_command[COMMAND_OK]);
                   rez = _OK;
                }
          }
          if(rez == _OK)
          {
             sprintf((char*)OldValue,"%d", ValveGetStatus());
             sprintf((char*)NewValue,"%d", temp);
             WriteChangeArcRecord(CHANGE_VALVE, source);
          }
     }
     else
       strcpy((char*)ptr_out_buf, reply_command[COMMAND_INCORRECT_VALUE]);   
}
//QT style, auto-documenting with Doxygen, https://habr.com/ru/post/252101/ 
/*!
VALVE_TIME
\param[in] pointer string
\param[in] pointer output bufer
\param[in] pointer request
\param[in] source command
\param[in] temp data source command
\return void
*/
void PF_VALVE_TIME(char* parString, uint8_t *ptr_out_buf, uint8_t *ptr_str_reply, TypeSourceCommands source, TTempDataParsingCommand *ptr_TempDataParsingCommand)
{
  if(parString == 0)   
  {
      strcpy((char*)ptr_out_buf, (const char*)ptr_str_reply);
      sprintf((char*)(ptr_out_buf + strlen((char*)ptr_out_buf)),"=%d;%d;%d%s", 
              valve_time_open_slightly_eeprom + valve_time_open_additionally_eeprom, valve_time_close_eeprom,  valve_time_open_slightly_eeprom, reply_OK);  
  }
  else
  {
     uint32_t temp1, temp2, temp3;
     if(CLB_lock != 0x66 && CLB_lock != 0x55 && StatusDevice.password_manufacture_enter == 0)
       strcpy((char*)ptr_out_buf, reply_command[COMMAND_ACCESS_DENIED]);
     else
     {
        if(CheckChrString((uint8_t*)parString, "%d;", strlen(parString)) == _FALSE) {strcpy((char*)ptr_out_buf, reply_command[COMMAND_INCORRECT_VALUE]); ptr_TempDataParsingCommand->flag_end = _OK; return;  }  
        sscanf((char*)parString, "%d;%d;%d", &temp1, &temp2, &temp3);
        // ������ ��������, ������ ��������, �����������
        if(temp1 > 999 && temp1 < 20001
           && temp2 > 999 && temp2 < 20001
           && temp3 > 99 && temp3 < 20001
           && temp3 < temp1)
        {
           temp1 = temp1 - temp3;
           sprintf((char*)OldValue,"%d,%d,%d",  valve_time_open_slightly_eeprom + valve_time_open_additionally_eeprom, valve_time_close_eeprom,  valve_time_open_slightly_eeprom);
           EEPROM_Write_Byte_FrameSemafor((uint32_t)&valve_time_open_additionally_eeprom, (uint8_t*)&temp1, sizeof(valve_time_open_additionally_eeprom), 10000);
           EEPROM_Write_Byte_FrameSemafor((uint32_t)&valve_time_close_eeprom, (uint8_t*)&temp2, sizeof(valve_time_close_eeprom), 10000);
           EEPROM_Write_Byte_FrameSemafor((uint32_t)&valve_time_open_slightly_eeprom, (uint8_t*)&temp3, sizeof(valve_time_open_slightly_eeprom), 10000);
           sprintf((char*)NewValue,"%d,%d,%d",  valve_time_open_slightly_eeprom + valve_time_open_additionally_eeprom, valve_time_close_eeprom,  valve_time_open_slightly_eeprom);
           WriteChangeArcRecord(CHANGE_VALVE_TIME, source);            
           strcpy((char*)ptr_out_buf, reply_command[COMMAND_OK]);           
        }
        else
          strcpy((char*)ptr_out_buf, reply_command[COMMAND_INCORRECT_VALUE]); 
     }
  }
  ptr_TempDataParsingCommand->flag_end = _OK;
}
//QT style, auto-documenting with Doxygen, https://habr.com/ru/post/252101/ 
/*!
VALVE_DELAY_BEFORE_OPEN
\param[in] pointer string
\param[in] pointer output bufer
\param[in] pointer request
\param[in] source command
\param[in] temp data source command
\return void
*/
void PF_VALVE_DELAY_BEFORE_OPEN(char* parString, uint8_t *ptr_out_buf, uint8_t *ptr_str_reply, TypeSourceCommands source, TTempDataParsingCommand *ptr_TempDataParsingCommand)
{
  if(parString == 0)   
  {
      strcpy((char*)ptr_out_buf, (const char*)ptr_str_reply);
      sprintf((char*)(ptr_out_buf + strlen((char*)ptr_out_buf)),"=%d%s", 
              valve_delay_before_check_flow_eeprom, reply_OK);  
  }
  else
  {
     uint32_t temp1;
   //  if(CLB_lock != 0x66 && CLB_lock != 0x55 && StatusDevice.password_manufacture_enter == 0)
   //    strcpy((char*)ptr_out_buf, reply_command[COMMAND_ACCESS_DENIED]);
   //  else
   //  {
        if(CheckChrString((uint8_t*)parString, "%d", strlen(parString)) == _FALSE) {strcpy((char*)ptr_out_buf, reply_command[COMMAND_INCORRECT_VALUE]); ptr_TempDataParsingCommand->flag_end = _OK; return;  }  
        temp1 = atoi(parString);
        if(temp1 > 2 && temp1 < 300)
        {
           sprintf((char*)OldValue,"%d",  valve_delay_before_check_flow_eeprom);
           EEPROM_Write_Byte_FrameSemafor((uint32_t)&valve_delay_before_check_flow_eeprom, (uint8_t*)&temp1, sizeof(valve_delay_before_check_flow_eeprom), 10000);
           sprintf((char*)NewValue,"%d",  valve_delay_before_check_flow_eeprom);
           WriteChangeArcRecord(CHANGE_VALVE_DELAY_BEFORE_OPEN, source);            
           strcpy((char*)ptr_out_buf, reply_command[COMMAND_OK]);           
        }
        else
          strcpy((char*)ptr_out_buf, reply_command[COMMAND_INCORRECT_VALUE]); 
   //  }
  }
  ptr_TempDataParsingCommand->flag_end = _OK;
}
//QT style, auto-documenting with Doxygen, https://habr.com/ru/post/252101/ 
/*!
CHECK_OPEN_TIME
\param[in] pointer string
\param[in] pointer output bufer
\param[in] pointer request
\param[in] source command
\param[in] temp data source command
\return void
*/
void PF_CHECK_OPEN_TIME(char* parString, uint8_t *ptr_out_buf, uint8_t *ptr_str_reply, TypeSourceCommands source, TTempDataParsingCommand *ptr_TempDataParsingCommand)
{
  if(parString == 0)   
  {
      strcpy((char*)ptr_out_buf, (const char*)ptr_str_reply);
      sprintf((char*)(ptr_out_buf + strlen((char*)ptr_out_buf)),"=%d%s", 
              valve_time_check_flow_eeprom, reply_OK);  
  }
  else
  {
     uint32_t temp1;
     if(CLB_lock != 0x66 && CLB_lock != 0x55 && StatusDevice.password_manufacture_enter == 0)
       strcpy((char*)ptr_out_buf, reply_command[COMMAND_ACCESS_DENIED]);
     else
     {
        if(CheckChrString((uint8_t*)parString, "%d", strlen(parString)) == _FALSE) {strcpy((char*)ptr_out_buf, reply_command[COMMAND_INCORRECT_VALUE]); ptr_TempDataParsingCommand->flag_end = _OK; return;  }  
        temp1 = atoi(parString);
        if(temp1 > 1 && temp1 < 601)
        {
           sprintf((char*)OldValue,"%d",  valve_time_check_flow_eeprom);
           EEPROM_Write_Byte_FrameSemafor((uint32_t)&valve_time_check_flow_eeprom, (uint8_t*)&temp1, sizeof(valve_time_check_flow_eeprom), 10000);
           sprintf((char*)NewValue,"%d",  valve_time_check_flow_eeprom);
           WriteChangeArcRecord(CHANGE_CHECK_OPEN_TIME, source);            
           strcpy((char*)ptr_out_buf, reply_command[COMMAND_OK]);           
        }
        else
          strcpy((char*)ptr_out_buf, reply_command[COMMAND_INCORRECT_VALUE]); 
     }
  }
  ptr_TempDataParsingCommand->flag_end = _OK;
}
//QT style, auto-documenting with Doxygen, https://habr.com/ru/post/252101/ 
/*!
VALVE_QMIN
\param[in] pointer string
\param[in] pointer output bufer
\param[in] pointer request
\param[in] source command
\param[in] temp data source command
\return void
*/
void PF_VALVE_QMIN(char* parString, uint8_t *ptr_out_buf, uint8_t *ptr_str_reply, TypeSourceCommands source, TTempDataParsingCommand *ptr_TempDataParsingCommand)
{
  if(parString == 0)   
  {
      strcpy((char*)ptr_out_buf, (const char*)ptr_str_reply);
      sprintf((char*)(ptr_out_buf + strlen((char*)ptr_out_buf)),"=%.2f%s", 
              valve_check_flow_multiplier_up_eeprom, reply_OK);  
  }
  else
  {
     float temp1;
     if(CLB_lock != 0x66 && CLB_lock != 0x55 && StatusDevice.password_manufacture_enter == 0)
       strcpy((char*)ptr_out_buf, reply_command[COMMAND_ACCESS_DENIED]);
     else
     {
        if(CheckChrString((uint8_t*)parString, "%d.", strlen(parString)) == _FALSE) {strcpy((char*)ptr_out_buf, reply_command[COMMAND_INCORRECT_VALUE]); ptr_TempDataParsingCommand->flag_end = _OK; return;  }  
        temp1 = atof(parString);
        if(temp1 > valve_check_flow_multiplier_down_eeprom && temp1 < 1.5)
        {
           sprintf((char*)OldValue,"%.2f",  valve_check_flow_multiplier_up_eeprom);
           EEPROM_Write_Byte_FrameSemafor((uint32_t)&valve_check_flow_multiplier_up_eeprom, (uint8_t*)&temp1, sizeof(valve_check_flow_multiplier_up_eeprom), 10000);
           sprintf((char*)NewValue,"%.2f",  valve_check_flow_multiplier_up_eeprom);
           WriteChangeArcRecord(CHANGE_VALVE_Q_MIN, source);            
           strcpy((char*)ptr_out_buf, reply_command[COMMAND_OK]);           
        }
        else
          strcpy((char*)ptr_out_buf, reply_command[COMMAND_INCORRECT_VALUE]); 
     }
  }
  ptr_TempDataParsingCommand->flag_end = _OK;
}
//QT style, auto-documenting with Doxygen, https://habr.com/ru/post/252101/ 
/*!
VALVE_MIN
\param[in] pointer string
\param[in] pointer output bufer
\param[in] pointer request
\param[in] source command
\param[in] temp data source command
\return void
*/
void PF_VALVE_MIN(char* parString, uint8_t *ptr_out_buf, uint8_t *ptr_str_reply, TypeSourceCommands source, TTempDataParsingCommand *ptr_TempDataParsingCommand)
{
  if(parString == 0)   
  {
      strcpy((char*)ptr_out_buf, (const char*)ptr_str_reply);
      sprintf((char*)(ptr_out_buf + strlen((char*)ptr_out_buf)),"=%.6f%s", 
              valve_check_flow_multiplier_down_eeprom, reply_OK);  
  }
  else
  {
     float temp1;
     if(CLB_lock != 0x66 && CLB_lock != 0x55 && StatusDevice.password_manufacture_enter == 0)
       strcpy((char*)ptr_out_buf, reply_command[COMMAND_ACCESS_DENIED]);
     else
     {
        if(CheckChrString((uint8_t*)parString, "%d.", strlen(parString)) == _FALSE) {strcpy((char*)ptr_out_buf, reply_command[COMMAND_INCORRECT_VALUE]); ptr_TempDataParsingCommand->flag_end = _OK; return;  }  
        temp1 = atof(parString);
        if(temp1 > 0.000099 && temp1 < 1.0001)
        {
           sprintf((char*)OldValue,"%.6f",  valve_check_flow_multiplier_down_eeprom);
           EEPROM_Write_Byte_FrameSemafor((uint32_t)&valve_check_flow_multiplier_down_eeprom, (uint8_t*)&temp1, sizeof(valve_check_flow_multiplier_down_eeprom), 10000);
           sprintf((char*)NewValue,"%.6f",  valve_check_flow_multiplier_down_eeprom);
           WriteChangeArcRecord(CHANGE_VALVE_MIN, source);            
           strcpy((char*)ptr_out_buf, reply_command[COMMAND_OK]);           
        }
        else
          strcpy((char*)ptr_out_buf, reply_command[COMMAND_INCORRECT_VALUE]); 
     }
  }
  ptr_TempDataParsingCommand->flag_end = _OK;
}
//QT style, auto-documenting with Doxygen, https://habr.com/ru/post/252101/ 
/*!
CNT_VALVE_MIN
\param[in] pointer string
\param[in] pointer output bufer
\param[in] pointer request
\param[in] source command
\param[in] temp data source command
\return void
*/
void PF_CNT_VALVE_MIN(char* parString, uint8_t *ptr_out_buf, uint8_t *ptr_str_reply, TypeSourceCommands source, TTempDataParsingCommand *ptr_TempDataParsingCommand)
{
  if(parString == 0)   
  {
      strcpy((char*)ptr_out_buf, (const char*)ptr_str_reply);
      sprintf((char*)(ptr_out_buf + strlen((char*)ptr_out_buf)),"=%d%s", 
              valve_cnt_min_eeprom, reply_OK);  
  }
  else
  {
     uint32_t temp1;
     if(CLB_lock != 0x66 && CLB_lock != 0x55 && StatusDevice.password_manufacture_enter == 0)
       strcpy((char*)ptr_out_buf, reply_command[COMMAND_ACCESS_DENIED]);
     else
     {
        if(CheckChrString((uint8_t*)parString, "%d", strlen(parString)) == _FALSE) {strcpy((char*)ptr_out_buf, reply_command[COMMAND_INCORRECT_VALUE]); ptr_TempDataParsingCommand->flag_end = _OK; return;  }  
        temp1 = atoi(parString);
        if(temp1 < 256)
        {
           sprintf((char*)OldValue,"%d",  valve_cnt_min_eeprom);
           EEPROM_Write_Byte_FrameSemafor((uint32_t)&valve_cnt_min_eeprom, (uint8_t*)&temp1, sizeof(valve_cnt_min_eeprom), 10000);
           sprintf((char*)NewValue,"%d",  valve_cnt_min_eeprom);
           WriteChangeArcRecord(CHANGE_CNT_VALVE_MIN, source);            
           strcpy((char*)ptr_out_buf, reply_command[COMMAND_OK]);           
        }
        else
          strcpy((char*)ptr_out_buf, reply_command[COMMAND_INCORRECT_VALUE]); 
     }
  }
  ptr_TempDataParsingCommand->flag_end = _OK;
}
//QT style, auto-documenting with Doxygen, https://habr.com/ru/post/252101/ 
/*!
VALVE_AUTO_CONTROL
\param[in] pointer string
\param[in] pointer output bufer
\param[in] pointer request
\param[in] source command
\param[in] temp data source command
\return void
*/
void PF_VALVE_AUTO_CONTROL(char* parString, uint8_t *ptr_out_buf, uint8_t *ptr_str_reply, TypeSourceCommands source, TTempDataParsingCommand *ptr_TempDataParsingCommand)
{
  if(parString == 0)   
  {
      strcpy((char*)ptr_out_buf, (const char*)ptr_str_reply);
      sprintf((char*)(ptr_out_buf + strlen((char*)ptr_out_buf)),"=%d%s", 
              valve_enable_auto_control_eeprom, reply_OK);  
  }
  else
  {
     uint32_t temp1;
    // if(CLB_lock != 0x66 && CLB_lock != 0x55 && StatusDevice.password_manufacture_enter == 0)
     if(source != SOURCE_TCP)
       strcpy((char*)ptr_out_buf, reply_command[COMMAND_ACCESS_DENIED]);
     else
     {
        if(CheckChrString((uint8_t*)parString, "%d", strlen(parString)) == _FALSE) {strcpy((char*)ptr_out_buf, reply_command[COMMAND_INCORRECT_VALUE]); ptr_TempDataParsingCommand->flag_end = _OK; return;  }  
        temp1 = atoi(parString);
        if(temp1 == 1 || temp1 == 0)
        {
           sprintf((char*)OldValue,"%d",  valve_enable_auto_control_eeprom);
           EEPROM_Write_Byte_FrameSemafor((uint32_t)&valve_enable_auto_control_eeprom, (uint8_t*)&temp1, sizeof(valve_enable_auto_control_eeprom), 10000);
           sprintf((char*)NewValue,"%d",  valve_enable_auto_control_eeprom);
           WriteChangeArcRecord(CHANGE_VALVE_AUTO_CONTROL, source);            
           strcpy((char*)ptr_out_buf, reply_command[COMMAND_OK]);         
           
           valve_going_beyond_Q_volue = sensorTypeParameters[_SgmType].Q_max * ((float)valve_going_beyond_Q_period_sec / 3600.0) * Q_max_multiplier_up + GasMeter.VE_Lm3;
           valve_going_beyond_Q_nexTime = timeSystemUnix + valve_going_beyond_Q_period_sec + KalmanCnt * 2;
        }
        else
          strcpy((char*)ptr_out_buf, reply_command[COMMAND_INCORRECT_VALUE]); 
     }
  }
  ptr_TempDataParsingCommand->flag_end = _OK;
}
//QT style, auto-documenting with Doxygen, https://habr.com/ru/post/252101/ 
/*!
VALVE_CLOSE_REV_P
\param[in] pointer string
\param[in] pointer output bufer
\param[in] pointer request
\param[in] source command
\param[in] temp data source command
\return void
*/
void PF_VALVE_CLOSE_REV_P(char* parString, uint8_t *ptr_out_buf, uint8_t *ptr_str_reply, TypeSourceCommands source, TTempDataParsingCommand *ptr_TempDataParsingCommand)
{
  if(parString == 0)   
  {
      strcpy((char*)ptr_out_buf, (const char*)ptr_str_reply);
      sprintf((char*)(ptr_out_buf + strlen((char*)ptr_out_buf)),"=%d%s", 
              valve_revers_flow_period_eeprom, reply_OK);  
  }
  else
  {
     uint32_t temp1;
  //   if(CLB_lock != 0x66 && CLB_lock != 0x55 && StatusDevice.password_manufacture_enter == 0)
  //     strcpy((char*)ptr_out_buf, reply_command[COMMAND_ACCESS_DENIED]);
  //   else
  //   {
        if(CheckChrString((uint8_t*)parString, "%d", strlen(parString)) == _FALSE) {strcpy((char*)ptr_out_buf, reply_command[COMMAND_INCORRECT_VALUE]); ptr_TempDataParsingCommand->flag_end = _OK; return;  }  
        temp1 = atoi(parString);
        if(temp1 > 1 && temp1 < 1001)
        {
           sprintf((char*)OldValue,"%d",  valve_revers_flow_period_eeprom);
           EEPROM_Write_Byte_FrameSemafor((uint32_t)&valve_revers_flow_period_eeprom, (uint8_t*)&temp1, sizeof(valve_revers_flow_period_eeprom), 10000);
           sprintf((char*)NewValue,"%d",  valve_revers_flow_period_eeprom);
           WriteChangeArcRecord(CHANGE_VALVE_CLOSE_REV_P, source);            
           strcpy((char*)ptr_out_buf, reply_command[COMMAND_OK]);           
        }
        else
          strcpy((char*)ptr_out_buf, reply_command[COMMAND_INCORRECT_VALUE]); 
   //  }
  }
  ptr_TempDataParsingCommand->flag_end = _OK;
}
//QT style, auto-documenting with Doxygen, https://habr.com/ru/post/252101/ 
/*!
VALVE_CLOSE_Q_PH
\param[in] pointer string
\param[in] pointer output bufer
\param[in] pointer request
\param[in] source command
\param[in] temp data source command
\return void
*/
void PF_VALVE_CLOSE_Q_PH(char* parString, uint8_t *ptr_out_buf, uint8_t *ptr_str_reply, TypeSourceCommands source, TTempDataParsingCommand *ptr_TempDataParsingCommand)
{
  if(parString == 0)   
  {
      strcpy((char*)ptr_out_buf, (const char*)ptr_str_reply);
      sprintf((char*)(ptr_out_buf + strlen((char*)ptr_out_buf)),"=%d%s", 
              valve_going_beyond_Q_period_sec, reply_OK);  
  }
  else
  {
     uint32_t temp1;
  //   if(CLB_lock != 0x66 && CLB_lock != 0x55 && StatusDevice.password_manufacture_enter == 0)
  //     strcpy((char*)ptr_out_buf, reply_command[COMMAND_ACCESS_DENIED]);
  //   else
  //   {
        if(CheckChrString((uint8_t*)parString, "%d", strlen(parString)) == _FALSE) {strcpy((char*)ptr_out_buf, reply_command[COMMAND_INCORRECT_VALUE]); ptr_TempDataParsingCommand->flag_end = _OK; return;  }  
        temp1 = atoi(parString);
        if(temp1 > 1 && temp1 < 2678401)
        {
           sprintf((char*)OldValue,"%d",  valve_going_beyond_Q_period_sec);
           EEPROM_Write_Byte_FrameSemafor((uint32_t)&valve_going_beyond_Q_period_sec, (uint8_t*)&temp1, sizeof(valve_going_beyond_Q_period_sec), 10000);
           sprintf((char*)NewValue,"%d",  valve_going_beyond_Q_period_sec);
           WriteChangeArcRecord(CHANGE_VALVE_CLOSE_Q_PH, source);            
           strcpy((char*)ptr_out_buf, reply_command[COMMAND_OK]); 
           
           valve_going_beyond_Q_volue = sensorTypeParameters[_SgmType].Q_max * ((float)valve_going_beyond_Q_period_sec / 3600.0) * Q_max_multiplier_up + GasMeter.VE_Lm3;
           valve_going_beyond_Q_nexTime = timeSystemUnix + valve_going_beyond_Q_period_sec + KalmanCnt * 2;
        }
        else
          strcpy((char*)ptr_out_buf, reply_command[COMMAND_INCORRECT_VALUE]); 
   //  }
  }
  ptr_TempDataParsingCommand->flag_end = _OK;
}
//QT style, auto-documenting with Doxygen, https://habr.com/ru/post/252101/ 
/*!
EN_MDM_CHNG_VLV
\param[in] pointer string
\param[in] pointer output bufer
\param[in] pointer request
\param[in] source command
\param[in] temp data source command
\return void
*/
void PF_EN_MDM_CHNG_VLV(char* parString, uint8_t *ptr_out_buf, uint8_t *ptr_str_reply, TypeSourceCommands source, TTempDataParsingCommand *ptr_TempDataParsingCommand)
{
  if(parString == 0)   
  {
      strcpy((char*)ptr_out_buf, (const char*)ptr_str_reply);
      sprintf((char*)(ptr_out_buf + strlen((char*)ptr_out_buf)),"=%d%s", 
              enable_MDM_change_valve, reply_OK);  
  }
  else
  {
     uint32_t temp1;
  //   if(CLB_lock != 0x66 && CLB_lock != 0x55 && StatusDevice.password_manufacture_enter == 0)
  //     strcpy((char*)ptr_out_buf, reply_command[COMMAND_ACCESS_DENIED]);
  //   else
  //   {
        if(CheckChrString((uint8_t*)parString, "%d", strlen(parString)) == _FALSE) {strcpy((char*)ptr_out_buf, reply_command[COMMAND_INCORRECT_VALUE]); ptr_TempDataParsingCommand->flag_end = _OK; return;  }  
        temp1 = atoi(parString);
        if(temp1 == 0 || temp1 == 1)
        {
           sprintf((char*)OldValue,"%d",  enable_MDM_change_valve);
           EEPROM_Write_Byte_FrameSemafor((uint32_t)&enable_MDM_change_valve, (uint8_t*)&temp1, sizeof(enable_MDM_change_valve), 10000);
           sprintf((char*)NewValue,"%d",  enable_MDM_change_valve);
           WriteChangeArcRecord(CHANGE_ENABLE_MDM_CHANGE_VALVE, source);            
           strcpy((char*)ptr_out_buf, reply_command[COMMAND_OK]);           
        }
        else
          strcpy((char*)ptr_out_buf, reply_command[COMMAND_INCORRECT_VALUE]); 
   //  }
  }
  ptr_TempDataParsingCommand->flag_end = _OK;
}
//QT style, auto-documenting with Doxygen, https://habr.com/ru/post/252101/ 
/*!
VALVE_DELAY_TRY_OPEN
\param[in] pointer string
\param[in] pointer output bufer
\param[in] pointer request
\param[in] source command
\param[in] temp data source command
\return void
*/
void PF_VALVE_DELAY_TRY_OPEN(char* parString, uint8_t *ptr_out_buf, uint8_t *ptr_str_reply, TypeSourceCommands source, TTempDataParsingCommand *ptr_TempDataParsingCommand)
{
  if(parString == 0)   
  {
      strcpy((char*)ptr_out_buf, (const char*)ptr_str_reply);
      sprintf((char*)(ptr_out_buf + strlen((char*)ptr_out_buf)),"=%d%s", 
              valve_time_delay_open_flow_non_null_eeprom, reply_OK);  
  }
  else
  {
     uint32_t temp1;
  //   if(CLB_lock != 0x66 && CLB_lock != 0x55 && StatusDevice.password_manufacture_enter == 0)
  //     strcpy((char*)ptr_out_buf, reply_command[COMMAND_ACCESS_DENIED]);
  //   else
  //   {
        if(CheckChrString((uint8_t*)parString, "%d", strlen(parString)) == _FALSE) {strcpy((char*)ptr_out_buf, reply_command[COMMAND_INCORRECT_VALUE]); ptr_TempDataParsingCommand->flag_end = _OK; return;  }  
        temp1 = atoi(parString);
        if(temp1 > 1 && temp1 < 181)
        {
           sprintf((char*)OldValue,"%d",  valve_time_delay_open_flow_non_null_eeprom);
           EEPROM_Write_Byte_FrameSemafor((uint32_t)&valve_time_delay_open_flow_non_null_eeprom, (uint8_t*)&temp1, sizeof(valve_time_delay_open_flow_non_null_eeprom), 10000);
           sprintf((char*)NewValue,"%d",  valve_time_delay_open_flow_non_null_eeprom);
           WriteChangeArcRecord(CHANGE_VALVE_DELAY_TRY_OPEN, source);            
           strcpy((char*)ptr_out_buf, reply_command[COMMAND_OK]);           
        }
        else
          strcpy((char*)ptr_out_buf, reply_command[COMMAND_INCORRECT_VALUE]); 
   //  }
  }
  ptr_TempDataParsingCommand->flag_end = _OK;
}











//QT style, auto-documenting with Doxygen, https://habr.com/ru/post/252101/ 
/*!
RESET
\param[in] pointer string
\param[in] pointer output bufer
\param[in] pointer request
\param[in] source command
\param[in] temp data source command
\return void
*/
void PF_RSTSMT(char* parString, uint8_t *ptr_out_buf, uint8_t *ptr_str_reply, TypeSourceCommands source, TTempDataParsingCommand *ptr_TempDataParsingCommand)
{
  if(parString == 0)   
  {
      strcpy((char*)ptr_out_buf, reply_command[COMMAND_WRITE_ONLY]);
  }
  else
  {
     uint32_t temp;
     if(CheckChrString((uint8_t*)parString, "%d", strlen(parString)) == _FALSE) {strcpy((char*)ptr_out_buf, reply_command[COMMAND_INCORRECT_VALUE]); ptr_TempDataParsingCommand->flag_end = _OK; return;  }  

     temp = atoi(parString);
     if(temp == 1)
     {
           sprintf((char*)OldValue,"%d", 0);
           sprintf((char*)NewValue,"%d", 1);
           WriteChangeArcRecord(CHANGE_RESTART, source);
           osDelay(2000);
           System_Reset();
     }
     else
       strcpy((char*)ptr_out_buf, reply_command[COMMAND_INCORRECT_VALUE]); 
  }
  ptr_TempDataParsingCommand->flag_end = _OK;
}
//QT style, auto-documenting with Doxygen, https://habr.com/ru/post/252101/ 
/*!
revers_flow_on_period_eeprom
\param[in] pointer string
\param[in] pointer output bufer
\param[in] pointer request
\param[in] source command
\param[in] temp data source command
\return void
*/
void PF_REV_FL_ON_PER_S(char* parString, uint8_t *ptr_out_buf, uint8_t *ptr_str_reply, TypeSourceCommands source, TTempDataParsingCommand *ptr_TempDataParsingCommand)
{
  if(parString == 0)   
  {
      strcpy((char*)ptr_out_buf, (const char*)ptr_str_reply);
      sprintf((char*)(ptr_out_buf + strlen((char*)ptr_out_buf)),"=%d%s", revers_flow_on_period_eeprom,  reply_OK);  
  }
  else
  {
     uint32_t temp;
     if(CheckChrString((uint8_t*)parString, "%d", strlen(parString)) == _FALSE) {strcpy((char*)ptr_out_buf, reply_command[COMMAND_INCORRECT_VALUE]); ptr_TempDataParsingCommand->flag_end = _OK; return;  }  

     temp = atoi(parString);
     if(temp > 0 && temp < 2678401)
     {
           sprintf((char*)OldValue,"%d", revers_flow_on_period_eeprom);
           Eeprom_WriteDword((uint32_t)&revers_flow_on_period_eeprom, temp);
           sprintf((char*)NewValue,"%d", revers_flow_on_period_eeprom);
           WriteChangeArcRecord(CHANGE_REV_FL_ON_PER_S, source);
           strcpy((char*)ptr_out_buf, reply_command[COMMAND_OK]);
     }
     else
       strcpy((char*)ptr_out_buf, reply_command[COMMAND_INCORRECT_VALUE]); 
  }
  ptr_TempDataParsingCommand->flag_end = _OK;
}
//QT style, auto-documenting with Doxygen, https://habr.com/ru/post/252101/ 
/*!
revers_flow_on_period_eeprom
\param[in] pointer string
\param[in] pointer output bufer
\param[in] pointer request
\param[in] source command
\param[in] temp data source command
\return void
*/
void PF_REV_FL_OFF_PER_S(char* parString, uint8_t *ptr_out_buf, uint8_t *ptr_str_reply, TypeSourceCommands source, TTempDataParsingCommand *ptr_TempDataParsingCommand)
{
  if(parString == 0)   
  {
      strcpy((char*)ptr_out_buf, (const char*)ptr_str_reply);
      sprintf((char*)(ptr_out_buf + strlen((char*)ptr_out_buf)),"=%d%s", revers_flow_off_period_eeprom,  reply_OK);  
  }
  else
  {
     uint32_t temp;
     if(CheckChrString((uint8_t*)parString, "%d", strlen(parString)) == _FALSE) {strcpy((char*)ptr_out_buf, reply_command[COMMAND_INCORRECT_VALUE]); ptr_TempDataParsingCommand->flag_end = _OK; return;  }  

     temp = atoi(parString);
     if(temp > 0 && temp < 2678401)
     {
           sprintf((char*)OldValue,"%d", revers_flow_off_period_eeprom);
           Eeprom_WriteDword((uint32_t)&revers_flow_off_period_eeprom, temp);
           sprintf((char*)NewValue,"%d", revers_flow_off_period_eeprom);
           WriteChangeArcRecord(CHANGE_REV_FL_OFF_PER_S, source);
           strcpy((char*)ptr_out_buf, reply_command[COMMAND_OK]);
     }
     else
       strcpy((char*)ptr_out_buf, reply_command[COMMAND_INCORRECT_VALUE]); 
  }
  ptr_TempDataParsingCommand->flag_end = _OK;
}
//QT style, auto-documenting with Doxygen, https://habr.com/ru/post/252101/ 
/*!
CLEAR_SN_SGM
\param[in] pointer string
\param[in] pointer output bufer
\param[in] pointer request
\param[in] source command
\param[in] temp data source command
\return void
*/
void PF_CLEAR_SN_SGM(char* parString, uint8_t *ptr_out_buf, uint8_t *ptr_str_reply, TypeSourceCommands source, TTempDataParsingCommand *ptr_TempDataParsingCommand)
{
  if(parString == 0)   
  {
      strcpy((char*)ptr_out_buf, reply_command[COMMAND_WRITE_ONLY]);  
  }
  else
  {
     uint32_t temp;

     if(CheckChrString((uint8_t*)parString, "%d", strlen(parString)) == _FALSE) {strcpy((char*)ptr_out_buf, reply_command[COMMAND_INCORRECT_VALUE]); ptr_TempDataParsingCommand->flag_end = _OK; return;  }  
     if (CLB_lock != 0x66 && CLB_lock != 0x55)
     {
           strcpy((char*)ptr_out_buf, reply_command[COMMAND_ACCESS_DENIED]); 
     }
     else
     {
        temp = atoi(parString);
        if(temp == 1)
        {
           for(uint8_t i = 0; i < 4; i++)
           {
               Eeprom_WriteWord((uint32_t)&EEPROM_SN_SGM[i][0], 0);
           }
           sprintf((char*)OldValue,"%d", 0);       
           sprintf((char*)NewValue,"%d", 0);            
           WriteChangeArcRecord(CHANGE_CLEAR_SN_SGM, source);    
           strcpy((char*)ptr_out_buf, reply_command[COMMAND_OK]);
        }
        else
           strcpy((char*)ptr_out_buf, reply_command[COMMAND_INCORRECT_VALUE]); 
     }
  }
  ptr_TempDataParsingCommand->flag_end = _OK;
}
//QT style, auto-documenting with Doxygen, https://habr.com/ru/post/252101/ 
/*!
VER_PROTOCOL
\param[in] pointer string
\param[in] pointer output bufer
\param[in] pointer request
\param[in] source command
\param[in] temp data source command
\return void
*/
void PF_VER_PROTOCOL(char* parString, uint8_t *ptr_out_buf, uint8_t *ptr_str_reply, TypeSourceCommands source, TTempDataParsingCommand *ptr_TempDataParsingCommand)
{
  if(parString == 0)   
  {
      strcpy((char*)ptr_out_buf, (const char*)ptr_str_reply);
      sprintf((char*)(ptr_out_buf + strlen((char*)ptr_out_buf)),"=%d%s", VER_PROTOCOL,  reply_OK);  
  }
  else
  {
     strcpy((char*)ptr_out_buf, reply_command[COMMAND_READ_ONLY]); 
  }
  ptr_TempDataParsingCommand->flag_end = _OK;
}
/*!
MODE_CONSERVATION
\param[in] pointer string
\param[in] pointer output bufer
\param[in] pointer request
\param[in] source command
\param[in] temp data source command
\return _OK or _FALSE
*/
void PF_MODE_CONSERVATION(char* parString, uint8_t *ptr_out_buf, uint8_t *ptr_str_reply, TypeSourceCommands source, TTempDataParsingCommand *ptr_TempDataParsingCommand)
{
   if(parString == 0)
   {
     strcpy((char*)ptr_out_buf, reply_command[COMMAND_WRITE_ONLY]);   
   }
   else
   {
      if (CLB_lock != 0x66 && CLB_lock != 0x55 && StatusDevice.password_manufacture_enter == 0)
         strcpy((char*)ptr_out_buf, reply_command[COMMAND_ACCESS_DENIED]);   
      else
      {
         if(CheckChrString((uint8_t*)parString, "%d", strlen(parString)) == _FALSE) {strcpy((char*)ptr_out_buf, reply_command[COMMAND_INCORRECT_VALUE]); ptr_TempDataParsingCommand->flag_end = _OK; return;  }  
     
         uint32_t temp;
         temp = atoi((char*)parString);
         if(temp == 1)
         {
            sprintf((char*)OldValue,"%d", 0);
            sprintf((char*)NewValue,"%d", 1);
            WriteChangeArcRecord(CHANGE_MODE_CONSERVATION, source);
            strcpy((char*)ptr_out_buf, reply_command[COMMAND_OK]);
            StatusDevice.mode_conservation = 1;
         }
         else
           strcpy((char*)ptr_out_buf, reply_command[COMMAND_INCORRECT_VALUE]);
     
      }
   }
   ptr_TempDataParsingCommand->flag_end = _OK;
}
/*!
KONTRAST
\param[in] pointer string
\param[in] pointer output bufer
\param[in] pointer request
\param[in] source command
\param[in] temp data source command
\return _OK or _FALSE
*/
void PF_KONTRAST(char* parString, uint8_t *ptr_out_buf, uint8_t *ptr_str_reply, TypeSourceCommands source, TTempDataParsingCommand *ptr_TempDataParsingCommand)
{
  if(parString == 0)   
  {
      strcpy((char*)ptr_out_buf, (const char*)ptr_str_reply);
      sprintf((char*)(ptr_out_buf + strlen((char*)ptr_out_buf)),"=%d%s", optic_kontrast,  reply_OK);  
  }
  else
  {
    /*
      if (CLB_lock != 0x66 && CLB_lock != 0x55 && StatusDevice.password_manufacture_enter == 0)
         strcpy((char*)ptr_out_buf, reply_command[COMMAND_ACCESS_DENIED]);   
      else
      {
    */
         if(CheckChrString((uint8_t*)parString, "%d", strlen(parString)) == _FALSE) {strcpy((char*)ptr_out_buf, reply_command[COMMAND_INCORRECT_VALUE]); ptr_TempDataParsingCommand->flag_end = _OK; return;  }  
     
         uint32_t temp;
         temp = atoi((char*)parString);
         if(temp < 8)
         {
            sprintf((char*)OldValue,"%d", optic_kontrast);
            Eeprom_WriteChar ((uint32_t)&optic_kontrast, temp);
            MODIFY_REG(LCD->FCR, LCD_FCR_CC, optic_kontrast << 10);    // Contrast Control = 7   
            sprintf((char*)NewValue,"%d", optic_kontrast);
            WriteChangeArcRecord(CHANGE_KONTRAST, source);
            strcpy((char*)ptr_out_buf, reply_command[COMMAND_OK]);
         }
         else
           strcpy((char*)ptr_out_buf, reply_command[COMMAND_INCORRECT_VALUE]);
     
   //   }      
  }
  ptr_TempDataParsingCommand->flag_end = _OK;
}
/*!
LCD_PARAM
\param[in] pointer string
\param[in] pointer output bufer
\param[in] pointer request
\param[in] source command
\param[in] temp data source command
\return _OK or _FALSE
*/
void PF_LCD_PARAM(char* parString, uint8_t *ptr_out_buf, uint8_t *ptr_str_reply, TypeSourceCommands source, TTempDataParsingCommand *ptr_TempDataParsingCommand)
{
  if(parString == 0)   
  {
      strcpy((char*)ptr_out_buf, (const char*)ptr_str_reply);
      sprintf((char*)(ptr_out_buf + strlen((char*)ptr_out_buf)),"=%d;%d%s", LCD_fcr_ps, LCD_fcr_div,  reply_OK);  
  }
  else
  {
      if (CLB_lock != 0x66 && CLB_lock != 0x55 && StatusDevice.password_manufacture_enter == 0)
         strcpy((char*)ptr_out_buf, reply_command[COMMAND_ACCESS_DENIED]);   
      else
      {
     
         if(CheckChrString((uint8_t*)parString, "%d;", strlen(parString)) == _FALSE) {strcpy((char*)ptr_out_buf, reply_command[COMMAND_INCORRECT_VALUE]); ptr_TempDataParsingCommand->flag_end = _OK; return;  }  
         
         char *ptr;
         uint32_t lo_fcr_ps, lo_fcr_div;
         ptr = strchr(parString, ';');
         if(ptr)
         {
            ++ptr;
            lo_fcr_ps = atoi(parString);
            lo_fcr_div = atoi(ptr);
            if(lo_fcr_ps < 16 && lo_fcr_div < 16)
            {
               sprintf((char*)OldValue,"%d,%d", LCD_fcr_ps, LCD_fcr_div);
               Eeprom_WriteChar ((uint32_t)&LCD_fcr_ps, lo_fcr_ps);
               Eeprom_WriteChar ((uint32_t)&LCD_fcr_div, lo_fcr_div);
               MODIFY_REG(LCD->FCR, LCD_FCR_PS,  lo_fcr_ps << LCD_FCR_PS_Pos);   //
               MODIFY_REG(LCD->FCR, LCD_FCR_DIV, lo_fcr_div << 18);   // DIV = 7 
               sprintf((char*)NewValue,"%d,%d", LCD_fcr_ps, LCD_fcr_div);
               WriteChangeArcRecord(CHANGE_LCD_FREQUENCY, source);
               strcpy((char*)ptr_out_buf, reply_command[COMMAND_OK]);
           }
           else
              strcpy((char*)ptr_out_buf, reply_command[COMMAND_INCORRECT_VALUE]);
         }
         else
              strcpy((char*)ptr_out_buf, reply_command[COMMAND_INCORRECT_VALUE]);
      }      
  }
  ptr_TempDataParsingCommand->flag_end = _OK;
}
/*!
LCD_PARAM
\param[in] pointer string
\param[in] pointer output bufer
\param[in] pointer request
\param[in] source command
\param[in] temp data source command
\return _OK or _FALSE
*/
void PF_READY_TO_DIALOG(char* parString, uint8_t *ptr_out_buf, uint8_t *ptr_str_reply, TypeSourceCommands source, TTempDataParsingCommand *ptr_TempDataParsingCommand)
{
  if(parString == 0)   
  {
     uint8_t *ptrAES_Key;
     uint8_t CryptStr[40];
     uint32_t len_otput;
     
     ptrAES_Key = &AES128_Key[source][0];
     memset((char*)CryptStr, 0, sizeof(CryptStr));
     sprintf((char*)CryptStr,"%s%u", (uint8_t*)Device_STM.SN_Device, 
                                     SystemUpTimeSecond); //### ������� ������ �����������
  //   sprintf((char*)CryptStr,"%s", "12345678910"); //### ������� ������ �����������
     
     //###
    // sprintf((char*)CryptStr,"%s", "Hello World"); //### ������� ������ �����������
   //  memcpy(ptrAES_Key, "\x2b\x7e\x15\x16\x28\xae\xd2\xa6\xab\xf7\x15\x88\x09\xcf\x4f\x3c",  16);
   //  AES128_CFB_Encrypt(CryptStr, 12, AES128_Key[source], decrypt_message);
   //  memset((char*)CryptStr, 0, sizeof(CryptStr)); 
   //  AES128_CFB_Decrypt(decrypt_message, 12, AES128_Key[source], CryptStr);
     Get_AES128_Key(CryptStr, ptrAES_Key);
     //###
         
     uint8_t i=0;
     AES128_Key[source][i++] = 0xcc;
     AES128_Key[source][i++] = 0xaa;
     AES128_Key[source][i++] = 0x45;
     AES128_Key[source][i++] = 0xf3;
     AES128_Key[source][i++] = 0x9e;
     AES128_Key[source][i++] = 0xc3;
     AES128_Key[source][i++] = 0x70;
     AES128_Key[source][i++] = 0x9f;
     AES128_Key[source][i++] = 0x21;
     AES128_Key[source][i++] = 0xce;
     AES128_Key[source][i++] = 0xc3;
     AES128_Key[source][i++] = 0x5e;
     AES128_Key[source][i++] = 0x7c;
     AES128_Key[source][i++] = 0x44;
     AES128_Key[source][i++] = 0x48;
     AES128_Key[source][i++] = 0xb3;
       
     
     
     
     
     
     //strcpy((char*)ptr_out_buf, (const char*)ptr_str_reply); 
     //sprintf((char*)(ptr_out_buf + strlen((char*)ptr_out_buf)), "=");
     len_otput = sprintf((char*)ptr_out_buf, "%s=", ptr_str_reply); 
     RSA_Encrypt(ptrAES_Key, 16,
                 crypto_open_key[source],
                 ptr_out_buf + len_otput, &len_otput);

     sprintf((char*)(ptr_out_buf + strlen((char*)ptr_out_buf)),"%s", reply_OK);  
     flag_crypt_message[source] = _OK; 
  }
  else
  {
      strcpy((char*)ptr_out_buf, reply_command[COMMAND_READ_ONLY]);
  }
  ptr_TempDataParsingCommand->flag_end = _OK;
}
/*!
READYTD
\param[in] pointer string
\param[in] pointer output bufer
\param[in] pointer request
\param[in] source command
\param[in] temp data source command
\return _OK or _FALSE
*/
void PF_READYTD(char* parString, uint8_t *ptr_out_buf, uint8_t *ptr_str_reply, TypeSourceCommands source, TTempDataParsingCommand *ptr_TempDataParsingCommand)
{
  if(parString == 0)   
  {
     strcpy((char*)ptr_out_buf, (const char*)ptr_str_reply);
     strcat((char*)(ptr_out_buf + strlen((char*)ptr_out_buf)), "=");
     BinToHexStr((ptr_out_buf + strlen((char*)ptr_out_buf)), AES128_Key[source], 16);
     sprintf((char*)(ptr_out_buf + strlen((char*)ptr_out_buf)),"%s", reply_OK); 
     flag_crypt_message[source] = _OK;
  }
  else
  {
      strcpy((char*)ptr_out_buf, reply_command[COMMAND_READ_ONLY]);
  }
  ptr_TempDataParsingCommand->flag_end = _OK;
}