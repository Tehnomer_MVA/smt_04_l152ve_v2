//=============================================================================
/// \file    valve.c
/// \author  Tukov E.A.
/// \date    17.02.2020
/// \brief   
//=============================================================================
//=============================================================================

/**
* @brief global variable to EEPROM
* uint8_t valve_cnt_min_eeprom;
* TypeValveStatus ValveStatus;
* uint16_t valve_time_open_slightly_eeprom;
* uint16_t valve_time_open_additionally_eeprom
* uint16_t valve_delay_before_check_flow_eeprom
* uint16_t valve_time_check_flow_eeprom
* float valve_check_flow_multiplier_up_eeprom
* float valve_check_flow_multiplier_doun_eeprom
* uint8_t valve_presence_eeprom;
* uint16_t valve_time_close_eeprom
* uint16_t valve_time_delay_open_flow_non_null_eeprom
* uint8_t valve_enable_auto_control_eeprom

* @param 
* @retval 
*/

#include "GlobalHandler.h"

TypeValveSettings ValveSettings = {0,0,0,0,0,0,0,0};
TypeValveAction valve_action = VALVE_ACTION_NON;
uint32_t valve_time_open_try_end = 0;
uint32_t valve_open_time_before = 0;
uint32_t valve_open_time_end = 0;
uint8_t valve_cnt_flow_non_null = 0;

int16_t maximum_gas_leak_test_time=0;
int8_t valve_close_anyway = -1;
double VE_Lm3_old=0;
//uint8_t valve_resolution_from_TCP_eeprom = 1;


/**
* @brief 
* @param 
* @retval 
*/
TypeValveRezult ValveInit()
{
   TypeValveRezult rez = VALVE_OK;
   
   if(ValveSettings.ptrDriverPint1_Port != 0)
     DriverMotorsSettings.ptrDriverPint1_Port = ValveSettings.ptrDriverPint1_Port;
   else
     rez = VALVE_FALSE;
   DriverMotorsSettings.driverPint1_Pin = ValveSettings.driverPint1_Pin;
   //----------------
   if(ValveSettings.ptrDriverPint2_Port != 0)
     DriverMotorsSettings.ptrDriverPint2_Port = ValveSettings.ptrDriverPint2_Port;
   else
     rez = VALVE_FALSE;
   DriverMotorsSettings.driverPint2_Pin = ValveSettings.driverPint2_Pin;
   //----------------
   if(ValveSettings.ptrDriverPint3_Port != 0)
     DriverMotorsSettings.ptrDriverPint3_Port = ValveSettings.ptrDriverPint3_Port;
   else
     rez = VALVE_FALSE;
   DriverMotorsSettings.driverPint3_Pin = ValveSettings.driverPint3_Pin;
   //----------------
   return   rez;
}
/**
* @brief 
* @param 
* @retval 
*/
void Valve_Parsing()
{
   if(valve_presence_eeprom) // this valve?
   {
      if(valve_action == VALVE_ACTION_OPEN_OPTIC)
      {
         if(valve_resolution_from_TCP_eeprom == _OK)
         {
       //     if(!valve_cnt_flow_non_null)
               ValveOpen(VALVE_NON_FORCED_OPEN);
               /*
            else
            {
               if((SystemUpTimeSecond - valve_time_open_try_end) > valve_time_delay_open_flow_non_null_eeprom)
               {
                  ValveOpen(VALVE_NON_FORCED_OPEN);
                  if(valve_cnt_flow_non_null > 2)
                    valve_cnt_flow_non_null = 0;
               }
            }
               */
         }
      }
      //------------------------------------------------------
      if(valve_action == VALVE_ACTION_OPEN_FORCED_OPTIC)
      {
         if(valve_resolution_from_TCP_eeprom == _OK)
         {
            ValveOpen(VALVE_FORCED_OPEN);
         }
      }
      //------------------------------------------------------
      if(valve_action == VALVE_ACTION_OPEN_TCP)
      { 
         
               //   valve_resolution_from_TCP_eeprom = _OK;
     //       if(!valve_cnt_flow_non_null)
               ValveOpen(VALVE_NON_FORCED_OPEN);
               /*
            else
            {
               if((SystemUpTimeSecond - valve_time_open_try_end) > valve_time_delay_open_flow_non_null_eeprom)
               {
                  ValveOpen(VALVE_NON_FORCED_OPEN);
                  if(valve_cnt_flow_non_null > 2)
                    valve_cnt_flow_non_null = 0;
               }
            }
               */
      }
      //------------------------------------------------------
      if(valve_action == VALVE_ACTION_OPEN_FORCED_TCP)
      { 
         ValveOpen(VALVE_FORCED_OPEN);
      }
      //------------------------------------------------------
      if(valve_action == VALVE_ACTION_CLOSE_OPTIC || valve_action == VALVE_ACTION_CLOSE_TCP)
      { 
         if(valve_action == VALVE_ACTION_CLOSE_OPTIC)
            ValveClose(VALVE_CLOSE_OPTIC);
         else
            ValveClose(VALVE_CLOSE_TCP);
      }
      //------------------------------------------------------
      if(valve_enable_auto_control_eeprom == 1 && ValveStatus != VALVE_NON_POWER)
      {
         if(valve_action == VALVE_ACTION_CLOSE_REVERSE_FLOW && ValveStatus != VALVE_CLOSE_REVERSE_FLOW)
         {
            ValveClose(VALVE_CLOSE_REVERSE_FLOW);
         }
         //------------------------------------------------------
         if(valve_action == VALVE_ACTION_CLOSE_Q_MAX && ValveStatus != VALVE_CLOSE_Q_MAX)
         {
            ValveClose(VALVE_CLOSE_Q_MAX);
         }
         //------------------------------------------------------
         if(valve_action == VALVE_ACTION_CLOSE_RF_SENSOR && ValveStatus != VALVE_CLOSE_RF_SENSOR)
         {
            ValveClose(VALVE_CLOSE_RF_SENSOR);
         }
         //------------------------------------------------------
         if(valve_action == VALVE_ACTION_CLOSE_BATARY && ValveStatus != VALVE_CLOSE_BATARY)
         {
            ValveClose(VALVE_CLOSE_BATARY);
         }
         //------------------------------------------------------
         if(valve_action == VALVE_ACTION_NON_SIM_CARD && ValveStatus != VALVE_CLOSE_NON_SIM_CARD)
         {
            ValveClose(VALVE_CLOSE_NON_SIM_CARD);
         }
         //------------------------------------------------------
         if(valve_action == VALVE_ACTION_CLOSE_BODY && ValveStatus != VALVE_CLOSE_OPEN_BODY)
         {
            ValveClose(VALVE_CLOSE_OPEN_BODY);
         }
         //---------------------------------------------------------
         if((StatusWarning.bit.residual_capacity_battery_modem_min10 || StatusWarning.bit.residual_capacity_battery_main_min10) && ValveStatus != VALVE_CLOSE_DISCHARGE_BATARY)
         {
            ValveClose(VALVE_CLOSE_DISCHARGE_BATARY);
         }
         //------------------------------------------------------
         if(valve_action == VALVE_ACTION_CLOSE_BAT_BODY && ValveStatus != VALVE_CLOSE_OPEN_BAT_BODY)
         {
            ValveClose(VALVE_CLOSE_OPEN_BAT_BODY);
         }
         //------------------------------------------------------
         if(valve_close_anyway > -1)
         {
            valve_cnt_flow_non_null = 0;
            valve_close_anyway = -1;
            ValveClose((TypeValveStatus)valve_close_anyway);
         }
      }
      if(!valve_cnt_flow_non_null)
         valve_action = VALVE_ACTION_NON;
   }
}
/**
* @brief 
* @param 
* @retval 
*/
void ValveSetAction(TypeValveAction action)
{
   valve_action = action;
   switch(action)
   {
   case VALVE_ACTION_CLOSE_BAT_BODY:
        valve_close_anyway = VALVE_CLOSE_OPEN_BAT_BODY;
        break;
   case VALVE_ACTION_CLOSE_BODY:
        valve_close_anyway = VALVE_CLOSE_OPEN_BODY;
        break;
   case VALVE_ACTION_NON_SIM_CARD:
        valve_close_anyway = VALVE_CLOSE_NON_SIM_CARD;
        break; 
   case VALVE_ACTION_CLOSE_BATARY:
        valve_close_anyway = VALVE_CLOSE_BATARY;
        break;    
   default:
        valve_close_anyway = -1;
        break;
   }
}
/**
* @brief 
* @param 
* @retval 
*/
TypeValveStatus ValveGetStatus()
{
   return ValveStatus;
}
/**
* @brief 
* @param 
* @retval 
*/
uint8_t ValveGetCntOpenFlowNonNull()
{
   return valve_cnt_flow_non_null;
}

/**
* @brief 
* @param 
* @retval 
*/
void VALVE_OPEN_SLIGHTLY()
{
   ValveSetStatus(VALVE_CLOSE_OPEN);
   DriverMotorForwardMovement(valve_time_open_slightly_eeprom);
   valve_open_time_before = SystemUpTimeSecond + valve_delay_before_check_flow_eeprom;
   valve_open_time_end = valve_open_time_before + valve_cnt_min_eeprom;
   ++valve_cnt_flow_non_null;
   maximum_gas_leak_test_time = valve_delay_before_check_flow_eeprom
                                + valve_cnt_min_eeprom 
                                + valve_time_open_slightly_eeprom / 1000
                              //  + valve_time_close_eeprom / 1000
                                + valve_time_delay_open_flow_non_null_eeprom;               
}
/**
* @brief 
* @param 
* @retval 
*/
TypeValveStatus ValveOpen(TypeValveForced flag_forced)
{
   if(ValveStatus != VALVE_OPEN)
   {
      if(ValveStatus != VALVE_CLOSE_OPEN)
      {
         if(ValveStatus == VALVE_CLOSE_OPEN_TRY)
         {
            if(SystemUpTimeSecond > valve_time_open_try_end)
            {
               if(ValvePower(VALVE_POWER_ON) == VALVE_OK)
               {
                  VALVE_OPEN_SLIGHTLY();
               }
               else
               {//###
                  ValveSetStatus(VALVE_NON_POWER);
                  ValvePower(VALVE_POWER_OFF);
                  valve_cnt_flow_non_null = 0;
               }
            }
         }
         else
         {
            if(ValvePower(VALVE_POWER_ON) == VALVE_OK)
            {
               VALVE_OPEN_SLIGHTLY();
            }
            else
            {//###
               ValveSetStatus(VALVE_NON_POWER);
               ValvePower(VALVE_POWER_OFF);
               valve_cnt_flow_non_null = 0;
            }
         }
         
      }
      else
      {// ValveStatus == VALVE_CLOSE_OPEN
         if(SystemUpTimeSecond > valve_open_time_before)
         {
            if(SystemUpTimeSecond < valve_open_time_end)
            {// analyze flow
               if(GasMeter.VE_Lm3 - VE_Lm3_old > valve_check_flow_multiplier_down_eeprom) 
               {// flow be present
                  DriverMotorReverseMovement(valve_time_close_eeprom);
                  valve_time_open_try_end = SystemUpTimeSecond + valve_time_delay_open_flow_non_null_eeprom;
                  ValvePower(VALVE_POWER_OFF);
                  if(valve_cnt_flow_non_null > 2)
                  {
                     valve_cnt_flow_non_null = 0;
                     ValveSetStatus(VALVE_CLOSE_NON_NULL);
                  }
                  else
                  {
                     ValveSetStatus(VALVE_CLOSE_OPEN_TRY);
                  }
               }
            }
            else
            {// analyze flow end. Flow absent
               DriverMotorForwardMovement(valve_time_open_additionally_eeprom);
               ValveSetStatus(VALVE_OPEN);
               valve_cnt_flow_non_null = 0;
               ValvePower(VALVE_POWER_OFF);
            }
         }
         else
         {
            VE_Lm3_old = GasMeter.VE_Lm3;           
         }
      }
   }
   if(!valve_cnt_flow_non_null && enable_MDM_change_valve)
 //    if((ValveStatus == VALVE_OPEN || ValveStatus == VALVE_CLOSE_NON_NULL || ValveStatus == VALVE_NON_POWER) && enable_MDM_change_valve)
      timeUnixGSMNextConnect = timeSystemUnix;
   return ValveStatus;
}
/**
* @brief 
* @param 
* @retval 
*/
/*
TypeValveStatus ValveOpen(TypeValveForced flag_forced)
{
   uint8_t valve_cnt_min = valve_cnt_min_eeprom;
   double VE_Lm3_old;
   if(ValveStatus == VALVE_OPEN)
      valve_cnt_flow_non_null = 0;
   else
   {
      ValveSetStatus(VALVE_CLOSE_OPEN);
      if(ValvePower(VALVE_POWER_ON) == VALVE_OK)
      {
         ++valve_cnt_flow_non_null;
         DriverMotorForwardMovement(0);
         if(flag_forced == VALVE_FORCED_OPEN)
         {
            osDelay(valve_time_open_slightly_eeprom + valve_time_open_additionally_eeprom);
            DriverMotorStopMovement();
            ValveSetStatus(VALVE_OPEN_FORCED);
         }
         else
         {
           
             maximum_gas_leak_test_time = valve_delay_before_check_flow_eeprom
                                          + valve_cnt_min_eeprom 
                                          + valve_time_open_slightly_eeprom / 1000
                                          + valve_time_close_eeprom / 1000
                                          + 2;
            if(valve_cnt_flow_non_null < 3)
            {
               maximum_gas_leak_test_time += valve_time_delay_open_flow_non_null_eeprom;
            }

            
            osDelay(valve_time_open_slightly_eeprom);
            DriverMotorStopMovement();
            for(uint16_t i=0; i < valve_delay_before_check_flow_eeprom * 4; i++)
            {
               osDelay(250);
               if((event_int_story & EVENT_INT_OPEN_CASE) && StatusCrashEnable.bit.body_cover_open)
                 break;
            }
            
            valve_cnt_min = valve_cnt_min_eeprom;
            if(GasMeter.QgL_m3 > GasmeterQmin[_SgmType] * valve_check_flow_multiplier_up_eeprom) // valve_check_flow_multiplier_down_eeprom
               valve_cnt_min = valve_cnt_min_eeprom;
            else
            {
               VE_Lm3_old = GasMeter.VE_Lm3;
               for (; valve_cnt_min != 0; valve_cnt_min--)  // 30
               {
                  for(uint8_t i=0; i < 4; i++)
                  {
                     osDelay(250);
                     if(GasMeter.VE_Lm3 - VE_Lm3_old > valve_check_flow_multiplier_down_eeprom)
                       break;
                     if((event_int_story & EVENT_INT_OPEN_CASE) && StatusCrashEnable.bit.body_cover_open)
                       break;
                  }
                  if(GasMeter.VE_Lm3 - VE_Lm3_old > valve_check_flow_multiplier_down_eeprom)
                     break;
                  if((event_int_story & EVENT_INT_OPEN_CASE) && StatusCrashEnable.bit.body_cover_open)
                     break;
               }
            }
            //------------------------------------------
            if(valve_cnt_min == 0)
            {
               DriverMotorForwardMovement(valve_time_open_additionally_eeprom);
               ValveSetStatus(VALVE_OPEN);
               valve_cnt_flow_non_null = 0;
            }
            else
            {
               DriverMotorReverseMovement(valve_time_close_eeprom);
               if((event_int_story & EVENT_INT_OPEN_CASE) && StatusCrashEnable.bit.body_cover_open)
               {
                  ValveSetStatus(VALVE_CLOSE_OPEN_BODY);
               }
               else
                  if(valve_cnt_flow_non_null > 2)
                     ValveSetStatus(VALVE_CLOSE_NON_NULL);
            }
         }
      }
      else
      {
        ValveSetStatus(VALVE_NON_POWER);
      }
   }
   ValvePower(VALVE_POWER_OFF);
   
   valve_time_open_try_end = timeSystemUnix;
   if((ValveStatus == VALVE_OPEN || ValveStatus == VALVE_CLOSE_NON_NULL || ValveStatus == VALVE_NON_POWER) && enable_MDM_change_valve)
      timeUnixGSMNextConnect = timeSystemUnix;
   
   return ValveStatus;
}
*/
/**
* @brief 
* @param 
* @retval 
*/
TypeValveStatus ValveClose(TypeValveStatus flag_close_source)
{
   valve_cnt_flow_non_null = 0;  
  
   if(ValveStatus == VALVE_CLOSE_TCP
      || ValveStatus == VALVE_CLOSE_REVERSE_FLOW
      || ValveStatus == VALVE_CLOSE_Q_MAX
      || ValveStatus == VALVE_CLOSE_NON_NULL
      || ValveStatus == VALVE_CLOSE_OPTIC
      || ValveStatus == VALVE_CLOSE_RF_SENSOR
      || ValveStatus == VALVE_CLOSE_BATARY       
      || ValveStatus == VALVE_CLOSE_NON_SIM_CARD
      || ValveStatus == VALVE_CLOSE_OPEN_BODY    
      || ValveStatus == VALVE_CLOSE_DISCHARGE_BATARY
      || ValveStatus == VALVE_CLOSE_OPEN_BAT_BODY        
        )
  

   /*
   if(ValveStatus != VALVE_OPEN
      && ValveStatus != VALVE_OPEN_FORCED
      && ValveStatus != VALVE_NON_POWER
      && ValveStatus != VALVE_UNKNOWN
      && flag_close_source != VALVE_CLOSE_OPEN_BAT_BODY
      && flag_close_source != VALVE_CLOSE_OPEN_BODY 
      && flag_close_source != VALVE_CLOSE_NON_SIM_CARD   
      && flag_close_source != VALVE_CLOSE_NON_SIM_CARD          
     )
   */
        __NOP();
   else
   {
      ValveSetStatus(VALVE_OPEN_CLOSE);
      if(ValvePower(VALVE_POWER_ON) == VALVE_OK)
      {
         DriverMotorReverseMovement(valve_time_close_eeprom);
         ValveSetStatus(flag_close_source);
      }
      else
        ValveSetStatus(VALVE_NON_POWER);
      
   if(enable_MDM_change_valve)
       timeUnixGSMNextConnect = timeSystemUnix;      
   }
   ValvePower(VALVE_POWER_OFF);

   return ValveStatus;
}
/**
* @brief 
* @param 
* @retval 
*/
TypeValveRezult ValveSetStatus(TypeValveStatus status_temp)
{
   TypeValveRezult rez = VALVE_OK;
   if(status_temp <= VALVE_CLOSE_OPEN_TRY)
      if(EEPROM_Write_Byte_FrameSemafor((uint32_t)&ValveStatus, (uint8_t*)&status_temp, sizeof(TypeValveStatus), portMAX_DELAY) != EEPROM_WRITE_OK)
        rez = VALVE_FALSE;
   else
     rez = VALVE_FALSE;
   return rez;
}
/**
* @brief 
* @param 
* @retval 
*/
TypeValveRezult ValvePower(TypeValvePower flag_valve_power)
{
   TypeValveRezult rez = VALVE_OK;
   
   if(ValveSettings.ptrPower_Port != 0)
   {
      if(flag_valve_power == VALVE_POWER_ON)
      {
       //  HAL_GPIO_WritePin(ValveSettings.ptrPower_Port, ValveSettings.power_Pin, GPIO_PIN_SET);
        HiPower_ON |= BIT1;
        GSM_PWR_ON();  // enable power value
        osDelay(200);   
        if(READ_BIT(GPIOD->IDR, GPIO_IDR_IDR_2) == RESET)  // �� - �� �������!
        {
           HiPower_ON &= ~BIT1;
           if(HiPower_ON == 0)
              GSM_PWR_OFF();
           rez = VALVE_FALSE;
        }
      }
      else
      {
        // HAL_GPIO_WritePin(ValveSettings.ptrPower_Port, ValveSettings.power_Pin, GPIO_PIN_RESET);
         HiPower_ON &= ~BIT1;
         if(HiPower_ON == 0)
           GSM_PWR_OFF();
      }
   }
   else
     rez = VALVE_FALSE;
   
   return rez;
}

void ValveDecrementGas_leak_test_time()
{
  --maximum_gas_leak_test_time;
}

int16_t ValveGetGas_leak_test_time()
{
   if(maximum_gas_leak_test_time > 0)
      return maximum_gas_leak_test_time;
   else
     return 0;
}