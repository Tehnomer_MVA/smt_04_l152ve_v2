
#include "GlobalHandler.h"

TypeDriverMotorsSettings DriverMotorsSettings = {0,0,0,0,0,0};

/*
forward movement
reverse movement
stop movement
*/
/**
* @brief 
* @param 
* @retval 
*/
TypeDriverMotorRezult DriverMotorForwardMovement(uint32_t time_delay)
{
   TypeDriverMotorRezult rez = DRIVER_MOTOR_OK;
   
   if(DriverMotorStopMovement() == DRIVER_MOTOR_OK)
   {
      HAL_GPIO_WritePin(DriverMotorsSettings.ptrDriverPint2_Port, DriverMotorsSettings.driverPint2_Pin, GPIO_PIN_RESET);// IN1
      HAL_GPIO_WritePin(DriverMotorsSettings.ptrDriverPint3_Port, DriverMotorsSettings.driverPint3_Pin, GPIO_PIN_SET);// IN2
      HAL_GPIO_WritePin(DriverMotorsSettings.ptrDriverPint1_Port, DriverMotorsSettings.driverPint1_Pin, GPIO_PIN_SET);// nSleep 
      if(time_delay)
      {
        osDelay(time_delay);
        DriverMotorStopMovement();
      }
   }
   else
     rez = DRIVER_MOTOR_FALSE;
   
   return rez;
}
/**
* @brief 
* @param 
* @retval 
*/
TypeDriverMotorRezult DriverMotorReverseMovement(uint32_t time_delay)
{
   TypeDriverMotorRezult rez = DRIVER_MOTOR_OK;
   
   if(DriverMotorStopMovement() == DRIVER_MOTOR_OK)
   {
      HAL_GPIO_WritePin(DriverMotorsSettings.ptrDriverPint2_Port, DriverMotorsSettings.driverPint2_Pin, GPIO_PIN_SET);// IN1
      HAL_GPIO_WritePin(DriverMotorsSettings.ptrDriverPint3_Port, DriverMotorsSettings.driverPint3_Pin, GPIO_PIN_RESET);// IN2
      HAL_GPIO_WritePin(DriverMotorsSettings.ptrDriverPint1_Port, DriverMotorsSettings.driverPint1_Pin, GPIO_PIN_SET);// nSleep 
      if(time_delay)
      {
        osDelay(time_delay);
        DriverMotorStopMovement();
      }
   }
   else
     rez = DRIVER_MOTOR_FALSE;
   
   return rez;
}
/**
* @brief 
* @param 
* @retval 
*/
TypeDriverMotorRezult DriverMotorStopMovement()
{
   TypeDriverMotorRezult rez = DRIVER_MOTOR_OK;
   if(DriverMotorsSettings.ptrDriverPint1_Port && DriverMotorsSettings.ptrDriverPint2_Port && DriverMotorsSettings.ptrDriverPint3_Port)
   {
     HAL_GPIO_WritePin(DriverMotorsSettings.ptrDriverPint1_Port, DriverMotorsSettings.driverPint1_Pin, GPIO_PIN_RESET);// nSleep
     HAL_GPIO_WritePin(DriverMotorsSettings.ptrDriverPint2_Port, DriverMotorsSettings.driverPint2_Pin, GPIO_PIN_RESET);// IN1
     HAL_GPIO_WritePin(DriverMotorsSettings.ptrDriverPint3_Port, DriverMotorsSettings.driverPint3_Pin, GPIO_PIN_RESET);// IN2
     osDelay(DRIVER_MOTOR_DELAY_AFTER_STOP_MS);
   }
   else
     rez = DRIVER_MOTOR_FALSE;
   return rez;
}