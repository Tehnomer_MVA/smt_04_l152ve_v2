/* 

while(){for(unlong i=0; i<0xFFFFFFF; i++);}
*/
#include "GlobalHandler.h"


const uint32_t Crc32Table[256] = {
    0x00000000, 0x77073096, 0xEE0E612C, 0x990951BA,
    0x076DC419, 0x706AF48F, 0xE963A535, 0x9E6495A3,
    0x0EDB8832, 0x79DCB8A4, 0xE0D5E91E, 0x97D2D988,
    0x09B64C2B, 0x7EB17CBD, 0xE7B82D07, 0x90BF1D91,
    0x1DB71064, 0x6AB020F2, 0xF3B97148, 0x84BE41DE,
    0x1ADAD47D, 0x6DDDE4EB, 0xF4D4B551, 0x83D385C7,
    0x136C9856, 0x646BA8C0, 0xFD62F97A, 0x8A65C9EC,
    0x14015C4F, 0x63066CD9, 0xFA0F3D63, 0x8D080DF5,
    0x3B6E20C8, 0x4C69105E, 0xD56041E4, 0xA2677172,
    0x3C03E4D1, 0x4B04D447, 0xD20D85FD, 0xA50AB56B,
    0x35B5A8FA, 0x42B2986C, 0xDBBBC9D6, 0xACBCF940,
    0x32D86CE3, 0x45DF5C75, 0xDCD60DCF, 0xABD13D59,
    0x26D930AC, 0x51DE003A, 0xC8D75180, 0xBFD06116,
    0x21B4F4B5, 0x56B3C423, 0xCFBA9599, 0xB8BDA50F,
    0x2802B89E, 0x5F058808, 0xC60CD9B2, 0xB10BE924,
    0x2F6F7C87, 0x58684C11, 0xC1611DAB, 0xB6662D3D,
    0x76DC4190, 0x01DB7106, 0x98D220BC, 0xEFD5102A,
    0x71B18589, 0x06B6B51F, 0x9FBFE4A5, 0xE8B8D433,
    0x7807C9A2, 0x0F00F934, 0x9609A88E, 0xE10E9818,
    0x7F6A0DBB, 0x086D3D2D, 0x91646C97, 0xE6635C01,
    0x6B6B51F4, 0x1C6C6162, 0x856530D8, 0xF262004E,
    0x6C0695ED, 0x1B01A57B, 0x8208F4C1, 0xF50FC457,
    0x65B0D9C6, 0x12B7E950, 0x8BBEB8EA, 0xFCB9887C,
    0x62DD1DDF, 0x15DA2D49, 0x8CD37CF3, 0xFBD44C65,
    0x4DB26158, 0x3AB551CE, 0xA3BC0074, 0xD4BB30E2,
    0x4ADFA541, 0x3DD895D7, 0xA4D1C46D, 0xD3D6F4FB,
    0x4369E96A, 0x346ED9FC, 0xAD678846, 0xDA60B8D0,
    0x44042D73, 0x33031DE5, 0xAA0A4C5F, 0xDD0D7CC9,
    0x5005713C, 0x270241AA, 0xBE0B1010, 0xC90C2086,
    0x5768B525, 0x206F85B3, 0xB966D409, 0xCE61E49F,
    0x5EDEF90E, 0x29D9C998, 0xB0D09822, 0xC7D7A8B4,
    0x59B33D17, 0x2EB40D81, 0xB7BD5C3B, 0xC0BA6CAD,
    0xEDB88320, 0x9ABFB3B6, 0x03B6E20C, 0x74B1D29A,
    0xEAD54739, 0x9DD277AF, 0x04DB2615, 0x73DC1683,
    0xE3630B12, 0x94643B84, 0x0D6D6A3E, 0x7A6A5AA8,
    0xE40ECF0B, 0x9309FF9D, 0x0A00AE27, 0x7D079EB1,
    0xF00F9344, 0x8708A3D2, 0x1E01F268, 0x6906C2FE,
    0xF762575D, 0x806567CB, 0x196C3671, 0x6E6B06E7,
    0xFED41B76, 0x89D32BE0, 0x10DA7A5A, 0x67DD4ACC,
    0xF9B9DF6F, 0x8EBEEFF9, 0x17B7BE43, 0x60B08ED5,
    0xD6D6A3E8, 0xA1D1937E, 0x38D8C2C4, 0x4FDFF252,
    0xD1BB67F1, 0xA6BC5767, 0x3FB506DD, 0x48B2364B,
    0xD80D2BDA, 0xAF0A1B4C, 0x36034AF6, 0x41047A60,
    0xDF60EFC3, 0xA867DF55, 0x316E8EEF, 0x4669BE79,
    0xCB61B38C, 0xBC66831A, 0x256FD2A0, 0x5268E236,
    0xCC0C7795, 0xBB0B4703, 0x220216B9, 0x5505262F,
    0xC5BA3BBE, 0xB2BD0B28, 0x2BB45A92, 0x5CB36A04,
    0xC2D7FFA7, 0xB5D0CF31, 0x2CD99E8B, 0x5BDEAE1D,
    0x9B64C2B0, 0xEC63F226, 0x756AA39C, 0x026D930A,
    0x9C0906A9, 0xEB0E363F, 0x72076785, 0x05005713,
    0x95BF4A82, 0xE2B87A14, 0x7BB12BAE, 0x0CB61B38,
    0x92D28E9B, 0xE5D5BE0D, 0x7CDCEFB7, 0x0BDBDF21,
    0x86D3D2D4, 0xF1D4E242, 0x68DDB3F8, 0x1FDA836E,
    0x81BE16CD, 0xF6B9265B, 0x6FB077E1, 0x18B74777,
    0x88085AE6, 0xFF0F6A70, 0x66063BCA, 0x11010B5C,
    0x8F659EFF, 0xF862AE69, 0x616BFFD3, 0x166CCF45,
    0xA00AE278, 0xD70DD2EE, 0x4E048354, 0x3903B3C2,
    0xA7672661, 0xD06016F7, 0x4969474D, 0x3E6E77DB,
    0xAED16A4A, 0xD9D65ADC, 0x40DF0B66, 0x37D83BF0,
    0xA9BCAE53, 0xDEBB9EC5, 0x47B2CF7F, 0x30B5FFE9,
    0xBDBDF21C, 0xCABAC28A, 0x53B39330, 0x24B4A3A6,
    0xBAD03605, 0xCDD70693, 0x54DE5729, 0x23D967BF,
    0xB3667A2E, 0xC4614AB8, 0x5D681B02, 0x2A6F2B94,
    0xB40BBE37, 0xC30C8EA1, 0x5A05DF1B, 0x2D02EF8D
};
extern SemaphoreHandle_t xCalculateCRC32Semaphore;

//############################################################
uint8_t CheckOneBitInByte(uint8_t Bit)
{
   uint8_t flag=0;
   for(uint8_t i=0; i<8; i++)
   {
      if((Bit & BIT0) && flag)
      {
         return 1;
      }
      else
      {
         if(Bit & BIT0)
         flag = 1;
         Bit >>= 1;
      }
   }
   return 0;
}
// ##################################################################
uint16_t SwapBytes(uint16_t buf)
{
   uint16_t temp = buf << 8;
   temp |= (uint8_t) (buf >> 8);
   return temp;
}
// ##################################################################
double SumDouble(double *ptrData, uint16_t cnt)
{
   double sum=0;
   for(uint16_t i=0; i < cnt; i++)
   {
     sum += *ptrData++;
   }
   return sum;
}
// ##################################################################
uint32_t SumUint16_t(uint16_t *ptrData, uint16_t cnt)
{
   uint32_t sum=0;
   for(uint16_t i=0; i < cnt; i++)
   {
     sum += *ptrData++;
   }
   return sum;
}     
// ##################################################################
uint16_t Average(uint16_t *ptrData, uint8_t cnt_word)
{
   uint32_t sum=0;
   for(uint8_t i=0; i < cnt_word; i++)
   {
     sum += *ptrData++;
   }
   sum /= cnt_word;
   return (uint16_t)sum;
}

// ##################################################################
uint8_t Calck_CSumme (uint8_t *ptr_cs, uint32_t counter)
{
   uint16_t i;
   uint8_t CS;
   CS=0;
   for (i = 0; i < counter; i++)
   {CS += *ptr_cs++;}
   CS=~CS;
   return CS;
}
//########################################################################################################################################
uint8_t Number_digits_U32(uint32_t x)
{
   uint8_t r=0;
   if(x < 10)
     r = 1;
   else if(x < 100)
     r = 2;
   else if(x < 1000)
     r = 3;
   else if(x < 10000)
     r = 4; 
   else if(x < 100000)
     r = 5; 
   else if(x < 1000000)
     r = 6; 
   else if(x < 10000000)
     r = 7;
   else if(x < 100000000)
     r = 8;
   else if(x < 1000000000)
     r = 9;
   else if(x > 999999999)
     r = 10;
   return r;
}
// ########################################################################################################################################
uint8_t Find_null_bite(uint8_t x, uint8_t n)
{
   if(n < 9)
      for(uint8_t i=1; i <= n; i++)
      {
         if(!(x & BIT0))
            return i; // ���������� ����� ���������� ����
         x >>= 1;
      }
   return 0; // ��� �� ������ �������� ����
}
// ########################################################################################################################################
uint8_t Find_null_byte(uint8_t *ptr, uint8_t n)
{
   for(uint8_t i=1; i <= n; i++)
   {
      if(!*ptr++)
         return i;
   }
   return 0;
}
// ########################################################################################################################################
void Change_bite(uint8_t *ptr_x, uint8_t n, uint8_t flag)
{
   if(flag)
   {
      *ptr_x |= BIT0 << n-1;
   }
   else
   {
      *ptr_x &= ~(BIT0 << n-1);
   }
}
// ########################################################################################################################################
uint16_t StrToHex(uint8_t *OutBuf, uint8_t * InBuf, uint16_t inc)
{
   int i,
       len,
       r_len=0,
       hex;
   
   len = strlen((char*)InBuf);
   for(i=0; i<len;)
   {
      sscanf((char *)InBuf+i,"%02X",&hex);
      OutBuf[r_len++]=hex;
      i += inc;
   }
   return r_len;
}
// ########################################################################################################################################
uint16_t BinToHexStr(uint8_t *OutBuf, uint8_t * InBuf, uint16_t inc)
{
   uint16_t len = 0;
   for(uint16_t i = 0; i < inc; i++)
     len += sprintf((char*)OutBuf + len,"%02X", InBuf[i]);// +
   return len;
}

//########################################################################################################################################
void AnsiToUnicod(uint8_t *ptrbuf, uint8_t *ptrstring)
{
   uint8_t len_str;
   char *ptr;
   ptr = strchr((char*)ptrbuf, 0);
   if(ptr == 0)
     return;
   len_str = strlen((char*)ptrstring);
   for(uint8_t i=0; i < len_str; i++)
   {
      *ptr++ = '0';
      *ptr++ = '0';
      sprintf((char*)ptr,"%02X", *ptrstring++);
      ptr += 2;
   }
}
// ########################################################################################################################################
void LongToUnicod(uint8_t *ptrbuf, uint32_t numbber)
{
   uint8_t buf[12];
   sprintf((char*)buf, "%d", numbber);
   AnsiToUnicod(ptrbuf, buf);
}

/*
// ########################################################################################################################################
uint32_t DateToInt(DateType dateType)
{// ������ ������
   uint32_t dateInt;
   dateInt = (uint32_t)dateType.Year * 10000;
   dateInt += (uint32_t)dateType.Date * 100;
   dateInt += (uint32_t)dateType.Month;
   return dateInt;
}
// ########################################################################################################################################
DateType IntToDate(uint32_t dateInt)
{// ������ ������
   DateType dateType;
   dateType.Year = (uint8_t)(dateInt / 10000);
   dateType.Date = (uint8_t)((dateInt % 10000) / 100);
   dateType.Date = (uint8_t)(dateInt % 100);
   return dateType;
}
*/
// ####################################################################################################################3
/*
1 - ���� 1 > 2
0 - ���� 1 == 2
-1 - ���� 1 < 2
*/
int8_t compareDateTime(_DateTimeType DateType1, _DateTimeType DateType2)
{
    if(DateType1.Date.Year > DateType2.Date.Year)
       return MORE_YEAR;
    if(DateType1.Date.Year < DateType2.Date.Year)
       return LESS_YEAR;
    
    if(DateType1.Date.Month > DateType2.Date.Month)
       return MORE_MONTH;
    if(DateType1.Date.Month < DateType2.Date.Month)
       return LESS_MONTH;
    
    if(DateType1.Date.Date > DateType2.Date.Date)
       return MORE_DAY;
    if(DateType1.Date.Date < DateType2.Date.Date)
       return LESS_DAY;
    
    if(DateType1.Time.Hours > DateType2.Time.Hours)
       return MORE_HOUR;
    if(DateType1.Time.Hours < DateType2.Time.Hours)
       return LESS_HOUR;
    
    if(DateType1.Time.Minutes > DateType2.Time.Minutes)
       return MORE_MINUTE;
    if(DateType1.Time.Minutes < DateType2.Time.Minutes)
       return LESS_MINUTE;
    
    if(DateType1.Time.Seconds > DateType2.Time.Seconds)
       return MORE_SECOND;
    if(DateType1.Time.Seconds < DateType2.Time.Seconds)
       return LESS_SECOND;
    
    return EQUALLY;
}
// ###########################################################################
uint32_t DifferenceTickCount(uint32_t TickCount, uint32_t temp)
{
   uint32_t buf = 0;
   if(TickCount < temp)
   {
      buf = (0xFFFFFFFF - temp) + 1 + TickCount;
   }
   else
   {
      buf =  TickCount - temp;
   } 
   return buf;
}
/*!
the number of occurrences of a character in a string. max 65535 char
\param[in] char *ptr_str - pointer for string
\param[in] char chr - find char
\param[out]
\return 
*/   
uint32_t str_fing_chr(char *ptr_str, char chr, uint32_t str_len)
{
   uint32_t i = 0, cnt=0;
   for(; *ptr_str != 0 && i < str_len; i++)
     if(*ptr_str++ == chr)
       ++cnt;
   return cnt;
}
// ###########################################################################
uint8_t check_crc32_str(uint8_t *ptr_str, uint8_t erase_tab_symbol)
{
   char *ptr_temp_for_crc32;
   uint8_t rez = _FALSE;
   uint32_t crc32, crc32_str;
   
   ptr_temp_for_crc32 = strchr((char*)ptr_str, '\t');
   ++ptr_temp_for_crc32;
   ptr_temp_for_crc32 = strchr((char*)ptr_temp_for_crc32, '\t');
   if(ptr_temp_for_crc32)
   {
       portBASE_TYPE xStatus;
       xStatus = xSemaphoreTake(xCalculateCRC32Semaphore, 5000);
       if( xStatus != pdPASS )
       {
        // ������ 
          xSemaphoreGive(xCalculateCRC32Semaphore);
          return rez;
       }
       crc32_str = CalculateCRC32((uint8_t*)ptr_str, (uint32_t)ptr_temp_for_crc32 - (uint32_t)ptr_str, CRC_START_STOP, 0);
       xSemaphoreGive(xCalculateCRC32Semaphore);
       sscanf(ptr_temp_for_crc32 + 1,"%X",&crc32);
       if(crc32_str == crc32)
          rez = _OK;
       if(erase_tab_symbol == _OK)
         *ptr_temp_for_crc32 = 0;
   }
   return rez;
}

/*!

\param[in] 
\param[in] 
\param[out]
\return 
*/   
void Formatting_string_to_right(char *ptr_str_part1, char *ptr_str_part2, uint8_t max_cnt_char)
{
   uint8_t str1_len, str2_len;
   uint8_t gap_length;
   
   str1_len = strlen((char*)ptr_str_part1);
   str2_len = strlen((char*)ptr_str_part2);
   if(max_cnt_char >= (str1_len + str2_len))
   {
      gap_length = max_cnt_char - str1_len - str2_len;
      if(gap_length)
           memset(ptr_str_part1 + str1_len, ' ', gap_length);
      strcpy((char*)(ptr_str_part1 + str1_len + gap_length), (char*)ptr_str_part2);
   }
}
/*!
the number of occurrences of a character in a string. max 65535 char
\param[in] char *ptr_str - pointer for string
\param[in] char chr - find char
\param[out]
\return 
*/  
uint32_t CalculateCRC32(uint8_t *buf, uint32_t len, uint8_t processing, uint32_t old_crc32)
{ 
    
    uint32_t crc;
    // 0x04C11DB7
    /*
    for (uint32_t i = 0; i < 256; i++)
    {
        crc = i;
        for (j = 0; j < 8; j++)
            crc = crc & 1 ? (crc >> 1) ^ 0xEDB88320 : crc >> 1;
        crc_table[i] = crc;
    }
    */
    if(processing == CRC_START || processing == CRC_START_STOP)
       crc = 0xFFFFFFFF;
    else
      crc = old_crc32;
    while (len--)
        crc = Crc32Table[(crc ^ *buf++) & 0xFF] ^ (crc >> 8);
    if(processing == CRC_STOP || processing == CRC_START_STOP)
       return crc ^ 0xFFFFFFFF;
    else
       return crc;
}
/*!
check server url in validation
\param[in] *ptr string
\return 0 - false, 1 - true
*/ 
uint8_t CheckUrlServer(char *Str)
{
int i,len;
uint8_t ret=0;
uint32_t Value=0;
char *poi,*spoi=Str;
for(i=0;i<5;i++)
	{
	if(i<4)
		{
		if(i==3)
			poi=strchr(spoi,':');
		else
			poi=strchr(spoi,'.');
		if(!poi)
			break;
		len=poi-spoi;
		Value=atoi(spoi);
		if(len>3 || Value>255 || is_digit(spoi,len)==0)
			break;
		spoi=poi+1;
		}
	else
		{
		len=strlen(spoi);
		Value=atoi(spoi);
		if(len==0 || len>5 || is_digit(spoi,len)==0 || Value>65535)
			break;		
		ret=1;
		}
	}
return ret;
}
/*!
check string in availability digital char
\param[in] *ptr string
\param[in] int len - lenght string
\return int 0 - FLSE, 1 - OK
*/
uint8_t is_digit(char *str, int len)
{
   uint32_t i;
   uint8_t ret = _FALSE;
   for(i = 0; i < len; i++)
   {
      if(str[i] > 0x2F && str[i] < 0x3A)
	ret = _OK;
      else
      {
	ret = _FALSE;
	break;
      }
   }
   return ret;
}
/*!
check string in cnt digital char
\param[in] *ptr string
\param[in] int len - lenght string
\return int
*/
uint8_t is_digit_cnt(char *str, int len)
{
   uint32_t i;
   uint8_t ret = 0;
   for(i = 0; i < len; i++)
   {
      if(str[i] > 0x2F && str[i] < 0x3A)
	++ret;
      else
      {
	break;
      }
   }
   return ret;
}
/*!
check string in availability digital char
if(CheckChrString((uint8_t*)parString, "%L%l.%d", strlen(parString)) == _FALSE) return OPTIC_INCORRECT_VALUE;
\param[in] *ptr string
\param[in] *ptr formating string %d %L %l %x etc "%d.,;:-%L%l"
\return len string
*/
uint8_t CheckChrString(uint8_t *input, uint8_t *format, uint32_t len)
{
   uint8_t rezult = _FALSE;
   uint8_t check = _FALSE;   
   uint8_t len_format = strlen((char*)format);
   
   for(uint32_t i = 0; i < len; i++)
   {
      if(input[i] == 0)
         break;
      check = _FALSE;
      for(uint8_t j = 0; j < len_format; j++)
      {
          if(format[j] == '%')
          {
             ++j;
             switch(format[j])
             {
             case 'L':
                  if(input[i] > 0x40 && input[i] < 0x5B)
                     check = _OK;
                  break;
             case 'l':
                  if(input[i] > 0x60 && input[i] < 0x7B)
                     check = _OK;
                  break;
             case 'x':   
                   if(input[i] > 0x40 && input[i] < 0x47)
                     check = _OK;
             case 'd':
                  if(input[i] > 0x2F && input[i] < 0x3A)
                     check = _OK;
                  break;
             case '%':
                  if(input[i] == '%')
                     check = _OK;
                  break;    
             default:break;
             }
          }
          else
            if(input[i] == format[j])
              check = _OK;
          if(check == _OK)
            break;
      }
      if(check == _OK)
         rezult = _OK;
      else
      {
         rezult = _FALSE;
         break;
      }      
   }
   return rezult;
}
/*
#pragma inline=forced
struct tm * _gmtime_(time_t *p)
{
   #if _DLIB_TIME_USES_64
      if(*p > 0x7915ECC00) // 3000 year
        *p = 0x7915ECC00;
      return __gmtime64(p);
    #else
      return __gmtime32(p);
    #endif
}
*/
inline struct tm * _gmtime_(time_t *p)
{
   #if _DLIB_TIME_USES_64
      if((uint64_t)(*p) > (uint64_t)0x790153500) // 3000 year
        *p = 0x7915ECC00;
      return __gmtime64(p);
    #else
      return __gmtime32(p);
    #endif
}