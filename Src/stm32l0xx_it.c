/**
  ******************************************************************************
  * @file    stm32l0xx_it.c
  * @brief   Interrupt Service Routines.
  ******************************************************************************
  *
  * COPYRIGHT(c) 2018 STMicroelectronics
  *
  * Redistribution and use in source and binary forms, with or without modification,
  * are permitted provided that the following conditions are met:
  *   1. Redistributions of source code must retain the above copyright notice,
  *      this list of conditions and the following disclaimer.
  *   2. Redistributions in binary form must reproduce the above copyright notice,
  *      this list of conditions and the following disclaimer in the documentation
  *      and/or other materials provided with the distribution.
  *   3. Neither the name of STMicroelectronics nor the names of its contributors
  *      may be used to endorse or promote products derived from this software
  *      without specific prior written permission.
  *
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
  * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
  * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
  * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
  * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
  * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  *
  ******************************************************************************
  */
/* Includes ------------------------------------------------------------------*/
// #include "stm32l0xx_hal.h"
#include "stm32l1xx.h"
#include "stm32l0xx_it.h"
#include "cmsis_os.h"

#include "GlobalHandler.h"


/* USER CODE BEGIN 0 */
extern uint8_t _buf[];
extern uint8_t _spi_buf_cnt;
extern uint8_t _spi_buf_max;


extern uint8_t WakeUpTimerFlag;
extern uint8_t AlarmFlag;
extern uint8_t IntArcFlag;

extern uint64_t timeSystemUnix;
//extern char MdmTXBuffer[];
extern uint16_t ModemTransmitEnable;

extern osThreadId OPTO_ReadTaskHandle;
extern SemaphoreHandle_t xRTCSemaphore;
extern SemaphoreHandle_t xOpticRxSemaphore;
extern osMessageQId QueueParsingCommandHandle;


portBASE_TYPE xHigherPriorityTaskWoken = pdFALSE;
/* USER CODE END 0 */

/* External variables --------------------------------------------------------*/


/******************************************************************************/
/*            Cortex-M0+ Processor Interruption and Exception Handlers         */ 
/******************************************************************************/

/**
* @brief This function handles Non maskable Interrupt.
*/
void NMI_Handler(void)
{
  /* USER CODE BEGIN NonMaskableInt_IRQn 0 */
 while(1);
  /* USER CODE END NonMaskableInt_IRQn 0 */
  /* USER CODE BEGIN NonMaskableInt_IRQn 1 */

  /* USER CODE END NonMaskableInt_IRQn 1 */
}

/**
* @brief This function handles Hard fault interrupt.
*/
void HardFault_Handler(void)
{
  /* USER CODE BEGIN HardFault_IRQn 0 */

  /* USER CODE END HardFault_IRQn 0 */
//  while (1)
 // {
    /* USER CODE BEGIN W1_HardFault_IRQn 0 */
    Eeprom_WriteChar((uint32_t)&hard_fault,  1);
 //   Eeprom_WriteDword((uint32_t)&number_communication_gsm_fail_eeprom,  number_communication_gsm_fail);
 //   Eeprom_WriteDword((uint32_t)&current_communication_gsm_number_eeprom,  current_communication_gsm_number);
    System_Reset();
    /* USER CODE END W1_HardFault_IRQn 0 */
 // }
  /* USER CODE BEGIN HardFault_IRQn 1 */

  /* USER CODE END HardFault_IRQn 1 */
}

/**
* @brief This function handles System tick timer.
*/
void SysTick_Handler(void)
{
  /* USER CODE BEGIN SysTick_IRQn 0 */

  /* USER CODE END SysTick_IRQn 0 */
//  HAL_IncTick();
  osSystickHandler();
  /* USER CODE BEGIN SysTick_IRQn 1 */
  GSM_increment_delay();
  ++system_tick_ms;
  CalculateMotoHourceWorkCPU_IDLE();
  /* USER CODE END SysTick_IRQn 1 */
}

/******************************************************************************/
/* STM32L0xx Peripheral Interrupt Handlers                                    */
/* Add here the Interrupt Handlers for the used peripherals.                  */
/* For the available peripheral interrupt handler names,                      */
/* please refer to the startup file (startup_stm32l0xx.s).                    */
/******************************************************************************/

/**
* @brief This function handles RTC global interrupt through EXTI lines 17, 19 
*         and 20 and LSE CSS interrupt through EXTI line 19.
*/
void RTC_IRQHandler(void)
{
  // Check Wakeup timer Flag
  if(READ_BIT(RTC->ISR, RTC_ISR_WUTF)) 
  {
    // Get the status of the Interrupt 
    if(RTC->CR & RTC_CR_WUTIE)
    { 
      // Set Flag
      WakeUpTimerFlag = 1;
      
      // Clear Wake Up Flag
      PWR->CR |= PWR_CR_CWUF;
      
      // Clear the WAKEUPTIMER interrupt pending bit
      CLEAR_BIT(RTC->ISR, RTC_ISR_WUTF);
    }
    
    // Clear the EXTI's line Flag for RTC WakeUpTimer
    SET_BIT(EXTI->PR,EXTI_IMR_IM20);
  }
    // Check Alarm Flag A
  if(READ_BIT(RTC->ISR, RTC_ISR_ALRAF)) 
  {
    
    portBASE_TYPE xHigherPriorityTaskWoken = pdFALSE;
    
    // Get the status of the Interrupt 
    if(RTC->CR & RTC_CR_ALRAIE) 
    { 
      // Increase Time
      SystemUpTimeSecond++;
   //  ++timeSystemUnix;
   //   timeSystemUnix += 60;//###
      
#ifndef RTC_SPEED 
     ++timeSystemUnix;
#endif   
   
#ifdef RTC_SPEED 
     timeSystemUnix += 60;
#endif  
      
      
      
      AlarmFlag = 1;
      // Clear Wake Up Flag
      PWR->CR |= PWR_CR_CWUF;
      
      // Clear the ALARM A interrupt pending bit
      CLEAR_BIT(RTC->ISR, RTC_ISR_ALRAF);
      
      if (KeyPressed==KEY_PRESSED_DOWN)
      {
        PressKeyDelay++;
        if(PressKeyDelay > 2 && Global_Menu != TEH_MENU_MDMTST)
           Switch_Sub_Menu();
        if(PressKeyDelay > 5 && Global_Menu == TEH_MENU_MDMTST)
          if(GSM_Get_flag_power() == GSM_MUST_DISABLE)
          {
             timeUnixGSMNextConnect = timeSystemUnix; // on GPRS
             source_start_modem = SOURCE_START_MDM_BUTTON;
          }
      }  
      xSemaphoreGiveFromISR(xRTCSemaphore, &xHigherPriorityTaskWoken );
    }
  }  
        // Check Alarm Flag B
  if(READ_BIT(RTC->ISR, RTC_ISR_ALRBF)) 
  {
    // Get the status of the Interrupt 
    if(RTC->CR & RTC_CR_ALRBIE) 
    { 
      IntArcFlag = 1;
      // Clear Wake Up Flag
      PWR->CR |= PWR_CR_CWUF;
      // Clear the ALARM A interrupt pending bit
      CLEAR_BIT(RTC->ISR, RTC_ISR_ALRBF);
    }

  } 
    // Clear the EXTI's line Flag for RTC Alarm
    SET_BIT(EXTI->PR, EXTI_IMR_IM17);
}


/**
* @brief This function handles DMA1 channel 2 and channel 3 interrupts.
*/
void DMA1_Channel2_3_IRQHandler(void)
{
 if((DMA1->ISR & DMA_ISR_TCIF2)) // �������� ����� �������� �������� � USART
  {
    SET_BIT(DMA1->IFCR,DMA_IFCR_CGIF2);  // ������ ���� �������� ������ 
    memset((void*)TransmitBuffer,0,TX_BUFFER_MAX_SIZE); // +
    CLEAR_BIT(DMA1_Channel2->CCR,DMA_CCR_EN);
    
    TransmitEnable = TRANSMIT_ENABLE;

  }
 if((DMA1->ISR & DMA_ISR_TCIF3))
  { // buffer overflow
    SET_BIT(DMA1->IFCR,DMA_IFCR_CGIF3);  
    CLEAR_BIT(DMA1_Channel3->CCR, DMA_CCR_EN);
    memset((void*)ReceivedBuffer,0,RX_BUFFER_MAX_SIZE); // +
       //����� ������ ���������
    DMA1_Channel3->CMAR = (uint32_t)ReceivedBuffer ;
    DMA1_Channel3->CNDTR = RX_BUFFER_MAX_SIZE;
    SET_BIT(DMA1_Channel3->CCR, DMA_CCR_EN);
  }
}

/**
* @brief This function handles SPI1 global interrupt.
*/
void SPI1_IRQHandler(void)
{
  if(LL_SPI_IsActiveFlag_RXNE(SPI1))
  {
    CLEAR_BIT(SPI1->SR, SPI_SR_RXNE);
 //   if (_spi_buf_cnt < _buf_SIZE) // _spi_buf_max
       _buf[_spi_buf_cnt] = LL_SPI_ReceiveData8(SPI1);
    _spi_buf_cnt++;

    
  //  __no_operation();
  }  
}  

/**
* @brief This function handles AES, RNG and LPUART1 interrupts /
* LPUART1 wake-up interrupt through EXTI line 28.
*/
void AES_RNG_LPUART1_IRQHandler(void)
{
 //  portBASE_TYPE xHigherPriorityTaskWoken = pdFALSE;

   SET_BIT(EXTI->PR,EXTI_IMR_IM28);
  
   if(LPUART1->ISR & USART_ISR_RXNE)
   {
     __no_operation(); 
   }
   if(LPUART1->ISR & USART_ISR_ORE) //   Overrun error
   {
     SET_BIT(LPUART1->CR3,USART_CR3_OVRDIS); 
     SET_BIT(LPUART1->ICR,USART_ICR_ORECF); 
   } 
  
   if(LPUART1->ISR & USART_ISR_FE) //   Framing error
   {
     SET_BIT(LPUART1->ICR,USART_ICR_FECF);  
   }  
   
   if(LPUART1->ISR & USART_ISR_PE) //   Parity error
   {
     SET_BIT(LPUART1->ICR,USART_ICR_PECF); 
   } 

  if(LPUART1->ISR & USART_ISR_NE) //   Noise error
   {
     SET_BIT(LPUART1->ICR,USART_ICR_NCF); 
   } 

   
   if(LPUART1->ISR & USART_ISR_CMF)  
   {
    SET_BIT(LPUART1->ICR,USART_ICR_CMCF); 
    CLEAR_BIT(DMA1_Channel3->CCR,DMA_CCR_EN);
    //DataReceived = 0xFF;
    
           //   ReceivedBuffer[ReceivedCounter-1] = 0;
              //ReceivedCounter = 0;         
             // xSemaphoreGiveFromISR(xOpticRxSemaphore, &xHigherPriorityTaskWoken );
              static portBASE_TYPE xHigherPriorityTaskWoken;
              xHigherPriorityTaskWoken = pdFALSE;
              TypeParsingCommand elementParsingCommand;
              elementParsingCommand.ptr_commands = (uint8_t*)ReceivedBuffer;
              elementParsingCommand.source = SOURCE_OPTIC;
              elementParsingCommand.ptr_out = (uint8_t*)TransmitBuffer;
              elementParsingCommand.sem_source = &xOpticRxSemaphore;
              elementParsingCommand.ptr_temp_data = &OPTIC_TempDataParsingCommand;
              OPTIC_TempDataParsingCommand.number_frame = 0;
              OPTIC_TempDataParsingCommand.flag_end = _FALSE;
              OPTIC_TempDataParsingCommand.index_commands = -1;
              OPTIC_TempDataParsingCommand.size_bufer = TX_BUFFER_MAX_SIZE;
              OPTIC_TempDataParsingCommand.size_bufer_left = TX_BUFFER_MAX_SIZE;
              xQueueSendToBackFromISR(QueueParsingCommandHandle, ( void * ) &elementParsingCommand, &xHigherPriorityTaskWoken);
            //  if(xHigherPriorityTaskWoken == errQUEUE_FULL)
            //    __NOP(); //### 
  //  xSemaphoreGiveFromISR(xOptoRxSemaphore, &xHigherPriorityTaskWoken ); 
   }
}

void EXTI0_1_IRQHandler(void)
{
  if (EXTI->PR & EXTI_PR_PR1) // LockKey request?
  {
     SET_BIT(EXTI->PR, EXTI_PR_PR1);
  }
  if (EXTI->PR & EXTI_PR_PR0) // LockKey request?
  {
     SET_BIT(EXTI->PR, EXTI_PR_PR0);
  }
}
// ----------------------------------------------------------------------------
void EXTI2_3_IRQHandler(void)
{
  //static uint32_t LockPressTime ;

 if (EXTI->PR & EXTI_PR_PR2) // LockKey request?
  {
    SET_BIT(EXTI->PR,EXTI_PR_PR2);
   if(READ_BIT(GPIOB->IDR, GPIO_IDR_IDR_2)==0) 
     {
  //     LockPressTime = Time_GetSystemUpTimeSecond();
       LockKeyState = KEY_PRESSED_DOWN;
     }  
     else
     {
      
      /* LockPressKeyDelay = Time_GetSystemUpTimeSecond() - LockPressTime;  
         if(LockPressKeyDelay > 5)
         { 
           LockKeyState = KEY_PRESSED_NOT;
           CLB_lock_IT = 1;
         LockPressKeyDelay =0;   
         } 
   */
    }
 }
}


/* USER CODE BEGIN 1 */
// ----------------------------------------------------------------------------
void EXTI4_15_IRQHandler(void)
{
  //--------------------------------------------------
  if (EXTI->PR & EXTI_PR_PR8) // Red Cont. request?
  {
     EXTI->PR |= EXTI_PR_PR8;
     if(READ_BIT(GPIOC->IDR, GPIO_IDR_IDR_8)==0) 
     { 
        if(!body_cover_open_it_flag)
        {
           body_cover_open_it_time = system_tick_ms;
           body_cover_open_it_flag = 1;
        }
     } 
  }  
  //--------------------------------------------------
  if (EXTI->PR & EXTI_PR_PR12) // Red Cont. request?
  {
    EXTI->PR |= EXTI_PR_PR12;
     if(READ_BIT(GPIOC->IDR, GPIO_IDR_IDR_12)==0) 
     { 
       OptoPort = OPTO_PORT_ENABLE;      /// ���� ���������� ���������
       tick_count_message_from_OPTIC_sec = SystemUpTimeSecond;
     } 
        
     else
     {
       OptoPort = OPTO_PORT_DISABLE;      /// ����� ����� ���������� ���������
       kfactor_emulator = 65535;
       TransmitOpticTest=0;
     } 
  }
  //--------------------------------------------------  
  if (EXTI->PR & EXTI_PR_PR13) // Key request?
  {
    SET_BIT(EXTI->PR, EXTI_PR_PR13);
    StatusDevice.mode_conservation = 0;
     if(READ_BIT(GPIOC->IDR, GPIO_IDR_IDR_13)==0) 
     {
       PressKeyTime = Time_GetSystemUpTimeSecond();
       KeyPressed = KEY_PRESSED_DOWN;
       return;
     }  
     else
     {
        PressKeyDelay = Time_GetSystemUpTimeSecond() - PressKeyTime;  
//      if(PressKeyDelay)
         if(PressKeyDelay < 2)
           KeyPressed = KEY_PRESSED_SHORT;
          else
           KeyPressed = KEY_PRESSED_LONG;
        PressKeyDelay =0;   
        return; 
     } 
     // End key request handler 
    } 


}

//-----------------------------------------------------------------------------
// ���������� ���������� �� ��������� �������� ������ �� uart
void DMA1_Channel4_5_6_7_IRQHandler(void)
{
 if((DMA1->ISR & DMA_ISR_TCIF4))
  {
    SET_BIT(DMA1->IFCR,DMA_IFCR_CGIF4);  // ������ ���� �������� ������ 
  //  memset((void*)MdmTXBuffer, 0, Mdm_TX_BUFFER_MAX_SIZE);
    CLEAR_BIT(DMA1_Channel4->CCR,DMA_CCR_EN);
    ModemTransmitEnable = TRANSMIT_ENABLE;
  }
if((DMA1->ISR & DMA_ISR_TCIF5) == DMA_ISR_TCIF5)
    {
	// ������ ���� �������� ������
	DMA1->IFCR |= DMA_IFCR_CTCIF5;
	DMA_ComModemRxBufReset();
	}
} 


/* USER CODE END 1 */
/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
