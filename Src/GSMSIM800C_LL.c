/*
//%%%b 
��� �������������� �������, � �� ������
//%%%e 
*/


//static uint8_t cnt_STR_SEND_OK=0;//###
#include "GlobalHandler.h"
#include "ATCom_SIM800.h"
// USER BEGIN
//extern _Setting Setting;
extern struct _FlagMessageTMR FlagMessageTMR;


extern uint8_t flag_end_task;
// USER END


SMessage Message;
void InitMessage(void);
void MakeMessage(void);
int GetMessage(void);
void ClrBitMessage(int index);
uint8_t GSMmodem_non_registration_GSM = 0;
//extern osMessageQId QueueGSM_RX_UARTHandle;
extern SemaphoreHandle_t xMdmRxSemaphore;
uint8_t cnt_element_QueueGSM_RX_UART;
portBASE_TYPE xStatus;
uint8_t GSMflag_power; // GSM_MUST_ENABLE - ���� �������� ������, GSM_MUST_DISABLE - ���� ��������� ������
Typeflag_BT GSMflag_BT;// BT_MUST_ENABLE - ���� �������� ������, BT_MUST_DISABLE - ���� ��������� ������
Typeflag_TCP GSMflag_TCP;
Typeflag_GPRS GSMflag_GPRS;
Typeflag_CALL GSMflag_CALL;
Typeflag_SMSOutput GSMflag_SMSOutput;
Typeflag_CALLOutput GSMflag_CALLOutput;



uint32_t pin_DTR;
GPIO_TypeDef *ptr_PxOUT_DTR;
GPIO_TypeDef *ptr_PxOUT_Power;
uint32_t pin_power;
GPIO_TypeDef *ptr_PxOUT_Pwrkey;
uint32_t pin_pwrkey;
USART_TypeDef *GSM_USART;

uint8_t availability_of_2_SIMcards;
GPIO_TypeDef *ptr_PORT_select_SIMcard;
uint16_t pin_select_SIMcard;
uint8_t  gsm_imei[16];
//int32_t delay_connect_internet = 0;
uint8_t cnt_error_TCP_connect=0;
uint16_t delay_receive_for_gsm_sms_ready;
uint32_t tick_count_message_from_GSM_msec;
uint32_t current_communication_gsm_number=0;
uint32_t tick_count_powerON_GSM_msec = 0;

uint8_t *ptr_buf_data_TCP;
uint8_t flag_buf_data_TCP=0; // 0 - ���������� ���� ����� ��� ���������, 
                             // 1 - ������������ ��������� �� ����� ��� �������� �������� ������ �� ���  �������, ����� �������� send ok
                             // 2 - ������������ ���������.
uint32_t cnt_byte_data_TCP=0;
uint8_t flag_cnt_byte_data_TCP=0;
uint8_t cntSEND_FAIL=0;
uint8_t cnt_error_CGATT_1 = 0;
uint8_t cnt_error_CGATT_0 = 0;
uint8_t cnt_error_CGATT_1_common = 0;
uint8_t cnt_error_speed_modem = 0;
uint8_t cnt_sim_card_error = 0;
uint8_t cnt_PLUS_CREG_0 = 0;
uint8_t cnt_CSQ_0 = 0;
uint8_t flag_AT_CIFSR_IP_addres_available = 0;
uint8_t CREG_volue;
uint8_t cnt_CONNECT_FAIL_server=0;
//uint8_t cnt_error_AT_CIICR = 0;
//uint8_t cnt_error_AT_CIFSR = 0;


const uint32_t mas_speed_uart[CNT_SPEED_UART] = { 115200, 19200, 38400, 9600, 57600, 230400};
// 4800 - �� ��������� HAL ���������� ��� ������� ���� 32 ���
// ��� ����������� �������� ������
struct _GSMFlag GSMFlag;
struct _GSMStatus GSMStatus;
struct _GSMInputSMS GSMInputSMS;
struct _GSMFlagMessage GSMFlagMessage;
 _GSMFlagMessageTCP GSMFlagMessageTCP;
 _GSMFlagMessageGPRS GSMFlagMessageGPRS; 
struct _GSMFlagMessageBT GSMFlagMessageBT;
struct _GSMFlagOutputSMS GSMFlagOutputSMS;
struct _GSMFlagOutputCALL GSMFlagOutputCALL;


//uint8_t  GSMstring_GSMModule[CNT_ARRAY_STRING_GSMModule][SIZE_ARRAY_ONE_STRING_GSMModule]; // ������ ��� �������� ����
uint8_t  ModemRxBuf[CNT_ARRAY_STRING_GSMModule * SIZE_ARRAY_ONE_STRING_GSMModule]; // ������ ��� �������� ����
//uint16_t  GSMcnt_string_GSMModule[CNT_ARRAY_STRING_GSMModule]; // ����� ���� � ���������� ���������
//uint8_t RX_Buff;     
uint8_t GSMcnt_message_for_gsm_module;// ����� �������� ���������
uint8_t number_indicator_message_for_gsm= 0;
uint8_t indicator_message_for_gsm = 0; // ������� ��������� �������� � ����������� ���������, 
                                      // ������������ ����� ������ ������ ��������� �� ��� �� ���������
                                      // �� ����������� ������ ������������ 1 ����
uint8_t *GSMptr_rx1; // ��������� �� ���� �������� ���������� �������� �� ������
uint8_t GSM_sequence_string[CNT_ARRAY_STRING_GSMModule] = {0}; // ������ ������������������ �������� ���������
uint8_t temp_number_indicator_message_for_gsm;
uint16_t GSMstatus_delay_init; // ����� ����� ��������������

uint8_t buf_str[BUF_STR_SIZE]; // ����� ��� ������ � ����������� �� ������� �� TCP
uint8_t level_gsm_signal; 
int16_t data_size_left = 300;
uint8_t GSM_network_name[24];
uint8_t CCID_SIM_card[24];
uint8_t message_SIM_card_balance[50];
//uint8_t BufferATTelNumberOutSMSMessages[100]; // �.�. ����� � ������� unicod
//uint8_t BufferATTelNumberOutSMSMessages[]="AT+CMGS=\"+79307000107\"\r";
// ������� (910 123 45 67) 20, 24,  28, 32, 36, 40, 44, 48, 52, 56
TypePtrURLPortServer PtrURLPortServer;
//-----------------------------------------------
const TypePtrParserCmdGSM PtrParserCmdGSM[]=
{
  {"OK",                MES_GSM_STR_OK},         
  {"ERROR",             MES_GSM_STR_ERROR},   
  {"SEND OK",           MES_GSM_STR_SEND_OK},
  {"SHUT OK",           MES_GSM_STR_SHUT_OK},
  {">",                 MES_GSM_STR_CHARACTER_MORE},  
  {"DATA ACCEPT:",      MES_GSM_STR_DATA_ACCEPT},
  {"+CIPSEND: ",        MES_GSM_STR_PLUS_CIPSEND},
  {"CONNECT OK",        MES_GSM_STR_CONNECT_OK},
  {"CONNECT FAIL",      MES_GSM_STR_CONNECT_FAIL}, 
  {"SEND FAIL",         MES_GSM_STR_SEND_FAIL},   
  {"+CSQ: ",            MES_GSM_STR_PLUS_CSQ},
  {"+COPS:",            MES_GSM_STR_PLUS_COPS},  
  {"+CUSD:",            MES_GSM_STR_PLUS_CUSD},   
  {"+CREG:",            MES_GSM_STR_PLUS_CREG},  
  {"+CGATT:",           MES_GSM_STR_PLUS_CGATT},
  {"Call Ready",        MES_GSM_STR_CALL_READY},
  {"SMS Ready",         MES_GSM_STR_SMS_READY},
  {"NORMAL POWER DOWN", MES_GSM_STR_NORMAL_POWER_OFF},
  {"+CPIN: NOT INSERTED", MES_GSM_STR_CPIN_NOT_INSERTED},  
  {"+PDP: DEACT",       MES_GSM_STR_PLUS_PDP_DEACT},
  {"+CMTI:",            MES_GSM_STR_PLUS_CMTI},
  {"RING",              MES_GSM_RING},  
  {"NO CARRIER",        MES_GSM_NO_CARRIER},  
  {"NO DIALTONE",       MES_GSM_NO_DIALTONE},    
  {"BUSY",              MES_GSM_BUSY},    
  {"NO ANSWER",         MES_GSM_NO_ANSWER},      
  {"PROCEEDING",        MES_GSM_PROCEEDING},     
  {"REMOTE",            MES_GSM_REMOTE},  
//  {"CLOSED",            MES_GSM_STR_CLOSED},
};
const uint8_t CNT_STR_GSM = sizeof(PtrParserCmdGSM)/sizeof(PtrParserCmdGSM[0]);

// #####################################################################################

void GSM_Init(GPIO_TypeDef *_ptr_PxOUT_Power,              // ��������� �� ������� ����� PxOUT ������ ������������ ������� GSM ������
           uint32_t _pin_power,    // ����� ������ ������������ ������� GSM ������
           GPIO_TypeDef *_ptr_PxOUT_Pwrkey,              // ��������� �� ������� ����� PxOUT ������ PWRKEY
           uint32_t _pin_pwrkey,   // ����� ������
 //          USART_TypeDef *_USART,          // ��. �� ����� ����������� ���������������� UART ������
 //          uint8_t _availability_of_2_SIMcards, // ������� 2-� ����
 //          GPIO_TypeDef *_ptr_PORT_select_SIMcard,
 //          uint16_t _pin_select_SIMcard,
           GPIO_TypeDef *_ptr_PxOUT_DTR,              
           uint32_t _pin_DTR)
{
  
  
  ptr_PxOUT_Power = _ptr_PxOUT_Power;
  pin_power = _pin_power;
  ptr_PxOUT_Pwrkey = _ptr_PxOUT_Pwrkey;
  pin_pwrkey = _pin_pwrkey;
  ptr_PxOUT_DTR = _ptr_PxOUT_DTR;
  pin_DTR = _pin_DTR;
 // GSM_USART = _USART;
 // availability_of_2_SIMcards = _availability_of_2_SIMcards;
 // ptr_PORT_select_SIMcard = _ptr_PORT_select_SIMcard;
 // pin_select_SIMcard = _pin_select_SIMcard;
  
   
   
   
   
   
   
   
   
   
   
   
   
   
  // ��������� ���������
  GSMflag_power = GSM_MUST_DISABLE;
  GSMflag_BT = BT_MUST_DISABLE;
 // HAL_GPIO_WritePin(_ptr_PxOUT_Power, _pin_power, GPIO_PIN_RESET);
//  HAL_GPIO_WritePin(_ptr_PxOUT_Pwrkey, _pin_pwrkey, GPIO_PIN_RESET); // ����� � M660 ON/OFF ����� ���������� � ���������. ���������� � ����, ��� �� ������ ���� ����������� � ���� ����������
//  HAL_GPIO_WritePin(ptr_PxOUT_DTR, pin_DTR, GPIO_PIN_RESET);
  // HAL_UART_Receive_IT(GSM_USART, &RX_Buff, 1);

  GSMStatus.power = POWER_NON;
  GSMStatus.init = INIT_NON;
  GSMStatus.speed = SPEED_NON_FOUND;
  GSMStatus.gprs = GPRS_NON_CONNECT;
  GSMStatus.TCP = TCP_NON_CONNECT;
  // ������������� �������� ������
  
}
// #####################################################################################
void GSMPower()
{
   if(GSMflag_power == GSM_MUST_ENABLE 
      && GSMStatus.power == POWER_NON)
   {
       // ����� ���� ������
 //      memset((uint8_t*)&FlagMessageTMR, 0, sizeof((uint8_t*)&FlagMessageTMR));
       Mdm_HardwareInit();
       CalculateMotoHourceGSM(MOTO_HOURCE_START);
      // StatusDevice.GSM_Modem_Power = _OK;
       cnt_sim_card_error = 0;
       cnt_PLUS_CREG_0 = 0;
       cnt_CSQ_0 = 0;
       level_gsm_signal = 0;
       flag_AT_CIFSR_IP_addres_available = 0;
       cnt_CONNECT_FAIL_server = 0;
       success_session_TCP = _FALSE;
       rez_GSM_ExtremalPowerDown = 0;
       InitMessage();
       memset((uint8_t*)&GSMFlag, 0, sizeof(GSMFlag));
       memset((uint8_t*)&GSMFlagMessage, 0, sizeof(GSMFlagMessage));
       memset((uint8_t*)&GSMFlagMessageTCP, 0, sizeof(GSMFlagMessageTCP));
       memset((uint8_t*)&GSMFlagMessageGPRS, 0, sizeof(GSMFlagMessageGPRS));       
       memset((uint8_t*)&GSMStatus, 0, sizeof(GSMStatus));
    //   memset((uint8_t*)&GSMInputSMS, 0, sizeof(GSMInputSMS));
    //   memset((uint8_t*)&GSMFlagMessageBT, 0, sizeof(GSMFlagMessageBT));
       memset((uint8_t*)&GSMFlagOutputSMS, 0, sizeof(GSMFlagOutputSMS));
    //   memset((uint8_t*)&GSMFlagOutputCALL, 0, sizeof(GSMFlagOutputCALL));
       xSemaphoreTake(xMdmRxSemaphore, 0);
       
      // successful_transfer_data_to_server = _FALSE; // ����� � GSM_TCPConnect
       
       flag_event = TELEM_EVENT_START_GSM_MODEM;
       flag_event_menu = flag_event;
       
       WriteTelemetry(TELEM_EVENT_POWER_ON, 0, 0, EVENT_BEGIN);// EVENT_END EVENT_BEGIN
       
       TransmitDebugMessageOptic("POWER_ON_PART0\r\n", 0);
       tick_count_powerON_GSM_msec = xTaskGetTickCount();
       GSM_DTR_OFF();
     //  HAL_GPIO_WritePin(ptr_PxOUT_Pwrkey, pin_pwrkey, GPIO_PIN_RESET);// ���������� ON/OFF � '1'
       GSM_PWRKEY_ON();
       osDelay(2);  
      // HAL_GPIO_WritePin(ptr_PxOUT_Power, pin_power, GPIO_PIN_SET);
       HiPower_ON |= BIT0;
       GSM_PWR_ON();
       osDelay(200);
       for(uint8_t i=0; i < 3; i++)
       {
          if(READ_BIT(GPIOD->IDR, GPIO_IDR_IDR_2) == RESET) // PG
          {// battery not connected or defective bust
             osDelay(200);
             if(i == 2)
             {
                if(StatusWarning.bit.battery_not_connected)
                {
                   GSMStatus.power = POWER_NON_BATARY;
                   return;
                }
                GSMStatus.power = POWER_ERROR; // for crach off
                return;
             }
          }
       }
        WriteTelemetry(TELEM_EVENT_POWER_ON, EVENT_OK, GSM_Get_current_communication_gsm_number() + 1, EVENT_END);// EVENT_END EVENT_BEGIN]
       WriteTelemetry(TELEM_EVENT_START_GSM_MODEM, 0, 0, EVENT_BEGIN);// EVENT_END EVENT_BEGIN
       //--------------------------------
       GSMStatus.power = POWER_ON_PART1;
       osDelay(2000); // 64 �� � �����

       GSM_PWRKEY_OFF();
       GSMStatus.power = POWER_ON_PART2;
       
   //    osDelay(2000 + 4000 * cnt_error_speed_modem); //
       osDelay(1000);
     //  HAL_GPIO_WritePin(ptr_PxOUT_Pwrkey, pin_pwrkey, GPIO_PIN_RESET);// ���������� ON/OFF � '1'
       GSM_PWRKEY_ON();
       for(uint8_t i = 0; i < 70; i++)
       {
          if(READ_BIT(GPIOD->IDR, GPIO_IDR_IDR_3)) // GSM Status
          {
             i = 70;
          }
          else
          {
               osDelay(150);
          }
       }
       osDelay(2000 + 2000 * cnt_error_speed_modem);
       GSMStatus.power = POWER_ON_PART3;
     //  HAL_UART_Receive_IT(GSM_USART, &RX_Buff, 1); // ���������� ��������� � Mdm_HardwareInit()
       
       WriteTelemetry(TELEM_EVENT_START_GSM_MODEM, EVENT_OK, GSM_Get_current_communication_gsm_number() + 1, EVENT_END);// EVENT_END EVENT_BEGIN
       flag_event = flag_event_menu = TELEM_EVENT_PARAM_GSM_MODEM;
       WriteTelemetry(TELEM_EVENT_PARAM_GSM_MODEM, 0, 0, EVENT_BEGIN);// EVENT_END EVENT_BEGIN
       delay_receive_for_gsm_sms_ready = 0;
       
//       osDelay(2000);
     
   }
   
   if(GSMStatus.power == POWER_ON_PART3)
   {
      if(GSMFlagMessage.SMS_READY)
      {
         GSMStatus.power = GSM_POWER_ON;
         tick_count_message_from_GSM_msec = xTaskGetTickCount();
         GSMStatus.speed = SPEED_FOUND;
       //  TransmitDebugMessageOptic("SPEED_FOUND\r\n");
      } 
      else
      {
         if(delay_receive_for_gsm_sms_ready > 3000)
         {
            GSMStatus.power = GSM_POWER_ON;
            tick_count_message_from_GSM_msec = xTaskGetTickCount();
            GSMStatus.speed = SPEED_NON_FOUND;
         }
      }
   }
   if(GSMFlagMessage.CPIN_NOT_INSERTED == ACCEPTED_MESSAGE)
   {
      if(StatusDevice.SIM_card_non)
         GSMStatus.power = POWER_NON_PIN_SIM;
      else
         GSMStatus.power = POWER_CPIN_NOT_INSERTED;
      TransmitDebugMessageOptic("SIM_CARD_ERROR\r\n", 0);
   }
   // ����������
   if(GSMflag_power == GSM_MUST_DISABLE 
      && GSMStatus.power == GSM_POWER_ON)
   {
      if(GSMFlagMessage.AT_CPOWD_1 == NON_MESSAGE)
      {
     //    LedBlueMode = LED_MODE_GSM_POWER_ON;
         GSMFlagMessage.AT_CPOWD_1 = SENT_MESSAGE;
         Mdm_Transmit2(AT_CPOWD_1);
         //HAL_UART_Transmit_IT(GSM_USART, (uint8_t*)AT_CPOWD_1, sizeof(AT_CPOWD_1)-1);
      }
      if(GSMFlagMessage.AT_CPOWD_1 == ACCEPTED_MESSAGE)
      {
        // osDelay(50);  
        // HAL_GPIO_WritePin(ptr_PxOUT_Power, pin_power, GPIO_PIN_RESET);  
         WriteTelemetry(TELEM_EVENT_OFF_GSM_MODEM, EVENT_OK, GSM_Get_current_communication_gsm_number(), EVENT_END);// EVENT_END EVENT_BEGIN 
         WriteTelemetry(TELEM_EVENT_POWER_OFF, 0, 0, EVENT_BEGIN);// EVENT_END EVENT_BEGIN 
         HiPower_ON &= ~BIT0;
         if (HiPower_ON == 0)
             GSM_PWR_OFF();
         GSMStatus.power = POWER_NON;
         memset((uint8_t*)&GSMFlagMessage, 0, sizeof(GSMFlagMessage));
         Mdm_HardwareDeInit();
    //     POWER_MCU_BAT3_ON();
         
         CalculateMotoHourceGSM(MOTO_HOURCE_STOP);
         osDelay(500);
      //   StatusDevice.GSM_Modem_Power_mode = _FALSE;
      //   StatusDevice.GSM_Modem_Power_optic = _FALSE;
      //   StatusDevice.GSM_Modem_Power_switch = _FALSE;
         osDelay(500);
         TransmitDebugMessageOptic("POWER_NON\r\n", 0);
         flag_event = flag_event_menu = TELEM_EVENT_OFF_GSM_MODEM;
         
         // check PG pin
         for(uint8_t i = 0; i < 30;  i++)
         {
            if(READ_BIT(GPIOD->IDR, GPIO_IDR_IDR_2) == RESET)
            {// battery not connected or defective bust
               WriteTelemetry(TELEM_EVENT_POWER_OFF, EVENT_OK, GSM_Get_current_communication_gsm_number(), EVENT_END);// EVENT_END EVENT_BEGIN 
               break;
            }
            osDelay(100);
            if(i == 29)
              WriteTelemetry(TELEM_EVENT_POWER_OFF, EVENT_OFF_EXTREMAL_GSM_MODEM, GSM_Get_current_communication_gsm_number(), EVENT_END);// EVENT_END EVENT_BEGIN 
         }
         flag_event_menu = TELEM_EVENT_INPUT_MODEM;
       }
   }
}
// #####################################################################################
uint8_t FindSpeedUARTGSMModule()
{
   if(GSMStatus.speed == SPEED_NON_FOUND && GSMStatus.power == GSM_POWER_ON)
   {
      for(uint8_t i=0; i < 5; i++)
      {
         osDelay(2000);
         if((GSMFlagMessage.CALL_READY
         || GSMFlagMessage.CPIN)
         && GSMFlagMessage.SMS_READY)
         {// ��������� ����, ������ �������� uart ������
            GSMStatus.speed = SPEED_FOUND;
            GSMFlagMessage.IPR = ACCEPTED_MESSAGE;
            GSMFlagMessage.AT_W = ACCEPTED_MESSAGE;
       //     GSMFlagMessage.AT_BTHOST = ACCEPTED_MESSAGE;            
            return GSMStatus.speed;
         }
      }
      // ��������� �� ����, ������ �������� UART �� �������������, ��� ������ � ������ ��������������� ��������
      for(uint8_t i=0; i < CNT_SPEED_UART; i++)
      {
         Mdm_USARTInit(mas_speed_uart[i]);
         //---------------------------
         for(int j=0; j < 5; j++)
         {
            GSMFlagMessage.AT = SENT_MESSAGE;
         //   HAL_UART_Transmit_IT(GSM_USART, (uint8_t*)AT_AT, sizeof(AT_AT)-1);
            Mdm_Transmit2(AT_AT);
            osDelay(100);
            if(GSMFlagMessage.AT == ACCEPTED_MESSAGE)
            {
               GSMStatus.speed = SPEED_FOUND;
             //  TransmitDebugMessageOptic("SPEED_FOUND\r\n");
               /*
               if(i == 4) // ��������� �������� ������������� 57600 ���
               { 

               }
               */
               return GSMStatus.speed;
            }
         }
      }
      // �� ���� �������� �� �������. ������.
      ++cnt_error_speed_modem;
      if(cnt_error_speed_modem > 2)
      {
         GSMStatus.speed = SPEED_FOUND_ERROR;
         cnt_error_speed_modem = 0;
         TransmitDebugMessageOptic("SPEED_FOUND_ERROR\r\n", 0);
      }
      else
      {
         WriteTelemetry(TELEM_EVENT_PARAM_GSM_MODEM, EVENT_GSM_ERROR, GSM_Get_current_communication_gsm_number() + 1, EVENT_END);// EVENT_END EVENT_BEGIN
         Mdm_HardwareDeInit();
         
         GSM_PWRKEY_OFF();
         osDelay(1750); //
         GSM_PWRKEY_ON();
         GSMStatus.power = POWER_NON;
         for(uint8_t i = 0; i < 20; i++)
         {
            if(READ_BIT(GPIOD->IDR, GPIO_IDR_IDR_3))
            {// status modem
               osDelay(150);
            }
            else
            {
               i = 20;
            }
         }
         osDelay(1000);
      }
   }
   return GSMStatus.speed;
}
// #####################################################################################
void InitGSMModule()
{
   if(GSMStatus.power == GSM_POWER_ON
      && GSMStatus.init != INIT_OK
      && GSMStatus.speed == SPEED_FOUND )
   {
      if(GSMFlagMessage.IPR == NON_MESSAGE)
      {
         GSMFlagMessage.IPR = SENT_MESSAGE;
       //  HAL_UART_Transmit_IT(GSM_USART, (uint8_t*)AT_IPR_57600, sizeof(AT_IPR_57600)-1);
         Mdm_Transmit2(AT_IPR_115200);
      }
      if(GSMFlagMessage.IPR == ACCEPTED_MESSAGE && GSMFlagMessage.AT_W == NON_MESSAGE)
      {
       //  (*GSM_USART).Init.BaudRate = 57600;
       //  if (HAL_UART_Init(GSM_USART) != HAL_OK)
       //  {
      //      _Error_Handler(__FILE__, 2);
      //   }
      //   HAL_UART_Receive_IT(GSM_USART, &RX_Buff, 1);
         Mdm_USARTInit(SPEED_UART_DEFAULT);
         osDelay(300); // ��������, �.�. �� ������ ����� �� ����� ������������� �������
         GSMFlagMessage.AT_W = SENT_MESSAGE;
       //  HAL_UART_Transmit_IT(GSM_USART, (uint8_t*)AT_W, sizeof(AT_W)-1);
         Mdm_Transmit2(AT_W);
      }
/*
      if(GSMFlagMessage.AT_W == ACCEPTED_MESSAGE && GSMFlagMessage.AT_BTHOST == NON_MESSAGE)
      {
         GSMFlagMessage.AT_BTHOST = SENT_MESSAGE;
       //  HAL_UART_Transmit_IT(GSM_USART, (uint8_t*)AT_BTHOST, sizeof(AT_BTHOST)-1);
         
         Mdm_Transmit2(AT_BTHOST);
      }
      */
      if(GSMFlagMessage.AT_W == ACCEPTED_MESSAGE && GSMFlagMessage.AT_ATE0 == NON_MESSAGE)
      {
         GSMFlagMessage.AT_ATE0 = SENT_MESSAGE;
       //  HAL_UART_Transmit_IT(GSM_USART, (uint8_t*)AT_ATE0, sizeof(AT_ATE0)-1);
         Mdm_Transmit2(AT_ATE0);
      }
      // �������� SIM �����
      if(GSMFlagMessage.AT_ATE0 == ACCEPTED_MESSAGE && GSMFlagMessage.AT_CCID == NON_MESSAGE)
      {
        
         WriteTelemetry(TELEM_EVENT_PARAM_GSM_MODEM, EVENT_OK, GSM_Get_current_communication_gsm_number() + 1, EVENT_END);// EVENT_END EVENT_BEGIN
         flag_event = flag_event_menu = TELEM_EVENT_CHECK_SIM_CARD;
         WriteTelemetry(TELEM_EVENT_CHECK_SIM_CARD, 0, 0, EVENT_BEGIN);// EVENT_END EVENT_BEGIN
        
         GSMFlagMessage.AT_CCID = SENT_MESSAGE;
       //  HAL_UART_Transmit_IT(GSM_USART, (uint8_t*)AT_CCID, sizeof(AT_CCID)-1);
         Mdm_Transmit2(AT_CCID);
      }
      if(GSMFlagMessage.AT_CCID == ACCEPTED_MESSAGE_ERROR)
      {
         ++cnt_sim_card_error;
         if(cnt_sim_card_error > 2)
         {
            if(StatusDevice.SIM_card_non)
               GSMStatus.init = SIM_CARD_NON;
            else
               GSMStatus.init = SIM_CARD_ERROR;
         }
         else
         {
            osDelay(2000 + 2000 * cnt_sim_card_error);
            GSMFlagMessage.AT_CCID = NON_MESSAGE;
         }
      }
      if(GSMFlagMessage.AT_CCID == ACCEPTED_MESSAGE && GSMFlagMessage.AT_CGSN == NON_MESSAGE)
      {
         GSMFlagMessage.AT_CGSN = SENT_MESSAGE;
       //  HAL_UART_Transmit_IT(GSM_USART, (uint8_t*)AT_CGSN, sizeof(AT_CGSN)-1);
         ++current_communication_gsm_number; 
         Eeprom_WriteDword((uint32_t)&current_communication_gsm_number_eeprom,  current_communication_gsm_number);
         Mdm_Transmit2(AT_CGSN);
      }
      if(GSMFlagMessage.AT_CGSN == ACCEPTED_MESSAGE && GSMFlagMessage.AT_CREG == NON_MESSAGE)
      {
         if(!cnt_PLUS_CREG_0)
         {
            WriteTelemetry(TELEM_EVENT_CHECK_SIM_CARD, EVENT_OK, GSM_Get_current_communication_gsm_number(), EVENT_END);// EVENT_END EVENT_BEGIN
            flag_event = flag_event_menu = TELEM_EVENT_CHECK_REGISTRATION_GSM;
            StatusDevice.GSM_modem_SIM_card_non = 0;
            WriteTelemetry(TELEM_EVENT_CHECK_REGISTRATION_GSM, 0, 0, EVENT_BEGIN);// EVENT_END EVENT_BEGIN
         }
        
         GSMFlagMessage.AT_CREG = SENT_MESSAGE;
       //  HAL_UART_Transmit_IT(GSM_USART, (uint8_t*)AT_CGSN, sizeof(AT_CGSN)-1);
         Mdm_Transmit2(AT_CREG);
      }
      
      
      
      
      //###
   /*   if(GSMFlagMessage.AT_CGSN == ACCEPTED_MESSAGE && GSMFlagMessage.AT_CSCLK_1 == NON_MESSAGE)
      {// ������� ��� ��������� ������� DTR
         GSMFlagMessage.AT_CSCLK_1 = SENT_MESSAGE;
       //  HAL_UART_Transmit_IT(GSM_USART, (uint8_t*)AT_CSCLK_1, sizeof(AT_CSCLK_1)-1);
         Mdm_Transmit2(AT_CSCLK_1);
      }*/
      // ������� �������
     // if(GSMFlagMessage.AT_CSCLK_1 == ACCEPTED_MESSAGE && GSMFlagMessage.AT_CSQ == NON_MESSAGE)
      /*
      if(GSMFlagMessage.AT_CREG == ACCEPTED_MESSAGE && GSMFlagMessage.AT_CSQ == NON_MESSAGE)
      {
         //%%%b
         WriteTelemetry(TELEM_EVENT_CHECK_REGISTRATION_GSM, EVENT_OK, GSM_Get_current_communication_gsm_number(), EVENT_END);// EVENT_END EVENT_BEGIN
         flag_event = flag_event_menu = TELEM_EVENT_CHECK_LEVEL_GSM_SIGNAL;
         WriteTelemetry(TELEM_EVENT_CHECK_LEVEL_GSM_SIGNAL, 0, 0, EVENT_BEGIN);// EVENT_END EVENT_BEGIN
         //%%%e
         
       
         GSMFlagMessage.AT_CSQ = SENT_MESSAGE;
      //   HAL_UART_Transmit_IT(GSM_USART, (uint8_t*)AT_CSQ, sizeof(AT_CSQ)-1);
           Mdm_Transmit2(AT_CSQ);
      }
      */
      // ��� GSM ����
      if(GSMFlagMessage.AT_CREG == ACCEPTED_MESSAGE && GSMFlagMessage.AT_COPS == NON_MESSAGE)
      {
        
      //   WriteTelemetry(TELEM_EVENT_CHECK_LEVEL_GSM_SIGNAL, level_gsm_signal, GSM_Get_current_communication_gsm_number(), EVENT_END);// EVENT_END EVENT_BEGIN

        
         GSMFlagMessage.AT_COPS = SENT_MESSAGE;
       //  HAL_UART_Transmit_IT(GSM_USART, (uint8_t*)AT_COPS, sizeof(AT_COPS)-1);
          Mdm_Transmit2(AT_COPS);
      }

      // ������ SIM �����
      if(GSMFlagMessage.AT_COPS == ACCEPTED_MESSAGE && GSMFlagMessage.AT_CUSD == NON_MESSAGE)
      {
         if(MdmNumberBalans[0] != 0)
         {
            memset(buf_str, 0, BUF_STR_SIZE);
            sprintf((char*)buf_str, (char*)AT_CUSD, (char*)MdmNumberBalans);
            GSMFlagMessage.AT_CUSD = SENT_MESSAGE;
          //  HAL_UART_Transmit_IT(GSM_USART, (uint8_t*)buf_str, strlen((char*)buf_str));
            Mdm_Transmit2(buf_str);
         }
         else
         {
            __NOP();
            GSMFlagMessage.AT_CUSD = ACCEPTED_MESSAGE;
            sprintf((char*)message_SIM_card_balance, "%s", "NO NUMBER PHONE BALANCE");
         }
      }      
      //------------------------------------------------
      if(GSMFlagMessage.AT_CUSD == ACCEPTED_MESSAGE)
      {
         GSMStatus.init = INIT_OK;
         //%%%b
    //     WriteArchiveSystemToFlash(GSM_CHECK_GSM, (int8_t)level_gsm_signal, SYSTEM_EVENT_END);
    //     WriteArchiveSystemToFlash(GSM_GPRS_UP, 0, SYSTEM_EVENT_BEGIN);
         //%%%e
         WriteTelemetry(TELEM_EVENT_CHECK_REGISTRATION_GSM, CREG_volue, GSM_Get_current_communication_gsm_number(), EVENT_END);// EVENT_END EVENT_BEGIN
         GSMstatus_delay_init = 0;      // ������ ������� ������� ����� �������������  
      }
   }
}

// #####################################################################################
void      GSM_Set_TCPFlag(Typeflag_TCP _GSMflag_TCP)
{
  GSMflag_TCP = _GSMflag_TCP;
  if(GSMflag_TCP == TCP_MUST_DISABLE)
  {
     memset((uint8_t*)&GSMFlagMessageTCP, 0, sizeof(GSMFlagMessageTCP));
 //    GSMStatus.gprs = GPRS_NON_CONNECT;
     GSMStatus.TCP = TCP_NON_CONNECT;
  }
}
/*!
Edite GPRSFlag need or not need to connect the GPRS service
\param[in] Typeflag_GPRS GPRS_MUST_DISABLE  GPRS_MUST_ENABLE
*/   
void GSM_Set_GPRSFlag(Typeflag_GPRS _GSMflag_GPRS)
{
  GSMflag_GPRS = _GSMflag_GPRS;
  if(GSMflag_GPRS == GPRS_MUST_DISABLE)
  {
     memset((uint8_t*)&GSMFlagMessageGPRS, 0, sizeof(GSMFlagMessageGPRS));
     GSMStatus.gprs = GPRS_NON_CONNECT;
  }
}
// #####################################################################################
void GSM_Set_flag_power(uint8_t _GSMflag_power)
{
   if(GSMflag_power == GSM_MUST_DISABLE && _GSMflag_power == GSM_MUST_ENABLE)
      memset((uint8_t*)&GSMStatus, 0, sizeof(GSMStatus));     
   GSMflag_power = _GSMflag_power;
   tick_count_message_from_GSM_msec = xTaskGetTickCount();
   if(GSMflag_power == GSM_MUST_DISABLE)
   {
      GSMflag_BT = BT_MUST_DISABLE;
      GSMflag_TCP = TCP_MUST_DISABLE;
      GSMStatus.BT = BT_NON_CONNECT;
      GSMFlagOutputSMS.status = SMS_NON_TRANSMITTED;
      GSMflag_SMSOutput = SMS_NON_MUST_OUTPUT;
      GSMflag_CALLOutput = CALL_NON_MUST_OUTPUT;
   }
}     
// #####################################################################################
//������/��������� ��������
uint8_t GSM_Get_flag_power()
{
   return GSMflag_power;
}
// #####################################################################################
void GSM_increment_delay()
{
   ++GSMstatus_delay_init;
   //++delay_connect_internet;
   ++delay_receive_for_gsm_sms_ready;
}
// #####################################################################################


// #####################################################################################
void fGSM_RX_UARTTask()
{
   uint8_t rez_find = _FALSE;
   uint16_t size;
   int Index;
   //xStatus = xQueuePeek( QueueGSM_RX_UARTHandle, &temp_number_indicator_message_for_gsm, portMAX_DELAY ); 
   flag_end_task |= 0x02;
   Index = GetMessage();   
   //if( xStatus == pdPASS )
   if(Index != -1)
   {
      // ������ ������� ������� �� ������� ��� ��������� 
      // The data was successfully received from the queue without cutting out
     TransmitDebugMessageOptic((uint8_t*)Message.String[Index], 0);// &GSMstring_GSMModule[temp_number_indicator_message_for_gsm-1][0], 0);
     //size = strlen((char*)&GSMstring_GSMModule[temp_number_indicator_message_for_gsm-1][0]); 
     size = strlen(Message.String[Index]);
     ClrBitMessage(Index);
     for(uint8_t i = 0; i < CNT_STR_GSM; i++)
     {
        //if(strstr((char*)&GSMstring_GSMModule[temp_number_indicator_message_for_gsm-1][0], PtrParserCmdGSM[i].Str) != 0)
	 if(strstr((char*)Message.String[Index], PtrParserCmdGSM[i].Str) != 0)
      //  if(strcmp((char*)&GSMstring_GSMModule[temp_number_indicator_message_for_gsm-1][0], PtrParserCmdGSM[i].Str) == 0)  
        {// �������
           if((i == 0 && size > 3) || (i == 1 && size > 6))
           { // ��� �� �� ��� ��������, � �� ������ ������
             __NOP();
           }
           else
           {
          //    TransmitDebugMessageOptic((uint8_t*)&GSMstring_GSMModule[temp_number_indicator_message_for_gsm-1][0]);
              rez_find = ParsingMessageForGSM(PtrParserCmdGSM[i].message_GSM, (uint8_t*)Message.String[Index]);
              break;
           }
        }
     }
     
     if(rez_find == _FALSE)
     {
        if(GSMReadSTR_Digit((uint8_t*)Message.String[Index]) == 1) 
          rez_find = _OK;
     }
     if(rez_find == _FALSE && GSMStatus.TCP == TCP_YES_CONNECT && size > 2)
     {
     //  TransmitDebugMessageOptic((uint8_t*)&GSMstring_GSMModule[temp_number_indicator_message_for_gsm-1][0]);
       if(Message.String[Index][0] != '+')
       {
         /*
          if(ReadMessageTMR((char*)Message.String[Index]) == 0 && size > 5)
             FlagMessageTMR.command = MESSAGE_ERROR;
         */
         for(uint8_t i = 0; i < 10; i++)
         {
            if(GSM_Get_Status_AT_CIPSEND() == NON_MESSAGE)
              break;
            else
              osDelay(100);
         }
         osDelay(100);
         ReadMessageTMR((char*)Message.String[Index]);
       }
     }
   }   
}
// #####################################################################################
uint8_t ParsingMessageForGSM(uint8_t CmdGSM, uint8_t *ptr_buf)
{
   uint8_t rez = _OK;
   switch(CmdGSM)
   {
   case MES_GSM_STR_OK:
        GSMReadSTR_OK();
        break;   
   case MES_GSM_STR_ERROR:
        if(GSMFlagMessage.AT_CCID == SENT_MESSAGE)     
        {
           GSMFlagMessage.AT_CCID = ACCEPTED_MESSAGE_ERROR;
        }

        if(GSMFlagMessageGPRS.AT_CIICR == SENT_MESSAGE)
        {
              //++cnt_error_AT_CIICR;
           GSMFlagMessageGPRS.AT_CIICR = ACCEPTED_MESSAGE_ERROR;
              osDelay(2800);
        }
        if(GSMFlagMessageGPRS.AT_CIFSR == SENT_MESSAGE)
        {
              //++cnt_error_AT_CIFSR;
           GSMFlagMessageGPRS.AT_CIFSR = ACCEPTED_MESSAGE_ERROR;
           osDelay(2800);
        }           
        GSMFlagMessage.ERROR = ACCEPTED_MESSAGE;
        break;
   case MES_GSM_STR_SEND_OK:
        if(GSMFlagMessageTCP.MessageTX == SENT_MESSAGE)
        {
           GSMFlagMessageTCP.MessageTX = ACCEPTED_MESSAGE;
        /*   if(flag_buf_data_TCP == 1)
           {
              HAL_UART_Abort_IT(GSM_USART); // ����� ����� � ��������
              HAL_UART_Receive_DMA(GSM_USART, ptr_buf_data_TCP, cnt_byte_data_TCP);
              flag_buf_data_TCP = 2;
              flag_cnt_byte_data_TCP = 0;
           }
           */
        }
        break;
   case MES_GSM_STR_SEND_FAIL:
        GSMFlagMessageTCP.SEND_FAIL = ACCEPTED_MESSAGE;
        break;
   case MES_GSM_STR_CALL_READY:
        GSMFlagMessage.CALL_READY = ACCEPTED_MESSAGE;
        break;
   case MES_GSM_STR_SMS_READY:
        GSMFlagMessage.SMS_READY = ACCEPTED_MESSAGE;
        break;       
   case MES_GSM_STR_PLUS_CREG:  
        {
          char* ptr;
          ptr = strchr((char*)ptr_buf, ',') + 1;
          CREG_volue  = *ptr - 0x30;
          if(CREG_volue == 1)
          {
            StatusDevice.GSMmodem_non_registration_GSM = 0;
            GSMmodem_non_registration_GSM = 0;
          }
          else
          {
            ///StatusDevice.GSMmodem_non_registration_GSM = 1;
            GSMmodem_non_registration_GSM = 1;
          }
        }
        break;
   case MES_GSM_STR_PLUS_CGATT:  
        {
          char* ptr;
          ptr = strchr((char*)ptr_buf, ' ') + 1;
          if(*ptr == '1')
          {
            GSMFlagMessageGPRS.PLUS_CGATT = ACCEPTED_MESSAGE;
          }
          else
            GSMFlagMessageGPRS.PLUS_CGATT = ACCEPTED_MESSAGE_ERROR;
        }
        break;        
   case MES_GSM_STR_PLUS_CSQ:
        {
        char* end_ptr;
        level_gsm_signal = strtoul((char*)&ptr_buf[6], &end_ptr, 10);
        }
        break;        
   case MES_GSM_STR_PLUS_CIPSEND:
        {
        char* end_ptr;
        data_size_left = strtoul((char*)&ptr_buf[10], &end_ptr, 10) - 50;
        }
        break;           
   case MES_GSM_STR_PLUS_COPS:
        {
           char *end_ptr, *begin_ptr;
           begin_ptr = strchr((char*)ptr_buf, '"');
           if(begin_ptr)
           { 
              ++begin_ptr;
              if(end_ptr = strchr(begin_ptr, '"'))
                *end_ptr = 0;
              strncpy((char*)GSM_network_name, begin_ptr, sizeof(GSM_network_name)); // +
           }
        }
        break;
   case MES_GSM_STR_PLUS_CUSD:
        {
        char *end_ptr, *begin_ptr;
        memset((char*)message_SIM_card_balance, 0, sizeof(message_SIM_card_balance));
        begin_ptr = strchr((char*)ptr_buf, '"') + 1;
        end_ptr = strchr(begin_ptr, '"');
        *end_ptr = 0;
        DecodeUcs2(begin_ptr, (char*)message_SIM_card_balance, sizeof(message_SIM_card_balance)-1);
        GSMFlagMessage.AT_CUSD = ACCEPTED_MESSAGE;
        }
        break;
   case MES_GSM_STR_CONNECT_OK:
        if(GSMFlagMessageTCP.AT_CIPSTART == SENT_MESSAGE)
        {
           GSMFlagMessageTCP.AT_CIPSTART = ACCEPTED_MESSAGE;
           GSMFlagMessageTCP.CONNECT_OK = ACCEPTED_MESSAGE;
           tick_count_message_from_server_msec = SystemUpTimeSecond;
        }
        break;     
   case MES_GSM_STR_CONNECT_FAIL:
   case MES_GSM_STR_PLUS_PDP_DEACT:
        GSMFlagMessageTCP.CONNECT_FAIL = ACCEPTED_MESSAGE;
        break;           
   case MES_GSM_STR_CHARACTER_MORE:
        if(GSMFlagMessageTCP.AT_CIPSEND == SENT_MESSAGE)
        {
           GSMFlagMessageTCP.AT_CIPSEND = ACCEPTED_MESSAGE;
        }
        if(GSMFlagMessageBT.AT_BTSPPSEND == SENT_MESSAGE)
        {
           GSMFlagMessageBT.AT_BTSPPSEND = ACCEPTED_MESSAGE;
        }
        if(GSMFlagOutputSMS.AT_CMGS == SENT_MESSAGE)
        {
           GSMFlagOutputSMS.AT_CMGS = ACCEPTED_MESSAGE;
        }
        break;   
   case MES_GSM_STR_DATA_ACCEPT:
        GSMFlagMessageTCP.MessageTX = ACCEPTED_MESSAGE;
        break;        
   case MES_GSM_STR_NORMAL_POWER_OFF:
        if(GSMFlagMessage.AT_CPOWD_1 == SENT_MESSAGE)
        {
           GSMFlagMessage.AT_CPOWD_1 = ACCEPTED_MESSAGE;
        }
        break;  
   case MES_GSM_STR_CPIN_NOT_INSERTED:
        GSMFlagMessage.CPIN_NOT_INSERTED = ACCEPTED_MESSAGE;
        break;          
   case MES_GSM_STR_SHUT_OK:
        if(GSMFlagMessageGPRS.AT_CIPSHUT == SENT_MESSAGE)
        {
           GSMFlagMessageGPRS.AT_CIPSHUT = ACCEPTED_MESSAGE;
        }
        if(GSMFlagMessageTCP.AT_CIPSHUT_Close == SENT_MESSAGE)
        {
           GSMFlagMessageTCP.AT_CIPSHUT_Close = ACCEPTED_MESSAGE;
        }
        break;       
   case MES_GSM_STR_CLOSED:
        if(GSMFlagMessageTCP.CONNECT_OK != ACCEPTED_MESSAGE)
        {
          GSMFlagMessageTCP.CONNECT_FAIL = ACCEPTED_MESSAGE;
        }
        else
          rez = _FALSE;
        break;
   default:break;
   }
   return rez;
}
/**********************************************************************************************************
** Description :                                                                                         **
***********************************************************************************************************
** In  :                                                                                          **
** Out :                                                                                     **
**********************************************************************************************************/
int DetectHexChr(char *In)
{
int i,ret=1;
for(i=0;i<8;i++)
	{
	if(!(In[i]>='0' && In[i]<='9') && !(In[i]>='A' && In[i]<='F'))
		{
		ret=0;
		break;
		}
	}
return ret;
}
/**********************************************************************************************************
** Description :                                                                                         **
***********************************************************************************************************
** In  :                                                                                          **
** Out :                                                                                     **
**********************************************************************************************************/
unsigned char HexToChr(char *str)
{
int i;
unsigned char chr=0;
for(i=0; i<2; i++)
	{
	chr <<= 4;
	if(str[i] >= '0' && str[i] <= '9')
		chr |=  str[i] - '0';
	else if(str[i] >= 'A' && str[i] <= 'F')
		chr |= str[i] - 'A' + 10;
	else if(str[i] >= 'a' && str[i] <= 'f')
		chr |= str[i] - 'a' + 10;
	}
return chr;
}
/**********************************************************************************************************
** Description :                                                                                         **
***********************************************************************************************************
** In  :                                                                                          **
** Out :                                                                                     **
**********************************************************************************************************/
char GetCP1252Code(uint16_t Ucs)
{
char Chr=(char)Ucs;
// ���������� ��� ������� '�'
if(Ucs==0x2116)
	Chr=0xB9;
// ���������� ��� ������� '�'
else if(Ucs==0x0451)
	Chr=0xB8;
// ���������� ��� ������� '�'
else if(Ucs==0x0401)
	Chr=0xA8;
// ��� ��������� ������� �����
else if(Ucs>=0x0410)
	Chr=Ucs-0x0410+0xC0;
return Chr;
}
/**********************************************************************************************************
** Description :                                                                                         **
***********************************************************************************************************
** In  :                                                                                          **
** Out :                                                                                     **
**********************************************************************************************************/

void DecodeUcs2(char *In, char *Out, int SizeOut)
{
uint16_t ChrUcs;
uint16_t len;
uint16_t i,k;
if(DetectHexChr(In))
	{
	len=strlen(In)/4;
	for(i=0,k=0;i<len;i++,k++)
		{
		if(k==SizeOut)
			break;
		ChrUcs=0;
		for(int j=0;j<4;j+=2)
			ChrUcs = (ChrUcs<<8) | HexToChr(In+(i*4)+j);
		Out[k]=GetCP1252Code(ChrUcs);
		}
	}
else
	strncpy(Out,In,SizeOut);
}
// #####################################################################################
uint8_t GSM_GPRSConnect(const uint8_t *ptr_APNAddress, const uint8_t *APN_login, const uint8_t *APN_passeord)
{
   if( GSMStatus.gprs != GPRS_YES_CONNECT// GSMStatus.TCP != TCP_YES_CONNECT
      && GSMflag_GPRS == GPRS_MUST_ENABLE
      && GSMflag_BT == BT_MUST_DISABLE
      && GSMStatus.init == INIT_OK
      && GSMStatus.power == GSM_POWER_ON
      && GSMflag_power == GSM_MUST_ENABLE
      && GSMstatus_delay_init >= PAUSE_AFTER_POWER_ON) // ������ n ��� ����� �������������  
   {
      // �����
      if(*PtrURLPortServer.ptr_url_server == 0 )
        return GPRS_ERROR_CONNECT;
      if(GSMFlagMessageGPRS.AT_CIPSHUT == NON_MESSAGE)
      {
         cnt_error_CGATT_1 = 0;
         cnt_error_CGATT_0 = 0;
         cnt_error_CGATT_1_common = 0;
         //cnt_error_AT_CIICR = 0;
         ///cnt_error_AT_CIFSR = 0;

         flag_event = flag_event_menu = TELEM_EVENT_CHECK_REGISTRATION_GPRS;
         WriteTelemetry(TELEM_EVENT_CHECK_REGISTRATION_GPRS, 0, 0, EVENT_BEGIN);// EVENT_END EVENT_BEGIN
        
         //delay_connect_internet = 0;  //+++ v 0.03
       //  GSMStatus.TCP =  TCP_CONNECTING;
         GSMStatus.gprs =  GPRS_CONNECTING;
         //successful_transfer_data_to_server = _FALSE;
         GSMFlagMessageGPRS.AT_CIPSHUT = SENT_MESSAGE;
        // HAL_UART_Transmit_IT(GSM_USART, (uint8_t*)AT_CIPSHUT, sizeof(AT_CIPSHUT)-1);
         Mdm_Transmit2(AT_CIPSHUT);
      }
      // 1 ����������
      if(GSMFlagMessageGPRS.AT_CIPSHUT == ACCEPTED_MESSAGE && GSMFlagMessageGPRS.AT_CIPMUX_0 == NON_MESSAGE)
      {
         //LedBlueMode = LED_MODE_TCP_CONNECTION;
         osDelay(200);
         GSMFlagMessageGPRS.AT_CIPMUX_0 = SENT_MESSAGE;
        // HAL_UART_Transmit_IT(GSM_USART, (uint8_t*)AT_CIPMUX_0, sizeof(AT_CIPMUX_0)-1);
         Mdm_Transmit2(AT_CIPMUX_0);
      }
      
      if(GSMFlagMessageGPRS.AT_CIPMUX_0 == ACCEPTED_MESSAGE && GSMFlagMessageGPRS.AT_CGATT_CHANGE == NON_MESSAGE)
      {
         GSMFlagMessageGPRS.AT_CGATT_CHANGE = SENT_MESSAGE;
         Mdm_Transmit2(AT_CGATT_CHANGE);
      }
      
      if(GSMFlagMessageGPRS.AT_CIICR == ACCEPTED_MESSAGE_ERROR 
         || GSMFlagMessageGPRS.AT_CIFSR == ACCEPTED_MESSAGE_ERROR
       /*  || GSMFlagMessageGPRS.CONNECT_FAIL == ACCEPTED_MESSAGE*/)
      {
        /*
         GSMFlagMessageGPRS.AT_CIICR = NON_MESSAGE;
         GSMFlagMessageGPRS.AT_CIFSR = NON_MESSAGE;
         GSMFlagMessageGPRS.AT_CIPSTART = NON_MESSAGE;
         GSMFlagMessageGPRS.AT_CGATT_CHANGE = ACCEPTED_MESSAGE_ERROR;
         GSMFlagMessage.ERROR = NON_MESSAGE;
         GSMFlagMessageGPRS.AT_CGATT_1 = NON_MESSAGE;
         GSMFlagMessageGPRS.AT_CGATT_0 = NON_MESSAGE;
        */
         memset((uint8_t*)&GSMFlagMessageGPRS, 0, sizeof(GSMFlagMessageGPRS));
         GSMFlagMessage.ERROR = NON_MESSAGE;
         cnt_error_CGATT_0 = 0;
         cnt_error_CGATT_1 = 8;
      //   StatusDevice.GSMmodem_non_registration_GPRS = 1;
      }      

      // ���������� ������
      if((GSMFlagMessageGPRS.AT_CGATT_CHANGE == ACCEPTED_MESSAGE 
         || GSMFlagMessageGPRS.AT_CGATT_CHANGE == ACCEPTED_MESSAGE_ERROR)
         && GSMFlagMessageGPRS.AT_CGATT_1 != ACCEPTED_MESSAGE)
      {
         if(GSMFlagMessageGPRS.AT_CGATT_CHANGE == ACCEPTED_MESSAGE)
            GSMFlagMessageGPRS.AT_CGATT_1 = ACCEPTED_MESSAGE;
         else
         {
            if(GSMFlagMessage.ERROR)
            {
               osDelay(7000);
               GSMFlagMessage.ERROR = 0;
               GSMFlagMessageGPRS.AT_CGATT_1 = NON_MESSAGE;
               GSMFlagMessageGPRS.AT_CGATT_0 = NON_MESSAGE;
               ++cnt_error_CGATT_1;
            }
            if(GSMFlagMessageGPRS.AT_CGATT_1 == NON_MESSAGE)
            {
               if(cnt_error_CGATT_0 > 2)
               {
                  memset((uint8_t*)&GSMFlagMessageGPRS, 0, sizeof(GSMFlagMessageGPRS));
                  GSMStatus.TCP = TCP_ERROR_CONNECT_INTERNET;
                //  GSMFlagMessageGPRS.AT_CGATT_1 = ACCEPTED_MESSAGE;
                  //StatusDevice.GSMmodem_non_registration_GPRS = 1;
               }
               else
               {
                  if(cnt_error_CGATT_1 < 8)
                  {
                     GSMFlagMessageGPRS.AT_CGATT_1 = SENT_MESSAGE;
                     Mdm_Transmit2(AT_CGATT_1);
                     ++cnt_error_CGATT_1_common;
                 //    StatusDevice.GSMmodem_non_registration_GPRS = 0;
                     if(cnt_error_CGATT_1 == 7 && cnt_error_CGATT_0 == 2)
                       cnt_error_CGATT_0 = 3;
                     
                  }
                  else
                  {
                     if(cnt_error_CGATT_0 == 0)
                     {
                        WriteTelemetry(TELEM_EVENT_CHECK_REGISTRATION_GPRS, cnt_error_CGATT_1_common, GSM_Get_current_communication_gsm_number(), EVENT_END);// EVENT_END EVENT_BEGIN 
                        WriteTelemetry(TELEM_EVENT_ADD_REGISTRATION_GPRS, 0, 0, EVENT_BEGIN);// EVENT_END EVENT_BEGIN
                     }
                     GSMFlagMessageGPRS.AT_CGATT_0 = SENT_MESSAGE;
                     GSMFlagMessageGPRS.AT_CGATT_1 = ACCEPTED_MESSAGE_ERROR;
                     Mdm_Transmit2(AT_CGATT_0);
                     cnt_error_CGATT_1 = 0;
                     ++cnt_error_CGATT_0;
                  //   StatusDevice.GSMmodem_non_registration_GPRS = 1;
                  }
               }
            }
         }
      }
/*
      if(GSMFlagMessageGPRS.CONNECT_FAIL == ACCEPTED_MESSAGE)
      {
        // GSMStatus.TCP = TCP_ERROR_CONNECT_INTERNET;
         error_connect_server = 1;
         GSMFlagMessageGPRS.AT_CGATT_1 = NON_MESSAGE;
         GSMFlagMessageGPRS.AT_CSTT = NON_MESSAGE;
         GSMFlagMessageGPRS.AT_CIICR = ACCEPTED_MESSAGE_ERROR;
      }
      */
      // Start Task and Set APN, USER NAME, PASSWORD
      if(GSMFlagMessageGPRS.AT_CGATT_1 == ACCEPTED_MESSAGE && GSMFlagMessageGPRS.AT_CSTT == NON_MESSAGE)
      {
        
         cnt_error_TCP_connect = 0;
         
         //StatusDevice.GSMmodem_non_registration_GPRS = 0;
         
         osDelay(200);
         memset(buf_str, 0, BUF_STR_SIZE);
         sprintf((char*)buf_str, (char*)AT_CSTT, ptr_APNAddress, APN_login, APN_passeord);
         GSMFlagMessageGPRS.AT_CSTT = SENT_MESSAGE;
        // HAL_UART_Transmit_IT(GSM_USART, (uint8_t*)buf_str, strlen((char*)buf_str));
           Mdm_Transmit2(buf_str);
         
      }
      // ����������

      
      if(GSMFlagMessageGPRS.AT_CSTT == ACCEPTED_MESSAGE && 
         /*(*/GSMFlagMessageGPRS.AT_CIICR == NON_MESSAGE/* || GSMFlagMessageGPRS.AT_CIICR == ACCEPTED_MESSAGE_ERROR)*/)
      {
       //  if(cnt_error_AT_CIICR < 10)
       //  {
            osDelay(200);
            GSMFlagMessageGPRS.AT_CIICR = SENT_MESSAGE;
            // HAL_UART_Transmit_IT(GSM_USART, (uint8_t*)AT_CIICR, sizeof(AT_CIICR)-1);
            Mdm_Transmit2(AT_CIICR);
        // }
        // else
        /// {
          // GSMFlagMessageGPRS.AT_CIICR = ACCEPTED_MESSAGE;
        // }
      }
      // �������� IP �����
      if(GSMFlagMessageGPRS.AT_CIICR == ACCEPTED_MESSAGE 
         && /*(*/GSMFlagMessageGPRS.AT_CIFSR == NON_MESSAGE /*|| GSMFlagMessageGPRS.AT_CIFSR == ACCEPTED_MESSAGE_ERROR)*/)
      {
      //   if(cnt_error_AT_CIFSR < 21)
      //   {
            osDelay(200);
            GSMFlagMessageGPRS.AT_CIFSR = SENT_MESSAGE;
        //  HAL_UART_Transmit_IT(GSM_USART, (uint8_t*)AT_CIFSR, sizeof(AT_CIFSR)-1);
            Mdm_Transmit2(AT_CIFSR);
       //  }
       // else
       //  {
       //    GSMFlagMessageGPRS.AT_CIFSR = ACCEPTED_MESSAGE;
       //  }
      }
      if(GSMFlagMessageGPRS.AT_CIFSR == ACCEPTED_MESSAGE && GSMFlagMessage.AT_CSQ == NON_MESSAGE)
      {
         if(cnt_CSQ_0 == 0)
           WriteTelemetry(TELEM_EVENT_CHECK_REGISTRATION_GPRS, cnt_error_CGATT_1_common, GSM_Get_current_communication_gsm_number(), EVENT_END);// EVENT_END EVENT_BEGIN
         flag_event = flag_event_menu = TELEM_EVENT_CHECK_LEVEL_GSM_SIGNAL;
         WriteTelemetry(TELEM_EVENT_CHECK_LEVEL_GSM_SIGNAL, 0, 0, EVENT_BEGIN);// EVENT_END EVENT_BEGIN
         
         GSMFlagMessage.AT_CSQ = SENT_MESSAGE;
      //   HAL_UART_Transmit_IT(GSM_USART, (uint8_t*)AT_CSQ, sizeof(AT_CSQ)-1);
           Mdm_Transmit2(AT_CSQ);
      }
      if(GSMFlagMessage.AT_CSQ == ACCEPTED_MESSAGE) // GSMFlagMessageTCP.AT_CIFSR == ACCEPTED_MESSAGE
      {
         WriteTelemetry(TELEM_EVENT_CHECK_LEVEL_GSM_SIGNAL, level_gsm_signal, GSM_Get_current_communication_gsm_number(), EVENT_END);// EVENT_END EVENT_BEGIN 
         flag_event = flag_event_menu = TELEM_EVENT_CHECK_REGISTRATION_SERVER;
         osDelay(180);
         GSMStatus.gprs = GPRS_YES_CONNECT;
      }

   }
   return GSMStatus.gprs; // GSMStatus.TCP == TCP_YES_CONNECT
}

uint8_t GSM_TCPConnect()
{
   if( GSMStatus.gprs == GPRS_YES_CONNECT
      && GSMStatus.TCP != TCP_YES_CONNECT
      && GSMflag_TCP == TCP_MUST_ENABLE
      && GSMflag_BT == BT_MUST_DISABLE
      && GSMStatus.power == GSM_POWER_ON
      && GSMflag_power == GSM_MUST_ENABLE) // ������ n ��� ����� �������������  
   {
   
      if(*PtrURLPortServer.ptr_url_server == 0 )
        return TCP_ERROR_CONNECT_SERVER;
      if(GSMFlagMessageTCP.AT_CIPSTART == NON_MESSAGE)
      {
         GSMStatus.TCP =  TCP_CONNECTING;
         flag_event = flag_event_menu = TELEM_EVENT_CHECK_REGISTRATION_SERVER;
         WriteTelemetry(TELEM_EVENT_CHECK_REGISTRATION_SERVER, 0, 0, EVENT_BEGIN);// EVENT_END EVENT_BEGIN
         osDelay(180);
         
     //    error_connect_server = 0;
         
         memset(buf_str, 0, BUF_STR_SIZE);
       //  sprintf((char*)buf_str, (char*)AT_CIPSTART, ptr_IPAddress, ptr_port);
         sprintf((char*)buf_str, (char*)AT_CIPSTART, PtrURLPortServer.ptr_url_server, PtrURLPortServer.ptr_port_server);
         GSMFlagMessageTCP.AT_CIPSTART = SENT_MESSAGE;
        // WriteArchiveSystemToFlash(GSM_GPRS_UP, SYSTEM_EVENT_OK, SYSTEM_EVENT_END);
       //  WriteArchiveSystemToFlash(GSM_TCP_UP, SYSTEM_EVENT_OK, SYSTEM_EVENT_BEGIN);
       //  HAL_UART_Transmit_IT(GSM_USART, (uint8_t*)buf_str, strlen((char*)buf_str));
         Mdm_Transmit2(buf_str);
         
      }
      if(GSMFlagMessageTCP.CONNECT_OK)
      {
         GSMStatus.TCP =  TCP_YES_CONNECT;
         
       //  error_connect_server = 0;
         
         WriteTelemetry(TELEM_EVENT_CHECK_REGISTRATION_SERVER, EVENT_OK, GSM_Get_current_communication_gsm_number(), EVENT_END);// EVENT_END EVENT_BEGIN 
         flag_event = flag_event_menu = TELEM_EVENT_START_DATA_SERVER;
         WriteTelemetry(TELEM_EVENT_START_DATA_SERVER, 0, 0, EVENT_BEGIN);// EVENT_END EVENT_BEGIN
         //%%%b
      //   WriteArchiveSystemToFlash(GSM_TCP_UP, SYSTEM_EVENT_OK, SYSTEM_EVENT_END);
       //  WriteArchiveSystemToFlash(GSM_TCP_MESSAGE_START, 0, SYSTEM_EVENT_BEGIN);
         //%%%e
      }
      if(GSMFlagMessageTCP.CONNECT_FAIL == ACCEPTED_MESSAGE)
      {
        ++cnt_CONNECT_FAIL_server;
        memset((uint8_t*)&GSMFlagMessageTCP, 0, sizeof(GSMFlagMessageTCP));
        if(cnt_CONNECT_FAIL_server > 2)
        {
           return TCP_ERROR_CONNECT_SERVER;
        }
        
      }
   }
   return GSMStatus.TCP; // GSMStatus.TCP == TCP_YES_CONNECT
}
// #####################################################################################
uint8_t GSMCloseTCP()
{
   if(GSMStatus.TCP == TCP_YES_CONNECT) 
   {
      // �����
      if(GSMFlagMessageTCP.AT_CIPSHUT_Close == NON_MESSAGE)
      {
         GSMFlagMessageTCP.AT_CIPSHUT_Close = SENT_MESSAGE;
        // HAL_UART_Transmit_IT(GSM_USART, (uint8_t*)AT_CIPSHUT, sizeof(AT_CIPSHUT)-1);
         Mdm_Transmit2(AT_CIPSHUT);
      }
      if(GSMFlagMessageTCP.AT_CIPSHUT_Close == ACCEPTED_MESSAGE)
      {
          //GSMFlagMessageTCP.AT_CIPSHUT_Close = ACCEPTED_MESSAGE;
          memset((uint8_t*)&GSMFlagMessageTCP, 0, sizeof(GSMFlagMessageTCP));
          GSMStatus.TCP = TCP_NON_CONNECT;
          return 1;
      }
   }
   return 0;
}
// #####################################################################################
uint8_t GSMTransmitMessageTCP(uint8_t *ptr_buf)
{// ������ ������ ��������� ��������� ���� 0x1A
   if(GSMStatus.TCP == TCP_YES_CONNECT)
   {
      if(GSMFlagMessageTCP.AT_CIPQSEND_0 == NON_MESSAGE)
      {// ������� � �������� ��������� ����� ����������� �������� ������
         GSMFlagMessageTCP.TransmitMessage = SENT_MESSAGE;
         GSMFlagMessageTCP.AT_CIPQSEND_0 = SENT_MESSAGE;
       //  HAL_UART_Transmit_IT(GSM_USART, (uint8_t*)AT_CIPQSEND_0, sizeof(AT_CIPQSEND_0)-1);
         Mdm_Transmit2(AT_CIPQSEND_0);
      }
      if(GSMFlagMessageTCP.AT_CIPQSEND_0 == ACCEPTED_MESSAGE && GSMFlagMessageTCP.AT_CIPSEND == NON_MESSAGE)
      {
         GSMFlagMessageTCP.AT_CIPSEND = SENT_MESSAGE;
         //HAL_UART_Transmit_IT(GSM_USART, (uint8_t*)AT_CIPSEND, sizeof(AT_CIPSEND)-1);
         Mdm_Transmit2(AT_CIPSEND);
      }
      if(GSMFlagMessageTCP.AT_CIPSEND == ACCEPTED_MESSAGE && GSMFlagMessageTCP.MessageTX == NON_MESSAGE)
      {
         GSMFlagMessageTCP.MessageTX = SENT_MESSAGE;
       //  HAL_UART_Transmit_IT(GSM_USART, ptr_buf, strlen((char*)ptr_buf));
         strcat((char*)ptr_buf, "\x1A");
         Mdm_Transmit2(ptr_buf);
      }
      if(GSMFlagMessageTCP.MessageTX == ACCEPTED_MESSAGE)
      {
         GSMFlagMessageTCP.AT_CIPSEND = NON_MESSAGE;
         GSMFlagMessageTCP.AT_CIPQSEND_0 = NON_MESSAGE;
         GSMFlagMessageTCP.MessageTX = NON_MESSAGE;
         GSMFlagMessageTCP.TransmitMessage = NON_MESSAGE;
         return 1;
      }
   }
   return 0;
}
// ####################################################################################################################
// Description :                                                                                         **
// ####################################################################################################################
// In  :                                                                                          **
// Out :                                                                                     **
// ####################################################################################################################
uint8_t GSMTransmitMessageTCPParts(uint8_t *ptr_buf, uint8_t last_line)
{
   if(GSMStatus.TCP == TCP_YES_CONNECT)
   {
      if(GSMFlagMessageTCP.AT_CIPQSEND_0 == NON_MESSAGE)
      {
         GSMFlagMessageTCP.TransmitMessage = SENT_MESSAGE;
         GSMFlagMessageTCP.AT_CIPQSEND_0 = SENT_MESSAGE;
       //  HAL_UART_Transmit_IT(GSM_USART, (uint8_t*)AT_CIPQSEND_1, sizeof(AT_CIPQSEND_1)-1);
         Mdm_Transmit2(AT_CIPQSEND_0);
      }
      if(GSMFlagMessageTCP.AT_CIPQSEND_0 == ACCEPTED_MESSAGE && GSMFlagMessageTCP.AT_CIPSEND_QUESTION == NON_MESSAGE)
      {
         GSMFlagMessageTCP.AT_CIPSEND_QUESTION = SENT_MESSAGE;
         GSMFlagMessageTCP.TransmitMessage = SENT_MESSAGE;
        // HAL_UART_Transmit_IT(GSM_USART, (uint8_t*)AT_CIPSEND, sizeof(AT_CIPSEND)-1);
         Mdm_Transmit2(AT_CIPSEND_QUESTION);
      }
      if(GSMFlagMessageTCP.AT_CIPSEND_QUESTION == ACCEPTED_MESSAGE && GSMFlagMessageTCP.AT_CIPSEND == NON_MESSAGE)
      {
         GSMFlagMessageTCP.AT_CIPSEND = SENT_MESSAGE;
         GSMFlagMessageTCP.TransmitMessage = SENT_MESSAGE;
        // HAL_UART_Transmit_IT(GSM_USART, (uint8_t*)AT_CIPSEND, sizeof(AT_CIPSEND)-1);
         Mdm_Transmit2(AT_CIPSEND);
      }
      
      if(GSMFlagMessageTCP.AT_CIPSEND == ACCEPTED_MESSAGE && GSMFlagMessageTCP.MessageTX == NON_MESSAGE/* && !last_line*/)
      {
      //   osDelay(2000);//###
        // cntSEND_FAIL = 0;
         if(last_line == _OK)
         {
            strcat((char*)ptr_buf, "\x1A");
            GSMFlagMessageTCP.MessageTX = SENT_MESSAGE;
            GSMFlagMessageTCP.TransmitMessage = SENT_MESSAGE;
         }
         else
           GSMFlagMessageTCP.TransmitMessage = SENT_MESSAGE_PART;
         data_size_left -= strlen((char*)ptr_buf);
         //HAL_UART_Transmit(GSM_USART, ptr_buf, strlen((char*)ptr_buf) , 100);
         Mdm_Transmit2(ptr_buf);
         Waiting_transfer_GSM(0xFFFF);
         if(last_line == _FALSE)
           osDelay(20);
      }
      if(GSMFlagMessageTCP.MessageTX == SENT_MESSAGE 
         && GSMFlagMessageTCP.SEND_FAIL == ACCEPTED_MESSAGE)
      {
         ++cntSEND_FAIL;
         if(cntSEND_FAIL > 3)
         {
           // WriteArchiveSystemToFlash(NULL, SYSTEM_EVENT_ERROR, SYSTEM_EVENT_END);
            FlagMessageTMR.command = MESSAGE_TCP_CLOSED;
            GSMFlagMessageTCP.AT_CIPQSEND_0 = NON_MESSAGE;
            GSMFlagMessageTCP.AT_CIPSEND = NON_MESSAGE;
            GSMFlagMessageTCP.AT_CIPSEND_QUESTION = NON_MESSAGE;            
            GSMFlagMessageTCP.MessageTX = NON_MESSAGE;
            cntSEND_FAIL = 0;
         }
         else
         {
            GSMFlagMessageTCP.AT_CIPSEND = NON_MESSAGE;
            GSMFlagMessageTCP.MessageTX = NON_MESSAGE;
            osDelay(200);
         }
         GSMFlagMessageTCP.SEND_FAIL = NON_MESSAGE;
      }
      if(GSMFlagMessageTCP.MessageTX == ACCEPTED_MESSAGE)
      {
        
         if(last_line == _OK)
         {
            GSMFlagMessageTCP.AT_CIPQSEND_0 = NON_MESSAGE;
           
            cntSEND_FAIL = 0;
         }
         GSMFlagMessageTCP.AT_CIPSEND_QUESTION = NON_MESSAGE;           
         GSMFlagMessageTCP.AT_CIPSEND = NON_MESSAGE;
         GSMFlagMessageTCP.MessageTX = NON_MESSAGE;
         GSMFlagMessageTCP.TransmitMessage = NON_MESSAGE;
         data_size_left = 1000;
         return 1;
        
      //  GSMFlagMessageTCP.AT_CIPSEND_QUESTION = NON_MESSAGE;
      //  GSMFlagMessageTCP.MessageTX = SENT_MESSAGE;
        
      }
      
      /*
      if(GSMFlagMessageTCP.MessageTX == SENT_MESSAGE && GSMFlagMessageTCP.AT_CIPSEND_QUESTION == ACCEPTED_MESSAGE)
      {
         if(last_line == _OK)
         {
            GSMFlagMessageTCP.AT_CIPQSEND_0 = NON_MESSAGE;
           
            cntSEND_FAIL = 0;
         }
         GSMFlagMessageTCP.AT_CIPSEND_QUESTION = NON_MESSAGE;           
         GSMFlagMessageTCP.AT_CIPSEND = NON_MESSAGE;
         GSMFlagMessageTCP.MessageTX = NON_MESSAGE;
         GSMFlagMessageTCP.TransmitMessage = NON_MESSAGE;
         return 1;
      }
      */
   }
   return 0;
}
//---------------------------------------
uint8_t GSM_Get_Status_AT_CIPSEND()
{
   if(GSMFlagMessageBT.TransmitMessage == NON_MESSAGE
      && GSMFlagMessageTCP.TransmitMessage == NON_MESSAGE)
      return NON_MESSAGE;
   else
      if(GSMFlagMessageTCP.TransmitMessage == SENT_MESSAGE_PART)
         return SENT_MESSAGE_PART;
      else
         return SENT_MESSAGE;
}
// #####################################################################################
void GSMReadSTR_OK()
{
      if(GSMFlagMessage.AT == SENT_MESSAGE)
      {
         GSMFlagMessage.AT = ACCEPTED_MESSAGE; // �������� ��
      }
      if(GSMFlagMessage.IPR == SENT_MESSAGE)
      {
           GSMFlagMessage.IPR = ACCEPTED_MESSAGE;
      }
      if(GSMFlagMessage.AT_W == SENT_MESSAGE)
      {
          GSMFlagMessage.AT_W = ACCEPTED_MESSAGE;
      }
      if(GSMFlagMessage.AT_ATE0 == SENT_MESSAGE)
      {
           GSMFlagMessage.AT_ATE0 = ACCEPTED_MESSAGE;
      }
      if(GSMFlagMessage.AT_CGSN == SENT_MESSAGE)
      {
           GSMFlagMessage.AT_CGSN = ACCEPTED_MESSAGE;
      }
      if(GSMFlagMessage.AT_CREG == SENT_MESSAGE)
      {
         if(GSMmodem_non_registration_GSM == 1)
         {
            osDelay(1000);
            ++cnt_PLUS_CREG_0;
            if(cnt_PLUS_CREG_0 > 15)
            {
              GSMFlagMessage.AT_CREG = ACCEPTED_MESSAGE;
              StatusDevice.GSMmodem_non_registration_GSM = 1;
            }
            else
              GSMFlagMessage.AT_CREG = NON_MESSAGE;
         }
         else
           GSMFlagMessage.AT_CREG = ACCEPTED_MESSAGE;
      }    
     
      if(GSMFlagMessage.AT_CSQ == SENT_MESSAGE)
      {
         if(!level_gsm_signal)
         {
           ++cnt_CSQ_0;
           osDelay(1000);
           if(cnt_CSQ_0 > 15)
              GSMFlagMessage.AT_CSQ = ACCEPTED_MESSAGE;
           else
              GSMFlagMessage.AT_CSQ = NON_MESSAGE;
         }
         else
            GSMFlagMessage.AT_CSQ = ACCEPTED_MESSAGE;
      }
     
      if(GSMFlagMessage.AT_BTHOST == SENT_MESSAGE)
      {
           GSMFlagMessage.AT_BTHOST = ACCEPTED_MESSAGE;
      }
      if(GSMFlagMessage.AT_CSCLK_1 == SENT_MESSAGE)
      {
           GSMFlagMessage.AT_CSCLK_1 = ACCEPTED_MESSAGE;
      }
      if(GSMFlagMessage.AT_COPS == SENT_MESSAGE)
      {
          GSMFlagMessage.AT_COPS = ACCEPTED_MESSAGE;
      }     
      if(GSMFlagMessage.AT_CCID == SENT_MESSAGE)
      {
         GSMFlagMessage.AT_CCID = ACCEPTED_MESSAGE;
      }         
      if(GSMFlagMessage.AT_SAPBR_3_1_APN == SENT_MESSAGE)
      {
         GSMFlagMessage.AT_SAPBR_3_1_APN = ACCEPTED_MESSAGE;
      }
      if(GSMFlagMessage.AT_SAPBR_1_1 == SENT_MESSAGE)
      {
         GSMFlagMessage.AT_SAPBR_1_1 = ACCEPTED_MESSAGE;
      }
      if(GSMFlagMessageGPRS.AT_CIPMUX_0 == SENT_MESSAGE)
      {
         GSMFlagMessageGPRS.AT_CIPMUX_0 = ACCEPTED_MESSAGE;
      }
      if(GSMFlagMessageGPRS.AT_CGATT_1 == SENT_MESSAGE)
      {
         GSMFlagMessageGPRS.AT_CGATT_1 = ACCEPTED_MESSAGE;
      }
      if(GSMFlagMessageGPRS.AT_CGATT_0 == SENT_MESSAGE)
      {
         osDelay(13000);
         GSMFlagMessage.ERROR = ACCEPTED_MESSAGE;
         GSMFlagMessageGPRS.AT_CGATT_0 = ACCEPTED_MESSAGE;
      }      
      if(GSMFlagMessageGPRS.AT_CGATT_CHANGE == SENT_MESSAGE)
      {
         if(GSMFlagMessageGPRS.PLUS_CGATT == ACCEPTED_MESSAGE)
            GSMFlagMessageGPRS.AT_CGATT_CHANGE = ACCEPTED_MESSAGE;
         else
            GSMFlagMessageGPRS.AT_CGATT_CHANGE = ACCEPTED_MESSAGE_ERROR;
      }      
      if(GSMFlagMessageGPRS.AT_CSTT == SENT_MESSAGE)
      {
         GSMFlagMessageGPRS.AT_CSTT = ACCEPTED_MESSAGE;
      }
      if(GSMFlagMessageGPRS.AT_CIICR == SENT_MESSAGE)
      {
         GSMFlagMessageGPRS.AT_CIICR = ACCEPTED_MESSAGE;
      }
      if(GSMFlagMessageTCP.AT_CIPSEND_QUESTION == SENT_MESSAGE)
      {
         GSMFlagMessageTCP.AT_CIPSEND_QUESTION = ACCEPTED_MESSAGE;
      }      
      if(GSMFlagMessageTCP.AT_CIPQSEND_1 == SENT_MESSAGE)
      {
         GSMFlagMessageTCP.AT_CIPQSEND_1 = ACCEPTED_MESSAGE;
      }
      if(GSMFlagMessageTCP.AT_CIPQSEND_0 == SENT_MESSAGE)
      {
         GSMFlagMessageTCP.AT_CIPQSEND_0 = ACCEPTED_MESSAGE;
      }
      if(GSMFlagMessage.AT_CREG_0 == SENT_MESSAGE)
      {
         GSMFlagMessage.AT_CREG_0 = ACCEPTED_MESSAGE;
      }
      if(GSMFlagMessageBT.AT_BTPOWER == SENT_MESSAGE)
      {
         GSMFlagMessageBT.AT_BTPOWER = ACCEPTED_MESSAGE;
        // GSMStatus.BT = BT_YES_POWER;
      }
      if(GSMFlagMessageBT.AT_BTPOWER_0 == SENT_MESSAGE)
      {
         GSMFlagMessageBT.AT_BTPOWER_0 = ACCEPTED_MESSAGE;
      }
      if(GSMFlagOutputSMS.AT_CSCS_UCS2 == SENT_MESSAGE)
      {
        GSMFlagOutputSMS.AT_CSCS_UCS2 = ACCEPTED_MESSAGE;
      }
      if(GSMFlagOutputSMS.AT_CSMP_17_167_0_25 == SENT_MESSAGE)
      {
        GSMFlagOutputSMS.AT_CSMP_17_167_0_25 = ACCEPTED_MESSAGE;
      }
      if(GSMFlagOutputSMS.AT_CMGF_ON == SENT_MESSAGE)
      {
        GSMFlagOutputSMS.AT_CMGF_ON = ACCEPTED_MESSAGE;
      }
      if(GSMFlagOutputSMS.MessageTX == SENT_MESSAGE)
      {
        GSMFlagOutputSMS.MessageTX = ACCEPTED_MESSAGE;
      }
      
}
// #####################################################################################
uint8_t GSMReadSTR_Digit(uint8_t *ptr_buf)
{
  if(*ptr_buf >= '0'
     && *ptr_buf <= '9')
  {
     if(GSMFlagMessageGPRS.AT_CIFSR == SENT_MESSAGE)
        GSMFlagMessageGPRS.AT_CIFSR = ACCEPTED_MESSAGE;
     if(GSMFlagMessage.AT_CGSN == SENT_MESSAGE)
     { 
          memset(gsm_imei, 0, 16);
          strncpy((char*)gsm_imei, (char*)ptr_buf, 15);
     }
     if(GSMFlagMessage.AT_CCID == SENT_MESSAGE)
     { 
          memset(CCID_SIM_card, 0, sizeof(CCID_SIM_card)); // +
          strncpy((char*)CCID_SIM_card, (char*)ptr_buf, sizeof(CCID_SIM_card) - 1); // +
     }
     
     
     return 1;
  }
  return 0;
}
/*******************************************************************
** InitRFChip : This routine initializes the RFChip registers     **
**              Using Pre Initialized variables                   **
********************************************************************
** In  : -                                                        **
** Out : -                                                        **
*******************************************************************/
char* GSM_Get_ptr_network_name()
{
   return (char*)GSM_network_name;
}
/*******************************************************************
** InitRFChip : This routine initializes the RFChip registers     **
**              Using Pre Initialized variables                   **
********************************************************************
** In  : -                                                        **
** Out : -                                                        **
*******************************************************************/
char* GSM_Get_ptr_CCID_SIM_card()
{
   return (char*)CCID_SIM_card;
}
/*******************************************************************
** InitRFChip : This routine initializes the RFChip registers     **
**              Using Pre Initialized variables                   **
********************************************************************
** In  : -                                                        **
** Out : -                                                        **
*******************************************************************/
char* GSM_Get_ptr_IMEI()
{
   return (char*)gsm_imei;
}

/*******************************************************************
** InitRFChip : This routine initializes the RFChip registers     **
**              Using Pre Initialized variables                   **
********************************************************************
** In  : -                                                        **
** Out : -                                                        **
*******************************************************************/
char* GSM_Get_ptr_message_SIM_card_balance()
{
   return (char*)message_SIM_card_balance;
}
/*******************************************************************
** InitRFChip : This routine initializes the RFChip registers     **
**              Using Pre Initialized variables                   **
********************************************************************
** In  : -                                                        **
** Out : -                                                        **
*******************************************************************/
uint8_t GSM_Get_level_gsm_signal()
{
   return level_gsm_signal;
}
/*******************************************************************
** InitRFChip : This routine initializes the RFChip registers     **
**              Using Pre Initialized variables                   **
********************************************************************
** In  : -                                                        **
** Out : -                                                        **
*******************************************************************/
uint8_t GSM_Get_Status_power()// GSM_POWER_ON
{
   return GSMStatus.power;
}
/*******************************************************************
** InitRFChip : This routine initializes the RFChip registers     **
**              Using Pre Initialized variables                   **
********************************************************************
** In  : -                                                        **
** Out : -                                                        **
*******************************************************************/
uint32_t GSM_Get_tick_count_message_from_GSM_msec()
{
   return tick_count_message_from_GSM_msec;
}
/*******************************************************************
** InitRFChip : This routine initializes the RFChip registers     **
**              Using Pre Initialized variables                   **
********************************************************************
** In  : -                                                        **
** Out : -                                                        **
*******************************************************************/
void GSM_ExtremalPowerDown(int8_t flag_end)
{
  uint8_t add_connection = 0;
  flag_event_menu = flag_end;
  rez_GSM_ExtremalPowerDown = flag_end;
  if(flag_event < TELEM_EVENT_CHECK_REGISTRATION_GSM)
    add_connection = 1;
  WriteTelemetry(flag_event, flag_end, GSM_Get_current_communication_gsm_number() + add_connection, EVENT_END);// EVENT_END EVENT_BEGIN 
  // HAL_GPIO_WritePin(ptr_PxOUT_Pwrkey, pin_pwrkey, GPIO_PIN_SET); // ���������� ON/OFF � '0'
  TransmitDebugMessageOptic("RESTART_GSM_MDM", flag_end);
  flag_event_menu = TELEM_EVENT_INPUT_MODEM;
//  if(flag_end == EVENT_NON_BATARY)
//  {
  
     GSM_PWRKEY_OFF();
     HiPower_ON &= ~BIT0;
     if (HiPower_ON == 0)
       GSM_PWR_OFF();
     CalculateMotoHourceGSM(MOTO_HOURCE_STOP);
     GSM_PWRKEY_ON();
  
     memset((uint8_t*)&GSMFlagMessage, 0, sizeof(GSMFlagMessage));
     GSMflag_power = GSM_MUST_DISABLE;
     GSMStatus.power = POWER_NON;
     GSMStatus.init = INIT_NON;
     GSMStatus.speed = SPEED_NON_FOUND;
     GSMStatus.gprs = GPRS_NON_CONNECT;
     GSMStatus.TCP = TCP_NON_CONNECT;
     osDelay(1000);
     Mdm_HardwareDeInit();
     /*
  }
  else
  {
     Mdm_HardwareDeInit();
         
     GSM_PWRKEY_OFF();
     osDelay(1750); //
     GSM_PWRKEY_ON();
     GSMStatus.power = POWER_NON;
     for(uint8_t i = 0; i < 20; i++)
     {
        if(READ_BIT(GPIOH->IDR, GPIO_IDR_IDR_2))
        {// status modem
          osDelay(150);
        }
        else
        {
          i = 20;
        }
     }
  }
  Mdm_HardwareDeInit();
     osDelay(1000);
     memset((uint8_t*)&GSMFlagMessage, 0, sizeof(GSMFlagMessage));
     GSMflag_power = GSM_MUST_DISABLE;
     GSMStatus.power = POWER_NON;
     GSMStatus.init = INIT_NON;
     GSMStatus.speed = SPEED_NON_FOUND;
     GSMStatus.gprs = GPRS_NON_CONNECT;
     GSMStatus.TCP = TCP_NON_CONNECT;
  */
  QueueGSMModemHandleStatus = EVENT_NON;
}
/*******************************************************************
** InitRFChip : This routine initializes the RFChip registers     **
**              Using Pre Initialized variables                   **
********************************************************************
** In  : -                                                        **
** Out : -                                                        **
*******************************************************************/
uint16_t GSM_Get_current_communication_gsm_number()
{
   return current_communication_gsm_number;
}
/*******************************************************************
** InitRFChip : This routine initializes the RFChip registers     **
**              Using Pre Initialized variables                   **
********************************************************************
** In  : -                                                        **
** Out : -                                                        **
*******************************************************************/
uint8_t GSM_Get_Status_init()
{
   return GSMStatus.init;
}
/*******************************************************************
** InitRFChip : This routine initializes the RFChip registers     **
**              Using Pre Initialized variables                   **
********************************************************************
** In  : -                                                        **
** Out : -                                                        **
*******************************************************************/
void GSM_Update_tick_count_message_from_GSM_msec()
{
   tick_count_message_from_GSM_msec = xTaskGetTickCount();
}
/*******************************************************************
** InitRFChip : This routine initializes the RFChip registers     **
**              Using Pre Initialized variables                   **
********************************************************************
** In  : -                                                        **
** Out : -                                                        **
*******************************************************************/
uint32_t GSM_Get_tick_count_powerON_GSM_msec()
{
   return tick_count_powerON_GSM_msec;
}
/*!
Returns the remaining number of bytes for transmission to the modem
\return number of bytes
*/
int16_t GSM_Get_data_size_left()
{
   return data_size_left;
}
/*!
Set pointer for url server and port server
*/
void SetURLPortServer(uint8_t *ptr_url, uint8_t *ptr_port)
{
  PtrURLPortServer.ptr_port_server = ptr_port;
  PtrURLPortServer.ptr_url_server = ptr_url;
  cnt_CONNECT_FAIL_server = 0;
}
/*******************************************************************
** InitRFChip : This routine initializes the RFChip registers     **
**              Using Pre Initialized variables                   **
********************************************************************
** In  : -                                                        **
** Out : -                                                        **
*******************************************************************/
void GSM_Clear_cnt_error_speed_modem()
{
   cnt_error_speed_modem = 0;
}
/*!
Get GSMflag_TCP 
\return  TCP_MUST_DISABLE, TCP_MUST_ENABLE
*/
Typeflag_TCP  GSM_Get_TCPFlag()
{
  return GSMflag_TCP;
}
/**********************************************************************************************************
** Description :                                                                                         **
***********************************************************************************************************
** In  :                                                                                          **
** Out :                                                                                     **
**********************************************************************************************************/
/*
void GSM_OutSMS(uint8_t *ptr_textSMS)
{
   // ��� BT
   // ��� GPRS
   // �������
   // �������������
   if(GSMStatus.TCP == TCP_NON_CONNECT
      && GSMStatus.init == INIT_OK
      && GSMStatus.power == GSM_POWER_ON
      && GSMflag_BT == BT_MUST_DISABLE
      && GSMStatus.BT == BT_NON_CONNECT
      && GSMflag_CALLOutput == CALL_NON_MUST_OUTPUT  
      && GSMflag_SMSOutput == SMS_MUST_OUTPUT_PDU
      )
   {
       if(GSMFlagOutputSMS.AT_CSMP_17_167_0_25 == NON_MESSAGE)
       {
          GSMFlagOutputSMS.status = SMS_TRANSMITTED;
          GSMFlagOutputSMS.AT_CSMP_17_167_0_25 = SENT_MESSAGE;
          Mdm_Transmit2(AT_CSMP_17_167_0_25);
       }
       if(GSMFlagOutputSMS.AT_CMGF_ON == NON_MESSAGE && GSMFlagOutputSMS.AT_CSMP_17_167_0_25 == ACCEPTED_MESSAGE)
       {
         GSMFlagOutputSMS.AT_CMGF_ON = SENT_MESSAGE;
         Mdm_Transmit2(AT_CMGF_ON);
       }       
       
       if(GSMFlagOutputSMS.AT_CMGF_ON == ACCEPTED_MESSAGE && GSMFlagOutputSMS.AT_CSCS_UCS2 == NON_MESSAGE)
       {
          GSMFlagOutputSMS.AT_CSCS_UCS2 = SENT_MESSAGE;
          Mdm_Transmit2(AT_CSCS_UCS2);
       }

       if(GSMFlagOutputSMS.AT_CSCS_UCS2 == ACCEPTED_MESSAGE && GSMFlagOutputSMS.AT_CMGS == NON_MESSAGE)
       {
          GSMFlagOutputSMS.AT_CMGS = SENT_MESSAGE;
          Mdm_Transmit2((uint8_t*)BufferATTelNumberOutSMSMessages);
       }       
       
       if(GSMFlagOutputSMS.AT_CMGS == ACCEPTED_MESSAGE && GSMFlagOutputSMS.MessageTX == NON_MESSAGE)
       {
          GSMFlagOutputSMS.MessageTX = SENT_MESSAGE;
          Mdm_Transmit2(ptr_textSMS);
       }
       if(GSMFlagMessage.ERROR == ACCEPTED_MESSAGE)
       {
         GSMFlagMessage.ERROR = NON_MESSAGE;
         GSMFlagOutputSMS.status = SMS_TRANSMIT_OK;
       }
       if(GSMFlagOutputSMS.MessageTX == ACCEPTED_MESSAGE)
       {
          GSMFlagOutputSMS.status = SMS_TRANSMIT_OK;
       }
   }
}
*/
//---------------------------------------
TypeSMSOutputStatus GSM_GetStatusOutSMS()
{
  return GSMFlagOutputSMS.status;
}

//---------------------------------------
void GSM_Set_StatusOutSMS(TypeSMSOutputStatus flag)
{
  GSMFlagOutputSMS.status = flag;
}
//---------------------------------------
void GSM_Set_flag_SMSOutput(Typeflag_SMSOutput flag)
{
  GSMflag_SMSOutput = flag;
  GSMFlagOutputSMS.status = SMS_NON_TRANSMITTED;
  if(GSMflag_SMSOutput == SMS_NON_MUST_OUTPUT)
  {
    memset((uint8_t*)&GSMFlagOutputSMS, 0, sizeof(GSMFlagOutputSMS));
  }
}
/**********************************************************************************************************
** Description :                                                                                         **
***********************************************************************************************************
** In  :                                                                                          **
** Out :                                                                                     **
**********************************************************************************************************/
/*
void CopyTelNumberOutSMS(uint8_t *ptr)
{// ������ - 910 123 45 67
  memset(BufferATTelNumberOutSMSMessages, 0, sizeof(BufferATTelNumberOutSMSMessages));
  sprintf((char*)BufferATTelNumberOutSMSMessages, "%s", "AT+CMGS=\"");
  AnsiToUnicod(BufferATTelNumberOutSMSMessages, ptr);
  strcat((char*)BufferATTelNumberOutSMSMessages, "\"\r");
}
*/
//-----------------------------------------------------------------------------
// ������������� ������ ��������� �� ������
void InitMessage(void)
{
memset(ModemRxBuf,0,sizeof(ModemRxBuf));
Message.Index=0;
Message.Register=0;
Message.String[0]=(char*)ModemRxBuf;
Mdm_DMA_RxReset();
}
//-----------------------------------------------------------------------------
// �������� ��������� �� ������ ���������� ������ �� ������
void MakeMessage(void)
{
char *poi;
while(1)
	{
	poi=strstr(Message.String[Message.Index],"\r\n");
	if(!poi)
		{
		poi=strstr(Message.String[Message.Index],"> ");
		if(poi)
			poi+=2;
		}
	if(poi)
		{
		*poi=0;
		// ����������� ������ ������
		if(strlen(Message.String[Message.Index])==0)
			Message.String[Message.Index]+=2;
		else
			{
			poi+=2;
			Message.Register|=(uint32_t)(1<<Message.Index);
			Message.Index++;
			if(Message.Index>32)
				Message.Index=32;
			Message.String[Message.Index]=poi;
			break;
			}
		}
	else
		break;
	}
}
//-----------------------------------------------------------------------------
// �������� ������ ��������������� ���������
int GetMessage(void)
{
int i,ret=-1;
uint32_t mask;
MakeMessage();
for(i=0;i<32;i++)
	{
	if(Message.Register==0)
		break;
	mask=(uint32_t)(1<<i);
	if((Message.Register & mask)==mask)
		{
		ret=i;
		break;
		}
	}
return ret;
}
//-----------------------------------------------------------------------------
// ������� �������� ������������� ��������� (�� �������)
void ClrBitMessage(int index)
{
Message.Register&=(uint32_t)~(1<<index);
}