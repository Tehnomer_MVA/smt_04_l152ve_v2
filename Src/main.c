// ******************************************************************************

/* Includes ------------------------------------------------------------------*/

#include "GlobalHandler.h"

/* Variables -----------------------------------------------------------------*/
uint8_t flag_end_task=0;


//extern uint8_t successful_transfer_data_to_server;
extern LL_RTC_TimeTypeDef SystTime;
extern LL_RTC_DateTypeDef SystDate;
/*
T_SMT_Errors SMT_Errors;
T_SMT_Warnings SMT_Warnings;

T_SMT_Errors Reg_SMT_Errors;
T_SMT_Warnings Reg_SMT_Warnings;
*/
//uint8_t cnt_repetition_connect_TCP;
osThreadId DispatcherTaskHandle;
osThreadId GasmeterTaskHandle;
osThreadId OPTO_ReadTaskHandle;
osThreadId ReadWriteARCTaskHandle;
osThreadId GSMTaskHandle;
osThreadId GSM_RX_UARTTaskHandle;
osThreadId ParsingComandsTaskHandle;


SemaphoreHandle_t xRTCSemaphore;// = NULL;
SemaphoreHandle_t xOpticRxSemaphore;// = NULL;
SemaphoreHandle_t xMdmRxSemaphore;// = NULL;
SemaphoreHandle_t xArcRecReadyWriteSemaphore;
SemaphoreHandle_t xCalculateCRC32Semaphore;
SemaphoreHandle_t xEepromWriteFrameReadySemaphore;


//osMessageQId QueueGSM_RX_UARTHandle;
osMessageQId QueueGSMModemHandle;
osMessageQId QueueParsingCommandHandle;



xQueueHandle xARCQueue;

Error Gl_Error;


/* Private function prototypes -----------------------------------------------*/
void DispatcherTask(void const * argument);
void GasmeterTask(void const * argument);
static Error InitModules(void);
void MX_FREERTOS_Init(void); /* (MISRA C 2004 rule 8.1) */
//static void LogError(Error error);
void StartGSMTask(void const * argument);
void StartGSM_RX_UARTTask(void const * argument);
void StartParsingComandsTask(void const * argument);
void Error_Handler(void);

//extern const char MdmPort_Server[6];




/*---------------------------------------------------------------------------*/
int main(void)
{

  /* Call init function for freertos objects (in freertos.c) */
 
  MX_FREERTOS_Init();
  
 // System_Init_volue();
  
  ParamServer.success_session[0] = _OK;
  ParamServer.success_session[1] = _OK;
  ParamServer.success_session[2] = _OK;
  ParamServer.availability_server1 = 0;
  ParamServer.availability_server2 = 0;
  ParamServer.availability_server3 = 0;

  GetDate_Time(&SystDate,&SystTime);
  timeSystemUnix = Get_IndexTime(SystDate, SystTime);
  timeGSMUnixStart = timeSystemUnix;
  timeUnixGSMNextConnect = timeSystemUnix + 600;
  tick_time_lcd_display_on_sec = SystemUpTimeSecond;  
  error_in_SGM_module  = 0;
  CLB_lock = CLB_lock_eeprom;
 // LCD_Clear(); 
  MotoHoursToMils = MotoHoursToMilsEEPROM;
  
 // ValveSettings.ptrDriverPint1_Port = ValveNSleep_GPIO_Port;
 // ValveSettings.driverPint1_Pin = ValveNSleep_GPIO_Pin;
  ValveSettings.ptrDriverPint2_Port = ValveIN1_GPIO_Port;
  ValveSettings.driverPint2_Pin = ValveIN1_GPIO_Pin;
  ValveSettings.ptrDriverPint3_Port = ValveIN2_Pin_GPIO_Port;
  ValveSettings.driverPint3_Pin = ValveIN2_GPIO_Pin;
 // ValveSettings.ptrPower_Port = ValvePower_Pin_GPIO_Port;
//  ValveSettings.power_Pin = ValvePower_GPIO_Pin;
  ValveInit();
  ValveStatusOld = ValveGetStatus();
  
  if(mode_conservation == 1)
    StatusDevice.mode_conservation = 1;

  event_int_story = event_int_story_old;

  /* Start scheduler */
  osKernelStart();
  
  while (1)
  {
//   LCD_TimeShow();
    __NOP();
  }
}

/*---------------------------------------------------------------------------*/
void MX_FREERTOS_Init(void) {
  
  System_Init();
  Time_InitHardware();

  Io_InitHardware();  //// Init OptoSwitch and Key 
  LCD8_Init();  
  I2c_InitHardware();
  Sgm_InitHardware();
  Opto_Uart_Init();

  LCD8_Menu(TEH_MENU_SW,0);

/* USER CODE BEGIN RTOS_SEMAPHORES */
   xRTCSemaphore =  xSemaphoreCreateBinary();
   xOpticRxSemaphore  =  xSemaphoreCreateBinary();
   xMdmRxSemaphore = xSemaphoreCreateBinary();
   xArcRecReadyWriteSemaphore = xSemaphoreCreateBinary();
   xCalculateCRC32Semaphore = xSemaphoreCreateBinary();   
   xEepromWriteFrameReadySemaphore = xSemaphoreCreateBinary();

   
  number_communication_gsm_fail = number_communication_gsm_fail_eeprom; 
 number_communication_gsm_fail_2 = number_communication_gsm_fail_2_eeprom;  
 number_communication_gsm_fail_3 = number_communication_gsm_fail_3_eeprom;  
 current_communication_gsm_number = current_communication_gsm_number_eeprom; 
   
 
  osThreadDef(DispatcherTask, DispatcherTask, osPriorityHigh, 0, 300); // +2
  DispatcherTaskHandle = osThreadCreate(osThread(DispatcherTask), NULL);
 
 
  osThreadDef(GasmeterTask, GasmeterTask, osPriorityAboveNormal, 0, 350);// +1
  GasmeterTaskHandle = osThreadCreate(osThread(GasmeterTask), NULL);
  
  osThreadDef(OPTO_ReadTask, OPTO_ReadTask, osPriorityNormal, 0, 350);  // 0
  OPTO_ReadTaskHandle = osThreadCreate(osThread(OPTO_ReadTask), NULL);
  
  osThreadDef(ReadWriteARC, ReadWriteARC, osPriorityNormal, 0, 230); // 0
  ReadWriteARCTaskHandle = osThreadCreate(osThread(ReadWriteARC), NULL);

    /* definition and creation of GSMTask */
  osThreadDef(GSMTask, StartGSMTask, osPriorityIdle, 0, 370);  // -3
  GSMTaskHandle = osThreadCreate(osThread(GSMTask), NULL);

  /* definition and creation of GSM_RX_UARTTask */
  osThreadDef(GSM_RX_UARTTask, StartGSM_RX_UARTTask, osPriorityIdle, 0, 250); // -3
  GSM_RX_UARTTaskHandle = osThreadCreate(osThread(GSM_RX_UARTTask), NULL);
  
  /* definition and creation of GSM_RX_UARTTask */
  osThreadDef(ParsingComandsTask, StartParsingComandsTask, osPriorityNormal, 0, 350);  // 0
  ParsingComandsTaskHandle = osThreadCreate(osThread(ParsingComandsTask), NULL);
  
 /* USER CODE END RTOS_THREADS */

  /* USER CODE BEGIN RTOS_QUEUES */
  xARCQueue = xQueueCreate( 10, sizeof(TARC_Queue)); 
  vQueueAddToRegistry( xARCQueue, "ARCQueue" );
  
  osMessageQDef(QueueGSMModem, 10, TypeQueueGSMModem);
  QueueGSMModemHandle = osMessageCreate(osMessageQ(QueueGSMModem), NULL);

  /*
    osMessageQDef(QueueGSM_RX_UART, 5, uint8_t);
  QueueGSM_RX_UARTHandle = osMessageCreate(osMessageQ(QueueGSM_RX_UART), NULL);
  */
  osMessageQDef(QueueParsingCommand, 5, TypeParsingCommand);
  QueueParsingCommandHandle = osMessageCreate(osMessageQ(QueueParsingCommand), NULL);  
  
  /* USER CODE END RTOS_QUEUES */
}

  // GSM GSM GSM GSM GSM GSM GSM GSM GSM GSM GSM GSM GSM GSM GSM GSM GSM GSM GSM GSM GSM GSM GSM GSM GSM GSM GSM GSM GSM 
  // GSM GSM GSM GSM GSM GSM GSM GSM GSM GSM GSM GSM GSM GSM GSM GSM GSM GSM GSM GSM GSM GSM GSM GSM GSM GSM GSM GSM GSM 
  // GSM GSM GSM GSM GSM GSM GSM GSM GSM GSM GSM GSM GSM GSM GSM GSM GSM GSM GSM GSM GSM GSM GSM GSM GSM GSM GSM GSM GSM 
/* StartGSMTask function */
void StartGSMTask(void const * argument)
{
  xSemaphoreGive(xCalculateCRC32Semaphore);
  for(;;)
  {
     flag_end_task |= 0x01;
     stack_task_GSM = uxTaskGetStackHighWaterMark(NULL);
     //-----------------------------------------------------------------------
     if(CLB_lock_IT)
     {
        CLB_lock_IT = 0;
        sprintf((char*)OldValue,"%d", CLB_lock);   
          
    //    if (CLB_lock == 0x55 || CLB_lock == 0x66) 
    //       CLB_lock = 0;
    //    else
        if(CLB_lock == 0)
           CLB_lock = 0x55;
        Eeprom_WriteChar((uint32_t)&CLB_lock_eeprom, CLB_lock);
        sprintf((char*)NewValue, "%d", CLB_lock); 
        WriteChangeArcRecord(CHANGE_CLB_LOCK, CHANGE_PROGRAM);     
        
     }

     if(TestModeActive != TestModeActive_previous)
     {
          sprintf((char*)OldValue,"%d", TestModeActive_previous);  
          sprintf((char*)NewValue, "%d", TestModeActive); 
          WriteChangeArcRecord(CHANGE_TEST_MODE, CHANGE_PROGRAM); 
          TestModeActive_previous = TestModeActive;
     }
     //--------------------------------------------------------------------------------------------        
     // BAT1_ON_OFF
     if(READ_BIT(GPIOE->IDR, GPIO_IDR_IDR_6))
     {
        if(!StatusSystem.bit.BAT1_off)
          StatusSystem.bit.BAT1_off = 1;
     }
     else
     {
        if(StatusSystem.bit.BAT1_off)
        {
          StatusSystem.bit.BAT1_off = 0;
          battery_counter_installation_time = timeSystemUnix;
        }
     }
     //--------------------------------------------------------------------------------------------
     if(OptoPort == OPTO_PORT_ENABLE)
     {
        if(!StatusDevice.optical_port_communication_session)
        {
           Opto_Uart_Init();
           CalculateMotoHourceOPTIC(MOTO_HOURCE_START);
           StatusDevice.optical_port_communication_session = 1;
           WriteTelemetry(TELEM_EVENT_OPTIC_UP, 0, 0, EVENT_BEGIN);// EVENT_END EVENT_BEGIN
           WriteTelemetry(TELEM_EVENT_OPTIC_UP, EVENT_OK, GSM_Get_current_communication_gsm_number(), EVENT_END);// EVENT_END EVENT_BEGIN 
        }
     }
     else
     {
        if(StatusDevice.optical_port_communication_session)
        {
           ClosePassword();
           Opto_Uart_deInit();
           CalculateMotoHourceOPTIC(MOTO_HOURCE_STOP);
           StatusDevice.optical_port_communication_session = 0;
           WriteTelemetry(TELEM_EVENT_OPTIC_DOWN, 0, 0, EVENT_BEGIN);// EVENT_END EVENT_BEGIN
           WriteTelemetry(TELEM_EVENT_OPTIC_DOWN, EVENT_OK, GSM_Get_current_communication_gsm_number(), EVENT_END);// EVENT_END EVENT_BEGIN 
        }
     }
     //-------------------------------------------------------
     if(OptoPort == OPTO_PORT_ENABLE && ((SystemUpTimeSecond - Get_tick_count_message_from_OPTIC_sec()) > waiting_time_connecting_optic_sec) && waiting_time_connecting_optic_sec) 
     {
        OptoPort = OPTO_PORT_DISABLE;
        flag_crypt_message[SOURCE_OPTIC] = _FALSE;
        kfactor_emulator = 65535;
        TransmitOpticTest = 0;
        Opto_Uart_deInit();
     }
     //-------------------------------------------------------    
     if(cnt_seans_TCP_mode_2 > 6 && Mode_Transfer.mode == 2 && auto_switching_mode)
     {
         change_transfermode_to_5(CHANGE_PROGRAM);
     }
     //-------------------------------------------------------     
    ParsingQueueGSMModemHandle();
    GSMPower();
   // ParsingCommands();
    if(GSM_Get_flag_power() == GSM_MUST_ENABLE ) 
    {
        if(GSM_Get_Status_power() == POWER_NON_BATARY)
        {
            GSM_ExtremalPowerDown(EVENT_NON_BATARY);
            Eeprom_WriteDword((uint32_t)&cnt_fail_speed_gsm_modem_eeprom, cnt_fail_speed_gsm_modem_eeprom + 1);
          //  ParamServer.success_session[number_server_transmit] = _OK;
            ParamServer.success_session[0] = _OK;
            ParamServer.success_session[1] = _OK;
            ParamServer.success_session[2] = _OK;
            QueueGSMModemHandleStatus = EVENT_NON_POWER;
        }
      
        if(FindSpeedUARTGSMModule() == SPEED_FOUND_ERROR || GSM_Get_Status_power() == POWER_ERROR)
        {
            GSM_ExtremalPowerDown(EVENT_GSM_ERROR);
            Eeprom_WriteDword((uint32_t)&cnt_fail_speed_gsm_modem_eeprom, cnt_fail_speed_gsm_modem_eeprom + 1);
         //   ParamServer.success_session[number_server_transmit] = _FALSE;
            ParamServer.success_session[0] = _FALSE;
            ParamServer.success_session[1] = _FALSE;
            ParamServer.success_session[2] = _FALSE;
            QueueGSMModemHandleStatus = EVENT_NON;
        }
        if(GSM_Get_Status_power() == POWER_CPIN_NOT_INSERTED)
        {
           GSM_ExtremalPowerDown(EVENT_CPIN_NOT_INSERTED);
           Eeprom_WriteDword((uint32_t)&cnt_fail_sim_card_eeprom, cnt_fail_sim_card_eeprom + 1);
        //   ParamServer.success_session[number_server_transmit] = _FALSE;
           ParamServer.success_session[0] = _FALSE;
           ParamServer.success_session[1] = _FALSE;
           ParamServer.success_session[2] = _FALSE;
           QueueGSMModemHandleStatus = EVENT_NON;
        }     
        InitGSMModule();
        if(GSM_Get_Status_init() == SIM_CARD_ERROR)
        {
           GSM_ExtremalPowerDown(EVENT_ERROR);
           Eeprom_WriteDword((uint32_t)&cnt_fail_sim_card_eeprom, cnt_fail_sim_card_eeprom + 1);
           ParamServer.success_session[number_server_transmit] = _FALSE;
           QueueGSMModemHandleStatus = EVENT_NON;
        }
        if(GSM_Get_Status_init() == SIM_CARD_NON || GSM_Get_Status_power() == POWER_NON_PIN_SIM)
        {
           GSM_ExtremalPowerDown(EVENT_NON_PIN_SIM);
           Eeprom_WriteDword((uint32_t)&cnt_fail_sim_card_eeprom, cnt_fail_sim_card_eeprom + 1);
           ParamServer.success_session[number_server_transmit] = _OK;
           StatusDevice.GSM_modem_SIM_card_non = 1;
          // if(Mode_Transfer.mode == 2)
          /// {
          //    change_transfermode_to_5(CHANGE_PROGRAM);
          // }
           QueueGSMModemHandleStatus = EVENT_NON;
        } 
        if(READ_BIT(GPIOD->IDR, GPIO_IDR_IDR_3) == 0 && GSM_Get_Status_power() == GSM_POWER_ON)
        {
           GSM_ExtremalPowerDown(EVENT_ERROR_STATUS_MODEM);
           ParamServer.success_session[0] = _FALSE;
           ParamServer.success_session[1] = _FALSE;
           ParamServer.success_session[2] = _FALSE;
           QueueGSMModemHandleStatus = EVENT_NON;
        }
    }
    //------------------------------------------------------------------------
    uint8_t rez_GSM_GPRSConnect, rez_GSM_TCPConnect = TCP_NON_CONNECT;
    
    rez_GSM_GPRSConnect = GSM_GPRSConnect(/*(uint8_t*)MdmURL_Server, (uint8_t*)MdmPort_Server,*/ (uint8_t*)MdmAPN_Adress, (uint8_t*)MdmAPN_Login, (uint8_t*)MdmAPN_Password);
    if(rez_GSM_GPRSConnect == GPRS_YES_CONNECT)
    {
       rez_GSM_TCPConnect = GSM_TCPConnect();
       if(rez_GSM_TCPConnect == TCP_YES_CONNECT)
       {
          if(((SystemUpTimeSecond - tick_count_message_from_server_msec) > WAITING_TIME_MESSAGE_FROM_SERVER_SEC) 
             && GSM_Get_flag_power() == GSM_MUST_ENABLE)  
          {
             IncrCntSessionServer(number_server_transmit);
             memset((char*)&FlagMessageTMR.command, 0, sizeof(FlagMessageTMR));
           //  FlagMessageTMR.command = MESSAGE_TCP_CLOSED;
           //  WriteTelemetry(flag_event, EVENT_WAITING_TIME_MESSAGE_FROM_SERVER_MSEC, GSM_Get_current_communication_gsm_number(), EVENT_END);
           //  xSemaphoreGive(xMdmRxSemaphore);
             GSM_ExtremalPowerDown(EVENT_WAITING_TIME_MESSAGE_FROM_SERVER_MSEC);
             rez_GSM_TCPConnect = TCP_NON_CONNECT;
          }
          ParsingMessageTMR();
       }
       else
       {
          if(rez_GSM_TCPConnect == TCP_CONNECTING)
             ParamServer.success_session[number_server_transmit] = _FALSE;
          tick_count_message_from_server_msec = SystemUpTimeSecond;
       }
       if(rez_GSM_TCPConnect == TCP_ERROR_CONNECT_SERVER)
       {
          IncrCntSessionServer(number_server_transmit);
          GSM_ExtremalPowerDown(EVENT_ERROR_CONNECT_SERVER);
          FlagMessageTMR.command = MESSAGE_BT_TCP_NON;
          QueueGSMModemHandleStatus = EVENT_NON;
          rez_GSM_TCPConnect = TCP_NON_CONNECT;
       }
    }
    else
    {
        __NOP();
    }
    //-------------------
    if(((xTaskGetTickCount() - GSM_Get_tick_count_message_from_GSM_msec()) > WAITING_TIME_MESSAGE_FROM_GSM_MSEC) 
        && GSM_Get_flag_power() == GSM_MUST_ENABLE
        && GSM_Get_Status_power() == GSM_POWER_ON)
    {// превышен таймаут ожидания ответов от модема
          // 
         IncrCntSessionServer(number_server_transmit);
         memset((char*)&FlagMessageTMR.command, 0, sizeof(FlagMessageTMR));
         GSM_ExtremalPowerDown(EVENT_GSM_ERROR);   

         
         ParamServer.success_session[number_server_transmit] = _FALSE;
       //  CalculationNextCommunicationSession();
       //  successful_transfer_data_to_server = _NULL2;
         rez_GSM_TCPConnect = TCP_NON_CONNECT;
    }    
    //-------------------
    if(((xTaskGetTickCount() - GSM_Get_tick_count_powerON_GSM_msec()) > WAITING_TIME_CONNECTING_SERVER_GSM_MSEC) 
        && GSM_Get_flag_power() == GSM_MUST_ENABLE
        && GSM_Get_Status_power() == GSM_POWER_ON
        && rez_GSM_TCPConnect != TCP_YES_CONNECT)
    {
       IncrCntSessionServer(number_server_transmit);
       ParamServer.success_session[number_server_transmit] = _FALSE;
       GSM_ExtremalPowerDown(EVENT_OFF_EXTREMAL_GSM_MODEM);
    }
      
    //-------------------------------------------------------------------------------------------
    if(GSM_Get_Status_power() == POWER_NON && GSM_Get_flag_power() == GSM_MUST_DISABLE
       && (ParamServer.success_session[0] != _NULL2 || ParamServer.success_session[1] != _NULL2 || ParamServer.success_session[2] != _NULL2))
    {
       TransmitDebugMessageOptic("CalculationNext\r\n", 0);
       if(ParamServer.success_session[0] == _FALSE || ParamServer.success_session[1] == _FALSE || ParamServer.success_session[2] == _FALSE)
         ++flagPowerONGSMOneTime;
       CalculationNextCommunicationSession();        
       ParamServer.success_session_old[0] = ParamServer.success_session[0];
       ParamServer.success_session_old[1] = ParamServer.success_session[1];
       ParamServer.success_session_old[2] = ParamServer.success_session[2];
       ParamServer.success_session[0] = _NULL2;
       ParamServer.success_session[1] = _NULL2;
       ParamServer.success_session[2] = _NULL2;
    }
    //-------------------------------------------------------------------------------------------
    //SMS_Output();
    //-------------------------------------------------------------------------------------------  
 //   if (GSM_Get_Status_power() == POWER_NON 
 //       && GSM_Get_flag_power() == GSM_MUST_DISABLE)
        Valve_Parsing();
    //--------------------------------------------------------------------------------------------
          ///////////////////////////
      if(StatusSystem_buf.word != StatusSystem.word)
      {
         // WriteChangeArcStatus(EVENT_BEGIN);
         StatusSystem_buf.word = StatusSystem.word;
         WriteSystemArcRecord(SYSTEM_CHANGE_STATUS, GSM_Get_current_communication_gsm_number());
         
         StatusSystem.bit.start_program = 0;
         StatusSystem.bit.RCC_CSR = 0;
         if(StatusSystem.bit.hard_fault)
         {
            StatusSystem.bit.hard_fault = 0;
          //  WriteSystemArcRecord(EVENT_RESTART, GSM_Get_current_communication_gsm_number());
            Eeprom_WriteChar((uint32_t)&hard_fault,  0); 
         }
         // WriteChangeArcStatus(EVENT_END);
      }

      //--------------------------------------------------------------------------------------------
     flag_end_task &= ~0x01;
    osDelay(4);
  }
}
  // GSM GSM GSM GSM GSM GSM GSM GSM GSM GSM GSM GSM GSM GSM GSM GSM GSM GSM GSM GSM GSM GSM GSM GSM GSM GSM GSM GSM GSM 
  // GSM GSM GSM GSM GSM GSM GSM GSM GSM GSM GSM GSM GSM GSM GSM GSM GSM GSM GSM GSM GSM GSM GSM GSM GSM GSM GSM GSM GSM 
  // GSM GSM GSM GSM GSM GSM GSM GSM GSM GSM GSM GSM GSM GSM GSM GSM GSM GSM GSM GSM GSM GSM GSM GSM GSM GSM GSM GSM GSM 
  
// GSM UART GSM UART GSM UART GSM UART GSM UART GSM UART GSM UART GSM UART GSM UART GSM UART GSM UART GSM UART GSM UART GSM UART 
// GSM UART GSM UART GSM UART GSM UART GSM UART GSM UART GSM UART GSM UART GSM UART GSM UART GSM UART GSM UART GSM UART GSM UART 
// GSM UART GSM UART GSM UART GSM UART GSM UART GSM UART GSM UART GSM UART GSM UART GSM UART GSM UART GSM UART GSM UART GSM UART 
/* StartGSM_RX_UARTTask function */
void StartGSM_RX_UARTTask(void const * argument)
{
  for(;;)
  {
     stack_GSM_RX_UARTTask = uxTaskGetStackHighWaterMark(NULL);
     fGSM_RX_UARTTask(); // РѕР±СЂР°Р±РѕС‚С‡РёРє РІС…РѕРґВ¤С‰РёС… СЃРѕРѕР±С‰РµРЅРёР№
     flag_end_task &= ~0x02;
     osDelay(1);
  }
}
  // GSM UART GSM UART GSM UART GSM UART GSM UART GSM UART GSM UART GSM UART GSM UART GSM UART GSM UART GSM UART GSM UART GSM UART 
  // GSM UART GSM UART GSM UART GSM UART GSM UART GSM UART GSM UART GSM UART GSM UART GSM UART GSM UART GSM UART GSM UART GSM UART 
  // GSM UART GSM UART GSM UART GSM UART GSM UART GSM UART GSM UART GSM UART GSM UART GSM UART GSM UART GSM UART GSM UART GSM UART 

//-----------------------------------------------------------------------------
/* Start GasmeterTask function */
void GasmeterTask(void const * argument)
{
  /* USER CODE BEGIN StartDefaultTask */
 // IntArcFlag = 2;
   System_Init_volue();
   StatusSystem.bit.start_program = 1;
   StatusSystem.bit.RCC_CSR = RCC->CSR >> 24;
   LL_RCC_ClearResetFlags();
   StatusSystem.bit.hard_fault = hard_fault;
   NVIC_EnableIRQ(RTC_Alarm_IRQn);
   InitModules();
   xSemaphoreGive(xEepromWriteFrameReadySemaphore);
  /* Infinite loop */
 //  while(!Error_IsFatal(Gl_Error)) 
   while(1)
   {
      stack_GasmeterTask = uxTaskGetStackHighWaterMark(NULL);
      xSemaphoreTake(xRTCSemaphore, portMAX_DELAY);
      System_WatchdogRefresh();
      
      Gl_Error = Gasmeter_Process();
    //  pressure_abs_gas = pressure_abs;//###

      
      if(FlagBadSensor)
      {
         ++cnt_error_SGM;        
         if(FlagBadSensor <= SensCntErrorLong)
         {
            if(cnt_error_SGM > cnt_error_SGM_long_eeprom)
            { 
               error_in_SGM_module = 1;
            }
         }
         if(FlagBadSensor > SensCntErrorLong && FlagBadSensor < GasMeter.SensCnt)
         {
            if(cnt_error_SGM > cnt_error_SGM_short_eeprom)
            { 
               error_in_SGM_module = 1;
            }
         }
         if(FlagBadSensor >= GasMeter.SensCnt)// GasMeter.SensCnt
         {
            if(cnt_error_SGM > cnt_error_SGM_eeprom)
            { 
               error_in_SGM_module = 1;

            }
         }
         if(cnt_error_SGM > cnt_error_SGM_eeprom)
         {
           StatusSystem.bit.error_in_SGM_module = 1;          
         }
         if(!(cnt_error_SGM % 10))
         InitModules();
      } 
      else
      {
         if(cnt_error_SGM)
         {
            cnt_error_SGM = 0;
            InitModules();
         }
         error_in_SGM_module = 0;
         StatusSystem.bit.error_in_SGM_module = 0;
      }
      //-------------------------------------------
      if(error_in_SGM_module || StatusCrash.bit.error_in_SGM_module)
        ClearFlow();
      
      //-------------------------------------------
      if(READ_BIT(GPIOB->IDR, GPIO_IDR_IDR_2) != pinPB2_old)
      {
         sprintf((char*)OldValue,"%d", pinPB2_old); 
         pinPB2_old = READ_BIT(GPIOB->IDR, GPIO_IDR_IDR_2);
         sprintf((char*)NewValue, "%d", pinPB2_old); 
         WriteChangeArcRecord(CHANGE_PIN_CALIB_KEY, CHANGE_PROGRAM);   
      }
      if(READ_BIT(GPIOB->IDR, GPIO_IDR_IDR_2) == 0) 
      {// pressed the button
         ++cnt_pressed_button_calib;
         if(cnt_pressed_button_calib > 5 && !CLB_lock_IT)
         {
             CLB_lock_IT = 1;
             cnt_pressed_button_calib = -10;
         }
      }
      else
      {
        CLB_lock_IT = 0;
        cnt_pressed_button_calib = 0;
      }      
      //-----------------------------------------
      if(   Global_Menu != MAIN_MENU_VOLUME 
            && (Time_GetSystemUpTimeSecond() - PressKeyTime) > 180
            && enable_switch_menu == _OK)
               Global_Menu = MAIN_MENU_VOLUME;            
         //-------------------------------------
      if(GasMeter.VE_Lm3 > 99999.9999)
           GasMeter.VE_Lm3 = 0;      
      //-------------------------------------------
      LCD8_Menu(0,0);  
      ///////////////////////////
      if(timeSystemUnix >= timeUnixGSMNextConnect 
         && GSM_Get_flag_power() == GSM_MUST_DISABLE 
         && (timeSystemUnix - timeGSMUnixStart) > 10 && mode_conservation != 1)
      {
          flag_crypt_message[SOURCE_TCP] = _FALSE;
          flag_crypt_message[SOURCE_CSD] = _FALSE;
          TransmitDebugMessageOptic("GSMStart-\r\n", GSM_Get_current_communication_gsm_number());
          timeGSMUnixStart = timeSystemUnix;
          timeUnixGSMNextConnect = timeSystemUnix + 3600*6; // do not interfere with shutdown
          if(hard_fault)
          {
             ParamServer.success_session[0] = _FALSE;
          }
          else
          {
             if(StatusDevice.GSM_Modem_Power_optic != _OK && StatusDevice.GSM_Modem_Power_switch != _OK)
               StatusDevice.GSM_Modem_Power_mode = _OK;
             
             if(StatusDevice.GSM_Modem_Power_optic == _OK)
             {
               StatusDevice.GSM_Modem_Power_optic = _FALSE;
               number_server_transmit = 0;                 
               WriteTelemetry(TELEM_EVENT_START_GSM_FOR_OPTIC, 0, 0, EVENT_BEGIN);
               WriteTelemetry(TELEM_EVENT_START_GSM_FOR_OPTIC, EVENT_OK, GSM_Get_current_communication_gsm_number() + 1, EVENT_END);
               ParamServer.success_session_old[0] = _OK;
               ParamServer.success_session_old[1] = _OK;
               ParamServer.success_session_old[2] = _OK;

               cnt_GSM_TCP_Connect_FALSE = 0;
               flagPowerONGSMOneTime = 0;
             }
             if(StatusDevice.GSM_Modem_Power_switch == _OK)
             {
               StatusDevice.GSM_Modem_Power_switch = _FALSE;
               number_server_transmit = 0;                             
               WriteTelemetry(TELEM_EVENT_START_GSM_FOR_BUTTON, 0, 0, EVENT_BEGIN);
               WriteTelemetry(TELEM_EVENT_START_GSM_FOR_BUTTON, EVENT_OK, GSM_Get_current_communication_gsm_number() + 1, EVENT_END);
               ParamServer.success_session_old[0] = _OK;
               ParamServer.success_session_old[1] = _OK;
               ParamServer.success_session_old[2] = _OK;

               cnt_GSM_TCP_Connect_FALSE = 0;
               flagPowerONGSMOneTime = 0;
             }            
             if(StatusDevice.GSM_Modem_Power_mode == _OK)
             {
                StatusDevice.GSM_Modem_Power_mode = _FALSE;
                if(flagPowerONGSMOneTime == 1)
                {
                   WriteTelemetry(TELEM_EVENT_START_GSM_REPEATED, 0, 0, EVENT_BEGIN);
                   WriteTelemetry(TELEM_EVENT_START_GSM_REPEATED, EVENT_OK, GSM_Get_current_communication_gsm_number() + 1, EVENT_END);
                }
                else
                {
                   if(cnt_GSM_TCP_Connect_FALSE)
                   {
                      WriteTelemetry(TELEM_EVENT_START_GSM_REZERVED, 0, 0, EVENT_BEGIN);
                      WriteTelemetry(TELEM_EVENT_START_GSM_REZERVED, EVENT_OK, GSM_Get_current_communication_gsm_number() + 1, EVENT_END);
                   }
                   else
                   {
                      number_server_transmit = 0;
                      WriteTelemetry(TELEM_EVENT_START_GSM_FOR_TIME, 0, 0, EVENT_BEGIN);
                      WriteTelemetry(TELEM_EVENT_START_GSM_FOR_TIME, EVENT_OK, GSM_Get_current_communication_gsm_number() + 1, EVENT_END);
                   }
                }
             }
             
             if(MdmURL_Server[0][0] == 0 && MdmURL_Server[1][0] == 0)
                ParamServer.lack_of_two_servers = _OK;
             else
                ParamServer.lack_of_two_servers = _FALSE; 
                
             if(!cnt_GSM_TCP_Connect_FALSE && !flagPowerONGSMOneTime)
             {
                if(MdmURL_Server[0][0] != 0)
                {
                   TransmitDebugMessageOptic("GSMStartQ0\r\n", 0);
                   QueueSendGSMModem(TRANSMIT_TCP_SERVER, 0, SMS_NON);
                   ParamServer.availability_server1 = 1;
                }
                if(MdmURL_Server[1][0] != 0)
                {
                   TransmitDebugMessageOptic("GSMStartQ1\r\n", 0);
                   QueueSendGSMModem(TRANSMIT_TCP_SERVER, 1, SMS_NON);
                   ParamServer.availability_server2 = 2;
                }
                if(MdmURL_Server[2][0] != 0)
                {
                   TransmitDebugMessageOptic("GSMStartQ2\r\n", 0);
                   QueueSendGSMModem(TRANSMIT_TCP_SERVER, 2, SMS_NON);
                   ParamServer.availability_server3 = 3;
                }
             }
             else
             {
               uint8_t rez = _FALSE;
               if(MdmURL_Server[0][0] != 0 && ParamServer.success_session_old[0] == _FALSE)
               {
                  TransmitDebugMessageOptic("GSMStartQ_0\r\n", 0);
                  QueueSendGSMModem(TRANSMIT_TCP_SERVER, 0, SMS_NON);
                  ParamServer.availability_server1 = 1;
                  rez = _OK;
               }
               if(MdmURL_Server[1][0] != 0 && ParamServer.success_session_old[1] == _FALSE)
               {
                  TransmitDebugMessageOptic("GSMStartQ_1\r\n", 0);
                  QueueSendGSMModem(TRANSMIT_TCP_SERVER, 1, SMS_NON);
                  ParamServer.availability_server2 = 2;
                  rez = _OK;
               }
               if(MdmURL_Server[2][0] != 0 && ParamServer.success_session_old[2] == _FALSE && ParamServer.lack_of_two_servers == _OK)
               {
                  TransmitDebugMessageOptic("GSMStartQ_2\r\n", 0);
                  QueueSendGSMModem(TRANSMIT_TCP_SERVER, 2, SMS_NON);
                  ParamServer.availability_server3 = 3;
                  rez = _OK;
               }
               if(rez == _FALSE)
               {//Something went wrong
                 cnt_GSM_TCP_Connect_FALSE = 0;
                 flagPowerONGSMOneTime = 0;
                 TransmitDebugMessageOptic("SWW\r\n", 0);
                 ParamServer.success_session[0] = _OK;
                 ParamServer.success_session[1] = _OK;
                 ParamServer.success_session[2] = _OK; 
               }
               ParamServer.success_session_old[0] = _NULL2;
               ParamServer.success_session_old[1] = _NULL2;
               ParamServer.success_session_old[2] = _NULL2;
             }
             QueueGSMModemHandleStatus = EVENT_NON; 
          }
       }      
       ///////////////////////////
      if(IntArcFlag)
      {
          WriteIntEventArcRecord(EVENT_INT_CURRENT_FLOW, EVENT_INT_BEGIN);
          if(OptoPort == OPTO_PORT_ENABLE && StatusDevice.optical_port_communication_session)
          {
             CalculateMotoHourceOPTIC(MOTO_HOURCE_STOP);
             CalculateMotoHourceOPTIC(MOTO_HOURCE_START);
          }
          Eeprom_WriteString((uint8_t*)&MotoHoursToMilsEEPROM.time_StopCPU, (uint8_t*)&MotoHoursToMils.time_StopCPU, sizeof(MotoHoursToMils), _FALSE);
          IntArcFlag =0;
          Calculation_residual_capacity_battery_percent();
          flag_measure_temperature = 1;
      }      

      ///////////////////////////
      if(((timeSystemUnix - time_record_Volume) > 600)
         &&  GSM_Get_Status_power() == POWER_NON 
         && GSM_Get_flag_power() == GSM_MUST_DISABLE)
      {
         time_record_Volume = timeSystemUnix;
         System_SaweBeforeReset();
      }
      ///////////////////////////
      for(uint8_t i = 0; i < 12; i++)
      {
         if(!flag_end_task)
         {
            EnterSTOPMode(_OK);
            break;
         }
         osDelay(1);
      }
      osDelay(1);
  }
 }

void DispatcherTask(void const * argument)
{
   while(1)
   {
      flag_end_task |= 0x20;
      stack_DispatcherTask = uxTaskGetStackHighWaterMark(NULL);
      //------------------------------------------------------
      /*
      if(StatusDevice.mode_conservation == 1)
      {
         System_WatchdogRefresh();    
          
         if(mode_conservation == 0)
         {
            Global_Menu = 0;            
            Eeprom_WriteChar((uint32_t)&mode_conservation, 1);
            if(HiPower_ON)
               GSM_ExtremalPowerDown(EVENT_GSM_ERROR);
            osDelay(1500);
    
            
            // Disable the write protection for RTC registers
            RTC->WPR = 0xCA;  // Password 1
            RTC->WPR = 0x53;  // Password 2             
            CLEAR_BIT(RTC->CR, RTC_CR_ALRAE);  // Disable Alarm A
            // Enable the write protection for RTC registers
            RTC->WPR = 0xFF;
            osDelay(1500);
            EXTI->IMR |= EXTI_IMR_IM1; // Enable interrept External Watchdog 

         }
         osDelay(1);
         OptoPort = OPTO_PORT_DISABLE ;    
         
               ///////////////////////////
         for(uint8_t i = 0; i < 4; i++)
         {
            if(!(flag_end_task & 0xDF))
            {
               EnterSTOPMode(_OK);
               break;
            }
            osDelay(1);
         }
      }
      else
      {
         if(mode_conservation == 1)
         {
            update_time_FMMeasurements();
            Eeprom_WriteChar((uint32_t)&mode_conservation, 0);
            sprintf((char*)OldValue,"%d", 1);
            sprintf((char*)NewValue,"%d", mode_conservation);
            WriteChangeArcRecord(CHANGE_MODE_CONSERVATION, SOURCE_PROGRAM);
           
            // Disable the write protection for RTC registers
            RTC->WPR = 0xCA;  // Password 1
            RTC->WPR = 0x53;  // Password 2             
            SET_BIT(RTC->CR, RTC_CR_ALRAE); // Enable Alarm A
            // Enable the write protection for RTC registers
            RTC->WPR = 0xFF;

            EXTI->IMR &= ~EXTI_IMR_IM1;// Disable interrept External Watchdog  
            GetDate_Time(&SystDate,&SystTime);
            timeSystemUnix = Get_IndexTime(SystDate, SystTime);
         }
      }
      */
      //------------------------------------------------------      
#ifdef E7
      if(body_cover_open_it_flag)
      {
         if(system_tick_ms - body_cover_open_it_time > 100)
         {
            if(!READ_BIT(GPIOC->IDR, GPIO_IDR_ID8))
            {
               body_cover_open = 1;
               body_cover_open_it_time = timeSystemUnix;
            }
            body_cover_open_it_flag = 0;
         }
      }
      if(timeSystemUnix - body_cover_open_it_time > 1)
      {
         if(READ_BIT(GPIOC->IDR, GPIO_IDR_ID8))
         {
            body_cover_open = 0;
         }
      }
#endif
      //---------------------------------------------------------
      /*
      if(battery_cover_open_it_flag)
      {
         if(system_tick_ms - battery_cover_open_it_time > 100)
         {
            if(!READ_BIT(GPIOC->IDR, GPIO_IDR_IDR_9))
            {
               battery_cover_open = 1;
               battery_cover_open_it_time = timeSystemUnix;
            }
            battery_cover_open_it_flag = 0;
         }
      }
      if(timeSystemUnix - battery_cover_open_it_time > 1)
      {
         if(READ_BIT(GPIOC->IDR, GPIO_IDR_IDR_9))
         {
            battery_cover_open = 0;
         }
      }
      */
      //----------------------------------------------------------
  //    if (KeyPressed == KEY_PRESSED_SHORT)
  //    {
  //        Switch_Next_Menu();
  //    }
  //    else
   //   {
   //      LCD8_Menu(0,0);    
    //  }
           //--------------------------------------------------------------------------------------------
     ParsingWarning();
     ParsingAlarm();
     ParsingCrash();
      //------------------------------------------------------------
      StackFreeRTOSAanalysis();
      //---------------------------------------------------------
#ifdef E7
      if(body_cover_open_it_flag == 0)// && battery_cover_open_it_flag == 0
#endif
         flag_end_task &= ~0x20;
      osDelay(1);
   }
}



/* Start GasmeterTask function */
void StartParsingComandsTask(void const * argument)
{
  while(1)
   {
      flag_end_task |= 0x10;
      stack_ParsingComandsTask = uxTaskGetStackHighWaterMark(NULL); //
      ParsingCommands();
      flag_end_task &= ~0x10;
      osDelay(4);
   }
}
//-----------------------------------------------------------------------------
static Error InitModules(void)
{
  Error error;
 
  error = Sgm_InitModule(); // power on
  
  
    error = FlowMeas_InitModule();
  if(sensEnable_eeprom)
  {
     for(uint8_t i=0; i < 1; i++)
     {
        if(sensEnable_eeprom & (BIT0 << i))
          GasMeter.SensEnable[i] = 0xFF;
     }
  }
  
  if(Error_IsNone(error)) 
    error = Gasmeter_InitModule();
  
  Sgm_DeinitModule();  // Сброс питания модулей
  if(Error_IsNone(error))
  {
     for(uint8_t i = 0; i < 4; i++)
     {
        if(strlen(EEPROM_SN_SGM[i]) !=0)
        {
           if(!strcmp(EEPROM_SN_SGM[i], (char*)Sensor_Const.SN_SGM[i]))
           {
             switch(i)
             {
             case 0:
               StatusSystem.bit.check_sn_sgm_false = 0;
               break;
             case 1:
               StatusSystem.bit.check_sn_sgm2_false = 0;
               break;
             case 2:
               StatusSystem.bit.check_sn_sgm3_false = 0;
               break;
             case 3:
               StatusSystem.bit.check_sn_sgm4_false = 0;
               break;
             default:break;
             }
           }
           else
           {
             switch(i)
             {
             case 0:
               StatusSystem.bit.check_sn_sgm_false = 1;
               break;
             case 1:
               StatusSystem.bit.check_sn_sgm2_false = 1;
               break;
             case 2:
               StatusSystem.bit.check_sn_sgm3_false = 1;
               break;
             case 3:
               StatusSystem.bit.check_sn_sgm4_false = 1;
               break;
             default:break;
             }
           }
        }
        else
        {
             switch(i)
             {
             case 0:
               StatusSystem.bit.check_sn_sgm_false = 0;
               break;
             case 1:
               StatusSystem.bit.check_sn_sgm2_false = 0;
               break;
             case 2:
               StatusSystem.bit.check_sn_sgm3_false = 0;
               break;
             case 3:
               StatusSystem.bit.check_sn_sgm4_false = 0;
               break;
             default:break;
             }
        }
     }
  }
  return error;
}
//-----------------------------------------------------------------------------
void vApplicationIdleHook( void )
{
  stack_IDLE = uxTaskGetStackHighWaterMark(NULL);
  
  if (KeyPressed == KEY_PRESSED_SHORT)
      Switch_Next_Menu();

} 


/**
  * @brief  This function is executed in case of error occurrence.
  * @retval None
  */
void Error_Handler(void)
{
  /* USER CODE BEGIN Error_Handler_Debug */
  /* User can add his own implementation to report the HAL error return state */

  /* USER CODE END Error_Handler_Debug */
}

