//=============================================================================
/// \file    ValveTask .c
/// \author  MVA
/// \date    30-Jan-2019
/// \brief   Implementing the Vlave-functionality.
//=============================================================================
// COPYRIGHT
//=============================================================================

#include "stm32l073xx.h"
#include "stm32l0xx.h"
#include "Time_g.h"
#include "USART_Modem.h"
#include "ValveTask.h"
#include "cmsis_os.h"
#include "gasmeter.h"
#include "global.h"
#include "OptoComand.h"
#include "eeprom.h"
#include "FlowMeasurement.h"
#include "Io.h"
#include "main_function.h"
#include "Archiv.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

//------------------------------------------------------------------------------
//TODO:  �������� ��������� ������� ����� ���������� �������
//       �������� ���� ���������� �������� �������
//       �������� ������� �������� ������� � GPRS ������.
//------------------------------------------------------------------------------

#define VALVE_OPEN_ON()   SET_BIT(GPIOA->ODR, GPIO_ODR_OD5)  
#define VALVE_OPEN_OFF()  CLEAR_BIT(GPIOA->ODR, GPIO_ODR_OD5)

#define VALVE_CLOSE_ON()   SET_BIT(GPIOA->ODR, GPIO_ODR_OD4)  
#define VALVE_CLOSE_OFF()  CLEAR_BIT(GPIOA->ODR, GPIO_ODR_OD4)

const uint8_t valve_enable_auto_control @".eeprom" = 0;  // 
const uint8_t cnt_valve_min             @".eeprom" = 6;  // 
const uint16_t time_to_open             @".eeprom" = 3000;
const uint16_t time_to_litte_open       @".eeprom" = 500;
const uint16_t time_to_close            @".eeprom" = 3000;
const uint16_t valve_open_delay         @".eeprom" = 30;  // ����� � �������� ���������
const uint32_t valve_going_beyond_Q_period_sec          @".eeprom" = 1000;  //
const float    valve_multiplier_Qmin_max                @".eeprom" = 1.0;// valve_Qmin
const float    valve_multiplier_Qmin_min                @".eeprom" = 0.25;// valve_min
const uint8_t  delay_slightly_open      @".eeprom" = 6;  // �������� ����������� 6 ���. �� ���������

//const char* Ansver[] = {"CLOSE", "OPEN", "CLOSE_REVERSE_FLOW", "CLOSE_Q_MAX", "CLOSE_NON_NULL", "NON_POWER", "UNKNOWN"};
//const char* message_valve_status[] = {"CLOSE_TCP", "OPEN", "CLOSE_REVERSE_FLOW", "CLOSE_Q_MAX", "CLOSE_NON_NULL", "NON_POWER", "UNKNOWN", "CLOSE_OPTIC"};



uint8_t valve_action = VALVE_ACTION_NON; // valve action to be performed depending on where the command came from
uint8_t valve_cnt_flow_non_null = 0;
uint8_t valve_going_beyond_Q_flag_begin=0;
uint8_t valve_reverse_flow_close_flag = 0;


uint64_t valve_going_beyond_Q_nexTime;
uint64_t valve_time_delay_open;

double valve_going_beyond_Q_volue=0;
//const uint8_t Valve_status @".eeprom" = VALVE_UNKNOWN; 


//#define _VALVE_OK 0
//#define _VALVE_NOT_OPEN 3
//#define _VALVE_NOT_ENABLE 4
//#define _VALVE_NOT_POWER  8
//#define _VALVE_ERR 1


//-----------------------------------------------------------------------------
// �������� p_Fors - ������������� �������� ������� ��� �������� ������
//-----------------------------------------------------------------------------
uint8_t Valve_Open(uint8_t p_Fors)
{
 // uint8_t  lo_Valve_status;
  uint8_t lo_FlagFlow = cnt_valve_min ;   // ����� �� �������
  
  if (valve_status == VALVE_OPEN)
  {
    valve_cnt_flow_non_null = 0;
    return VALVE_OPEN;  //
  }
  
  
  if(!READ_BIT(RCC->IOPENR,RCC_IOPENR_IOPDEN)) // 
     SET_BIT(RCC->IOPENR,RCC_IOPENR_IOPDEN); // enable RCC port IO D
   
  Set_valve_status(VALVE_CLOSE_OPEN);
  CalculateMotoHourceValve(MOTO_HOURCE_START);
  HiPower_ON |= BIT1;
   GSM_PWR_ON();  // enable power value
   
   osDelay(200);   
   if(READ_BIT(GPIOD->IDR, GPIO_IDR_ID2) == RESET)  // �� - �� �������!
   {
     HiPower_ON &= BIT0 | BIT1; // clear bit2 - bit7
     HiPower_ON &= ~BIT1;
     if(HiPower_ON == 0)
       GSM_PWR_OFF();
     Set_valve_status(VALVE_NON_POWER);
     return VALVE_NON_POWER;
   }
   
   // ���������� ������
   VALVE_OPEN_ON();   
   if(p_Fors == VALVE_OPEN_FORS_YES)
   {
    //  osDelay(time_to_open); 
    //  VALVE_OPEN_OFF();
    //  Set_valve_status(VALVE_OPEN);
    //  lo_Valve_status = VALVE_OPEN; 
      lo_FlagFlow = 0;
   }   
   else
   {
      osDelay(time_to_litte_open);
      VALVE_OPEN_OFF();    
   
      //++ �������� ����������� 6 ���. �� ���������
      for (int cnt = 0; cnt < delay_slightly_open; cnt++)
      {
        osDelay(1000); 
      }
      
      for (int cnt = 0; cnt < valve_open_delay; cnt++)  // 30
      {
         osDelay(1000);
         if(GasMeter.QgL_m3 < sensorTypeParameters[_SgmType].Q_min * valve_multiplier_Qmin_max  )   // Qmin G4 0.04
         {
            if(GasMeter.QgL_m3 < sensorTypeParameters[_SgmType].Q_min * valve_multiplier_Qmin_min)
            {
               lo_FlagFlow-- ;         //zero flow !
               if (lo_FlagFlow == 0)
               {
                 __NOP();
                 break;
               }
                 
            }
            else
              lo_FlagFlow = cnt_valve_min;
         }  
         else
         {
            lo_FlagFlow = cnt_valve_min;
            ++cnt; // accelerate twice
         }
      } 
    }
   
    if(lo_FlagFlow)       // ���� ������ - ������� ������
    {
       VALVE_CLOSE_ON();   
       osDelay(time_to_close); 
       VALVE_CLOSE_OFF();  
       
       if(valve_cnt_flow_non_null > 1)
          Set_valve_status(VALVE_CLOSE_NON_NULL);
       ++valve_cnt_flow_non_null;
    }
    else
    {
       VALVE_OPEN_ON();   
       osDelay(time_to_open); 
       VALVE_OPEN_OFF(); 
    //   lo_Valve_status = VALVE_OPEN;
       Set_valve_status(VALVE_OPEN);
  } 
  osDelay(100);
  HiPower_ON &= BIT0 | BIT1; // clear bit2 - bit7
  HiPower_ON &= ~BIT1;
  if(HiPower_ON == 0)
     GSM_PWR_OFF();   
  
   //Eeprom_WriteChar((uint32_t)&valve_status , lo_Valve_status);
   valve_time_delay_open = timeSystemUnix;
   if((valve_status == VALVE_OPEN || valve_status == VALVE_CLOSE_NON_NULL) && enable_MDM_change_valve)
      timeUnixGSMNextConnect = timeSystemUnix;
   CalculateMotoHourceValve(MOTO_HOURCE_STOP);
   return lo_FlagFlow; 
}  
    
//-----------------------------------------------------------------------------
void Valve_Close(uint8_t flag_close_source)
{
 
//  uint8_t lo_FlagFlow = _VALVE_NOT_CLOSED;   // ����� �� �������
//  uint8_t lo_Valve_status; 
  
  if (valve_status == VALVE_CLOSE_TCP || valve_status == VALVE_CLOSE_OPTIC)
    return;  

  Set_valve_status(VALVE_OPEN_CLOSE);
  
  if(!READ_BIT(RCC->IOPENR,RCC_IOPENR_IOPDEN))
     SET_BIT(RCC->IOPENR,RCC_IOPENR_IOPDEN);  // enable RCC port IO D
  CalculateMotoHourceValve(MOTO_HOURCE_START);
   HiPower_ON |= BIT1;
   GSM_PWR_ON();  
   osDelay(200);
   
   if(READ_BIT(GPIOD->IDR, GPIO_IDR_ID2) == RESET)  // �� - �� �������!
   {
      HiPower_ON &= BIT0 | BIT1; // clear bit2 - bit7
      HiPower_ON &= ~BIT1;
      if(HiPower_ON == 0)
        GSM_PWR_OFF();
      Set_valve_status(VALVE_NON_POWER);
      return;
   }

   VALVE_CLOSE_ON();   
   osDelay(time_to_close);
    
   VALVE_CLOSE_OFF();    
   osDelay(100);
   HiPower_ON &= BIT0 | BIT1; // clear bit2 - bit7
   HiPower_ON &= ~BIT1;
   if (HiPower_ON == 0)
       GSM_PWR_OFF();   
/*
  for (int cnt = 0; cnt < Valve_Open_delay; cnt++)  
    {
      osDelay(Config_GasmeterMeasurementIntervalNormalModeFM +500);
      if(GasMeter.QgL ==0.0)   // ����� ��������? 
      {
        lo_FlagFlow-- ;         //����� ������� !
        if (lo_FlagFlow == 0)
          break;
      }  
      else
        lo_FlagFlow = _VALVE_NOT_CLOSED;
      } 
    }

   if (lo_FlagFlow)
     lo_Valve_status = VALVE_UNKNOWN;
   else  */
   
  // lo_Valve_status = VALVE_CLOSE;   
   
   //Eeprom_WriteChar((uint32_t)&valve_status , lo_Valve_status);
   Set_valve_status(flag_close_source);
   if(enable_MDM_change_valve)
       timeUnixGSMNextConnect = timeSystemUnix;
   CalculateMotoHourceValve(MOTO_HOURCE_STOP);
}
     
//------------------------------------------------------------------------------
uint8_t PFW_valve(char* parString)
{
  //  uint8_t lo_Valve_status;
 //   const char* Ansver[] = {"CLOSE", "OPEN", "CLOSE_REVERSE_FLOW", "CLOSE_Q_MAX", "CLOSE_NON_NULL", "NON_POWER", "UNKNOWN"};
  //  const char* lo_ErrMsg[] = {"������ ������!\r\n", "��� ������� �������!!\r\n",\
  //          "��� ����������!!\r\n","������ �� ������\r\n", "������ �������\r\n"};
 //   char* lo_message; 
    
   if(parString ==0)   
   {
    if(while_TransmitEnable() == _FALSE) return 1;
    sprintf((char*)TransmitBuffer,"Valve_status=%d\r\n", valve_status);   // +
    Opto_Uart_Transmit(); 
    return OPTIC_OK; 
  }
  else
  {
     if(CheckChrString((uint8_t*)parString, "%d", strlen(parString)) == _FALSE) return OPTIC_INCORRECT_VALUE; 
     if(*parString == '0')
     {
        SetDateTimeToArchiveChanges();
        sprintf((char*)ChArc.OldValue,"%d", valve_status); // +
        sprintf((char*)ChArc.NewValue,"%d", VALVE_CLOSE_OPTIC); // +

        valve_action = VALVE_ACTION_CLOSE_OPTIC;
     }
     else 
        if(*parString == '1')
        {
           if(valve_status == VALVE_CLOSE_Q_MAX || valve_status == VALVE_CLOSE_REVERSE_FLOW || valve_status == VALVE_CLOSE_BATARY || valve_status == VALVE_CLOSE_OPEN_BODY || valve_status == VALVE_CLOSE_TCP )
             return OPTIC_ACCESS_DENIED;
           SetDateTimeToArchiveChanges();
           sprintf((char*)ChArc.OldValue,"%d", valve_status); // +
           sprintf((char*)ChArc.NewValue,"%d", 1); // +
           valve_action = VALVE_ACTION_OPEN_OPTIC;
        }
        else 
           return OPTIC_INCORRECT_VALUE; 
     WriteChangeArcRecord(CHANGE_VALVE_OPTIC, CHANGE_OPTIC);

  }
       return OPTIC_OK; 
}
//------------------------------------------------------------------------------
uint8_t PFW_valve_force(char* parString)
{
  //  uint8_t lo_Valve_status;
 //   const char* Ansver[] = {"CLOSE", "OPEN", "CLOSE_REVERSE_FLOW", "CLOSE_Q_MAX", "CLOSE_NON_NULL", "NON_POWER", "UNKNOWN"};
  //  const char* lo_ErrMsg[] = {"������ ������!\r\n", "��� ������� �������!!\r\n",\
  //          "��� ����������!!\r\n","������ �� ������\r\n", "������ �������\r\n"};
 //   char* lo_message; 
    
   if(parString ==0)   
   {
    if(while_TransmitEnable() == _FALSE) return 1;
    sprintf((char*)TransmitBuffer,"Valve_status=%d\r\n", valve_status);   // +
    Opto_Uart_Transmit(); 
    return OPTIC_OK; 
  }
  else
  {
      uint32_t temp;
    if(CheckChrString((uint8_t*)parString, "%d", strlen(parString)) == _FALSE) return OPTIC_INCORRECT_VALUE; 
      temp = atoi(parString);
        if(temp == 1)
        {
           if(valve_status == VALVE_CLOSE_Q_MAX || valve_status == VALVE_CLOSE_REVERSE_FLOW || valve_status == VALVE_CLOSE_BATARY || valve_status == VALVE_CLOSE_OPEN_BODY || valve_status == VALVE_CLOSE_TCP )
             return OPTIC_ACCESS_DENIED;
           SetDateTimeToArchiveChanges();
           sprintf((char*)ChArc.OldValue,"%d", valve_status); // +
           sprintf((char*)ChArc.NewValue,"%d", VALVE_OPEN_FORCE); // +
           valve_action = VALVE_ACTION_OPEN_OPTIC_FORCE;
        }
        else 
           return OPTIC_INCORRECT_VALUE; 
     WriteChangeArcRecord(CHANGE_VALVE_OPTIC, CHANGE_OPTIC);

  }
       return OPTIC_OK; 
}
//------------------------------------------------------------------------------
uint8_t PFW_valve_time(char* parString)
{
 
  if(parString == 0)   
  {
    if(while_TransmitEnable() == _FALSE) return 1;
    
    sprintf((char*)TransmitBuffer,"VALVE_TIME=%d;%d;%d\r\n", time_to_open, time_to_close, time_to_litte_open);  // +
    Opto_Uart_Transmit(); 
    return OPTIC_OK; 
  }
  else
  {
     uint32_t lo_Time_Open, lo_Time_Close, lo_Time_LOpen;
     if(CLB_lock == 0x55)
     {
       if(CheckChrString((uint8_t*)parString, "%d;", strlen(parString)) == _FALSE) return OPTIC_INCORRECT_VALUE;
       lo_Time_Open = atoi(parString);
       if(lo_Time_Open < 1000 || lo_Time_Open > 20000)
         return OPTIC_INCORRECT_VALUE;

       parString = strchr(parString,';');
       if(parString)
       { 
          lo_Time_Close = atoi(++parString);
          if(lo_Time_Close < 1000 || lo_Time_Close > 20000)
             return OPTIC_INCORRECT_VALUE;
       }
       parString = strchr(parString,';');
       if(parString)
       { 
          lo_Time_LOpen = atoi(++parString);
          if(lo_Time_LOpen < 500 || lo_Time_LOpen > 10000)
             return OPTIC_INCORRECT_VALUE;
       }
       
       SetDateTimeToArchiveChanges();
       sprintf((char*)ChArc.OldValue,"%d,%d,%d", time_to_open, time_to_close, time_to_litte_open);  // +

       Eeprom_WriteWord((uint32_t)&time_to_open, lo_Time_Open);
       Eeprom_WriteWord((uint32_t)&time_to_close, lo_Time_Close);       
       Eeprom_WriteWord((uint32_t)&time_to_litte_open, lo_Time_LOpen);
       sprintf((char*)ChArc.NewValue,"%d,%d,%d", time_to_open, time_to_close, time_to_litte_open);  // +
       WriteChangeArcRecord(CHANGE_VALVE_TIME, CHANGE_OPTIC);
     }
     else
       return OPTIC_ACCESS_DENIED;
  
  }
 return OPTIC_OK; 
 }

//------------------------------------------------------------------------------
uint8_t PFW_valve_check_open_time(char* parString)
{
  
  if(parString ==0)   
  {
    if(while_TransmitEnable() == _FALSE) return 1;

    sprintf((char*)TransmitBuffer,"CHECK_OPEN_TIME=%u\r\n",  // +
      valve_open_delay);  
     Opto_Uart_Transmit(); 
     return OPTIC_OK; 
  }
  else
  {
       if(change_valve_open_delay(parString, CHANGE_OPTIC) == _OK)
           return OPTIC_OK;
       else
           return OPTIC_INCORRECT_VALUE;
   }  
}

uint8_t change_valve_open_delay(char* ptr_str, uint8_t source)
{
   uint8_t rez;
   uint16_t  lo_Time_Open;
   if(CheckChrString((uint8_t*)ptr_str, "%d", strlen(ptr_str)) == _FALSE) return OPTIC_INCORRECT_VALUE;
   lo_Time_Open = atoi(ptr_str);
  
   if(lo_Time_Open < 6 || lo_Time_Open > 600)
       rez = _FALSE;
   else
   {
       
       SetDateTimeToArchiveChanges();
       sprintf((char*)ChArc.OldValue,"%d", valve_open_delay);  // +
       Eeprom_WriteWord((uint32_t)&valve_open_delay, lo_Time_Open);
       sprintf((char*)ChArc.NewValue,"%d", valve_open_delay);   // +
       WriteChangeArcRecord(CHANGE_CHECK_OPEN_TIME, source);
       rez = _OK;
   }
   return rez;
}

//------------------------------------------------------------------------------
uint8_t PFW_cnt_valve_min(char* parString)
{
 
  
  if(parString ==0)   
  {
     if(while_TransmitEnable() == _FALSE) return 1;

     sprintf((char*)TransmitBuffer,"CNT_VALVE_MIN=%d\r\n", cnt_valve_min);   // +
     Opto_Uart_Transmit(); 
     return OPTIC_OK; 
  }
  else
  {
     if(change_cnt_valve_min(parString, CHANGE_OPTIC) == _OK)
       return OPTIC_OK; 
     else
       return OPTIC_INCORRECT_VALUE;
  }  
}

uint8_t change_cnt_valve_min(char* ptr_str, uint8_t source)
{
   uint32_t temp;
     if(CheckChrString((uint8_t*)ptr_str, "%d", strlen(ptr_str)) == _FALSE) return OPTIC_INCORRECT_VALUE;
       temp = atoi(ptr_str);
       if(temp > 100 || temp >= valve_open_delay)
          return _FALSE;
       
       SetDateTimeToArchiveChanges();
       sprintf((char*)ChArc.OldValue,"%d", cnt_valve_min);   // +
       Eeprom_WriteChar((uint32_t)&cnt_valve_min, temp);
       sprintf((char*)ChArc.NewValue,"%d", cnt_valve_min);   // +
       WriteChangeArcRecord(CHANGE_CNT_VALVE_MIN, source);
       return _OK;
}
//------------------------------------------------------------------------------
uint8_t PFW_Valve_min(char* parString)
{
  
  
  if(parString ==0)   
  {
     if(while_TransmitEnable() == _FALSE) return 1;

     sprintf((char*)TransmitBuffer,"VALVE_MIN=%0.4f\r\n", valve_multiplier_Qmin_min);   // +
     Opto_Uart_Transmit(); 
     return OPTIC_OK; 
  }
  else
  {

     if(change_valve_min(parString, CHANGE_OPTIC) == _OK)
       return OPTIC_OK; 
     else
       return OPTIC_INCORRECT_VALUE;

  }  
}

uint8_t change_valve_min(char* ptr_str, uint8_t source)
{
  float lo_Valve_min;
  uint8_t rez;
  if(CheckChrString((uint8_t*)ptr_str, "%d.", strlen(ptr_str)) == _FALSE) return OPTIC_INCORRECT_VALUE;
       lo_Valve_min = atof(ptr_str);
       if(lo_Valve_min < 0.01 || lo_Valve_min >= valve_multiplier_Qmin_max )
          rez = _FALSE;
       else
       {
       SetDateTimeToArchiveChanges();
       sprintf((char*)ChArc.OldValue,"%f", valve_multiplier_Qmin_min); // +
       Eeprom_WriteFloat((uint32_t)&valve_multiplier_Qmin_min, lo_Valve_min);
       sprintf((char*)ChArc.NewValue,"%f", valve_multiplier_Qmin_min);   // +
       WriteChangeArcRecord(CHANGE_VALVE_MIN, source);
       rez = _OK;
       }
       return rez;
}
//------------------------------------------------------------------------------
uint8_t PFW_Valve_Qmin(char* parString)
{
  if(parString ==0)   
  {
    if(while_TransmitEnable() == _FALSE) return 1;

    sprintf((char*)TransmitBuffer,"VALVE_QMIN=%0.4f\r\n", valve_multiplier_Qmin_max);   // +
     Opto_Uart_Transmit(); 
     return OPTIC_OK; 
  }
  else
  {
     if(change_valve_Qmin(parString, CHANGE_OPTIC) == _OK)
       return OPTIC_OK; 
     else
       return OPTIC_INCORRECT_VALUE;
  }  
}

uint8_t change_valve_Qmin(char* ptr_str, uint8_t source)
{
  float temp;
  uint8_t rez;
  if(CheckChrString((uint8_t*)ptr_str, "%d.", strlen(ptr_str)) == _FALSE) return OPTIC_INCORRECT_VALUE;
       temp = atof(ptr_str);
       if(temp > 1.5 || temp <= valve_multiplier_Qmin_min)
          rez = _FALSE;
       else
       {
       SetDateTimeToArchiveChanges();
       sprintf((char*)ChArc.OldValue,"%f", valve_multiplier_Qmin_max); // +
       Eeprom_WriteFloat((uint32_t)&valve_multiplier_Qmin_max, temp);
       sprintf((char*)ChArc.NewValue,"%f", valve_multiplier_Qmin_max);   // +
       WriteChangeArcRecord(CHANGE_VALVE_Q_MIN, source);
       rez = _OK;
       }
       return rez;
}
//------------------------------------------------------------------------------
uint8_t PFW_delay_slightly_open(char* parString)
{
  if(parString ==0)   
  {
    if(while_TransmitEnable() == _FALSE) return 1;
    sprintf((char*)TransmitBuffer,"VALVE_DELAY_BEFORE_OPEN=%d\r\n", delay_slightly_open);   // +
    Opto_Uart_Transmit(); 
    return OPTIC_OK; 
  }
  else
  {
     if(change_delay_slightly_open(parString, CHANGE_OPTIC) == _OK)
       return OPTIC_OK; 
     else
       return OPTIC_INCORRECT_VALUE;
  }  
}

uint8_t change_delay_slightly_open(char* ptr_str, uint8_t source)
{
  uint8_t temp;
  uint8_t rez;
  if(CheckChrString((uint8_t*)ptr_str, "%d", strlen(ptr_str)) == _FALSE) return OPTIC_INCORRECT_VALUE;
       temp = atoi(ptr_str);
       if(temp > 20 || temp < 2)
          rez = _FALSE;
       else
       {
       SetDateTimeToArchiveChanges();
       sprintf((char*)ChArc.OldValue,"%d", delay_slightly_open); // +
       Eeprom_WriteChar((uint32_t)&delay_slightly_open, temp);
       sprintf((char*)ChArc.NewValue,"%d", delay_slightly_open);   // +
       WriteChangeArcRecord(CHANGE_VALVE_DELAY_BEFORE_OPEN, source);
       rez = _OK;
       }
       return rez;
}
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
uint8_t PFW_valve_going_beyond_Q_period_h(char* parString)
{
  if(parString ==0)   
  {
    if(while_TransmitEnable() == _FALSE) return 1;

    sprintf((char*)TransmitBuffer,"VALVE_CLOSE_Q_PH=%d\r\n", valve_going_beyond_Q_period_sec);   // +
     Opto_Uart_Transmit(); 
     return OPTIC_OK; 
  }
  else
  {
     if(change_valve_going_beyond_Q_period_h(parString, CHANGE_OPTIC) == _OK)
       return OPTIC_OK; 
     else
       return OPTIC_INCORRECT_VALUE;
  }  
}

uint8_t change_valve_going_beyond_Q_period_h(char* ptr_str, uint8_t source)
{
  uint32_t temp;
  uint8_t rez;
  if(CheckChrString((uint8_t*)ptr_str, "%d", strlen(ptr_str)) == _FALSE) return OPTIC_INCORRECT_VALUE;
       temp = atoi(ptr_str);
       if(temp < 1 || temp > 2678400)
          rez = _FALSE;
       else
       {
       SetDateTimeToArchiveChanges();
       sprintf((char*)ChArc.OldValue,"%d", valve_going_beyond_Q_period_sec); // +
       Eeprom_WriteDword((uint32_t)&valve_going_beyond_Q_period_sec, temp);
       sprintf((char*)ChArc.NewValue,"%d", valve_going_beyond_Q_period_sec); // +
       WriteChangeArcRecord(CHANGE_VALVE_CLOSE_Q_PH, source);
       
       valve_going_beyond_Q_volue = sensorTypeParameters[_SgmType].Q_max * ((float)valve_going_beyond_Q_period_sec / 3600.0) + GasMeter.VE_Lm3;
       valve_going_beyond_Q_nexTime = timeSystemUnix + valve_going_beyond_Q_period_sec + 8;
       
       rez = _OK;
       }
       return rez;
}

//------------------------------------------------------------------------------
uint8_t PFW_valve_enable_auto_control(char* parString)
{
  if(parString ==0)   
  {
    if(while_TransmitEnable() == _FALSE) return 1;

    sprintf((char*)TransmitBuffer,"VALVE_AUTO_CONTROL=%d\r\n", valve_enable_auto_control);   // +
     Opto_Uart_Transmit(); 
     return OPTIC_OK; 
  }
  else
  {
     if(change_valve_enable_auto_control(parString, CHANGE_OPTIC) == _OK)
       return OPTIC_OK; 
     else
       return OPTIC_INCORRECT_VALUE;
  }  
}

uint8_t change_valve_enable_auto_control(char* ptr_str, uint8_t source)
{
  uint8_t temp;
  uint8_t rez;
  if(CheckChrString((uint8_t*)ptr_str, "%d", strlen(ptr_str)) == _FALSE) return OPTIC_INCORRECT_VALUE;
       temp = atoi(ptr_str);
       if((temp != 1 && temp != 0) || (temp == 0 && source == CHANGE_OPTIC))
          rez = _FALSE;
       else
       {
       SetDateTimeToArchiveChanges();
       sprintf((char*)ChArc.OldValue,"%d", valve_enable_auto_control); // +
       Eeprom_WriteChar((uint32_t)&valve_enable_auto_control, temp);
       sprintf((char*)ChArc.NewValue,"%d", valve_enable_auto_control); // +
       WriteChangeArcRecord(CHANGE_VALVE_AUTO_CONTROL, source);
       rez = _OK;
       }
       return rez;
}
//------------------------------------------------------------------------------
uint8_t PFW_Valve_set(char* parString)
{
  if(parString ==0)   
  {
     return OPTIC_WRITE_ONLY; 
  }
  else
  { 
     if(CLB_lock == 0x55)
     {
  if(CheckChrString((uint8_t*)parString, "%d", strlen(parString)) == _FALSE) return OPTIC_INCORRECT_VALUE;
        uint16_t temp;
        temp = atol(parString);
        if(temp == 0 || temp == 1)
        {
           SetDateTimeToArchiveChanges();
           sprintf((char*)ChArc.OldValue,"%d", valve_presence); // +
           Eeprom_WriteChar((uint32_t)&valve_presence, temp);
           sprintf((char*)ChArc.NewValue,"%d", valve_presence); // +
           WriteChangeArcRecord(CHANGE_VALVE_PRESENCE, CHANGE_OPTIC);
        }
        else 
           return OPTIC_INCORRECT_VALUE; 
     }
     else
       return OPTIC_ACCESS_DENIED;
     
  }
  return OPTIC_OK; 
}

/**********************************************************************************************************
** Description :                                                                                         **
***********************************************************************************************************
** In  :                                                                                          **
** Out :                                                                                     **
**********************************************************************************************************/
void Valve_Parsing()
{
   StatusDevice.valve = valve_status;
   if(valve_presence ) // this valve?
   {
      if(valve_action == VALVE_ACTION_OPEN_OPTIC)
      {
          if(valve_resolution)
          {
            if(!valve_cnt_flow_non_null)
               Valve_Open(VALVE_OPEN_FORS_NON);
            else
            {
               if((timeSystemUnix - valve_time_delay_open) > 60)
               {
                  Valve_Open(VALVE_OPEN_FORS_NON);
                  if(valve_cnt_flow_non_null > 2)
                    valve_cnt_flow_non_null = 0;
               }
            }
          }
      }
      if(valve_action == VALVE_ACTION_OPEN_TCP)
      {
            if(!valve_cnt_flow_non_null)
               Valve_Open(VALVE_OPEN_FORS_NON);
            else
            {
               if((timeSystemUnix - valve_time_delay_open) > 60)
               {
                  Valve_Open(VALVE_OPEN_FORS_NON);
                  if(valve_cnt_flow_non_null > 2)
                    valve_cnt_flow_non_null = 0;
               }
            }
      }
      if(valve_action == VALVE_ACTION_OPEN_OPTIC_FORCE)
      {
          if(valve_resolution)
          {
             Valve_Open(VALVE_OPEN_FORS_YES);
          }
      }
      if(valve_action == VALVE_ACTION_OPEN_TCP_FORCE)
      {
         Valve_Open(VALVE_OPEN_FORS_YES);
      }
      if(valve_action == VALVE_ACTION_CLOSE_OPTIC || valve_action == VALVE_ACTION_CLOSE_TCP)
      { 
         valve_cnt_flow_non_null = 0;
         if(valve_action == VALVE_ACTION_CLOSE_OPTIC)
            Valve_Close(VALVE_CLOSE_OPTIC);
         else
            Valve_Close(VALVE_CLOSE_TCP);
      }
      
      if(valve_enable_auto_control && valve_status != VALVE_NON_POWER)
      {
         if(valve_reverse_flow_close_flag)
         {
            valve_cnt_flow_non_null = 0;
            valve_reverse_flow_close_flag = 0;
            valve_revers_flow_on_utime = timeSystemUnix;
            if(valve_status == VALVE_OPEN)
               Valve_Close(VALVE_CLOSE_REVERSE_FLOW);
         }
      
         if(valve_going_beyond_Q_flag_begin)
         {
            if((timeSystemUnix > valve_going_beyond_Q_nexTime) || valve_going_beyond_Q_volue < GasMeter.VE_Lm3)
            {
               if(valve_going_beyond_Q_volue < GasMeter.VE_Lm3)
               {
                  valve_cnt_flow_non_null = 0;
                  Valve_Close(VALVE_CLOSE_Q_MAX);
               }
               valve_going_beyond_Q_flag_begin = 0;
            }
         }
         
         if(StatusDevice.non_batary_telemetry)
         {
            if(valve_status == VALVE_OPEN)
               Valve_Close(VALVE_CLOSE_BATARY);
         }
         
         if(StatusDevice.open_body)
         {
            if(valve_status == VALVE_OPEN)
               Valve_Close(VALVE_CLOSE_OPEN_BODY);
         }
         
         if(StatusSystem.SIM_card_non)
         {
            if(valve_status == VALVE_OPEN)
               Valve_Close(VALVE_CLOSE_NON_SIM_CARD);
         }
      }
      
      if(!valve_cnt_flow_non_null)
         valve_action = VALVE_ACTION_NON;
   }
}
/**********************************************************************************************************
** Description :                                                                                         **
***********************************************************************************************************
** In  :                                                                                          **
** Out :                                                                                     **
**********************************************************************************************************/
void Set_valve_status(uint8_t temp_status)
{
   Eeprom_WriteChar((uint32_t)&valve_status , temp_status);
}
