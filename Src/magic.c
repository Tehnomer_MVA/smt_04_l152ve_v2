#include "stm32l1xx_hal.h"
#include "cmsis_os.h"
#include "GlobalHandler.h"
#include "magic.h"
#include "Uart_152.h"

#include <time.h>
#include <ctype.h>

uint32_t MagicStatus=0;
char MagicTmpBuf[30];
SCom Com;
SUpdateFile	UpdateFile;

extern uint8_t OptoPort;
extern uint8_t TransmitEnable;
extern uint8_t flag_end_task;
extern SemaphoreHandle_t xCalculateCRC32Semaphore;
extern osThreadId ReadWriteARCTaskHandle;

int AddCrcStr(char *Out, char *In);
void InitOutputOptoLed(void);
//----------------------------------------------------------------------------
__ramfunc void read_bytes(char *out,int count)
{
int i;
for(i=0;i<count;i++)
	{
	while((SPI1->SR & SPI_SR_TXE)==0); // ждем готовности буфера TX
	SPI1->DR=0xff;
	while((SPI1->SR & SPI_SR_RXNE)==0); // wait for RX buffer (full)
	out[i]=SPI1->DR;
	}
}
//----------------------------------------------------------------------------
__ramfunc void UpdateFirmware(SUpdateFile *File)
{
uint32_t i=0,j;
uint32_t* AddrProg = (uint32_t*)START_UPDATE_FW;
uint32_t* Data;
uint32_t AddrClear = START_UPDATE_FW;
uint32_t AddrData = File->FlashAddress;//(uint32_t*)Address;
uint32_t WriteSize=0;
uint32_t CountSectors=File->Size/256;
uint32_t SizeByte=0;
unsigned char adr_hi_hl_lo[4];

while((File->Size%4)!=0)
	File->Size++;
if((File->Size%256)!=0)
	CountSectors++;
FLASH->SR = 0xFFFFFFFF;
// Unlocking the Data memory and FLASH_PECR register access
if((FLASH->PECR & FLASH_PECR_PELOCK) != RESET)
	{  
	FLASH->PEKEYR = FLASH_PEKEY1;
	FLASH->PEKEYR = FLASH_PEKEY2;
	}
// Unlocking the Program memory register access  
if((FLASH->PECR & FLASH_PECR_PRGLOCK) != RESET)
	{
	FLASH->PRGKEYR = FLASH_PRGKEY1;
	FLASH->PRGKEYR = FLASH_PRGKEY2;
	}
// сброс всех WDT
System_WatchdogRefresh();
// включим светодиод оптики
SET_BIT(GPIOD->BSRR,GPIO_BSRR_BR_5);
// очищаем все перепрограммируемое пространство
for(i=0;i<CountSectors;i++,AddrClear+=256)
	{
	// Set the ERASE bit 
	FLASH->PECR |= (FLASH_PECR_ERASE | FLASH_PECR_PROG);
	// Write 00000000h to the first word of the program page to erase 
	*(__IO uint32_t*)AddrClear = (uint32_t)0x00000000;
	while ((FLASH->SR & FLASH_SR_BSY) != 0);
	if((FLASH->SR & FLASH_SR_EOP) != 0)
		FLASH->SR = FLASH_SR_EOP; 
	// Set the ERASE bit 
	FLASH->PECR &= ~(FLASH_PECR_ERASE | FLASH_PECR_PROG); 
	}
// сброс всех WDT
System_WatchdogRefresh();
// устанавливаем линию CS в активное состояние (LOW)
SET_BIT(GPIOE->BSRR,GPIO_BSRR_BR_12);
for(uint16_t i=0;i<0x1FF;i++);

for(i=0;i<100000;i++)
	__no_operation();
adr_hi_hl_lo[0] = CMD_READ;
adr_hi_hl_lo[1] = (AddrData >> 16) & 0xFF;
adr_hi_hl_lo[2] = (AddrData >> 8) & 0xFF;
adr_hi_hl_lo[3] = AddrData & 0xFF;
// передаем адрес flash по SPI
for(i=0;i<4;i++)
	{
	while((SPI1->SR & SPI_SR_TXE)==0); // ждем готовности буфера TX
	SPI1->DR=adr_hi_hl_lo[i]; 			// пишем адрес
	}
while((SPI1->SR & SPI_SR_TXE)==0); // ждем готовности буфера TX
while((SPI1->SR & SPI_SR_BSY)==SPI_SR_BSY); 
i = SPI1->DR;                   
i = SPI1->SR;
for(i=0;i<1000000;i++)
	__no_operation();
// копируем данные
while(WriteSize < File->Size)
	{
	// сброс всех WDT
	System_WatchdogRefresh();
	// включим светодиод оптики
	SET_BIT(GPIOD->BSRR,GPIO_BSRR_BR_5);
	// расчет кол-ва считываемых данных
	if((WriteSize+1024)>File->Size)
		SizeByte = File->Size - WriteSize;
	else
		SizeByte=1024;
	// считываем 4 страницы данных
	read_bytes(Com.TxBuf,SizeByte);
	Data=(uint32_t *)Com.TxBuf;
	for(j=0;j<SizeByte;j+=4,AddrProg++,Data++)
		{
		FLASH->PECR |= FLASH_PECR_FPRG;           
		*((__IO uint32_t*)AddrProg) = *Data;        
		while((FLASH->SR & FLASH_SR_BSY) == FLASH_SR_BSY);
		FLASH->PECR &= ~(uint32_t)FLASH_PECR_PROG;
		} 
	AddrData+=SizeByte;
	WriteSize+=SizeByte;
	// отключим светодиод оптики
	SET_BIT(GPIOD->BSRR,GPIO_BSRR_BS_5);
	}
FLASH->PECR |= FLASH_PECR_PRGLOCK;  
while((FLASH->SR & FLASH_SR_BSY) == FLASH_SR_BSY); 
__DSB(); //Ensure all outstanding memory accesses included buffered write are completed before reset
SCB->AIRCR = ((0x5FA << SCB_AIRCR_VECTKEY_Pos) | SCB_AIRCR_SYSRESETREQ_Msk);
__DSB(); // Ensure completion of memory access
while(1);
}
//-----------------------------------------------------------------------------
void SendTxData(void)
{
if(Com.TxCountByte > 0)
	{
	CLEAR_BIT(DMA1_Channel7->CCR,DMA_CCR_EN);
	SET_BIT(DMA1->IFCR,DMA_IFCR_CTCIF7);  // чистим флаг удачного обмена
	DMA1_Channel7->CNDTR = Com.TxCountByte; // Число символов в регистр ДМА
	// Запуск передачи
	SET_BIT(DMA1_Channel7->CCR ,DMA_CCR_EN);
	TransmitEnable =  TRANSMIT_DISABLE;
	while(TransmitEnable != TRANSMIT_ENABLE)
        osDelay(10);
	}  
} 
//----------------------------------------------------------------------------
void InitStructCom(void)
{
LL_DMA_SetMemoryAddress(DMA1, LL_DMA_CHANNEL_7,(uint32_t)Com.TxBuf);
Com.RxMaxByte=sizeof(Com.RxBuf);
Com.TxMaxByte=sizeof(Com.TxBuf);
Com.RxCountByte=0;
Com.TxCountByte=0;
Com.SendData=SendTxData;
UpdateFile.FlashAddress=MAGIC_ADDRES_FLASH_START;
}
//----------------------------------------------------------------------------
void MagicIRQ(char Byte)
{
Com.RxBuf[Com.RxCountByte]=Byte;
Com.RxCountByte++;
if(Com.RxCountByte>Com.RxMaxByte)
	Com.RxCountByte=0;	
}
//----------------------------------------------------------------------------
void magic(uint32_t Size, uint32_t CrcFile)
{
int repNack=0;
portBASE_TYPE xStatus;
uint32_t Time,ReadTime,Addr,DownloadSize,CountRead;
uint32_t Crc, *CrcPack;
uint32_t StartReadAddress;
ReadTime = xTaskGetTickCount(); 
// ждем завершение работы таска "ReadWriteARCTaskHandle"
while((flag_end_task&0x08)==0x08)
{
  if(xTaskGetTickCount() - ReadTime > 25000 || OptoPort == OPTO_PORT_DISABLE)
    return;
}
// останавливаем работу таска "ReadWriteARCTaskHandle" 
// для монопольного использования внешней Flash-памяти
vTaskSuspend(ReadWriteARCTaskHandle);
// переключаем работу прерывания по UART на функцию "MagicIRQ"
MagicStatus=1;
UpdateFile.Size=Size;
// инициализация Сом структуры
InitStructCom();
// отсылаем сообщение ПК с подтверждение загрузки и размером загружаемых блоков
sprintf(MagicTmpBuf,"update:%lu",DOWN_LOAD_DATA_SIZE);
Com.TxCountByte=AddCrcStr(Com.TxBuf,MagicTmpBuf);
Com.SendData();
Time=xTaskGetTickCount();
Addr=UpdateFile.FlashAddress;
DownloadSize=0;
repNack=0;
CountRead=0;
while(1)
	{
	// крутимся пока не сняли оптику
	if(OptoPort == OPTO_PORT_DISABLE)
		break;
	// пошли данные
	if(Com.RxCountByte > CountRead)
		{
		CountRead=Com.RxCountByte;
		// засекаем время
		ReadTime=xTaskGetTickCount();
		}
	// отлавливаем "CAN" (команда 0x09 CRC32 0x0D 0x0A)
	if(Com.RxCountByte==14 && strstr(Com.RxBuf,"CAN\t"))
		{
		osDelay(150);
		// повторно проверяем размер принятых данных
		if(Com.RxCountByte==14)
			{
			Com.TxCountByte=AddCrcStr(Com.TxBuf,"CAN OK");
			Com.SendData();
			DownloadSize=0;
			break;
			}
		else
			continue;
		}
	else if(Com.RxCountByte==DOWN_LOAD_DATA_SIZE || Size==(DownloadSize + (Com.RxCountByte - 4)))
		{
		// подсчет контрольной суммы полученного пакета
		xStatus = xSemaphoreTake(xCalculateCRC32Semaphore, 5000);
		if(xStatus != pdPASS)
			{
			// Ошибка 
			xSemaphoreGive(xCalculateCRC32Semaphore);
			break;
			}
		Crc = CalculateCRC32((uint8_t*)Com.RxBuf, Com.RxCountByte - 4, CRC_START_STOP, 0);
		xSemaphoreGive(xCalculateCRC32Semaphore);
		// вычисляем указатель на crc пакета данных
		CrcPack=(uint32_t *)(Com.RxBuf + (Com.RxCountByte - 4));
		// сравниваем
		if(Crc == *CrcPack)
			{
			if(!IS_FLASH_ON())
     			SPI_InitHardware();
			// записываем данные на флеш
			SPI_Write(Addr,(uint8_t *)Com.RxBuf,Com.RxCountByte-4);
			DownloadSize+=Com.RxCountByte-4;
			Addr+=Com.RxCountByte-4;
			if(Size==DownloadSize) // приняли все данные
				{
				Com.TxCountByte=AddCrcStr(Com.TxBuf,"ACK update");
				Com.SendData();
				break;
				}
			else
				Com.TxCountByte=AddCrcStr(Com.TxBuf,"ACK");
			}
		else
			Com.TxCountByte=AddCrcStr(Com.TxBuf,"NACK");
		// очищаем буфер
		memset(Com.RxBuf,0,Com.RxMaxByte);
		Com.RxCountByte=0;
		CountRead=0;
		Time=xTaskGetTickCount();
		Com.SendData();
		// оттягиваем время отключения оптики
		tick_count_message_from_OPTIC_sec = SystemUpTimeSecond;
		}
	// прервалась передача пакета (длит. тишины более 3 сек.) или timeout (нет ответа от компьютера 20 сек.) 
	else if(((CountRead>0) && (xTaskGetTickCount()-ReadTime)>=3000) || ((xTaskGetTickCount()-Time)>=WAIT_TIMEOUT))
		{
		memset(Com.RxBuf,0,Com.RxMaxByte);
		Com.RxCountByte=0;
		CountRead=0;
		if(repNack==0)
			{
			Com.TxCountByte=AddCrcStr(Com.TxBuf,"NACK");
			Com.SendData();
			repNack++;
			Time=xTaskGetTickCount();
			continue;
			}
		else
			{
			Com.TxCountByte=AddCrcStr(Com.TxBuf,"CAN OK");
			Com.SendData();
			break;
			}
		}
	}
if(Size==DownloadSize) // приняли все данные
	{
	// оттягиваем время отключения оптики
	tick_count_message_from_OPTIC_sec = SystemUpTimeSecond;
	// проверка CRC загруженной прошивки
	StartReadAddress=UpdateFile.FlashAddress;
	// расчет кол-ва блоков
	Com.TxCountByte=Size/256;
	Crc = 0xFFFFFFFF;
	for(int i=0;i<Com.TxCountByte;i++)
		{
		if(!IS_FLASH_ON())
     		SPI_InitHardware();
		// очищаем буфер
		memset(Com.TxBuf,0,256);
		SPI_Read_Data(StartReadAddress,256,(uint8_t *)Com.TxBuf);
		xStatus = xSemaphoreTake(xCalculateCRC32Semaphore, 5000);
		if(xStatus != pdPASS) // Ошибка
			xSemaphoreGive(xCalculateCRC32Semaphore);
		else
			Crc = CalculateCRC32((uint8_t*)Com.TxBuf, 256, CRC_IN_PROCESSING, Crc);
		xSemaphoreGive(xCalculateCRC32Semaphore);
		StartReadAddress+=256;
		}
	// досчитываем CRC остатка данных
	Com.TxCountByte=(256 * Com.TxCountByte);
	if(Com.TxCountByte < Size)
		{
		// очищаем буфер
		memset(Com.TxBuf,0,256);
		Com.TxCountByte = Size - Com.TxCountByte;
		// врубаем питание внешней Flash
		if(!IS_FLASH_ON())
     		SPI_InitHardware();
		SPI_Read_Data(StartReadAddress,Com.TxCountByte,(uint8_t *)Com.TxBuf);
		xStatus = xSemaphoreTake(xCalculateCRC32Semaphore, 5000);
		if(xStatus != pdPASS) // Ошибка
			xSemaphoreGive(xCalculateCRC32Semaphore);
		else
			Crc = CalculateCRC32((uint8_t*)Com.TxBuf, Com.TxCountByte, CRC_IN_PROCESSING, Crc);
		xSemaphoreGive(xCalculateCRC32Semaphore);
		}
	Crc ^= 0xFFFFFFFF;
	// проверка подсчитанной CRC с переданной
	if(Crc == CrcFile)
		{
		// отрубаем оптику
		Opto_Uart_deInit();
		osDelay(500);
		// порт оптики уст. на выход
		InitOutputOptoLed();
		//FLASH_Reset();
		//SPI_InitHardware();
		LL_SPI_DisableIT_RXNE(SPI1);
		// останавливаем все задачи
		vTaskSuspendAll();
		// запрещаем прерывания
		__disable_irq();
		// обновление ПО (перепрошивка)
		UpdateFirmware(&UpdateFile);
		}
	}
// загрузка/прошивка прибора остановлена или завершена с ошибкой
// восстанавливаем параметры работы прибора
MagicStatus=0;
vTaskResume(ReadWriteARCTaskHandle);
LL_DMA_SetMemoryAddress(DMA1, LL_DMA_CHANNEL_7,(uint32_t)TransmitBuffer);
}
//-----------------------------------------------------------------------------
int AddCrcStr(char *Out, char *In)
{
int size=0;
uint32_t crc;
portBASE_TYPE xStatus;
size=sprintf(Out,"%s\r\n",In);
xStatus = xSemaphoreTake(xCalculateCRC32Semaphore, 5000);
if(xStatus != pdPASS)
	{
	// Ошибка 
	xSemaphoreGive(xCalculateCRC32Semaphore);
	return 0;
	}
crc = CalculateCRC32((uint8_t*)Out, size, CRC_START_STOP, 0);
xSemaphoreGive(xCalculateCRC32Semaphore);
size+=sprintf(Out+size,"\t%08X\r\n",crc);
return size;
}
//-----------------------------------------------------------------------------
void InitOutputOptoLed(void)
{  
LL_GPIO_InitTypeDef lo_port;
// Enable the peripheral clock of GPIOD
if((RCC->AHBENR & RCC_AHBENR_GPIODEN) == 0)
	RCC->AHBENR |= RCC_AHBENR_GPIODEN;  
lo_port.Pin =  LL_GPIO_PIN_5;
lo_port.Mode = LL_GPIO_MODE_OUTPUT;
lo_port.Speed = LL_GPIO_SPEED_FREQ_MEDIUM; 
lo_port.Pull  = LL_GPIO_PULL_NO;
LL_GPIO_Init(GPIOD, &lo_port);
SET_BIT(GPIOD->BSRR,GPIO_BSRR_BR_5);
} 
//-----------------------------------------------------------------------------
