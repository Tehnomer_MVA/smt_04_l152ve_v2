//=============================================================================
/// \file    ValveTask.h
/// \author  MVA
/// \date    30-Jan-2019
/// \brief   Implementing the Vlave-functionality.
//=============================================================================
// COPYRIGHT
//=============================================================================
/*
4.1	����������� ����������� �������� ������ ������������� ����������� 
����������� ������� �� �������� ����������� ��������� ������� � ��������� �������:
�	���������� �������� ������� ���� ����� �������� ������� ��������� 
0,015�3/� � �������� ������� ��������� ������� ���� ����� �������� ������� 
������ ���� �� ����� 1 ���; ��� ����������� ������� ���� ������ ������ ���� 
�	���������� ������������� ����������� ������� ���� ( Q ? 1,2 Qmax); 
����������� �������� �������, ����� �������� ������� ������ ������� ������ 
( 1��� � 1��� ), ��������������� ����������; 
���������� ���������� �������� ��������� ���������� ����� ����, 
��������� ����� ������� �� ������������� ����������� �������� �������. 
*/
uint8_t Valve_Open(uint8_t p_Fors);
void Valve_Close(uint8_t);

#ifndef VALVETASK_H
#define VALVETASK_H

//#define VALVE_UNKNOWN 0
//#define VALVE_OPEN 1
//#define VALVE_CLOSE 2

#define VALVE_CLOSE_TCP                 0
#define VALVE_OPEN                      1
#define VALVE_CLOSE_REVERSE_FLOW        2
#define VALVE_CLOSE_Q_MAX               3
#define VALVE_CLOSE_NON_NULL            4
#define VALVE_NON_POWER                 5
#define VALVE_UNKNOWN                   6
#define VALVE_CLOSE_OPTIC               7
#define VALVE_CLOSE_OPEN                8
#define VALVE_OPEN_CLOSE                9

#define VALVE_CLOSE_BATARY              11
#define VALVE_CLOSE_NON_SIM_CARD        12
#define VALVE_CLOSE_OPEN_BODY           13
#define VALVE_OPEN_FORCE                14
//// valve action to be performed depending on where the command came from
enum
{
   VALVE_ACTION_NON=0,
   VALVE_ACTION_OPEN_TCP,
   VALVE_ACTION_OPEN_OPTIC,
   VALVE_ACTION_CLOSE_TCP,
   VALVE_ACTION_CLOSE_OPTIC,
   VALVE_ACTION_OPEN_TCP_FORCE,
   VALVE_ACTION_OPEN_OPTIC_FORCE,
};

enum
{
   VALVE_OPEN_FORS_NON = 0,
   VALVE_OPEN_FORS_YES
};

extern const uint8_t valve_enable_auto_control @".eeprom";  // 
extern const  uint8_t valve_presence; // 1 - there is
extern const uint8_t valve_status; 
extern const uint8_t valve_resolution; // 0 - close, 1 - open
extern const uint32_t valve_going_beyond_Q_period_sec  @".eeprom";  //

extern uint8_t valve_action;
extern uint8_t valve_cnt_flow_non_null;
extern uint8_t valve_going_beyond_Q_flag_begin;
extern uint8_t valve_reverse_flow_close_flag;

extern uint64_t valve_going_beyond_Q_nexTime;
extern uint64_t valve_time_delay_open;

extern double valve_going_beyond_Q_volue;

uint8_t PFW_valve(char* parString);
uint8_t PFW_valve_force(char* parString);
uint8_t PFW_valve_time(char* parString);
uint8_t PFW_valve_check_open_time(char* parString); //
uint8_t change_valve_open_delay(char* ptr_str, uint8_t source);
uint8_t PFW_Valve_min(char* parString); // 
uint8_t change_valve_min(char* ptr_str, uint8_t source);
uint8_t PFW_Valve_Qmin(char* parString); //
uint8_t change_valve_Qmin(char* ptr_str, uint8_t source);
uint8_t PFW_Valve_set(char* parString);
uint8_t PFW_cnt_valve_min(char* parString); //
uint8_t change_cnt_valve_min(char* ptr_str, uint8_t source);
uint8_t PFW_delay_slightly_open(char* parString); // �������� ����� ������������ ������ ��� ��������
uint8_t change_delay_slightly_open(char* ptr_str, uint8_t source);
uint8_t PFW_valve_going_beyond_Q_period_h(char* parString);
uint8_t change_valve_going_beyond_Q_period_h(char* ptr_str, uint8_t source);
uint8_t PFW_valve_enable_auto_control(char* parString);
uint8_t change_valve_enable_auto_control(char* ptr_str, uint8_t source);


void Valve_Parsing();
void Set_valve_status(uint8_t temp_status);

#endif