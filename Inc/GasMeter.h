//=============================================================================
/// \file    GasMeter.h
/// \author  RFU
/// \date    05-Feb-2016
/// \brief   Gasmeter functionality
//=============================================================================
//=============================================================================

#ifndef GASMETER_H
#define GASMETER_H

#include "Typedef.h"
#include "Error.h"
#include "sgm.h"
#include "SensorTypeParameters.h"
#include "stm32l1xx_ll_rtc.h"


//-----------------------------------------------------------------------------
// Definition of module errors. Add here new module errors. 
// Module errors from 0x00 to 0x1F are allowed.
#define ERROR_GASMETER_UNKNOWN_SENSOR     (ERROR_MODULE_GASMETER | 0x00)
#define ERROR_GASMETER_INVALID_FLOW_UNIT  (ERROR_MODULE_GASMETER | 0x01)
#define ERROR_GASMETER_INIT_GASREC        (ERROR_MODULE_GASMETER | 0x02)


typedef struct
	{
          double QgL;         // ������ long ���� � ������
 //         double QgS;         // ������ short ���� � ������
          double dQf;         // ������� �������������� ������
          double QgL_m3;      // +++ MVA ������ long � ����� � ���
          double QgL_Disp;    // ������ ��� �������  
 //         double QgS_m3;      // +++ MVA ������ short � ����� � ���
          double var_T;       // ����������� � �������  
          double VE_L;        // ����� long ����������� ������ � �����
//          double VE_S;        // ����� short ����������� ������ � �����  
          double VE_Lm3;      // ����� long ����������� ������ � �3
//          double VE_Sm3;      // ����� short ����������� ������ � �3  
          double VE_Pulse;    // ����� �� �������� ���������
          float mid_GasRecond; // ������� �������� ���� ��� ������ �� �������  
          uint8_t Gas_Type_Good;  // �������� ���� � ���������
          float   VDD_power[4];    // ���������� ������� �������
          uint8_t SensEnable[4];
          Error   SensError[4];
          uint8_t SensCnt;
  } TGasMeter;

struct TypeGasMeter
{
   uint32_t _MeasurementIntervalFM;
  float Kalman_KT;
  uint8_t Range_T; // 0 �������� �� -25 �� +55, 1-�� 55 �� 60 � -25 �� -30, 2 - ����� +60 � ���� -30  
  float T_Kalman;
};
extern struct TypeGasMeter SGasMeter;

typedef struct
	{
 	  uint16_t SF[4];      // ���������� ����������� 
          uint16_t Lut[4];     // �����
          uint16_t SF_Mid;
          float   CalkValue;   // � ��������� � ���������� �����
          uint16_t FW_major;   // ������ �� ��������
          uint16_t FW_minor;   // ������ �� ��������
          uint16_t HW_major;   
          uint16_t HW_minor;   // ������ ������ ��������    
          LL_RTC_DateTypeDef DateOfManufact; // ���� ������������ 
          uint8_t  SN_Device[20];
              } TDevice;
enum
{
  KFACTOR_MODE_NORM,
  KFACTOR_MODE_MEASURE,
  KFACTOR_MODE_ERROR
};

typedef struct
         { 
           uint8_t Num_param;      // ����� ���������
           uint8_t Good_Bad;       // ������� ������� -FF, ������ 00
           float K_sect_min;       // ������ ���������
           float K_sect_max;       // ����� ���������
         }  T_GasType ;

typedef enum  { 
                TestModeOFF = 0,
                TestMode_1  = 1, 
                TestMode_2  = 2,
                TestMode_3  = 3,  
                TestMode_4  = 4,
                TestMode_5  = 5,
              } T_TestMode ;



extern const T_GasType GasType[];
extern const TDevice Device_STM;
extern TGasMeter GasMeter;
extern TSensor Sensor;
extern uint8_t Kfactor_measure_mode;

extern double TEST_VE_L;
extern double TEST_VE_Pulse;   //�����!!!!       
extern double TEST_VE_Lm3;

extern uint16_t TestInterval;

//extern uint32_t TEST_QF_summ;
extern uint32_t TEST_QF_summ0;
extern uint32_t TEST_QF_summ1;
extern uint32_t TEST_QF_summ2;

extern uint16_t TEST_QF_Count;
extern float Test_QF_midle;

extern float Int_Flow_Max;
extern double Int_Temp_Midl;
extern uint16_t Int_Temp_Count;
extern float Int_Temp_Max;
extern float Int_Temp_Min;
extern double GasSelfBorder;
extern uint8_t TransmitOpticTest;
extern uint8_t going_beyond_T_gas;
extern uint8_t going_beyond_Q;
extern uint8_t Kfactor_UI_measure_mode;
extern uint16_t kfactor_emulator;
extern uint32_t _LastGasRecognitionTime;



                                
extern T_TestMode TestModeActive;
extern T_TestMode TestModeActive_previous;



//-----------------------------------------------------------------------------
Error Gasmeter_InitModule(void);
//-----------------------------------------------------------------------------
Error Gasmeter_Process(void);
void ActivateTestMode(T_TestMode pTestMode);
//-----------------------------------------------------------------------------
double GetTempMidl_crash();
void Clear_cnt_TempMidl_crash();
uint32_t Get_cnt_TempMidl_crash();
void update_time_FMMeasurements();
uint32_t GetTimeSinceLastMeasurementFM(void);
#endif /* GASMETER_H */
