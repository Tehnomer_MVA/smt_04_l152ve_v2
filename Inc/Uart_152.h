//=============================================================================
// 
//=============================================================================
/// \file    Uart.h
/// \author 
/// \date   
/// \brief   UART communication
//=============================================================================
// COPYRIGHT
//=============================================================================
// 
//=============================================================================
#include "typedef.h"
#include "stm32l1xx.h"

#define DMA1_ENABLE  1
#define DMA1_DISABLE 0

#define TRANSMIT_ENABLE 0xFF
#define TRANSMIT_DISABLE 0x0
#define OPTOPORT_CLOSED 0x0
#define OPTOPORT_OPENED 0xFF

#define RX_BUFFER_MAX_SIZE 400
#define TX_BUFFER_MAX_SIZE 400

extern char TransmitBuffer[];
extern char TransmitBuffer_temp[];
extern char ReceivedBuffer[];


extern uint8_t TransmitEnable; 
extern uint8_t  DataReceived ;
extern uint8_t OptoPortStatus;  

void Opto_Uart_Init(void);
void Opto_Uart_deInit(void);
void Opto_Uart_Transmit(void);
void OPTO_ReadTask(void const * argument);
void EnableDisableDMA1(uint8_t flag);
void Mdm_DMA_RxReset(void);
