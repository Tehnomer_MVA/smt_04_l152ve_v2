#include "stm32l1xx.h"
#include "Time_g.h"
#include "Archiv.h"
#include "commands_func.h"
#include "valve.h"

#include <time.h>

#define BUF_MESSAGE_GSM_MODULE_SIZE 300

#define WAITING_TIME_MESSAGE_FROM_SERVER_SEC            (uint32_t)75
#define WAITING_TIME_MESSAGE_FROM_GSM_MSEC              170000
#define WAITING_TIME_CONNECTING_SERVER_GSM_MSEC         300000
//#define WAITING_TIME_CONNECTING_OPTIC_MSEC              300000


#define MESSAGE_NON_PARTS 0
#define MESSAGE_PARTS 1

#define EVENT_INT_BEGIN         (uint64_t)0x0000000000000000
#define EVENT_INT_END           (uint64_t)0x8000000000000000

#define EVENT_INT_WARNING_CLEAR                 (uint64_t)0x000000000000FFFF
#define EVENT_INT_WARNING_MASK                  (uint64_t)0x000000000000FFFF
#define EVENT_INT_CURRENT_FLOW                  (uint64_t)0x0000000000000000  // +
#define EVENT_INT_COMMISSIONING                 (uint64_t)0x0000000000000001  // + 
#define EVENT_INT_SIM_CARD_NON                  (uint64_t)0x0000000000000002 // +
#define EVENT_INT_ERROR_SESSION_TCP             (uint64_t)0x0000000000000004
#define EVENT_INT_BATARY_TELEMETRY_NON          (uint64_t)0x0000000000000008
#define EVENT_INT_BATTERY_TELEMETRY_MIN_15      (uint64_t)0x0000000000000010
#define EVENT_INT_BATTERY_TELEMETRY_MIN_10      (uint64_t)0x0000000000000020
#define EVENT_INT_BATTERY_COUNTER_MIN_15        (uint64_t)0x0000000000000040
#define EVENT_INT_BATTERY_COUNTER_MIN_10        (uint64_t)0x0000000000000080
#define EVENT_INT_BATTERY_COVER_OPEN            (uint64_t)0x0000000000000100

#define EVENT_INT_CLOSE_VALVE                   (uint64_t)0x0000000000000400

#define EVENT_INT_ALARM_CLEAR                   (uint64_t)0x00000000FFFF0000
#define EVENT_INT_ALARM_MASK                    (uint64_t)0x00000000FFFF0000
#define EVENT_INT_REVERSE_FLOW                  (uint64_t)0x0000000000010000
#define EVENT_INT_MAX_Q                         (uint64_t)0x0000000000020000
#define EVENT_INT_GOING_BEYOND_T_GAS            (uint64_t)0x0000000000040000
#define EVENT_INT_GOING_BEYOND_T_ENVIRON        (uint64_t)0x0000000000080000
#define EVENT_INT_GOING_BEYOND_K_FACTOR         (uint64_t)0x0000000000100000
#define EVENT_INT_UNAUTHORIZED_INTERVENTION     (uint64_t)0x0000000000200000

#define EVENT_INT_CRASH_MASK                    (uint64_t)0x0000FFFF00000000
#define EVENT_INT_ERROR_SGM_MODULE              (uint64_t)0x0000000100000000  // +
#define EVENT_INT_GOING_BEYOND_T_GAS_LONG       (uint64_t)0x0000000200000000
#define EVENT_INT_GOING_BEYOND_T_ENVIRON_LONG   (uint64_t)0x0000000400000000
#define EVENT_INT_OPEN_CASE                     (uint64_t)0x0000000800000000
#define EVENT_INT_OPEN_CALIB_SWITCH             (uint64_t)0x0000001000000000
#define EVENT_INT_ERROR_CS_DQF                  (uint64_t)0x0000002000000000
#define EVENT_INT_ERROR_SER_NUMB_SGM            (uint64_t)0x0000004000000000
#define EVENT_INT_CRASH_CLEAR                   (uint64_t)0x0000FFFF00000000


#define EVENT_INT_ERROR_EL_COMPONENT            0x00000002
#define EVENT_INT_CLEAR_REG_STORY               0x00020000 
#define EVENT_INT_ERROR_CS_PO                   0x40000000

#define SIZE_MdmURL_Server      16
#define SIZE_MdmPort_Server     6
#define SIZE_MdmAPN_Adress      24
#define SIZE_MdmAPN_Login       12
#define SIZE_MdmAPN_Password    12
#define SIZE_MdmNumberBalans    6
#define SIZE_EEPROM_SN_SGM      20


//��� ������� ������ 
//#define  MAX_CNT_COMMUNICATION_GSM  3000

/*
E01 - ����� �����
�02 - � ������
�03 - Q max
E04 - �����������
�05 - ������� 10%
�06 - ������
*/
enum
{
  STR_ALL_END,
  STR_PART_END
};

enum
{
  K_FACTOR_NORM,
  K_FAKTOR_PRELIMINARY,
  K_FAKTOR_ALARM,
};

enum
{
  ARCHIVE_HOURLY = 0,
  ARCHIVE_DAILY = 1
};

enum
{
  MODE_ONCE_AN_HOUR     = 1,
  MODE_ONCE_A_DAY       = 2,  
  MODE_ONCE_A_MONTH     = 4,   
  MODE_ONCE_A_DECADE   = 5  
};
__packed struct _Mode_Transfer // 4�����
{
   uint8_t mode;  // 1 - ������ ���
                  // 2 - ��� � ����
                  // 3 - ��� � ������
                  // 4 - ��� � �����
   uint8_t hour;
   uint8_t minute;
   uint8_t day;   // mode = 3, ���� ������. ����� = 1
                  // mode = 4, ����� ������
};
/*
__packed typedef struct 
{
   uint8_t     addressAPN[24]; // 30
   uint8_t     loginAPN[12]; // 42
   uint8_t     passwordAPN[12]; // 54
   uint8_t     ipserver[64]; // 118, 63 �� ����� + ������� ����, ��� ���������� �����
   uint8_t     portserver[6]; // 124, 5 �� ���� + ������� ����
   struct _Mode_Transfer Mode_Transfer; // 154
   uint32_t    SN_device; // 158
   uint32_t    reserved_int; // 323
   uint8_t     gas_day; // 324 ������� ����. ����� (����) ��� ������������ ��������� ������
   uint8_t     number_balans[6]; // 330
   uint8_t     rez[147]; // 499 �������� ��� ��������� � ��������� �� ˸��)
   uint8_t     CS; // 500 // ���� ������ �����������, ������� ������� ������, �.�. ���������� �������� �������� �������
}_Setting;
*/
enum
{
    MESSAGE_BT_TCP_NON, // +
    /*
    MESSAGE_TCP_KEY,
    MESSAGE_TCP_GET_GROUP_INFO,
    MESSAGE_TCP_ARCHIVE,
    MESSAGE_TCP_ARCHIVE_HOURLY,
    MESSAGE_TCP_ARCHIVE_DAILY,
    MESSAGE_TCP_ARCHIVE_SYSTEM,
    MESSAGE_TCP_ARCHIVE_CHANGES_SETTING,
    MESSAGE_TCP_ARCHIVE_EVENT,    
    MESSAGE_TCP_DATETIME,
    MESSAGE_TCP_DATETIME_SET,
    MESSAGE_TCP_APN_ADDRESS,    
    MESSAGE_TCP_APN_ADDRESS_SET,
    MESSAGE_TCP_APN_LOGIN,
    MESSAGE_TCP_APN_LOGIN_SET,
    MESSAGE_TCP_APN_PASSWORD,
    MESSAGE_TCP_APN_PASSWORD_SET,
    MESSAGE_TCP_VOLUME_PULSE,
    MESSAGE_TCP_VOLUME_PULSE_SET,
    MESSAGE_TCP_SERVER_URL,
    MESSAGE_TCP_SERVER_URL_SET,
    MESSAGE_TCP_SERVER_URL2,
    MESSAGE_TCP_SERVER_URL2_SET,
    MESSAGE_TCP_SERVER_URL3,
    MESSAGE_TCP_SERVER_URL3_SET, 
    MESSAGE_TCP_MODE_TRANSFER,
    MESSAGE_TCP_MODE_TRANSFER_SET,
    MESSAGE_TCP_SMS_PHONE1,
    MESSAGE_TCP_SMS_PHONE1_SET,
    MESSAGE_TCP_SMS_PHONE2,
    MESSAGE_TCP_SMS_PHONE2_SET,
    MESSAGE_TCP_LOCK_STATE,
    MESSAGE_TCP_LOCK_STATE_SET,
    MESSAGE_TCP_COUNT_SESSION,
    MESSAGE_TCP_COUNT_SESSION_SET,
    MESSAGE_TCP_ERROR_SESSION,
    MESSAGE_TCP_ERROR_SESSION_SET,    
    MESSAGE_TCP_MAX_SESSION,
    MESSAGE_TCP_MAX_SESSION_SET,     
    MESSAGE_TCP_COUNT_FAIL_SIM,
    MESSAGE_TCP_COUNT_FAIL_SIM_SET,    
    MESSAGE_TCP_COUNT_FAIL_SPEED,
    MESSAGE_TCP_COUNT_FAIL_SPEED_SET,    
    MESSAGE_TCP_EXIT,
    MESSAGE_TCP_PABS,
    MESSAGE_TCP_PABS_SET,
    */
    MESSAGE_TCP_CLOSED, // +
    /*
    MESSAGE_SN_DEVICE,    
    MESSAGE_SN_DEVICE_SET,
    MESSAGE_UPDATE,
    MESSAGE_GET_FLASH,
    MESSAGE_GET_SETTING,
    MESSAGE_GET_FLASH_UPDATE,
    MESSAGE_GET_ARHIVE_INTERVALE, // UART,BT
    MESSAGE_GET_ARHIVE_DAILY,    // UART,BT 
    MESSAGE_GET_ARHIVE_SYSTEM, // UART,BT
    MESSAGE_GET_ARHIVE_CHANGE, // UART,BT
    MESSAGE_GET_ARHIVE_EVENT, // UART,BT
    MESSAGE_ERASE_FLASH, // UART,BT
    MESSAGE_CLEAR_BATTERY_VOLTAGE,
    MESSAGE_GET_ALL_RF_SENSOR,    // UART,BT
    MESSAGE_START_ADD_RF_SENS,// UART,BT
    MESSAGE_STOP_ADD_RF_SENS,// UART,BT
    MESSAGE_SET_TYPE_RF_SENS,
    MESSAGE_SET_TYPE_ALARM_RF_SENS,    
    MESSAGE_DEL_RF_SENS,
    MESSAGE_DEL_ALL_RF_SENS,    
    MESSAGE_RESERVED_INT,
    MESSAGE_RESERVED_INT_SET,
    MESSAGE_START_DEBUG, // UART,BT
    MESSAGE_STOP_DEBUG,   // UART,BT 
    MESSAGE_SETTINGS_DEFAULT,
    MESSAGE_GAS_DAY,
    MESSAGE_GAS_DAY_SET,
    MESSAGE_GET_GSM_INFO,
    MESSAGE_BALANCE_PHONE,    
    MESSAGE_BALANCE_PHONE_SET,
    MESSAGE_AUTO_SWITCH_MODE,    
    MESSAGE_AUTO_SWITCH_MODE_SET,
    MESSAGE_REPLACE_BATTERY,
    MESSAGE_ERROR,
    MESSAGE_TCP_VALVE,
    MESSAGE_TCP_VALVE_SET,
    MESSAGE_MOTO_INFO,
    MESSAGE_MOTO_INFO_SET,
    MESSAGE_TCP_QMAX_WAR_CNT_MEAS,
    MESSAGE_TCP_QMAX_WAR_CNT_MEAS_SET,
    MESSAGE_TCP_QMAX_MIN_WARNING,
    MESSAGE_TCP_QMAX_MIN_WARNING_SET,
    MESSAGE_TCP_TMAX_WARNING,
    MESSAGE_TCP_TMAX_WARNING_SET,
    MESSAGE_TCP_TMIN_WARNING,
    MESSAGE_TCP_TMIN_WARNING_SET,
    MESSAGE_TCP_T_WARN_CNT,
    MESSAGE_TCP_T_WARN_CNT_SET,
    MESSAGE_TCP_N_GAS_REC_TIME,
    MESSAGE_TCP_N_GAS_REC_TIME_SET,
    MESSAGE_TCP_GAS_RANGE,
    MESSAGE_TCP_GAS_RANGE_SET,
    MESSAGE_TCP_N_GAS_REC,
    MESSAGE_TCP_N_GAS_REC_SET
    */
};

enum
{
  SYSTEM_CHANGE_STATUS = 11
};



enum
{
  TELEM_EVENT_INPUT_MODEM=0,  // 
  TELEM_EVENT_START_GSM_MODEM=1, // +  start on the modem
  TELEM_EVENT_PARAM_GSM_MODEM, // +
  TELEM_EVENT_CHECK_SIM_CARD,  
  TELEM_EVENT_CHECK_REGISTRATION_GSM,
  TELEM_EVENT_CHECK_LEVEL_GSM_SIGNAL,
  TELEM_EVENT_CHECK_REGISTRATION_GPRS,
  TELEM_EVENT_CHECK_REGISTRATION_SERVER,
  TELEM_EVENT_START_DATA_SERVER,
  TELEM_EVENT_DATA_SERVER,  
  TELEM_EVENT_OFF_GSM_MODEM,  //10
  TELEM_EVENT_CHANGE_STATUS,   //11
  TELEM_EVENT_ADD_REGISTRATION_GPRS = 13 ,
  TELEM_EVENT_TRANSMIT_SMS = 14 ,
  TELEM_EVENT_REZERVED_TMR_01,      // 15
  TELEM_EVENT_START_GSM_FOR_BUTTON, // +16
  TELEM_EVENT_START_GSM_FOR_OPTIC,  // +17
  TELEM_EVENT_POWER_ON, // +18
  TELEM_EVENT_POWER_OFF, //19
  TELEM_EVENT_OPTIC_UP,//20
  TELEM_EVENT_OPTIC_DOWN, // 21
  TELEM_EVENT_BEGIN_CSD, // 22
  TELEM_EVENT_END_CSD ,  // 23
  TELEM_EVENT_START_GSM_FOR_TIME,   // 24
  TELEM_EVENT_START_GSM_REPEATED,   // 25
  TELEM_EVENT_START_GSM_REZERVED,   // 26

};

/*
enum
{
   EVENT_RESTART=0, // +
   EVENT_ERROR_CS_DQF, 
   EVENT_K_FACTOR_BEGIN, // + 
   EVENT_K_FACTOR_END, // +
   EVENT_BATTERY_CAPACITY_MIN_BEGIN,
   EVENT_BATTERY_CAPACITY_MIN_END,
   EVENT_CALIBRATION_LOCK_BEGIN, // +
   EVENT_CALIBRATION_LOCK_END, // +     
   EVENT_OPTOPORT_BEGIN, // +
   EVENT_OPTOPORT_END, // +    
   EVENT_ERROR_IN_SGM_MODULE_BEGIN, // +
   EVENT_ERROR_IN_SGM_MODULE_END, // +    
   EVENT_GOING_BEYOND_T_BEGIN, // +
   EVENT_GOING_BEYOND_T_END,  // +
   EVENT_GOING_BEYOND_Q_BEGIN, // +
   EVENT_GOING_BEYOND_Q_END,   // +
   EVENT_REVERSE_FLOW_BEGIN, // +
   EVENT_REVERSE_FLOW_END,  // +
   EVENT_CASE_OPENING_BEGIN,
   EVENT_CASE_OPENING_END,  
   EVENT_BATTERY_CASE_OPENING_BEGIN,
   EVENT_BATTERY_CASE_OPENING_END,  
};
*/
enum
{
  EVENT_OK = 1,
  EVENT_GSM_ERROR = -1, // ���������� ������ �� ������
  EVENT_ERROR = -2, // ������� ERROR
  EVENT_OFF_EXTREMAL_GSM_MODEM = -3, // 
  EVENT_WAITING_TIME_MESSAGE_FROM_SERVER_MSEC = -4, //
  EVENT_NON_PIN_SIM = -5,
  EVENT_CPIN_NOT_INSERTED = -6,  //  
  EVENT_NON_BATARY = -7,  // 
  EVENT_ERROR_CONNECT_SERVER = -8,  // 
  EVENT_ERROR_PIN_COD = -9,  // 
  EVENT_ERROR_STATUS_MODEM = -10,
};



typedef struct
{
   const char* commandStr;    
   uint8_t   message_TCP;
} TypePtrParserCmdTCP;

typedef enum
{
   GSM_TCP,
   GSM_BT,
   UART_SETTING   
}TypSourceMessageTMR;


typedef struct 
{
  uint8_t end_part_message:1;
  uint8_t end_message:1;
  uint8_t rez:6;
}_ResultReply;

struct _FlagMessageTMR
{
   uint8_t command; //
   TypSourceMessageTMR source_message; //
   uint8_t buf_message_tcp[BUF_MESSAGE_GSM_MODULE_SIZE];
};

__packed typedef struct 
{
   LL_RTC_DateTypeDef Date;   
   LL_RTC_TimeTypeDef Time;
}TypeDateTimeFind;

__packed typedef struct 
{
   uint16_t password_manufacture_enter:1;
   uint16_t password_provider_enter:1;
   uint16_t optical_port_communication_session: 1;  // -
   uint16_t GSM_Modem_Power_mode: 1;   // -
  
   uint16_t GSM_Modem_Power_optic: 1; // -
   uint16_t GSM_Modem_Power_switch: 1; // -
   uint16_t rs485_port_communication_session: 1; // - 
   uint16_t GSMmodem_non_registration_GSM:1;

   uint16_t availability_crach :1;
   uint16_t going_beyond_T_crash_gas:         1;
   uint16_t going_beyond_T_crash_environ:     1;
   uint16_t SIM_card_non:     1;
   
   uint16_t GSM_modem_SIM_card_non:     1;   
   uint16_t stack_task_GSM_small:     1;
   uint16_t stack_ParsingComandsTask:     1;
   uint16_t stack_DispatcherTask:     1;

   uint16_t stack_GasmeterTask:     1;
   uint16_t stack_OPTO_ReadTask:     1;
   uint16_t stack_ReadWriteARC:     1;
   uint16_t stack_GSM_RX_UARTTask:     1;

   uint16_t stack_IDLE:     1;
   uint16_t sizefree:     1;
   uint16_t mode_conservation:     1;    
   
}TypeStatusDevice;

__packed typedef struct 
{
   uint8_t start_program: 1;  // +
   uint8_t flash_memory_error: 1;  // +
   uint8_t hard_fault:1; //  +
   uint8_t check_sn_sgm_false:1;  
   
   uint8_t check_sn_sgm2_false:1;
   uint8_t check_sn_sgm3_false:1;
   uint8_t check_sn_sgm4_false:1; 
   uint8_t error_in_SGM_module:1;
   
   uint8_t RCC_CSR;

   uint8_t BAT1_off:1;
   uint8_t BAT2_off:1;
   uint8_t BAT3_off:1;
   uint8_t sensor_temper_environs:1;
   
   uint8_t rez2:4;
   
   uint8_t rez3;
}TypeStatusSystem;


//-------------
typedef union 
{
  uint32_t             word;  
  TypeStatusSystem     bit;
}UTypeStatusSystem;

//--------------------------------------------------------------
__packed typedef struct 
{
  uint16_t counter_not_put_into_operation:1;
  uint16_t SIM_card_non:                1;  
  uint16_t error_connect_server:        1;
  uint16_t battery_not_connected:       1;
  
  uint16_t residual_capacity_battery_modem_min15: 1;
  uint16_t residual_capacity_battery_modem_min10: 1;
  uint16_t residual_capacity_battery_main_min15: 1;  
  uint16_t residual_capacity_battery_main_min10: 1;
  
  uint16_t battery_cover_open:          1;
  uint16_t rez:                         1;
  uint16_t close_valve:                 1;
  uint16_t rez2:                        5;  
}TypeStatusWarning;
//-------------
typedef union 
{
  uint16_t              word;  
  TypeStatusWarning     bit;
}UTypeStatusWarning;
//--------------------------------------------------------------
__packed typedef struct 
{
   uint16_t reverse_flow:               1;
   uint16_t going_beyond_Q:             1;
   uint16_t going_beyond_T_gas:         1;
   uint16_t going_beyond_T_environ:     1;

   uint16_t going_beyond_K_factor:      1;
   uint16_t unauthorized_intervention:  1; 
   uint16_t rez:                        10;
}TypeStatusAlarm;
//-------------
typedef union 
{
  uint16_t            word;  
  TypeStatusAlarm     bit;
}UTypeStatusAlarm;
//--------------------------------------------------------------
__packed typedef struct 
{
   uint16_t error_in_SGM_module:        1;
   uint16_t going_beyond_T_gas_long:    1;
   uint16_t going_beyond_T_environ_long:1;
   uint16_t body_cover_open:            1;
   
   uint16_t calibration_lock_open:      1;
   uint16_t check_crc_dQf_false:        1; 
   uint16_t check_sn_sgm_false:         1;
   
   uint16_t rez:                        9;

}TypeStatusCrash;
//-------------
typedef union 
{
  uint16_t            word;  
  TypeStatusCrash     bit;
}UTypeStatusCrash;
//--------------------------------------------------------------

__packed typedef struct
{
   uint8_t typeEvent;
   uint8_t number_phone_server; // 1 ��� 2 ��� 3
   uint8_t type_sms;
}TypeQueueGSMModem;


typedef enum
{
   NON_EVENT=0,
   TRANSMIT_TCP_SERVER,
   //TRANSMIT_TCP_SERVER_ADD,
   TRANSMIT_SMS,
   GSM_CALL_CSD
}typeEventGSMModem;

typedef enum
{
   SMS_NON,
   SMS_NON_GPRS,
   SMS_BREAK_GPRS,
   SMS_NON_SERVER,
   SMS_TO_SERVER
}TypeSMSOutput;

__packed typedef struct
{
   uint8_t must;
   TypeSMSOutput type;
   uint8_t eventRF;
   uint8_t text[1000];
}structSMSOutput;

typedef enum
{
   EVENT_NULL=0,
   EVENT_NON,
   EVENT_DURING,
   EVENT_NON_POWER,
   
}typeStatusEventGSMModem;

#define MOTO_TIME_START_STOP_CPU        BIT0
#define MOTO_TIME_START_GSM             BIT1
#define MOTO_TIME_START_OPTIC           BIT2
#define MOTO_TIME_START_SGM             BIT3
#define MOTO_TIME_START_LCD             BIT4
#define MOTO_TIME_START_VALVE           BIT5
#define MASK_MOTO_TIME_START (MOTO_TIME_START_GSM | MOTO_TIME_START_OPTIC | MOTO_TIME_START_SGM | MOTO_TIME_START_VALVE)


typedef struct 
{
  uint64_t time_StopCPU;  // 
  uint64_t time_WorkCPU;
  uint64_t time_workingGSM; // 
  uint64_t time_workingOPTIC;  //  current optic to stop mode add 30 ���
  uint64_t time_workingSGM; // 
  uint64_t time_workingLCD; // 
  uint64_t time_workingValve; //  
  float    StopCPU_mkA;
  float    workingGSM_ma;
  float    workingOPTIC_ma;  
  float    workingSGM_ma;  
  float    workingLCD_ma; 
  float    workingValve_ma;
  uint16_t capacity_battery_modem_ma;
  uint16_t capacity_battery_main_ma;
}_TypeMotoHoursToMils;

enum
{
  MOTO_HOURCE_START,
  MOTO_HOURCE_STOP
};

__packed typedef struct 
{
  uint64_t time_workingCPU_sum;
  uint32_t time_workingCPU;
  uint32_t time_StopCPU; 
  uint32_t time_workingGSM; 
  uint32_t time_workingOPTIC;   
  uint32_t time_workingSGM; 
  uint32_t time_workingLCD;
  uint8_t  flag_start_time_workingCPU:1;
  uint8_t  flag_start_time_stopCPU:1;
  uint8_t  flag_start_time_workingGSM:1;
  uint8_t  flag_start_time_workingOPTIC:1;
  uint8_t  flag_start_time_workingSGM:1;
  uint8_t  flag_start_time_workingLCD:1;
}_TypeMotoHoursTempToMils;

typedef enum
{
   SOURCE_START_MDM_NON,
   SOURCE_START_MDM_OPTIC,
   SOURCE_START_MDM_BUTTON
}TypeSourceStartModem;

/*
E01 - ����� �����
�02 - � ������
�03 - Q max
E04 - �����������
�05 - ������� 10%
�06 - ������ (Device_STM.Lut)
*/
/*
DateTimeFindBegin
DateTimeFindEnd
*/
typedef struct 
{
   LL_RTC_DateTypeDef date; // 4
   LL_RTC_TimeTypeDef time; // 7
}_TypeDateTimeCommissioning; // 11

typedef struct
{
   uint8_t success_session[3];
   uint8_t success_session_old[3];
   uint8_t availability_server1;
   uint8_t availability_server2;
   uint8_t availability_server3;
   uint8_t lack_of_two_servers;
}TypeParamServer;

extern TypeValveStatus ValveStatusOld;
extern TypeSourceStartModem source_start_modem;
extern TypeParamServer ParamServer;
extern const _TypeDateTimeCommissioning dateTimeCommissioning;
extern const uint16_t KalmanBorderH;
extern const uint16_t KalmanBorderL;
extern const uint16_t Config_GasmeterGasRecognitionIntervalMeasureMode; // 
extern const uint16_t Config_GasmeterGasRecognitionIntervalErrorMode; // 
extern const  double VolumeBeforeReset[];
extern const  double VolumeBeforeReset2[];
extern uint32_t cnt_error_SGM;
extern uint8_t flag_check_crc_dQf;
//extern uint8_t Qmax_warning_temp;
extern uint8_t going_beyond_T_gas;
extern uint8_t going_beyond_Q;
extern uint8_t going_beyond_T_warning;
extern uint8_t source_change_CLB_LOCK;
extern uint8_t number_server_transmit;
//extern uint8_t  cntQueueGSMModemHandle;
//extern uint8_t flag_ban_Receive_485;
//extern uint8_t flag_transmit_485_end;
extern uint8_t cnt_GSM_TCP_Connect_FALSE;
//extern int8_t HiPower_ON;
extern uint64_t   timeUnixGSMNextConnect;
extern uint64_t   timeGSMUnixStart;


extern int8_t flag_event;
extern int8_t flag_event_menu;
extern uint8_t cnt_seans_TCP_mode_2;
extern uint8_t flag_change_server_url;
extern uint8_t flagPowerONGSMOneTime;

extern uint32_t PressKeyTime;
extern uint32_t tick_count_message_from_server_msec;
extern float residual_capacity_battery_modem_percent;
extern float residual_capacity_battery_main_percent;

extern const  uint32_t cnt_fail_sim_card_eeprom; 
extern const  uint32_t cnt_fail_speed_gsm_modem_eeprom; 
extern const uint8_t telephone_number_SMS1[] ;
extern const uint8_t telephone_number_SMS2[];
extern  const int8_t type_device;
extern  const uint8_t hard_fault;
extern const  uint16_t hw_version; 
extern const  double pressure_abs;
extern  double pressure_abs_gas;
extern const char EEPROM_SN_SGM [4][SIZE_EEPROM_SN_SGM];

extern const char MdmURL_Server[3][SIZE_MdmURL_Server];
extern const char MdmPort_Server[3][SIZE_MdmPort_Server];//40200
extern const char MdmAPN_Adress[SIZE_MdmAPN_Adress];
extern const char MdmAPN_Login [SIZE_MdmAPN_Login];
extern const char MdmAPN_Password[SIZE_MdmAPN_Password];
extern const char MdmNumberBalans[SIZE_MdmNumberBalans];
extern const uint32_t reserved_int;//43200 300
extern const uint8_t auto_switching_mode;
//#pragma section = ".eeprom"
extern const struct _Mode_Transfer Mode_Transfer; 
extern const  uint8_t hard_fault; 
extern const uint8_t telephone_number_SMS1[15]; // 14 + null, user server
extern const int8_t type_device;
extern const int32_t max_cnt_communication_gsm;
extern const int8_t CLB_lock_eeprom;
extern const int8_t optic_kontrast;
extern const _TypeMotoHoursToMils MotoHoursToMilsEEPROM;
extern const LL_RTC_DateTypeDef DateVerification;
extern const LL_RTC_DateTypeDef DateNextVerification;
extern const float Q_max_multiplier_up;
extern const uint32_t going_beyond_Q_period_sec ;
extern const uint16_t cnt_going_beyond_Q_off_eeprom;
extern const uint16_t cnt_measurement_temper_on_long_eeprom;
extern const uint8_t cnt_measurement_temper_on_chort_eeprom;  // 
extern const uint8_t cnt_measurement_temper_off_eeprom;  
extern const int8_t Tmax_warning;
extern const int8_t Tmin_warning;
extern const uint16_t cnt_measurement_temper_warning_eeprom;
extern const uint32_t crc_dQf_eeprom;
//extern const uint16_t cnt_reverse_flow_eeprom;
//extern const int16_t Qfl_reverse_flow;
extern const uint8_t serial_number_board[];
extern const uint32_t cnt_error_SGM_eeprom;
extern const uint16_t Kfactor_min;
extern const uint16_t Kfactor_average;
extern const uint16_t Kfactor_max;
extern const uint8_t Kfactor_measure_cnt_eeprom;
extern const uint8_t sensEnable_eeprom;
extern const float percent_QgL_Disp_min;
extern const float percent_QgL_Disp_max;
extern const uint16_t Qfl_limit_QgL_Disp;
extern const uint16_t st_Lut[4];
extern const uint64_t event_int_story_old;

extern uint64_t event_int_story;
extern uint32_t stack_ParsingComandsTask;//
extern uint32_t stack_DispatcherTask;//
extern uint32_t stack_GasmeterTask;//
extern uint32_t stack_OPTO_ReadTask; //
extern uint32_t stack_ReadWriteARC;//
extern uint32_t stack_task_GSM;//
extern uint32_t stack_GSM_RX_UARTTask;//
extern uint32_t stack_IDLE;//
extern uint32_t number_communication_gsm_fail;
extern uint32_t number_communication_gsm_fail_2;
extern uint32_t number_communication_gsm_fail_3;

extern const  uint32_t number_communication_gsm_fail_eeprom ; 
extern const  uint32_t number_communication_gsm_fail_2_eeprom ; 
extern const  uint32_t number_communication_gsm_fail_3_eeprom ; 
extern const  uint32_t current_communication_gsm_number_eeprom; 
//extern uint8_t successful_transfer_data_to_server;
extern const int QposLimit;
extern const int QnegLimit;
extern uint8_t type_device_int;
extern const uint16_t EnableKalmanFilter;
extern UTypeStatusWarning StatusWarning;
extern UTypeStatusAlarm   StatusAlarm;
extern UTypeStatusCrash   StatusCrash;
extern UTypeStatusSystem  StatusSystem;
extern UTypeStatusSystem  StatusSystem_buf;
extern TypeStatusDevice   StatusDevice;
extern const float KalmanK1;
extern const float KalmanK2;
extern const uint16_t KalmanCnt;
extern const  uint8_t LCD_fcr_ps;
extern const  uint8_t LCD_fcr_div;

extern int8_t cnt_pressed_button_calib;
//extern uint16_t cnt_str_transmit;
extern typeStatusEventGSMModem QueueGSMModemHandleStatus;
extern uint64_t   timeSystemUnix;
extern uint32_t tick_time_lcd_display_on_sec;
extern _TypeMotoHoursToMils MotoHoursToMils;
extern _TypeMotoHoursTempToMils MotoHoursTempToMils;
extern struct _FlagMessageTMR FlagMessageTMR;
extern uint8_t AllSGMModuleErr;
extern IntArcTypeDef frameArchiveInt;
extern uint8_t enable_switch_menu; 
extern const uint8_t password_provider[];
extern const uint32_t waiting_time_connecting_optic_sec;
extern const uint16_t delayMeasureExtTemp ;
extern const float ExtTempMax ;
extern const float ExtTempMin ;
extern const uint16_t going_beyond_T_environ_cnt_measurement;
extern const UTypeStatusWarning StatusWarningEnable;
extern const UTypeStatusAlarm StatusAlarmEnable;
extern const UTypeStatusCrash StatusCrashEnable;
extern const uint8_t Kfactor_UI_measure_cnt_eeprom ;
extern const uint16_t Kfactor_UI_min;
extern const uint16_t Kfactor_UI_max;
extern const float Tmin_gas_crash;
extern const float Tmax_gas_crash;
extern const uint32_t cnt_TempMidl_crash_eeprom;
extern const float Tmin_environ_crash;
extern const float Tmax_environ_crash;
extern const uint32_t cnt_TempEnvironMidl_crash_eeprom;
extern const uint32_t cnt_error_SGM_long_eeprom;
extern const uint32_t cnt_error_SGM_short_eeprom;
extern const int8_t SensCntErrorLong;
extern const uint32_t delay_time_BAT_REPLACE;
extern const uint8_t valve_cnt_min_eeprom;
extern const uint16_t valve_time_open_slightly_eeprom;
extern const uint16_t valve_time_open_additionally_eeprom;
extern const TypeValveStatus ValveStatus;
extern const uint16_t valve_delay_before_check_flow_eeprom;
extern const uint16_t valve_time_check_flow_eeprom;
extern const uint16_t valve_time_close_eeprom;
extern const float GasmeterQmin[5];
extern const uint16_t valve_time_close_eeprom;
extern const float valve_check_flow_multiplier_up_eeprom;
extern const float valve_check_flow_multiplier_down_eeprom;
extern const uint8_t enable_MDM_change_valve;
extern const uint8_t valve_presence_eeprom;
extern const uint16_t valve_time_delay_open_flow_non_null_eeprom;
extern const uint8_t valve_enable_auto_control_eeprom;
extern const uint32_t valve_going_beyond_Q_period_sec_eeprom;
extern const uint8_t show_ver_po_eeprom;
extern const uint8_t modem_log_eeprom;
extern const uint32_t revers_flow_on_period_eeprom;
extern const uint32_t revers_flow_off_period_eeprom;
extern const uint8_t valve_resolution_from_TCP_eeprom;
extern const uint32_t valve_revers_flow_period_eeprom;
extern const uint32_t valve_going_beyond_Q_period_sec;
extern const uint8_t mode_conservation;


extern uint64_t  valve_going_beyond_Q_nexTime;
extern uint8_t   valve_going_beyond_Q_flag_begin; // flag
extern double    valve_going_beyond_Q_volue;
extern uint8_t success_session_TCP;
extern uint8_t  battery_cover_open;
extern uint8_t  battery_cover_open_it_flag;
extern uint64_t battery_cover_open_it_time;
extern uint8_t  body_cover_open;
extern uint8_t  body_cover_open_it_flag;
extern uint64_t body_cover_open_it_time;
extern uint32_t system_tick_ms;
extern uint32_t system_tick_ms;
extern uint64_t time_record_Volume;
extern uint64_t battery_telemetry_installation_time;
extern uint64_t battery_counter_installation_time;
extern uint64_t timeUnixMeasureExtTemp;
//extern uint8_t going_beyond_K_factor;
extern uint32_t error_in_SGM_module;
extern const uint64_t datetime_eeprom[];
extern int8_t rez_GSM_ExtremalPowerDown;
extern uint8_t CLB_lock_IT;
extern uint32_t pinPB14_old;
extern uint8_t reverse_flow;
extern uint32_t pinPB2_old;
extern int8_t HiPower_ON;



uint8_t ReadMessageTMR(char *ptrMessageTMR);
uint8_t TransmitMessageTMR(uint8_t *ptr_buf, uint8_t last_line, uint8_t parts);
extern uint8_t lcd_flag_feature;
void ParsingMessageTMR();
void CalculationNextCommunicationSession();
void  CalculationTimeOnceADay();
void  CalculationTimeOnceADecade();
void  CalculationTimeOnceAMonth();
void Response_TCP(uint8_t response);
void TransmitDebugMessageOptic(const uint8_t *ptr_message, int32_t rez);
void WriteTelemetry(uint16_t p_Event_Code, 
                int8_t p_Event_Result, 
                uint16_t p_Sesion_Number, 
                uint8_t type_record);
void sprintfArchive(uint8_t typeArchive, uint8_t flagBegin);
uint8_t ParsingMESSAGE_TCP_LOCK_STATE_SET(uint8_t checkcrc32);

void sprintfEndMessage(uint8_t flag);
uint8_t ParsingMESSAGE_TCP_APN_SET(TCHANG_Type MdmAPN, uint8_t checkcrc32);
uint8_t ParsingMESSAGE_TCP_ERROR_SESSION_SET(uint8_t checkcrc32);
uint8_t ParsingMESSAGE_TCP_COUNT_SESSION_SET(uint8_t checkcrc32);
uint8_t ParsingMESSAGE_TCP_COUNT_FAIL_SPEED_SET(uint8_t checkcrc32);
uint8_t ParsingMESSAGE_TCP_COUNT_FAIL_SIM_SET(uint8_t checkcrc32);
uint8_t ParsingMESSAGE_TCP_MAX_SESSION_SET(uint8_t checkcrc32);
uint8_t ParsingMESSAGE_TCP_VALVE_SET(uint8_t checkcrc32);
void QueueSendGSMModem(typeEventGSMModem _typeEventGSMModem, uint8_t numb_phone, TypeSMSOutput type_sms_);
void ClearCntSession(uint8_t change_source, uint32_t cnt_all, uint32_t cnt_fail, uint32_t cnt_fail_2, uint32_t cnt_fail_3, uint32_t cnt_fail_sim, uint32_t cnt_fail_speed);
void ParsingQueueGSMModemHandle();
void ParsingCommands();
void ParsingWarning();
void ParsingAlarm();
void ParsingCrash();
// ADC
extern uint8_t flag_measure_temperature;
extern float temperature_environs;
extern double    valve_going_beyond_Q_volue;
extern uint64_t  valve_going_beyond_Q_nexTime;

void change_transfermode_to_5(uint8_t flag_change);
uint8_t ChangeURLPortServer(char *ptr_str, uint8_t index_server, uint8_t source);

void MX_LPTIM1_Init(void);
void CRC_Init(void);
void SMS_Output();
void Measure_Temerature();

void CalculateMotoHourceGSM(uint8_t flag);
void CalculateMotoHourceOPTIC(uint8_t flag);
void CalculateMotoHourceSGM(uint8_t flag);
void CalculateMotoHourceWorkCPU(uint8_t flag);
void CalculateMotoHourceWorkCPU_IDLE();
void CalculateMotoHourceStopCPU();
void Calculation_residual_capacity_battery_percent();
void CalculateMotoHourceWorkLCD(uint8_t flag);
void IncrCntSessionServer(uint8_t numb_serv);
void ClosePassword();
void StackFreeRTOSAanalysis();