//=============================================================================
//   
//=============================================================================
/// \file    menu.h
/// \author  MVA
/// \date    12-Dec-2017
/// \brief   Module for Menu Switch
//=============================================================================
// COPYRIGHT
//=============================================================================
// 
//=============================================================================

#ifndef MENU_H
#define MENU_H



// main menu
#define MAIN_MENU_VOLUME  0 //  + 1
#define MAIN_MENU_FLOW    1 // + 2
#define MAIN_MENU_DATE    2 // + 3,
#define MAIN_MENU_VALVE   3 // + 3,


#define TEH_MENU_WARNING  10  // указатель -
#define TEH_MENU_ALARM    11  // указатель -
#define TEH_MENU_CRASH    12
#define MENU_REGISTR_STORY_WAR 13 // -
#define MENU_REGISTR_STORY_ALARM 14 // -
#define TEH_MENU_VOLUME_FLOAT 15 // -
#define MAIN_MENU_GASTYPE 16 // -
#define MAIN_MENU_TEMPER_GAZ  17 // + 2
#define MAIN_MENU_TEMPER_ENVIRONS  18 // + 2
#define MAIN_MENU_TIME    19 // + 1
#define MENU_MODE_TRANSMI   20 // -
#define TEH_MENU_MDMTST   21 // -
#define MENU_RESIDUAL_CAPACITY_BATTERY_MEAS   22 // -
#define MENU_RESIDUAL_CAPACITY_BATTERY_MODEM   23 // -
#define TEH_SERIAL_DEVICE 24 // + 4
#define TEH_MENU_SW       25 // -
#define MENU_CHKSUMM  26 // -
#define MENU_DATE_VERIFIC  27 // -
#define MENU_DATE_VERIFIC_NEXT  28 // -
#define TEH_TEST_LCD      29 // + 4




#define MAX_MAIN_MENU  3
#define MAX_TEH_MENU   TEH_TEST_LCD 

extern uint8_t Global_Menu;
void Switch_Next_Menu(void);
void Switch_Sub_Menu(void);
void LCD8_Menu(uint8_t pMenu, uint8_t pParam);

#endif /* MENU_H */

