//=============================================================================
/// \file    SensorTypeParameters.h
/// \author  
/// \date    29-Oct-2015
/// \brief   Module for sensor specific parameters
//=============================================================================

#ifndef SENSORTYPEPARAMTER_H
#define SENSORTYPEPARAMTER_H

#include "Typedef.h"
#include "main.h"
typedef struct {
                 float   Q_nominal;
                 float   Q_max;
                 float   Q_Err;
                 float   Tmax;
                 float   Tmin;
                 char*   Name;
                    } TSensorTypeParameters;



  // ��������� ������
typedef struct
	{
   int      Qfl[4];     //Long parametr
   int      Qf_summ;     //Summ of Qfl 
   uint16_t Qfl_FA[4];  //Long parametr Fine adjust
   uint16_t KL[4];      //�������� ���� long 
   uint16_t Traw[4];    //�������� ����������� 
   uint16_t Traw_midl;   
  } TSensor;

typedef struct
	{
          uint16_t FlowUnit[4];
          uint8_t  SN_SGM[4][20];
          uint8_t  Article[4][30];
         } TSens_Const;


typedef struct {
                 uint16_t num_param;        //2
                 uint32_t  min_Border_Qf;   //4
                 uint32_t  max_Border_Qf;   //4
                 float offset_dQf;          //4
                 float ctg_dQf;             //4
              } TQf_param;


extern TSensor Sensor;
extern TSens_Const Sensor_Const;
extern const TSensorTypeParameters sensorTypeParameters[];

extern const uint16_t Qf_count;
extern const TQf_param Qf_param[];

#endif /* SENSORTYPEPARAMTER_H */
