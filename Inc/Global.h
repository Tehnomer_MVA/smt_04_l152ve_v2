/* 
   Autor: Tukov Evgeniy
   version 1.0 
*/

#ifndef __GLOBAL_H
#define __GLOBAL_H

#include "time.h"
#include "cmsis_os.h"
#include "ctype.h"

#define BIT_ON 1
#define BIT_OFF 0

#define _OK 1
#define _FALSE 0
#define _NULL2 2


#define BIT0    0x0001
#define BIT1    0x0002
#define BIT2    0x0004
#define BIT3    0x0008
#define BIT4    0x0010
#define BIT5    0x0020
#define BIT6    0x0040
#define BIT7    0x0080
#define BIT8    0x0100
#define BIT9    0x0200
#define BIT10   0x0400

enum
{
  CRC_NON,
  CRC_START,
  CRC_IN_PROCESSING,
  CRC_STOP,
  CRC_START_STOP
};




   



#define MORE_YEAR       6
#define MORE_MONTH      5
#define MORE_DAY        4
#define MORE_HOUR       3
#define MORE_MINUTE     2
#define MORE_SECOND     1
#define EQUALLY         0
#define LESS_SECOND    -1
#define LESS_MINUTE    -2
#define LESS_HOUR      -3 
#define LESS_DAY       -4  
#define LESS_MONTH     -5
#define LESS_YEAR      -6  
/*
enum
{
   MORE=-1,
   EQUALLY,
   LESS
};
*/
union TwoByte
{
   uint16_t word;
   struct 
   {
      uint8_t _0;
      uint8_t _1;
   }Byte;
};

/*__packed*/ typedef  struct 
{
  uint8_t Hours;            /*!< Specifies the RTC Time Hour.
                                 This parameter must be a number between Min_Data = 0 and Max_Data = 12 if the RTC_HourFormat_12 is selected.
                                 This parameter must be a number between Min_Data = 0 and Max_Data = 23 if the RTC_HourFormat_24 is selected */

  uint8_t Minutes;          /*!< Specifies the RTC Time Minutes.
                                 This parameter must be a number between Min_Data = 0 and Max_Data = 59 */

  uint8_t Seconds;          /*!< Specifies the RTC Time Seconds.
                                 This parameter must be a number between Min_Data = 0 and Max_Data = 59 */
}_TimeType;

__packed typedef struct 
{
  uint8_t Month;    /*!< Specifies the RTC Date Month (in BCD format).
                         This parameter can be a value of @ref RTC_Month_Date_Definitions */

  uint8_t Date;     /*!< Specifies the RTC Date.
                         This parameter must be a number between Min_Data = 1 and Max_Data = 31 */

  uint8_t Year;     /*!< Specifies the RTC Date Year.
                         This parameter must be a number between Min_Data = 0 and Max_Data = 99 */
               

}_DateType;

__packed typedef struct 
{
  _DateType Date;
  _TimeType Time;
}_DateTimeType;

// ############################################################
void Clear_memory(uint8_t *ptr, uint16_t n);
void U32ToStr(uint8_t *ptr, uint32_t x);
uint8_t Number_digits_U32(uint32_t x);
uint32_t StrToInt(const int8_t *str, uint8_t n);
uint8_t Find_null_bite(uint8_t x, uint8_t n);
void Change_bite(uint8_t *ptr_x, uint8_t n, uint8_t flag);
uint8_t Find_null_byte(uint8_t *ptr, uint8_t n);
uint16_t StrToHex(uint8_t *OutBuf, uint8_t * InBuf, uint16_t inc);
int8_t compareDateTime(_DateTimeType DateType1, _DateTimeType DateType2);
uint8_t Calck_CSumme (uint8_t *ptr_cs, uint32_t counter);
uint32_t DifferenceTickCount(uint32_t, uint32_t);
void AnsiToUnicod(uint8_t *ptrbuf, uint8_t *ptrstring);
void LongToUnicod(uint8_t *ptrbuf, uint32_t numbber);
uint32_t CalculateCRC32(uint8_t *buf, uint32_t len, uint8_t processing, uint32_t old_crc32);
uint8_t check_crc32_str(uint8_t *ptr_str, uint8_t erase_tab_symbol);
uint32_t CRC_Stop(uint8_t *pBuffer, uint32_t cnt_byte_data, uint32_t i);
void Formatting_string_to_right(char *ptr_str_part1, char *ptr_str_part2, uint8_t max_cnt_char);
uint32_t str_fing_chr(char *ptr_str, char chr, uint32_t str_len);
uint8_t CheckUrlServer(char *Str);
uint8_t is_digit(char *str, int len);
uint8_t is_digit_cnt(char *str, int len);
uint8_t CheckChrString(uint8_t *input, uint8_t *format, uint32_t len);
//struct tm * _gmtime_(time_t *p);
//-------------------------------------------------
uint16_t BinToHexStr(uint8_t *OutBuf, uint8_t * InBuf, uint16_t inc);
extern inline struct tm * _gmtime_(time_t *p);
#endif