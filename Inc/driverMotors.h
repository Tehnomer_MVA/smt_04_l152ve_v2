//=============================================================================
/// \file    driverMotors.h
/// \author  Tukov E.A.
/// \date    17.02.2020
/// \brief   DRV8837
//=============================================================================
//=============================================================================

#ifndef __DRIVERMOTORS_H
#define __DRIVERMOTORS_H

#define DRIVER_MOTOR_DELAY_AFTER_STOP_MS 1 /* Minimum delay = 1 tick */

typedef enum 
{
   DRIVER_MOTOR_FALSE = 0, 
   DRIVER_MOTOR_OK

}TypeDriverMotorRezult;

typedef struct 
{
   GPIO_TypeDef *ptrDriverPint1_Port; // nSleep
   uint32_t driverPint1_Pin;
   GPIO_TypeDef *ptrDriverPint2_Port; // IN1
   uint32_t driverPint2_Pin;
   GPIO_TypeDef *ptrDriverPint3_Port; // IN2
   uint32_t driverPint3_Pin;
}TypeDriverMotorsSettings;

extern TypeDriverMotorsSettings DriverMotorsSettings;


TypeDriverMotorRezult DriverMotorForwardMovement(uint32_t time_delay);
TypeDriverMotorRezult DriverMotorReverseMovement(uint32_t time_delay);
TypeDriverMotorRezult DriverMotorStopMovement();
#endif