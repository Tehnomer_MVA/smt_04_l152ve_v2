//=============================================================================
/// \file    USART_Modem.h
/// \author  MNA
/// \date    23-Apr-2018
/// \brief   Module for SGM modem
//=============================================================================
// COPYRIGHT
//=============================================================================
// Power key
#define GSM_PWRKEY_ON()  SET_BIT(GPIOD->BSRR, GPIO_BSRR_BR_0) //+++ RESET 
#define GSM_PWRKEY_OFF() SET_BIT(GPIOD->BSRR, GPIO_BSRR_BS_0) //+++ SET

#define GSM_DTR_ON()  SET_BIT(GPIOD->BSRR, GPIO_BSRR_BS_7 ) 
#define GSM_DTR_OFF() SET_BIT(GPIOD->BSRR, GPIO_BSRR_BR_7) //

// On-off power 
#define GSM_PWR_ON()   SET_BIT(GPIOD->BSRR,GPIO_BSRR_BS_1) // SET
#define GSM_PWR_OFF()  SET_BIT(GPIOD->BSRR,GPIO_BSRR_BR_1) // RESET



void  Mdm_HardwareInit(void);

void Mdm_Transmit2(uint8_t const *ptr_str);
uint8_t Mdm_Test(uint8_t modemSpeed);
void Mdm_HardwareDeInit(void);
void Mdm_USARTInit(uint32_t BaudRate);
void Waiting_transfer_GSM(uint16_t limit);
//void TransmitDebugMessageOptic(const uint8_t *ptr_message, uint8_t rez);


extern uint8_t GPRS_flag;
//extern TMdm_Flags Mdm_Flags;

extern const uint8_t MdmTransferMode;
extern const uint8_t MdmTransferHour;
extern const uint8_t MdmTransferMinutes;
extern const uint8_t MdmTransferDay;

extern const uint32_t reserved_int;

void Mdm_DMA_RxReset(void);



