//=============================================================================
/// \file    Configuration.h
/// \author 
/// \date   
/// \brief   Configuration for Gasmeter Reference Design.
//=============================================================================
// PO_RELEASE
//=============================================================================

#ifndef CONFIGURATION_H
#define CONFIGURATION_H

//-----------------------------------------------------------------------------
// Firmware Version
//-----------------------------------------------------------------------------
/** \name Firmware Version */
/// \{ 
#define FW_VERSION_MAJOR  1
#define FW_VERSION_MINOR  28
#define FW_GSM_VERSION    36

#ifdef PO_RELEASE 
#define FW_TEST_VERSION   00
#endif

#ifndef PO_RELEASE 
#define FW_TEST_VERSION   12
#endif

#define VER_PROTOCOL 3

#define HW_VERSION_MAJOR  2
#define HW_VERSION_MINOR  9

#define FW_VERSION_DEBUG  0


#endif /* CONFIGURATION_H */

