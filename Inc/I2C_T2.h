//#include "stm32l152xE.h"
//#include <stdbool.h>
//#include "I2c.h"

//#define ERROR_I2C_ACK             0x01
//#define ERROR_I2C_DATA_LINE_LOW   0x02

#define CONFIG_USE_CATEGORY_I2C

/// I2c acknowledge
/*
typedef enum {
  ACK         = 0, ///< Signals acknowledge
  NO_ACK      = 1, ///< Signals no acknowledge
} I2c_Ack;
*/
/// Enum-typedef for manipulations of I2c-headers
/*
typedef enum {
  I2C_WRITE   = 0x00, ///< Write bit in header
  I2C_READ    = 0x01, ///< Read bit in header
  I2C_RW_MASK = 0x01  ///< Bit position read/write bit in header
} I2c_Header;
*/
typedef enum {
  CONFIG_I2C_SPEED_20_KHZ  = 20, ///< I2c-speed  20 kHz (25 kHz 40.1 �� time request)  
  CONFIG_I2C_SPEED_30_KHZ  = 15, ///< I2c-speed  35 kHz (27.2 kHz 35 �� time request)
  CONFIG_I2C_SPEED_40_KHZ  = 10, ///< I2c-speed  40 kHz
  CONFIG_I2C_SPEED_65_KHZ  =  5, ///< I2c-speed  65 kHz
  CONFIG_I2C_SPEED_90_KHZ  =  3, ///< I2c-speed  90 kHz
  CONFIG_I2C_SPEED_110_KHZ =  2, ///< I2c-speed 110 kHz
  CONFIG_I2C_SPEED_140_KHZ =  1, ///< I2c-speed 140 kHz
  CONFIG_I2C_SPEED_180_KHZ =  0  ///< I2c-speed 180 kHz (max. cable length: 30cm)
} Config_I2cTSpeedType;


typedef union 
{
  struct
            {
              uint8_t SetPowerMode       : 1; 
              uint8_t AlertPinMode      : 1;
              uint8_t PolarityOutput     : 1;
              uint8_t ConversionFault    : 2;
              uint8_t ConversionRate     : 2;
              uint8_t OneShortConversion : 1;
             } items;
  uint8_t Raw;
}  TTypeTMP_Config;


void I2cT_InitHardware(void);
void I2cT_DeInitHardware(void);
uint8_t I2c_StartConditionChT(void);
void I2c_StopConditionChT(void);
uint8_t I2c_WriteByteChT(uint8_t data);
uint8_t I2c_ReadByteChT(I2c_Ack ack);
void InitSclLineT(void);
void InitSdaLineT(void);
uint8_t I2c_MakeWriteHeaderT(uint8_t address);
uint8_t I2c_MakeReadHeaderT(uint8_t address);
uint8_t CheckDataLineTHigh(void);
void ReadDataChT(uint8_t data[], uint8_t numberOfBytes, bool finalizeWithNack);