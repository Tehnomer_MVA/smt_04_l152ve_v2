#ifndef __VALVE_H
#define __VALVE_H

#include "stm32l1xx.h"

typedef enum 
{
   VALVE_FALSE = 0, 
   VALVE_OK

}TypeValveRezult;

typedef enum 
{
   VALVE_NON_FORCED_OPEN = 0,
   VALVE_FORCED_OPEN, 


}TypeValveForced;

typedef enum 
{
   VALVE_POWER_OFF = 0, 
   VALVE_POWER_ON

}TypeValvePower;

typedef struct
{
   GPIO_TypeDef *ptrDriverPint1_Port;
   uint32_t driverPint1_Pin;
   GPIO_TypeDef *ptrDriverPint2_Port;
   uint32_t driverPint2_Pin;
   GPIO_TypeDef *ptrDriverPint3_Port;
   uint32_t driverPint3_Pin;
   GPIO_TypeDef *ptrPower_Port;
   uint32_t power_Pin;
}TypeValveSettings;

typedef enum 
{
   VALVE_CLOSE_TCP = 0, 
   VALVE_OPEN,
   VALVE_CLOSE_REVERSE_FLOW,
   VALVE_CLOSE_Q_MAX,
   VALVE_CLOSE_NON_NULL = 4,
   VALVE_NON_POWER,
   VALVE_UNKNOWN,
   VALVE_CLOSE_OPTIC,
   VALVE_CLOSE_OPEN = 8,
   VALVE_OPEN_CLOSE,
   VALVE_CLOSE_RF_SENSOR,
   VALVE_CLOSE_BATARY,
   VALVE_CLOSE_NON_SIM_CARD = 12,
   VALVE_CLOSE_OPEN_BODY,        // !!! recheck in function ValveSetStatus()
   VALVE_OPEN_FORCED,
   VALVE_CLOSE_DISCHARGE_BATARY = 15, 
   VALVE_CLOSE_OPEN_BAT_BODY,  
   VALVE_CLOSE_OPEN_TRY,     

}TypeValveStatus; // !!! ��� ���������� ��������� �-�� ValveSetStatus
     
typedef enum
{
   VALVE_ACTION_NON=0,
   VALVE_ACTION_OPEN_TCP,
   VALVE_ACTION_OPEN_OPTIC,
   VALVE_ACTION_OPEN_FORCED_TCP,
   VALVE_ACTION_OPEN_FORCED_OPTIC,   
   VALVE_ACTION_CLOSE_TCP,
   VALVE_ACTION_CLOSE_OPTIC,
   VALVE_ACTION_CLOSE_REVERSE_FLOW,
   VALVE_ACTION_CLOSE_Q_MAX,
   VALVE_ACTION_CLOSE_RF_SENSOR,
   VALVE_ACTION_CLOSE_BATARY,
   VALVE_ACTION_NON_SIM_CARD,
   VALVE_ACTION_CLOSE_BODY ,
   VALVE_ACTION_CLOSE_BAT_BODY ,   
   
}TypeValveAction;

extern TypeValveSettings ValveSettings;

TypeValveRezult ValveInit();
TypeValveStatus ValveOpen(TypeValveForced flag_forced);
void ValveSetAction(TypeValveAction action);
TypeValveStatus ValveGetStatus();
TypeValveStatus ValveClose(TypeValveStatus flag_close_source);
TypeValveRezult ValveSetStatus(TypeValveStatus status_temp);
TypeValveRezult ValvePower(TypeValvePower flag_valve_power);
void Valve_Parsing();
uint8_t ValveGetCntOpenFlowNonNull();
void ValveDecrementGas_leak_test_time();
int16_t ValveGetGas_leak_test_time();
#endif