// ��������� ��������� ��� ��������� � SIM900
#define STR_CFUN_1         "+CFUN: 1" // 8
#define STR_CPIN_OK        "+CPIN: READY" // 12
#define STR_CALL_READY     "Call Ready" // 10 
#define STR_SMS_READY      "SMS Ready" // 9 
#define STR_CPIN_NON       "+CPIN: NOT INSERTED" // 19
#define STR_OK             "OK" // 2
#define STR_ERROR          "ERROR" // 5
#define STR_SHUT_OK        "SHUT OK" // 7
#define STR_CONNECT_OK     "CONNECT OK" // 10
#define STR_CONNECT_FAIL     "CONNECT FAIL" // 12
#define STR_SEND_OK        "SEND OK" // 7
#define STR_SEND_FAIL      "SEND FAIL" // 9
#define STR_CHARACTER_MORE        ">"
#define STR_NORMAL_POWER_OFF "NORMAL POWER DOWN" // 17
#define STR_DATA_ACCEPT     "DATA ACCEPT:"// 12
#define STR_PLUS_CSQ        "+CSQ: "//6 
#define STR_PLUS_BTPAIRING  "+BTPAIRING:"//11 
#define STR_PLUS_BTCONNECTING  "+BTCONNECTING:"//14 
#define STR_PLUS_BTDISCONN  "+BTDISCONN"//10 
#define STR_PLUS_BTCONNECT  "+BTCONNECT:"//11 
#define STR_PLUS_BTSPPDATA  "+BTSPPDATA:"//11
#define STR_PLUS_COPS       "+COPS:"//6
#define STR_PLUS_CUSD       "+CUSD:"//6
#define STR_PLUS_CGATT      "+CGATT:"//7

enum
{
  MES_GSM_STR_OK,
  MES_GSM_STR_ERROR,
  MES_GSM_STR_SEND_OK,
  MES_GSM_STR_SEND_FAIL,
  MES_GSM_STR_CALL_READY,
  MES_GSM_STR_SMS_READY,
  MES_GSM_STR_PLUS_CREG,
  MES_GSM_STR_PLUS_CSQ,
  MES_GSM_STR_PLUS_COPS,
  MES_GSM_STR_PLUS_CUSD,
  MES_GSM_STR_PLUS_CGATT,
  MES_GSM_STR_CONNECT_OK,
  MES_GSM_STR_CONNECT_FAIL,
  MES_GSM_STR_CHARACTER_MORE,
  MES_GSM_STR_DATA_ACCEPT,
  MES_GSM_STR_NORMAL_POWER_OFF,
  MES_GSM_STR_SHUT_OK,
  MES_GSM_STR_CPIN_NOT_INSERTED,
  MES_GSM_STR_PLUS_PDP_DEACT,
  MES_GSM_STR_PLUS_CIPSEND,
  MES_GSM_STR_CLOSED,
  MES_GSM_STR_PLUS_CMTI,
  MES_GSM_RING,
  MES_GSM_NO_CARRIER,
  MES_GSM_NO_DIALTONE,
  MES_GSM_BUSY,
  MES_GSM_NO_ANSWER,
  MES_GSM_PROCEEDING,
  MES_GSM_REMOTE
  
  
  
};

// ��� ��������������� ��������, ��� ������ ��������� GSM ������
const uint8_t AT_AT[]="AT\r";
// ���������� ������
const uint8_t AT_CPOWD_1[] = "AT+CPOWD=1\r";
// ���������� ����� GSM
const uint8_t AT_CREG_0[] = "AT+CREG=0\r";
// ����������� ��������
const uint8_t  AT_IPR_115200[]="AT+IPR=115200\r";
const uint8_t  AT_IPR_38400[]="AT+IPR=38400\r";
const uint8_t  AT_IPR_57600[]="AT+IPR=57600\r";
const uint8_t  AT_IPR_19200[]="AT+IPR=19200\r";
// ���������� ��������
const uint8_t  AT_W[]="AT&W\r";
// ������ ���
const uint8_t AT_ATE0[]="ATE0\r";
// ��������� ������������ sleep mode 1 c ������� DTR
const uint8_t  AT_CSCLK_1[]="AT+CSCLK=1\r";



// ����������� GSM
const uint8_t AT_CREG[] = "AT+CREG?\r";
// AT+CSQ - ������� GSM �������
const uint8_t AT_CSQ[] = "AT+CSQ\r\n";
// �������� IMEI
const uint8_t AT_CGSN[] = "AT+CGSN\r";
// �������� ��� ����,, � ������� ������������������
const uint8_t AT_COPS[] = "AT+COPS?\r";
// serial number SIM card
const uint8_t AT_CCID[] = "AT+CCID\r";
// ������ �����
const uint8_t AT_CUSD[] = "AT+CUSD=1,\"%s\"\r"; // AT+CUSD=1,"#100#"

//��������� ����� �������
const uint8_t AT_SAPBR_3_1[] = "AT+SAPBR=3,1,\"APN\",\"internet\"\r";
// �������� GPRS ������
const uint8_t AT_SAPBR_1_1[]  = "AT+SAPBR=1,1\r";

// TCP
// Deactivate GPRS PDP Context
const uint8_t AT_CIPSHUT[] = "AT+CIPSHUT\r";
// ���������� 1 IP ����������
const uint8_t AT_CIPMUX_0[] = "AT+CIPMUX=0\r";
// ���������� ������ GPRS
const uint8_t AT_CGATT_1[] = "AT+CGATT=1\r";
// ��������� ������ GPRS
const uint8_t AT_CGATT_0[] = "AT+CGATT=0\r";
// �������� ������� GPRS
const uint8_t AT_CGATT_CHANGE[] = "AT+CGATT?\r";
// Start Task and Set APN, USER NAME, PASSWORD
const uint8_t AT_CSTT[] = "AT+CSTT=\"%s\",\"%s\",\"%s\"\r";
// ���������� ������������ ���������� � GPRS ��� CSD
const uint8_t AT_CIICR[] = "AT+CIICR\r";
// Get Local IP Address
const uint8_t AT_CIFSR[] = "AT+CIFSR\r";
// ��������� TCP ������ �����, ����
const uint8_t AT_CIPSTART[] = "AT+CIPSTART=\"TCP\",\"%s\",\"%s\"\r";
// ��������� ������ �� ������
const uint8_t AT_CIPSEND[] = "AT+CIPSEND\r";
// ������� ���� ����� ��������� �� ������?
const uint8_t AT_CIPSEND_QUESTION[] = "AT+CIPSEND?\r";
// �������� ����� ����������
const uint8_t AT_CIPQSEND_1[] = "AT+CIPQSEND=1\r";
// �������� ����� ��� ����������
const uint8_t AT_CIPQSEND_0[] = "AT+CIPQSEND=0\r";
// ��������� ������ �� ������
const uint8_t SEND_OK[] = "SEND OK\r";

//BT
// ��������� �������� ����������
const uint8_t AT_BTHOST[] = "AT+BTHOST=TEHNOMER TMR-02\r";
// ���������
const uint8_t AT_BTPOWER[] = "AT+BTPOWER=1\r";
// ����������
const uint8_t AT_BTPOWER_0[] = "AT+BTPOWER=0\r";
// �������� ����
const uint8_t AT_BTPAIR_1_1[] = "AT+BTPAIR=1,1\r";
// �������� ����
const uint8_t AT_BTACPT_1[] = "AT+BTACPT=1\r";
// �������� ������
const uint8_t AT_BTSPPSEND[] = "AT+BTSPPSEND=%d\r";

//���
// ������� ������� ��������� ��� � utf-16
const uint8_t  AT_CSCS_UCS2[]="AT+CSCS=\"UCS2\"\r";
// ����� ��������� ����� 0 - ��������
const uint8_t  AT_CMGF_ON[]="AT+CMGF=1\r";
// ����� ����� ���
const uint8_t  AT_CSMP_17_167_0_25[]="AT+CSMP=17,167,0,25\r";

// ������
// ������� ��������� �����
const uint8_t ATD[] = "ATD%s\r";
// ��������� ������
const uint8_t ATH0[] = "ATH0\r";

/*




// ������





#define STR_CUSD           "+CUSD:" // 6

#define STR_CSQ            "+CSQ: " // 6
#define STR_CMTI           "+CMTI:"//6
#define STR_CMGR           "+CMGR:"//6
//#define AT_CMGF_ON         "AT+CMGF=1"//9 ���������  ����� ON
//#define AT_CMGF_OFF        "AT+CMGF=0"//9 ���������  ����� OFF
#define STR_PLUS_CMGS      "+CMGS:"//6
#define AT_CREG            "+CREG:"//6
#define RING               "RING"//4
#define STR_CLIP           "+CLIP:"//6
#define STR_DTMF           "+DTMF:"//6
#define STR_NO_CARRIER     "NO CARRIER"//10
#define STR_CME_ERROR      "+CME ERROR:"//10
#define STR_CCLK           "+CCLK:"//5
#define STR_POWER_OFF_NORMAL           "NORMAL POWER DOWN"//17

#define STR_CHARACTER_QUOTE        "\""
#define STR_DOWNLOAD        "DOWNLOAD"   
#define STR_PLUS_HTTPACTION  "+HTTPACTION:"
#define STR_PLUS_HTTPREAD  "+HTTPREAD:"


// ������� ������
//unchar ATA[4]={'A','T','A',0x0D};
const unchar ATA[]="ATA\r";
// �������� ������
//unchar ATH[5]={'A','T','H','0',0x0D};
const unchar CALL_DOWN[]="ATH0\r";

// ������� ������� ��������� ��� � utf-16
//unchar  AT_CSCS_UCS2[15]={'A','T','+','C','S','C','S','=','"','U','C','S','2','"',0x0D};
const unchar  AT_CSCS_UCS2[]="AT+CSCS=\"UCS2\"\r";

//unchar  AT_IPR_57600[13]={'A','T','+','I','P','R','=','5','7','6','0','0',0x0D};

// ����� ��������� ����� 0 - ��������
unchar  AT_CMGF_OFF[10]={'A','T',0x2B,'C','M','G','F',0x3D,'0',0x0D};
// ����� ��������� ����� 1 - ���������
//unchar  AT_CMGF_ON[10]={'A','T',0x2B,'C','M','G','F',0x3D,'1',0x0D};
const unchar  AT_CMGF_ON[]="AT+CMGF=1\r";
//��������� ��� ��� ������� 1 � ������� ������, �.�. ��� ��������� � ��� ��������� ��� ��� ��������� 
const unchar AT_CMGR_1[]="AT+CMGR=1,0\r";
// ������� ��� ��� ���������
const unchar AT_CMGDA_DEL_ALL[]="AT+CMGDA=\"DEL ALL\"\r";

const unchar  AT_CSMP_17_167_0_25[]="AT+CSMP=17,167,0,25\r";
//�������� ������������� ������
const unchar AT_CLIP_ON[]="AT+CLIP=1\r";
// �������� DTMF
unchar AT_DDET_ON[]="AT+DDET=1\r";

// �������� � ������ �����
unchar clocks[31]={'A','T','+','C','C','L','K','=','"','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','"',0x0D};
// ������� � ������ �������� ������ ������ 
unchar tone_error_GSM[15]={'A','T',0x2B,'V','T','S','=','"','5',',','5',',','5','"',0x0D};
// ������ ���������� �������
unchar tone_success_GSM[]="AT+VTS=\"1,9\"\r";
// ������ ������������ ���������� �������
const unchar tone_fail_GSM[]="AT+VTS=\"9,1\"\r";
//������ ������� , �� �� ����, �.�. ����� ��������
const unchar cnt_Balans_request = 33;
                                            // 0   1   2   3   4   5   6   7   8   9   10   11  12  13  14  15  16  17  18  19  20 21  22   23  24  25  26  27  28  29  30  31   32
unchar  request_Balans[cnt_Balans_request] = {'A','T','+','C','U','S','D','=','1',',','"','0','0','2','A','0','0','3','1','0','0','3','0','0','0','3','0','0','0','2','3',0x22, 0x0D};//UCS2


// HTTP
const unchar AT_HTTPINIT[] = "AT+HTTPINIT\r";
const unchar AT_HTTPPARA_CID[] = "AT+HTTPPARA=\"CID\",1\r";
const unchar AT_HTTPPARA_URL_regMe[] = "AT+HTTPPARA=\"URL\",\"unicode-nn.ru/API/send/regMe.php\"\r";
const unchar AT_HTTPPARA_URL_importData[] = "AT+HTTPPARA=\"URL\",\"unicode-nn.ru/API/send/importData.php\"\r";
const unchar AT_HTTPPARA_URL_Time[] = "AT+HTTPPARA=\"URL\",\"unicode-nn.ru/API/get/time.php\"\r";
const unchar AT_HTTPPARA_URL_Relay[] = "AT+HTTPPARA=\"URL\",\"unicode-nn.ru/API/get/relayStatus.php\"\r";
 unchar AT_HTTPDATA[25] = {0};
const unchar AT_HTTPACTION[] = "AT+HTTPACTION=1\r";
const unchar AT_HTTPREAD[] = "AT+HTTPREAD\r";
const unchar AT_HTTPTERM[] = "AT+HTTPTERM\r";
*/