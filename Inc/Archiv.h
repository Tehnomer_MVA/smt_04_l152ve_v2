//=============================================================================
/// \file    Archiv.h
/// \author  MVA
/// \date    13-Mar-2018
/// \brief   Functions for archive
//=============================================================================
// COPYRIGHT
//=============================================================================

#include "stm32l1xx_ll_rtc.h"
#include "commands_func.h"

#ifndef ARC_H
#define ARC_H

/*
#define IntArcMemBorder_Start     (uint32_t) 0
#define IntArcMemBorder_End       (uint32_t)(IntArcMemBorder_Start  + 0x20000)  // 2048 frame // 0 - 0x20000
#define DayArcMemBorder_Start     (uint32_t)IntArcMemBorder_End 
#define DayArcMemBorder_End       (uint32_t)(DayArcMemBorder_Start  + 0x10000) // 1024 frame // 0x20000 - 0x30000
#define EventArcMemBorder_Start   (uint32_t)DayArcMemBorder_End 
#define EventArcMemBorder_End     (uint32_t)(EventArcMemBorder_Start  + 0x40000) // 4096 frame // 0x30000 - 0x70000
#define ChangeArcMemBorder_Start  (uint32_t)EventArcMemBorder_End
#define ChangeArcMemBorder_End    (uint32_t)(ChangeArcMemBorder_Start + 0x00100000) // 4096 frame // 0x70000 - 0x00170000
#define SystemArcMemBorder_Start  (uint32_t)ChangeArcMemBorder_End
#define SystemArcMemBorder_End    (uint32_t)(SystemArcMemBorder_Start + 0x00010000) // 1024 frame // 0x00170000 - 0x00180000
#define TelemetryArcMemBorder_Start  (uint32_t)SystemArcMemBorder_End
#define TelemetryArcMemBorder_End    (uint32_t)(TelemetryArcMemBorder_Start + 0x00010000) // 1024 frame // 0x00180000 - 0x00190000
*/
// MAX 0x007FFFFF

/*
int 10000
day 2500
chan 1100
tele 1100
sys 1100
*/

/* ������ ���������� ������� � ExtFlash */
#define IntArcMemBorder_Start     (uint32_t) 0
#define IntArcMemBorder_End       (uint32_t)(IntArcMemBorder_Start  + 0x09D000)  // 10048 frame * 64 = 643 072d = 0x09D000 (643 072 / 4096 = 157) 0 - 0x09D000
#define DayArcMemBorder_Start     (uint32_t)IntArcMemBorder_End 
#define DayArcMemBorder_End       (uint32_t)(DayArcMemBorder_Start  + 0x028000) // 2560 frame * 64 = 163 840 = 0x028000 *** 0x09D000 - 0x0C5000
#define ChangeArcMemBorder_Start  (uint32_t)DayArcMemBorder_End
#define ChangeArcMemBorder_End    (uint32_t)(ChangeArcMemBorder_Start + 0x045000) // 1104 frame * 256 = 282 624 = 0x045000 *** 0x0C5000 - 0x10A000
#define SystemArcMemBorder_Start  (uint32_t)ChangeArcMemBorder_End
#define SystemArcMemBorder_End    (uint32_t)(SystemArcMemBorder_Start + 0x00012000) // 1152 frame * 64 = 73 728 = 0x012000 *** 0x10A000 - 0x11C000
#define TelemetryArcMemBorder_Start  (uint32_t)SystemArcMemBorder_End
#define TelemetryArcMemBorder_End    (uint32_t)(TelemetryArcMemBorder_Start + 0x000B4000) // 11520 frame * 64 = 737 280 = 0x0B4000 *** 0x11C000 - 0x1D0000

#define EventArcMemBorder_Start   (uint32_t)TelemetryArcMemBorder_End // NON
#define EventArcMemBorder_End     (uint32_t)(EventArcMemBorder_Start  + 0x40000) // NON 4096 frame // 0x30000 - 0x70000


#define MAGIC_ADDRES_FLASH_START   (uint32_t)0x00780000 // 0x80000 - 524288 ����
// MAX 0x00 7F FF FF


__packed typedef struct
 { 
   uint8_t  Day;             //1 /* ���� ������ */
   uint8_t  Mon;             //2
   uint16_t Year;            //4
   uint8_t  Hour;            //5 /* ����� ������ */
   uint8_t  Minute;          //6
   uint8_t  Second;          //7
 }TypeTime;


typedef enum 
{
    TYPE_ARCHIVE_HOURLY_TIME = 1,
    TYPE_ARCHIVE_DAILY_TIME = 2,
    TYPE_ARCHIVE_CHANG_NUMBER = 3,
    TYPE_ARCHIVE_TELEMETRY_NUMBER = 4, 
    //TYPE_ARCHIVE_EVENT_NUMBER = 5,    
    TYPE_ARCHIVE_DAILY_NUMBER = 6,    
    TYPE_ARCHIVE_HOURLY_NUMBER = 8,
    TYPE_ARCHIVE_CHANG_TIME = 9,
    //TYPE_ARCHIVE_EVENT_TIME = 10,
    TYPE_ARCHIVE_TELEMETRY_TIME = 11,   
    TYPE_ARCHIVE_SYSTEM_NUMBER = 12,
    TYPE_ARCHIVE_SYSTEM_TIME = 13
}TArchiv_type;

typedef enum {
              ARC_WRITE = 0,
              ARC_READ  = 1
}TARC_RW;


 

__packed typedef struct
 { 
   uint32_t ArcRecNo;           // /* ����� ������ */
   uint64_t TimeBegine;         //
   uint64_t TimeEnd;            //   
   uint16_t Event_Code;         //    /* ��� ������� */
   int8_t Event_Result;         //    /* ��� ������ ������� */
   uint8_t avail_server1;          //
   uint8_t avail_server2;          // 
   uint8_t avail_server3;          //
   uint16_t Sesion_Number;      //    /* ����� ������ �����, 0 - �����. ������� �������� */ 
   uint32_t Status;             //
   uint32_t ArcCRC;             //36
 } EventSystemArcTypeDef;//
#define EventSystemArcTypeDefSIZE_FLASH    64

__packed typedef struct
{
                uint32_t ArcRecNo;     //4      /* ����� ������ */  
                uint64_t Index_Time;   //12      /* ����� ������ � Unix ������� */
                double ArcVolume;      //20      /* ����������� ����� */
                float  ArcTempGas;        //24      /* ������� ����������� ���� �� �������� * 100*/ 
                float  ArcTempEnv;        //28      /* ������� ����������� ���. ��. �� �������� * 100*/                 
                uint64_t event;        //36
                uint64_t event_story;  //44
                uint32_t num_int;      //48
                uint16_t ArcK;         //50
                uint16_t status_system;   //52
                uint16_t status_warning;  //54              
                uint16_t status_alarm;    //56
                uint16_t status_crash;    //58  
                uint16_t rez;    //60
                uint32_t ArcCRC;       //64  
                
              //  uint32_t ArcError;     //52      /* ������ ��� */                
              //  float  ArcMaxT;        //40
              //  float  ArcMinT;        //44                
              //  float  ArcMaxQ;        //36      /* ������������ ������ �� �������� */                

}IntArcTypeDef; // 60
#define IntArcTypeDefSIZE_FLASH    64

__packed typedef struct
{ 
               uint32_t  ArcRecNo;         // 4   /* ����� ������ */  
               uint64_t Index_Time;        // 12      /* ����� ������ � Unix ������� */
               uint8_t  ID_Param;          // 13   /* ID ���������  14*/
               uint8_t  ID_Change;         // 14 ID ��������� ���������
               char OldValue[64];          // 44  /* ������ �������� */
               char NewValue[64];          // 74  /* ����� �������� */
               uint8_t  ArcCLB_Lock;       // 75   /* ��������� ������ ����� */ 
               uint8_t  lock_manufacture;
               uint8_t  lock_provider;
               uint8_t  status_cover;       // 76   /* ��������� ������ */
               uint8_t  license_key[30];
               uint32_t ArcCRC;            // 80
} ChangeArcTypeDef;
#define ChangeArcTypeDefSIZE    180
#define ChangeArcTypeDefSIZE_FLASH    256

__packed typedef struct
{ 
               uint32_t  ArcRecNo;         // 4   /* ����� ������ */  
               uint64_t Index_Time;        // 12      /* ����� ������ � Unix ������� */
               uint64_t Index_Time_end;        // 20      /* ����� ������ � Unix ������� */
} NoAndTimeArcTypeDef;

typedef union 
{
  IntArcTypeDef          IntArc; // 64
  ChangeArcTypeDef       ChangeArc; // 180
  EventSystemArcTypeDef  EventSystemArc; // 36
  NoAndTimeArcTypeDef    NoAndTimeArcType; // 20
  uint8_t               *ptr_read; // 1
  uint8_t                beginArc; // 1
}enumArcTypeDef; // 178

/*
typedef struct
{
  TArchiv_type Arc_Type;
  TARC_RW      Arc_RW;
  TARC_SOURCE  Arc_Source;
  uint8_t*     Arc_Buffer;
  uint32_t     Arc_RecNo;
} TARC_Queue; 
*/
typedef struct
{
  TArchiv_type Arc_Type; //1
  TARC_RW      Arc_RW; //1
//  TARC_SOURCE  Arc_Source;
  enumArcTypeDef     Arc_Buffer; //180
  uint32_t     Arc_RecNo;//4
} TARC_Queue;//154

enum
{
  EVENT_BEGIN = 1,
  EVENT_END  
};

enum
{
  ARC_READ_STATUS_IN_RANGE_OK = 0,
  ARC_READ_STATUS_NOT_RANGE_ERROR,
  ARC_READ_STATUS_TIME_FAIL_ERROR,
  ARC_READ_STATUS_CRC_ERROR,
  ARC_READ_STATUS_END_FIND
};

enum
{
  LABEL_DATE,
  LABEL_NUMBER
};






typedef enum {                        // optic tcp progr
              CHANGE_DATE_TIME = 5,   // + +
              CHANGE_APN_ADDRESS = 6, // + +
              CHANGE_APN_LOGIN = 7,   // + +
              CHANGE_APN_PASWORD = 8, // + +
          //    CHANGE_TCP_SERVER = 9, // + +
              CHANGE_SMS_NUMBBER = 10, // + +
              CHANGE_TRANSFER_MODE = 11, // + + +
              CHANGE_SERIAL_NUMBER = 15, // +
              CHANGE_BALANCE_NUMBER = 16, // + +
              CHANGE_GAS_DAY = 19, // + +
              CHANGE_AUTO_MODE = 21, // - +
              CHANGE_CNT_SESSION = 29, // + +
              CHANGE_RESERVED_INT = 31, // + +
              CHANGE_MAX_CNT_COMMUNICATION_GSM  = 35, // - +
              CHANGE_MOTO_INFO = 42,       
              CHANGE__dQF = 51,    // + // ���� ������
              CHANGE_REC_TIME  = 52, // + // ������ ��������� � �������
            //  CHANGE_S_MES_TIME = 53,    // Short ���������
              CHANGE_L_MES_TIME = 54,     // + Long ���������  
              CHANGE_TEST_REC_TIME = 55,  // ������ ��������� � �������,�������� �����
              CHANGE_TEST_L_MES_TIME = 56, // ������ long ��������� ,�������� �����   
        //      CHANGE_SHORT_MEAS = 57,    // ������������ �������� ��������� 
              CHANGE_MAX_FLOW  = 58,    // ������� ���� ������ 
              CHANGE_TEMP_PAR_MAX  = 59,    // ����-��� ������� �����������
              CHANGE_CLB_LOCK  = 60,    // + + ��������� ������� �������������� �����
              CHANGE_TYPE_DEVICE  = 61,    // +
              CHANGE_STATUS  = 62, // - - +
              CHANGE_PRESSURE_ABS  = 63, // + +
              CHANGE_K_FACTOR  = 64, // + +
              CHANGE_VALVE_RESOLUTION  = 65, // - +
              CHANGE_VALVE_PRESENCE  = 66, // 

              CHANGE_VALVE = 68,
              CHANGE_TCP_SERVER1 = 69, // + +
              CHANGE_TCP_SERVER2 = 70, // + +
              CHANGE_TCP_SERVER3 = 71, // + +
              CHANGE_QPOS_LIMIT = 72, // +        
              CHANGE_QNEG_LIMIT = 73, // +  
              CHANGE_KONTRAST = 74, // +  
              CHANGE_SERIAL_NUMBER_SGM1  = 75, //
              CHANGE_SERIAL_NUMBER_SGM2  = 76, //
              CHANGE_SERIAL_NUMBER_SGM3  = 77, //
              CHANGE_SERIAL_NUMBER_SGM4  = 78, //   
              CHANGE_DATE_VERIFIC = 79,
              CHANGE_DATE_VERIFIC_NEXT = 80,      
              CHANGE_Q_MAX_MULTIPLIER_UP = 81,  
              CHANGE_GAS_RANGE_UI = 82,  
              CHANGE_QMAX_ON_PER_S = 83,  
              CHANGE_QMAX_OFF_PER_S = 84, 
              CHANGE_CNT_MEASUREMENT_TEMPER_EEPROM = 85,
              CHANGE_TEMP_PAR_MIN = 86,
              CHANGE_GAS_CNT_ERROR_UI = 87,
              CHANGE_OPEN_PASSWORD_FABRIC = 88, 
              CHANGE_OPEN_PASSWORD_PROVIDER = 89,              
              CHANGE_REVERSE_FLOW_CNT = 90,
              CHANGE_VALUE_REVERSE_FLOW = 91,
              CHANGE_SERIAL_NUMBER_BOARD = 92,

              CHANGE_GAS_RANGE = 94,
              CHANGE_GAS_CNT_ERROR = 95,
              CHANGE_BODY_COVER_OPEN = 96,
              CHANGE_BATTERY_COVER_OPEN = 97,
              CHANGE_TEST_MODE = 98,   
              CHANGE_CLEAR_ARHIVE = 99, 
              CHANGE_CLEAR_COUNTER = 100,
              CHANGE_SMS_NUMBBER2 = 101,
              CHANGE_CNT_VALVE_MIN = 102,
              CHANGE_CHECK_OPEN_TIME = 103,
              CHANGE_VALVE_MIN = 104,
              CHANGE_VALVE_Q_MIN = 105,
              CHANGE_VALVE_CLOSE_Q_PH = 106,
              CHANGE_VALVE_AUTO_CONTROL = 107,
              CHANGE_ENABLE_SWITCH_MENU = 108,
              CHANGE_KALMAN_BORDER_L = 109,
              CHANGE_KALMAN_BORDER_H = 110,
              CHANGE_LCD_FREQUENCY = 111,
              CHANGE_HW_VERSION = 112,
              CHANGE_QGL_MAX = 113,
              CHANGE_QGL_MIN = 114,
              CHANGE_QFL_LIMIT = 115,
              CHANGE_CRC_QDF = 116,
              CHANGE_ENABLE_KALMAN_FILTER = 117,
              CHANGE_KALMAN_FACTOR = 118,
              CHANGE_KALMAN_COUNTER = 119,
              CHANGE_ST_LUT = 120,
              CHANGE_PASWORD_PROVIDER = 121,
              CHANGE_COMMISSIONING = 122,
              CHANGE_OPTIC_TIME = 123,
              CHANGE_TIME_MEAS_EX_TEMP  =  124,
              CHANGE_EVENT_WARNING_EN  =  125,           
              CHANGE_EVENT_ALARM_EN  =  126,                
              CHANGE_EVENT_CRASH_EN  =  127,              
              CHANGE_EXT_MAX_TEMP  =  128,                
              CHANGE_EXT_MIN_TEMP  =  129,  
              CHANGE_EXT_TEMP_CNT  =  130, 
              
              CHANGE_TEMP_GAS_MIN_CRASH  =  132, 
              CHANGE_TEMP_GAS_MAX_CRASH  =  133, 
              CHANGE_TEMP_GAS_PER_CRASH  =  134, 
              CHANGE_TEMP_ENV_MIN_CRASH  =  135, 
              CHANGE_TEMP_ENV_MAX_CRASH  =  136, 
              CHANGE_TEMP_ENV_PER_CRASH  =  137, 
              CHANGE_TEMP_GAS_CRASH  =  138, 
              CHANGE_TEMP_ENVIRON_CRASH  =  139,    
              CHANGE_ERR_SGM_PER_LONG = 140,
              CHANGE_ERR_SGM_PER_SHORT = 141,
              CHANGE_ERR_SGM_PER_FAST = 142,
              CHANGE_ERR_SGM_CNT_LONG = 143,
              CHANGE_TEL_BAT_REPLACE = 144,
              CHANGE_COUNT_BAT_REPLACE = 145,
              CHANGE_TIME_BAT_REPLACE = 146,
              
              CHANGE_WARNING_CLEAR = 152,
              CHANGE_ALARM_CLEAR = 153,              
              CHANGE_CRASH_CLEAR = 154,   
              CHANGE_VALVE_DELAY_BEFORE_OPEN = 155,
              
              CHANGE_ENABLE_MDM_CHANGE_VALVE = 157,
              CHANGE_PIN_CALIB_KEY = 158,
              CHANGE_ARCHIVE_NUMBER = 159,
              
              CHANGE_SHOW_VER_PO = 162,
              CHANGE_WAITING_TIME_LCD = 163,
              CHANGE_MODEM_LOG = 164,
              CHANGE_RESTART = 165,
              CHANGE_REV_FL_ON_PER_S = 166,
              CHANGE_REV_FL_OFF_PER_S = 167,
              CHANGE_VALVE_CLOSE_REV_P = 168,
              CHANGE_VALVE_TIME = 169,
              CHANGE_CLEAR_SN_SGM = 170,   
              CHANGE_VALVE_DELAY_TRY_OPEN = 171,                 
              CHANGE_MIN_STACK_DISPATCHER_TASK = 172, 
              CHANGE_MIN_STACK_GASMETER_TASK = 173, 
              CHANGE_MIN_STACK_OPTO_READ_TASK = 174, 
              CHANGE_MIN_STACK_READ_WRITE_ARC_TASK = 175,               
              CHANGE_MIN_STACK_GSM_TASK = 176,     
              CHANGE_MIN_STACK_GSM_RX_UART_TASK = 177,  
              CHANGE_MIN_STACK_PARSING_COMANDS_TASK = 178,   
              CHANGE_MIN_IDLE_TASK = 179,
              CHANGE_MIN_SIZEFREE_TASK = 180,
              CHANGE_MODE_CONSERVATION = 181,

} TCHANG_Type;

enum
{
   CHANGE_PROGRAM = 0,
   CHANGE_TCP,
   CHANGE_OPTIC
};




// extern const uint32_t IntArcMem_Size; //?
extern const uint16_t IntArcRecCount;
extern const uint16_t IntArcRecordPerSector; 

// extern const uint32_t DayArcMem_Size; //?
extern const uint16_t DayArcRecCount;

// extern const uint32_t EventArcMem_Size; //?
extern const uint16_t EvArcRecCount;
extern const uint16_t EventArcRecordPerSector; 
extern const uint16_t ChangeRecordPerSector;

//extern const uint32_t ChangeArcMem_Size; //? 
extern const uint16_t ChangeArcRecCount;
extern const uint16_t SystemArcRecCount;
extern const uint16_t TelemetryArcRecCount;

extern const uint8_t IntArcLen;
extern const uint8_t ChangeArcLen;
extern const uint8_t EventArcLen; 
extern const uint8_t SystemArcLen; 
extern const uint8_t TelemetryArcLen; 

extern const uint32_t  IntArcRecNo;
extern const uint32_t  DayArcRecNo;
//extern const uint32_t  EvArcRecNo;
extern const uint32_t ChangeArcRecNo;
extern const uint32_t SystemArcRecNo;
extern const uint32_t TelemetryArcRecNo;

extern const uint16_t  GasDayBorder;

extern char OldValue[];
extern char NewValue[];
extern uint64_t EventTimeBegin;
extern  uint64_t EventTimeEnd;
extern double Int_QgL_m3_midl;
extern uint8_t srt_license_key[30];


//extern IntArcTypeDef frameArchiveInt;
//extern EventArcTypeDef frameArchiveEvent;
//extern ChangeArcTypeDef frameArchiveChange;

/*
extern IntArcTypeDef    IntArc;
extern EventArcTypeDef  EvArc;
extern ChangeArcTypeDef ChangeArc;
*/


uint8_t WriteIntEventArcRecord(uint64_t  ArcFlag, uint64_t flag_begin_end);
uint8_t ReadIntArc(uint32_t p_IntArcRecNo, uint8_t p_arcType);
uint64_t Get_IndexTime(LL_RTC_DateTypeDef p_ArcDate, LL_RTC_TimeTypeDef p_ArcTime);
uint32_t CalcCRC(uint8_t* pRecord, uint16_t pSize);
uint8_t WriteTelemetryArcRecord(uint16_t p_Event_Code, int8_t p_Event_Result, uint16_t p_Sesion_Number);
uint8_t WriteChangeArcRecord(TCHANG_Type p_Parametr, uint8_t _ID_Change)   ;
void WriteChangeArcStatus(uint8_t flag_end);
uint8_t WriteSystemArcRecord(uint16_t p_Event_Code, uint16_t p_Sesion_Number);
void ReadArchiveParsing(enumArcTypeDef *ptr_Arc_Buffer, TArchiv_type archiv_type, uint8_t *ptr_out, uint32_t numberArcCRC_ERROR);
uint8_t ReadArchiveQueue(TArchiv_type archiv_type, 
                    uint32_t     n_record,
                    uint64_t     label_begin,
                    uint64_t     label_end,
                    uint8_t      *ptr_out);
uint8_t TransmitARCHIVE(uint8_t *ptr_buf_in, uint8_t *ptr_buf_out, TTempDataParsingCommand *ptr_TempDataParsingCommand);



#endif

