uint8_t init_THardWare();
uint8_t Read_Device(uint8_t Register, uint16_t* Data );
uint8_t MeasureExtTemp(void);
void MX_InitTSensor(void);
float GetExtTemp();
float GetExtTempMidl();
float GetExtTempMidl_2();
float GetExtTempMidl_3();
void Clear_cnt_ExtTempMidl();
void Clear_cnt_ExtTempMidl_2();
void Clear_cnt_ExtTempMidl_3();
uint32_t Get_cnt_ExtTempMidl_2();
uint32_t Get_cnt_ExtTempMidl_3();
//uint8_t Get_going_beyond_T_environ();

//extern float ExtTemp;