//=============================================================================
/// \file    FlowMeasurement.h
/// \author  SUL
/// \date    08-Dec-2014
/// \brief   Module for Flow-Measurement
//=============================================================================
// COPYRIGHT
//=============================================================================

#ifndef FLOW_MEASUREMENT_H
#define FLOW_MEASUREMENT_H

#include "Typedef.h"
#include "Error.h"
#include "Sgm.h"



//-----------------------------------------------------------------------------
// Definition of module errors. Add here new module errors. 
// Module errors from 0x00 to 0x1F are allowed.
#define ERROR_FLOWMEAS_LUT_NUMBER_OUT_OF_RANGE (ERROR_MODULE_FLOWMEAS | 0x00)
#define ERROR_FLOWMEAS_TOO_FEW_SAMPLINGPOINTS  (ERROR_MODULE_FLOWMEAS | 0x01)
#define ERROR_FLOWMEAS_TOO_MANY_SAMPLINGPOINTS (ERROR_MODULE_FLOWMEAS | 0x02)
#define ERROR_FLOWMEAS_LUT_VALUES_INCORRECT    (ERROR_MODULE_FLOWMEAS | 0x03)
#define ERROR_FLOWMEAS_UNKOWN_FLOW_UNIT        (ERROR_MODULE_FLOWMEAS | 0x04)
#define ERROR_FLOWMEAS_SENSOR_CONFIGURATION    (ERROR_MODULE_FLOWMEAS | 0x05)
#define ERROR_FLOWMEAS_TIMEOUT                 (ERROR_MODULE_FLOWMEAS | 0x06)
#define ERROR_FLOWMEAS_MEASURE_FLOW            (ERROR_MODULE_FLOWMEAS | 0x09)
#define ERROR_FLOWMEAS_MEASURE_FLOW_CHECK      (ERROR_MODULE_FLOWMEAS | 0x0A) 
#define ERROR_NO_MODULE_GASMETER               (ERROR_MODULE_FLOWMEAS | 0x0B)

//-----------------------------------------------------------------------------
/// \brief flow unit
typedef enum {
  FLOWMEAS_UNIT_UNKOWN,
  FLOWMEAS_UNIT_NORM_LITER_PER_MINUTE,        ///< 
  FLOWMEAS_UNIT_STANDARD_LITER_20_PER_MINUTE, ///< 
  FLOWMEAS_UNIT_STANDARD_LITER_15_PER_MINUTE, ///< 
  FLOWMEAS_UNIT_STANDARD_LITER_25_PER_MINUTE, ///< 
  FLOWMEAS_UNIT_NORM_LITER_PER_HOUR,          ///< 
  FLOWMEAS_UNIT_STANDARD_LITER_20_PER_HOUR,   ///< 
  FLOWMEAS_UNIT_STANDARD_LITER_15_PER_HOUR,   ///< 
  FLOWMEAS_UNIT_STANDARD_LITER_25_PER_HOUR,   ///< 
} FlowMeas_Unit;

struct TypeFlowMeasurement
{
  int           Sensor_Qfl; 
  double        lo_QgLm3;     
  uint32_t      lo_mesCount; 
  double        dQf_l;                  
  float         lo_LastSensQf;
  float         lo_KalmanK;
  uint8_t       lo_KalmanFlag;
  uint8_t       lo_AddToFlow;
  uint32_t      lo_LastMeas;
  uint16_t      KalmanBorder;
  int           lo_LUT_Sum;
  int           lo_TRaw_midl;
  
};
extern struct TypeFlowMeasurement Flow;

// ----------------------------------------------------------------------------
/// \brief Initialize Flow-Measurement-Module.
Error FlowMeas_InitModule(void);

// ----------------------------------------------------------------------------
/// \brief Perform a flow measurement.
/// \param value       Pointer to return the flow measurment value
/// \param k           TODO
/// \return Error flags
void FlowMeas_MeasureFlow(void);
void FlowMeas_MeasureFlow_Old(void);
Error FlowMeas_MeasureFlowShort(void); 

// -- Normal Mode (default) ---------------------------------------------------
/** \name Normal Mode (default) */
extern const uint16_t Config_GasmeterMeasurementIntervalNormalModeFM;
extern const uint16_t Config_GasmeterMeasurementIntervalNormalModeSM;
//const Config_GasmeterVDDRecognitionIntervalNormalMode @".eeprom" = 86400  ;
extern const uint16_t Config_GasmeterGasRecognitionIntervalNormalMode;

extern const uint16_t Config_GasmeterGasRecognitionRetryIntervalNormalMode; // 5 �����
//#define Config_GasmeterBackupIntervalNormalMode 1800 // 30min

// -- Test Mode ---------------------------------------------------------------
extern const uint16_t Config_GasmeterMeasurementIntervalTestModeFM;
extern const uint16_t Config_GasmeterGasRecognitionIntervalTestMode;
extern const uint16_t Qf_sizeof;
//extern int Sensor_Qfl;
//extern uint8_t lo_KalmanFlag;
//extern float lo_KalmanK;

// ----------------------------------------------------------------------------
/// \brief Get the unit of the flow-measurement.
///
/// Unit is only valid for flow measurements!
/// \return The unit of the flow-measurement
// FlowMeas_Unit FlowMeas_GetFlowUnit(void);
extern const int QposLimit;
extern const int QnegLimit;
extern uint8_t reverse_flow;
extern uint8_t FlagBadSensor;


extern const uint16_t EnableKalmanFilter;
//extern uint16_t KalmanBorder;
extern const uint16_t KalmanCnt;
extern const float KalmanK1;
extern const float KalmanK2;

//extern uint32_t _MeasurementIntervalFM;
extern   double GasMeter_QgL_Disp;
extern const int Reverse_Cnt;

Error ReadDeviceVDD(void);
void ClearFlow();
#endif /* FLOW_MEASUREMENT_H */
