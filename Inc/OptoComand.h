//=============================================================================
//  
//=============================================================================
/// \file    OptoComand.h
/// \author 
/// \date   
/// \brief   Comand optical interface
//=============================================================================
// COPYRIGHT
//=============================================================================
// 
//=============================================================================

#include "commands_func.h"

#ifndef OPTOCOMAND_H
#define OPTOCOMAND_H

extern TTempDataParsingCommand OPTIC_TempDataParsingCommand;
extern uint16_t ReceivedCounter;
#endif