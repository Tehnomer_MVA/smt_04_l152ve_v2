//=============================================================================
/// \file    Sgm.c
/// \author  RFU
/// \date    13-Apr-2016
/// \brief   Module for Sensirion Gasmeter Sensor
//=============================================================================
// COPYRIGHT
//=============================================================================

#ifndef SGM_H
#define SGM_H

#include "Typedef.h"
#include "Error.h"
#include "SensorTypeParameters.h"
  
//-----------------------------------------------------------------------------
/// Sgm Configuration
//-----------------------------------------------------------------------------
#ifdef CONFIG_USE_CATEGORY_SGM

/// \brief I2c-address of the Sgm-Sensor
const uint8_t  Config_SgmAddress                             = 0x28;
/// \brief Sgm-reset-time. The time to wait to reset the sensor.
const uint32_t Config_SgmResetTimeMilliseconds               = 500;
/// \brief Sgm-startup-time. The time needed for startup of the sensor.
const uint32_t Config_SgmStartupTimeMilliseconds             = 200;
/// \brief The CRC-Polynomial used by the sensor.
///        P(x)=x^8+x^5+x^4+1 = 100110001 => 0x0131
///        The MSB of the CRC generator polynomials is omitted,
///        it is allwas 1 and will not be used.
const uint8_t  Config_SgmCrcPolynomial                       = 0x31;
/// \brief The CRC start vector used by the sensor.
const uint8_t  Config_SgmCrcStartVector                      = 0xFF;

#endif /* CONFIG_USE_CATEGORY_SGM */

extern TSensor Sensor;

//-----------------------------------------------------------------------------
// Definition of module errors. Add here new module errors. 
// Module errors from 0x00 to 0x1F are allowed.
#define ERROR_SGM_CRC                   (ERROR_MODULE_SGM | 0x00)
#define ERROR_SGM_SENSOR_EEPROM_READ    (ERROR_MODULE_SGM | 0x01)
#define ERROR_SGM_UNKOWN_SGM_TYPE       (ERROR_MODULE_SGM | 0x02)
#define ERROR_SGM_WAIT_FOR_PRAM_TIMEOUT (ERROR_MODULE_SGM | 0x03)  

//-----------------------------------------------------------------------------
/// \brief Sensor types indexes.
/// Must match with SensorTypeParameters in SensorTypeParameters.c!
typedef enum {
  SGM_TYPE_UNDEFINED = -1,
  SGM_TYPE_SGM4      =  0,
  SGM_TYPE_SGM6      =  1,
  SGM_TYPE_SGM10     =  2,
  SGM_TYPE_SGM16     =  3,
  SGM_TYPE_SGM25     =  4,
} Sgm_Type;


extern Sgm_Type _SgmType;
//-----------------------------------------------------------------------------
/// \brief Initialize the sensor power pin.
void Sgm_InitHardware(void);

//-----------------------------------------------------------------------------
/// \brief Restart sensor and read the sensor type.
///
/// Performs a power-cycle to reset the sensor and waits until the sensor is
/// ready for communication through I2c.
/// \return Error flags
Error Sgm_InitModule(void);
Error Sgm_DeinitModule(void);

void SGM_PowerOn(void);
void SGM_PowerOff(void);


//-----------------------------------------------------------------------------
/// \brief Gets the sensor id number.
// Error Sgm_GetId(uint16_t* p_id0,uint16_t* p_id1,uint16_t* p_id2);

//-----------------------------------------------------------------------------
/// \brief Performs a flow measurement.
//Error Sgm_MeasFlow(uint16_t k, uint16_t* rawFlow, uint16_t* rawQfa);
/*Error Sgm_MeasFlow(uint16_t k, // ��� ������
					uint16_t* qLutShort, // ��������� ��� ��������� Short 
					uint16_t* qLutLong,  // ��������� ��� ��������� Long
					uint16_t* k8T8,      // R ������ ��� Sort ��������� 
					uint16_t* qFA_long,  // ������ Long
					uint16_t* qFA_short, // ������ Short
					uint16_t* Traw);       // �����������
*/
Error Sgm_MeasFlow(TSensor* pSensor);
//-----------------------------------------------------------------------------
//Error Sgm_MeasFlowShort(uint16_t k8T8, uint16_t* qShort, uint16_t* qFA_Short);
Error Sgm_MeasFlowShort(TSensor* pSensor);
//-----------------------------------------------------------------------------
/// \brief Performs a gas recognition and returns the k value.
Error Sgm_GasRecognition(uint16_t* pGasRec0,uint16_t* pGasRec1,uint16_t* pGasRec2, uint16_t* pGasRec3);


//-----------------------------------------------------------------------------
/// \brief Gets the scale factor, flow unit and offset.
Error Sgm_GetSf_Offset(uint16_t* p_scaleFactor, uint16_t* p_flowUnit, uint16_t* p_offset, uint8_t p_chan);

//-----------------------------------------------------------------------------
/// \brief Gets the product serial number from the sensor.
Error Sgm_GetProductSerialNumberCh0(uint64_t* serialNumber);
/*Error Sgm_GetProductSerialNumberCh1(uint64_t* serialNumber);
Error Sgm_GetProductSerialNumberCh2(uint64_t* serialNumber);
Error Sgm_GetProductSerialNumberCh3(uint64_t* serialNumber);
*/
//-----------------------------------------------------------------------------
/// \brief Gets the article code from the sensor.
Error Sgm_GetArticleCodeCh0(uint8_t* buffer);
/*Error Sgm_GetArticleCodeCh1(uint8_t* buffer);
Error Sgm_GetArticleCodeCh2(uint8_t* buffer);
Error Sgm_GetArticleCodeCh3(uint8_t* buffer);
*/
//-----------------------------------------------------------------------------
/// \brief Gets the sensor specific parameters.
/// \return sensor parameters
//SensorTypeParameters* Sgm_GetParameters(void);

//-----------------------------------------------------------------------------
/// \brief Gets the sensor specific parameters.
/// \return sensor parameters
//Error Sgm_GetVDDCh0(short* p_dQVDD, uint16_t* p_VDD);
//Error Sgm_GetVDDCh1(short* p_dQVDD, uint16_t* p_VDD);
//Error Sgm_GetVDDCh2(short* p_dQVDD, uint16_t* p_VDD);
extern uint16_t manual_KL;

#endif /* SGM_H */
