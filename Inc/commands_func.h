//=============================================================================
//  
//=============================================================================
/// \file    commands_func.h
/// \author 
/// \date   
/// \brief   Comand optical interface
//=============================================================================
// COPYRIGHT
//=============================================================================
// 
//=============================================================================


#ifndef COMMANDS_FUNC_H
#define COMMANDS_FUNC_H

#include "main.h"
#include "stdint.h"
#include "stdbool.h"
#include "cmsis_os.h"
#include "magic.h"
#include "valve.h"
typedef enum
{
   SOURCE_PROGRAM=0,
   SOURCE_TCP,   
   SOURCE_OPTIC,
   SOURCE_RS485,
   SOURCE_CSD
}TypeSourceCommands;

__packed typedef struct
  {
   uint64_t TimeFindUnixBegin;
   uint64_t TimeFindUnixEnd;
   uint32_t numBeg;
   uint32_t numEnd;
   uint32_t crc32_old;
   uint16_t number_frame;   
   int16_t index_commands;
   uint16_t size_bufer;
   uint16_t size_bufer_left;
   uint8_t flag_detection_archive; 
   uint8_t flag_message_of_patrs;
   uint8_t flag_message_of_patrs_end;
   uint8_t flag_end; 
  } TTempDataParsingCommand;

typedef uint8_t (*TParsFunc)(char* param, uint8_t *ptr_out_buf, uint8_t *ptr_str_reply, TypeSourceCommands source, TTempDataParsingCommand *ptr_TempDataParsingCommand);

__packed typedef struct
  {
   TParsFunc ParsFunc;
   char* commandStr;
  } TParser;



enum
{
  COMMAND_OK = 0,
  COMMAND_ERROR,
  COMMAND_INCORRECT_VALUE,
  COMMAND_ACCESS_DENIED,
  COMMAND_READ_ONLY,
  COMMAND_ERROR_CRC,
  COMMAND_ERROR_COMMAND,
  COMMAND_WRITE_ONLY,
  COMMAND_INCORRECT_PASSWORD
};

__packed typedef struct 
{
  TypeSourceCommands    source;
  SemaphoreHandle_t     *sem_source; // ptr semafore
  uint8_t               *ptr_commands;
  uint8_t               *ptr_out;
  TTempDataParsingCommand *ptr_temp_data;
}TypeParsingCommand;

extern uint8_t flag_crypt_message[];


void PF_DateTime(char* parString, uint8_t *ptr_out_buf, uint8_t *ptr_str_reply, TypeSourceCommands source, TTempDataParsingCommand *ptr_TempDataParsingCommand);
void PF_GroupInfo(char* parString, uint8_t *ptr_out_buf, uint8_t *ptr_str_reply, TypeSourceCommands source, TTempDataParsingCommand *ptr_TempDataParsingCommand);
void FormationGROUP_INFO(uint8_t *ptr_buf, TTempDataParsingCommand *ptr_TempDataParsingCommand);
void PF_GET_GSM_INFO(char* parString, uint8_t *ptr_out_buf, uint8_t *ptr_str_reply, TypeSourceCommands source, TTempDataParsingCommand *ptr_TempDataParsingCommand);
void  FormationGSM_INFO(uint8_t *ptr_buf, TTempDataParsingCommand *ptr_TempDataParsingCommand);
void PF_EXIT(char* parString, uint8_t *ptr_out_buf, uint8_t *ptr_str_reply, TypeSourceCommands source, TTempDataParsingCommand *ptr_TempDataParsingCommand);

void PF_TestMode(char* parString, uint8_t *ptr_out_buf, uint8_t *ptr_str_reply, TypeSourceCommands source, TTempDataParsingCommand *ptr_TempDataParsingCommand);
void PF_Qdf(char* parString, uint8_t *ptr_out_buf, uint8_t *ptr_str_reply, TypeSourceCommands source, TTempDataParsingCommand *ptr_TempDataParsingCommand);
void PF_DevInfo(char* parString, uint8_t *ptr_out_buf, uint8_t *ptr_str_reply, TypeSourceCommands source, TTempDataParsingCommand *ptr_TempDataParsingCommand);
void PF_SerialNum(char* parString, uint8_t *ptr_out_buf, uint8_t *ptr_str_reply, TypeSourceCommands source, TTempDataParsingCommand *ptr_TempDataParsingCommand);
void PF_MeasTest(char* parString, uint8_t *ptr_out_buf, uint8_t *ptr_str_reply, TypeSourceCommands source, TTempDataParsingCommand *ptr_TempDataParsingCommand);
void PF_CurVol(char* parString, uint8_t *ptr_out_buf, uint8_t *ptr_str_reply, TypeSourceCommands source, TTempDataParsingCommand *ptr_TempDataParsingCommand);
void PF_Server_MT(char* parString, uint8_t *ptr_out_buf, uint8_t *ptr_str_reply, TypeSourceCommands source, TTempDataParsingCommand *ptr_TempDataParsingCommand);
void PF_CLOSED(char* parString, uint8_t *ptr_out_buf, uint8_t *ptr_str_reply, TypeSourceCommands source, TTempDataParsingCommand *ptr_TempDataParsingCommand);

void PF_GasRTime(char* parString, uint8_t *ptr_out_buf, uint8_t *ptr_str_reply, TypeSourceCommands source, TTempDataParsingCommand *ptr_TempDataParsingCommand);
void PF_N_GasRTime(char* parString, uint8_t *ptr_out_buf, uint8_t *ptr_str_reply, TypeSourceCommands source, TTempDataParsingCommand *ptr_TempDataParsingCommand);
void PF_N_GAS_REC_UI(char* parString, uint8_t *ptr_out_buf, uint8_t *ptr_str_reply, TypeSourceCommands source, TTempDataParsingCommand *ptr_TempDataParsingCommand);
void PF_GAS_RANGE_UI(char* parString, uint8_t *ptr_out_buf, uint8_t *ptr_str_reply, TypeSourceCommands source, TTempDataParsingCommand *ptr_TempDataParsingCommand);
void PF_GasRange(char* parString, uint8_t *ptr_out_buf, uint8_t *ptr_str_reply, TypeSourceCommands source, TTempDataParsingCommand *ptr_TempDataParsingCommand);
void PF_GasLTime(char* parString, uint8_t *ptr_out_buf, uint8_t *ptr_str_reply, TypeSourceCommands source, TTempDataParsingCommand *ptr_TempDataParsingCommand);
void PF_TestRTime(char* parString, uint8_t *ptr_out_buf, uint8_t *ptr_str_reply, TypeSourceCommands source, TTempDataParsingCommand *ptr_TempDataParsingCommand);
void PF_TestLTime(char* parString, uint8_t *ptr_out_buf, uint8_t *ptr_str_reply, TypeSourceCommands source, TTempDataParsingCommand *ptr_TempDataParsingCommand);
void PF_ReadArc(char* parString, uint8_t *ptr_out_buf, uint8_t *ptr_str_reply, TypeSourceCommands source, TTempDataParsingCommand *ptr_TempDataParsingCommand);
void PF_ArcNumRecords(char* parString, uint8_t *ptr_out_buf, uint8_t *ptr_str_reply, TypeSourceCommands source, TTempDataParsingCommand *ptr_TempDataParsingCommand);
void PF_ServerURL(char* parString, uint8_t *ptr_out_buf, uint8_t *ptr_str_reply, TypeSourceCommands source, TTempDataParsingCommand *ptr_TempDataParsingCommand, uint8_t number);
void PF_ServerURL1(char* parString, uint8_t *ptr_out_buf, uint8_t *ptr_str_reply, TypeSourceCommands source, TTempDataParsingCommand *ptr_TempDataParsingCommand);
void PF_ServerURL2(char* parString, uint8_t *ptr_out_buf, uint8_t *ptr_str_reply, TypeSourceCommands source, TTempDataParsingCommand *ptr_TempDataParsingCommand);
void PF_ServerURL3(char* parString, uint8_t *ptr_out_buf, uint8_t *ptr_str_reply, TypeSourceCommands source, TTempDataParsingCommand *ptr_TempDataParsingCommand);
void PF_APNAdress(char* parString, uint8_t *ptr_out_buf, uint8_t *ptr_str_reply, TypeSourceCommands source, TTempDataParsingCommand *ptr_TempDataParsingCommand);
void PF_APNLogin(char* parString, uint8_t *ptr_out_buf, uint8_t *ptr_str_reply, TypeSourceCommands source, TTempDataParsingCommand *ptr_TempDataParsingCommand);
void PF_APNPassword(char* parString, uint8_t *ptr_out_buf, uint8_t *ptr_str_reply, TypeSourceCommands source, TTempDataParsingCommand *ptr_TempDataParsingCommand);
void PF_MaxFlowRate(char* parString, uint8_t *ptr_out_buf, uint8_t *ptr_str_reply, TypeSourceCommands source, TTempDataParsingCommand *ptr_TempDataParsingCommand);
void PF_EXT_MAX_TEMP(char* parString, uint8_t *ptr_out_buf, uint8_t *ptr_str_reply, TypeSourceCommands source, TTempDataParsingCommand *ptr_TempDataParsingCommand);
void PF_EXT_MIN_TEMP(char* parString, uint8_t *ptr_out_buf, uint8_t *ptr_str_reply, TypeSourceCommands source, TTempDataParsingCommand *ptr_TempDataParsingCommand);
void PF_EXT_TEMP_CNT(char* parString, uint8_t *ptr_out_buf, uint8_t *ptr_str_reply, TypeSourceCommands source, TTempDataParsingCommand *ptr_TempDataParsingCommand);

void PF_MaxTemp(char* parString, uint8_t *ptr_out_buf, uint8_t *ptr_str_reply, TypeSourceCommands source, TTempDataParsingCommand *ptr_TempDataParsingCommand);
void PF_MinTemp(char* parString, uint8_t *ptr_out_buf, uint8_t *ptr_str_reply, TypeSourceCommands source, TTempDataParsingCommand *ptr_TempDataParsingCommand);
void PF_GetLockState(char* parString, uint8_t *ptr_out_buf, uint8_t *ptr_str_reply, TypeSourceCommands source, TTempDataParsingCommand *ptr_TempDataParsingCommand);
void PF_MdmStart(char* parString, uint8_t *ptr_out_buf, uint8_t *ptr_str_reply, TypeSourceCommands source, TTempDataParsingCommand *ptr_TempDataParsingCommand);
void PF_GasDayBorter(char* parString, uint8_t *ptr_out_buf, uint8_t *ptr_str_reply, TypeSourceCommands source, TTempDataParsingCommand *ptr_TempDataParsingCommand);
void PF_ModemModeTransfer(char* parString, uint8_t *ptr_out_buf, uint8_t *ptr_str_reply, TypeSourceCommands source, TTempDataParsingCommand *ptr_TempDataParsingCommand);
void PF_ModemBalansePhohe(char* parString, uint8_t *ptr_out_buf, uint8_t *ptr_str_reply, TypeSourceCommands source, TTempDataParsingCommand *ptr_TempDataParsingCommand);
void PF_ModemReservedInterval(char* parString, uint8_t *ptr_out_buf, uint8_t *ptr_str_reply, TypeSourceCommands source, TTempDataParsingCommand *ptr_TempDataParsingCommand);
void PF_ModemAutoSwitchMode(char* parString, uint8_t *ptr_out_buf, uint8_t *ptr_str_reply, TypeSourceCommands source, TTempDataParsingCommand *ptr_TempDataParsingCommand);
void PF_MdmToOpto(char* parString, uint8_t *ptr_out_buf, uint8_t *ptr_str_reply, TypeSourceCommands source, TTempDataParsingCommand *ptr_TempDataParsingCommand);
void PF_ClearArc(char* parString, uint8_t *ptr_out_buf, uint8_t *ptr_str_reply, TypeSourceCommands source, TTempDataParsingCommand *ptr_TempDataParsingCommand);
void PF_ClearCount(char* parString, uint8_t *ptr_out_buf, uint8_t *ptr_str_reply, TypeSourceCommands source, TTempDataParsingCommand *ptr_TempDataParsingCommand);
void PF_ClearMdmCnt(char* parString, uint8_t *ptr_out_buf, uint8_t *ptr_str_reply, TypeSourceCommands source, TTempDataParsingCommand *ptr_TempDataParsingCommand);
void PF_sms_phone2(char* parString, uint8_t *ptr_out_buf, uint8_t *ptr_str_reply, TypeSourceCommands source, TTempDataParsingCommand *ptr_TempDataParsingCommand);
void PF_sms_phone1(char* parString, uint8_t *ptr_out_buf, uint8_t *ptr_str_reply, TypeSourceCommands source, TTempDataParsingCommand *ptr_TempDataParsingCommand);
void PF_type_device(char* parString, uint8_t *ptr_out_buf, uint8_t *ptr_str_reply, TypeSourceCommands source, TTempDataParsingCommand *ptr_TempDataParsingCommand);
void PF_QposLimit(char* parString, uint8_t *ptr_out_buf, uint8_t *ptr_str_reply, TypeSourceCommands source, TTempDataParsingCommand *ptr_TempDataParsingCommand);
void PF_QnegLimit(char* parString, uint8_t *ptr_out_buf, uint8_t *ptr_str_reply, TypeSourceCommands source, TTempDataParsingCommand *ptr_TempDataParsingCommand);
void PF_hw_version(char* parString, uint8_t *ptr_out_buf, uint8_t *ptr_str_reply, TypeSourceCommands source, TTempDataParsingCommand *ptr_TempDataParsingCommand);
void PF_pressure_abs(char* parString, uint8_t *ptr_out_buf, uint8_t *ptr_str_reply, TypeSourceCommands source, TTempDataParsingCommand *ptr_TempDataParsingCommand);
void PF_LCD_MENU(char* parString, uint8_t *ptr_out_buf, uint8_t *ptr_str_reply, TypeSourceCommands source, TTempDataParsingCommand *ptr_TempDataParsingCommand);
void PF_SN_SGM_E(char* parString, uint8_t *ptr_out_buf, uint8_t *ptr_str_reply, TypeSourceCommands source, TTempDataParsingCommand *ptr_TempDataParsingCommand);
void PF_QGL_MAX(char* parString, uint8_t *ptr_out_buf, uint8_t *ptr_str_reply, TypeSourceCommands source, TTempDataParsingCommand *ptr_TempDataParsingCommand);
void PF_QGL_MIN(char* parString, uint8_t *ptr_out_buf, uint8_t *ptr_str_reply, TypeSourceCommands source, TTempDataParsingCommand *ptr_TempDataParsingCommand);
void PF_QFL_LIMIT(char* parString, uint8_t *ptr_out_buf, uint8_t *ptr_str_reply, TypeSourceCommands source, TTempDataParsingCommand *ptr_TempDataParsingCommand);
void PF_VOLUME_INST(char* parString, uint8_t *ptr_out_buf, uint8_t *ptr_str_reply, TypeSourceCommands source, TTempDataParsingCommand *ptr_TempDataParsingCommand);
void PF_MOTO_INFO(char* parString, uint8_t *ptr_out_buf, uint8_t *ptr_str_reply, TypeSourceCommands source, TTempDataParsingCommand *ptr_TempDataParsingCommand);
void PF_DATE_VERIFIC_NEXT(char* parString, uint8_t *ptr_out_buf, uint8_t *ptr_str_reply, TypeSourceCommands source, TTempDataParsingCommand *ptr_TempDataParsingCommand);
void PF_DATE_VERIFIC(char* parString, uint8_t *ptr_out_buf, uint8_t *ptr_str_reply, TypeSourceCommands source, TTempDataParsingCommand *ptr_TempDataParsingCommand);
void PF_Q_max_multiplier_up(char* parString, uint8_t *ptr_out_buf, uint8_t *ptr_str_reply, TypeSourceCommands source, TTempDataParsingCommand *ptr_TempDataParsingCommand);
//void PF_Qmax_min_warning(char* parString, uint8_t *ptr_out_buf, uint8_t *ptr_str_reply, TypeSourceCommands source, TTempDataParsingCommand *ptr_TempDataParsingCommand);
//void PF_Qmax_warning_cnt_measurement(char* parString, uint8_t *ptr_out_buf, uint8_t *ptr_str_reply, TypeSourceCommands source, TTempDataParsingCommand *ptr_TempDataParsingCommand);
void PF_QMAX_ON_PER_S(char* parString, uint8_t *ptr_out_buf, uint8_t *ptr_str_reply, TypeSourceCommands source, TTempDataParsingCommand *ptr_TempDataParsingCommand);
void PF_QMAX_OFF_PER_S(char* parString, uint8_t *ptr_out_buf, uint8_t *ptr_str_reply, TypeSourceCommands source, TTempDataParsingCommand *ptr_TempDataParsingCommand);
void PF_DQF_END(char* parString, uint8_t *ptr_out_buf, uint8_t *ptr_str_reply, TypeSourceCommands source, TTempDataParsingCommand *ptr_TempDataParsingCommand);
void PF_cnt_measurement_temper_eeprom(char* parString, uint8_t *ptr_out_buf, uint8_t *ptr_str_reply, TypeSourceCommands source, TTempDataParsingCommand *ptr_TempDataParsingCommand);
//void PF_cnt_reverse_flow_eeprom(char* parString, uint8_t *ptr_out_buf, uint8_t *ptr_str_reply, TypeSourceCommands source, TTempDataParsingCommand *ptr_TempDataParsingCommand);
void PF_Qfl_reverse_flow(char* parString, uint8_t *ptr_out_buf, uint8_t *ptr_str_reply, TypeSourceCommands source, TTempDataParsingCommand *ptr_TempDataParsingCommand);
void PF_serial_number_board(char* parString, uint8_t *ptr_out_buf, uint8_t *ptr_str_reply, TypeSourceCommands source, TTempDataParsingCommand *ptr_TempDataParsingCommand);
void PF_status_system(char* parString, uint8_t *ptr_out_buf, uint8_t *ptr_str_reply, TypeSourceCommands source, TTempDataParsingCommand *ptr_TempDataParsingCommand);
void PF_status_warning(char* parString, uint8_t *ptr_out_buf, uint8_t *ptr_str_reply, TypeSourceCommands source, TTempDataParsingCommand *ptr_TempDataParsingCommand);
void PF_status_alarm(char* parString, uint8_t *ptr_out_buf, uint8_t *ptr_str_reply, TypeSourceCommands source, TTempDataParsingCommand *ptr_TempDataParsingCommand);
void PF_status_crash(char* parString, uint8_t *ptr_out_buf, uint8_t *ptr_str_reply, TypeSourceCommands source, TTempDataParsingCommand *ptr_TempDataParsingCommand);
//void PF_WaitingTimeLCD(char* parString, uint8_t *ptr_out_buf, uint8_t *ptr_str_reply, TypeSourceCommands source, TTempDataParsingCommand *ptr_TempDataParsingCommand);
void PF_Self_GasBorder(char* parString, uint8_t *ptr_out_buf, uint8_t *ptr_str_reply, TypeSourceCommands source, TTempDataParsingCommand *ptr_TempDataParsingCommand);
void PF_Set_KalmanFilter(char* parString, uint8_t *ptr_out_buf, uint8_t *ptr_str_reply, TypeSourceCommands source, TTempDataParsingCommand *ptr_TempDataParsingCommand);
void PF_KalmanK(char* parString, uint8_t *ptr_out_buf, uint8_t *ptr_str_reply, TypeSourceCommands source, TTempDataParsingCommand *ptr_TempDataParsingCommand);
void PF_KalmanBorderH(char* parString, uint8_t *ptr_out_buf, uint8_t *ptr_str_reply, TypeSourceCommands source, TTempDataParsingCommand *ptr_TempDataParsingCommand);
void PF_KalmanBorderL(char* parString, uint8_t *ptr_out_buf, uint8_t *ptr_str_reply, TypeSourceCommands source, TTempDataParsingCommand *ptr_TempDataParsingCommand);
void PF_KalmanCounter(char* parString, uint8_t *ptr_out_buf, uint8_t *ptr_str_reply, TypeSourceCommands source, TTempDataParsingCommand *ptr_TempDataParsingCommand);
void PF_MAGIC(char* parString, uint8_t *ptr_out_buf, uint8_t *ptr_str_reply, TypeSourceCommands source, TTempDataParsingCommand *ptr_TempDataParsingCommand);
void PF_COUNT_SESSION(char* parString, uint8_t *ptr_out_buf, uint8_t *ptr_str_reply, TypeSourceCommands source, TTempDataParsingCommand *ptr_TempDataParsingCommand);
void PF_ERROR_SESSION(char* parString, uint8_t *ptr_out_buf, uint8_t *ptr_str_reply, TypeSourceCommands source, TTempDataParsingCommand *ptr_TempDataParsingCommand);
void PF_ERROR_2_SESSION(char* parString, uint8_t *ptr_out_buf, uint8_t *ptr_str_reply, TypeSourceCommands source, TTempDataParsingCommand *ptr_TempDataParsingCommand);
void PF_ERROR_3_SESSION(char* parString, uint8_t *ptr_out_buf, uint8_t *ptr_str_reply, TypeSourceCommands source, TTempDataParsingCommand *ptr_TempDataParsingCommand);
void PF_COUNT_FAIL_SIM(char* parString, uint8_t *ptr_out_buf, uint8_t *ptr_str_reply, TypeSourceCommands source, TTempDataParsingCommand *ptr_TempDataParsingCommand);
void PF_COUNT_FAIL_SPEED(char* parString, uint8_t *ptr_out_buf, uint8_t *ptr_str_reply, TypeSourceCommands source, TTempDataParsingCommand *ptr_TempDataParsingCommand);
void PF_MAX_SESSION(char* parString, uint8_t *ptr_out_buf, uint8_t *ptr_str_reply, TypeSourceCommands source, TTempDataParsingCommand *ptr_TempDataParsingCommand);
void PF_PASWORD_FABRIC(char* parString, uint8_t *ptr_out_buf, uint8_t *ptr_str_reply, TypeSourceCommands source, TTempDataParsingCommand *ptr_TempDataParsingCommand);
void PF_ENABLE_SWITCH_MENU(char* parString, uint8_t *ptr_out_buf, uint8_t *ptr_str_reply, TypeSourceCommands source, TTempDataParsingCommand *ptr_TempDataParsingCommand);
void PF_MANUAL_LUT(char* parString, uint8_t *ptr_out_buf, uint8_t *ptr_str_reply, TypeSourceCommands source, TTempDataParsingCommand *ptr_TempDataParsingCommand);
void PF_PASWORD_PROVIDER(char* parString, uint8_t *ptr_out_buf, uint8_t *ptr_str_reply, TypeSourceCommands source, TTempDataParsingCommand *ptr_TempDataParsingCommand);
void PF_PASWORD_PROVID_VALUE(char* parString, uint8_t *ptr_out_buf, uint8_t *ptr_str_reply, TypeSourceCommands source, TTempDataParsingCommand *ptr_TempDataParsingCommand);
void PF_COMMISSIONING(char* parString, uint8_t *ptr_out_buf, uint8_t *ptr_str_reply, TypeSourceCommands source, TTempDataParsingCommand *ptr_TempDataParsingCommand);
void PF_OPTIC_TIME(char* parString, uint8_t *ptr_out_buf, uint8_t *ptr_str_reply, TypeSourceCommands source, TTempDataParsingCommand *ptr_TempDataParsingCommand);
//void PF_TIME_MEAS_EX_TEMP(char* parString, uint8_t *ptr_out_buf, uint8_t *ptr_str_reply, TypeSourceCommands source, TTempDataParsingCommand *ptr_TempDataParsingCommand);
void PF_EVENT_WARNING_EN(char* parString, uint8_t *ptr_out_buf, uint8_t *ptr_str_reply, TypeSourceCommands source, TTempDataParsingCommand *ptr_TempDataParsingCommand);
void PF_EVENT_ALARM_EN(char* parString, uint8_t *ptr_out_buf, uint8_t *ptr_str_reply, TypeSourceCommands source, TTempDataParsingCommand *ptr_TempDataParsingCommand);
void PF_EVENT_CRASH_EN(char* parString, uint8_t *ptr_out_buf, uint8_t *ptr_str_reply, TypeSourceCommands source, TTempDataParsingCommand *ptr_TempDataParsingCommand);
void PF_TEMP_GAS_MIN_CRASH(char* parString, uint8_t *ptr_out_buf, uint8_t *ptr_str_reply, TypeSourceCommands source, TTempDataParsingCommand *ptr_TempDataParsingCommand);
void PF_TEMP_GAS_MAX_CRASH(char* parString, uint8_t *ptr_out_buf, uint8_t *ptr_str_reply, TypeSourceCommands source, TTempDataParsingCommand *ptr_TempDataParsingCommand);
void PF_TEMP_GAS_PER_CRASH(char* parString, uint8_t *ptr_out_buf, uint8_t *ptr_str_reply, TypeSourceCommands source, TTempDataParsingCommand *ptr_TempDataParsingCommand);
void PF_TEMP_ENV_MIN_CRASH(char* parString, uint8_t *ptr_out_buf, uint8_t *ptr_str_reply, TypeSourceCommands source, TTempDataParsingCommand *ptr_TempDataParsingCommand);
void PF_TEMP_ENV_MAX_CRASH(char* parString, uint8_t *ptr_out_buf, uint8_t *ptr_str_reply, TypeSourceCommands source, TTempDataParsingCommand *ptr_TempDataParsingCommand);
void PF_TEMP_ENV_PER_CRASH(char* parString, uint8_t *ptr_out_buf, uint8_t *ptr_str_reply, TypeSourceCommands source, TTempDataParsingCommand *ptr_TempDataParsingCommand);
void PF_ERR_SGM_PER_LONG(char* parString, uint8_t *ptr_out_buf, uint8_t *ptr_str_reply, TypeSourceCommands source, TTempDataParsingCommand *ptr_TempDataParsingCommand);
void PF_ERR_SGM_PER_SHORT(char* parString, uint8_t *ptr_out_buf, uint8_t *ptr_str_reply, TypeSourceCommands source, TTempDataParsingCommand *ptr_TempDataParsingCommand);
void PF_ERR_SGM_PER_FAST(char* parString, uint8_t *ptr_out_buf, uint8_t *ptr_str_reply, TypeSourceCommands source, TTempDataParsingCommand *ptr_TempDataParsingCommand);
void PF_ERR_SGM_CNT_LONG(char* parString, uint8_t *ptr_out_buf, uint8_t *ptr_str_reply, TypeSourceCommands source, TTempDataParsingCommand *ptr_TempDataParsingCommand);
void PF_TEL_BAT_REPLACE(char* parString, uint8_t *ptr_out_buf, uint8_t *ptr_str_reply, TypeSourceCommands source, TTempDataParsingCommand *ptr_TempDataParsingCommand);
void PF_COUNT_BAT_REPLACE(char* parString, uint8_t *ptr_out_buf, uint8_t *ptr_str_reply, TypeSourceCommands source, TTempDataParsingCommand *ptr_TempDataParsingCommand);
void PF_TIME_BAT_REPLACE(char* parString, uint8_t *ptr_out_buf, uint8_t *ptr_str_reply, TypeSourceCommands source, TTempDataParsingCommand *ptr_TempDataParsingCommand);
void PF_KFACTOR(char* parString, uint8_t *ptr_out_buf, uint8_t *ptr_str_reply, TypeSourceCommands source, TTempDataParsingCommand *ptr_TempDataParsingCommand);
void PF_WARNING_CLEAR(char* parString, uint8_t *ptr_out_buf, uint8_t *ptr_str_reply, TypeSourceCommands source, TTempDataParsingCommand *ptr_TempDataParsingCommand);
void PF_ALARM_CLEAR(char* parString, uint8_t *ptr_out_buf, uint8_t *ptr_str_reply, TypeSourceCommands source, TTempDataParsingCommand *ptr_TempDataParsingCommand);
void PF_CRASH_CLEAR(char* parString, uint8_t *ptr_out_buf, uint8_t *ptr_str_reply, TypeSourceCommands source, TTempDataParsingCommand *ptr_TempDataParsingCommand);
void PF_VALVE_SET(char* parString, uint8_t *ptr_out_buf, uint8_t *ptr_str_reply, TypeSourceCommands source, TTempDataParsingCommand *ptr_TempDataParsingCommand);
void PF_VALVE(char* parString, uint8_t *ptr_out_buf, uint8_t *ptr_str_reply, TypeSourceCommands source, TTempDataParsingCommand *ptr_TempDataParsingCommand);
void PF_VALVE_TIME(char* parString, uint8_t *ptr_out_buf, uint8_t *ptr_str_reply, TypeSourceCommands source, TTempDataParsingCommand *ptr_TempDataParsingCommand);
void PF_VALVE_DELAY_BEFORE_OPEN(char* parString, uint8_t *ptr_out_buf, uint8_t *ptr_str_reply, TypeSourceCommands source, TTempDataParsingCommand *ptr_TempDataParsingCommand);
void PF_CHECK_OPEN_TIME(char* parString, uint8_t *ptr_out_buf, uint8_t *ptr_str_reply, TypeSourceCommands source, TTempDataParsingCommand *ptr_TempDataParsingCommand);
void PF_VALVE_QMIN(char* parString, uint8_t *ptr_out_buf, uint8_t *ptr_str_reply, TypeSourceCommands source, TTempDataParsingCommand *ptr_TempDataParsingCommand);
void PF_VALVE_MIN(char* parString, uint8_t *ptr_out_buf, uint8_t *ptr_str_reply, TypeSourceCommands source, TTempDataParsingCommand *ptr_TempDataParsingCommand);
void PF_CNT_VALVE_MIN(char* parString, uint8_t *ptr_out_buf, uint8_t *ptr_str_reply, TypeSourceCommands source, TTempDataParsingCommand *ptr_TempDataParsingCommand);
void PF_VALVE_AUTO_CONTROL(char* parString, uint8_t *ptr_out_buf, uint8_t *ptr_str_reply, TypeSourceCommands source, TTempDataParsingCommand *ptr_TempDataParsingCommand);
void PF_VALVE_CLOSE_REV_P(char* parString, uint8_t *ptr_out_buf, uint8_t *ptr_str_reply, TypeSourceCommands source, TTempDataParsingCommand *ptr_TempDataParsingCommand);
void PF_VALVE_CLOSE_Q_PH(char* parString, uint8_t *ptr_out_buf, uint8_t *ptr_str_reply, TypeSourceCommands source, TTempDataParsingCommand *ptr_TempDataParsingCommand);
void PF_FORCE_VALVE(char* parString, uint8_t *ptr_out_buf, uint8_t *ptr_str_reply, TypeSourceCommands source, TTempDataParsingCommand *ptr_TempDataParsingCommand);
void PF_EN_MDM_CHNG_VLV(char* parString, uint8_t *ptr_out_buf, uint8_t *ptr_str_reply, TypeSourceCommands source, TTempDataParsingCommand *ptr_TempDataParsingCommand);
void PF_VER_PO(char* parString, uint8_t *ptr_out_buf, uint8_t *ptr_str_reply, TypeSourceCommands source, TTempDataParsingCommand *ptr_TempDataParsingCommand);
void PF_RSTSMT(char* parString, uint8_t *ptr_out_buf, uint8_t *ptr_str_reply, TypeSourceCommands source, TTempDataParsingCommand *ptr_TempDataParsingCommand);
void PF_REV_FL_ON_PER_S(char* parString, uint8_t *ptr_out_buf, uint8_t *ptr_str_reply, TypeSourceCommands source, TTempDataParsingCommand *ptr_TempDataParsingCommand);
void PF_REV_FL_OFF_PER_S(char* parString, uint8_t *ptr_out_buf, uint8_t *ptr_str_reply, TypeSourceCommands source, TTempDataParsingCommand *ptr_TempDataParsingCommand);
void PF_CLEAR_SN_SGM(char* parString, uint8_t *ptr_out_buf, uint8_t *ptr_str_reply, TypeSourceCommands source, TTempDataParsingCommand *ptr_TempDataParsingCommand);
void PF_VALVE_DELAY_TRY_OPEN(char* parString, uint8_t *ptr_out_buf, uint8_t *ptr_str_reply, TypeSourceCommands source, TTempDataParsingCommand *ptr_TempDataParsingCommand);
void PF_VER_PROTOCOL(char* parString, uint8_t *ptr_out_buf, uint8_t *ptr_str_reply, TypeSourceCommands source, TTempDataParsingCommand *ptr_TempDataParsingCommand);
void PF_MODE_CONSERVATION(char* parString, uint8_t *ptr_out_buf, uint8_t *ptr_str_reply, TypeSourceCommands source, TTempDataParsingCommand *ptr_TempDataParsingCommand);
void PF_KONTRAST(char* parString, uint8_t *ptr_out_buf, uint8_t *ptr_str_reply, TypeSourceCommands source, TTempDataParsingCommand *ptr_TempDataParsingCommand);
void PF_LCD_PARAM(char* parString, uint8_t *ptr_out_buf, uint8_t *ptr_str_reply, TypeSourceCommands source, TTempDataParsingCommand *ptr_TempDataParsingCommand);
void PF_READY_TO_DIALOG(char* parString, uint8_t *ptr_out_buf, uint8_t *ptr_str_reply, TypeSourceCommands source, TTempDataParsingCommand *ptr_TempDataParsingCommand);
void PF_READYTD(char* parString, uint8_t *ptr_out_buf, uint8_t *ptr_str_reply, TypeSourceCommands source, TTempDataParsingCommand *ptr_TempDataParsingCommand);




//uint8_t PFW_ShortMeassure(char* parString);

//uint8_t PFW_ManualK(char* parString);


uint8_t PFW_Gl_Error(char* parString);
//uint8_t PFW_All_Mod_ErrorCnt(char* parString);

uint8_t PFW_TestRTime(char* parString);
uint8_t PFW_TestLTime(char* parString);

uint8_t PFW_N_GasRTime(char* parString);
uint8_t PFW_GasRange(char* parString);

uint8_t PFW_Set_KalmanFilter(char* parString);
uint8_t PFW_KalmanK(char* parString);
uint8_t PFW_KalmanBorderL(char* parString);
uint8_t PFW_KalmanBorderH(char* parString);
uint8_t change_KalmanBorder(char* ptr_str, uint8_t source);
uint8_t PFW_KalmanCounter(char* parString);



uint8_t PFR_HWVers(char* parString);
uint8_t PFR_DevName(char* parString);
//uint8_t PFW_GasSTime(char* parString);

extern const uint16_t MAX_FUNC;
extern const char UnitsName[];
extern const TParser PFunc[];
extern uint32_t tick_count_message_from_OPTIC_sec;

uint8_t while_TransmitEnable ();
uint8_t Str2DataTime(LL_RTC_DateTypeDef* p_Date,LL_RTC_TimeTypeDef* p_Time, char*parString, uint8_t check_time);
uint32_t Get_tick_count_message_from_OPTIC_sec();
uint8_t change_Qmax_warning_cnt_measurement(char* parString, uint8_t flag_source);
uint8_t change_Qmax_cnt_measurement(char* parString, uint8_t flag_source);
uint8_t change_Tmax_warning(char* parString, uint8_t flag_source);
uint8_t change_Tmin_warning(char* parString, uint8_t flag_source);
uint8_t change_cnt_measurement_temper_warning_eeprom(char* parString, uint8_t flag_source);
uint8_t change_GasRecognitionInterval(char* parString, TypeSourceCommands flag_source);
uint8_t change_Kfactor_range(char* parString, TypeSourceCommands flag_source);
uint8_t change_Kfactor_measure_cnt_eeprom(char* parString, TypeSourceCommands flag_source);
uint8_t change_KalmanBorderL(char* ptr_str, uint8_t source);
uint8_t change_KalmanBorderH(char* ptr_str, uint8_t source);
uint8_t ParsingMESSAGE_MOTO_INFO_SET(char *ptr_buf_str, uint8_t flag_source);
uint8_t ParsingMESSAGE_MOTO_INFO(uint8_t *ptr_buf, uint8_t part);
void ChangeValve(char* parString, uint8_t *ptr_out_buf, uint8_t *ptr_str_reply, TypeSourceCommands source, TTempDataParsingCommand *ptr_TempDataParsingCommand, TypeValveAction action);
uint8_t Str2DataTime(LL_RTC_DateTypeDef* p_Date,LL_RTC_TimeTypeDef* p_Time, char*parString, uint8_t check_time);
#endif