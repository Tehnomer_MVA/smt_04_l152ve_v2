#ifndef __magic_h__
#define __magic_h__

#define START_UPDATE_FW		0x08000000
#define SIZE_RX_BUF			512
#define SIZE_TX_BUF			1024
#define DOWN_LOAD_DATA_SIZE	260//(SIZE_RX_BUF-8)
#define FLASH_CNT_BYTE		8388607
#define PAGE_SIZE			256
#define CMD_READ			0x03
#define WAIT_TIMEOUT		20000

typedef struct 
	{
	char RxBuf[SIZE_RX_BUF];		// ��������� �� ����� ������ ������
	char TxBuf[SIZE_TX_BUF];		// ��������� �� ����� �������� ������
	uint32_t RxMaxByte;
	uint32_t TxMaxByte;
	uint32_t RxCountByte;			// ���-�� �������� ����
	uint32_t TxCountByte;			// ���-�� ���� ��� ��������
	void (*SendData)(void);
	} SCom;

typedef struct
	{
	uint32_t Size;
	uint32_t FlashAddress;
	} SUpdateFile;

extern void magic(uint32_t Size, uint32_t CrcFile);

#endif